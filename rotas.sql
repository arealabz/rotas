--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.unique_schema_migrations;
ALTER TABLE ONLY public.videos DROP CONSTRAINT videos_pkey;
ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
ALTER TABLE ONLY public.uploads DROP CONSTRAINT uploads_pkey;
ALTER TABLE ONLY public.upload_folders DROP CONSTRAINT upload_folders_pkey;
ALTER TABLE ONLY public.translations DROP CONSTRAINT translations_pkey;
ALTER TABLE ONLY public.translation_smls DROP CONSTRAINT translation_smls_pkey;
ALTER TABLE ONLY public.static_pages DROP CONSTRAINT static_pages_pkey;
ALTER TABLE ONLY public.static_page_smls DROP CONSTRAINT static_page_smls_pkey;
ALTER TABLE ONLY public.servizis DROP CONSTRAINT servizis_pkey;
ALTER TABLE ONLY public.mods DROP CONSTRAINT mods_pkey;
ALTER TABLE ONLY public.mod_smls DROP CONSTRAINT mod_smls_pkey;
ALTER TABLE ONLY public.mod_cats DROP CONSTRAINT mod_cats_pkey;
ALTER TABLE ONLY public.mod_cat_smls DROP CONSTRAINT mod_cat_smls_pkey;
ALTER TABLE ONLY public.media_videos DROP CONSTRAINT media_videos_pkey;
ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
ALTER TABLE ONLY public.lingues DROP CONSTRAINT lingues_pkey;
ALTER TABLE public.videos ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.uploads ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.upload_folders ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.translations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.translation_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.static_pages ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.static_page_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.servizis ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mods ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_cats ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_cat_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.media_videos ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.media ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.lingues ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.videos_id_seq;
DROP TABLE public.videos;
DROP SEQUENCE public.users_id_seq;
DROP TABLE public.users;
DROP SEQUENCE public.uploads_id_seq;
DROP TABLE public.uploads;
DROP SEQUENCE public.upload_folders_id_seq;
DROP TABLE public.upload_folders;
DROP SEQUENCE public.translations_id_seq;
DROP TABLE public.translations;
DROP SEQUENCE public.translation_smls_id_seq;
DROP TABLE public.translation_smls;
DROP SEQUENCE public.static_pages_id_seq;
DROP TABLE public.static_pages;
DROP SEQUENCE public.static_page_smls_id_seq;
DROP TABLE public.static_page_smls;
DROP SEQUENCE public.servizis_id_seq;
DROP TABLE public.servizis;
DROP TABLE public.schema_migrations;
DROP SEQUENCE public.mods_id_seq;
DROP TABLE public.mods;
DROP SEQUENCE public.mod_smls_id_seq;
DROP TABLE public.mod_smls;
DROP SEQUENCE public.mod_cats_id_seq;
DROP TABLE public.mod_cats;
DROP SEQUENCE public.mod_cat_smls_id_seq;
DROP TABLE public.mod_cat_smls;
DROP SEQUENCE public.media_videos_id_seq;
DROP TABLE public.media_videos;
DROP SEQUENCE public.media_id_seq;
DROP TABLE public.media;
DROP SEQUENCE public.lingues_id_seq;
DROP TABLE public.lingues;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: lingues; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lingues (
    id integer NOT NULL,
    cod character varying(255),
    lingua character varying(255),
    active integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: lingues_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lingues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lingues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lingues_id_seq OWNED BY lingues.id;


--
-- Name: lingues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('lingues_id_seq', 1, true);


--
-- Name: media; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE media (
    id integer NOT NULL,
    fkfile integer,
    fkmod integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    descrizione text,
    ordine integer DEFAULT 0
);


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE media_id_seq OWNED BY media.id;


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('media_id_seq', 355, true);


--
-- Name: media_videos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE media_videos (
    id integer NOT NULL,
    fkvideo integer,
    fkmod integer,
    descrizione text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: media_videos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE media_videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE media_videos_id_seq OWNED BY media_videos.id;


--
-- Name: media_videos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('media_videos_id_seq', 21, true);


--
-- Name: mod_cat_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_cat_smls (
    id integer NOT NULL,
    fkparent integer,
    fklang character varying(255),
    title character varying(255),
    abstract text,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_cat_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_cat_smls_id_seq OWNED BY mod_cat_smls.id;


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_cat_smls_id_seq', 11, true);


--
-- Name: mod_cats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_cats (
    id integer NOT NULL,
    idserv integer,
    f_del integer,
    ordine integer,
    fkparent integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published integer,
    settori character varying,
    applicazioni character varying
);


--
-- Name: mod_cats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_cats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_cats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_cats_id_seq OWNED BY mod_cats.id;


--
-- Name: mod_cats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_cats_id_seq', 13, true);


--
-- Name: mod_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_smls (
    id integer NOT NULL,
    fkparent integer,
    fklang character varying(255),
    url_title character varying(255),
    title character varying(255),
    abstract text,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mod_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_smls_id_seq OWNED BY mod_smls.id;


--
-- Name: mod_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_smls_id_seq', 196, true);


--
-- Name: mods; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mods (
    id integer NOT NULL,
    idserv integer,
    fkcat integer,
    home character varying(255),
    f_del integer,
    ordine integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published integer,
    data_evento character varying(255),
    etc character varying(255),
    settori character varying,
    applicazioni character varying
);


--
-- Name: mods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mods_id_seq OWNED BY mods.id;


--
-- Name: mods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mods_id_seq', 196, true);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: servizis; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servizis (
    id integer NOT NULL,
    fkparent integer,
    nome character varying(255),
    label character varying(255),
    enabled integer,
    ordine integer,
    path character varying(255),
    skip_cat integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: servizis_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE servizis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: servizis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE servizis_id_seq OWNED BY servizis.id;


--
-- Name: servizis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('servizis_id_seq', 7, true);


--
-- Name: static_page_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE static_page_smls (
    id integer NOT NULL,
    title character varying(255),
    subtitle character varying(255),
    header text,
    testo text,
    footer text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    static_page_id integer,
    fklang character varying(255)
);


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE static_page_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE static_page_smls_id_seq OWNED BY static_page_smls.id;


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('static_page_smls_id_seq', 15, true);


--
-- Name: static_pages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE static_pages (
    id integer NOT NULL,
    f_del integer,
    published integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    foto character varying(255),
    foto_home character varying(255),
    url_pagina character varying(255),
    home_page integer,
    ordine integer,
    related_cat character varying(255)
);


--
-- Name: static_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE static_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: static_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE static_pages_id_seq OWNED BY static_pages.id;


--
-- Name: static_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('static_pages_id_seq', 15, true);


--
-- Name: translation_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE translation_smls (
    id integer NOT NULL,
    translation_id integer,
    testo text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fklang character varying(255)
);


--
-- Name: translation_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE translation_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: translation_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE translation_smls_id_seq OWNED BY translation_smls.id;


--
-- Name: translation_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('translation_smls_id_seq', 1, false);


--
-- Name: translations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE translations (
    id integer NOT NULL,
    etc character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE translations_id_seq OWNED BY translations.id;


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('translations_id_seq', 1, false);


--
-- Name: upload_folders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE upload_folders (
    id integer NOT NULL,
    fkparent integer,
    nome character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    f_del integer DEFAULT 0
);


--
-- Name: upload_folders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE upload_folders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: upload_folders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE upload_folders_id_seq OWNED BY upload_folders.id;


--
-- Name: upload_folders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('upload_folders_id_seq', 24, true);


--
-- Name: uploads; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE uploads (
    id integer NOT NULL,
    filename character varying(255),
    ext character varying(255),
    content_type character varying(255),
    file_size integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fkfolder integer
);


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE uploads_id_seq OWNED BY uploads.id;


--
-- Name: uploads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('uploads_id_seq', 193, true);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255),
    email character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying(255),
    remember_token character varying(255),
    level integer,
    active integer
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: videos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE videos (
    id integer NOT NULL,
    url text,
    img text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title character varying(255),
    url_name character varying(255)
);


--
-- Name: videos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE videos_id_seq OWNED BY videos.id;


--
-- Name: videos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('videos_id_seq', 20, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lingues ALTER COLUMN id SET DEFAULT nextval('lingues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY media ALTER COLUMN id SET DEFAULT nextval('media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY media_videos ALTER COLUMN id SET DEFAULT nextval('media_videos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_cat_smls ALTER COLUMN id SET DEFAULT nextval('mod_cat_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_cats ALTER COLUMN id SET DEFAULT nextval('mod_cats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_smls ALTER COLUMN id SET DEFAULT nextval('mod_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mods ALTER COLUMN id SET DEFAULT nextval('mods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY servizis ALTER COLUMN id SET DEFAULT nextval('servizis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY static_page_smls ALTER COLUMN id SET DEFAULT nextval('static_page_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY static_pages ALTER COLUMN id SET DEFAULT nextval('static_pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY translation_smls ALTER COLUMN id SET DEFAULT nextval('translation_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY translations ALTER COLUMN id SET DEFAULT nextval('translations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY upload_folders ALTER COLUMN id SET DEFAULT nextval('upload_folders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY uploads ALTER COLUMN id SET DEFAULT nextval('uploads_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY videos ALTER COLUMN id SET DEFAULT nextval('videos_id_seq'::regclass);


--
-- Data for Name: lingues; Type: TABLE DATA; Schema: public; Owner: -
--

COPY lingues (id, cod, lingua, active, created_at, updated_at) FROM stdin;
1	it	Italiano	1	2012-11-08 10:06:53.342303	2012-11-08 10:06:53.342303
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: -
--

COPY media (id, fkfile, fkmod, created_at, updated_at, descrizione, ordine) FROM stdin;
164	29	11	2012-11-22 15:47:17.365991	2012-11-22 15:47:17.365991	\N	0
165	28	12	2012-11-22 15:47:24.297727	2012-11-22 15:47:24.297727	\N	0
166	28	3	2012-11-22 15:51:24.27114	2012-11-22 15:51:24.27114	\N	0
167	29	3	2012-11-22 15:51:24.276778	2012-11-22 15:51:24.276778	\N	0
168	36	13	2012-12-13 16:02:56.525261	2012-12-13 16:02:56.525261	\N	0
169	32	14	2012-12-13 16:03:32.389903	2012-12-13 16:03:32.389903	\N	0
170	35	15	2012-12-13 16:03:57.555728	2012-12-13 16:03:57.555728	\N	0
172	39	18	2012-12-13 16:05:16.45786	2012-12-13 16:05:16.45786	\N	0
173	31	19	2012-12-13 16:05:43.604711	2012-12-13 16:05:43.604711	\N	0
174	38	20	2012-12-13 16:06:19.328594	2012-12-13 16:06:19.328594	\N	0
175	37	21	2012-12-13 16:06:54.65078	2012-12-13 16:06:54.65078	\N	0
176	33	22	2012-12-13 16:07:18.724664	2012-12-13 16:07:18.724664	\N	0
177	38	35	2013-03-14 10:53:06.949932	2013-03-14 10:53:06.949932	\N	0
178	34	36	2013-03-14 10:54:01.719634	2013-03-14 10:54:01.719634	\N	0
179	35	36	2013-03-14 10:54:01.724431	2013-03-14 10:54:01.724431	\N	0
180	36	36	2013-03-14 10:54:01.7276	2013-03-14 10:54:01.7276	\N	0
181	34	16	2013-07-08 10:02:46.489531	2013-07-08 10:06:32.635469	fasdf	2
182	36	16	2013-07-08 10:02:46.494886	2013-07-08 10:06:34.322149	asdfasdf	1
183	40	23	2013-07-15 15:15:22.758363	2013-07-15 15:16:08.98777	FARMACEUTICA	0
184	41	61	2013-07-22 07:24:18.345333	2013-07-22 07:24:18.345333	\N	0
185	42	62	2013-07-22 07:34:09.730479	2013-07-22 07:34:09.730479	\N	0
186	43	65	2013-07-22 07:45:58.722011	2013-07-22 07:45:58.722011	\N	0
187	44	66	2013-07-22 07:48:18.828595	2013-07-22 07:48:18.828595	\N	0
188	45	67	2013-07-22 08:05:20.417028	2013-07-22 08:05:20.417028	\N	0
189	46	68	2013-07-22 08:07:19.451881	2013-07-22 08:07:19.451881	\N	0
190	47	69	2013-07-22 08:08:41.83803	2013-07-22 08:08:41.83803	\N	0
191	48	70	2013-07-22 08:09:58.438724	2013-07-22 08:09:58.438724	\N	0
192	49	71	2013-07-22 08:12:33.849621	2013-07-22 08:12:33.849621	\N	0
193	50	72	2013-07-22 08:14:17.515593	2013-07-22 08:14:17.515593	\N	0
194	51	73	2013-07-22 08:17:17.446669	2013-07-22 08:17:17.446669	\N	0
195	52	74	2013-07-22 08:38:51.293772	2013-07-22 08:38:51.293772	\N	0
196	53	75	2013-07-22 08:40:16.588554	2013-07-22 08:40:16.588554	\N	0
197	54	76	2013-07-22 08:41:36.750079	2013-07-22 08:41:36.750079	\N	0
200	56	77	2013-07-22 08:44:38.280094	2013-07-22 08:44:38.280094	\N	0
201	55	78	2013-07-22 08:46:34.306358	2013-07-22 08:46:34.306358	\N	0
202	58	79	2013-07-22 08:48:04.790155	2013-07-22 08:48:04.790155	\N	0
203	59	80	2013-07-22 08:49:19.032268	2013-07-22 08:49:19.032268	\N	0
204	61	82	2013-07-22 09:12:14.517596	2013-07-22 09:12:14.517596	\N	0
205	60	81	2013-07-22 09:12:24.921422	2013-07-22 09:12:24.921422	\N	0
206	62	83	2013-07-22 09:13:35.24509	2013-07-22 09:13:35.24509	\N	0
207	63	84	2013-07-22 09:14:53.519216	2013-07-22 09:14:53.519216	\N	0
208	64	85	2013-07-22 09:15:59.90488	2013-07-22 09:15:59.90488	\N	0
209	65	86	2013-07-22 09:17:12.889964	2013-07-22 09:17:12.889964	\N	0
210	66	87	2013-07-22 09:18:10.027013	2013-07-22 09:18:10.027013	\N	0
211	67	88	2013-07-22 09:19:10.820245	2013-07-22 09:19:10.820245	\N	0
212	68	89	2013-07-22 09:32:49.389167	2013-07-22 09:32:49.389167	\N	0
213	69	90	2013-07-22 09:33:57.558407	2013-07-22 09:33:57.558407	\N	0
214	70	91	2013-07-22 09:35:12.000109	2013-07-22 09:35:12.000109	\N	0
215	71	92	2013-07-22 09:36:27.88693	2013-07-22 09:36:27.88693	\N	0
216	72	93	2013-07-22 09:38:07.348212	2013-07-22 09:38:07.348212	\N	0
217	73	94	2013-07-22 09:39:05.301306	2013-07-22 09:39:05.301306	\N	0
218	74	95	2013-07-22 09:52:22.813678	2013-07-22 09:52:22.813678	\N	0
219	75	96	2013-07-22 09:53:39.318629	2013-07-22 09:53:39.318629	\N	0
220	76	97	2013-07-22 09:55:10.616195	2013-07-22 09:55:10.616195	\N	0
221	77	98	2013-07-22 09:56:17.952933	2013-07-22 09:56:17.952933	\N	0
222	78	99	2013-07-22 09:57:20.1081	2013-07-22 09:57:20.1081	\N	0
223	79	100	2013-07-22 09:58:18.276266	2013-07-22 09:58:18.276266	\N	0
243	97	110	2013-07-23 13:18:02.072288	2013-07-23 13:18:02.072288	\N	0
245	96	102	2013-07-23 13:18:36.659769	2013-07-23 13:18:36.659769	\N	0
247	100	101	2013-07-23 13:24:07.418548	2013-07-23 13:24:07.418548	\N	0
249	105	109	2013-07-23 13:24:54.047056	2013-07-23 13:24:54.047056	\N	0
251	99	105	2013-07-23 13:25:15.679177	2013-07-23 13:25:15.679177	\N	0
253	98	103	2013-07-23 13:25:48.501355	2013-07-23 13:25:48.501355	\N	0
255	103	106	2013-07-23 13:26:17.348363	2013-07-23 13:26:17.348363	\N	0
257	104	108	2013-07-23 13:26:33.696637	2013-07-23 13:26:33.696637	\N	0
259	101	107	2013-07-23 13:26:56.627617	2013-07-23 13:26:56.627617	\N	0
261	102	104	2013-07-23 13:27:15.30061	2013-07-23 13:27:15.30061	\N	0
262	106	111	2013-07-23 13:36:55.417263	2013-07-23 13:36:55.417263	\N	0
263	107	112	2013-07-23 13:37:14.357832	2013-07-23 13:37:14.357832	\N	0
265	111	116	2013-07-23 13:38:10.248672	2013-07-23 13:38:10.248672	\N	0
266	109	114	2013-07-23 13:38:29.386347	2013-07-23 13:38:29.386347	\N	0
267	110	115	2013-07-23 13:38:50.073258	2013-07-23 13:38:50.073258	\N	0
268	112	113	2013-07-23 14:11:19.499408	2013-07-23 14:11:19.499408	\N	0
269	114	117	2013-07-25 06:39:13.089783	2013-07-25 06:39:13.089783	\N	0
270	115	118	2013-07-25 06:39:50.166139	2013-07-25 06:39:50.166139	\N	0
271	116	119	2013-07-25 06:40:28.718039	2013-07-25 06:40:28.718039	\N	0
273	118	121	2013-07-25 06:59:08.601897	2013-07-25 06:59:08.601897	\N	0
274	119	122	2013-07-25 06:59:35.971293	2013-07-25 06:59:35.971293	\N	0
275	120	123	2013-07-25 07:00:04.794982	2013-07-25 07:00:04.794982	\N	0
276	121	124	2013-07-25 07:00:39.109634	2013-07-25 07:00:39.109634	\N	0
308	140	145	2013-07-29 06:46:53.1977	2013-07-29 06:46:53.1977	\N	0
298	122	125	2013-07-25 08:00:59.682982	2013-07-25 08:00:59.682982	\N	0
309	141	144	2013-07-29 06:47:33.259725	2013-07-29 06:47:33.259725	\N	0
310	142	146	2013-07-29 06:51:03.9499	2013-07-29 06:51:03.9499	\N	0
286	123	126	2013-07-25 07:05:30.347078	2013-07-25 07:05:30.347078	\N	0
287	124	127	2013-07-25 07:06:54.359673	2013-07-25 07:06:54.359673	\N	0
288	125	128	2013-07-25 07:07:18.347977	2013-07-25 07:07:18.347977	\N	0
289	126	129	2013-07-25 07:10:56.624316	2013-07-25 07:10:56.624316	\N	0
290	127	130	2013-07-25 07:11:19.98527	2013-07-25 07:11:19.98527	\N	0
291	128	131	2013-07-25 07:11:38.610003	2013-07-25 07:11:38.610003	\N	0
292	129	132	2013-07-25 07:11:55.512971	2013-07-25 07:11:55.512971	\N	0
293	130	134	2013-07-25 07:12:15.818612	2013-07-25 07:12:15.818612	\N	0
294	131	135	2013-07-25 07:12:31.756508	2013-07-25 07:12:31.756508	\N	0
295	132	136	2013-07-25 07:12:49.450516	2013-07-25 07:12:49.450516	\N	0
299	117	120	2013-07-25 08:20:35.081428	2013-07-25 08:20:35.081428	\N	0
300	134	137	2013-07-26 16:24:08.590314	2013-07-26 16:24:08.590314	\N	0
301	133	140	2013-07-26 16:24:22.417495	2013-07-26 16:24:22.417495	\N	0
302	135	139	2013-07-26 16:24:42.881544	2013-07-26 16:24:42.881544	\N	0
303	136	138	2013-07-26 16:24:57.145562	2013-07-26 16:24:57.145562	\N	0
304	137	141	2013-07-29 06:35:11.58766	2013-07-29 06:35:11.58766	\N	0
305	138	142	2013-07-29 06:36:27.514914	2013-07-29 06:36:27.514914	\N	0
306	139	143	2013-07-29 06:40:22.285023	2013-07-29 06:40:22.285023	\N	0
311	143	147	2013-07-29 06:52:34.932092	2013-07-29 06:52:34.932092	\N	0
312	145	148	2013-07-29 06:55:38.717562	2013-07-29 06:55:38.717562	\N	0
313	149	149	2013-07-29 06:57:11.81033	2013-07-29 06:57:11.81033	\N	0
314	146	151	2013-07-29 06:58:40.489355	2013-07-29 06:58:40.489355	\N	0
315	144	152	2013-07-29 07:17:07.655163	2013-07-29 07:17:07.655163	\N	0
316	169	153	2013-07-29 07:22:16.990124	2013-07-29 07:22:16.990124	\N	0
317	171	154	2013-07-29 07:26:08.460219	2013-07-29 07:26:08.460219	\N	0
318	152	155	2013-07-29 07:27:43.95737	2013-07-29 07:27:43.95737	\N	0
319	147	156	2013-07-29 07:29:31.352728	2013-07-29 07:29:31.352728	\N	0
321	166	157	2013-07-29 07:31:02.780491	2013-07-29 07:31:02.780491	\N	0
322	150	158	2013-07-29 07:32:43.231755	2013-07-29 07:32:43.231755	\N	0
323	163	159	2013-07-29 07:33:57.219928	2013-07-29 07:33:57.219928	\N	0
324	173	160	2013-07-29 07:36:52.135745	2013-07-29 07:36:52.135745	\N	0
325	174	161	2013-07-29 07:39:48.50986	2013-07-29 07:39:48.50986	\N	0
326	151	162	2013-07-29 07:41:24.438448	2013-07-29 07:41:24.438448	\N	0
327	155	163	2013-07-29 07:42:44.551706	2013-07-29 07:42:44.551706	\N	0
328	165	164	2013-07-29 07:44:51.560929	2013-07-29 07:44:51.560929	\N	0
329	153	165	2013-07-29 07:46:10.684127	2013-07-29 07:46:10.684127	\N	0
330	154	166	2013-07-29 07:48:43.019033	2013-07-29 07:48:43.019033	\N	0
331	156	168	2013-07-29 07:50:08.466992	2013-07-29 07:50:08.466992	\N	0
332	157	169	2013-07-29 07:52:47.391005	2013-07-29 07:52:47.391005	\N	0
333	158	170	2013-07-29 07:54:24.12056	2013-07-29 07:54:24.12056	\N	0
334	159	171	2013-07-29 07:55:39.817991	2013-07-29 07:55:39.817991	\N	0
335	162	172	2013-07-29 07:57:21.165195	2013-07-29 07:57:21.165195	\N	0
336	164	173	2013-07-29 07:58:31.465559	2013-07-29 07:58:31.465559	\N	0
337	160	174	2013-07-29 08:00:00.686103	2013-07-29 08:00:00.686103	\N	0
338	161	175	2013-07-29 08:01:08.15366	2013-07-29 08:01:08.15366	\N	0
339	185	180	2013-07-30 13:12:22.498012	2013-07-30 13:12:22.498012	\N	0
340	179	182	2013-07-30 13:13:03.448217	2013-07-30 13:13:03.448217	\N	0
341	186	184	2013-07-30 13:14:24.001997	2013-07-30 13:14:24.001997	\N	0
342	181	186	2013-07-30 13:14:55.242452	2013-07-30 13:14:55.242452	\N	0
343	184	188	2013-07-30 13:15:23.811541	2013-07-30 13:15:23.811541	\N	0
344	177	181	2013-07-30 13:16:10.611551	2013-07-30 13:16:10.611551	\N	0
345	180	183	2013-07-30 13:16:52.554734	2013-07-30 13:16:52.554734	\N	0
346	183	196	2013-07-30 13:17:23.453757	2013-07-30 13:17:23.453757	\N	0
347	178	185	2013-07-30 13:17:48.374748	2013-07-30 13:17:48.374748	\N	0
348	182	194	2013-07-30 13:18:23.144009	2013-07-30 13:18:23.144009	\N	0
349	188	189	2013-07-30 13:25:37.568134	2013-07-30 13:25:37.568134	\N	0
350	187	190	2013-07-30 13:26:09.024928	2013-07-30 13:26:09.024928	\N	0
351	190	191	2013-07-30 13:36:34.509806	2013-07-30 13:36:34.509806	\N	0
352	189	192	2013-07-30 13:36:57.200048	2013-07-30 13:36:57.200048	\N	0
353	193	193	2013-07-30 13:48:55.240037	2013-07-30 13:48:55.240037	\N	0
354	192	195	2013-07-30 13:49:20.896809	2013-07-30 13:49:20.896809	\N	0
355	191	187	2013-07-30 13:49:38.93211	2013-07-30 13:49:38.93211	\N	0
\.


--
-- Data for Name: media_videos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY media_videos (id, fkvideo, fkmod, descrizione, created_at, updated_at) FROM stdin;
8	16	8	rugby	2012-11-16 16:53:59.107313	2012-11-16 16:53:59.107313
9	17	8	spot	2012-11-16 16:54:05.729308	2012-11-16 16:54:05.729308
12	18	2	torta	2012-11-16 16:54:22.051043	2012-11-16 16:54:22.051043
13	19	2	mercato	2012-11-16 16:54:30.381283	2012-11-16 16:54:30.381283
21	18	10	test	2012-11-16 17:06:28.862281	2012-11-16 17:06:28.862281
\.


--
-- Data for Name: mod_cat_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_cat_smls (id, fkparent, fklang, title, abstract, description, created_at, updated_at) FROM stdin;
1	1	it	Home Page			2012-11-08 10:12:19.847346	2012-11-08 10:12:19.847346
6	8	it	new tech			2012-11-12 13:55:13.337737	2012-11-12 13:55:13.337737
7	9	it	etc			2012-11-12 13:55:17.420343	2012-11-12 13:55:17.420343
8	10	it	banner home			2012-11-22 15:25:57.793287	2012-11-22 15:25:57.793287
9	11	it	Categoria principale settori			2013-03-11 14:07:03.01533	2013-03-11 14:07:03.01533
10	12	it	Categoria principale applicazioni			2013-03-11 14:10:19.433234	2013-03-11 14:10:19.433234
3	5	it	Industria			2012-11-08 11:06:18.469259	2013-03-11 14:26:37.473871
4	6	it	RFID			2012-11-12 13:55:03.310651	2013-03-11 14:26:47.03269
5	7	it	Special Apps			2012-11-12 13:55:08.476802	2013-03-11 14:26:56.333184
11	13	it	Categoria blog			2013-03-14 10:47:17.960801	2013-03-14 10:47:17.960801
2	2	it	Wine, Beverage & Oil			2012-11-08 10:25:33.883798	2013-07-30 14:00:54.068147
\.


--
-- Data for Name: mod_cats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_cats (id, idserv, f_del, ordine, fkparent, created_at, updated_at, published, settori, applicazioni) FROM stdin;
1	2	1	\N	0	2012-11-08 10:12:19.836172	2012-11-09 10:45:35.180418	\N	\N	\N
10	4	0	\N	0	2012-11-22 15:25:57.785238	2012-11-22 15:25:57.785238	1	\N	\N
11	5	0	\N	0	2013-03-11 14:07:03.009558	2013-03-11 14:07:03.009558	1	\N	\N
12	6	0	\N	0	2013-03-11 14:10:19.430324	2013-03-11 14:10:19.430324	1	\N	\N
8	3	1	\N	0	2012-11-12 13:55:13.333891	2013-03-11 14:27:00.719572	1	\N	\N
9	3	1	\N	0	2012-11-12 13:55:17.416183	2013-03-11 14:27:03.131103	1	\N	\N
13	7	0	\N	0	2013-03-14 10:47:17.951971	2013-03-14 10:47:17.951971	1	\N	\N
7	3	0	4	0	2012-11-12 13:55:08.472597	2013-03-14 13:08:22.578045	1	\N	\N
5	3	0	2	0	2012-11-08 11:06:18.44113	2013-07-23 13:33:22.657771	1	110#109#108#107#106#105#104#103#102#101	116#115#114#113#112#111
6	3	0	3	0	2012-11-12 13:55:03.16869	2013-07-30 10:17:46.001504	1	129#130#131#132#134#135#136	117#118#120#122#121#123#124#125#126#127#128
2	3	0	1	0	2012-11-08 10:25:33.877003	2013-07-30 14:49:10.476326	1	137#138#139#140	#
\.


--
-- Data for Name: mod_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_smls (id, fkparent, fklang, url_title, title, abstract, description, created_at, updated_at) FROM stdin;
1	1	it	Slogan	Slogan	Costruiamo soluzioni	\N	2012-11-08 10:12:43.966402	2012-11-08 10:12:43.966402
2	2	it	Primo-elemento-industria	Primo elemento industria	\N	testo primo elemento industria	2012-11-08 10:26:01.80017	2012-11-08 10:26:01.80017
3	3	it	Elemento-industria	Elemento industria	\N	testo elemento industria	2012-11-08 10:49:54.741358	2012-11-08 10:49:54.741358
4	4	it	test	test	\N	test	2012-11-08 10:57:28.557509	2012-11-08 10:57:28.557509
5	5	it	Ampliamenti	Ampliamenti	\N	asdf asdfa sf e	2012-11-08 10:58:55.264961	2012-11-08 10:58:55.264961
6	6	it	asfgsdg	asfgsdg	\N	sdfgsdgrgr	2012-11-08 11:00:40.802295	2012-11-08 11:00:40.802295
7	7	it	atatrert-a	atatrert a	\N	a fga r r rr r	2012-11-08 11:02:02.49621	2012-11-08 11:02:02.49621
8	8	it	test	test	\N	test	2012-11-08 15:33:04.201089	2012-11-08 15:33:04.201089
9	9	it	tartwert-wer	tartwert wer	\N	t wert wert werte	2012-11-08 17:04:10.565961	2012-11-08 17:04:10.565961
10	10	it	test-NFC	test NFC	\N	test	2012-11-12 16:54:42.801657	2012-11-12 16:54:42.801657
11	11	it	Il-team	Il team	\N	il team	2012-11-22 15:26:14.012826	2012-11-22 15:36:23.616491
12	12	it	banner-2	banner 2	\N		2012-11-22 15:38:11.842039	2012-11-22 15:38:11.842039
13	13	it	Industry-01	Industry 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:02:30.019362	2012-12-13 16:02:30.019362
14	14	it	Industry-02	Industry 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:03:10.180798	2012-12-13 16:03:10.180798
15	15	it	Industry-03	Industry 03	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:03:51.951439	2012-12-13 16:03:51.951439
16	16	it	NFC-01	NFC 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:04:20.707184	2012-12-13 16:04:20.707184
17	17	it	NFC-02	NFC 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:04:42.090306	2012-12-13 16:04:42.090306
49	49	it	Biglietti-da-visita	Biglietti da visita	\N	\N	2013-03-26 10:23:08.40304	2013-03-26 10:23:08.40304
50	50	it	Anti-contraffazione	Anti-contraffazione	\N	\N	2013-03-26 10:23:18.793612	2013-03-26 10:23:18.793612
51	51	it	Anti-taccheggio	Anti-taccheggio	\N	\N	2013-03-26 10:23:26.393134	2013-03-26 10:23:26.393134
52	52	it	Auto-adesive-main-purpose	Auto-adesive main purpose	\N	\N	2013-03-26 10:23:36.733594	2013-03-26 10:23:36.733594
53	53	it	Pubblicit%C3%A1	Pubblicitá	\N	\N	2013-03-26 10:23:44.865553	2013-03-26 10:23:44.865553
20	20	it	RFID-01	RFID 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:06:08.476915	2012-12-13 16:06:08.476915
21	21	it	New-Tech-01	New Tech 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:06:45.233553	2012-12-13 16:06:45.233553
22	22	it	New-Tech-02	New Tech 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:07:10.605995	2012-12-13 16:07:10.605995
23	23	it	Farmaceutica	Farmaceutica	\N	\N	2013-03-11 14:08:49.669845	2013-03-11 14:08:49.669845
24	24	it	Chimica-e-prodotti-casa	Chimica e prodotti casa	\N	\N	2013-03-11 14:09:05.984938	2013-03-11 14:09:05.984938
25	25	it	Tecnics-&-Electronics	Tecnics & Electronics	\N	\N	2013-03-11 14:09:18.266966	2013-03-11 14:09:18.266966
26	26	it	Food-&-Beverage	Food & Beverage	\N	\N	2013-03-11 14:09:29.80222	2013-03-11 14:09:29.80222
27	27	it	Automotive	Automotive	\N	\N	2013-03-11 14:09:38.208682	2013-03-11 14:09:38.208682
29	29	it	Logistica	Logistica	\N	\N	2013-03-11 14:10:03.385038	2013-03-11 14:10:03.385038
30	30	it	Sicurezza	Sicurezza	\N	\N	2013-03-11 14:10:28.526453	2013-03-11 14:10:28.526453
31	31	it	Anti-contraffazione	Anti contraffazione	\N	\N	2013-03-11 14:10:36.885981	2013-03-11 14:10:36.885981
32	32	it	Decorative	Decorative	\N	\N	2013-03-11 14:10:45.142182	2013-03-11 14:10:45.142182
33	33	it	Funzionali	Funzionali	\N	\N	2013-03-11 14:10:49.83411	2013-03-11 14:10:49.83411
34	34	it	Promozionali	Promozionali	\N	\N	2013-03-11 14:10:58.511456	2013-03-11 14:10:58.511456
35	35	it	Test	Test	\N	test	2013-03-14 10:47:32.600754	2013-03-14 10:47:32.600754
36	36	it	altro-elemento-blog	altro elemento blog	\N	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquet justo quis massa commodo vel lacinia lectus malesuada. Mauris et nibh sed erat tempor aliquam. Suspendisse eleifend nunc sit amet risus ornare posuere. Donec sollicitudin dictum risus sit amet suscipit. Fusce vitae magna massa, sit amet semper lectus. Etiam sagittis nisl at dolor accumsan elementum. Sed a arcu sapien, ac adipiscing quam. Sed malesuada nibh vitae leo pellentesque facilisis. Donec metus turpis, rutrum sed blandit nec, rutrum id dolor. </p><p>Pellentesque mattis egestas lacinia. Sed in felis at nibh commodo adipiscing et eget ipsum. Curabitur porttitor mollis nulla, et gravida diam convallis sit amet. Nulla sed diam et lectus tempus hendrerit. Phasellus in lacus eu ante rutrum ultricies. Nunc nunc dui, placerat non imperdiet eget, luctus sed purus. Maecenas ante diam, rhoncus id accumsan vel, ultrices vel metus. Mauris placerat dui et lorem commodo porttitor. Aliquam aliquam metus in ipsum adipiscing ullamcorper. Cras fringilla eros eu purus condimentum feugiat faucibus purus mollis. Phasellus purus eros, condimentum scelerisque congue eget, tincidunt eget ante. Donec blandit velit facilisis odio tincidunt venenatis dictum leo vulputate. Cras vel orci lorem, ut volutpat mauris. </p>	2013-03-14 10:53:55.518091	2013-03-14 10:54:04.002704
38	38	it	Pubblica-amministrazione	Pubblica amministrazione	\N	\N	2013-03-26 10:20:42.474317	2013-03-26 10:20:42.474317
39	39	it	Gestione-aziendale	Gestione aziendale	\N	\N	2013-03-26 10:20:57.77247	2013-03-26 10:20:57.77247
40	40	it	Gestione-consumer/negozi	Gestione consumer/negozi	\N	\N	2013-03-26 10:21:08.971648	2013-03-26 10:21:08.971648
41	41	it	Sport	Sport	\N	\N	2013-03-26 10:21:18.3802	2013-03-26 10:21:18.3802
42	42	it	Sanit%C3%A1	Sanitá	\N	\N	2013-03-26 10:21:25.876064	2013-03-26 10:21:25.876064
43	43	it	Gestione-eventi	Gestione eventi	\N	\N	2013-03-26 10:21:34.487536	2013-03-26 10:21:34.487536
44	44	it	Gestione-accessi-veicoli/persone	Gestione accessi veicoli/persone	\N	\N	2013-03-26 10:22:10.44681	2013-03-26 10:22:10.44681
45	45	it	Ticketing	Ticketing	\N	\N	2013-03-26 10:22:21.283512	2013-03-26 10:22:21.283512
46	46	it	Gestione-rifiuti	Gestione rifiuti	\N	\N	2013-03-26 10:22:39.900137	2013-03-26 10:22:39.900137
47	47	it	Logistica	Logistica	\N	\N	2013-03-26 10:22:47.348318	2013-03-26 10:22:47.348318
48	48	it	Asset-Tracking	Asset Tracking	\N	\N	2013-03-26 10:22:58.028825	2013-03-26 10:22:58.028825
37	37	it	Tag-hf-adesivi	Tag hf adesivi	\N		2013-03-26 09:30:32.985503	2013-03-26 10:33:20.783407
54	54	it	Timing	Timing	\N	\N	2013-03-26 10:23:52.609394	2013-03-26 10:23:52.609394
55	55	it	Gestione-e-controllo-linee-di-produzione	Gestione e controllo linee di produzione	\N	\N	2013-03-26 10:24:04.881129	2013-03-26 10:24:04.881129
18	18	it	Tag-Adesivi	Tag Adesivi	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:05:08.1442	2013-03-26 10:32:23.849422
28	28	it	Cosmetica-&-body-care	Cosmetica & body care	\N	\N	2013-03-11 14:09:51.927192	2013-07-23 06:55:04.000325
19	19	it	Tag-uhf-adesivi	Tag uhf adesivi	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:05:37.720639	2013-03-26 10:32:49.778227
56	56	it	Tag-ufh-bracciale	Tag ufh bracciale	\N		2013-03-26 10:35:38.56825	2013-03-26 10:35:38.56825
57	57	it	Keyfob	Keyfob	\N		2013-03-26 10:36:12.871913	2013-03-26 10:36:12.871913
58	58	it	Tag-on-metal	Tag on metal	\N		2013-03-26 10:36:38.935847	2013-03-26 10:36:38.935847
59	59	it	Biglietti-da-visita-NFC	Biglietti da visita NFC	\N		2013-03-26 10:37:11.573739	2013-03-26 10:37:11.573739
60	60	it	Adesivi-NFC	Adesivi NFC	\N		2013-03-26 10:37:36.221532	2013-03-26 10:37:36.221532
62	62	it	Etichetta-VOID	Etichetta VOID	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">etichetta “Tamper Proof” anti-manomissione.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="color:#545757;position:relative;top:0.5pt;"><span style="font-family:'Times New Roman';font-size:7pt;">&nbsp;&nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Quando si tenta di rimuovere l’etichetta, l’adesivo si separa da questa, secondo un pattern o totalmente. Sulla superficie di applicazione rimane un segnale &nbsp;che evidenzia il tentativo di manomissione e impedisce un successivo utilizzo dell’etichetta stessa.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">In alcuni casi, il segnale che rimane sulla superficie può essere personalizzato, aumentando ulteriormente il livello di sicurezza.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza. </span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi&nbsp; di anti-manomissione alternativi: vedi schede 1.1-RUD e 1.2-RFA.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 07:29:35.105725	2013-07-22 07:29:38.327993
84	84	it	Etichette-integrate-per-la-logistica	Etichette integrate per la logistica	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Etichette adesive integrate in una costruzione non autoadesiva.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Il prodotto consente la stampa contemporanea di dati su carta semplice e su una o più etichette autoadesive: il foglio è di materiale non autoadesivo, ma è dotato di una o più etichette adesive, staccabili dal foglio, del formato desiderato.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ possibile unire in un unico foglio dati di diversa natura e con differente destinazione, riducendo anche possibili errori di etichettatura dei colli.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ possibile avere diversi colori, formati e numero di etichette su ciascun foglio.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note: </span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi alternativi per la logistica vedi schede 7.4 RFM, 7.7 RMU, 7.10 RDM.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 09:14:24.994027	2013-07-22 09:14:47.497801
63	63	it	Etichetta-ultradistruttibile-anti-manomissione	Etichetta ultradistruttibile anti-manomissione	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Etichetta in materiale distruttibile anti-manomissione.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta è composta da un particolare materiale che, abbinato alle caratteristiche dell’adesivo, si rompe in piccoli pezzi in caso di rimozione, rendendo evidenti i tentativi di manomissione e impedendo che possa essere riutilizzata.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Buon rapporto costi-prestazioni.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Non idoneo alla sovrastampa laser e alle alte temperature in generale.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi&nbsp; di anti-manomissione alternativi: vedi scheda 1.2-RFA e 1.3-RVL.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 07:35:13.567511	2013-07-23 07:14:13.705513
64	64	it	Etichette-WOOD	Etichette WOOD	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">etichetta anti-contraffazione, con inchiostro fluorescente.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Una parte dell’etichetta viene stampata con un inchiostro invisibile ad occhio nudo. L’inchiostro reagisce ai raggi UV di una speciale lampada (Wood) emettendo una luce colorata, generata da un fenomeno di fluorescenza</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Valido sistema anti-contraffazione, non essendo visibile ad occhio nudo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La lampada Wood è uno strumento di controllo diffuso in banche, negozi, grandi magazzini, essendo già impiegato per la verifica delle banconote.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’utilizzo di inchiostri Wood è obbligatorio per l’esportazione delle bevande superalcoliche in Gran Bretagna (UK).</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi anticontraffazione alternativi: vedi 2.2-ROL, 2.3-RSOC e 2.4-ROM.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 07:40:28.229026	2013-07-23 07:14:33.370142
72	72	it	Etichetta-magnetica	Etichetta magnetica	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="color:#7f7f7f;font-family:Verdana, sans-serif;font-size:12pt;">Etichetta non adesiva in materiale magnetico.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Si tratta di una calamita sottile, flessibile e stampabile. Viene unita a un supporto di carta (fogli A4 o in bobina), dove sono presenti degli opportuni pre-tagli, per facilitarne il distacco.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Loghi, immagini, messaggi personalizzabili a richiesta del cliente.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta può essere applicata a tutti i supporti metallici ferrosi, può essere rimossa senza lasciare tracce di adesivo e riapplicata per un numero illimitato di volte.</span></li></ul>	2013-07-22 08:13:29.256872	2013-07-23 07:22:27.673497
66	66	it	Etichetta-con-ologrammi-miniaturizzati	Etichetta con ologrammi miniaturizzati	\N	<p><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="color:#7f7f7f;font-family:Verdana, sans-serif;font-size:12pt;line-height:1.5;">Etichetta anti-contraffazione con micro-ologrammi.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">I&nbsp; micro-ologrammi&nbsp; rappresentano&nbsp; un&nbsp; sistema&nbsp; di identificazione e anti-contraffazione tra i più completi: non sono verificabili a occhio nudo, ma si possono leggere con un microscopio illuminato 100x.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;">v<span style="font-size:7pt;font-family:'Times New Roman';">&nbsp;&nbsp; </span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Garantiscono al 100% l’autenticità del prodotto.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;">v<span style="font-size:7pt;font-family:'Times New Roman';">&nbsp;&nbsp; </span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Possono essere realizzati in varie forme e colori.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Possono sopportare temperature elevate senza degradarsi.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi anticontraffazione alternativi: vedi 2.1-RWO, 2.2-ROL e 2.3-RSOC.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 07:47:40.495015	2013-07-22 07:47:49.775333
67	67	it	Etichetta-memo-riposizionabile	Etichetta memo riposizionabile	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">etichetta stile memo riposizionabile.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta riposizionabile viene fornita già applicata su altro materiale adesivo o su altri fogli non adesivi. Il memo è staccabile e riposizionabile più volte. </span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Il maggior impatto visivo è utile per dare risalto a promozioni, inviti, promemoria,&nbsp; ecc.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Venendo forniti insieme, è possibile stampare in un'unica pagina una lettera e il memo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ possibile personalizzare sia i fogli che il memo con loghi o messaggi.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La costruzione è sovrastampabile a trasferimento termico, laser o inkjet.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 08:04:46.83022	2013-07-23 07:18:56.385921
69	69	it	Etichetta-a-bindello	Etichetta a bindello	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">binde<span style="letter-spacing:-.05pt;">l</span>lo non adesivo.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Il bindello viene impiegato per dare maggior visibilità al prodotto,<span style="letter-spacing:3.5pt;"> </span>fare promozioni, comunicare messaggi in<span style="letter-spacing:.05pt;"> </span>generale</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Velocità di applicazione: le<span style="letter-spacing:.05pt;"> </span>etichette vengono consegnate su un supporto in bobina, per consentirne la dispensazione in modo automatico, anche ad alta velocità.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabile a trasfe<span style="letter-spacing:-.05pt;">r</span>imento termico.</span></li></ul><p style="line-height:10.0pt;"><strong></strong></p><p style="line-height:10.0pt;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Possiamo<span style="letter-spacing:-.5pt;"> </span>fornire<span style="letter-spacing:-.5pt;"> </span>applicatrici<span style="letter-spacing:-.5pt;"> </span>ad<span style="letter-spacing:-.5pt;"> </span>alta<span style="letter-spacing:-.5pt;"> </span>velocità<span style="letter-spacing:-.5pt;"> </span>per<span style="letter-spacing:-.5pt;"> </span>bindelli, fino a 10.000 et/h.</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></li></ul>	2013-07-22 08:08:12.545352	2013-07-23 07:20:35.293608
70	70	it	Etichette-per-offerte-2x1	Etichette per offerte 2x1	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">: </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">etichetta promozionale offerte 2x1.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">Riduzione dei costi del packaging: le confezioni singole vengono unite mediante queste etichette,&nbsp; permettendo una maggiore flessibilità e un risparmio di materiale per il confezionamento</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">Si possono realizzare confezioni da due<span style="letter-spacing:4.2pt;"> </span>o più pezzi, senza grandi modifiche agli imballaggi.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">E’ possibile esporre i prodotti in offerta già accoppiati, eventualmente anche appendendoli.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">possibilità di stampare dati aggiuntivi sull’etichetta.</span></li></ul>	2013-07-22 08:09:31.203879	2013-07-23 07:21:39.187235
71	71	it	Etichette-grattabili	Etichette grattabili	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta con inchiostro scratch-off coprente</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta può essere impiegata per vari scopi: per nascondere un messaggio di possibile vincita, uno sconto, per realizzare dei giochi o per gestire l’utilizzo di codici nascosti, in abbinamento ad un sistema anti-contraffazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Coprenza costante in tutta la lavorazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Durata nel tempo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Resistenza a graffi accidentali e manipolazioni durante e dopo la lavorazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Possibilità di personalizzare la parte grattabile con scritte, immagini, loghi e messaggi specifici per ogni cliente.&nbsp;</span></li></ul>	2013-07-22 08:11:51.517053	2013-07-23 07:22:03.626055
77	77	it	Etichette-ad-alta-adesivit%C3%A0	Etichette ad alta adesività	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Etichette con adesivo speciale per applicazioni su superfici difficili.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Etichette con adesivo ad alta tenuta, ideale per superfici critiche, in presenza di siliconi o altre sostanze che annullano l’adesivo, o per finiture grezze, stoffe, velluto</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Disponibile in differenti versioni a seconda dell’applicazione (carta, film, ecc.)</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Non idoneo alla stampa laser e alte temperature in genere.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La rimozione dell’etichetta può rovinare la superficie su cui era applicata, a causa dell’aggressività dell’adesivo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi ad alta adesività alternativi: vedi scheda 7.18-RHER</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione: il prodotto deve essere testato e sviluppato in maniera corretta, per evitare fenomeni di&nbsp; <em>bleeding.</em></span></li></ul>	2013-07-22 08:42:39.524862	2013-07-22 08:43:07.592113
78	78	it	Etichetta-a-fustellatura-multipla	Etichetta a fustellatura multipla	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">etichetta per la logistica con fustellatura multipla.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Facilita la logistica interna e la gestione documentale: l’etichetta è suddivisa in più parti che vengono stampate contemporaneamente. Le varie parti possono essere staccate successivamente, in momenti diversi, e attaccate sul prodotto o su altre superfici (bolle&nbsp; di&nbsp; accompagnamento, documenti di controllo, buste, ecc), riducendo il rischio di errori di etichettatura.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione in radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi alternativi per la logistica: vedi schede 7.7-RMU, 7.10-RDM, 7.12-RIL.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p>	2013-07-22 08:45:33.860429	2013-07-22 08:45:35.107801
73	73	it	Etichetta-Braille	Etichetta Braille	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">etichetta con stampa a rilievo Braille e triangolo tattile.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">L’etichetta Braille nasce dall’attenzione di Rotas per la sicurezza.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">La direttiva dell’UE 2004/27, recepita in Italia con la legge 149, sancisce che tutti i prodotti farmaceutici (etici ed OTC) debbano riportare le informazioni più importanti, anche in caratteri Braille, sulla superficie esterna della confezione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">Tutti i prodotti pericolosi, corrosivi, tossici, infiammabili e nocivi devono riportare almeno il triangolo tattile e le frasi di rischio in caratteri Braille.</span></li><li><span style="color:#545757;font-family:'Times New Roman';font-size:9px;line-height:13px;">&nbsp; &nbsp;</span><span style="color:#545757;font-family:Verdana, sans-serif;font-size:12pt;line-height:1.5;">Risparmio economico rispetto alla punzonatura della confezione, specialmente quando la quantità non è sufficiente ad assorbire i costi di punzonatura.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">Indispensabile quando non é possibile punzonare la confezione, ad esempio per sacchetti in film plastico.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;position:relative;top:.5pt;">Dimensione e spessore sono a norma di legge (direttiva comunitaria</span><span style="font-size:12pt;font-family:Verdana, sans-serif;">&nbsp;</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;position:relative;top:.5pt;">2004/27/CE e norma UNI EN 15823 ).</span></li></ul>	2013-07-22 08:16:17.809332	2013-07-23 07:22:57.37042
75	75	it	Etichette-con-adesivo-coprente	Etichette con adesivo coprente	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta con adesivo nero coprente.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Riduzione delle varianti di contenitori, imballi e confezioni: le etichette possono essere <span style="letter-spacing:.6pt;">&nbsp;</span>utilizzate per la correzione di errori di etichettatura o per aggiornare dati obsoleti.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Garanzia di copertura, anche di codici a barre.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="line-height:10.0pt;"><strong></strong></p><p style="line-height:10.0pt;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;layout-grid-mode:line;">Consultare l’Ufficio RFID per verificare l’applicazione</span></li></ul>	2013-07-22 08:39:47.048463	2013-07-23 07:32:23.197223
76	76	it	Etichetta-apri-e-chiudi	Etichetta apri e chiudi	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">etichetta sigillo “apri-e-chiudi”.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">La costruzione è ottenuta grazie a zone con diverse proprietà. La prima, permanente, rimane saldamente adesa alla confezione. La seconda,<span style="letter-spacing:-.75pt;"> </span>rimovibile<span style="letter-spacing:-.75pt;"> </span>speciale, permette l’apertura e la chiusura ripetuta del packaging. La<span style="letter-spacing:-.75pt;"> </span>terza, senza adesivo, facilita le operazioni di sollevamento dell’etichetta.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;">v<span style="font-size:7pt;font-family:'Times New Roman';">&nbsp;&nbsp; </span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">Il prodotto resta fragrante anche dopo l’apertura della confezione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">Lunga durata delle pro<span style="letter-spacing:-.05pt;">p</span>rietà di rimovibilità e riposizionabilità.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">Ottima resistenza all’all<span style="letter-spacing:-.05pt;">u</span>ngamento e allo strappo, anche dopo molti utilizzi.</span></li></ul><p style="line-height:10.0pt;"><strong></strong></p><p style="line-height:10.0pt;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 08:41:01.593465	2013-07-23 07:33:46.17191
74	74	it	Etichetta-per-contenitori-appesi	Etichetta per contenitori appesi	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta da applicare a bottiglie e contenitori che devono essere appesi.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta dispone di una parte adesiva appositamente studiata per sopportare il peso di una bottiglia farmaceutica o una sacca di sangue. Può venire utilizzata al posto dei comuni reggi-flaconi in plastica.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Pur essendo trasparente l’etichetta è stampata con uno speciale inchiostro che ne permette il rilevamento con apposite fotocellule UV.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Ciascun lotto di produzione è severamente testato per soddisfare la normativa ISO 15137 secondo una precisa procedura di test.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabile a trasferimento termico.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 08:38:11.274919	2013-07-23 07:56:19.129361
79	79	it	Etichetta-multistrato	Etichetta multistrato	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Etichetta per la logistica in materiale multistrato.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="color:#545757;position:relative;top:0.5pt;"><span style="font-family:'Times New Roman';font-size:7pt;">&nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta è composta di una o più sezioni che poggiano su di una speciale siliconata adesiva, protetta a sua volta da un normale <em>liner</em> siliconato. Viene applicata quando è richiesta un’operazione di ri-etichettatura con adesivo permanente, oppure il riutilizzo di una parte dell’etichetta.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Facile manipolazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">I dati stampati sull’etichetta possono essere rimossi senza difficoltà, o trasportati su altri moduli, oggetti o confezioni, certi dell’ identificazione univoca, senza trascriverli.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi alternativi per la logistica vedi schede 7.4 RFM, 7.10 RDM, 7.12 RIM.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">consultare l’Ufficio Tecnico e/o RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 08:47:25.173906	2013-07-22 08:47:27.293325
80	80	it	Etichetta-idrosolubile	Etichetta idrosolubile	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Etichetta idrosolubile.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Le etichette dopo essere state applicate ed&nbsp;&nbsp; utilizzate, possono essere rimosse in acqua facilmente, senza lasciare tracce.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Prevenzione dei rischi di intasamento degli scarichi e dei filtri nei sistemi di lavaggio: una volta solubilizzata l’etichetta, rimangono solo delle microparticelle solide in sospensione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabili a trasferimento termico.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note: </span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 08:48:50.839977	2013-07-22 08:48:53.951391
81	81	it	Rotas-Smart-Hangtag	Rotas Smart Hangtag	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">bindello non adesivo, fornito in forma di doppia etichetta adesiva.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Il prodotto viene fornito in rotoli, sotto forma di etichette adesive, per facilitare la stampa dei dati variabili su una normale stampante. Una volta stampate, le etichette vengono staccate dal supporto e piegate a metà lungo la perforazione, ottenendo così un “tag” non adesivo, che può essere appeso.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Evita l’utilizzo di altre etichette da applicare al bindello già in uso.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Applicazione manuale o semiautomatica.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Diverse possibilità di colori e formati.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabili a trasferimento termico o inkjet.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 09:10:54.00733	2013-07-22 09:10:55.235542
82	82	it	Etichetta-solare	Etichetta solare	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">etichetta con inchiostro sensibile alla luce.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Lasciando esposta l’etichetta al sole, il materiale speciale con cui è costituita cambia progressivamente colore. Al raggiungimento del colore pieno, il prodotto solare dovrebbe essere applicato nuovamente</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta è di facile utilizzo: è sufficiente rimuovere la pellicola di protezione pretagliata, al momento d’applicazione del prodotto solare, e verificare periodicamente il colore l’etichetta.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Utile: aiuta a prevenire le scottature della pelle: aiuta a capire visivamente quando dovrebbe essere il momento di riapplicare il prodotto solare.</span></li></ul>	2013-07-22 09:12:03.400164	2013-07-22 09:12:04.733678
83	83	it	Etichette-a-doppio-materiale	Etichette a doppio materiale	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">etichetta per la logistica, composta da 2 differenti materiali: uno per interni, uno per esterni, permanente a lunga durata.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ possibile associare diversi materiali da stampa nello stesso foglio: plastica + carta, metallo + plastica, metallo + carta, materiale adesivo e non adesivo, ecc.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sono disponibili diverse combinazioni di tipologie di adesivo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Disponibile in foglio o in bobina.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi alternativi per la logistica vedi schede 7.4 RFM, 7.7 RMU, 7.12 RIM.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 09:13:16.292692	2013-07-22 09:13:27.980555
85	85	it	Card-personalizzabili	Card personalizzabili	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">card personalizzabile e plastificabile dall’utente.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Queste card sono realizzate in modo da rendere possibile la personalizzazione (inserimento dati, foto, ecc.) e la successiva plastificazione protettiva anche direttamente dall’utilizzatore, grazie alla particolare costruzione “stacca-gira-attacca-separa”.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Diverse possibilità di colori e formati</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Il film di plastificazione può essere realizzato in materiale olografico.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabili a trasferimento termico, laser o inkjet.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Integrabili con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li></ul>	2013-07-22 09:15:38.31515	2013-07-22 09:15:40.560463
86	86	it	Etichetta-anticorrosione	Etichetta anticorrosione	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Etichetta per la prevenzione alla corrosione di metalli ferrosi o accoppiati.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta RDL è un metodo di protezione alla corrosione di oggetti metallici a freddo, in materiale e adesivo speciali, conduttivi.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Corrosione elettrolitica: l’utilizzo della RDL previene il naturale processo di corrosione della superficie metallica in contatto con l’etichetta in ambienti umidi, essendo l’etichetta stessa a corrodersi, in luogo del metallo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Accoppiamento tra metalli: il punto di contatto tra 2 metalli con diversi potenziali di ossidazione, in presenza di umidità, agisce come una cella galvanica, così che uno dei 2 metalli forza la corrosione dell’altro, come in una batteria. Il posizionamento di una RDL tra i 2 metalli evita che questo fenomeno si verifichi, fungendo da anodo sacrificale.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Metodo più economico, semplice e rapido dei processi di zincatura a caldo o a freddo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consente la protezione di particolari metallici già in opera.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consente la protezione di parti metalliche nel caso in cui non si possa, o non sia necessario, proteggere l’intera struttura, ma solamente le parti critiche.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:16:53.665573	2013-07-22 09:17:06.63152
87	87	it	HERCULES%E2%80%9A-etichetta-ad-alta-adesivit%C3%A0	HERCULES‚ etichetta ad alta adesività	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta per superfici particolarmente difficili, materiali umidi, sporchi, molto ruvidi e porosi.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Etichetta con adesivo permanente, progettata per applicazioni dove è richiesta una elevata resistenza dell’adesivo sulla superficie di applicazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La conformabilità dell’etichetta influenza le proprietà di sollevamento del bordo, fornendo un'ottima adesione a superfici difficili (presenza di umidità, tracce di cere o siliconi, materiali rugosi, porosi, soffici ecc..), tollerando fluttuazioni di temperatura e umidità sulla linea di produzione, durante lo stoccaggio e nelle varie fasi di vita del prodotto.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabile a trasferimento termico (TTR).</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note: </span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:17:51.334335	2013-07-22 09:18:03.330634
88	88	it	Etichette-avvolgibili	Etichette avvolgibili	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">etichetta “wrap-around” parzialmente adesiva, per oggetti di forma cilindrica.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta permette l’avvolgimento di oggetti cilindrici in modo automatico.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Lo speciale design<span style="letter-spacing:.05pt;"> dell’etichetta </span>evita la<span style="letter-spacing:.05pt;"> </span>formazione di grinze antiestetiche durante l’avvolgimento, specialmente nel caso in cui il prodotto non abbia una<span style="letter-spacing:.05pt;"> </span>superficie regolare.</span></li></ul><p style="line-height:10.0pt;"><strong></strong></p><p style="line-height:10.0pt;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">consultare l’Ufficio Tecnico per verificare l’applicazione</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p>	2013-07-22 09:18:50.211428	2013-07-22 09:19:05.105822
89	89	it	Etichetta-per-rimozione-ad-aria	Etichetta per rimozione ad aria	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Etichetta con adesivo speciale, che consente la rimozione dell’etichetta con getto d’aria.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta rimovibile a getto d’aria è indicata per tutti i settori dove è necessario il riutilizzo e la ri-etichettatura dei contenitori dei prodotti, pur garantendo un’ottima adesione durante l’utilizzo. La particolare disposizione dell’adesivo consente infatti il passaggio di un getto d’aria compressa per consentire la rimozione dell’etichetta.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Riduzione dei costi di imballaggio (contenitori a rendere).</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:32:14.644779	2013-07-22 09:32:18.669018
90	90	it	Disco-di-protezione-per-mole-abrasive	Disco di protezione per mole abrasive	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Etichette non adesive in materiale termosaldabile.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ l’etichetta a fungere da confezione: viene chiusa a caldo durante il riempimento</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Può essere utilizzata per prodotti confezionati in buste monodose, come sementi, bulbi, prodotti cosmetici, ecc.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:33:30.237109	2013-07-22 09:33:50.877515
91	91	it	Etichette-Termosaldabili	Etichette Termosaldabili	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Etichette non adesive in materiale termosaldabile.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ l’etichetta a fungere da confezione: viene chiusa a caldo durante il riempimento</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Può essere utilizzata per prodotti confezionati in buste monodose, come sementi, bulbi, prodotti cosmetici, ecc.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:34:52.003693	2013-07-22 09:35:05.829145
92	92	it	Etichetta-eco-compatibile-Biolabel	Etichetta eco-compatibile Biolabel	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta<span style="letter-spacing:.05pt;"> in materiale </span>eco-compatibile.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Il materiale, ottenuto dalla pietra, è resistente ed ha una consistenza al tatto simile alla carta, ma con il pregio di resistere all’acqua.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Rispetto all’equivalente quantità di carta, una tonnellata di<span style="letter-spacing:4.2pt;"> </span>biolabel<span style="letter-spacing:4.2pt;"> </span>consente il risparmio di:</span><ul style="margin-left:30px;"><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">20 Alberi</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">20 kg di reflui di scarico</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">2000 litri di acqua di processo</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">80 kg di rifiuti solidi</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">120 kg di emissioni in atmosfera</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">4 milioni e mezzo di calorie</span></strong></li><li><strong><span style="font-family:'Verdana','sans-serif';color:#7F7F7F;">1/3 dell’inchiostro</span></strong></li></ul></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">rispetto ambientale (materiale a basso LCA).</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">sovrastampabile a trasferimento termico</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Trova applicazione nell’etichettatura di flaconi di detergenti, cosmetici e cura del corpo, e più in generale per tutti quei prodotti dove si voglia evidenziare il rispetto per l’ambiente.</span></li></ul>	2013-07-22 09:36:06.532989	2013-07-22 09:36:19.552402
93	93	it	Etichette-durevoli	Etichette durevoli	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><strong style="text-indent:-3cm;font-size:0.75em;"><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></strong></p><ul><li><span style="text-indent:-3cm;font-size:12pt;font-family:Verdana, sans-serif;color:#7f7f7f;position:relative;top:0.5pt;">Etichette tecniche durevoli nel tempo.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Tutti i materiali che compongono l’etichetta sono selezionati in modo da garantire durata dell’etichetta, leggibilità dei dati stampati e adesività nel tempo</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Le etichette durevoli aderiscono ottimamente sia sui metalli che su una vasta gamma di plastiche, e si prestano per l’etichettatura di avvertenza ed identificazione di beni durevoli.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Ottima resistenza alle temperature estreme, agli agenti atmosferici, allo strappo e al taglio, allo scolorimento da raggi UV, agli agenti chimici, all’abrasione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabili a trasferimento termico con opportuni ribbon.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Possibilità di integrare con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;line-height:115%;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico/RFID per verificare l’applica</span></li></ul>	2013-07-22 09:37:48.929691	2013-07-22 09:37:59.583069
94	94	it	Etichetta-Blocca-Sacco	Etichetta Blocca-Sacco	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Etichetta multiadesivo per sacchi in plastica.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta consente di fissare in modo agevole il sacco al supporto (cestini, anelli, contenitori, ecc) e, al temine dell’utilizzo, possono essere aperte nuovamente, senza causare la lacerazione del sacco.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Fissaggio e separazione agevole del sacco dal relativo supporto, da usare in luogo di elastici o altri sistemi di fissaggio.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;">v<span style="font-size:7pt;font-family:'Times New Roman';">&nbsp;&nbsp; </span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Il frontale viene selezionato in base alla natura del film plastico del sacco: in questo modo è sempre garantita una perfetta compatibilità dell’etichetta con il materiale del sacco.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">E’ possibile avere il prodotto anche in versione BIODEGRADABILE!</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;line-height:normal;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:38:51.506584	2013-07-22 09:38:53.030363
95	95	it	Rotas-Metal-Chiesel	Rotas Metal Chiesel	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Decorazione metallizzata con particolari grafici dettagliati.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta è decorata con una lamina metallizzata, in combinazione con una precisa e particolare lavorazione, che la rende più viva e sorprendentemente dettagliata.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Identifica a prima vista il prodotto, distinguendolo da tutti gli altri, contribuendo all’autenticità e rappresentando un efficace strumento di marketing.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La particolare lavorazione rende le etichette difficilmente copiabili.</span></li></ul>	2013-07-22 09:51:41.734493	2013-07-22 09:51:43.70203
96	96	it	Etichetta-con-resina-3D	Etichetta con resina 3D	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Etichetta decorativa resinata.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Effetto decorativo tridimensionale di grande effetto.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Maggiore impatto visivo rispetto ad un’etichetta tradizionale.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La resina ad alto spessore è molto resistente e non si degrada nel tempo.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Le sagome devono essere semplici (cerchi, o forme geometriche regolari).</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:53:08.342409	2013-07-22 09:53:28.440387
97	97	it	Etichetta-ultratrasparente	Etichetta ultratrasparente	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Etichetta ad altissima trasparenza.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Le etichette sono realizzate con pellicole altamente trasparenti, con effetto no-label-look.</span>&nbsp;<span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sostituisce quindi la serigrafia diretta sul prodotto, riducendo i costi del packaging e rendendo più economici anche eventuali cambi della grafica stampata.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta ad alta trasparenza è adatta a tutte le tecniche di stampa.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La stabilità dei materiali con cui è realizzata l’etichetta permette alte velocità di applicazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Ottima trasparenza sia del materiale che dell’adesivo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sovrastampabile a trasferimento termico.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La piena trasparenza si sviluppa 24h dopo l’applicazione.</span></li></ul>	2013-07-22 09:54:48.974032	2013-07-22 09:55:03.423827
163	163	it	Luccarelli	Luccarelli	\N		2013-07-29 07:41:57.067671	2013-07-29 07:41:57.067671
164	164	it	Lum%C3%A0	Lumà	\N		2013-07-29 07:43:55.396625	2013-07-29 07:43:55.396625
165	165	it	Monte-Rossa	Monte Rossa	\N		2013-07-29 07:45:13.961582	2013-07-29 07:45:13.961582
166	166	it	Primitivo-di-Manduria	Primitivo di Manduria	\N		2013-07-29 07:46:59.509027	2013-07-29 07:46:59.509027
167	167	it	Primitivo-di-Manduria	Primitivo di Manduria	\N		2013-07-29 07:46:59.559672	2013-07-29 07:46:59.559672
168	168	it	Rivalta-Valdobbiadene	Rivalta Valdobbiadene	\N		2013-07-29 07:49:11.769703	2013-07-29 07:49:11.769703
169	169	it	Rubinaia-grande-amoroso	Rubinaia grande amoroso	\N		2013-07-29 07:50:41.920381	2013-07-29 07:50:41.920381
170	170	it	Soliccum-Soligo	Soliccum Soligo	\N		2013-07-29 07:53:23.081814	2013-07-29 07:53:23.081814
171	171	it	Valdobbiadene-Millesimato	Valdobbiadene Millesimato	\N		2013-07-29 07:54:53.675001	2013-07-29 07:54:53.675001
172	172	it	Terre-alte-Pecorino	Terre alte Pecorino	\N		2013-07-29 07:56:30.627747	2013-07-29 07:56:30.627747
173	173	it	Tre-calici-Orcia	Tre calici Orcia	\N		2013-07-29 07:57:44.673953	2013-07-29 07:57:44.673953
174	174	it	Vesevo-Beneventano-Aglianico	Vesevo Beneventano Aglianico	\N		2013-07-29 07:59:14.359748	2013-07-29 07:59:14.359748
175	175	it	Vineyards-v8	Vineyards v8	\N		2013-07-29 08:00:24.826782	2013-07-29 08:00:24.826782
98	98	it	Etichette-con-stampa-sul-dorso	Etichette con stampa sul dorso	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">etichetta adesiva con stampa fronte e retro.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Affascinante soluzione decorativa per bottiglie, flaconi e packaging trasparenti. Un’etichetta a doppio senso, che si legge sia frontalmente che posteriormente, attraverso il vetro o la confezione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">E’ possibile realizzare etichette ricche e di effetto, dall'interazione tra i due lati della stessa etichetta alla combinazione di varie tecniche di stampa, in un’unica soluzione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Disponile anche con adesivo idoneo alle basse temperature ed in condizioni di elevata umidità.</span></li></ul>	2013-07-22 09:55:59.245125	2013-07-22 09:56:10.84898
99	99	it	Etichetta-metallizzate-ad-alto-rilievo	Etichetta metallizzate ad alto rilievo	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">etichetta metallica con dettagli ad alto rilievo.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">La speciale costruzione </span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">consente di ottenere un elevato spessore e rilievi estremamente pronunciati che, in combinazione con la superficie metallica, permettono al prodotto etichettato&nbsp; di presentarsi in modo molto<span style="letter-spacing:-.05pt;"> </span>esclusivo ed efficace.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">Idoneo per applicazione automatica</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">Soluzione versatile ed economica rispetto ad altre etichette realizzate in metallo, stagno o peltro.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#545757;">Le<span style="letter-spacing:-.5pt;"> </span>etichette<span style="letter-spacing:-.5pt;"> </span>hanno<span style="letter-spacing:-.5pt;"> </span>rilievi<span style="letter-spacing:-.55pt;"> </span>molto<span style="letter-spacing:-.5pt;"> </span>marcati<span style="letter-spacing:-.5pt;"> </span>anche<span style="letter-spacing:-.5pt;"> </span>sui<span style="letter-spacing:-.5pt;"> </span>testi<span style="letter-spacing:-.5pt;"> </span>più<span style="letter-spacing:-.5pt;"> </span>fini<span style="letter-spacing:-.5pt;"> </span>e<span style="letter-spacing:-.5pt;"> </span>possono essere realizzate riproducendo effetti oro, argento, rame, bronzo, lucidi ed opachi, oltre ad una gamma di altri effetti<span style="letter-spacing:4.2pt;"> </span>metallizzati,<span style="letter-spacing:-.05pt;"> </span>o totalmente coprenti.</span></li></ul>	2013-07-22 09:57:07.48829	2013-07-22 09:57:08.794722
100	100	it	Rotas-Holo-Chiesel	Rotas Holo Chiesel	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">etichetta con decorazione ad effetto olografico dettagliato.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La speciale lavorazione consente di ottenere dettagli estremamente fini e precisi. In combinazione con l’effetto olografico, queste etichette si presentano in modo molto esclusivo ed appariscente.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">La somiglianza della decorazione ad un vero ologramma, consente di utilizzare questa etichetta come deterrente alla contraffazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Disponibile su una vasta gamma di materiali, anche plastici.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note: </span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 09:58:10.348359	2013-07-22 09:58:12.030134
101	101	it	Farmaceutica	Farmaceutica	\N	\N	2013-07-23 06:55:47.131497	2013-07-23 06:55:47.131497
102	102	it	Chimica	Chimica	\N	\N	2013-07-23 06:56:00.557623	2013-07-23 06:56:00.557623
103	103	it	Consumer-Goods	Consumer Goods	\N	\N	2013-07-23 06:56:18.161269	2013-07-23 06:56:18.161269
104	104	it	Food-&-Beverage	Food & Beverage	\N	\N	2013-07-23 06:56:39.267345	2013-07-23 06:56:39.267345
105	105	it	Cosmetica-&-body-care	Cosmetica & body care	\N	\N	2013-07-23 06:57:59.47569	2013-07-23 06:58:23.272316
106	106	it	Logistica	Logistica	\N	\N	2013-07-23 06:58:33.013803	2013-07-23 06:58:33.013803
108	108	it	Packaging	Packaging	\N	\N	2013-07-23 06:59:18.250703	2013-07-23 06:59:18.250703
109	109	it	Prodotti-Industriali	Prodotti Industriali	\N	\N	2013-07-23 06:59:33.232602	2013-07-23 06:59:33.232602
61	61	it	Etichetta-con-fustellatura-anti-rimozione	Etichetta con fustellatura anti-rimozione	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span></p><ul><li><span style="color:gray;font-family:Verdana, sans-serif;font-size:12pt;line-height:1.5;">etichetta di sigillo antimanomissione.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp;&nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Protezione contro manomissioni, contraffazioni e frodi:</span>&nbsp;<span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">l’etichetta dispone, al suo interno, di una serie di tagli, progettati per rendere evidenti i tentativi di manomissione, o impedire che la stessa possa essere riutilizzata.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Sistema anti-manomissione semplice ed a basso costo.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">L’etichetta è realizzabile con un’ampia gamma di materiali, per rispondere alle specifiche esigenze del Cliente.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;">Combinabile con tecnologia di identificazione a radiofrequenza.</span></li></ul><p style="line-height:10.0pt;"><strong></strong></p><p style="line-height:10.0pt;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;position:relative;top:.5pt;">Note:</span></strong></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Consultare l’Ufficio Tecnico/RFID per verificare l’applicazione.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;layout-grid-mode:line;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">Sistemi&nbsp; di anti-manomissione alternativi: vedi scheda 1.1 ultradistruttibile e 1.3 Void</span></li></ul>	2013-07-22 07:13:48.652353	2013-07-23 07:10:57.267538
113	113	it	Decorative	Decorative	\N	\N	2013-07-23 08:12:35.421061	2013-07-23 08:12:56.757921
110	110	it	Office-&-Business	Office & Business	\N	\N	2013-07-23 07:00:01.662884	2013-07-23 13:18:17.615452
107	107	it	Apparel-&-Fashion	Apparel & Fashion	\N	\N	2013-07-23 06:58:57.211351	2013-07-23 15:34:07.667297
127	127	it	Ticketing	Ticketing	\N	\N	2013-07-25 06:38:17.921588	2013-07-29 14:50:27.062378
176	176	it	Anti---contraffazione	Anti - contraffazione	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong></strong></span></p><p><span style="color:#7f7f7f;font-size:small;"><strong>Descrizione:</strong></span><br /></p><ul><li><span style="font-size:small;">Etichette adesive antimanomissione con diverse tecnologie incorporabili:fustellatura speciale, Wood, Void, anti-manomissione e molte altre, da attaccare al capo d'abbigliamento, disponibili anche senza tecnologia RFId, integrabili con altri strumenti di sicurezza antitaccheggio o antimanomissione, rendendo il Tag RFid passivo e l'antitaccheggio invisibile.</span></li></ul><p><span style="color:#7f7f7f;font-size:small;"><strong>Vantaggi:</strong></span><br /></p><ul><li><span style="font-size:small;">maggiore sicurezza per il consumatore della provenienza del prodotto</span><br /></li><li><span style="font-size:small;">localizzazione immediata del prodotto</span><br /></li><li><span style="font-size:small;">associazione univoca tra il prodotto e il TAG</span><br /></li><li><span style="font-size:small;">riduzione dei falsi</span><br /></li><li><span style="font-size:small;">aumento di sicurezza delle informazioni del prodotto</span></li></ul><p style="margin-top:0.5em;margin-bottom:0.5em;padding-right:0px;padding-left:0px;font-family:Arial, sans-serif, Verdana, Helvetica;line-height:15px;text-align:justify;"><br /></p>	2013-07-30 10:12:04.134937	2013-07-30 10:12:04.134937
65	65	it	Etichette-anticontraffazione-combinate	Etichette anticontraffazione combinate	\N	<p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione:</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">etichetta anti-rimozione che abbina inchiostri di sicurezza termosensibili, inchiostri grattabili e fustellatura antirimozione.</span></li></ul><p style="margin-top:0cm;margin-right:0cm;margin-bottom:0cm;margin-left:3.0cm;margin-bottom:.0001pt;text-indent:-3.0cm;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Vengono abbinati inchiostri scratch-off (argento/oro), termosensibili e/o fustellature anti-rimozione. L’etichetta garantisce così sia l’autenticità che la protezione dei dati sensibili. Eventuali manomissioni dell’etichetta risultano subito evidenti.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Gli inchiostri termosensibili sono un ottimo ed efficace sistema anti-contraffazione, non fotocopiabile e facile da verificare.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Note:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Sistemi anticontraffazione alternativi: vedi 2.1-RWO, 2.2-ROL e 2.4-ROM.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Consultare l’Ufficio Tecnico per verificare l’applicazione.</span></li></ul>	2013-07-22 07:45:20.834683	2013-07-23 07:15:31.715386
68	68	it	Etichetta-Coupon-multipagina	Etichetta Coupon multipagina	\N	<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Descrizione</span></strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">:</span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;"> </span></p><ul><li><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:gray;layout-grid-mode:line;">etichetta a pagina doppia o a libretto, plastificata e richiudibile.</span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"><strong><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;">Vantaggi:</span></strong></p><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p><ul><li><span style="color:#545757;position:relative;top:0.5pt;"><span style="font-family:'Times New Roman';font-size:7pt;">&nbsp;&nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">L’etichetta “a pagina multipla” è ripiegata, ed è così possibile comunicare una grande quantità d’informazioni attraverso un’etichetta di dimensioni ridotte.</span></li><li><span style="font-size:12.0pt;font-family:Wingdings;color:#545757;position:relative;top:.5pt;"><span style="font-size:7pt;font-family:'Times New Roman';">&nbsp; &nbsp;</span></span><span style="font-size:12.0pt;font-family:'Verdana','sans-serif';color:#7F7F7F;position:relative;top:.5pt;">Maggiore impatto visivo rispetto ad un’etichetta tradizionale. </span></li></ul><p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal;"></p>	2013-07-22 08:06:14.507583	2013-07-23 07:20:11.837182
111	111	it	Anti---contraffazione	Anti - contraffazione	\N	\N	2013-07-23 08:12:25.455471	2013-07-23 08:12:25.455471
112	112	it	Anti---manomissione	Anti - manomissione	\N	\N	2013-07-23 08:12:35.254444	2013-07-23 08:12:35.254444
114	114	it	Non-adesive	Non adesive	\N	\N	2013-07-23 08:13:08.701538	2013-07-23 08:13:08.701538
115	115	it	Promozionali	Promozionali	\N	\N	2013-07-23 08:13:23.583286	2013-07-23 08:13:23.583286
116	116	it	Tecniche	Tecniche	\N	\N	2013-07-23 08:13:44.681226	2013-07-23 08:13:44.681226
117	117	it	Anti---taccheggio	Anti - taccheggio	\N	\N	2013-07-25 06:35:37.469351	2013-07-25 06:35:37.469351
119	119	it	Autoadesive	Autoadesive	\N	\N	2013-07-25 06:36:16.486711	2013-07-25 06:36:16.486711
120	120	it	Biglietti-da-visita	Biglietti da visita	\N	\N	2013-07-25 06:36:33.084718	2013-07-25 06:36:33.084718
123	123	it	Gestione-linee-di-produzione	Gestione linee di produzione	\N	\N	2013-07-25 06:37:31.499668	2013-07-25 06:37:31.499668
124	124	it	Gestione-rifiuti	Gestione rifiuti	\N	\N	2013-07-25 06:37:44.050461	2013-07-25 06:37:44.050461
125	125	it	Logistica	Logistica	\N	\N	2013-07-25 06:37:55.343591	2013-07-25 06:37:55.343591
126	126	it	Pubblicit%C3%A0	Pubblicità	\N	\N	2013-07-25 06:38:07.452952	2013-07-25 06:38:07.452952
128	128	it	Timing	Timing	\N	\N	2013-07-25 06:38:30.259641	2013-07-25 06:38:30.259641
129	129	it	Gestione-aziendale	Gestione aziendale	\N	\N	2013-07-25 07:08:24.871654	2013-07-25 07:08:24.871654
130	130	it	Gestione-consumer---negozi	Gestione consumer - negozi	\N	\N	2013-07-25 07:08:46.369423	2013-07-25 07:08:46.369423
131	131	it	Gestione-eventi	Gestione eventi	\N	\N	2013-07-25 07:09:00.542983	2013-07-25 07:09:00.542983
132	132	it	NFC	NFC	\N	\N	2013-07-25 07:09:10.95016	2013-07-25 07:09:10.95016
133	133	it	NFC	NFC	\N	\N	2013-07-25 07:09:11.475418	2013-07-25 07:09:11.475418
134	134	it	Pubblica-amministrazione	Pubblica amministrazione	\N	\N	2013-07-25 07:09:47.105067	2013-07-25 07:09:47.105067
135	135	it	Sanit%C3%A0	Sanità	\N	\N	2013-07-25 07:10:01.770125	2013-07-25 07:10:01.770125
136	136	it	Sport	Sport	\N	\N	2013-07-25 07:10:10.603455	2013-07-25 07:10:10.603455
137	137	it	Birra	Birra	\N	\N	2013-07-26 16:20:07.5418	2013-07-26 16:20:07.5418
138	138	it	Olio	Olio	\N	\N	2013-07-26 16:20:16.780717	2013-07-26 16:20:16.780717
139	139	it	Superalcolici	Superalcolici	\N	\N	2013-07-26 16:20:36.479084	2013-07-26 16:20:36.479084
140	140	it	Vino	Vino	\N	\N	2013-07-26 16:20:44.503298	2013-07-26 16:20:44.503298
141	141	it	2Castelli-Tenuta	2Castelli Tenuta	\N		2013-07-29 06:34:14.483426	2013-07-29 06:34:14.483426
142	142	it	Amber-Oro	Amber Oro	\N		2013-07-29 06:35:41.120725	2013-07-29 06:35:41.120725
143	143	it	Borgo-Magredo	Borgo Magredo	\N		2013-07-29 06:39:41.148657	2013-07-29 06:39:41.148657
144	144	it	Ca-del-sole	Ca del sole	\N		2013-07-29 06:41:31.809535	2013-07-29 06:41:31.809535
145	145	it	Creativo-prosecco-D.O.C.	Creativo prosecco D.O.C.	\N		2013-07-29 06:45:54.516292	2013-07-29 06:45:54.516292
147	147	it	Due-lune	Due lune	\N		2013-07-29 06:51:47.548251	2013-07-29 06:51:47.548251
154	154	it	Sangiovese-Fantini-Farnese	Sangiovese Fantini Farnese	\N		2013-07-29 07:23:55.834203	2013-07-29 07:23:55.834203
148	148	it	Duo-blanc-Menade	Duo blanc Menade	\N		2013-07-29 06:54:56.553778	2013-07-29 06:54:56.553778
149	149	it	Duo-ros%C3%A8-Menade	Duo rosè Menade	\N		2013-07-29 06:56:07.480802	2013-07-29 06:56:07.480802
150	150	it	Duo-ros%C3%A8-Menade	Duo rosè Menade	\N		2013-07-29 06:56:07.494758	2013-07-29 06:56:07.494758
152	152	it	Epsilon-gold	Epsilon gold	\N		2013-07-29 07:13:43.577391	2013-07-29 07:13:43.577391
153	153	it	Epsilon-pink	Epsilon pink	\N		2013-07-29 07:17:32.543333	2013-07-29 07:17:32.543333
155	155	it	Chardonney-Fantini-Farnese	Chardonney Fantini Farnese	\N		2013-07-29 07:26:46.689529	2013-07-29 07:26:46.689529
151	151	it	Edizione-cinque-autoctoni-Farnese	Edizione cinque autoctoni Farnese	\N		2013-07-29 06:57:51.926264	2013-07-29 07:23:10.282623
146	146	it	Cuve%C3%A8-cococciola-Fantini-Farnese	Cuveè cococciola Fantini Farnese	\N		2013-07-29 06:50:27.451651	2013-07-29 07:23:23.911041
156	156	it	Colline-Teramane-Farnese	Colline Teramane Farnese	\N		2013-07-29 07:28:35.525426	2013-07-29 07:28:50.606435
157	157	it	Fiano-Salento-Magia	Fiano Salento Magia	\N		2013-07-29 07:30:06.426219	2013-07-29 07:30:06.426219
158	158	it	GB-Odoardi	GB Odoardi	\N		2013-07-29 07:32:02.615851	2013-07-29 07:32:02.615851
159	159	it	GK-Kerin-Brut	GK Kerin Brut	\N		2013-07-29 07:33:17.447661	2013-07-29 07:33:17.447661
160	160	it	GranTrio-Rosso-Salento	GranTrio Rosso Salento	\N		2013-07-29 07:34:32.921817	2013-07-29 07:34:32.921817
161	161	it	Gran-Cuve%C3%A8-Ros%C3%A8-Fantini-Farnese	Gran Cuveè Rosè Fantini Farnese	\N		2013-07-29 07:37:52.726333	2013-07-29 07:37:52.726333
162	162	it	La-Tordera	La Tordera	\N		2013-07-29 07:40:30.687705	2013-07-29 07:40:30.687705
118	118	it	Asset-Tracking	Asset Tracking	\N	\N	2013-07-25 06:35:57.648187	2013-07-29 14:50:38.25904
121	121	it	Controllo-accessi-veicoli	Controllo accessi veicoli	\N	\N	2013-07-25 06:36:56.454199	2013-07-29 14:50:51.97255
122	122	it	Controllo-accessi-persone	Controllo accessi persone	\N	\N	2013-07-25 06:37:15.042926	2013-07-29 14:51:02.388255
177	177	it	Asset-Tracking	Asset Tracking	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong>Descrizione:</strong></span></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">La tecnologia RFId applicata all'Asset Tracking consente di identificare e mettere sotto controllo i beni aziendali (ad esempio tutte le apparecchiature informatiche) di valore all'interno di una certa area oppure di tracciarli lungo un determinato processo. In questo modo si può conoscere esattamente il numero dei cespiti, la posizione e controllare la loro storia.</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">In questo contesto la tecnologia RFId offre molti vantaggi rispetto alle altre tecnologie di identificazione in quanto garantisce la lettura anche in condizioni difficili, in assenza di visibilità o in ambienti sporchi o con scarsa illuminazione. Quando graffi ed usura del tempo impediscono la lettura completa delle scritte sulla superficie, il chip inserito all'interno dell'etichetta RFId continua a mantenere la capacità di comunicare.&nbsp;</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Il Tag passivo RFId può essere anche scritto e quindi anche contenere una serie di informazioni (anche) fruibili direttamente per il tramite di dispositivi di comunicazione in radiofrequenza come, ad esempio, un lettore palmare (anche) senza necessità di connessione al database remoto.&nbsp;</span><br /></li></ul><p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong>Vantaggi:</strong></span></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Identificazione automatica degli asset</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Inventario</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Verifica ubicazione, configurazione e condizioni</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Informazioni in tempo reale</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Tracciabilità al 100%</span></li></ul>	2013-07-30 10:17:05.923012	2013-07-30 10:17:05.923012
178	178	it	Controllo-accessi-veicoli	Controllo accessi veicoli	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong>Descrizione:</strong></span></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Tag RFId UHF e HF da attaccare al parabrezza dell'auto che permettono il riconoscimento in tempo reale del veicolo e l'apertura automatica del varco / cancello con lettore RFId.</span></li></ul><p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong>Vantaggi:</strong></span><br /></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">identificazione automatica del veicolo</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">apertura automatica della barriera/ cancello</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">registrazione dei passaggi</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">totalmente a mani libere</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">controllo in tempo reale della disponibilità di parcheggio</span></li></ul>	2013-07-30 10:26:04.123229	2013-07-30 10:27:47.107486
190	190	it	Metal-Tag-con-Tag-passivi	Metal Tag con Tag passivi	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas progetta produce e distribuisce Metal Tag con caratteristiche speciali per superfici metalliche o difficili.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Negli anni abbiamo soddisfatto numerose esigenze di clienti di tutto il mondo nei più svariati campi di applicazioni arrivando sempre alla soluzione ottimale, riusciamo a personalizzare ogni componente, creando sempre un prodotto ad hoc.&nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Metal Tag RFId passivi UHF e HF resistenti, impiegabili nei settori con applicazioni estreme o speciali per un'alta resistenza all'usura, di resistenza e con performance durature nel tempo.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">I Metal Tag RFId &nbsp;di Rotas possono essere utilizzati direttamente su superfici metalliche o dentro contenitori metallici.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Disponibili in:</span></strong><br /></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Diversi tipi di housing (bulloni, occhielli metallici) per facilità di attacco alla superfice, garantiscono alta resistenza.</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Adesivi resistenti</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Sottoforma di card con spessore di 3mm (o maggiore), per identificare oggetti metallici in carrelli.</span><br /></li></ul><p><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per le applicazioni estreme:</span></strong><br /></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Lettura a distanza e immediata</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Lettura in condizioni difficili</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Lettura multipla di oggetti</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Lettura dal muletto</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Riduzione tempi in entrata e uscita merci</span><br /></li></ul>	2013-07-30 12:52:23.230431	2013-07-30 12:52:23.230431
191	191	it	Mobilifici-con-Tag-passivi-UHF	Mobilifici con Tag passivi UHF	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId specifiche per Mobilifici.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Negli anni abbiamo soddisfatto le esigenze di numerosi mobilifici italiani e nel mondo, realizzando sempre prodotti ad hoc per soddisfare ogni &nbsp;singola esigenza del cliente, migliorando la logistica in entrata ed uscita.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Questo è solo un esempio, usato in molti paesi in tutto il mondo.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Riusciamo a soddisfare ogni vostro singolo bisogno adattando il materiale, consistenza, inlay, forma e tecnologia, al singolo ambiente arrivando sempre alla migliore soluzione dei vostri &nbsp;problemi.&nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Con le soluzioni Rotas RFId puoi ridurre il tempo e &nbsp;il personale per fare meglio le stesse cose.&nbsp;</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Attraverso un software tutte le informazioni relative agli oggetti con tag / etichette RFId uhf o HF e alle operazioni svolte su di essi sono disponibili sempre e in tempo reale.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Processi produttivi e filiera:</span><br /></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Si identifica dal componente al pezzo finito con un &nbsp;Tag / etichetta rfid uhf o hf introducendolo in un database. Quando si passa da una fase di lavorazione ad un’altra si realizza una lettura (lettore manuale o automatico) attraverso varchi RFId o tunnel RFId potendo visualizzare in ogni momento attraverso un software tutte le informazioni relative alla singola parte e ad ogni fase.</span><br /></li></ul><p><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Gestione parco pallet:</span><br /></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Le etichette adesive a tag RFId UHF Roats possono inoltre essere applicate direttamente su ogni singolo pallet. Le sue caratteristiche la rendono un ideale ausilio del parco pallet, nonchè per prevenire la perdita e il furto dei pallet stessi e consentirne la rintracciabilità, monitorandone il passaggio attraverso appositi varchi o tunnel RFId.</span><br /></li></ul>	2013-07-30 12:54:07.277913	2013-07-30 12:54:07.277913
192	192	it	Raccolta-dei-rifiuti-urbani-con-Tag	Raccolta dei rifiuti urbani con Tag	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId per la Raccolta dei rifiuti urbani con Tag RFId attivi o passivi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La nostra soluzione, estremamente flessibile e versatile, è stata sviluppata per permettere la gestione completa dei vari servizi, limitando al minimo gli inserimenti e le procedure manuali dell’operatore.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Le funzionalità utilizzabili da ogni operatore sono personalizzabili.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La nostra decennale esperienza ci permette di ottenere sempre la migliore soluzione ai vostri bisogni, adattando il materiale, inlay, forma, adesivo, tecnologie, ecc.&nbsp;</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Soluzione:</span></strong></p><p><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Targhette con Tag attivi RFId che integrati in un completo sistema di gestione dei rifiuti, consentono le rilevazioni automatiche degli svuotamenti senza intervento da parte dell’operatore di raccolta ma solamente grazie ad un lettore / reader RFId che, in maniera automatica, rileva e legge le informazioni contenute nel Tag RFId permettendo di effettuare una accurata rilevazione e garantendo al singolo cittadino di pagare l’effettiva produzione di rifiuti.</span></p><p><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Case History:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Progettazione e realizzazione di Tag RFId attivi per la società responsabile della raccolta dei rifiuti di una città del nord Italia.&nbsp;</span><br /></li></ul><p><br /><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;"><strong>5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per la gestione dei rifiuti:</strong></span><br /></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Addebito del servizio in funzione della reale fruizione</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">registrazione automatica dei dati e informazione del cassonetto</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione interventi manuali</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">identificazione del cassonetto in ogni situazione</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione tempi di lavorazione</span></li></ul>	2013-07-30 12:56:40.529711	2013-07-30 12:56:40.529711
193	193	it	Shopping-experience-con-Tag-passivi	Shopping experience con Tag passivi	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas propone etichette o cartellini RFId a Tag passivi UHF e HF che permettono una nuova esperienza dello shopping.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">RFId dressing room:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">la tecnologia RFId a tag passivi UHF e HF crea una nuova shopping experience con un camerino intelligente e tecnologico che informa il cliente sulla disponibilità di taglie e colori, delle promozioni e suggerimenti sui possibili abbinamenti tutto questo in tempo reale creando attorno al cliente una nuova esperienza dello shopping.</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Il sistema utilizza lettori o varchi RFID UHF ed etichette autoadesive o tag RFId UHF inseriti all'interno delle etichette o dei cartellini di ogni singolo capo e uno schermo all'interno del camerino.</span></li></ul><p><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Gestione punto vendita:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">la tecnologia RFId a tag passivi UHF e HF riduce il tempo per la gestione ordinaria del punto vendita, il personale e le azioni di cassa;&nbsp;</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Il sistema è efficace anche come antitaccheggio e verifica dell'autenticità dei prodotti.</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Può essere realizzato un sistema RFId a tag passivi UHF e HF completo che comprenda vantaggi logistici, produttivi, di gestione singoli dei punti vendita (inventari, riordini, ecc.), per azioni di marketing e scelte strategiche e di miglioramento del servizio.</span></li></ul>	2013-07-30 12:58:44.050299	2013-07-30 12:58:44.050299
179	179	it	Controllo-accessi-persone	Controllo accessi persone	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;"><strong>Descrizione:</strong></span></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Sistema di riconoscimento automatico composto da un Tag RFId UHF o HF inserito nel badge o ticket che grazie ad un apposito reader RFId posizionato nel desk permette l'identificazione immediata della persona ad una distanza inferiore al metro o al passaggio attraverso il varco</span></li></ul><p><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#7f7f7f;"><strong>Vantaggi:</strong></span><br /></p><ul><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">se</span><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">nza contatto visivo</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">utilizzo a mani libere</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">restrizione accessi ad aree riservate</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">integrazione del TAG in svariati oggetti come braccialetti o badge</span></li><li><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">riconoscimento a distanza</span></li></ul>	2013-07-30 10:30:57.970576	2013-07-30 10:31:12.785034
180	180	it	Anti---contraffazione	Anti - contraffazione	\N	<p><span style="font-size:medium;">Rotas produce e distribuisce nel mondo soluzioni RFId per l'Anticontraffazione e antimanomissione per garantire l'originalità del prodotto.</span><br /><br /><span style="font-size:medium;">Negli anni abbiamo costruito un sistema lavoro che ci permette di adattare ogni componente: inlay, Tag, adesivo, e la superficie in ogni forma e colore; tutto ciò ci permette di ottenere sempre la soluzione ai vostri bisogni, soddisfando ogni vostra singola esigenza.</span><br /><br /><strong><span style="font-size:medium;color:#7f7f7f;">Soluzione:</span></strong><br /><span style="font-size:medium;">Etichette adesive antimanomissione con diverse tecnologie incorporabili: fustellatura speciale, Wood, Void, anti-manomissione e molte altre, da attaccare al capo d'abbigliamento, disponibili anche senza tecnologia RFId, integrabili con altri strumenti di sicurezza antitaccheggio o antimanomissione, rendendo il Tag RFid passivo e l'antitaccheggio invisibile.</span></p><ul><li><span style="font-size:medium;line-height:1.5;">Rotas collabora con il TechALab&nbsp;</span><br /></li><li><span style="font-size:medium;line-height:1.5;">Rotas ha partecipato alla 1^ Edizione della "Giornata Nazionale Anticontraffazione"&nbsp;</span></li></ul><p><strong><span style="font-size:medium;color:#7f7f7f;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId nel campo dell'anticontraffazione:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;">maggiore sicurezza per il consumatore della provenienza del prodotto</span><br /></li><li><span style="font-size:medium;line-height:1.5;">localizzazione immediata del prodotto</span></li><li><span style="font-size:medium;line-height:1.5;">associazione univoca tra il prodotto e il TAG</span></li><li><span style="font-size:medium;line-height:1.5;">riduzione dei falsi</span></li><li><span style="font-size:medium;line-height:1.5;">aumento di sicurezza delle informazioni del prodotto</span><br /></li></ul>	2013-07-30 12:24:46.46998	2013-07-30 12:24:46.46998
181	181	it	Applicazioni-speciali-con-tag-passivi	Applicazioni speciali con tag passivi	\N	<p><span style="font-size:medium;">Rotas, grazie alla lunga esperienza, riesce ad arrivare sempre alla migliore soluzione RFId anche per Applicazioni speciali o estreme.</span><br /><span style="font-size:medium;">Conoscenza delle tecniche e metodo di lavoro ci consentono una estrema personalizzazione creando prodotti disponibili in svariati modelli, forme e colori.</span><br /><span style="font-size:medium;">Per questo è in grado di creare soluzioni &nbsp;adattando al cliente ogni elemento della tecnologia.</span><br /><br /><span style="font-size:medium;">Questi sono solamente degli esempi ma dimostrano come siamo in grado di adattare la tecnologia RFId a tag passivi ad ogni applicazione e richiesta del cliente, arrivando sempre alla soluzione dei vostri problemi.</span><br /><br /><br /><span style="font-size:medium;color:#7f7f7f;"><strong>Case History:</strong></span></p><ul><li><span style="font-size:medium;line-height:1.5;">Ideazione,creazione e produzione di Tag RFid passivi per una società italiana di trasporto di gas migliorando la gestione della manutenzione degli impianti.</span></li></ul>	2013-07-30 12:26:49.425057	2013-07-30 12:26:49.425057
182	182	it	Asset-Tracking	Asset Tracking	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">Rotas produce e distribuisce nel mondo soluzioni RFId per l'Asset Tracking con Tag passivi.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">Rotas da sempre ascolta le esigenze ed i bisogni dei partner.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">Tutto questo ci permette di arrivare sempre alla migliore soluzione dei vostri problemi.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">Poste Italiane utilizza le nostre etichette RFId per l'Asset Tracking.</span><br /><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">La tecnologia RFId applicata all'Asset Tracking consente di identificare e mettere sotto controllo i beni aziendali (ad esempio tutte le apparecchiature informatiche) di valore all'interno di una certa area oppure di tracciarli lungo un determinato processo. In questo modo si può conoscere esattamente il numero dei cespiti, la posizione e controllare la loro storia.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">In questo contesto la tecnologia RFId offre molti vantaggi rispetto alle altre tecnologie di identificazione in quanto garantisce la lettura anche in condizioni difficili, in assenza di visibilità o in ambienti sporchi o con scarsa illuminazione. Quando graffi ed usura del tempo impediscono la lettura completa delle scritte sulla superficie, il chip inserito all'interno dell'etichetta RFId continua a mantenere la capacità di comunicare.&nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#000000;">Il Tag passivo RFId può essere anche scritto e quindi anche contenere una serie di informazioni (anche) fruibili direttamente per il tramite di dispositivi di comunicazione in radiofrequenza come, ad esempio, un lettore palmare (anche) senza necessità di connessione al database remoto.&nbsp;</span><br /><br /><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#7f7f7f;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId a tag passivi per l'asset tracking:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">Identificazione automatica degli asset</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">Inventario</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">Verifica ubicazione, configurazione e condizioni</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">Informazioni in tempo reale</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;color:#000000;">Tracciabilità al 100%</span></li></ul>	2013-07-30 12:28:52.48726	2013-07-30 12:28:52.48726
183	183	it	Biglietti-da-visita-NFC-ed-etichette-NFC	Biglietti da visita NFC ed etichette NFC	\N	<span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Rotas produce e distribuisce biglietti da visita NFC&nbsp;</span><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">ed etichette NFC personalizzabili</span><br /><br /><strong><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;">Cos’è l’NFC ?</span></strong><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">NFC, ovvero Near Field Communication: l’RFID (Radio Frequency IDentification) per applicazioni a distanza ravvicinata.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">E’ una tecnologia in rapida diffusione dalle aree di applicazione tanto varie e vaste da avere come unico limite la fantasia di chi le progetta.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Biglietti da visita, etichette,ticket del metrò, magliette, poster (…e l’elenco potrebbe allungarsi a dismisura) sono solo alcuni dei prodotti che possono integrare al loro interno, in maniera del tutto invisibile, questa tecnologia.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Gli smartphone moderni rappresentano il “biglietto” (e mai termine è stato più appropriato) per la sua definitiva consacrazione all’utilizzo nel mercato di massa.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Marchi noti come Samsung™, HTC™, Nokia™ sempre più spesso inseriscono questa funzionalità all’interno dei loro telefonini di ultima generazione. Google fa la sua parte col sistema operativo Android, ormai onnipresente e perfettamente integrato ad NFC tanto da permetterne un utilizzo quotidiano alla portata di chiunque.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Un biglietto da visita che una volta letto fa apparire il sito aziendale sul proprio telefono o un poster che rimanda al trailer del film in programmazione e ci fa condividere la data di uscita sui social network.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Se nell’arco di qualche mese ci ritroveremo a pagare la spesa appoggiando il telefono sulla cassa del supermercato non dobbiamo stupirci, sarà merito dell’NFC.</span>	2013-07-30 12:31:57.594682	2013-07-30 12:31:57.594682
184	184	it	Controllo-accessi-veicoli	Controllo accessi veicoli	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Rotas produce e distribuisce nel mondo soluzioni RFId per il controllo accessi di veicoli con Tag Attivi o Passivi.&nbsp;</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Questi, nelle immagini, sono solo esempi, usati in molte situazioni in Italia e all'estero con ottimi risultati in termini di efficienza ed efficacia.</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">La nostra esperienza decennale ci permette di soddisfare ogni vostra singola esigenza adattando ogni componente e arrivando sempre alla migliore soluzione, realizzando prodotti ad hoc.&nbsp;</span><br /><br /><strong><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;">Soluzione:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Tag RFId attivi UHF e HF da attaccare al parabrezza dell'auto che permettono, da parte del varco / cancello RFId, il riconoscimento automatico del veicolo e l'apertura del cancello / varco stesso da una distanza predefinibile.</span></li></ul><p><br /><br /><br /><strong><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;color:#7f7f7f;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per il vostro problema di controllo accessi veicoli:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">identificazione automatica del veicolo</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">apertura automatica della barriera/ cancello</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">registrazione dei passaggi</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">totalmente a mani libere</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">controllo in tempo reale della disponibilità di parcheggio</span><br /></li></ul>	2013-07-30 12:34:42.385908	2013-07-30 12:34:42.385908
185	185	it	Controllo-accessi-persone	Controllo accessi persone	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId per il controllo accessi con Tag RFId attivi e passivi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Negli anni abbiamo soddisfatto numerose esigenze per il controllo accessi di mostre, congressi, eventi, fiere, etc. in tutto il mondo.&nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Grazie alla nostra esperienza e alla continua ricerca e sviluppo riusciamo a soddisfare ogni vostro singolo bisogno adattando ogni singolo componente: il materiale, consistenza, inlay, forma e tipo incorporando per esempio il Tag RFId in Badge o braccialetti rendendolo invisibile, riuscendo ad implementare in un unico prodotto diverse tecnologie.</span><br /><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Soluzione:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Sistema di riconoscimento automatico composto da un Tag RFId UHF o HF inserito nel badge o ticket che grazie ad un apposito reader RFId posizionato nel desk permette l'identificazione immediata della persona ad una distanza inferiore al metro o al passaggio attraverso il varco</span><br /><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Funzionamento:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La lettura del tag RFID attivo o passivo e le conseguenti azioni di abilitazione/non abilitazione e registrazione dell’accesso avvengono al passaggio della persona attraverso un varco o per avvicinamento a un lettore. Il tag passivo RFId può essere integrato in una tessera, in un portachiavi, in un braccialetto o in qualsiasi altro oggetto tascabile. Attraverso un software si registrano tutte le informazioni a cui si può accedere in tempo reale. L’applicazione può essere abbinata ai normali sistemi di apertura porta o varchi. Gli accessi possono essere personalizzati e vincolati a fasce orarie, periodi o aree.</span><br /><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Case History:&nbsp;</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">50° della Scuola Navale Militare "F. Morosini"</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">G8 Energy Ministers Meeting 2009</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">I.A.B.S.E.&nbsp;</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">A.I.S.V. Video</span></li></ul><p><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per il vostro problema di controllo accessi persone:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">senza contatto visivo</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">utilizzo a mani libere</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">restrizione accessi ad aree riservate</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">integrazione del TAG in svariati oggetti come braccialetti o badge</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riconoscimento a distanza</span><br /></li></ul>	2013-07-30 12:41:14.56439	2013-07-30 12:41:14.56439
186	186	it	Etichette-RFId-autoadesive	Etichette RFId autoadesive	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo etichette RFId UHF e HF autoadesive. &nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">In Rotas, siamo addestrati ad ascoltare le esigenze ed i bisogni dei nostri clienti.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Tutto questo ci permette di arrivare sempre alla migliore soluzione dei vostri problemi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Da più di quarant’anni il laboratorio di ricerca di Rotas seleziona Materiali, Superfici, Adesivi, modalità di Stampa e Liner e li combina assieme per produrre l’etichetta adesiva più adatta all’applicazione finale.&nbsp;</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La tecnologia RFID a tag passivi UHF e HF è molto versatile: può trovare applicazione in ogni ambito in cui si parli di tracciabilità, anticontraffazione o identificazione, permette un notevole risparmio di tempo nelle operazioni di verifica degli oggetti in transito.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Il tutto con la possibilità di rendere il tag RFId invisibile e compatibile con altre tecnologie</span><br /><br /><strong><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;color:#7f7f7f;">5 buoni motivi per scegliere le etichette RFId autoadesive Rotas:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">estremamente personalizzabili</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">trovano applicazioni in moltissimi settori</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">incorporano chip RFId ad alte prestazioni</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">sovrastampabili con codice a barre, logo o dati</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riducono notevolmente il tempo di lettura degli oggetti in transito</span><br /></li></ul>	2013-07-30 12:42:42.24698	2013-07-30 12:42:42.24698
187	187	it	Gioielli-e-monili-con-Tag-RFId-passivi	Gioielli e monili con Tag RFId passivi	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId a Tag RFId passivi per gioielli, monili e oggetti preziosi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas, con un'esperienza decennale nel RFId, adattando materiale, inlay e tecnologia riesce sempre ad arrivare alla migliore soluzione per il vostro progetto RFId.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La tecnologia RFId può essere di ausilio per la movimentazione, a partire dalle fasi di produzione fino ad arrivare al momento della vendita, e l’inventariazione dei gioielli, per un aggiornamento rapido degli inventari.&nbsp;</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Soluzione:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Etichetta ideata per rispondere a diverse esigenze, non ultima la semplicità di applicazione ai gioielli e oggetti preziosi: un particolare accorgimento tecnico ne permette la facile applicazione anche agli orecchini.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Etichette RFId a tag passivi UHF e tag passivi HF, bindelli RFId e cartellini RFId UHF e HF, in diversi materiali, stampabili a TT con stampante RFId , richiudibili dopo la stampa e con la possibilità di imprimere un logo stampato in alta qualità.&nbsp;</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Trovano applicazione quando é necessario l'utilizzo di un tag RFID passivo invisibile da appendere all’oggetto che si vuole poi identificare per mezzo di lettori RFID.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">5 buoni motivi per scegliere Rotas ed i suoi prodotti RFId:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">gestione rapida degli stock e degli inventari</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">gestione dei punti vendita</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">eliminazione degli errori di inventario</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione tempo e personale</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">tracciabilità delle merci</span></li></ul>	2013-07-30 12:44:14.045799	2013-07-30 12:44:14.045799
188	188	it	Logistica-con-Tag-passivi-UHF	Logistica con Tag passivi UHF	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId per la logistica con Tag RFId passivi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Negli anni abbiamo soddisfatto le esigenze di miglioramento della logistica in entrata ed uscita di numerose aziende nel mondo riducendo i costi e le operazioni manuali degli operatori, realizzando prodotti ad hoc per soddisfare le singole esigenze.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Supply Chain management:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">I Tag RFId passivi UHF e HF sono ideali per facilitare e migliorare la gestione della catena logistica in tutti i campi di applicazioni.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Attraverso un software tutte le informazioni relative agli oggetti e alle operazioni svolte su di essi sono disponibili sempre e in tempo reale</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Funzionamento:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Si identifica dal componente al pezzo finito con un Tag RFId passivo introducendolo in un database e relazionandolo alla sua specifica provenienza se necessario. Quando si passa da una fase di lavorazione ad un’altra si realizza una lettura (lettore manuale o automatico attraverso un tunnel o varco RFId) potendo visualizzare in ogni momento attraverso un software tutte le informazioni relative alla singola parte e ad ogni fase.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Gestione parco pallet:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Le etichette RFId UHF e HF possono inoltre essere applicate direttamente su ogni singolo pallet. Le sue caratteristiche la rendono un ideale ausilio del parco pallet, nonchè per prevenire la perdita e il furto dei pallet stessi e consentirne la rintracciabilità, monitorandone il passaggio attraverso appositi varchi o tunnel RFId.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Esempio su capi d’abbigliamento:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Entrata: 110 scatole, 10.000 capi Lettura Manuale: 6 ore Lettura RFID: 36 min.</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Uscita: 520 scatole, 10.000 capi Lettura Manuale: 6 ore Lettura RFID: 1,5 ore</span></li></ul><p><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per la logistica:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">tracciabilità delle merci al 100%</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione tempi carico/scarico</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">eliminazione errori nelle consegne</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">miglioramento gestione punti di distribuzione</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione tempo e personale</span></li></ul>	2013-07-30 12:47:31.334652	2013-07-30 12:47:31.334652
189	189	it	Tag-RFId-passivi-per-applicazioni-speciali-in-acciaieria	Tag RFId passivi per applicazioni speciali in acciaieria	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas Italia progetta e produce etichette adesive RFId e tag RFId UHF e HF per ambienti difficili che richiedono applicazioni speciali o estreme.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas propone:</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Etichette autoadesive con adesivo speciale: in grado di aderire in modo tenace su superfici irregolari e materiali metallici.</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Tag rfid passivi UHF e HF in grado di lavorare anche a temperature elevate</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Le forme, i materiali, la superficie e le stampe del Tag o dell’etichetta sono scelti per creare un’etichetta su misura che verrà prodotta con procedimenti industriali</span><br /></li></ul><p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Nelle acciaierie, la tecnologia RFId di Rotas viene utilizzata per l'identificazione e la tracciabilità delle staffe di formatura dell'acciaio all’interno degli stabilimenti.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">A questo fine possono essere utilizzati sia tag RFid UHF che tag RFid HF.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">I Tag sono progettati e testati da Rotas per resistere in condizioni estreme.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas Italia sceglie materiali, adesivi e chip RFId e li combina tra loro per ottenere etichette RFid o Tag RFid resistenti ad urti, vibrazioni come ad alte e basse temperature.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Nel caso del processo di colata dell'acciaio il Tag deve rimanere tenacemente attaccato anche in presenza di alte temperature e di vibrazioni, deve proteggere il chip dagli urti e deve mantenere nel tempo un’efficiente comunicazione in radio frequenza.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">La lettura dell’etichetta RFid o del tag RFId HF o UHF avviene mediante un lettore (Reader RFId).</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Il lettore deve essere facile da utilizzare ed essere progettato per l'utilizzo in ambienti industriali critici e difficili: deve quindi essere in grado di operare in ambienti polverosi e di resistere agli urti.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">E’ possibile anche integrare appositi lettori RFid in tunnel o porte in modo da rilevare i dati contenuti nei chip al momento del transito.</span><br /></p>	2013-07-30 12:50:34.158072	2013-07-30 12:50:34.158072
194	194	it	Sport-Timing-con-Tag-passivi-UHF	Sport Timing con Tag passivi UHF	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId Sport Timing per supporto al cronometraggio.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Negli anni abbiamo soddisfatto le esigenze di numerose associazioni sportive nel mondo come ausilio al cronometraggio di gare sportive, corse e gare podistiche realizzando prodotti ad hoc soddisfando singole esigenze, adattando il materiale, consistenza, inlay, forma e tipo.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Questo è solo un esempio, usato in molti paesi del mondo: Francia, Italia, Filippine, Portogallo e molti altri.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Soluzione:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Etichetta RFId passiva UHF usa e getta da fissare ai lacci della scarpa del peso di pochi grammi</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Non incide minimamente sulle prestazioni dell'atleta e si attiva al passaggio sopra di una pedana.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Con l’ausilio della tecnologia RFID a tag passivi la gestione delle competizioni sportive risulta notevolmente semplificata, più veloce e meno costosa.&nbsp;</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Funzionamento:</span></strong><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">E' semplice. Gli atleti e/o i sport rfid loro mezzi sono dotati di Tag passivi UHF.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Lettori o pedane RFId posizionate nei punti di controllo leggono in tempo reale le informazioni contenute nel Tag passivo e attraverso un software le registrano rendendole disponibili in tempo reale.Le letture vengono realizzate con modalità specifiche in funzione del tipo di competizione.</span><br /><br /><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;"><strong>5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per risolvere il vostro problema di sport timing:</strong></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">lettura automatica</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">possibilità di collegamento con altri sistemi (es:invio sms ad amici con tempi)</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">utilizzo Tag RFId UHF usa e getta e costo inferiore ad 1 €</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">letture intermedie precise</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">lettura multipla di atleti</span><br /></li></ul>	2013-07-30 13:07:46.104925	2013-07-30 13:07:46.104925
195	195	it	Tessile-e-Fashion-con-Tag-passivi	Tessile e Fashion con Tag passivi	\N	<p><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Rotas produce e distribuisce nel mondo soluzioni RFId per il Mondo del Tessile e il Mondo del Fashion.&nbsp;</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Realizziamo bindelli RFID, etichette RFID di sicurezza e cartellini pendenti RFID, Hang Tag RFId passivi UHf e HF per la realizzazione di soluzioni RFID di successo.&nbsp;</span><br /><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">Grazie &nbsp;alla nostra decennale esperienza, realizziamo tag RFID adattando ogni componente, materiale e tecnologia, arrivando sempre alla migliore soluzione per il vostro progetto RFID .&nbsp;</span><br /><br /><span style="color:#7f7f7f;font-family:Verdana, Geneva, sans-serif;font-size:medium;"><strong>Soluzione:</strong></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Bindelli RFID, pendenti RFID, cartellini RFId o hang tag rfid in diversi materiali, stampabili a TT con stampante &nbsp;RFId , richiudibili dopo la stampa.&nbsp;</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Ideati inizialmente per il settore dell'abbigliamento, trovano applicazione quando é necessario l'utilizzo di un tag RFID da appendere all’oggetto che si vuole poi identificare per mezzo di lettori RFId, aumentando la sicurezza del prodotto.</span><br /></li></ul><p><br /><span style="font-family:Verdana, Geneva, sans-serif;font-size:medium;">E non solo:</span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">etichette di composizione RFID, sulle quali sono stampate informazioni sul prodotto, ma che inglobano anche un tag RFID. Cucibili sul capo d'abbigliamento per scopi logistici, per scopi di anticontraffazione e a garanzia dell’originalità per il consumatore finale.</span></li></ul><p><strong><span style="color:#7f7f7f;font-family:Verdana, Geneva, sans-serif;font-size:medium;">Case History:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Una grande azienda utilizza le etichette RFID ROTAS per garantire all'industria Francese dell'abbigliamento un modo veloce per essere aggiornati sulle tendenze del settore, dalla creazione del capo alla sua distribuzione in negozio</span></li></ul><p><br /><br /><strong><span style="color:#7f7f7f;font-family:Verdana, Geneva, sans-serif;font-size:medium;">Esempio su capi d’abbigliamento:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Entrata: 110 scatole, 10.000 capi Lettura Manuale: 6 ore Lettura RFID: 36 min.&nbsp;</span><br /></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">Uscita: 520 scatole, 10.000 capi Lettura Manuale: 6 ore Lettura RFID: 1,5 ore</span></li></ul><p><strong><span style="color:#7f7f7f;font-family:Verdana, Geneva, sans-serif;font-size:medium;">5 buoni motivi per scegliere Rotas ed i suoi prodotti RFId nel campo tessile:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">tracciabilità della merce lungo tutta la filiera</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione di costi per il controllo dei materiali</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">maggiore sicurezza per il consumatore</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">aggiornamento informazioni contenute nei TAG</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">maggiore condivisione delle informazioni lungo la filiera</span><br /></li></ul>	2013-07-30 13:10:22.7311	2013-07-30 13:10:22.7311
196	196	it	Ticketing-con-Tag-passivi	Ticketing con Tag passivi	\N	<p><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas produce e distribuisce nel mondo soluzioni RFId per il Ticketing con Tag passivi.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Questo è solo un esempio ma realizziamo Tag Rfid passivi usa e getta personalizzando ogni componente, materiale, inlay, forma e tipo.</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Le nostre soluzioni per il ticketing sono state scelte da eventi, mostre, congressi, conferenze di tutto il mondo ma possono essere utilizzate, ad esempio, anche per il trasporto pubblico (cosa che avviene già in diverse città).</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Le nostre soluzioni hanno sempre ottenuti ottimi risultati in termini di efficacia ed efficienza grazie all'esperienza decennale che ci permette di soddisfare ogni singola esigenza del cliente.</span><br /><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Rotas adattanda ogni componente e materiale, arrivando sempre alla migliore soluzione dei vostri &nbsp;problemi.&nbsp;</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">Case history:</span></strong></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">IABSE</span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">A.I.S.V. Video&nbsp;</span><br /></li></ul><p><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Funzionamento</span><br /><span style="font-size:medium;font-family:Verdana, Geneva, sans-serif;">Il sistema consente il riconoscimento automatico delle persone al loro passaggio attraverso il varco, registrandone le operazioni di ingresso/uscita.In tempo reale, oppure in modalità offline, si conosceranno il numero e le identità dei partecipanti ad un evento, si potranno tracciare transiti e presenze all’interno dell’area d’interesse e si potranno gestire i dati degli ospiti (generazione e gestione database,reporting), in locale e su server centrale, attraverso un software dedicato. Il badge RFId passivo identificativo, tessera normalmente utilizzata in ogni forma di attività congressuale od evento, è dotato di un chip che permette il riconoscimento univoco dell’ospite che lo riceve e viene inizializzato e distribuito prima dell’evento o all’ingresso, durante la fase di accoglienza.</span><br /><br /><strong><span style="color:#7f7f7f;font-size:medium;font-family:Verdana, Geneva, sans-serif;">5 buoni motivi per scegliere Rotas e i suoi prodotti RFId per il ticketing:</span></strong><br /><span style="font-size:0.75em;line-height:1.5;"></span></p><ul><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">sistema hands free</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione smaltimento tessere esaurite</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">riduzione tempi controllo</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">sicurezza dei dati personali</span><br /><span style="font-size:0.75em;line-height:1.5;"></span></li><li><span style="font-size:medium;line-height:1.5;font-family:Verdana, Geneva, sans-serif;">personalizzazione delle tessere</span><br /></li></ul>	2013-07-30 13:11:48.122635	2013-07-30 13:11:48.122635
\.


--
-- Data for Name: mods; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mods (id, idserv, fkcat, home, f_del, ordine, created_at, updated_at, published, data_evento, etc, settori, applicazioni) FROM stdin;
1	2	1	\N	0	\N	2012-11-08 10:12:43.957083	2012-11-08 10:12:43.957083	\N	\N	slogan_home	\N	\N
2	3	2	\N	1	\N	2012-11-08 10:26:01.764873	2012-11-08 10:40:30.898614	\N	\N	\N	\N	\N
11	4	10	\N	0	\N	2012-11-22 15:26:13.942511	2012-11-22 15:26:29.601736	1	\N	\N	\N	\N
12	4	10	\N	0	\N	2012-11-22 15:38:11.836707	2012-11-22 15:38:35.587019	1	\N	\N	\N	\N
3	3	2	\N	1	1	2012-11-08 10:49:54.68346	2012-12-13 15:51:37.097381	1	\N	\N	\N	\N
4	3	2	\N	1	2	2012-11-08 10:57:28.518635	2012-12-13 15:51:37.101385	1	\N	\N	\N	\N
6	3	2	\N	1	3	2012-11-08 11:00:40.765318	2012-12-13 15:51:37.104884	1	\N	\N	\N	\N
9	3	2	\N	1	4	2012-11-08 17:04:10.559746	2012-12-13 15:51:37.108358	\N	\N	\N	\N	\N
8	3	2	\N	1	5	2012-11-08 15:33:04.167152	2012-12-13 15:51:37.112293	\N	\N	\N	\N	\N
5	3	2	\N	1	6	2012-11-08 10:58:55.251288	2012-12-13 15:51:37.116077	\N	\N	\N	\N	\N
7	3	2	\N	1	7	2012-11-08 11:02:02.488112	2012-12-13 15:51:37.119724	\N	\N	\N	\N	\N
10	3	5	\N	1	\N	2012-11-12 16:54:42.72813	2012-12-13 15:51:49.326578	1	\N	\N	\N	\N
20	3	7	\N	0	\N	2012-12-13 16:06:08.472256	2012-12-13 16:06:29.257558	1	\N	\N	\N	\N
21	3	8	\N	0	\N	2012-12-13 16:06:45.228853	2012-12-13 16:07:00.695993	1	\N	\N	\N	\N
22	3	8	\N	0	\N	2012-12-13 16:07:10.601111	2012-12-13 16:07:23.339472	1	\N	\N	\N	\N
35	7	13	\N	0	\N	2013-03-14 10:47:32.594624	2013-03-14 10:53:11.926754	1	\N	\N	\N	\N
36	7	13	\N	0	\N	2013-03-14 10:53:55.514167	2013-03-14 10:54:07.020179	1	\N	\N	\N	\N
24	5	11	\N	1	2	2013-03-11 14:09:05.981496	2013-07-23 06:55:33.790027	1	\N	\N	\N	\N
17	3	5	\N	0	1	2012-12-13 16:04:42.085221	2013-07-22 07:14:07.303469	0	\N	\N	\N	\N
38	5	11	\N	1	6	2013-03-26 10:20:42.107172	2013-07-23 06:55:33.856552	1	\N	\N	\N	\N
64	3	5	\N	0	6	2013-07-22 07:40:28.205311	2013-07-23 13:49:12.008502	1	\N	\N	109#108#107#105#104#103#102#101	115#113#111
72	3	5	\N	0	14	2013-07-22 08:13:29.232364	2013-07-23 13:53:41.46822	1	\N	\N	110	115#114
73	3	5	\N	0	15	2013-07-22 08:16:17.779431	2013-07-23 13:54:09.084543	1	\N	\N	110#109#105#104#103#102#101	116#115#113
82	3	5	\N	0	28	2013-07-22 09:12:03.365712	2013-07-23 14:01:51.879347	1	\N	\N	110#105#103	115#111
65	3	5	\N	0	7	2013-07-22 07:45:20.806376	2013-07-23 13:49:52.792967	1	\N	\N	108#107#105#103	111
66	3	5	\N	0	8	2013-07-22 07:47:40.473359	2013-07-23 13:51:18.20196	1	\N	\N	107#105#104#103	111
67	3	5	\N	0	9	2013-07-22 08:04:46.797009	2013-07-23 13:51:42.972967	1	\N	\N	110	115
68	3	5	\N	0	10	2013-07-22 08:06:14.482817	2013-07-23 13:52:02.904024	1	\N	\N	108#105#104#103	115
69	3	5	\N	0	11	2013-07-22 08:08:12.522065	2013-07-23 13:52:27.579519	1	\N	\N	108#105#104#103	115#114
71	3	5	\N	0	13	2013-07-22 08:11:51.482886	2013-07-23 13:53:24.874203	1	\N	\N	108#105#104#103	115#112#111
75	3	5	\N	0	17	2013-07-22 08:39:47.013778	2013-07-23 13:55:41.195955	1	\N	\N	110#108#107#106#105#104#103#102	116#115#113
84	3	5	\N	0	20	2013-07-22 09:14:24.780048	2013-07-23 13:57:00.934281	1	\N	\N	110#109#106#103	116#115
85	3	5	\N	0	21	2013-07-22 09:15:38.279574	2013-07-23 13:57:26.721773	1	\N	\N	110	115#114#111
77	3	5	\N	0	19	2013-07-22 08:42:39.491229	2013-07-23 13:56:25.655597	1	\N	\N	109#108#106#103#102	116
86	3	5	\N	0	22	2013-07-22 09:16:53.636832	2013-07-23 13:57:42.777528	1	\N	\N	109#103#102	116
79	3	5	\N	0	24	2013-07-22 08:47:25.150282	2013-07-23 13:58:49.408647	1	\N	\N	110#109#108#106#103#102#101	116
30	6	12	\N	1	\N	2013-03-11 14:10:28.523377	2013-07-23 08:11:38.271551	1	\N	\N	\N	\N
78	3	5	\N	0	26	2013-07-22 08:45:33.833344	2013-07-23 14:00:02.339066	1	\N	\N	110#109#108#106#103#102	116
83	3	5	\N	0	29	2013-07-22 09:13:16.250729	2013-07-23 14:02:25.849396	1	\N	\N	109#106#105#104#103#102	116#113
81	3	5	\N	0	27	2013-07-22 09:10:53.973188	2013-07-23 14:01:12.824908	1	\N	\N	109#108#107#105#104#103	115#114
89	3	5	\N	0	31	2013-07-22 09:32:14.616166	2013-07-23 14:03:21.753977	1	\N	\N	109#108#106#104#102#101	116
90	3	5	\N	0	32	2013-07-22 09:33:30.214955	2013-07-23 14:04:05.000511	1	\N	\N	109#103	116#115#114
91	3	5	\N	0	33	2013-07-22 09:34:51.968465	2013-07-23 14:04:19.34501	1	\N	\N	108#106#104#102	116#115#114
92	3	5	\N	0	34	2013-07-22 09:36:06.460281	2013-07-23 14:04:55.698003	1	\N	\N	108#106#105#104#103#102	115
94	3	5	\N	0	36	2013-07-22 09:38:51.484419	2013-07-23 14:05:54.774192	1	\N	\N	110#108	116#115
13	3	2	\N	1	1	2012-12-13 16:02:29.970917	2013-07-26 16:28:13.935941	1	\N	\N	23#24#26	31#30
14	3	2	\N	1	2	2012-12-13 16:03:10.175649	2013-07-26 16:28:13.965324	1	\N	\N	23#24	31#30
31	6	12	\N	1	\N	2013-03-11 14:10:36.882883	2013-07-23 08:11:38.292697	1	\N	\N	\N	\N
32	6	12	\N	1	\N	2013-03-11 14:10:45.138343	2013-07-23 08:11:38.30935	1	\N	\N	\N	\N
33	6	12	\N	1	\N	2013-03-11 14:10:49.831121	2013-07-23 08:11:38.326015	1	\N	\N	\N	\N
34	6	12	\N	1	\N	2013-03-11 14:10:58.507916	2013-07-23 08:11:38.34268	1	\N	\N	\N	\N
55	6	12	\N	1	\N	2013-03-26 10:24:04.864244	2013-07-23 08:11:38.359361	1	\N	\N	\N	\N
16	3	5	\N	0	2	2012-12-13 16:04:20.703059	2013-07-22 10:00:24.627555	0	\N	\N	\N	\N
45	6	12	\N	1	\N	2013-03-26 10:22:18.044142	2013-07-23 08:11:38.375999	1	\N	\N	\N	\N
27	5	11	\N	1	7	2013-03-11 14:09:38.205682	2013-07-23 06:55:33.873231	1	\N	\N	\N	\N
42	5	11	\N	1	9	2013-03-26 10:21:25.76584	2013-07-23 06:55:33.89907	1	\N	\N	\N	\N
41	5	11	\N	1	10	2013-03-26 10:21:18.01072	2013-07-23 06:55:33.91496	1	\N	\N	\N	\N
15	3	2	\N	1	3	2012-12-13 16:03:51.947503	2013-07-26 16:28:13.998588	1	\N	\N	23#24	34
39	5	11	\N	1	12	2013-03-26 10:20:56.83225	2013-07-23 06:55:20.632909	1	\N	\N	\N	\N
28	5	11	\N	1	13	2013-03-11 14:09:51.924073	2013-07-23 06:55:20.64957	1	\N	\N	\N	\N
23	5	11	\N	1	1	2013-03-11 14:08:49.664351	2013-07-23 06:55:33.75671	1	\N	\N	\N	\N
43	5	11	\N	1	11	2013-03-26 10:21:34.38784	2013-07-23 06:55:20.606762	1	\N	\N	\N	\N
25	5	11	\N	1	3	2013-03-11 14:09:18.263761	2013-07-23 06:55:33.806552	1	\N	\N	\N	\N
29	5	11	\N	1	5	2013-03-11 14:10:03.382042	2013-07-23 06:55:33.839876	1	\N	\N	\N	\N
47	6	12	\N	1	\N	2013-03-26 10:22:47.274241	2013-07-23 08:11:38.392668	1	\N	\N	\N	\N
49	6	12	\N	1	\N	2013-03-26 10:23:08.376326	2013-07-23 08:11:38.409338	1	\N	\N	\N	\N
51	6	12	\N	1	\N	2013-03-26 10:23:26.361855	2013-07-23 08:11:38.425999	1	\N	\N	\N	\N
52	6	12	\N	1	\N	2013-03-26 10:23:36.701469	2013-07-23 08:11:38.44267	1	\N	\N	\N	\N
44	6	12	\N	1	\N	2013-03-26 10:22:09.797278	2013-07-23 08:11:38.459348	1	\N	\N	\N	\N
46	6	12	\N	1	\N	2013-03-26 10:22:38.889332	2013-07-23 08:11:38.475999	1	\N	\N	\N	\N
48	6	12	\N	1	\N	2013-03-26 10:22:58.001792	2013-07-23 08:11:38.492658	1	\N	\N	\N	\N
50	6	12	\N	1	\N	2013-03-26 10:23:18.773007	2013-07-23 08:11:38.50931	1	\N	\N	\N	\N
53	6	12	\N	1	\N	2013-03-26 10:23:44.115722	2013-07-23 08:11:38.526011	1	\N	\N	\N	\N
54	6	12	\N	1	\N	2013-03-26 10:23:51.193874	2013-07-23 08:11:38.542657	1	\N	\N	\N	\N
61	3	5	\N	0	3	2013-07-22 07:13:48.607036	2013-07-23 13:44:06.685159	1	\N	\N	109#108#107#106#105#104#103#102#101	116#112
18	3	6	\N	1	\N	2012-12-13 16:05:08.139933	2013-07-30 08:23:06.685135	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
19	3	6	\N	1	\N	2012-12-13 16:05:37.716083	2013-07-30 08:23:06.716235	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
37	3	6	\N	1	\N	2013-03-26 09:30:32.682323	2013-07-30 08:23:06.73282	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
56	3	6	\N	1	\N	2013-03-26 10:35:38.539259	2013-07-30 08:23:06.74947	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
57	3	6	\N	1	\N	2013-03-26 10:36:12.845089	2013-07-30 08:23:06.766132	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
58	3	6	\N	1	\N	2013-03-26 10:36:38.913653	2013-07-30 08:23:06.782819	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
59	3	6	\N	1	\N	2013-03-26 10:37:11.545694	2013-07-30 08:23:06.799498	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
60	3	6	\N	1	\N	2013-03-26 10:37:36.193234	2013-07-30 08:23:06.816127	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
115	6	12	\N	0	14	2013-07-23 08:13:23.558746	2013-07-29 15:35:37.064419	1	\N	\N	\N	\N
113	6	12	\N	0	9	2013-07-23 08:12:35.407376	2013-07-29 15:35:24.285848	1	\N	\N	\N	\N
112	6	12	\N	0	2	2013-07-23 08:12:35.233638	2013-07-29 15:34:41.850227	1	\N	\N	\N	\N
114	6	12	\N	0	13	2013-07-23 08:13:08.682414	2013-07-29 15:35:30.65469	1	\N	\N	\N	\N
26	5	11	\N	1	4	2013-03-11 14:09:29.798968	2013-07-23 06:55:33.823231	1	\N	\N	\N	\N
40	5	11	\N	1	8	2013-03-26 10:21:08.436205	2013-07-23 06:55:33.889883	1	\N	\N	\N	\N
122	6	12	\N	0	7	2013-07-25 06:37:15.020148	2013-07-29 15:35:24.215597	1	\N	\N	\N	\N
128	6	12	\N	0	18	2013-07-25 06:38:30.238062	2013-07-29 15:35:37.221409	1	\N	\N	\N	\N
127	6	12	\N	0	17	2013-07-25 06:38:17.893164	2013-07-29 15:35:37.175606	1	\N	\N	\N	\N
116	6	12	\N	0	16	2013-07-23 08:13:44.657539	2013-07-29 15:35:37.137314	1	\N	\N	\N	\N
126	6	12	\N	0	15	2013-07-25 06:38:07.424535	2013-07-29 15:35:37.098474	1	\N	\N	\N	\N
148	3	2	\N	0	2	2013-07-29 06:54:56.536079	2013-07-29 07:53:26.972089	1	\N	\N	\N	\N
133	5	11	\N	1	\N	2013-07-25 07:09:11.046394	2013-07-25 07:09:30.081898	\N	\N	\N	\N	\N
135	5	11	\N	0	18	2013-07-25 07:10:01.755441	2013-07-29 15:34:09.979356	1	\N	\N	\N	\N
102	5	11	\N	0	3	2013-07-23 06:56:00.529977	2013-07-29 15:33:54.122223	1	\N	\N	\N	\N
136	5	11	\N	0	19	2013-07-25 07:10:10.589167	2013-07-29 15:34:10.013557	1	\N	\N	\N	\N
109	5	11	\N	0	16	2013-07-23 06:59:33.21822	2013-07-29 15:34:09.902691	1	\N	\N	\N	\N
130	5	11	\N	0	9	2013-07-25 07:08:46.355729	2013-07-29 15:33:54.401495	1	\N	\N	\N	\N
111	6	12	\N	0	1	2013-07-23 08:12:25.428781	2013-07-29 15:34:37.294025	1	\N	\N	\N	\N
149	3	2	\N	0	3	2013-07-29 06:56:07.441817	2013-07-29 07:53:27.010397	1	\N	\N	\N	\N
150	3	2	\N	1	\N	2013-07-29 06:56:07.472319	2013-07-29 06:56:23.399068	\N	\N	\N	\N	\N
139	5	11	\N	0	20	2013-07-26 16:20:36.466483	2013-07-29 15:34:10.047785	1	\N	\N	\N	\N
132	5	11	\N	0	12	2013-07-25 07:09:10.93764	2013-07-29 15:33:54.530339	1	\N	\N	\N	\N
141	3	2	\N	0	4	2013-07-29 06:34:14.441121	2013-07-29 07:53:27.049199	1	\N	\N	140	\N
62	3	5	\N	0	4	2013-07-22 07:29:35.047498	2013-07-23 13:44:37.474893	1	\N	\N	109#108#107#106#103#102#101	116#112#111
63	3	5	\N	0	5	2013-07-22 07:35:13.532813	2013-07-23 13:48:13.434915	1	\N	\N	109#108#107#106#105#104#103#102#101	116#112
70	3	5	\N	0	12	2013-07-22 08:09:31.17367	2013-07-23 13:53:05.494334	1	\N	\N	108#106#105#104#103	115
76	3	5	\N	0	18	2013-07-22 08:41:01.572201	2013-07-23 13:56:00.930381	1	\N	\N	108#105#104#102	116
87	3	5	\N	0	23	2013-07-22 09:17:51.310746	2013-07-23 13:58:27.814571	1	\N	\N	109#108#106#105#104#103	116#112
80	3	5	\N	0	25	2013-07-22 08:48:50.805229	2013-07-23 13:59:05.705629	1	\N	\N	108#106#104	116
88	3	5	\N	0	30	2013-07-22 09:18:50.187277	2013-07-23 14:03:01.330469	1	\N	\N	108#105#104#103	116#115#113
93	3	5	\N	0	35	2013-07-22 09:37:48.907838	2013-07-23 14:05:13.323215	1	\N	\N	109#106#103#102	116
74	3	5	\N	0	16	2013-07-22 08:38:11.241252	2013-07-23 14:05:34.878749	1	\N	\N	109#108#105#102#101	116#112
96	3	5	\N	0	37	2013-07-22 09:53:08.312417	2013-07-23 14:08:19.806768	1	\N	\N	109#103#102	116#113
97	3	5	\N	0	38	2013-07-22 09:54:48.952553	2013-07-23 14:08:32.216496	1	\N	\N	108#105#104#101	113
98	3	5	\N	0	39	2013-07-22 09:55:59.216198	2013-07-23 14:08:54.541524	1	\N	\N	108#105#104	116#113
99	3	5	\N	0	40	2013-07-22 09:57:07.452876	2013-07-23 14:09:08.026397	1	\N	\N	108#105#104	113
100	3	5	\N	0	41	2013-07-22 09:58:10.326022	2013-07-23 14:09:25.039483	1	\N	\N	107#105#104	113#111
95	3	5	\N	0	42	2013-07-22 09:51:41.707335	2013-07-23 14:09:35.974664	1	\N	\N	107#105#104	113
106	5	11	\N	0	11	2013-07-23 06:58:32.997299	2013-07-29 15:33:54.484682	1	\N	\N	\N	\N
131	5	11	\N	0	10	2013-07-25 07:09:00.528165	2013-07-29 15:33:54.446916	1	\N	\N	\N	\N
117	6	12	\N	0	3	2013-07-25 06:35:37.452181	2013-07-29 15:35:01.201926	1	\N	\N	\N	\N
120	6	12	\N	0	6	2013-07-25 06:36:33.066617	2013-07-29 15:35:01.3361	1	\N	\N	\N	\N
118	6	12	\N	0	4	2013-07-25 06:35:57.622886	2013-07-29 15:35:01.241916	1	\N	\N	\N	\N
123	6	12	\N	0	10	2013-07-25 06:37:31.473428	2013-07-29 15:35:24.324672	1	\N	\N	\N	\N
124	6	12	\N	0	11	2013-07-25 06:37:44.025324	2013-07-29 15:35:27.795482	1	\N	\N	\N	\N
107	5	11	\N	0	1	2013-07-23 06:58:57.181236	2013-07-23 15:35:12.936593	1	\N	\N	\N	\N
103	5	11	\N	0	4	2013-07-23 06:56:18.143813	2013-07-29 15:33:54.168422	1	\N	\N	\N	\N
105	5	11	\N	0	5	2013-07-23 06:57:59.461376	2013-07-29 15:33:54.20567	1	\N	\N	\N	\N
101	5	11	\N	0	6	2013-07-23 06:55:47.111326	2013-07-29 15:33:54.251245	1	\N	\N	\N	\N
104	5	11	\N	0	7	2013-07-23 06:56:39.247655	2013-07-29 15:33:54.303756	1	\N	\N	\N	\N
129	5	11	\N	0	8	2013-07-25 07:08:24.850968	2013-07-29 15:33:54.349425	1	\N	\N	\N	\N
134	5	11	\N	0	17	2013-07-25 07:09:47.089581	2013-07-29 15:34:09.945159	1	\N	\N	\N	\N
140	5	11	\N	0	21	2013-07-26 16:20:44.46675	2013-07-29 15:34:10.081891	1	\N	\N	\N	\N
137	5	11	\N	0	2	2013-07-26 16:20:07.004284	2013-07-29 15:33:54.075769	1	\N	\N	\N	\N
110	5	11	\N	0	13	2013-07-23 07:00:01.647825	2013-07-29 15:33:54.576491	1	\N	\N	\N	\N
121	6	12	\N	0	8	2013-07-25 06:36:56.43126	2013-07-29 15:35:24.247519	1	\N	\N	\N	\N
125	6	12	\N	0	12	2013-07-25 06:37:55.321771	2013-07-29 15:35:27.841236	1	\N	\N	\N	\N
119	6	12	\N	0	5	2013-07-25 06:36:16.460787	2013-07-29 15:35:01.293528	1	\N	\N	\N	\N
108	5	11	\N	0	15	2013-07-23 06:59:18.233081	2013-07-29 15:34:09.868949	1	\N	\N	\N	\N
151	3	2	\N	0	5	2013-07-29 06:57:51.886192	2013-07-29 07:53:27.087503	1	\N	\N	140	\N
142	3	2	\N	0	6	2013-07-29 06:35:41.09801	2013-07-29 07:53:27.134784	1	\N	\N	139	\N
143	3	2	\N	0	7	2013-07-29 06:39:41.105941	2013-07-29 07:53:27.173545	1	\N	\N	140	\N
144	3	2	\N	0	8	2013-07-29 06:41:31.769901	2013-07-29 07:53:27.211813	1	\N	\N	140	\N
145	3	2	\N	0	9	2013-07-29 06:45:54.48203	2013-07-29 07:53:27.250655	1	\N	\N	140	\N
146	3	2	\N	0	10	2013-07-29 06:50:27.391207	2013-07-29 07:53:27.288925	1	\N	\N	140	\N
152	3	2	\N	0	11	2013-07-29 07:13:43.562891	2013-07-29 07:53:27.327807	1	\N	\N	140	\N
153	3	2	\N	0	12	2013-07-29 07:17:32.524017	2013-07-29 07:53:27.366094	1	\N	\N	140	\N
154	3	2	\N	0	13	2013-07-29 07:23:55.81304	2013-07-29 07:53:27.40494	1	\N	\N	140	\N
155	3	2	\N	0	14	2013-07-29 07:26:46.665795	2013-07-29 07:53:27.44328	1	\N	\N	140	\N
156	3	2	\N	0	15	2013-07-29 07:28:35.395899	2013-07-29 07:53:27.4821	1	\N	\N	140	\N
157	3	2	\N	0	16	2013-07-29 07:30:06.40656	2013-07-29 07:53:27.520416	1	\N	\N	140	\N
158	3	2	\N	0	17	2013-07-29 07:32:02.59831	2013-07-29 07:53:27.559219	1	\N	\N	140	\N
159	3	2	\N	0	18	2013-07-29 07:33:17.425659	2013-07-29 07:53:27.59755	1	\N	\N	140	\N
160	3	2	\N	0	19	2013-07-29 07:34:32.833529	2013-07-29 07:53:27.636372	1	\N	\N	140	\N
161	3	2	\N	0	20	2013-07-29 07:37:52.691354	2013-07-29 07:53:27.68293	1	\N	\N	\N	\N
162	3	2	\N	0	21	2013-07-29 07:40:30.673073	2013-07-29 07:53:27.718136	1	\N	\N	140	\N
164	3	2	\N	0	22	2013-07-29 07:43:55.374986	2013-07-29 07:53:27.753814	1	\N	\N	140	\N
163	3	2	\N	0	23	2013-07-29 07:41:57.050574	2013-07-29 07:53:27.789061	1	\N	\N	140	\N
165	3	2	\N	0	24	2013-07-29 07:45:13.943898	2013-07-29 07:53:27.824243	1	\N	\N	140	\N
166	3	2	\N	0	25	2013-07-29 07:46:59.46693	2013-07-29 07:53:27.859386	1	\N	\N	140	\N
167	3	2	\N	1	\N	2013-07-29 07:46:59.537186	2013-07-29 07:47:15.952684	\N	\N	\N	140	\N
168	3	2	\N	0	26	2013-07-29 07:49:11.72442	2013-07-29 07:53:27.894626	1	\N	\N	140	\N
169	3	2	\N	0	27	2013-07-29 07:50:41.896009	2013-07-29 07:53:27.929799	1	\N	\N	140	\N
147	3	2	\N	0	1	2013-07-29 06:51:47.520509	2013-07-29 07:53:26.889876	1	\N	\N	140	\N
170	3	2	\N	0	28	2013-07-29 07:53:23.057428	2013-07-29 07:54:29.578835	1	\N	\N	140	\N
171	3	2	\N	0	\N	2013-07-29 07:54:53.658813	2013-07-29 07:56:06.980581	1	\N	\N	140	\N
172	3	2	\N	0	\N	2013-07-29 07:56:30.607322	2013-07-29 08:01:23.703552	1	\N	\N	140	\N
175	3	2	\N	0	\N	2013-07-29 08:00:24.802956	2013-07-29 08:01:17.170213	1	\N	\N	140	\N
174	3	2	\N	0	\N	2013-07-29 07:59:14.34016	2013-07-29 08:01:19.540016	1	\N	\N	140	\N
173	3	2	\N	0	\N	2013-07-29 07:57:44.632574	2013-07-29 08:01:21.672907	1	\N	\N	140	\N
138	5	11	\N	0	14	2013-07-26 16:20:16.767733	2013-07-29 15:34:09.852713	1	\N	\N	\N	\N
178	3	6	\N	1	\N	2013-07-30 10:26:04.101468	2013-07-30 12:21:52.459194	1	\N	\N	\N	121
179	3	6	\N	1	\N	2013-07-30 10:30:57.947882	2013-07-30 12:21:52.471202	1	\N	\N	\N	122
176	3	6	\N	1	\N	2013-07-30 10:12:04.106722	2013-07-30 12:21:52.479817	1	\N	\N	\N	\N
177	3	6	\N	1	\N	2013-07-30 10:17:05.888879	2013-07-30 12:21:52.504672	1	\N	\N	\N	\N
180	3	6	\N	0	\N	2013-07-30 12:24:46.452772	2013-07-30 13:50:18.230464	1	\N	\N	\N	117
182	3	6	\N	0	\N	2013-07-30 12:28:52.468514	2013-07-30 13:50:27.812066	1	\N	\N	\N	118
184	3	6	\N	0	\N	2013-07-30 12:34:42.369891	2013-07-30 13:51:32.779429	1	\N	\N	134	121
186	3	6	\N	0	\N	2013-07-30 12:42:42.234435	2013-07-30 13:51:44.984856	1	\N	\N	132	125
188	3	6	\N	0	\N	2013-07-30 12:47:31.307724	2013-07-30 13:52:04.562266	1	\N	\N	134	125
181	3	6	\N	0	\N	2013-07-30 12:26:49.409256	2013-07-30 13:53:48.435935	1	\N	\N	132	123
183	3	6	\N	0	\N	2013-07-30 12:31:57.576199	2013-07-30 13:54:29.371436	1	\N	\N	129#131#132#134	120#122#126
196	3	6	\N	0	\N	2013-07-30 13:11:48.09786	2013-07-30 13:54:48.105368	1	\N	\N	129#131#132#134	122#127
185	3	6	\N	0	\N	2013-07-30 12:41:14.550533	2013-07-30 13:54:59.494274	1	\N	\N	131#132#134	122
194	3	6	\N	0	\N	2013-07-30 13:07:46.089047	2013-07-30 13:55:29.600113	1	\N	\N	136	\N
189	3	6	\N	0	\N	2013-07-30 12:50:34.140828	2013-07-30 13:55:54.409591	1	\N	\N	129	123#125
190	3	6	\N	0	\N	2013-07-30 12:52:23.213795	2013-07-30 13:56:09.311716	1	\N	\N	129#132	123
191	3	6	\N	0	\N	2013-07-30 12:54:07.2573	2013-07-30 13:56:31.101944	1	\N	\N	129	123
192	3	6	\N	0	\N	2013-07-30 12:56:40.516421	2013-07-30 13:56:48.032744	1	\N	\N	129#134	124
193	3	6	\N	0	\N	2013-07-30 12:58:44.032072	2013-07-30 13:57:15.591189	1	\N	\N	130#131	117#122#126
195	3	6	\N	0	\N	2013-07-30 13:10:22.704304	2013-07-30 13:57:41.112992	1	\N	\N	129#131#132	117#122
187	3	6	\N	0	\N	2013-07-30 12:44:14.03444	2013-07-30 13:58:05.370954	1	\N	\N	130	117
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY schema_migrations (version) FROM stdin;
20120905160159
20120906075651
20120907092435
20120907095401
20120907095636
20120907095854
20120907100028
20120913125449
20120914155318
20120918075741
20120918080253
20120918080307
20120918080317
20120918080328
20120918080715
20120920074505
20120920101331
20120920135502
20120929141337
20121001153131
20121008161229
20121008161428
20121008162633
20121008163653
20121114094411
\.


--
-- Data for Name: servizis; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servizis (id, fkparent, nome, label, enabled, ordine, path, skip_cat, created_at, updated_at) FROM stdin;
1	0	home	Home	1	1	/	\N	2012-11-08 10:06:46.584074	2012-11-08 10:06:46.584074
4	2	banner	Banner home	1	\N	/admin/mods?idserv=4	1	2012-11-22 15:25:49.875	2012-11-22 15:25:49.914521
5	2	settori	Gestione Settori	1	\N	/admin/mods?idserv=5	1	2013-03-11 14:06:26.792245	2013-03-11 14:06:26.800847
6	2	applicazioni	Gestione Applicazioni	1	\N	/admin/mods?idserv=6	1	2013-03-11 14:06:38.300794	2013-03-11 14:06:38.30317
7	2	blog	Gestione Blog	1	\N	/admin/mods?idserv=7	1	2013-03-14 10:46:25.388882	2013-03-14 10:47:05.068167
3	2	produzione	Produzione	1	\N	/admin/mods?idserv=3	1	2012-11-08 10:22:35.062329	2013-03-26 09:43:37.57577
\.


--
-- Data for Name: static_page_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY static_page_smls (id, title, subtitle, header, testo, footer, created_at, updated_at, static_page_id, fklang) FROM stdin;
4	Innovazione	\N	\N	In Rotas la capacità produttiva è un fatto strategico. I nostri macchinari sono “nostri” nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. A misura delle vostre esigenze. <br /><p style="color:#000;"><br />La capacità di disporre di un approccio globale, vi consente soluzioni semplici, o ricche di unicità e specificità. La produzione Rotas, lavora in stretto rapporto con l'ideazione. Per questo è in grado di creare soluzioni che affrontano tutti gli elementi della tecnologia: il <span style="background-color:;font-weight:bold;">materiale</span>, la <span style="background-color:;font-weight:bold;">superficie</span>, l'<span style="background-color:;font-weight:bold;">inchiostro</span>, l'<span style="background-color:;font-weight:bold;">adesivo</span>, il <span style="background-color:;font-weight:bold;">supporto</span>, <span style="background-color:;font-weight:bold;">rfid</span>. </p>	\N	2013-05-27 09:23:08.653094	2013-05-27 10:02:38.45361	4	it
3	Storia	\N	\N	<p style="color:#000;"><strong>40 anni di esperienza</strong> ci hanno portato a un modo originale di affrontare le opportunità di vestizione dei vostri prodotti. La nostra unicità, è di creare plus valore attraverso elementi di comunicazione, agendo sulle singole basi della tecnologia</p><p style="color:#000;"><span style="background-color:;font-weight:bold;">ROTAS</span>, crea un vestito ai vostri prodotti, dando un valore non solo estetico. Affianca i vostri creativi nell'individuare le soluzioni tecniche che creano plus valore per catturare l'attenzione, differenziare i prodotti e dare un'immagine di prima qualità e perciò di maggior valore. </p><p style="color:#000;">In <span style="background-color:;font-weight:bold;">Rotas</span> la capacità produttiva è un fatto strategico. I nostri macchinari sono "nostri" nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. </p><p style="color:#000;"><span style="background-color:;font-weight:bold;">A misura delle vostre esigenze.</span></p>	\N	2013-05-27 09:22:47.840327	2013-07-08 08:10:18.348109	3	it
6	Rotas Institute	\N	\N	testo rotas institute<br />	\N	2013-05-27 12:10:07.319283	2013-05-27 12:10:07.319283	6	it
1	-1- ... da 40 anni i primi	\N	\N	<p style="color:#000;"><span style="background-color:;font-weight:bold;">Abbiamo sempre contribuito</span> a creare le macchine nuove per la produzione e la stampa di etichette autoadesive perché abbiamo ideato prodotti che non esistevano, anticipando le richieste del mercato e gli sviluppi dei consumi. </p><h4>1966</h4><p class="ef-small-title" style="color:#000;">costruiamo la nostra <span style="background-color:;font-weight:bold;">prima macchina per stampare in bobina</span> </p><h4>1967</h4><p class="ef-small-title" style="color:#000;">Nasce Rotas <span style="background-color:;font-weight:bold;">prima azienda</span> in italia <span style="background-color:;font-weight:bold;">a produrre esclusivamente etichette adesive in bobina</span> </p><h4>1980</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">primo brevetto</span> (immagini e disegni 3D) </p><h4>1982</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rotas certificata da UL</span> in Italia e Spagna (sicurezza prodotti USA) </p><h4>1989</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta adesiva per enologia.</span> </p><h4>1992</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">primo "label-Book"</span> (etichetta multipagina) </p><h4>1993</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rototac, la prima macchina elettrica semiautomatica</span> per applicare etichette e marcare lotto </p><h4>1999</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta scratch-off</span> (tessere telefoniche) </p><h4>2001</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta RFID</span> (industria farmaceutica) </p><h4>2005</h4><p class="ef-small-title" style="color:#000;">nasce la nuova sede produttiva in Spagna, <span style="background-color:;font-weight:bold;">Rotas Ibérica</span> </p><h4>2006</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima antenna RFID totalmente disegnata e prodotta da Rotas</span> </p><h4>2006</h4><p class="ef-small-title" style="color:#000;">Creaiamo un'<span style="background-color:;font-weight:bold;">azienda esclusivamdnte dedicata a</span> sistemi e soluzioni <span style="background-color:;font-weight:bold;">RFID</span> </p><h4>2007</h4><p class="ef-small-title" style="color:#000;">prima etichetta RFID per <span style="background-color:;font-weight:bold;">applicazione sotterranea</span> (condotti gas) </p><h4>2009</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Biolabel (R)</span> l'etichetta piú ecologica (fatta di pietra) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Hercules (R)</span>, l'etichetta per bottiglie fredde e umide (ance per il cemento) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rotas Pipe</span>, (sistema RFID per composti medicinali) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;">Membri dell'Istituto di Centromarca per la Difesa e l'Identificazione di Marchi Autentici e Lotta alla Contraffazione <span style="background-color:;font-weight:bold;">(INDICAM)</span> </p><h4>2011</h4><p class="ef-small-title" style="color:#000;">Patent applications: <span style="background-color:;font-weight:bold;">Rotas Building Tracking</span> (sistema per la tracciabilità nell'edilizia). Patent pending: <span style="background-color:;font-weight:bold;">Rotas Art</span> (innovativo tablet videoguida per musei). </p><h4>2012</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">NFC Tag</span> integrati in biglietti da visita ed etichette, da leggere con smartphone. </p><h4>2012</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">1ª etichetta con Brillante.</span> </p>	\N	2013-05-27 08:59:38.396125	2013-07-08 08:04:53.603157	1	it
7	Bandi	\N	\N	<span style="background-color:#ffffff;">Testo rotas bandi</span><br />	\N	2013-05-27 12:10:20.553632	2013-07-08 08:10:50.459917	7	it
5	Company Profile	\N	\N	<p style="color:#000;"><span style="background-color:;font-weight:bold;">ROTAS</span>, crea un vestito ai vostri prodotti, dando un valore non solo estetico. Affianca i vostri creativi nell'individuare le soluzioni tecniche che creano plus valore per catturare l'attenzione, differenziare i prodotti e dare un'immagine di prima qualità e perciò di maggior valore. </p><p style="color:#000;">In <span style="background-color:;font-weight:bold;">Rotas</span> la capacità produttiva è un fatto strategico. I nostri macchinari sono "nostri" nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. </p><p style="color:#000;"><span style="background-color:;font-weight:bold;">A misura delle vostre esigenze.</span></p>	\N	2013-05-27 10:08:39.7419	2013-07-12 12:07:23.663855	5	it
2	Come	\N	\N	<h4>Analisi dei problemi</h4><p style="color:#000;">Siamo educati all'ascolto.<br />In Rotas, siamo addestrati ad ascoltare in modo attivo, per “scavare” insieme, per capire fino in fondo le vostre esigenze, per individuare i vostri problemi, per mettere a fuoco il problema stesso. È parte integrante del nostro lavoro.<br />Spesso un differente angolo di visione, una nuova ottica amplia il campo visivo e fa emergere nuovi elementi.</p><h4>Problem solving</h4><p style="color:#000;">La soluzione creativa. Intuizione o professionalità?<br />Per dare soluzioni su misura e a richiesta, Rotas non si affida solo al “colpo di genio” che spesso ci vuole. Conoscenza delle tecniche e metodo di lavoro ci consentono di disporre di un patrimonio creativo che si basa sulle tecniche di IMAGINERING.<br />Immaginazione finalizzata alla soluzione.</p><h4>Soluzione lavori</h4><p style="color:#000;">Quasi 50 anni di attività: un'esperienza che ci consente di affrontare le opportunità in modo ampio. ”La” soluzione anticipa spesso “le” soluzioni.<br />Rotas affianca il cliente nel trovare insieme un equilibrio tra la qualità delle proposte, i costi, i tempi.<br />Il nostro patrimonio è anche la capacità di essere semplici, sintetici e pratici per individuare un percorso produttivo, capace di favorire i vostri prodotti con un approccio efficace e fattibile. Siamo un'azienda che produce direttamente. Il nostro punto di forza è la ricerca e l'innovazione, cioè lo sviluppo delle più moderne tecnologie e di nuovi prodotti. Siamo titolari di brevetti da molti anni.</p><h4>Pianificazione progetto</h4><p style="color:#000;">Fare le cose giuste, al momento giusto, nei tempi giusti. La qualità di coordinare rapidamente le esigenze del cliente con le possibilità produttive è un servizio fondamentale; i problemi si auto alimentano perché non vengono ben capiti. Così i rischi di interpretazione aumentano e le soluzioni si allontanano. Rotas ha individuato un sistema lavoro con un buon livello di pianificazione e con un sistema informatico totalmente integrato. Definizione delle caratteristiche del prodotto, definizione del disegno con l'approvazione del cliente, produzione e consegna.</p>	\N	2013-05-27 09:05:51.817487	2013-07-12 12:07:13.259919	2	it
8	Il Materiale	\N	\N	Le nuove tecnologie, e la capacità di studiare l'etichetta come un ... sistema integrato, ci consentono di utilizzare i più differenti materiali: dall'alluminio alla classica carta vergata, dal film alla carta riciclata, dai materiali resistenti alle alte temperature alla completa fragilità di quelli anti-manomissione. Abbiamo anche la possibilità di far coesistere differenti materiali nella stessa etichetta.	\N	2013-07-16 10:00:33.714329	2013-07-16 10:00:33.714329	8	it
9	La superficie	\N	\N	Trattamenti speciali di superficie, possono essere una forte ... caratterizzazione del packaging. Richiedono una impegnativa ricerca per la complessa relazione che intercorre fra il trattamento ed i materiali. L'aspetto nasconde molta sostanza. Plastificazione, laminazione, verniciatura lucida-opaca, goffratura, fustellatura superficiale sia interna che nel retro, Braillle, sono solo alcuni esempi.	\N	2013-07-16 10:01:20.416308	2013-07-16 10:01:20.416308	9	it
10	Il supporto	\N	\N	Spessore, grammatura, comportamento al calore, ... resistenza alla trazione, velocità di applicazione, leggibilità delle fotocellule. Queste tra le caratteristiche che devono essere considerate per garantire alte prestazioni nel tempo. Il liner può essere anche stampato nel retro sicurezza, a rilievo, ecologici, acidocromici, ecc.	\N	2013-07-16 10:01:57.695775	2013-07-16 10:01:57.695775	10	it
11	L'adesione	\N	\N	E' il principio fondamentale che, attraverso il contatto e la ... pressione, permette di creare un legame tra due superfici. Rotas produce etichette per le più differenti condizioni di applicazione e di utilizzo ed adatte alle diverse superfici di applicazione, lisce o ruvide, a temperature estreme, all'umidità, alla resistenza chimica, con adesioni permanenti, semipermanenti, riposizionabili, rimovibili, ultrarimovibili, antimanomissione, con interazione intramolecolare.	\N	2013-07-16 10:02:31.409095	2013-07-16 10:02:31.409095	11	it
12	L'inchiostro	\N	\N	Dalla capacità di "coprire" alla forza di trasmettere un messaggio ... attivo, vivo con l'ambiente. Uso di nanotecnologie, inchiostri lucidi e opachi, termocromici, fotocromatici, magnetici, igrostatici, di sicurezza, a rilievo, ecologici, acidocromici, ecc.	\N	2013-07-16 10:02:59.264336	2013-07-16 10:02:59.264336	12	it
13	L'RFID	\N	\N	La nostra divisione RFID é specializzata nella produzione a basso costo ed in grande volume di etichette con tag RFID.	\N	2013-07-16 10:03:29.427867	2013-07-16 10:03:29.427867	13	it
14	Rotas More info	\N	\N	testo rotas more info	\N	2013-07-16 10:14:46.869755	2013-07-16 10:14:46.869755	14	it
15	Social	\N	\N	testo social	\N	2013-07-16 10:14:56.659122	2013-07-16 10:14:56.659122	15	it
\.


--
-- Data for Name: static_pages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY static_pages (id, f_del, published, created_at, updated_at, foto, foto_home, url_pagina, home_page, ordine, related_cat) FROM stdin;
2	0	\N	2013-05-27 09:05:51.812489	2013-07-12 13:15:19.221211	9afa2fd2-d548.jpg	\N	/about	0	1	
4	0	\N	2013-05-27 09:23:08.65012	2013-07-12 13:15:19.24501	6182964a-c0c4.jpg	\N	/innovazione	\N	2	
5	0	\N	2013-05-27 10:08:39.704536	2013-07-12 13:15:19.252578	ea381bb3-a580.jpg	\N	/company-profile	\N	3	
6	0	\N	2013-05-27 12:10:07.279859	2013-07-12 13:15:19.261006	028d3e1e-e0c3.jpg	\N	/rotas-institute	\N	4	
7	0	\N	2013-05-27 12:10:20.545711	2013-07-12 13:15:19.302806	5e6f4b0b-b2fa.jpg	\N	/bandi	\N	5	
1	0	\N	2013-05-27 08:59:38.378063	2013-07-12 13:15:19.310957	ea45afd8-23a1.jpg	\N	/rotas	0	6	
3	0	\N	2013-05-27 09:22:47.835653	2013-07-12 13:15:19.319317	4658c820-3f5b.jpg	\N	/storia	\N	7	
8	0	\N	2013-07-16 10:00:33.676931	2013-07-16 10:00:33.676931	\N	\N	/materiale	\N	\N	
9	0	\N	2013-07-16 10:01:20.40261	2013-07-16 10:01:20.40261	\N	\N	/superficie	\N	\N	
10	0	\N	2013-07-16 10:01:57.673065	2013-07-16 10:01:57.673065	\N	\N	/supporto	\N	\N	
11	0	\N	2013-07-16 10:02:31.394079	2013-07-16 10:02:31.394079	\N	\N	/adesione	\N	\N	
12	0	\N	2013-07-16 10:02:59.242585	2013-07-16 10:02:59.242585	\N	\N	/inchiostro	\N	\N	
13	0	\N	2013-07-16 10:03:29.411037	2013-07-16 10:03:29.411037	\N	\N	/rfid	\N	\N	
14	0	\N	2013-07-16 10:14:46.849924	2013-07-16 10:14:46.849924	\N	\N	/moreinfo	\N	\N	
15	0	\N	2013-07-16 10:14:56.638326	2013-07-16 10:14:56.638326	\N	\N	/social	\N	\N	
\.


--
-- Data for Name: translation_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY translation_smls (id, translation_id, testo, created_at, updated_at, fklang) FROM stdin;
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY translations (id, etc, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: upload_folders; Type: TABLE DATA; Schema: public; Owner: -
--

COPY upload_folders (id, fkparent, nome, created_at, updated_at, f_del) FROM stdin;
11	0	Desktop	2012-11-14 11:06:03.142096	2012-11-14 11:06:03.142096	0
12	11	immagini banner home	2012-11-22 15:25:24.42255	2012-11-22 15:25:24.42255	0
13	11	test	2013-07-16 06:55:37.236621	2013-07-16 06:55:41.32197	1
14	11	test1	2013-07-16 06:55:45.562271	2013-07-16 06:55:56.070733	1
15	11	industria	2013-07-16 15:29:06.810643	2013-07-16 15:29:06.810643	0
18	11	RFID	2013-07-24 15:52:23.208738	2013-07-24 15:52:23.208738	0
21	11	Food Beverage & Oil	2013-07-26 16:21:44.725827	2013-07-26 16:21:44.725827	0
24	18	RFID_produzione	2013-07-30 10:01:36.536487	2013-07-30 10:01:36.536487	0
19	18	RFID_settori	2013-07-24 15:53:09.378072	2013-07-30 10:01:43.05319	0
20	18	RFID_applicazioni	2013-07-24 15:53:17.159839	2013-07-30 10:01:48.852402	0
17	15	Industria_applicazioni	2013-07-23 13:35:32.9598	2013-07-30 10:02:04.899387	0
16	15	Industria_settori	2013-07-23 13:35:25.174183	2013-07-30 10:02:09.783612	0
23	21	FoodBeverage&Oil_produzione	2013-07-29 06:26:28.487943	2013-07-30 10:02:23.263498	0
22	21	FoodBevarge&Oil_settori	2013-07-29 06:25:17.865412	2013-07-30 10:02:31.06044	0
\.


--
-- Data for Name: uploads; Type: TABLE DATA; Schema: public; Owner: -
--

COPY uploads (id, filename, ext, content_type, file_size, created_at, updated_at, fkfolder) FROM stdin;
31	5c2cde45-ccd8.jpg	image/jpeg	\N	178467	2012-12-13 15:47:49.141993	2012-12-13 15:47:49.141993	11
32	dae89035-6402.jpg	image/jpeg	\N	240510	2012-12-13 15:47:49.702667	2012-12-13 15:47:49.702667	11
33	879decf8-b95b.jpg	image/jpeg	\N	108248	2012-12-13 15:47:50.221277	2012-12-13 15:47:50.221277	11
34	60e7d48d-af59.jpg	image/jpeg	\N	106012	2012-12-13 15:47:50.759677	2012-12-13 15:47:50.759677	11
35	c46456f5-c231.jpg	image/jpeg	\N	264411	2012-12-13 15:50:43.873748	2012-12-13 15:50:43.873748	11
36	b6077cb4-b1b6.jpg	image/jpeg	\N	237897	2012-12-13 15:51:18.400599	2012-12-13 15:51:18.400599	11
37	3f11c2bc-cc20.jpg	image/jpeg	\N	220472	2012-12-13 15:51:19.112852	2012-12-13 15:51:19.112852	11
38	bc56c4fb-9bd4.jpg	image/jpeg	\N	187696	2012-12-13 15:51:19.667434	2012-12-13 15:51:19.667434	11
39	768f21f1-fc22.jpg	image/jpeg	\N	84002	2012-12-13 16:01:57.656988	2012-12-13 16:01:57.656988	11
28	02A13AWG.jpg	image/jpeg	\N	270718	2012-11-22 15:47:04.095673	2012-11-22 15:47:04.095673	12
29	team.jpg	image/jpeg	\N	363193	2012-11-22 15:47:04.865831	2012-11-22 15:47:04.865831	12
40	05dd65d9-41fb.jpg	image/jpeg	\N	590068	2013-07-15 15:14:43.066953	2013-07-16 15:29:27.799842	15
41	332382ef-002b.png	image/png	\N	462419	2013-07-22 07:23:42.204336	2013-07-22 07:23:42.204336	11
42	c75e9d02-1a47.png	image/png	\N	611608	2013-07-22 07:30:29.010477	2013-07-22 07:30:29.010477	11
43	2697c508-514b.png	image/png	\N	982420	2013-07-22 07:45:37.024803	2013-07-22 07:45:37.024803	11
44	5a8d2cc8-e7fc.png	image/png	\N	701853	2013-07-22 07:48:07.45428	2013-07-22 07:48:07.45428	11
45	3ac5ffdc-2bde.png	image/png	\N	478938	2013-07-22 08:05:07.732283	2013-07-22 08:05:07.732283	11
46	4336914b-4947.png	image/png	\N	901919	2013-07-22 08:07:10.986055	2013-07-22 08:07:10.986055	11
47	254fed05-978b.png	image/png	\N	412647	2013-07-22 08:08:32.273972	2013-07-22 08:08:32.273972	11
48	a59b1a56-160d.png	image/png	\N	1194483	2013-07-22 08:09:43.357544	2013-07-22 08:09:43.357544	11
49	c94bd2ff-d1ca.png	image/png	\N	1222834	2013-07-22 08:12:11.739981	2013-07-22 08:12:11.739981	11
50	b276ca4e-d01f.png	image/png	\N	802515	2013-07-22 08:14:04.351654	2013-07-22 08:14:04.351654	11
51	2cbc344a-0c43.png	image/png	\N	1089016	2013-07-22 08:17:03.319696	2013-07-22 08:17:03.319696	11
52	7d26db6b-db2f.png	image/png	\N	790428	2013-07-22 08:38:36.081593	2013-07-22 08:38:36.081593	11
53	5701f1ac-8d5a.png	image/png	\N	1169486	2013-07-22 08:40:03.234562	2013-07-22 08:40:03.234562	11
54	89f935b7-0451.png	image/png	\N	753509	2013-07-22 08:41:25.387798	2013-07-22 08:41:25.387798	11
55	aa996159-5bc3.png	image/png	\N	726244	2013-07-22 08:43:03.979098	2013-07-22 08:43:03.979098	11
56	9f82df96-c8c9.png	image/png	\N	1345291	2013-07-22 08:44:23.499381	2013-07-22 08:44:23.499381	11
58	b851e530-6000.png	image/png	\N	607627	2013-07-22 08:47:56.468357	2013-07-22 08:47:56.468357	11
59	c102bea3-d701.png	image/png	\N	1345240	2013-07-22 08:49:10.970848	2013-07-22 08:49:10.970848	11
60	d9831d14-2ee3.png	image/png	\N	570293	2013-07-22 09:11:08.414215	2013-07-22 09:11:08.414215	11
61	322bde70-0f0b.png	image/png	\N	705951	2013-07-22 09:11:35.590794	2013-07-22 09:11:35.590794	11
62	8ca8b5e5-b564.png	image/png	\N	526635	2013-07-22 09:13:26.030332	2013-07-22 09:13:26.030332	11
63	cc89ea1e-3173.png	image/png	\N	468351	2013-07-22 09:14:45.782172	2013-07-22 09:14:45.782172	11
64	7da169b9-5cbb.png	image/png	\N	609072	2013-07-22 09:15:53.936239	2013-07-22 09:15:53.936239	11
65	627d8ba4-f1ec.png	image/png	\N	2312412	2013-07-22 09:17:06.595452	2013-07-22 09:17:06.595452	11
66	d7c76773-2d62.png	image/png	\N	996977	2013-07-22 09:18:03.328061	2013-07-22 09:18:03.328061	11
67	bddf81b0-f651.png	image/png	\N	691946	2013-07-22 09:19:03.295965	2013-07-22 09:19:03.295965	11
68	43ad2887-aec2.png	image/png	\N	705076	2013-07-22 09:32:41.594773	2013-07-22 09:32:41.594773	11
69	643b4979-2ebf.png	image/png	\N	1269112	2013-07-22 09:33:50.867343	2013-07-22 09:33:50.867343	11
70	977fc565-df15.png	image/png	\N	1452563	2013-07-22 09:35:05.718779	2013-07-22 09:35:05.718779	11
71	4afe5398-401f.png	image/png	\N	608048	2013-07-22 09:36:18.679341	2013-07-22 09:36:18.679341	11
72	13c9076c-7aec.png	image/png	\N	1174857	2013-07-22 09:37:58.465647	2013-07-22 09:37:58.465647	11
73	b9e77942-96f1.png	image/png	\N	816378	2013-07-22 09:39:00.348877	2013-07-22 09:39:00.348877	11
74	0f1adbd5-4992.png	image/png	\N	648439	2013-07-22 09:52:02.390298	2013-07-22 09:52:02.390298	11
75	d0c7ded0-a9b6.png	image/png	\N	1242683	2013-07-22 09:53:28.43083	2013-07-22 09:53:28.43083	11
76	f1aa3349-d69b.png	image/png	\N	993777	2013-07-22 09:55:02.345999	2013-07-22 09:55:02.345999	11
77	a81a92c9-1fb8.png	image/png	\N	963380	2013-07-22 09:56:09.826075	2013-07-22 09:56:09.826075	11
78	80730879-be25.png	image/png	\N	903778	2013-07-22 09:56:43.758875	2013-07-22 09:56:43.758875	11
79	aa896ef4-5428.png	image/png	\N	957689	2013-07-22 09:57:41.397802	2013-07-22 09:57:41.397802	11
80	a867a68e-d587.jpg	image/jpeg	\N	639925	2013-07-23 07:01:22.237193	2013-07-23 07:01:22.237193	11
81	81061bd1-6076.jpg	image/jpeg	\N	1569711	2013-07-23 07:02:35.301057	2013-07-23 07:02:35.301057	11
82	5958c1ed-8efd.jpg	image/jpeg	\N	1563411	2013-07-23 07:03:27.211305	2013-07-23 07:03:27.211305	11
83	e94061d4-cf29.jpg	image/jpeg	\N	653497	2013-07-23 07:03:59.905437	2013-07-23 07:03:59.905437	11
84	b2f11ce7-40d0.jpg	image/jpeg	\N	790381	2013-07-23 07:04:21.958832	2013-07-23 07:04:21.958832	11
86	9209995a-5c59.jpg	image/jpeg	\N	511463	2013-07-23 07:05:30.69968	2013-07-23 07:05:30.69968	11
87	9d0295e7-93cf.jpg	image/jpeg	\N	856355	2013-07-23 07:06:09.314885	2013-07-23 07:06:09.314885	11
88	69f30a0a-8d45.jpg	image/jpeg	\N	1297266	2013-07-23 07:06:47.902408	2013-07-23 07:06:47.902408	11
89	7ceaf0ff-7083.jpg	image/jpeg	\N	853281	2013-07-23 07:07:24.081716	2013-07-23 07:07:24.081716	11
90	5d49e0b5-ab87.jpg	image/jpeg	\N	1857906	2013-07-23 08:14:40.199289	2013-07-23 08:14:40.199289	11
91	fa6e9703-d207.jpg	image/jpeg	\N	1303083	2013-07-23 08:15:07.961965	2013-07-23 08:15:07.961965	11
92	3e203902-df3b.jpg	image/jpeg	\N	1112497	2013-07-23 08:15:34.832449	2013-07-23 08:15:34.832449	11
93	d744f555-9a3c.jpg	image/jpeg	\N	993674	2013-07-23 08:16:50.722203	2013-07-23 08:16:50.722203	11
94	b0a35754-7cc5.jpg	image/jpeg	\N	558296	2013-07-23 08:17:17.291908	2013-07-23 08:17:17.291908	11
95	f05ae615-b2d9.jpg	image/jpeg	\N	1202837	2013-07-23 08:17:41.143613	2013-07-23 08:17:41.143613	11
106	99d75126-c28f.jpg	image/jpeg	\N	1188253	2013-07-23 13:35:45.826922	2013-07-23 13:35:45.826922	17
107	c67ce828-26ee.jpg	image/jpeg	\N	1162851	2013-07-23 13:35:55.44759	2013-07-23 13:35:55.44759	17
109	3bd75e8a-7180.jpg	image/jpeg	\N	681107	2013-07-23 13:36:15.232877	2013-07-23 13:36:15.232877	17
110	e6d3e746-326c.jpg	image/jpeg	\N	499271	2013-07-23 13:36:24.251062	2013-07-23 13:36:24.251062	17
111	bc3b2783-36bd.jpg	image/jpeg	\N	921543	2013-07-23 13:36:31.586006	2013-07-23 13:36:31.586006	17
97	f48d4b1c-966d.jpg	image/jpeg	\N	869258	2013-07-23 13:17:36.886864	2013-07-23 13:40:17.056557	16
98	ce0ba4ad-34d9.jpg	image/jpeg	\N	1261550	2013-07-23 13:21:48.262622	2013-07-23 13:40:17.073327	16
99	ea3b5514-155e.jpg	image/jpeg	\N	483568	2013-07-23 13:21:59.150059	2013-07-23 13:40:17.090006	16
100	42825233-0811.jpg	image/jpeg	\N	739369	2013-07-23 13:22:06.907472	2013-07-23 13:40:17.106668	16
101	c53c7cd1-31db.jpg	image/jpeg	\N	1009434	2013-07-23 13:22:14.491856	2013-07-23 13:40:17.123357	16
102	ff545ddc-190f.jpg	image/jpeg	\N	615459	2013-07-23 13:22:35.880558	2013-07-23 13:40:17.140027	16
103	40542e2a-87b7.jpg	image/jpeg	\N	478351	2013-07-23 13:22:45.769708	2013-07-23 13:40:17.156652	16
104	75a48455-7d99.jpg	image/jpeg	\N	665771	2013-07-23 13:22:53.088612	2013-07-23 13:40:17.173326	16
105	d70e85f7-693c.jpg	image/jpeg	\N	1151893	2013-07-23 13:22:59.979036	2013-07-23 13:40:17.190003	16
112	32b663d5-2ee7.jpg	image/jpeg	\N	997362	2013-07-23 14:11:05.973457	2013-07-23 14:11:05.973457	17
96	c6e661a3-1d18.jpg	image/jpeg	\N	873874	2013-07-23 13:16:56.714753	2013-07-23 13:40:17.032266	16
113	50e5bef4-8870.jpg	image/jpeg	\N	1167023	2013-07-24 15:53:51.493494	2013-07-24 15:53:51.493494	20
114	27782eea-d678.jpg	image/jpeg	\N	718465	2013-07-24 15:54:11.781377	2013-07-24 15:54:11.781377	20
115	5809a8a6-c099.jpg	image/jpeg	\N	564212	2013-07-24 15:54:19.725615	2013-07-24 15:54:19.725615	20
116	351637e0-930a.jpg	image/jpeg	\N	688875	2013-07-24 15:54:25.978449	2013-07-24 15:54:25.978449	20
117	3e916ae4-835c.jpg	image/jpeg	\N	531911	2013-07-24 15:54:34.445298	2013-07-24 15:54:34.445298	20
118	67d2b31f-c551.jpg	image/jpeg	\N	576733	2013-07-24 15:54:40.667292	2013-07-24 15:54:40.667292	20
119	4f814cd0-4c70.jpg	image/jpeg	\N	831909	2013-07-24 15:54:49.705894	2013-07-24 15:54:49.705894	20
120	a0a26800-4250.jpg	image/jpeg	\N	684000	2013-07-24 15:54:56.531234	2013-07-24 15:54:56.531234	20
121	7a64457a-9f0d.jpg	image/jpeg	\N	759381	2013-07-24 15:55:04.943629	2013-07-24 15:55:04.943629	20
122	3290f6bf-29fc.jpg	image/jpeg	\N	544908	2013-07-24 15:55:12.110169	2013-07-24 15:55:12.110169	20
123	9e2f322d-1d13.jpg	image/jpeg	\N	645007	2013-07-24 15:55:18.227314	2013-07-24 15:55:18.227314	20
124	89414ae6-67fb.jpg	image/jpeg	\N	745990	2013-07-24 15:55:26.107046	2013-07-24 15:55:26.107046	20
125	1d2e9e16-329b.jpg	image/jpeg	\N	811130	2013-07-24 15:55:36.180384	2013-07-24 15:55:36.180384	20
126	4a453b22-2646.jpg	image/jpeg	\N	761060	2013-07-24 15:55:49.274478	2013-07-24 15:55:49.274478	19
127	5c58c46e-0b56.jpg	image/jpeg	\N	970301	2013-07-24 15:55:55.898014	2013-07-24 15:55:55.898014	19
128	af1ee7d5-2642.jpg	image/jpeg	\N	509391	2013-07-24 15:56:02.272364	2013-07-24 15:56:02.272364	19
129	a0675066-31ad.jpg	image/jpeg	\N	531911	2013-07-24 15:56:08.748388	2013-07-24 15:56:08.748388	19
130	f5de2295-8120.jpg	image/jpeg	\N	876960	2013-07-24 15:56:15.593026	2013-07-24 15:56:15.593026	19
131	ab14b9c7-0ecb.jpg	image/jpeg	\N	599186	2013-07-24 15:56:21.238483	2013-07-24 15:56:21.238483	19
132	936afdf4-c49c.jpg	image/jpeg	\N	1062924	2013-07-24 15:56:28.115008	2013-07-24 15:56:28.115008	19
133	ae372a93-7d14.jpg	image/jpeg	\N	869110	2013-07-26 16:22:35.313475	2013-07-29 06:25:51.782356	22
134	54095226-5f47.jpg	image/jpeg	\N	983909	2013-07-26 16:22:50.860829	2013-07-29 06:25:51.818328	22
135	ea48c735-e7cf.jpg	image/jpeg	\N	1134940	2013-07-26 16:23:04.342573	2013-07-29 06:25:51.841654	22
136	ca534f15-f413.jpg	image/jpeg	\N	778199	2013-07-26 16:23:53.723499	2013-07-29 06:25:51.865196	22
137	e900f997-ad27.jpg	image/jpeg	\N	632719	2013-07-29 06:26:43.973439	2013-07-29 06:26:43.973439	23
138	4b3b8089-0ce4.jpg	image/jpeg	\N	635743	2013-07-29 06:26:53.889735	2013-07-29 06:26:53.889735	23
139	843da062-ce1e.jpg	image/jpeg	\N	941611	2013-07-29 06:27:00.549688	2013-07-29 06:27:00.549688	23
140	afc1e861-ff80.jpg	image/jpeg	\N	437044	2013-07-29 06:27:11.770169	2013-07-29 06:27:11.770169	23
141	1fe19736-84d8.jpg	image/jpeg	\N	485047	2013-07-29 06:27:12.74782	2013-07-29 06:27:12.74782	23
142	9f7db1a8-1a7b.jpg	image/jpeg	\N	412092	2013-07-29 06:27:12.753605	2013-07-29 06:27:12.753605	23
143	c8d27ad3-a04a.jpg	image/jpeg	\N	366826	2013-07-29 06:27:13.26513	2013-07-29 06:27:13.26513	23
144	7210a222-fcc3.jpg	image/jpeg	\N	388856	2013-07-29 06:27:13.928066	2013-07-29 06:27:13.928066	23
145	3de3989e-aefd.jpg	image/jpeg	\N	558592	2013-07-29 06:27:14.869463	2013-07-29 06:27:14.869463	23
146	2a1ffad6-8c49.jpg	image/jpeg	\N	507589	2013-07-29 06:27:15.904192	2013-07-29 06:27:15.904192	23
147	eb743da8-bc67.jpg	image/jpeg	\N	801217	2013-07-29 06:27:17.542965	2013-07-29 06:27:17.542965	23
148	5887f137-3fe0.jpg	image/jpeg	\N	534694	2013-07-29 06:27:17.950078	2013-07-29 06:27:17.950078	23
149	690e66bb-2a92.jpg	image/jpeg	\N	883437	2013-07-29 06:27:19.376527	2013-07-29 06:27:19.376527	23
150	930987e4-31e6.jpg	image/jpeg	\N	517203	2013-07-29 06:27:19.899063	2013-07-29 06:27:19.899063	23
151	430a3a85-3ef7.jpg	image/jpeg	\N	483695	2013-07-29 06:27:21.270997	2013-07-29 06:27:21.270997	23
152	4c2a48e7-bc79.jpg	image/jpeg	\N	383486	2013-07-29 06:27:21.917559	2013-07-29 06:27:21.917559	23
153	1c213e51-7611.jpg	image/jpeg	\N	705222	2013-07-29 06:27:23.596291	2013-07-29 06:27:23.596291	23
154	e63737ff-4679.jpg	image/jpeg	\N	547428	2013-07-29 06:27:24.898944	2013-07-29 06:27:24.898944	23
155	c327770b-2372.jpg	image/jpeg	\N	682069	2013-07-29 06:27:25.567347	2013-07-29 06:27:25.567347	23
156	838bb928-a25d.jpg	image/jpeg	\N	623889	2013-07-29 06:27:26.394011	2013-07-29 06:27:26.394011	23
157	588d39ca-d078.jpg	image/jpeg	\N	749600	2013-07-29 06:27:27.285629	2013-07-29 06:27:27.285629	23
158	b4e0a783-8ce5.jpg	image/jpeg	\N	654753	2013-07-29 06:27:28.829399	2013-07-29 06:27:28.829399	23
159	c04936fd-ce37.jpg	image/jpeg	\N	423852	2013-07-29 06:27:29.850201	2013-07-29 06:27:29.850201	23
160	016fb285-39e4.jpg	image/jpeg	\N	610659	2013-07-29 06:27:31.253362	2013-07-29 06:27:31.253362	23
161	49ee505b-d13b.jpg	image/jpeg	\N	414512	2013-07-29 06:27:32.323684	2013-07-29 06:27:32.323684	23
162	cd794cc6-dec5.jpg	image/jpeg	\N	549571	2013-07-29 06:27:33.245924	2013-07-29 06:27:33.245924	23
163	efba9442-1ef5.jpg	image/jpeg	\N	616765	2013-07-29 06:27:34.787948	2013-07-29 06:27:34.787948	23
164	703b7555-116b.jpg	image/jpeg	\N	444091	2013-07-29 06:27:36.664239	2013-07-29 06:27:36.664239	23
165	026b2884-2cbb.jpg	image/jpeg	\N	541288	2013-07-29 06:27:37.8223	2013-07-29 06:27:37.8223	23
166	759c7833-3bce.jpg	image/jpeg	\N	534694	2013-07-29 06:27:40.532799	2013-07-29 06:27:40.532799	23
169	9ced0413-31e0.jpg	image/jpeg	\N	390941	2013-07-29 07:20:34.753693	2013-07-29 07:20:34.753693	23
171	e5ca6879-c8c7.jpg	image/jpeg	\N	481844	2013-07-29 07:25:12.273586	2013-07-29 07:25:12.273586	23
173	98a84122-c98d.jpg	image/jpeg	\N	686924	2013-07-29 07:35:48.081827	2013-07-29 07:35:48.081827	23
174	1282b01f-67b5.jpg	image/jpeg	\N	311395	2013-07-29 07:38:59.040034	2013-07-29 07:38:59.040034	23
176	58b15351-9b64.jpg	image/jpeg	\N	749600	2013-07-29 07:51:54.331119	2013-07-29 07:51:54.331119	23
177	891e1298-8837.png	image/png	\N	476522	2013-07-30 10:02:53.982361	2013-07-30 10:02:53.982361	24
178	7da2154d-a80a.png	image/png	\N	1095852	2013-07-30 10:02:57.870968	2013-07-30 10:02:57.870968	24
179	ab313cab-074d.png	image/png	\N	726259	2013-07-30 10:02:57.878362	2013-07-30 10:02:57.878362	24
180	94e09dd3-ebf1.png	image/png	\N	1098090	2013-07-30 10:02:59.046774	2013-07-30 10:02:59.046774	24
181	68e64f25-1fe7.png	image/png	\N	667355	2013-07-30 10:02:59.464813	2013-07-30 10:02:59.464813	24
182	7b514beb-4f55.png	image/png	\N	1362972	2013-07-30 10:03:02.250499	2013-07-30 10:03:02.250499	24
183	2041b669-505c.png	image/png	\N	644049	2013-07-30 10:03:04.572272	2013-07-30 10:03:04.572272	24
184	d47fc245-94e0.png	image/png	\N	1369889	2013-07-30 10:03:05.369236	2013-07-30 10:03:05.369236	24
185	5b9b9df9-e4e4.png	image/png	\N	394122	2013-07-30 10:03:17.30784	2013-07-30 10:03:17.30784	24
186	749d39d7-4cc4.png	image/png	\N	971906	2013-07-30 13:14:06.047782	2013-07-30 13:14:06.047782	24
187	a457491a-d369.png	image/png	\N	509753	2013-07-30 13:25:18.480548	2013-07-30 13:25:18.480548	24
188	b052efb7-2ce5.png	image/png	\N	1393086	2013-07-30 13:25:20.769825	2013-07-30 13:25:20.769825	24
189	de0e6ab9-faba.png	image/png	\N	1204632	2013-07-30 13:36:16.456315	2013-07-30 13:36:16.456315	24
190	81268703-ad66.png	image/png	\N	1376235	2013-07-30 13:36:18.87068	2013-07-30 13:36:18.87068	24
191	332c0931-7afa.png	image/png	\N	723566	2013-07-30 13:48:26.270581	2013-07-30 13:48:26.270581	24
192	d02f9afb-a3cb.png	image/png	\N	750674	2013-07-30 13:48:26.891562	2013-07-30 13:48:26.891562	24
193	d542f7d5-c3c1.png	image/png	\N	844338	2013-07-30 13:48:35.397284	2013-07-30 13:48:35.397284	24
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, name, email, created_at, updated_at, password_digest, remember_token, level, active) FROM stdin;
1	Administrator	admin@rotas.it	2012-11-08 10:06:38.109858	2012-11-08 10:06:38.109858	$2a$10$lqlG.WLcUNNeqa1TOE7m5.h7duyotk7rjUCsuWjEs0croerYoEyvq	yKMhzHX8w-j0yNCi8QRXXA	0	1
\.


--
-- Data for Name: videos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY videos (id, url, img, created_at, updated_at, title, url_name) FROM stdin;
16	https://www.youtube.com/watch?v=Ro0oQtJh1Ck&#38;feature=youtube_gdata	http://img.youtube.com/vi/Ro0oQtJh1Ck/2.jpg	2012-11-16 15:18:41.257315	2012-11-16 15:18:41.257315	Fresco e Vario - Spot TV 25" La squadra di rugby	Ro0oQtJh1Ck
17	https://www.youtube.com/watch?v=-Klyw1LgDv4&#38;feature=youtube_gdata	http://img.youtube.com/vi/-Klyw1LgDv4/2.jpg	2012-11-16 15:18:41.262447	2012-11-16 15:18:41.262447	Fresco e Vario - Spot TV 25" Frigo vuoto?	-Klyw1LgDv4
18	https://www.youtube.com/watch?v=M8zRrdgiaAI&#38;feature=youtube_gdata	http://img.youtube.com/vi/M8zRrdgiaAI/2.jpg	2012-11-16 15:18:41.265651	2012-11-16 15:18:41.265651	Fresco e Vario - Spot TV 25" La torta dei tuoi sogni	M8zRrdgiaAI
19	https://www.youtube.com/watch?v=b0cdoFhlRsQ&#38;feature=youtube_gdata	http://img.youtube.com/vi/b0cdoFhlRsQ/2.jpg	2012-11-16 15:18:41.268842	2012-11-16 15:18:41.268842	Fresco & Vario - Spot TV 25" - Al mercato	b0cdoFhlRsQ
20	https://www.youtube.com/watch?v=vQIFO4R4Fcg&#38;feature=youtube_gdata	http://img.youtube.com/vi/vQIFO4R4Fcg/2.jpg	2012-11-16 15:18:41.272956	2012-11-16 15:18:41.272956	Fresco e Vario - Spot TV 7"	vQIFO4R4Fcg
\.


--
-- Name: lingues_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lingues
    ADD CONSTRAINT lingues_pkey PRIMARY KEY (id);


--
-- Name: media_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: media_videos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_videos
    ADD CONSTRAINT media_videos_pkey PRIMARY KEY (id);


--
-- Name: mod_cat_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_cat_smls
    ADD CONSTRAINT mod_cat_smls_pkey PRIMARY KEY (id);


--
-- Name: mod_cats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_cats
    ADD CONSTRAINT mod_cats_pkey PRIMARY KEY (id);


--
-- Name: mod_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_smls
    ADD CONSTRAINT mod_smls_pkey PRIMARY KEY (id);


--
-- Name: mods_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mods
    ADD CONSTRAINT mods_pkey PRIMARY KEY (id);


--
-- Name: servizis_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servizis
    ADD CONSTRAINT servizis_pkey PRIMARY KEY (id);


--
-- Name: static_page_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY static_page_smls
    ADD CONSTRAINT static_page_smls_pkey PRIMARY KEY (id);


--
-- Name: static_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY static_pages
    ADD CONSTRAINT static_pages_pkey PRIMARY KEY (id);


--
-- Name: translation_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY translation_smls
    ADD CONSTRAINT translation_smls_pkey PRIMARY KEY (id);


--
-- Name: translations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: upload_folders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY upload_folders
    ADD CONSTRAINT upload_folders_pkey PRIMARY KEY (id);


--
-- Name: uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY uploads
    ADD CONSTRAINT uploads_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: videos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY videos
    ADD CONSTRAINT videos_pkey PRIMARY KEY (id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

