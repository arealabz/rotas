/* contact form */
$(document).ready(function(){
	// form contatti
	var form = $("#ef-contact");
	var name = $("#name");
	var email = $("#email");
	var message = $("#message");

	var inputs = form.find(":input").filter(":not(:submit)").filter(":not(:checkbox)").filter(":not([type=hidden])").filter(":not([validate=false])");

	$("#send_contatti").click(function() {

		if(validateText(name) & validateEmail(email)){
			var $name = name.val();
				var $email = email.val();
				var $message = message.val();
				$.ajax({
					type: 'POST',
					url: "/contatti",
					data: form.serialize(),
					success: function(ajaxCevap) {
						$('.ef-list').hide();
						$('.ef-list').prepend(ajaxCevap);
						$('.ef-list').fadeIn("normal");
						name.attr("value", "");
						email.attr("value", "");
						message.attr("value", "");
					}
				});
			return false;
		} else {
			return false;
		}
	});

	$("#send_job").click(function() {
		if(validateEmail( $("#email_job") ) & validateText($("#nome")) & validateText($("#via")) & validateText($("#citta")) & validateText($("#cognome")) & validateText($("#richiesta")) ){
			$.ajax({
				type: 'POST',
				url: "/sendjob",
				data: $("#ef-contact-job").serialize(),
				success: function(ajaxCevap) {
					$('.ef-list').hide();
					$('.ef-list').prepend(ajaxCevap);
					$('.ef-list').fadeIn("normal");
				}
			});
			return false;
		} else {
			return false;
		}
	});


	function validateEmail(obj){
		var a = obj.val();
		var filter = /^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/;
		if(filter.test(a)){
			obj.parent().removeClass("not-valid").addClass("valid");
			return true;
		}
		else{
			obj.parent().addClass("not-valid").removeClass("valid");
			return false;
		}
	}

	function validateText(obj){
		if(!obj.val()){
			obj.parent().addClass("not-valid").removeClass("valid");
			return false;
		}
		else{
			obj.parent().removeClass("not-valid").addClass("valid");
			return true;
		}
	}

});


/* end contact form */