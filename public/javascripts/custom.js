function hideOrShowProduzioneSidebar() {
	if ( $("#produzione-sidebar") && $(window).width() <= 1024 ) {
		$("#produzione-sidebar").hide();
		$("#page-title").hide();
	}

	if ( $("#produzione-sidebar") && $(window).width() > 1024 ) {
		$("#produzione-sidebar").show();
		$("#page-title").show();
	}
}
//Initialize
jQuery(function($){

	hideOrShowProduzioneSidebar();

/*Detect touch device*/
	var tryTouch;
	try {
	document.createEvent("TouchEvent");
	tryTouch = 1;
	} catch (e) {
		tryTouch = 0;
	}

/*Browser detection*/
	var deviceAgent = navigator.userAgent.toLowerCase();
	var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
	if (agentID) {
	    $('body').addClass('ef-ios');
	}

	if ($.browser.msie) {
		$('body').addClass('ef-ie');
	    if ($.browser.version == 8) $('body').addClass('ef-ie8');
	    if ($.browser.version == 7) $('body').addClass('ef-ie7');
	};

	if( $.browser.opera ){
		$('body').addClass('ef-opera');
	}

	if ($('body').hasClass('ef-ie7')) {
		$('body').css({position: 'relative'}).append('<span class="ie7overlay"></span>').html('<div class="ie7message">Hello! My website requires MS Internet Explorer 9 or higher version. Please update your browser.</div>')
	}

/*Setting up variables*/
	var homePage = '#home';
	var sidebarWdt = 15; /*Width of the Sidebar in percents*/

/*Replace page scroll on desktops */
	if (!$('body').hasClass('ef-opera') || tryTouch == 0) {

		$(".ef-fold").niceScroll({
			cursoropacitymax:0.7,
			cursorcolor: '#000000',
			cursorborder:"0px solid rgba(255, 255, 255, 0.5)",
			touchbehavior:false, /*enable cursor-drag scrolling like touch devices in desktop computer, default is false*/
			grabcursorenabled:false,
			cursorwidth: 3,
			railoffset:{left:-3},
			autohidemode: true
		});
	}


/*Disable arrow navigation*/
	var disableArrowKeys = function(e) {
	    if ($.inArray(e.keyCode, ar)>=0) {
	        e.preventDefault();
	    }
	}

	var ar=new Array(1,45,33,34,35,36,38,40);
	var ininput = false;

	$("input, textarea").focus(function(){ininput = true;});
	$("input, textarea").blur(function(){ininput = false;});

	$(document).keydown(function(e) {
	     var key = e.which;
	      if(($.inArray(key,ar) > -1) || (!ininput && key == 32)) {
	          e.preventDefault();
	          return false;
	      }
	      return true;
	});


/*Navigation menu, smooth scrolling and current section detection*/

		/* hack for keep the menu always visible */
		$('.ef-menu-wrapper').toggleClass('ef-toggle-menu')

		/*Toggle menu
			old code, move the above line into the hover function to make it works
			as expected

		$('.ef-menu-tab, .ef-menu-wrapper').hover(function () {
			$('.ef-menu-wrapper').toggleClass('ef-toggle-menu')
		});
		*/

		/*Lavalamp*/
		    $('.ef-navmenu').lavaLamp({
		        fx: 'easeOutBack',
		        speed: 800,
		        returnDelay: 500,
		        returnHome: true,
		        autoReturn: false,
		        setOnClick: false,
		        homeLeft:-1000
		    });

	    /*Main menu, smooth scrolling to anchors and urls*/
	    	$('body').delegate('.ef-content', 'waypoint.reached', function(event, direction) {
	    		var $active = $(this);
	    		if (direction === "up") {
	    			$active = $active.prev();
	    		}
	    		if (!$active.length) $active = $active.end();

	    		$('.current-section').removeClass('current-section');
	    		$active.addClass('current-section');

	    		$('.ef-menu-cur').removeClass('ef-menu-cur');
	    		$('.ef-navmenu a[rel=external]').click(function() {
	    			document.location = $(this).attr("href");
	    		});
	    		$('.ef-navmenu a[href=#'+$active.attr('id')+']').parent().addClass('ef-menu-cur');

	    	});
	    	$('.ef-content').waypoint({ offset: '50%' });

			$('.ef-column a[href*=#]:not([href$=#]), #ef-topbar a[href*=#]:not([href$=#]), .ef-navmenu a, .ef-logo, .ef-next-page, .ef-prev-page').click(function (event) {
				if ($('body').hasClass('ef-ie8') || $('body').hasClass('ef-opera') || $('body').hasClass('ef-ios') || $(this).hasClass('ef-external-link')){} else {
					event.preventDefault();
					var linkOffset = $($(this).attr('href')).offset().top;
					$.scrollTo(linkOffset, 400, {axis:'y', onAfter: function () {
						if ($(window).width()>1024) {
							setTimeout(function() {
								window.location.hash = '#'+$('.current-section').attr('id')
							}, 500);
						}
					},  easing:'easeInOutQuart'})
				}
			});

			$('a[href$=#]').click(function(event){return false;});

		/*Mobile menu*/
			$('.ef-navmenu').mobileMenu({
				defaultText: '',
				className: 'ef-select-menu',
				subMenuClass: 'sub-menu',
				subMenuDash: '&ndash;',
				appendMenu: '#ef-mobile-menu-wrapper'
			});

/*Fittext for page title*/
	$('.ef-page-details h1').fitText(0.8);

/*Image overlay*/
	if (tryTouch == 1) {
		$('a.lb').remove()
	}

	$('.ef-overlay').each(function () {
		if ($(this).find('a.lb').lenght == 0 && $(this).find('.ef-read-more').length == 0) {
			$(this).remove();
		} else if ($(this).find('a.lb').length > 0 && $(this).find('.ef-read-more').length == 0) {
			$(this).addClass('ef-lightbox')
		} else if ($(this).find('a.lb').length == 0 && $(this).find('.ef-read-more').length > 0) {
			$(this).addClass('ef-read')
		} else {
			$(this).addClass('ef-lightbox-read')
		}
	});

	$('.ef-overlay').hover(function () {
		$(this).addClass('ef-hover')
	}, function () {
		$(this).removeClass('ef-hover')
	});

/*Topbar*/
	$('#ef-topbar-button').click(function() {
		$('#ef-topbar').toggleClass('ef-top-o');
		$(this).toggleClass('ef-close');

		if ($(this).hasClass('ef-close')) {
			$('#ef-topbar').animate({top: '0%'},500, 'easeInOutQuart');
		} else {
			$('#ef-topbar').animate({top: '-100%'},500, 'easeInOutQuart');
		}
	});

/*Fullscreen mode*/
	$('#tray-button').click(function() {

		var foldIcon = $(this),
			foldIconWdt = foldIcon.outerWidth()*3,
			currentSec = $('.current-section')

		foldIcon.toggleClass('ef-close');

		if($(this).hasClass('ef-close')){

			$('#ef-topbar-button').fadeOut();

			if ($('#ef-topbar-button').hasClass('ef-close')) {
				$('#ef-topbar-button').click()
			}
			$('.gradi').css({display: 'none'});
			$('.ef-slider-overlay').fadeOut();
			$('.dots-play').css({zIndex: '-1'});
			$('.dots-play, .ef-nav-wrapper').animate({left: '0%'},500, 'easeInOutQuart');
			$('.ef-toggled').animate({left: -sidebarWdt+'%'},500, 'easeInOutQuart');
			currentSec.find('.ef-fold').stop().animate({
				left: '100%'
			},500, 'easeInOutQuart', function(){
				$('.ef-nav-wrapper').fadeOut();
				$('#slidecaption').fadeIn();
				foldIcon.stop().animate({
					right: -foldIconWdt
				}, function(){
					$('.dots-play').css({display: 'none'});
					foldIcon.stop().animate({right: '0%'}).addClass('ef-close-add');
					$('#slider-controls-wrapper').css({zIndex: '3'})
				});
			});
		} else {
			$('#slidecaption').fadeOut();
			$('.ef-nav-wrapper').fadeIn();
			foldIcon.stop().animate({
				right: -foldIconWdt
			}, function(){
				$('.ef-slider-overlay').fadeIn();
				$('.dots-play').css({zIndex: 'auto', display: 'block'});
				$('.dots-play, .ef-nav-wrapper').animate({left: sidebarWdt+'%'}, 500, 'easeInOutQuart');
				$('.ef-toggled').animate({left: '0%'}, 500, 'easeInOutQuart');

				currentSec.find('.ef-fold').stop().animate({
					left: sidebarWdt+'%'
				}, 500, 'easeInOutQuart', function(){
					foldIcon.stop().animate({right: '0%'}).removeClass('ef-close-add');
					$('#ef-topbar-button').fadeIn();
					$('.gradi').css({display: 'block'});
					$('#slider-controls-wrapper').css({zIndex: 'auto'})
				});
			});
		}
	});

/*Portfolio & Blog*/
	var $containerP = $('#ef-portfolio .shortcode-wrapper'),
		$wrapperP = $('#ef-portfolio'),
		$efItemP = $('.ef-item-p'),
		$container = $('#ef-blog .shortcode-wrapper'),
		$wrapper = $('#ef-blog'),
		$efItem = $('.ef-item'),
		iOptions = {
			resizable: false,
			transformsEnabled: false,
			animationEngine: 'css'
		}

	$(window).smartresize(function(){

		if ($(window).width() >= 500) {

			$efItemP.css({ width: Math.floor($wrapperP.width()*0.2)});
			if ($(window).width() <= 1440) {
				$efItemP.css({width: Math.floor($wrapperP.width()*0.25)});
			}
			if ($(window).width() <= 1024) {
				$efItemP.css({width: Math.floor($wrapperP.width()*0.33)});
			}
			if ($(window).width() <= 800) {
				$efItemP.css({width: Math.floor($wrapperP.width()*0.5)});
			}

			$efItemP.each(function () {
				if ($(this).hasClass('ef-featured')) {
					$(this).css({width: $efItemP.width()*2});
				}
			})

			$efItem.css({ width: Math.floor($wrapper.width()*0.2-8)});
			if ($(window).width() <= 1440) {
				$efItem.css({width: Math.floor($wrapper.width()*0.25-8)});
			}
			if ($(window).width() <= 1024) {
				$efItem.css({width: Math.floor($wrapper.width()*0.33-8)});
			}
			if ($(window).width() <= 800) {
				$efItem.css({width: Math.floor($wrapper.width()*0.5-8)});
			}

			$efItem.each(function () {
				if ($(this).hasClass('ef-featured')) {
					$(this).css({width: $efItem.width()*2+8});
				}
			})
			$containerP.isotope(iOptions);
			$container.isotope(iOptions);
		}

	}).smartresize();

	$('#ef-filter a').click(function(){
		var selector = $(this).attr('data-filter');
		$containerP.isotope({ filter: selector,  transformsEnabled: false, animationEngine: 'css'});
		$('#ef-filter a').parent().removeClass();
		$(this).parent().addClass('p-current');
		return false;
	});

/*Window resize or browser zooming layout enhancements*/

	$(window).resize(function(){
		if ("#"+$('.current-section').attr('id') != homePage && tryTouch == 0) {
			$.scrollTo($('.current-section'),0);
		}

		if ($(window).width() <= 1024 && $('#tray-button').hasClass('ef-close')) {
			$('#tray-button').click()
		}

		if ($(window).width() <= 1024 && $('#ef-topbar-button').hasClass('ef-close')) {
			$('#ef-topbar-button').click()
		}

		if (Modernizr.mq('only screen and (max-width: 1024px)')) {
			$('#nextslide, #prevslide').css({top: $(window).height()/2, zIndex: '20'});
		} else {
			$('#nextslide, #prevslide').css({top: 'auto'});
		}

		/*Center images in the slider if image width < slider width*/
		$('ul.slides img').each(function () {
			if ($(this).width() < $('ul.slides').width()) {
				$(this).css({display: 'inline-block'})
			} else {
				$(this).css({display: 'block'})
			}
		})

		hideOrShowProduzioneSidebar();	

	});

	/*Alerts*/
	$('.ef-alertBox, .ef-list').append('<span></span>');
	$('.ef-alertBox span, .ef-list span').click(function() {
		$(this).parent().fadeOut(500);
	});

	/*goMap*/
	var lat1 = 45.68028,
		long1 = 12.238916

    $(".ef-map").goMap({
		maptype:"ROADMAP",
		zoom: 15, /*Default Zoom level*/
		scaleControl: true,
		navigationControl: true,
        scrollwheel: true,
        mapTypeControl: true,
        mapTypeControlOptions: {
            position: 'RIGHT',
            style: 'DROPDOWN_MENU'
        },
        markers: [{
            latitude: lat1,
            longitude: long1,
            id: 'info1',
            html: {
                popup: false
            }
        }],

        hideByClick: true,
        icon: '/assets/home.png',
        addMarker: false

    }); /* Other plugin options see here: http://www.pittss.lv/jquery/gomap/examples.php */

    $('#ef-office-1').click(function () {
    	$.goMap.setInfo('info1', 'Rotas Italia - Via San Francesco di Sales, 11 - 31100 Treviso - ITALY ');
    	google.maps.event.trigger($($.goMap.mapId).data('info1'), 'click');

    	var center = new google.maps.LatLng(lat1, long1);
    	$.goMap.map.setCenter(center);

    	$('.ef-cur-office').removeClass();
    	$(this).addClass('ef-cur-office');
    });

/*Tabs*/
    $('.ef-tabs').tabs({ fx: {opacity: 'show'}, selected: 0 });

/*Accordion*/
    $(".ef-accordion").accordion({ autoHeight: false, navigation: true });

/*Toggle boxes*/
    $('.ef-toggle-box').addClass('toggle-icn');
    $('.ef-toggle-box .toggle-content').css("display", "none");
    $('.ef-toggle-box li:first-child').addClass('open').find('.toggle-content').css("display", "block");
    $('.ef-toggle-box .toggle-head, .ef-toggle-box .toggle-head a').click(function() {
        $(this).next('.toggle-content').toggle('blind', 200);
        $(this).parent().toggleClass('open');
        return false
    });

/*jTweet*/
    $(".ef-tweet").tweet({
        count: 3,
        username: "perugini",
        loading_text: "Loading tweets",
        refresh_interval: 60
    }).bind("loaded", function() {
        $(this).find("a").attr("target", "_blank");
    });

/*jFlickfeed*/
    $('.jflickr').jflickrfeed({
        limit: 8,
        qstrings: {
            id: '41598036@N06'
        },
        itemTemplate: '<li>' + '<a class="lb" href="{{image}}" title="{{title}}">' + '<img src="{{image_s}}" alt="{{title}}" />' + '<span></span>' + '</a>' + '</li>'
    }, function(data) {

    });

/*Isotope relayout in iOS*/
    window.onorientationchange = detectIPadOrientation;
	function detectIPadOrientation () {
		$(window).smartresize(function(){}).smartresize();
	}

})

$(window).load(function() {

	/* hide or show the sidebar on load */
	hideOrShowProduzioneSidebar();

	/*Remove website loader*/
	$('#ef-loader-overlay').fadeOut(800);

/*Turn Isotope on load (just in case)*/
	$(window).smartresize(function(){}).smartresize();

	/*Responsive lightbox*/
		try {
			document.createEvent("TouchEvent");
		} catch (e) {
			$('.lb').rlightbox({
				counterDelimiter: " of ",
				setPrefix: "lb" /* To group 2 images, add a new class, e.g. ‘lb_flowers’ to both of them. */
			});
		}

	/*Post slider thumb width*/
		var thumbWdt = $('.ef-post-slider-thumbs').width()/5.5;

	/*Post slider with thumbs nav (Note, you are able to use only one slider with thumbs nav through all sections on a page. If you want some sliders on a page you can use slider with dots nav)*/
		$('.ef-post-slider-thumbs').flexslider({
		    animation: "fade",
	        controlNav: false,
	        smoothHeight: true,
	        animationLoop: false,
	        slideshow: false,
	        sync: ".carousel-thumbs"
		});

		$('.carousel-thumbs').flexslider({
		    animation: "slide",
		    controlNav: false,
		    animationLoop: false,
		    slideshow: false,
		    itemWidth: thumbWdt,
		    itemMargin: 0,
		    asNavFor: '.ef-post-slider-thumbs'
		});
})