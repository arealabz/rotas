/*!
 * jQuery JavaScript Library v1.7.2
 * http://jquery.com/
 *
 * Copyright 2011, John Resig
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 * Copyright 2011, The Dojo Foundation
 * Released under the MIT, BSD, and GPL Licenses.
 *
 * Date: Wed Mar 21 12:46:34 2012 -0700
 */

(function( window, undefined ) {

// Use the correct document accordingly with window argument (sandbox)
var document = window.document,
	navigator = window.navigator,
	location = window.location;
var jQuery = (function() {

// Define a local copy of jQuery
var jQuery = function( selector, context ) {
		// The jQuery object is actually just the init constructor 'enhanced'
		return new jQuery.fn.init( selector, context, rootjQuery );
	},

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$,

	// A central reference to the root jQuery(document)
	rootjQuery,

	// A simple way to check for HTML strings or ID strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	quickExpr = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,

	// Check if a string has a non-whitespace character in it
	rnotwhite = /\S/,

	// Used for trimming whitespace
	trimLeft = /^\s+/,
	trimRight = /\s+$/,

	// Match a standalone tag
	rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,

	// JSON RegExp
	rvalidchars = /^[\],:{}\s]*$/,
	rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
	rvalidtokens = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
	rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g,

	// Useragent RegExp
	rwebkit = /(webkit)[ \/]([\w.]+)/,
	ropera = /(opera)(?:.*version)?[ \/]([\w.]+)/,
	rmsie = /(msie) ([\w.]+)/,
	rmozilla = /(mozilla)(?:.*? rv:([\w.]+))?/,

	// Matches dashed string for camelizing
	rdashAlpha = /-([a-z]|[0-9])/ig,
	rmsPrefix = /^-ms-/,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return ( letter + "" ).toUpperCase();
	},

	// Keep a UserAgent string for use with jQuery.browser
	userAgent = navigator.userAgent,

	// For matching the engine and version of the browser
	browserMatch,

	// The deferred used on DOM ready
	readyList,

	// The ready event handler
	DOMContentLoaded,

	// Save a reference to some core methods
	toString = Object.prototype.toString,
	hasOwn = Object.prototype.hasOwnProperty,
	push = Array.prototype.push,
	slice = Array.prototype.slice,
	trim = String.prototype.trim,
	indexOf = Array.prototype.indexOf,

	// [[Class]] -> type pairs
	class2type = {};

jQuery.fn = jQuery.prototype = {
	constructor: jQuery,
	init: function( selector, context, rootjQuery ) {
		var match, elem, ret, doc;

		// Handle $(""), $(null), or $(undefined)
		if ( !selector ) {
			return this;
		}

		// Handle $(DOMElement)
		if ( selector.nodeType ) {
			this.context = this[0] = selector;
			this.length = 1;
			return this;
		}

		// The body element only exists once, optimize finding it
		if ( selector === "body" && !context && document.body ) {
			this.context = document;
			this[0] = document.body;
			this.selector = selector;
			this.length = 1;
			return this;
		}

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			// Are we dealing with HTML string or an ID?
			if ( selector.charAt(0) === "<" && selector.charAt( selector.length - 1 ) === ">" && selector.length >= 3 ) {
				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = quickExpr.exec( selector );
			}

			// Verify a match, and that no context was specified for #id
			if ( match && (match[1] || !context) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[1] ) {
					context = context instanceof jQuery ? context[0] : context;
					doc = ( context ? context.ownerDocument || context : document );

					// If a single string is passed in and it's a single tag
					// just do a createElement and skip the rest
					ret = rsingleTag.exec( selector );

					if ( ret ) {
						if ( jQuery.isPlainObject( context ) ) {
							selector = [ document.createElement( ret[1] ) ];
							jQuery.fn.attr.call( selector, context, true );

						} else {
							selector = [ doc.createElement( ret[1] ) ];
						}

					} else {
						ret = jQuery.buildFragment( [ match[1] ], [ doc ] );
						selector = ( ret.cacheable ? jQuery.clone(ret.fragment) : ret.fragment ).childNodes;
					}

					return jQuery.merge( this, selector );

				// HANDLE: $("#id")
				} else {
					elem = document.getElementById( match[2] );

					// Check parentNode to catch when Blackberry 4.6 returns
					// nodes that are no longer in the document #6963
					if ( elem && elem.parentNode ) {
						// Handle the case where IE and Opera return items
						// by name instead of ID
						if ( elem.id !== match[2] ) {
							return rootjQuery.find( selector );
						}

						// Otherwise, we inject the element directly into the jQuery object
						this.length = 1;
						this[0] = elem;
					}

					this.context = document;
					this.selector = selector;
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || rootjQuery ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return rootjQuery.ready( selector );
		}

		if ( selector.selector !== undefined ) {
			this.selector = selector.selector;
			this.context = selector.context;
		}

		return jQuery.makeArray( selector, this );
	},

	// Start with an empty selector
	selector: "",

	// The current version of jQuery being used
	jquery: "1.7.2",

	// The default length of a jQuery object is 0
	length: 0,

	// The number of elements contained in the matched element set
	size: function() {
		return this.length;
	},

	toArray: function() {
		return slice.call( this, 0 );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {
		return num == null ?

			// Return a 'clean' array
			this.toArray() :

			// Return just the object
			( num < 0 ? this[ this.length + num ] : this[ num ] );
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems, name, selector ) {
		// Build a new jQuery matched element set
		var ret = this.constructor();

		if ( jQuery.isArray( elems ) ) {
			push.apply( ret, elems );

		} else {
			jQuery.merge( ret, elems );
		}

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		ret.context = this.context;

		if ( name === "find" ) {
			ret.selector = this.selector + ( this.selector ? " " : "" ) + selector;
		} else if ( name ) {
			ret.selector = this.selector + "." + name + "(" + selector + ")";
		}

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	// (You can seed the arguments with an array of args, but this is
	// only used internally.)
	each: function( callback, args ) {
		return jQuery.each( this, callback, args );
	},

	ready: function( fn ) {
		// Attach the listeners
		jQuery.bindReady();

		// Add the callback
		readyList.add( fn );

		return this;
	},

	eq: function( i ) {
		i = +i;
		return i === -1 ?
			this.slice( i ) :
			this.slice( i, i + 1 );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ),
			"slice", slice.call(arguments).join(",") );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map(this, function( elem, i ) {
			return callback.call( elem, i, elem );
		}));
	},

	end: function() {
		return this.prevObject || this.constructor(null);
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: [].sort,
	splice: [].splice
};

// Give the init function the jQuery prototype for later instantiation
jQuery.fn.init.prototype = jQuery.fn;

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[0] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;
		target = arguments[1] || {};
		// skip the boolean and the target
		i = 2;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction(target) ) {
		target = {};
	}

	// extend jQuery itself if only one argument is passed
	if ( length === i ) {
		target = this;
		--i;
	}

	for ( ; i < length; i++ ) {
		// Only deal with non-null/undefined values
		if ( (options = arguments[ i ]) != null ) {
			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)) ) ) {
					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && jQuery.isArray(src) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject(src) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend({
	noConflict: function( deep ) {
		if ( window.$ === jQuery ) {
			window.$ = _$;
		}

		if ( deep && window.jQuery === jQuery ) {
			window.jQuery = _jQuery;
		}

		return jQuery;
	},

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Hold (or release) the ready event
	holdReady: function( hold ) {
		if ( hold ) {
			jQuery.readyWait++;
		} else {
			jQuery.ready( true );
		}
	},

	// Handle when the DOM is ready
	ready: function( wait ) {
		// Either a released hold or an DOMready/load event and not yet ready
		if ( (wait === true && !--jQuery.readyWait) || (wait !== true && !jQuery.isReady) ) {
			// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
			if ( !document.body ) {
				return setTimeout( jQuery.ready, 1 );
			}

			// Remember that the DOM is ready
			jQuery.isReady = true;

			// If a normal DOM Ready event fired, decrement, and wait if need be
			if ( wait !== true && --jQuery.readyWait > 0 ) {
				return;
			}

			// If there are functions bound, to execute
			readyList.fireWith( document, [ jQuery ] );

			// Trigger any bound ready events
			if ( jQuery.fn.trigger ) {
				jQuery( document ).trigger( "ready" ).off( "ready" );
			}
		}
	},

	bindReady: function() {
		if ( readyList ) {
			return;
		}

		readyList = jQuery.Callbacks( "once memory" );

		// Catch cases where $(document).ready() is called after the
		// browser event has already occurred.
		if ( document.readyState === "complete" ) {
			// Handle it asynchronously to allow scripts the opportunity to delay ready
			return setTimeout( jQuery.ready, 1 );
		}

		// Mozilla, Opera and webkit nightlies currently support this event
		if ( document.addEventListener ) {
			// Use the handy event callback
			document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );

			// A fallback to window.onload, that will always work
			window.addEventListener( "load", jQuery.ready, false );

		// If IE event model is used
		} else if ( document.attachEvent ) {
			// ensure firing before onload,
			// maybe late but safe also for iframes
			document.attachEvent( "onreadystatechange", DOMContentLoaded );

			// A fallback to window.onload, that will always work
			window.attachEvent( "onload", jQuery.ready );

			// If IE and not a frame
			// continually check to see if the document is ready
			var toplevel = false;

			try {
				toplevel = window.frameElement == null;
			} catch(e) {}

			if ( document.documentElement.doScroll && toplevel ) {
				doScrollCheck();
			}
		}
	},

	// See test/unit/core.js for details concerning isFunction.
	// Since version 1.3, DOM methods and functions like alert
	// aren't supported. They return false on IE (#2968).
	isFunction: function( obj ) {
		return jQuery.type(obj) === "function";
	},

	isArray: Array.isArray || function( obj ) {
		return jQuery.type(obj) === "array";
	},

	isWindow: function( obj ) {
		return obj != null && obj == obj.window;
	},

	isNumeric: function( obj ) {
		return !isNaN( parseFloat(obj) ) && isFinite( obj );
	},

	type: function( obj ) {
		return obj == null ?
			String( obj ) :
			class2type[ toString.call(obj) ] || "object";
	},

	isPlainObject: function( obj ) {
		// Must be an Object.
		// Because of IE, we also have to check the presence of the constructor property.
		// Make sure that DOM nodes and window objects don't pass through, as well
		if ( !obj || jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow( obj ) ) {
			return false;
		}

		try {
			// Not own constructor property must be Object
			if ( obj.constructor &&
				!hasOwn.call(obj, "constructor") &&
				!hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
				return false;
			}
		} catch ( e ) {
			// IE8,9 Will throw exceptions on certain host objects #9897
			return false;
		}

		// Own properties are enumerated firstly, so to speed up,
		// if last one is own, then all properties are own.

		var key;
		for ( key in obj ) {}

		return key === undefined || hasOwn.call( obj, key );
	},

	isEmptyObject: function( obj ) {
		for ( var name in obj ) {
			return false;
		}
		return true;
	},

	error: function( msg ) {
		throw new Error( msg );
	},

	parseJSON: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}

		// Make sure leading/trailing whitespace is removed (IE can't handle it)
		data = jQuery.trim( data );

		// Attempt to parse using the native JSON parser first
		if ( window.JSON && window.JSON.parse ) {
			return window.JSON.parse( data );
		}

		// Make sure the incoming data is actual JSON
		// Logic borrowed from http://json.org/json2.js
		if ( rvalidchars.test( data.replace( rvalidescape, "@" )
			.replace( rvalidtokens, "]" )
			.replace( rvalidbraces, "")) ) {

			return ( new Function( "return " + data ) )();

		}
		jQuery.error( "Invalid JSON: " + data );
	},

	// Cross-browser xml parsing
	parseXML: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}
		var xml, tmp;
		try {
			if ( window.DOMParser ) { // Standard
				tmp = new DOMParser();
				xml = tmp.parseFromString( data , "text/xml" );
			} else { // IE
				xml = new ActiveXObject( "Microsoft.XMLDOM" );
				xml.async = "false";
				xml.loadXML( data );
			}
		} catch( e ) {
			xml = undefined;
		}
		if ( !xml || !xml.documentElement || xml.getElementsByTagName( "parsererror" ).length ) {
			jQuery.error( "Invalid XML: " + data );
		}
		return xml;
	},

	noop: function() {},

	// Evaluates a script in a global context
	// Workarounds based on findings by Jim Driscoll
	// http://weblogs.java.net/blog/driscoll/archive/2009/09/08/eval-javascript-global-context
	globalEval: function( data ) {
		if ( data && rnotwhite.test( data ) ) {
			// We use execScript on Internet Explorer
			// We use an anonymous function so that context is window
			// rather than jQuery in Firefox
			( window.execScript || function( data ) {
				window[ "eval" ].call( window, data );
			} )( data );
		}
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	nodeName: function( elem, name ) {
		return elem.nodeName && elem.nodeName.toUpperCase() === name.toUpperCase();
	},

	// args is for internal usage only
	each: function( object, callback, args ) {
		var name, i = 0,
			length = object.length,
			isObj = length === undefined || jQuery.isFunction( object );

		if ( args ) {
			if ( isObj ) {
				for ( name in object ) {
					if ( callback.apply( object[ name ], args ) === false ) {
						break;
					}
				}
			} else {
				for ( ; i < length; ) {
					if ( callback.apply( object[ i++ ], args ) === false ) {
						break;
					}
				}
			}

		// A special, fast, case for the most common use of each
		} else {
			if ( isObj ) {
				for ( name in object ) {
					if ( callback.call( object[ name ], name, object[ name ] ) === false ) {
						break;
					}
				}
			} else {
				for ( ; i < length; ) {
					if ( callback.call( object[ i ], i, object[ i++ ] ) === false ) {
						break;
					}
				}
			}
		}

		return object;
	},

	// Use native String.trim function wherever possible
	trim: trim ?
		function( text ) {
			return text == null ?
				"" :
				trim.call( text );
		} :

		// Otherwise use our own trimming functionality
		function( text ) {
			return text == null ?
				"" :
				text.toString().replace( trimLeft, "" ).replace( trimRight, "" );
		},

	// results is for internal usage only
	makeArray: function( array, results ) {
		var ret = results || [];

		if ( array != null ) {
			// The window, strings (and functions) also have 'length'
			// Tweaked logic slightly to handle Blackberry 4.7 RegExp issues #6930
			var type = jQuery.type( array );

			if ( array.length == null || type === "string" || type === "function" || type === "regexp" || jQuery.isWindow( array ) ) {
				push.call( ret, array );
			} else {
				jQuery.merge( ret, array );
			}
		}

		return ret;
	},

	inArray: function( elem, array, i ) {
		var len;

		if ( array ) {
			if ( indexOf ) {
				return indexOf.call( array, elem, i );
			}

			len = array.length;
			i = i ? i < 0 ? Math.max( 0, len + i ) : i : 0;

			for ( ; i < len; i++ ) {
				// Skip accessing in sparse arrays
				if ( i in array && array[ i ] === elem ) {
					return i;
				}
			}
		}

		return -1;
	},

	merge: function( first, second ) {
		var i = first.length,
			j = 0;

		if ( typeof second.length === "number" ) {
			for ( var l = second.length; j < l; j++ ) {
				first[ i++ ] = second[ j ];
			}

		} else {
			while ( second[j] !== undefined ) {
				first[ i++ ] = second[ j++ ];
			}
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, inv ) {
		var ret = [], retVal;
		inv = !!inv;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( var i = 0, length = elems.length; i < length; i++ ) {
			retVal = !!callback( elems[ i ], i );
			if ( inv !== retVal ) {
				ret.push( elems[ i ] );
			}
		}

		return ret;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var value, key, ret = [],
			i = 0,
			length = elems.length,
			// jquery objects are treated as arrays
			isArray = elems instanceof jQuery || length !== undefined && typeof length === "number" && ( ( length > 0 && elems[ 0 ] && elems[ length -1 ] ) || length === 0 || jQuery.isArray( elems ) ) ;

		// Go through the array, translating each of the items to their
		if ( isArray ) {
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret[ ret.length ] = value;
				}
			}

		// Go through every key on the object,
		} else {
			for ( key in elems ) {
				value = callback( elems[ key ], key, arg );

				if ( value != null ) {
					ret[ ret.length ] = value;
				}
			}
		}

		// Flatten any nested arrays
		return ret.concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		if ( typeof context === "string" ) {
			var tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		var args = slice.call( arguments, 2 ),
			proxy = function() {
				return fn.apply( context, args.concat( slice.call( arguments ) ) );
			};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || proxy.guid || jQuery.guid++;

		return proxy;
	},

	// Mutifunctional method to get and set values to a collection
	// The value/s can optionally be executed if it's a function
	access: function( elems, fn, key, value, chainable, emptyGet, pass ) {
		var exec,
			bulk = key == null,
			i = 0,
			length = elems.length;

		// Sets many values
		if ( key && typeof key === "object" ) {
			for ( i in key ) {
				jQuery.access( elems, fn, i, key[i], 1, emptyGet, value );
			}
			chainable = 1;

		// Sets one value
		} else if ( value !== undefined ) {
			// Optionally, function values get executed if exec is true
			exec = pass === undefined && jQuery.isFunction( value );

			if ( bulk ) {
				// Bulk operations only iterate when executing function values
				if ( exec ) {
					exec = fn;
					fn = function( elem, key, value ) {
						return exec.call( jQuery( elem ), value );
					};

				// Otherwise they run against the entire set
				} else {
					fn.call( elems, value );
					fn = null;
				}
			}

			if ( fn ) {
				for (; i < length; i++ ) {
					fn( elems[i], key, exec ? value.call( elems[i], i, fn( elems[i], key ) ) : value, pass );
				}
			}

			chainable = 1;
		}

		return chainable ?
			elems :

			// Gets
			bulk ?
				fn.call( elems ) :
				length ? fn( elems[0], key ) : emptyGet;
	},

	now: function() {
		return ( new Date() ).getTime();
	},

	// Use of jQuery.browser is frowned upon.
	// More details: http://docs.jquery.com/Utilities/jQuery.browser
	uaMatch: function( ua ) {
		ua = ua.toLowerCase();

		var match = rwebkit.exec( ua ) ||
			ropera.exec( ua ) ||
			rmsie.exec( ua ) ||
			ua.indexOf("compatible") < 0 && rmozilla.exec( ua ) ||
			[];

		return { browser: match[1] || "", version: match[2] || "0" };
	},

	sub: function() {
		function jQuerySub( selector, context ) {
			return new jQuerySub.fn.init( selector, context );
		}
		jQuery.extend( true, jQuerySub, this );
		jQuerySub.superclass = this;
		jQuerySub.fn = jQuerySub.prototype = this();
		jQuerySub.fn.constructor = jQuerySub;
		jQuerySub.sub = this.sub;
		jQuerySub.fn.init = function init( selector, context ) {
			if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
				context = jQuerySub( context );
			}

			return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
		};
		jQuerySub.fn.init.prototype = jQuerySub.fn;
		var rootjQuerySub = jQuerySub(document);
		return jQuerySub;
	},

	browser: {}
});

// Populate the class2type map
jQuery.each("Boolean Number String Function Array Date RegExp Object".split(" "), function(i, name) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
});

browserMatch = jQuery.uaMatch( userAgent );
if ( browserMatch.browser ) {
	jQuery.browser[ browserMatch.browser ] = true;
	jQuery.browser.version = browserMatch.version;
}

// Deprecated, use jQuery.browser.webkit instead
if ( jQuery.browser.webkit ) {
	jQuery.browser.safari = true;
}

// IE doesn't match non-breaking spaces with \s
if ( rnotwhite.test( "\xA0" ) ) {
	trimLeft = /^[\s\xA0]+/;
	trimRight = /[\s\xA0]+$/;
}

// All jQuery objects should point back to these
rootjQuery = jQuery(document);

// Cleanup functions for the document ready method
if ( document.addEventListener ) {
	DOMContentLoaded = function() {
		document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
		jQuery.ready();
	};

} else if ( document.attachEvent ) {
	DOMContentLoaded = function() {
		// Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
		if ( document.readyState === "complete" ) {
			document.detachEvent( "onreadystatechange", DOMContentLoaded );
			jQuery.ready();
		}
	};
}

// The DOM ready check for Internet Explorer
function doScrollCheck() {
	if ( jQuery.isReady ) {
		return;
	}

	try {
		// If IE is used, use the trick by Diego Perini
		// http://javascript.nwbox.com/IEContentLoaded/
		document.documentElement.doScroll("left");
	} catch(e) {
		setTimeout( doScrollCheck, 1 );
		return;
	}

	// and execute any waiting functions
	jQuery.ready();
}

return jQuery;

})();


// String to Object flags format cache
var flagsCache = {};

// Convert String-formatted flags into Object-formatted ones and store in cache
function createFlags( flags ) {
	var object = flagsCache[ flags ] = {},
		i, length;
	flags = flags.split( /\s+/ );
	for ( i = 0, length = flags.length; i < length; i++ ) {
		object[ flags[i] ] = true;
	}
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	flags:	an optional list of space-separated flags that will change how
 *			the callback list behaves
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible flags:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( flags ) {

	// Convert flags from String-formatted to Object-formatted
	// (we check in cache first)
	flags = flags ? ( flagsCache[ flags ] || createFlags( flags ) ) : {};

	var // Actual callback list
		list = [],
		// Stack of fire calls for repeatable lists
		stack = [],
		// Last fire value (for non-forgettable lists)
		memory,
		// Flag to know if list was already fired
		fired,
		// Flag to know if list is currently firing
		firing,
		// First callback to fire (used internally by add and fireWith)
		firingStart,
		// End of the loop when firing
		firingLength,
		// Index of currently firing callback (modified by remove if needed)
		firingIndex,
		// Add one or several callbacks to the list
		add = function( args ) {
			var i,
				length,
				elem,
				type,
				actual;
			for ( i = 0, length = args.length; i < length; i++ ) {
				elem = args[ i ];
				type = jQuery.type( elem );
				if ( type === "array" ) {
					// Inspect recursively
					add( elem );
				} else if ( type === "function" ) {
					// Add if not in unique mode and callback is not in
					if ( !flags.unique || !self.has( elem ) ) {
						list.push( elem );
					}
				}
			}
		},
		// Fire callbacks
		fire = function( context, args ) {
			args = args || [];
			memory = !flags.memory || [ context, args ];
			fired = true;
			firing = true;
			firingIndex = firingStart || 0;
			firingStart = 0;
			firingLength = list.length;
			for ( ; list && firingIndex < firingLength; firingIndex++ ) {
				if ( list[ firingIndex ].apply( context, args ) === false && flags.stopOnFalse ) {
					memory = true; // Mark as halted
					break;
				}
			}
			firing = false;
			if ( list ) {
				if ( !flags.once ) {
					if ( stack && stack.length ) {
						memory = stack.shift();
						self.fireWith( memory[ 0 ], memory[ 1 ] );
					}
				} else if ( memory === true ) {
					self.disable();
				} else {
					list = [];
				}
			}
		},
		// Actual Callbacks object
		self = {
			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {
					var length = list.length;
					add( arguments );
					// Do we need to add the callbacks to the
					// current firing batch?
					if ( firing ) {
						firingLength = list.length;
					// With memory, if we're not firing then
					// we should call right away, unless previous
					// firing was halted (stopOnFalse)
					} else if ( memory && memory !== true ) {
						firingStart = length;
						fire( memory[ 0 ], memory[ 1 ] );
					}
				}
				return this;
			},
			// Remove a callback from the list
			remove: function() {
				if ( list ) {
					var args = arguments,
						argIndex = 0,
						argLength = args.length;
					for ( ; argIndex < argLength ; argIndex++ ) {
						for ( var i = 0; i < list.length; i++ ) {
							if ( args[ argIndex ] === list[ i ] ) {
								// Handle firingIndex and firingLength
								if ( firing ) {
									if ( i <= firingLength ) {
										firingLength--;
										if ( i <= firingIndex ) {
											firingIndex--;
										}
									}
								}
								// Remove the element
								list.splice( i--, 1 );
								// If we have some unicity property then
								// we only need to do this once
								if ( flags.unique ) {
									break;
								}
							}
						}
					}
				}
				return this;
			},
			// Control if a given callback is in the list
			has: function( fn ) {
				if ( list ) {
					var i = 0,
						length = list.length;
					for ( ; i < length; i++ ) {
						if ( fn === list[ i ] ) {
							return true;
						}
					}
				}
				return false;
			},
			// Remove all callbacks from the list
			empty: function() {
				list = [];
				return this;
			},
			// Have the list do nothing anymore
			disable: function() {
				list = stack = memory = undefined;
				return this;
			},
			// Is it disabled?
			disabled: function() {
				return !list;
			},
			// Lock the list in its current state
			lock: function() {
				stack = undefined;
				if ( !memory || memory === true ) {
					self.disable();
				}
				return this;
			},
			// Is it locked?
			locked: function() {
				return !stack;
			},
			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( stack ) {
					if ( firing ) {
						if ( !flags.once ) {
							stack.push( [ context, args ] );
						}
					} else if ( !( flags.once && memory ) ) {
						fire( context, args );
					}
				}
				return this;
			},
			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},
			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};




var // Static reference to slice
	sliceDeferred = [].slice;

jQuery.extend({

	Deferred: function( func ) {
		var doneList = jQuery.Callbacks( "once memory" ),
			failList = jQuery.Callbacks( "once memory" ),
			progressList = jQuery.Callbacks( "memory" ),
			state = "pending",
			lists = {
				resolve: doneList,
				reject: failList,
				notify: progressList
			},
			promise = {
				done: doneList.add,
				fail: failList.add,
				progress: progressList.add,

				state: function() {
					return state;
				},

				// Deprecated
				isResolved: doneList.fired,
				isRejected: failList.fired,

				then: function( doneCallbacks, failCallbacks, progressCallbacks ) {
					deferred.done( doneCallbacks ).fail( failCallbacks ).progress( progressCallbacks );
					return this;
				},
				always: function() {
					deferred.done.apply( deferred, arguments ).fail.apply( deferred, arguments );
					return this;
				},
				pipe: function( fnDone, fnFail, fnProgress ) {
					return jQuery.Deferred(function( newDefer ) {
						jQuery.each( {
							done: [ fnDone, "resolve" ],
							fail: [ fnFail, "reject" ],
							progress: [ fnProgress, "notify" ]
						}, function( handler, data ) {
							var fn = data[ 0 ],
								action = data[ 1 ],
								returned;
							if ( jQuery.isFunction( fn ) ) {
								deferred[ handler ](function() {
									returned = fn.apply( this, arguments );
									if ( returned && jQuery.isFunction( returned.promise ) ) {
										returned.promise().then( newDefer.resolve, newDefer.reject, newDefer.notify );
									} else {
										newDefer[ action + "With" ]( this === deferred ? newDefer : this, [ returned ] );
									}
								});
							} else {
								deferred[ handler ]( newDefer[ action ] );
							}
						});
					}).promise();
				},
				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					if ( obj == null ) {
						obj = promise;
					} else {
						for ( var key in promise ) {
							obj[ key ] = promise[ key ];
						}
					}
					return obj;
				}
			},
			deferred = promise.promise({}),
			key;

		for ( key in lists ) {
			deferred[ key ] = lists[ key ].fire;
			deferred[ key + "With" ] = lists[ key ].fireWith;
		}

		// Handle state
		deferred.done( function() {
			state = "resolved";
		}, failList.disable, progressList.lock ).fail( function() {
			state = "rejected";
		}, doneList.disable, progressList.lock );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( firstParam ) {
		var args = sliceDeferred.call( arguments, 0 ),
			i = 0,
			length = args.length,
			pValues = new Array( length ),
			count = length,
			pCount = length,
			deferred = length <= 1 && firstParam && jQuery.isFunction( firstParam.promise ) ?
				firstParam :
				jQuery.Deferred(),
			promise = deferred.promise();
		function resolveFunc( i ) {
			return function( value ) {
				args[ i ] = arguments.length > 1 ? sliceDeferred.call( arguments, 0 ) : value;
				if ( !( --count ) ) {
					deferred.resolveWith( deferred, args );
				}
			};
		}
		function progressFunc( i ) {
			return function( value ) {
				pValues[ i ] = arguments.length > 1 ? sliceDeferred.call( arguments, 0 ) : value;
				deferred.notifyWith( promise, pValues );
			};
		}
		if ( length > 1 ) {
			for ( ; i < length; i++ ) {
				if ( args[ i ] && args[ i ].promise && jQuery.isFunction( args[ i ].promise ) ) {
					args[ i ].promise().then( resolveFunc(i), deferred.reject, progressFunc(i) );
				} else {
					--count;
				}
			}
			if ( !count ) {
				deferred.resolveWith( deferred, args );
			}
		} else if ( deferred !== firstParam ) {
			deferred.resolveWith( deferred, length ? [ firstParam ] : [] );
		}
		return promise;
	}
});




jQuery.support = (function() {

	var support,
		all,
		a,
		select,
		opt,
		input,
		fragment,
		tds,
		events,
		eventName,
		i,
		isSupported,
		div = document.createElement( "div" ),
		documentElement = document.documentElement;

	// Preliminary tests
	div.setAttribute("className", "t");
	div.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>";

	all = div.getElementsByTagName( "*" );
	a = div.getElementsByTagName( "a" )[ 0 ];

	// Can't get basic test support
	if ( !all || !all.length || !a ) {
		return {};
	}

	// First batch of supports tests
	select = document.createElement( "select" );
	opt = select.appendChild( document.createElement("option") );
	input = div.getElementsByTagName( "input" )[ 0 ];

	support = {
		// IE strips leading whitespace when .innerHTML is used
		leadingWhitespace: ( div.firstChild.nodeType === 3 ),

		// Make sure that tbody elements aren't automatically inserted
		// IE will insert them into empty tables
		tbody: !div.getElementsByTagName("tbody").length,

		// Make sure that link elements get serialized correctly by innerHTML
		// This requires a wrapper element in IE
		htmlSerialize: !!div.getElementsByTagName("link").length,

		// Get the style information from getAttribute
		// (IE uses .cssText instead)
		style: /top/.test( a.getAttribute("style") ),

		// Make sure that URLs aren't manipulated
		// (IE normalizes it by default)
		hrefNormalized: ( a.getAttribute("href") === "/a" ),

		// Make sure that element opacity exists
		// (IE uses filter instead)
		// Use a regex to work around a WebKit issue. See #5145
		opacity: /^0.55/.test( a.style.opacity ),

		// Verify style float existence
		// (IE uses styleFloat instead of cssFloat)
		cssFloat: !!a.style.cssFloat,

		// Make sure that if no value is specified for a checkbox
		// that it defaults to "on".
		// (WebKit defaults to "" instead)
		checkOn: ( input.value === "on" ),

		// Make sure that a selected-by-default option has a working selected property.
		// (WebKit defaults to false instead of true, IE too, if it's in an optgroup)
		optSelected: opt.selected,

		// Test setAttribute on camelCase class. If it works, we need attrFixes when doing get/setAttribute (ie6/7)
		getSetAttribute: div.className !== "t",

		// Tests for enctype support on a form(#6743)
		enctype: !!document.createElement("form").enctype,

		// Makes sure cloning an html5 element does not cause problems
		// Where outerHTML is undefined, this still works
		html5Clone: document.createElement("nav").cloneNode( true ).outerHTML !== "<:nav></:nav>",

		// Will be defined later
		submitBubbles: true,
		changeBubbles: true,
		focusinBubbles: false,
		deleteExpando: true,
		noCloneEvent: true,
		inlineBlockNeedsLayout: false,
		shrinkWrapBlocks: false,
		reliableMarginRight: true,
		pixelMargin: true
	};

	// jQuery.boxModel DEPRECATED in 1.3, use jQuery.support.boxModel instead
	jQuery.boxModel = support.boxModel = (document.compatMode === "CSS1Compat");

	// Make sure checked status is properly cloned
	input.checked = true;
	support.noCloneChecked = input.cloneNode( true ).checked;

	// Make sure that the options inside disabled selects aren't marked as disabled
	// (WebKit marks them as disabled)
	select.disabled = true;
	support.optDisabled = !opt.disabled;

	// Test to see if it's possible to delete an expando from an element
	// Fails in Internet Explorer
	try {
		delete div.test;
	} catch( e ) {
		support.deleteExpando = false;
	}

	if ( !div.addEventListener && div.attachEvent && div.fireEvent ) {
		div.attachEvent( "onclick", function() {
			// Cloning a node shouldn't copy over any
			// bound event handlers (IE does this)
			support.noCloneEvent = false;
		});
		div.cloneNode( true ).fireEvent( "onclick" );
	}

	// Check if a radio maintains its value
	// after being appended to the DOM
	input = document.createElement("input");
	input.value = "t";
	input.setAttribute("type", "radio");
	support.radioValue = input.value === "t";

	input.setAttribute("checked", "checked");

	// #11217 - WebKit loses check when the name is after the checked attribute
	input.setAttribute( "name", "t" );

	div.appendChild( input );
	fragment = document.createDocumentFragment();
	fragment.appendChild( div.lastChild );

	// WebKit doesn't clone checked state correctly in fragments
	support.checkClone = fragment.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Check if a disconnected checkbox will retain its checked
	// value of true after appended to the DOM (IE6/7)
	support.appendChecked = input.checked;

	fragment.removeChild( input );
	fragment.appendChild( div );

	// Technique from Juriy Zaytsev
	// http://perfectionkills.com/detecting-event-support-without-browser-sniffing/
	// We only care about the case where non-standard event systems
	// are used, namely in IE. Short-circuiting here helps us to
	// avoid an eval call (in setAttribute) which can cause CSP
	// to go haywire. See: https://developer.mozilla.org/en/Security/CSP
	if ( div.attachEvent ) {
		for ( i in {
			submit: 1,
			change: 1,
			focusin: 1
		}) {
			eventName = "on" + i;
			isSupported = ( eventName in div );
			if ( !isSupported ) {
				div.setAttribute( eventName, "return;" );
				isSupported = ( typeof div[ eventName ] === "function" );
			}
			support[ i + "Bubbles" ] = isSupported;
		}
	}

	fragment.removeChild( div );

	// Null elements to avoid leaks in IE
	fragment = select = opt = div = input = null;

	// Run tests that need a body at doc ready
	jQuery(function() {
		var container, outer, inner, table, td, offsetSupport,
			marginDiv, conMarginTop, style, html, positionTopLeftWidthHeight,
			paddingMarginBorderVisibility, paddingMarginBorder,
			body = document.getElementsByTagName("body")[0];

		if ( !body ) {
			// Return for frameset docs that don't have a body
			return;
		}

		conMarginTop = 1;
		paddingMarginBorder = "padding:0;margin:0;border:";
		positionTopLeftWidthHeight = "position:absolute;top:0;left:0;width:1px;height:1px;";
		paddingMarginBorderVisibility = paddingMarginBorder + "0;visibility:hidden;";
		style = "style='" + positionTopLeftWidthHeight + paddingMarginBorder + "5px solid #000;";
		html = "<div " + style + "display:block;'><div style='" + paddingMarginBorder + "0;display:block;overflow:hidden;'></div></div>" +
			"<table " + style + "' cellpadding='0' cellspacing='0'>" +
			"<tr><td></td></tr></table>";

		container = document.createElement("div");
		container.style.cssText = paddingMarginBorderVisibility + "width:0;height:0;position:static;top:0;margin-top:" + conMarginTop + "px";
		body.insertBefore( container, body.firstChild );

		// Construct the test element
		div = document.createElement("div");
		container.appendChild( div );

		// Check if table cells still have offsetWidth/Height when they are set
		// to display:none and there are still other visible table cells in a
		// table row; if so, offsetWidth/Height are not reliable for use when
		// determining if an element has been hidden directly using
		// display:none (it is still safe to use offsets if a parent element is
		// hidden; don safety goggles and see bug #4512 for more information).
		// (only IE 8 fails this test)
		div.innerHTML = "<table><tr><td style='" + paddingMarginBorder + "0;display:none'></td><td>t</td></tr></table>";
		tds = div.getElementsByTagName( "td" );
		isSupported = ( tds[ 0 ].offsetHeight === 0 );

		tds[ 0 ].style.display = "";
		tds[ 1 ].style.display = "none";

		// Check if empty table cells still have offsetWidth/Height
		// (IE <= 8 fail this test)
		support.reliableHiddenOffsets = isSupported && ( tds[ 0 ].offsetHeight === 0 );

		// Check if div with explicit width and no margin-right incorrectly
		// gets computed margin-right based on width of container. For more
		// info see bug #3333
		// Fails in WebKit before Feb 2011 nightlies
		// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
		if ( window.getComputedStyle ) {
			div.innerHTML = "";
			marginDiv = document.createElement( "div" );
			marginDiv.style.width = "0";
			marginDiv.style.marginRight = "0";
			div.style.width = "2px";
			div.appendChild( marginDiv );
			support.reliableMarginRight =
				( parseInt( ( window.getComputedStyle( marginDiv, null ) || { marginRight: 0 } ).marginRight, 10 ) || 0 ) === 0;
		}

		if ( typeof div.style.zoom !== "undefined" ) {
			// Check if natively block-level elements act like inline-block
			// elements when setting their display to 'inline' and giving
			// them layout
			// (IE < 8 does this)
			div.innerHTML = "";
			div.style.width = div.style.padding = "1px";
			div.style.border = 0;
			div.style.overflow = "hidden";
			div.style.display = "inline";
			div.style.zoom = 1;
			support.inlineBlockNeedsLayout = ( div.offsetWidth === 3 );

			// Check if elements with layout shrink-wrap their children
			// (IE 6 does this)
			div.style.display = "block";
			div.style.overflow = "visible";
			div.innerHTML = "<div style='width:5px;'></div>";
			support.shrinkWrapBlocks = ( div.offsetWidth !== 3 );
		}

		div.style.cssText = positionTopLeftWidthHeight + paddingMarginBorderVisibility;
		div.innerHTML = html;

		outer = div.firstChild;
		inner = outer.firstChild;
		td = outer.nextSibling.firstChild.firstChild;

		offsetSupport = {
			doesNotAddBorder: ( inner.offsetTop !== 5 ),
			doesAddBorderForTableAndCells: ( td.offsetTop === 5 )
		};

		inner.style.position = "fixed";
		inner.style.top = "20px";

		// safari subtracts parent border width here which is 5px
		offsetSupport.fixedPosition = ( inner.offsetTop === 20 || inner.offsetTop === 15 );
		inner.style.position = inner.style.top = "";

		outer.style.overflow = "hidden";
		outer.style.position = "relative";

		offsetSupport.subtractsBorderForOverflowNotVisible = ( inner.offsetTop === -5 );
		offsetSupport.doesNotIncludeMarginInBodyOffset = ( body.offsetTop !== conMarginTop );

		if ( window.getComputedStyle ) {
			div.style.marginTop = "1%";
			support.pixelMargin = ( window.getComputedStyle( div, null ) || { marginTop: 0 } ).marginTop !== "1%";
		}

		if ( typeof container.style.zoom !== "undefined" ) {
			container.style.zoom = 1;
		}

		body.removeChild( container );
		marginDiv = div = container = null;

		jQuery.extend( support, offsetSupport );
	});

	return support;
})();




var rbrace = /^(?:\{.*\}|\[.*\])$/,
	rmultiDash = /([A-Z])/g;

jQuery.extend({
	cache: {},

	// Please use with caution
	uuid: 0,

	// Unique for each copy of jQuery on the page
	// Non-digits removed to match rinlinejQuery
	expando: "jQuery" + ( jQuery.fn.jquery + Math.random() ).replace( /\D/g, "" ),

	// The following elements throw uncatchable exceptions if you
	// attempt to add expando properties to them.
	noData: {
		"embed": true,
		// Ban all objects except for Flash (which handle expandos)
		"object": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
		"applet": true
	},

	hasData: function( elem ) {
		elem = elem.nodeType ? jQuery.cache[ elem[jQuery.expando] ] : elem[ jQuery.expando ];
		return !!elem && !isEmptyDataObject( elem );
	},

	data: function( elem, name, data, pvt /* Internal Use Only */ ) {
		if ( !jQuery.acceptData( elem ) ) {
			return;
		}

		var privateCache, thisCache, ret,
			internalKey = jQuery.expando,
			getByName = typeof name === "string",

			// We have to handle DOM nodes and JS objects differently because IE6-7
			// can't GC object references properly across the DOM-JS boundary
			isNode = elem.nodeType,

			// Only DOM nodes need the global jQuery cache; JS object data is
			// attached directly to the object so GC can occur automatically
			cache = isNode ? jQuery.cache : elem,

			// Only defining an ID for JS objects if its cache already exists allows
			// the code to shortcut on the same path as a DOM node with no cache
			id = isNode ? elem[ internalKey ] : elem[ internalKey ] && internalKey,
			isEvents = name === "events";

		// Avoid doing any more work than we need to when trying to get data on an
		// object that has no data at all
		if ( (!id || !cache[id] || (!isEvents && !pvt && !cache[id].data)) && getByName && data === undefined ) {
			return;
		}

		if ( !id ) {
			// Only DOM nodes need a new unique ID for each element since their data
			// ends up in the global cache
			if ( isNode ) {
				elem[ internalKey ] = id = ++jQuery.uuid;
			} else {
				id = internalKey;
			}
		}

		if ( !cache[ id ] ) {
			cache[ id ] = {};

			// Avoids exposing jQuery metadata on plain JS objects when the object
			// is serialized using JSON.stringify
			if ( !isNode ) {
				cache[ id ].toJSON = jQuery.noop;
			}
		}

		// An object can be passed to jQuery.data instead of a key/value pair; this gets
		// shallow copied over onto the existing cache
		if ( typeof name === "object" || typeof name === "function" ) {
			if ( pvt ) {
				cache[ id ] = jQuery.extend( cache[ id ], name );
			} else {
				cache[ id ].data = jQuery.extend( cache[ id ].data, name );
			}
		}

		privateCache = thisCache = cache[ id ];

		// jQuery data() is stored in a separate object inside the object's internal data
		// cache in order to avoid key collisions between internal data and user-defined
		// data.
		if ( !pvt ) {
			if ( !thisCache.data ) {
				thisCache.data = {};
			}

			thisCache = thisCache.data;
		}

		if ( data !== undefined ) {
			thisCache[ jQuery.camelCase( name ) ] = data;
		}

		// Users should not attempt to inspect the internal events object using jQuery.data,
		// it is undocumented and subject to change. But does anyone listen? No.
		if ( isEvents && !thisCache[ name ] ) {
			return privateCache.events;
		}

		// Check for both converted-to-camel and non-converted data property names
		// If a data property was specified
		if ( getByName ) {

			// First Try to find as-is property data
			ret = thisCache[ name ];

			// Test for null|undefined property data
			if ( ret == null ) {

				// Try to find the camelCased property
				ret = thisCache[ jQuery.camelCase( name ) ];
			}
		} else {
			ret = thisCache;
		}

		return ret;
	},

	removeData: function( elem, name, pvt /* Internal Use Only */ ) {
		if ( !jQuery.acceptData( elem ) ) {
			return;
		}

		var thisCache, i, l,

			// Reference to internal data cache key
			internalKey = jQuery.expando,

			isNode = elem.nodeType,

			// See jQuery.data for more information
			cache = isNode ? jQuery.cache : elem,

			// See jQuery.data for more information
			id = isNode ? elem[ internalKey ] : internalKey;

		// If there is already no cache entry for this object, there is no
		// purpose in continuing
		if ( !cache[ id ] ) {
			return;
		}

		if ( name ) {

			thisCache = pvt ? cache[ id ] : cache[ id ].data;

			if ( thisCache ) {

				// Support array or space separated string names for data keys
				if ( !jQuery.isArray( name ) ) {

					// try the string as a key before any manipulation
					if ( name in thisCache ) {
						name = [ name ];
					} else {

						// split the camel cased version by spaces unless a key with the spaces exists
						name = jQuery.camelCase( name );
						if ( name in thisCache ) {
							name = [ name ];
						} else {
							name = name.split( " " );
						}
					}
				}

				for ( i = 0, l = name.length; i < l; i++ ) {
					delete thisCache[ name[i] ];
				}

				// If there is no data left in the cache, we want to continue
				// and let the cache object itself get destroyed
				if ( !( pvt ? isEmptyDataObject : jQuery.isEmptyObject )( thisCache ) ) {
					return;
				}
			}
		}

		// See jQuery.data for more information
		if ( !pvt ) {
			delete cache[ id ].data;

			// Don't destroy the parent cache unless the internal data object
			// had been the only thing left in it
			if ( !isEmptyDataObject(cache[ id ]) ) {
				return;
			}
		}

		// Browsers that fail expando deletion also refuse to delete expandos on
		// the window, but it will allow it on all other JS objects; other browsers
		// don't care
		// Ensure that `cache` is not a window object #10080
		if ( jQuery.support.deleteExpando || !cache.setInterval ) {
			delete cache[ id ];
		} else {
			cache[ id ] = null;
		}

		// We destroyed the cache and need to eliminate the expando on the node to avoid
		// false lookups in the cache for entries that no longer exist
		if ( isNode ) {
			// IE does not allow us to delete expando properties from nodes,
			// nor does it have a removeAttribute function on Document nodes;
			// we must handle all of these cases
			if ( jQuery.support.deleteExpando ) {
				delete elem[ internalKey ];
			} else if ( elem.removeAttribute ) {
				elem.removeAttribute( internalKey );
			} else {
				elem[ internalKey ] = null;
			}
		}
	},

	// For internal use only.
	_data: function( elem, name, data ) {
		return jQuery.data( elem, name, data, true );
	},

	// A method for determining if a DOM node can handle the data expando
	acceptData: function( elem ) {
		if ( elem.nodeName ) {
			var match = jQuery.noData[ elem.nodeName.toLowerCase() ];

			if ( match ) {
				return !(match === true || elem.getAttribute("classid") !== match);
			}
		}

		return true;
	}
});

jQuery.fn.extend({
	data: function( key, value ) {
		var parts, part, attr, name, l,
			elem = this[0],
			i = 0,
			data = null;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = jQuery.data( elem );

				if ( elem.nodeType === 1 && !jQuery._data( elem, "parsedAttrs" ) ) {
					attr = elem.attributes;
					for ( l = attr.length; i < l; i++ ) {
						name = attr[i].name;

						if ( name.indexOf( "data-" ) === 0 ) {
							name = jQuery.camelCase( name.substring(5) );

							dataAttr( elem, name, data[ name ] );
						}
					}
					jQuery._data( elem, "parsedAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each(function() {
				jQuery.data( this, key );
			});
		}

		parts = key.split( ".", 2 );
		parts[1] = parts[1] ? "." + parts[1] : "";
		part = parts[1] + "!";

		return jQuery.access( this, function( value ) {

			if ( value === undefined ) {
				data = this.triggerHandler( "getData" + part, [ parts[0] ] );

				// Try to fetch any internally stored data first
				if ( data === undefined && elem ) {
					data = jQuery.data( elem, key );
					data = dataAttr( elem, key, data );
				}

				return data === undefined && parts[1] ?
					this.data( parts[0] ) :
					data;
			}

			parts[1] = value;
			this.each(function() {
				var self = jQuery( this );

				self.triggerHandler( "setData" + part, parts );
				jQuery.data( this, key, value );
				self.triggerHandler( "changeData" + part, parts );
			});
		}, null, value, arguments.length > 1, null, false );
	},

	removeData: function( key ) {
		return this.each(function() {
			jQuery.removeData( this, key );
		});
	}
});

function dataAttr( elem, key, data ) {
	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {

		var name = "data-" + key.replace( rmultiDash, "-$1" ).toLowerCase();

		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = data === "true" ? true :
				data === "false" ? false :
				data === "null" ? null :
				jQuery.isNumeric( data ) ? +data :
					rbrace.test( data ) ? jQuery.parseJSON( data ) :
					data;
			} catch( e ) {}

			// Make sure we set the data so it isn't changed later
			jQuery.data( elem, key, data );

		} else {
			data = undefined;
		}
	}

	return data;
}

// checks a cache object for emptiness
function isEmptyDataObject( obj ) {
	for ( var name in obj ) {

		// if the public data object is empty, the private is still empty
		if ( name === "data" && jQuery.isEmptyObject( obj[name] ) ) {
			continue;
		}
		if ( name !== "toJSON" ) {
			return false;
		}
	}

	return true;
}




function handleQueueMarkDefer( elem, type, src ) {
	var deferDataKey = type + "defer",
		queueDataKey = type + "queue",
		markDataKey = type + "mark",
		defer = jQuery._data( elem, deferDataKey );
	if ( defer &&
		( src === "queue" || !jQuery._data(elem, queueDataKey) ) &&
		( src === "mark" || !jQuery._data(elem, markDataKey) ) ) {
		// Give room for hard-coded callbacks to fire first
		// and eventually mark/queue something else on the element
		setTimeout( function() {
			if ( !jQuery._data( elem, queueDataKey ) &&
				!jQuery._data( elem, markDataKey ) ) {
				jQuery.removeData( elem, deferDataKey, true );
				defer.fire();
			}
		}, 0 );
	}
}

jQuery.extend({

	_mark: function( elem, type ) {
		if ( elem ) {
			type = ( type || "fx" ) + "mark";
			jQuery._data( elem, type, (jQuery._data( elem, type ) || 0) + 1 );
		}
	},

	_unmark: function( force, elem, type ) {
		if ( force !== true ) {
			type = elem;
			elem = force;
			force = false;
		}
		if ( elem ) {
			type = type || "fx";
			var key = type + "mark",
				count = force ? 0 : ( (jQuery._data( elem, key ) || 1) - 1 );
			if ( count ) {
				jQuery._data( elem, key, count );
			} else {
				jQuery.removeData( elem, key, true );
				handleQueueMarkDefer( elem, type, "mark" );
			}
		}
	},

	queue: function( elem, type, data ) {
		var q;
		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			q = jQuery._data( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !q || jQuery.isArray(data) ) {
					q = jQuery._data( elem, type, jQuery.makeArray(data) );
				} else {
					q.push( data );
				}
			}
			return q || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			fn = queue.shift(),
			hooks = {};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
		}

		if ( fn ) {
			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			jQuery._data( elem, type + ".run", hooks );
			fn.call( elem, function() {
				jQuery.dequeue( elem, type );
			}, hooks );
		}

		if ( !queue.length ) {
			jQuery.removeData( elem, type + "queue " + type + ".run", true );
			handleQueueMarkDefer( elem, type, "queue" );
		}
	}
});

jQuery.fn.extend({
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[0], type );
		}

		return data === undefined ?
			this :
			this.each(function() {
				var queue = jQuery.queue( this, type, data );

				if ( type === "fx" && queue[0] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			});
	},
	dequeue: function( type ) {
		return this.each(function() {
			jQuery.dequeue( this, type );
		});
	},
	// Based off of the plugin by Clint Helfers, with permission.
	// http://blindsignals.com/index.php/2009/07/jquery-delay/
	delay: function( time, type ) {
		time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
		type = type || "fx";

		return this.queue( type, function( next, hooks ) {
			var timeout = setTimeout( next, time );
			hooks.stop = function() {
				clearTimeout( timeout );
			};
		});
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},
	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, object ) {
		if ( typeof type !== "string" ) {
			object = type;
			type = undefined;
		}
		type = type || "fx";
		var defer = jQuery.Deferred(),
			elements = this,
			i = elements.length,
			count = 1,
			deferDataKey = type + "defer",
			queueDataKey = type + "queue",
			markDataKey = type + "mark",
			tmp;
		function resolve() {
			if ( !( --count ) ) {
				defer.resolveWith( elements, [ elements ] );
			}
		}
		while( i-- ) {
			if (( tmp = jQuery.data( elements[ i ], deferDataKey, undefined, true ) ||
					( jQuery.data( elements[ i ], queueDataKey, undefined, true ) ||
						jQuery.data( elements[ i ], markDataKey, undefined, true ) ) &&
					jQuery.data( elements[ i ], deferDataKey, jQuery.Callbacks( "once memory" ), true ) )) {
				count++;
				tmp.add( resolve );
			}
		}
		resolve();
		return defer.promise( object );
	}
});




var rclass = /[\n\t\r]/g,
	rspace = /\s+/,
	rreturn = /\r/g,
	rtype = /^(?:button|input)$/i,
	rfocusable = /^(?:button|input|object|select|textarea)$/i,
	rclickable = /^a(?:rea)?$/i,
	rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
	getSetAttribute = jQuery.support.getSetAttribute,
	nodeHook, boolHook, fixSpecified;

jQuery.fn.extend({
	attr: function( name, value ) {
		return jQuery.access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each(function() {
			jQuery.removeAttr( this, name );
		});
	},

	prop: function( name, value ) {
		return jQuery.access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		name = jQuery.propFix[ name ] || name;
		return this.each(function() {
			// try/catch handles cases where IE balks (such as removing a property on window)
			try {
				this[ name ] = undefined;
				delete this[ name ];
			} catch( e ) {}
		});
	},

	addClass: function( value ) {
		var classNames, i, l, elem,
			setClass, c, cl;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).addClass( value.call(this, j, this.className) );
			});
		}

		if ( value && typeof value === "string" ) {
			classNames = value.split( rspace );

			for ( i = 0, l = this.length; i < l; i++ ) {
				elem = this[ i ];

				if ( elem.nodeType === 1 ) {
					if ( !elem.className && classNames.length === 1 ) {
						elem.className = value;

					} else {
						setClass = " " + elem.className + " ";

						for ( c = 0, cl = classNames.length; c < cl; c++ ) {
							if ( !~setClass.indexOf( " " + classNames[ c ] + " " ) ) {
								setClass += classNames[ c ] + " ";
							}
						}
						elem.className = jQuery.trim( setClass );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classNames, i, l, elem, className, c, cl;

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( j ) {
				jQuery( this ).removeClass( value.call(this, j, this.className) );
			});
		}

		if ( (value && typeof value === "string") || value === undefined ) {
			classNames = ( value || "" ).split( rspace );

			for ( i = 0, l = this.length; i < l; i++ ) {
				elem = this[ i ];

				if ( elem.nodeType === 1 && elem.className ) {
					if ( value ) {
						className = (" " + elem.className + " ").replace( rclass, " " );
						for ( c = 0, cl = classNames.length; c < cl; c++ ) {
							className = className.replace(" " + classNames[ c ] + " ", " ");
						}
						elem.className = jQuery.trim( className );

					} else {
						elem.className = "";
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value,
			isBool = typeof stateVal === "boolean";

		if ( jQuery.isFunction( value ) ) {
			return this.each(function( i ) {
				jQuery( this ).toggleClass( value.call(this, i, this.className, stateVal), stateVal );
			});
		}

		return this.each(function() {
			if ( type === "string" ) {
				// toggle individual class names
				var className,
					i = 0,
					self = jQuery( this ),
					state = stateVal,
					classNames = value.split( rspace );

				while ( (className = classNames[ i++ ]) ) {
					// check each className given, space seperated list
					state = isBool ? state : !self.hasClass( className );
					self[ state ? "addClass" : "removeClass" ]( className );
				}

			} else if ( type === "undefined" || type === "boolean" ) {
				if ( this.className ) {
					// store className if set
					jQuery._data( this, "__className__", this.className );
				}

				// toggle whole className
				this.className = this.className || value === false ? "" : jQuery._data( this, "__className__" ) || "";
			}
		});
	},

	hasClass: function( selector ) {
		var className = " " + selector + " ",
			i = 0,
			l = this.length;
		for ( ; i < l; i++ ) {
			if ( this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf( className ) > -1 ) {
				return true;
			}
		}

		return false;
	},

	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[0];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] || jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks && "get" in hooks && (ret = hooks.get( elem, "value" )) !== undefined ) {
					return ret;
				}

				ret = elem.value;

				return typeof ret === "string" ?
					// handle most common string cases
					ret.replace(rreturn, "") :
					// handle cases where value is null/undef or number
					ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each(function( i ) {
			var self = jQuery(this), val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, self.val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";
			} else if ( typeof val === "number" ) {
				val += "";
			} else if ( jQuery.isArray( val ) ) {
				val = jQuery.map(val, function ( value ) {
					return value == null ? "" : value + "";
				});
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !("set" in hooks) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		});
	}
});

jQuery.extend({
	valHooks: {
		option: {
			get: function( elem ) {
				// attributes.value is undefined in Blackberry 4.7 but
				// uses .value. See #6932
				var val = elem.attributes.value;
				return !val || val.specified ? elem.value : elem.text;
			}
		},
		select: {
			get: function( elem ) {
				var value, i, max, option,
					index = elem.selectedIndex,
					values = [],
					options = elem.options,
					one = elem.type === "select-one";

				// Nothing was selected
				if ( index < 0 ) {
					return null;
				}

				// Loop through all the selected options
				i = one ? index : 0;
				max = one ? index + 1 : options.length;
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Don't return options that are disabled or in a disabled optgroup
					if ( option.selected && (jQuery.support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null) &&
							(!option.parentNode.disabled || !jQuery.nodeName( option.parentNode, "optgroup" )) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				// Fixes Bug #2551 -- select.val() broken in IE after form.reset()
				if ( one && !values.length && options.length ) {
					return jQuery( options[ index ] ).val();
				}

				return values;
			},

			set: function( elem, value ) {
				var values = jQuery.makeArray( value );

				jQuery(elem).find("option").each(function() {
					this.selected = jQuery.inArray( jQuery(this).val(), values ) >= 0;
				});

				if ( !values.length ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	},

	attrFn: {
		val: true,
		css: true,
		html: true,
		text: true,
		data: true,
		width: true,
		height: true,
		offset: true
	},

	attr: function( elem, name, value, pass ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// don't get/set attributes on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( pass && name in jQuery.attrFn ) {
			return jQuery( elem )[ name ]( value );
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		// All attributes are lowercase
		// Grab necessary hook if one is defined
		if ( notxml ) {
			name = name.toLowerCase();
			hooks = jQuery.attrHooks[ name ] || ( rboolean.test( name ) ? boolHook : nodeHook );
		}

		if ( value !== undefined ) {

			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;

			} else if ( hooks && "set" in hooks && notxml && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				elem.setAttribute( name, "" + value );
				return value;
			}

		} else if ( hooks && "get" in hooks && notxml && (ret = hooks.get( elem, name )) !== null ) {
			return ret;

		} else {

			ret = elem.getAttribute( name );

			// Non-existent attributes return null, we normalize to undefined
			return ret === null ?
				undefined :
				ret;
		}
	},

	removeAttr: function( elem, value ) {
		var propName, attrNames, name, l, isBool,
			i = 0;

		if ( value && elem.nodeType === 1 ) {
			attrNames = value.toLowerCase().split( rspace );
			l = attrNames.length;

			for ( ; i < l; i++ ) {
				name = attrNames[ i ];

				if ( name ) {
					propName = jQuery.propFix[ name ] || name;
					isBool = rboolean.test( name );

					// See #9699 for explanation of this approach (setting first, then removal)
					// Do not do this for boolean attributes (see #10870)
					if ( !isBool ) {
						jQuery.attr( elem, name, "" );
					}
					elem.removeAttribute( getSetAttribute ? name : propName );

					// Set corresponding property to false for boolean attributes
					if ( isBool && propName in elem ) {
						elem[ propName ] = false;
					}
				}
			}
		}
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				// We can't allow the type property to be changed (since it causes problems in IE)
				if ( rtype.test( elem.nodeName ) && elem.parentNode ) {
					jQuery.error( "type property can't be changed" );
				} else if ( !jQuery.support.radioValue && value === "radio" && jQuery.nodeName(elem, "input") ) {
					// Setting the type on a radio button after the value resets the value in IE6-9
					// Reset value to it's default in case type is set after value
					// This is for element creation
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		},
		// Use the value property for back compat
		// Use the nodeHook for button elements in IE6/7 (#1954)
		value: {
			get: function( elem, name ) {
				if ( nodeHook && jQuery.nodeName( elem, "button" ) ) {
					return nodeHook.get( elem, name );
				}
				return name in elem ?
					elem.value :
					null;
			},
			set: function( elem, value, name ) {
				if ( nodeHook && jQuery.nodeName( elem, "button" ) ) {
					return nodeHook.set( elem, value, name );
				}
				// Does not return so that setAttribute is also used
				elem.value = value;
			}
		}
	},

	propFix: {
		tabindex: "tabIndex",
		readonly: "readOnly",
		"for": "htmlFor",
		"class": "className",
		maxlength: "maxLength",
		cellspacing: "cellSpacing",
		cellpadding: "cellPadding",
		rowspan: "rowSpan",
		colspan: "colSpan",
		usemap: "useMap",
		frameborder: "frameBorder",
		contenteditable: "contentEditable"
	},

	prop: function( elem, name, value ) {
		var ret, hooks, notxml,
			nType = elem.nodeType;

		// don't get/set properties on text, comment and attribute nodes
		if ( !elem || nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		notxml = nType !== 1 || !jQuery.isXMLDoc( elem );

		if ( notxml ) {
			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks && (ret = hooks.set( elem, value, name )) !== undefined ) {
				return ret;

			} else {
				return ( elem[ name ] = value );
			}

		} else {
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, name )) !== null ) {
				return ret;

			} else {
				return elem[ name ];
			}
		}
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {
				// elem.tabIndex doesn't always return the correct value when it hasn't been explicitly set
				// http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				var attributeNode = elem.getAttributeNode("tabindex");

				return attributeNode && attributeNode.specified ?
					parseInt( attributeNode.value, 10 ) :
					rfocusable.test( elem.nodeName ) || rclickable.test( elem.nodeName ) && elem.href ?
						0 :
						undefined;
			}
		}
	}
});

// Add the tabIndex propHook to attrHooks for back-compat (different case is intentional)
jQuery.attrHooks.tabindex = jQuery.propHooks.tabIndex;

// Hook for boolean attributes
boolHook = {
	get: function( elem, name ) {
		// Align boolean attributes with corresponding properties
		// Fall back to attribute presence where some booleans are not supported
		var attrNode,
			property = jQuery.prop( elem, name );
		return property === true || typeof property !== "boolean" && ( attrNode = elem.getAttributeNode(name) ) && attrNode.nodeValue !== false ?
			name.toLowerCase() :
			undefined;
	},
	set: function( elem, value, name ) {
		var propName;
		if ( value === false ) {
			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			// value is true since we know at this point it's type boolean and not false
			// Set boolean attributes to the same name and set the DOM property
			propName = jQuery.propFix[ name ] || name;
			if ( propName in elem ) {
				// Only set the IDL specifically if it already exists on the element
				elem[ propName ] = true;
			}

			elem.setAttribute( name, name.toLowerCase() );
		}
		return name;
	}
};

// IE6/7 do not support getting/setting some attributes with get/setAttribute
if ( !getSetAttribute ) {

	fixSpecified = {
		name: true,
		id: true,
		coords: true
	};

	// Use this for any attribute in IE6/7
	// This fixes almost every IE6/7 issue
	nodeHook = jQuery.valHooks.button = {
		get: function( elem, name ) {
			var ret;
			ret = elem.getAttributeNode( name );
			return ret && ( fixSpecified[ name ] ? ret.nodeValue !== "" : ret.specified ) ?
				ret.nodeValue :
				undefined;
		},
		set: function( elem, value, name ) {
			// Set the existing or create a new attribute node
			var ret = elem.getAttributeNode( name );
			if ( !ret ) {
				ret = document.createAttribute( name );
				elem.setAttributeNode( ret );
			}
			return ( ret.nodeValue = value + "" );
		}
	};

	// Apply the nodeHook to tabindex
	jQuery.attrHooks.tabindex.set = nodeHook.set;

	// Set width and height to auto instead of 0 on empty string( Bug #8150 )
	// This is for removals
	jQuery.each([ "width", "height" ], function( i, name ) {
		jQuery.attrHooks[ name ] = jQuery.extend( jQuery.attrHooks[ name ], {
			set: function( elem, value ) {
				if ( value === "" ) {
					elem.setAttribute( name, "auto" );
					return value;
				}
			}
		});
	});

	// Set contenteditable to false on removals(#10429)
	// Setting to empty string throws an error as an invalid value
	jQuery.attrHooks.contenteditable = {
		get: nodeHook.get,
		set: function( elem, value, name ) {
			if ( value === "" ) {
				value = "false";
			}
			nodeHook.set( elem, value, name );
		}
	};
}


// Some attributes require a special call on IE
if ( !jQuery.support.hrefNormalized ) {
	jQuery.each([ "href", "src", "width", "height" ], function( i, name ) {
		jQuery.attrHooks[ name ] = jQuery.extend( jQuery.attrHooks[ name ], {
			get: function( elem ) {
				var ret = elem.getAttribute( name, 2 );
				return ret === null ? undefined : ret;
			}
		});
	});
}

if ( !jQuery.support.style ) {
	jQuery.attrHooks.style = {
		get: function( elem ) {
			// Return undefined in the case of empty string
			// Normalize to lowercase since IE uppercases css property names
			return elem.style.cssText.toLowerCase() || undefined;
		},
		set: function( elem, value ) {
			return ( elem.style.cssText = "" + value );
		}
	};
}

// Safari mis-reports the default selected property of an option
// Accessing the parent's selectedIndex property fixes it
if ( !jQuery.support.optSelected ) {
	jQuery.propHooks.selected = jQuery.extend( jQuery.propHooks.selected, {
		get: function( elem ) {
			var parent = elem.parentNode;

			if ( parent ) {
				parent.selectedIndex;

				// Make sure that it also works with optgroups, see #5701
				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
			return null;
		}
	});
}

// IE6/7 call enctype encoding
if ( !jQuery.support.enctype ) {
	jQuery.propFix.enctype = "encoding";
}

// Radios and checkboxes getter/setter
if ( !jQuery.support.checkOn ) {
	jQuery.each([ "radio", "checkbox" ], function() {
		jQuery.valHooks[ this ] = {
			get: function( elem ) {
				// Handle the case where in Webkit "" is returned instead of "on" if a value isn't specified
				return elem.getAttribute("value") === null ? "on" : elem.value;
			}
		};
	});
}
jQuery.each([ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = jQuery.extend( jQuery.valHooks[ this ], {
		set: function( elem, value ) {
			if ( jQuery.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery(elem).val(), value ) >= 0 );
			}
		}
	});
});




var rformElems = /^(?:textarea|input|select)$/i,
	rtypenamespace = /^([^\.]*)?(?:\.(.+))?$/,
	rhoverHack = /(?:^|\s)hover(\.\S+)?\b/,
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|contextmenu)|click/,
	rfocusMorph = /^(?:focusinfocus|focusoutblur)$/,
	rquickIs = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,
	quickParse = function( selector ) {
		var quick = rquickIs.exec( selector );
		if ( quick ) {
			//   0  1    2   3
			// [ _, tag, id, class ]
			quick[1] = ( quick[1] || "" ).toLowerCase();
			quick[3] = quick[3] && new RegExp( "(?:^|\\s)" + quick[3] + "(?:\\s|$)" );
		}
		return quick;
	},
	quickIs = function( elem, m ) {
		var attrs = elem.attributes || {};
		return (
			(!m[1] || elem.nodeName.toLowerCase() === m[1]) &&
			(!m[2] || (attrs.id || {}).value === m[2]) &&
			(!m[3] || m[3].test( (attrs[ "class" ] || {}).value ))
		);
	},
	hoverHack = function( events ) {
		return jQuery.event.special.hover ? events : events.replace( rhoverHack, "mouseenter$1 mouseleave$1" );
	};

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	add: function( elem, types, handler, data, selector ) {

		var elemData, eventHandle, events,
			t, tns, type, namespaces, handleObj,
			handleObjIn, quick, handlers, special;

		// Don't attach events to noData or text/comment nodes (allow plain objects tho)
		if ( elem.nodeType === 3 || elem.nodeType === 8 || !types || !handler || !(elemData = jQuery._data( elem )) ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		events = elemData.events;
		if ( !events ) {
			elemData.events = events = {};
		}
		eventHandle = elemData.handle;
		if ( !eventHandle ) {
			elemData.handle = eventHandle = function( e ) {
				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && (!e || jQuery.event.triggered !== e.type) ?
					jQuery.event.dispatch.apply( eventHandle.elem, arguments ) :
					undefined;
			};
			// Add elem as a property of the handle fn to prevent a memory leak with IE non-native events
			eventHandle.elem = elem;
		}

		// Handle multiple events separated by a space
		// jQuery(...).bind("mouseover mouseout", fn);
		types = jQuery.trim( hoverHack(types) ).split( " " );
		for ( t = 0; t < types.length; t++ ) {

			tns = rtypenamespace.exec( types[t] ) || [];
			type = tns[1];
			namespaces = ( tns[2] || "" ).split( "." ).sort();

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend({
				type: type,
				origType: tns[1],
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				quick: selector && quickParse( selector ),
				namespace: namespaces.join(".")
			}, handleObjIn );

			// Init the event handler queue if we're the first
			handlers = events[ type ];
			if ( !handlers ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener/attachEvent if the special events handler returns false
				if ( !special.setup || special.setup.call( elem, data, namespaces, eventHandle ) === false ) {
					// Bind the global event handler to the element
					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle, false );

					} else if ( elem.attachEvent ) {
						elem.attachEvent( "on" + type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

		// Nullify elem to prevent memory leaks in IE
		elem = null;
	},

	global: {},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var elemData = jQuery.hasData( elem ) && jQuery._data( elem ),
			t, tns, type, origType, namespaces, origCount,
			j, events, special, handle, eventType, handleObj;

		if ( !elemData || !(events = elemData.events) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = jQuery.trim( hoverHack( types || "" ) ).split(" ");
		for ( t = 0; t < types.length; t++ ) {
			tns = rtypenamespace.exec( types[t] ) || [];
			type = origType = tns[1];
			namespaces = tns[2];

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector? special.delegateType : special.bindType ) || type;
			eventType = events[ type ] || [];
			origCount = eventType.length;
			namespaces = namespaces ? new RegExp("(^|\\.)" + namespaces.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;

			// Remove matching events
			for ( j = 0; j < eventType.length; j++ ) {
				handleObj = eventType[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					 ( !handler || handler.guid === handleObj.guid ) &&
					 ( !namespaces || namespaces.test( handleObj.namespace ) ) &&
					 ( !selector || selector === handleObj.selector || selector === "**" && handleObj.selector ) ) {
					eventType.splice( j--, 1 );

					if ( handleObj.selector ) {
						eventType.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( eventType.length === 0 && origCount !== eventType.length ) {
				if ( !special.teardown || special.teardown.call( elem, namespaces ) === false ) {
					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			handle = elemData.handle;
			if ( handle ) {
				handle.elem = null;
			}

			// removeData also checks for emptiness and clears the expando if empty
			// so use it instead of delete
			jQuery.removeData( elem, [ "events", "handle" ], true );
		}
	},

	// Events that are safe to short-circuit if no handlers are attached.
	// Native DOM events should not be added, they may have inline handlers.
	customEvent: {
		"getData": true,
		"setData": true,
		"changeData": true
	},

	trigger: function( event, data, elem, onlyHandlers ) {
		// Don't do events on text and comment nodes
		if ( elem && (elem.nodeType === 3 || elem.nodeType === 8) ) {
			return;
		}

		// Event object or event type
		var type = event.type || event,
			namespaces = [],
			cache, exclusive, i, cur, old, ontype, special, handle, eventPath, bubbleType;

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "!" ) >= 0 ) {
			// Exclusive events trigger only for the exact event (no namespaces)
			type = type.slice(0, -1);
			exclusive = true;
		}

		if ( type.indexOf( "." ) >= 0 ) {
			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split(".");
			type = namespaces.shift();
			namespaces.sort();
		}

		if ( (!elem || jQuery.event.customEvent[ type ]) && !jQuery.event.global[ type ] ) {
			// No jQuery handlers for this event type, and it can't have inline handlers
			return;
		}

		// Caller can pass in an Event, Object, or just an event type string
		event = typeof event === "object" ?
			// jQuery.Event object
			event[ jQuery.expando ] ? event :
			// Object literal
			new jQuery.Event( type, event ) :
			// Just the event type (string)
			new jQuery.Event( type );

		event.type = type;
		event.isTrigger = true;
		event.exclusive = exclusive;
		event.namespace = namespaces.join( "." );
		event.namespace_re = event.namespace? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
		ontype = type.indexOf( ":" ) < 0 ? "on" + type : "";

		// Handle a global trigger
		if ( !elem ) {

			// TODO: Stop taunting the data cache; remove global events and always attach to document
			cache = jQuery.cache;
			for ( i in cache ) {
				if ( cache[ i ].events && cache[ i ].events[ type ] ) {
					jQuery.event.trigger( event, data, cache[ i ].handle.elem, true );
				}
			}
			return;
		}

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data != null ? jQuery.makeArray( data ) : [];
		data.unshift( event );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		eventPath = [[ elem, special.bindType || type ]];
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			cur = rfocusMorph.test( bubbleType + type ) ? elem : elem.parentNode;
			old = null;
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push([ cur, bubbleType ]);
				old = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( old && old === elem.ownerDocument ) {
				eventPath.push([ old.defaultView || old.parentWindow || window, bubbleType ]);
			}
		}

		// Fire handlers on the event path
		for ( i = 0; i < eventPath.length && !event.isPropagationStopped(); i++ ) {

			cur = eventPath[i][0];
			event.type = eventPath[i][1];

			handle = ( jQuery._data( cur, "events" ) || {} )[ event.type ] && jQuery._data( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}
			// Note that this is a bare JS function and not a jQuery handler
			handle = ontype && cur[ ontype ];
			if ( handle && jQuery.acceptData( cur ) && handle.apply( cur, data ) === false ) {
				event.preventDefault();
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( (!special._default || special._default.apply( elem.ownerDocument, data ) === false) &&
				!(type === "click" && jQuery.nodeName( elem, "a" )) && jQuery.acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name name as the event.
				// Can't use an .isFunction() check here because IE6/7 fails that test.
				// Don't do default actions on window, that's where global variables be (#6170)
				// IE<9 dies on focus/blur to hidden element (#1486)
				if ( ontype && elem[ type ] && ((type !== "focus" && type !== "blur") || event.target.offsetWidth !== 0) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					old = elem[ ontype ];

					if ( old ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( old ) {
						elem[ ontype ] = old;
					}
				}
			}
		}

		return event.result;
	},

	dispatch: function( event ) {

		// Make a writable jQuery.Event from the native event object
		event = jQuery.event.fix( event || window.event );

		var handlers = ( (jQuery._data( this, "events" ) || {} )[ event.type ] || []),
			delegateCount = handlers.delegateCount,
			args = [].slice.call( arguments, 0 ),
			run_all = !event.exclusive && !event.namespace,
			special = jQuery.event.special[ event.type ] || {},
			handlerQueue = [],
			i, j, cur, jqcur, ret, selMatch, matched, matches, handleObj, sel, related;

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[0] = event;
		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers that should run if there are delegated events
		// Avoid non-left-click bubbling in Firefox (#3861)
		if ( delegateCount && !(event.button && event.type === "click") ) {

			// Pregenerate a single jQuery object for reuse with .is()
			jqcur = jQuery(this);
			jqcur.context = this.ownerDocument || this;

			for ( cur = event.target; cur != this; cur = cur.parentNode || this ) {

				// Don't process events on disabled elements (#6911, #8165)
				if ( cur.disabled !== true ) {
					selMatch = {};
					matches = [];
					jqcur[0] = cur;
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];
						sel = handleObj.selector;

						if ( selMatch[ sel ] === undefined ) {
							selMatch[ sel ] = (
								handleObj.quick ? quickIs( cur, handleObj.quick ) : jqcur.is( sel )
							);
						}
						if ( selMatch[ sel ] ) {
							matches.push( handleObj );
						}
					}
					if ( matches.length ) {
						handlerQueue.push({ elem: cur, matches: matches });
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		if ( handlers.length > delegateCount ) {
			handlerQueue.push({ elem: this, matches: handlers.slice( delegateCount ) });
		}

		// Run delegates first; they may want to stop propagation beneath us
		for ( i = 0; i < handlerQueue.length && !event.isPropagationStopped(); i++ ) {
			matched = handlerQueue[ i ];
			event.currentTarget = matched.elem;

			for ( j = 0; j < matched.matches.length && !event.isImmediatePropagationStopped(); j++ ) {
				handleObj = matched.matches[ j ];

				// Triggered event must either 1) be non-exclusive and have no namespace, or
				// 2) have namespace(s) a subset or equal to those in the bound event (both can have no namespace).
				if ( run_all || (!event.namespace && !handleObj.namespace) || event.namespace_re && event.namespace_re.test( handleObj.namespace ) ) {

					event.data = handleObj.data;
					event.handleObj = handleObj;

					ret = ( (jQuery.event.special[ handleObj.origType ] || {}).handle || handleObj.handler )
							.apply( matched.elem, args );

					if ( ret !== undefined ) {
						event.result = ret;
						if ( ret === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	// Includes some event props shared by KeyEvent and MouseEvent
	// *** attrChange attrName relatedNode srcElement  are not normalized, non-W3C, deprecated, will be removed in 1.8 ***
	props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),

	fixHooks: {},

	keyHooks: {
		props: "char charCode key keyCode".split(" "),
		filter: function( event, original ) {

			// Add which for key events
			if ( event.which == null ) {
				event.which = original.charCode != null ? original.charCode : original.keyCode;
			}

			return event;
		}
	},

	mouseHooks: {
		props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
		filter: function( event, original ) {
			var eventDoc, doc, body,
				button = original.button,
				fromElement = original.fromElement;

			// Calculate pageX/Y if missing and clientX/Y available
			if ( event.pageX == null && original.clientX != null ) {
				eventDoc = event.target.ownerDocument || document;
				doc = eventDoc.documentElement;
				body = eventDoc.body;

				event.pageX = original.clientX + ( doc && doc.scrollLeft || body && body.scrollLeft || 0 ) - ( doc && doc.clientLeft || body && body.clientLeft || 0 );
				event.pageY = original.clientY + ( doc && doc.scrollTop  || body && body.scrollTop  || 0 ) - ( doc && doc.clientTop  || body && body.clientTop  || 0 );
			}

			// Add relatedTarget, if necessary
			if ( !event.relatedTarget && fromElement ) {
				event.relatedTarget = fromElement === event.target ? original.toElement : fromElement;
			}

			// Add which for click: 1 === left; 2 === middle; 3 === right
			// Note: button is not normalized, so don't use it
			if ( !event.which && button !== undefined ) {
				event.which = ( button & 1 ? 1 : ( button & 2 ? 3 : ( button & 4 ? 2 : 0 ) ) );
			}

			return event;
		}
	},

	fix: function( event ) {
		if ( event[ jQuery.expando ] ) {
			return event;
		}

		// Create a writable copy of the event object and normalize some properties
		var i, prop,
			originalEvent = event,
			fixHook = jQuery.event.fixHooks[ event.type ] || {},
			copy = fixHook.props ? this.props.concat( fixHook.props ) : this.props;

		event = jQuery.Event( originalEvent );

		for ( i = copy.length; i; ) {
			prop = copy[ --i ];
			event[ prop ] = originalEvent[ prop ];
		}

		// Fix target property, if necessary (#1925, IE 6/7/8 & Safari2)
		if ( !event.target ) {
			event.target = originalEvent.srcElement || document;
		}

		// Target should not be a text node (#504, Safari)
		if ( event.target.nodeType === 3 ) {
			event.target = event.target.parentNode;
		}

		// For mouse/key events; add metaKey if it's not there (#3368, IE6/7/8)
		if ( event.metaKey === undefined ) {
			event.metaKey = event.ctrlKey;
		}

		return fixHook.filter? fixHook.filter( event, originalEvent ) : event;
	},

	special: {
		ready: {
			// Make sure the ready event is setup
			setup: jQuery.bindReady
		},

		load: {
			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},

		focus: {
			delegateType: "focusin"
		},
		blur: {
			delegateType: "focusout"
		},

		beforeunload: {
			setup: function( data, namespaces, eventHandle ) {
				// We only want to do this special case on windows
				if ( jQuery.isWindow( this ) ) {
					this.onbeforeunload = eventHandle;
				}
			},

			teardown: function( namespaces, eventHandle ) {
				if ( this.onbeforeunload === eventHandle ) {
					this.onbeforeunload = null;
				}
			}
		}
	},

	simulate: function( type, elem, event, bubble ) {
		// Piggyback on a donor event to simulate a different one.
		// Fake originalEvent to avoid donor's stopPropagation, but if the
		// simulated event prevents default then we do the same on the donor.
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{ type: type,
				isSimulated: true,
				originalEvent: {}
			}
		);
		if ( bubble ) {
			jQuery.event.trigger( e, null, elem );
		} else {
			jQuery.event.dispatch.call( elem, e );
		}
		if ( e.isDefaultPrevented() ) {
			event.preventDefault();
		}
	}
};

// Some plugins are using, but it's undocumented/deprecated and will be removed.
// The 1.7 special event interface should provide all the hooks needed now.
jQuery.event.handle = jQuery.event.dispatch;

jQuery.removeEvent = document.removeEventListener ?
	function( elem, type, handle ) {
		if ( elem.removeEventListener ) {
			elem.removeEventListener( type, handle, false );
		}
	} :
	function( elem, type, handle ) {
		if ( elem.detachEvent ) {
			elem.detachEvent( "on" + type, handle );
		}
	};

jQuery.Event = function( src, props ) {
	// Allow instantiation without the 'new' keyword
	if ( !(this instanceof jQuery.Event) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = ( src.defaultPrevented || src.returnValue === false ||
			src.getPreventDefault && src.getPreventDefault() ) ? returnTrue : returnFalse;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

function returnFalse() {
	return false;
}
function returnTrue() {
	return true;
}

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// http://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	preventDefault: function() {
		this.isDefaultPrevented = returnTrue;

		var e = this.originalEvent;
		if ( !e ) {
			return;
		}

		// if preventDefault exists run it on the original event
		if ( e.preventDefault ) {
			e.preventDefault();

		// otherwise set the returnValue property of the original event to false (IE)
		} else {
			e.returnValue = false;
		}
	},
	stopPropagation: function() {
		this.isPropagationStopped = returnTrue;

		var e = this.originalEvent;
		if ( !e ) {
			return;
		}
		// if stopPropagation exists run it on the original event
		if ( e.stopPropagation ) {
			e.stopPropagation();
		}
		// otherwise set the cancelBubble property of the original event to true (IE)
		e.cancelBubble = true;
	},
	stopImmediatePropagation: function() {
		this.isImmediatePropagationStopped = returnTrue;
		this.stopPropagation();
	},
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse
};

// Create mouseenter/leave events using mouseover/out and event-time checks
jQuery.each({
	mouseenter: "mouseover",
	mouseleave: "mouseout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj,
				selector = handleObj.selector,
				ret;

			// For mousenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || (related !== target && !jQuery.contains( target, related )) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
});

// IE submit delegation
if ( !jQuery.support.submitBubbles ) {

	jQuery.event.special.submit = {
		setup: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Lazy-add a submit handler when a descendant form may potentially be submitted
			jQuery.event.add( this, "click._submit keypress._submit", function( e ) {
				// Node name check avoids a VML-related crash in IE (#9807)
				var elem = e.target,
					form = jQuery.nodeName( elem, "input" ) || jQuery.nodeName( elem, "button" ) ? elem.form : undefined;
				if ( form && !form._submit_attached ) {
					jQuery.event.add( form, "submit._submit", function( event ) {
						event._submit_bubble = true;
					});
					form._submit_attached = true;
				}
			});
			// return undefined since we don't need an event listener
		},
		
		postDispatch: function( event ) {
			// If form was submitted by the user, bubble the event up the tree
			if ( event._submit_bubble ) {
				delete event._submit_bubble;
				if ( this.parentNode && !event.isTrigger ) {
					jQuery.event.simulate( "submit", this.parentNode, event, true );
				}
			}
		},

		teardown: function() {
			// Only need this for delegated form submit events
			if ( jQuery.nodeName( this, "form" ) ) {
				return false;
			}

			// Remove delegated handlers; cleanData eventually reaps submit handlers attached above
			jQuery.event.remove( this, "._submit" );
		}
	};
}

// IE change delegation and checkbox/radio fix
if ( !jQuery.support.changeBubbles ) {

	jQuery.event.special.change = {

		setup: function() {

			if ( rformElems.test( this.nodeName ) ) {
				// IE doesn't fire change on a check/radio until blur; trigger it on click
				// after a propertychange. Eat the blur-change in special.change.handle.
				// This still fires onchange a second time for check/radio after blur.
				if ( this.type === "checkbox" || this.type === "radio" ) {
					jQuery.event.add( this, "propertychange._change", function( event ) {
						if ( event.originalEvent.propertyName === "checked" ) {
							this._just_changed = true;
						}
					});
					jQuery.event.add( this, "click._change", function( event ) {
						if ( this._just_changed && !event.isTrigger ) {
							this._just_changed = false;
							jQuery.event.simulate( "change", this, event, true );
						}
					});
				}
				return false;
			}
			// Delegated event; lazy-add a change handler on descendant inputs
			jQuery.event.add( this, "beforeactivate._change", function( e ) {
				var elem = e.target;

				if ( rformElems.test( elem.nodeName ) && !elem._change_attached ) {
					jQuery.event.add( elem, "change._change", function( event ) {
						if ( this.parentNode && !event.isSimulated && !event.isTrigger ) {
							jQuery.event.simulate( "change", this.parentNode, event, true );
						}
					});
					elem._change_attached = true;
				}
			});
		},

		handle: function( event ) {
			var elem = event.target;

			// Swallow native change events from checkbox/radio, we already triggered them above
			if ( this !== elem || event.isSimulated || event.isTrigger || (elem.type !== "radio" && elem.type !== "checkbox") ) {
				return event.handleObj.handler.apply( this, arguments );
			}
		},

		teardown: function() {
			jQuery.event.remove( this, "._change" );

			return rformElems.test( this.nodeName );
		}
	};
}

// Create "bubbling" focus and blur events
if ( !jQuery.support.focusinBubbles ) {
	jQuery.each({ focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler while someone wants focusin/focusout
		var attaches = 0,
			handler = function( event ) {
				jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ), true );
			};

		jQuery.event.special[ fix ] = {
			setup: function() {
				if ( attaches++ === 0 ) {
					document.addEventListener( orig, handler, true );
				}
			},
			teardown: function() {
				if ( --attaches === 0 ) {
					document.removeEventListener( orig, handler, true );
				}
			}
		};
	});
}

jQuery.fn.extend({

	on: function( types, selector, data, fn, /*INTERNAL*/ one ) {
		var origFn, type;

		// Types can be a map of types/handlers
		if ( typeof types === "object" ) {
			// ( types-Object, selector, data )
			if ( typeof selector !== "string" ) { // && selector != null
				// ( types-Object, data )
				data = data || selector;
				selector = undefined;
			}
			for ( type in types ) {
				this.on( type, selector, data, types[ type ], one );
			}
			return this;
		}

		if ( data == null && fn == null ) {
			// ( types, fn )
			fn = selector;
			data = selector = undefined;
		} else if ( fn == null ) {
			if ( typeof selector === "string" ) {
				// ( types, selector, fn )
				fn = data;
				data = undefined;
			} else {
				// ( types, data, fn )
				fn = data;
				data = selector;
				selector = undefined;
			}
		}
		if ( fn === false ) {
			fn = returnFalse;
		} else if ( !fn ) {
			return this;
		}

		if ( one === 1 ) {
			origFn = fn;
			fn = function( event ) {
				// Can use an empty set, since event contains the info
				jQuery().off( event );
				return origFn.apply( this, arguments );
			};
			// Use same guid so caller can remove using origFn
			fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
		}
		return this.each( function() {
			jQuery.event.add( this, types, fn, data, selector );
		});
	},
	one: function( types, selector, data, fn ) {
		return this.on( types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		if ( types && types.preventDefault && types.handleObj ) {
			// ( event )  dispatched jQuery.Event
			var handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {
			// ( types-object [, selector] )
			for ( var type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {
			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each(function() {
			jQuery.event.remove( this, types, fn, selector );
		});
	},

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	live: function( types, data, fn ) {
		jQuery( this.context ).on( types, this.selector, data, fn );
		return this;
	},
	die: function( types, fn ) {
		jQuery( this.context ).off( types, this.selector || "**", fn );
		return this;
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {
		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length == 1? this.off( selector, "**" ) : this.off( types, selector, fn );
	},

	trigger: function( type, data ) {
		return this.each(function() {
			jQuery.event.trigger( type, data, this );
		});
	},
	triggerHandler: function( type, data ) {
		if ( this[0] ) {
			return jQuery.event.trigger( type, data, this[0], true );
		}
	},

	toggle: function( fn ) {
		// Save reference to arguments for access in closure
		var args = arguments,
			guid = fn.guid || jQuery.guid++,
			i = 0,
			toggler = function( event ) {
				// Figure out which function to execute
				var lastToggle = ( jQuery._data( this, "lastToggle" + fn.guid ) || 0 ) % i;
				jQuery._data( this, "lastToggle" + fn.guid, lastToggle + 1 );

				// Make sure that clicks stop
				event.preventDefault();

				// and execute the function
				return args[ lastToggle ].apply( this, arguments ) || false;
			};

		// link all the functions, so any of them can unbind this click handler
		toggler.guid = guid;
		while ( i < args.length ) {
			args[ i++ ].guid = guid;
		}

		return this.click( toggler );
	},

	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
});

jQuery.each( ("blur focus focusin focusout load resize scroll unload click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup error contextmenu").split(" "), function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		if ( fn == null ) {
			fn = data;
			data = null;
		}

		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};

	if ( jQuery.attrFn ) {
		jQuery.attrFn[ name ] = true;
	}

	if ( rkeyEvent.test( name ) ) {
		jQuery.event.fixHooks[ name ] = jQuery.event.keyHooks;
	}

	if ( rmouseEvent.test( name ) ) {
		jQuery.event.fixHooks[ name ] = jQuery.event.mouseHooks;
	}
});



/*!
 * Sizzle CSS Selector Engine
 *  Copyright 2011, The Dojo Foundation
 *  Released under the MIT, BSD, and GPL Licenses.
 *  More information: http://sizzlejs.com/
 */
(function(){

var chunker = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
	expando = "sizcache" + (Math.random() + '').replace('.', ''),
	done = 0,
	toString = Object.prototype.toString,
	hasDuplicate = false,
	baseHasDuplicate = true,
	rBackslash = /\\/g,
	rReturn = /\r\n/g,
	rNonWord = /\W/;

// Here we check if the JavaScript engine is using some sort of
// optimization where it does not always call our comparision
// function. If that is the case, discard the hasDuplicate value.
//   Thus far that includes Google Chrome.
[0, 0].sort(function() {
	baseHasDuplicate = false;
	return 0;
});

var Sizzle = function( selector, context, results, seed ) {
	results = results || [];
	context = context || document;

	var origContext = context;

	if ( context.nodeType !== 1 && context.nodeType !== 9 ) {
		return [];
	}

	if ( !selector || typeof selector !== "string" ) {
		return results;
	}

	var m, set, checkSet, extra, ret, cur, pop, i,
		prune = true,
		contextXML = Sizzle.isXML( context ),
		parts = [],
		soFar = selector;

	// Reset the position of the chunker regexp (start from head)
	do {
		chunker.exec( "" );
		m = chunker.exec( soFar );

		if ( m ) {
			soFar = m[3];

			parts.push( m[1] );

			if ( m[2] ) {
				extra = m[3];
				break;
			}
		}
	} while ( m );

	if ( parts.length > 1 && origPOS.exec( selector ) ) {

		if ( parts.length === 2 && Expr.relative[ parts[0] ] ) {
			set = posProcess( parts[0] + parts[1], context, seed );

		} else {
			set = Expr.relative[ parts[0] ] ?
				[ context ] :
				Sizzle( parts.shift(), context );

			while ( parts.length ) {
				selector = parts.shift();

				if ( Expr.relative[ selector ] ) {
					selector += parts.shift();
				}

				set = posProcess( selector, set, seed );
			}
		}

	} else {
		// Take a shortcut and set the context if the root selector is an ID
		// (but not if it'll be faster if the inner selector is an ID)
		if ( !seed && parts.length > 1 && context.nodeType === 9 && !contextXML &&
				Expr.match.ID.test(parts[0]) && !Expr.match.ID.test(parts[parts.length - 1]) ) {

			ret = Sizzle.find( parts.shift(), context, contextXML );
			context = ret.expr ?
				Sizzle.filter( ret.expr, ret.set )[0] :
				ret.set[0];
		}

		if ( context ) {
			ret = seed ?
				{ expr: parts.pop(), set: makeArray(seed) } :
				Sizzle.find( parts.pop(), parts.length === 1 && (parts[0] === "~" || parts[0] === "+") && context.parentNode ? context.parentNode : context, contextXML );

			set = ret.expr ?
				Sizzle.filter( ret.expr, ret.set ) :
				ret.set;

			if ( parts.length > 0 ) {
				checkSet = makeArray( set );

			} else {
				prune = false;
			}

			while ( parts.length ) {
				cur = parts.pop();
				pop = cur;

				if ( !Expr.relative[ cur ] ) {
					cur = "";
				} else {
					pop = parts.pop();
				}

				if ( pop == null ) {
					pop = context;
				}

				Expr.relative[ cur ]( checkSet, pop, contextXML );
			}

		} else {
			checkSet = parts = [];
		}
	}

	if ( !checkSet ) {
		checkSet = set;
	}

	if ( !checkSet ) {
		Sizzle.error( cur || selector );
	}

	if ( toString.call(checkSet) === "[object Array]" ) {
		if ( !prune ) {
			results.push.apply( results, checkSet );

		} else if ( context && context.nodeType === 1 ) {
			for ( i = 0; checkSet[i] != null; i++ ) {
				if ( checkSet[i] && (checkSet[i] === true || checkSet[i].nodeType === 1 && Sizzle.contains(context, checkSet[i])) ) {
					results.push( set[i] );
				}
			}

		} else {
			for ( i = 0; checkSet[i] != null; i++ ) {
				if ( checkSet[i] && checkSet[i].nodeType === 1 ) {
					results.push( set[i] );
				}
			}
		}

	} else {
		makeArray( checkSet, results );
	}

	if ( extra ) {
		Sizzle( extra, origContext, results, seed );
		Sizzle.uniqueSort( results );
	}

	return results;
};

Sizzle.uniqueSort = function( results ) {
	if ( sortOrder ) {
		hasDuplicate = baseHasDuplicate;
		results.sort( sortOrder );

		if ( hasDuplicate ) {
			for ( var i = 1; i < results.length; i++ ) {
				if ( results[i] === results[ i - 1 ] ) {
					results.splice( i--, 1 );
				}
			}
		}
	}

	return results;
};

Sizzle.matches = function( expr, set ) {
	return Sizzle( expr, null, null, set );
};

Sizzle.matchesSelector = function( node, expr ) {
	return Sizzle( expr, null, null, [node] ).length > 0;
};

Sizzle.find = function( expr, context, isXML ) {
	var set, i, len, match, type, left;

	if ( !expr ) {
		return [];
	}

	for ( i = 0, len = Expr.order.length; i < len; i++ ) {
		type = Expr.order[i];

		if ( (match = Expr.leftMatch[ type ].exec( expr )) ) {
			left = match[1];
			match.splice( 1, 1 );

			if ( left.substr( left.length - 1 ) !== "\\" ) {
				match[1] = (match[1] || "").replace( rBackslash, "" );
				set = Expr.find[ type ]( match, context, isXML );

				if ( set != null ) {
					expr = expr.replace( Expr.match[ type ], "" );
					break;
				}
			}
		}
	}

	if ( !set ) {
		set = typeof context.getElementsByTagName !== "undefined" ?
			context.getElementsByTagName( "*" ) :
			[];
	}

	return { set: set, expr: expr };
};

Sizzle.filter = function( expr, set, inplace, not ) {
	var match, anyFound,
		type, found, item, filter, left,
		i, pass,
		old = expr,
		result = [],
		curLoop = set,
		isXMLFilter = set && set[0] && Sizzle.isXML( set[0] );

	while ( expr && set.length ) {
		for ( type in Expr.filter ) {
			if ( (match = Expr.leftMatch[ type ].exec( expr )) != null && match[2] ) {
				filter = Expr.filter[ type ];
				left = match[1];

				anyFound = false;

				match.splice(1,1);

				if ( left.substr( left.length - 1 ) === "\\" ) {
					continue;
				}

				if ( curLoop === result ) {
					result = [];
				}

				if ( Expr.preFilter[ type ] ) {
					match = Expr.preFilter[ type ]( match, curLoop, inplace, result, not, isXMLFilter );

					if ( !match ) {
						anyFound = found = true;

					} else if ( match === true ) {
						continue;
					}
				}

				if ( match ) {
					for ( i = 0; (item = curLoop[i]) != null; i++ ) {
						if ( item ) {
							found = filter( item, match, i, curLoop );
							pass = not ^ found;

							if ( inplace && found != null ) {
								if ( pass ) {
									anyFound = true;

								} else {
									curLoop[i] = false;
								}

							} else if ( pass ) {
								result.push( item );
								anyFound = true;
							}
						}
					}
				}

				if ( found !== undefined ) {
					if ( !inplace ) {
						curLoop = result;
					}

					expr = expr.replace( Expr.match[ type ], "" );

					if ( !anyFound ) {
						return [];
					}

					break;
				}
			}
		}

		// Improper expression
		if ( expr === old ) {
			if ( anyFound == null ) {
				Sizzle.error( expr );

			} else {
				break;
			}
		}

		old = expr;
	}

	return curLoop;
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Utility function for retreiving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
var getText = Sizzle.getText = function( elem ) {
    var i, node,
		nodeType = elem.nodeType,
		ret = "";

	if ( nodeType ) {
		if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
			// Use textContent || innerText for elements
			if ( typeof elem.textContent === 'string' ) {
				return elem.textContent;
			} else if ( typeof elem.innerText === 'string' ) {
				// Replace IE's carriage returns
				return elem.innerText.replace( rReturn, '' );
			} else {
				// Traverse it's children
				for ( elem = elem.firstChild; elem; elem = elem.nextSibling) {
					ret += getText( elem );
				}
			}
		} else if ( nodeType === 3 || nodeType === 4 ) {
			return elem.nodeValue;
		}
	} else {

		// If no nodeType, this is expected to be an array
		for ( i = 0; (node = elem[i]); i++ ) {
			// Do not traverse comment nodes
			if ( node.nodeType !== 8 ) {
				ret += getText( node );
			}
		}
	}
	return ret;
};

var Expr = Sizzle.selectors = {
	order: [ "ID", "NAME", "TAG" ],

	match: {
		ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
		CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
		NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
		ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
		TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
		CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
		POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
		PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
	},

	leftMatch: {},

	attrMap: {
		"class": "className",
		"for": "htmlFor"
	},

	attrHandle: {
		href: function( elem ) {
			return elem.getAttribute( "href" );
		},
		type: function( elem ) {
			return elem.getAttribute( "type" );
		}
	},

	relative: {
		"+": function(checkSet, part){
			var isPartStr = typeof part === "string",
				isTag = isPartStr && !rNonWord.test( part ),
				isPartStrNotTag = isPartStr && !isTag;

			if ( isTag ) {
				part = part.toLowerCase();
			}

			for ( var i = 0, l = checkSet.length, elem; i < l; i++ ) {
				if ( (elem = checkSet[i]) ) {
					while ( (elem = elem.previousSibling) && elem.nodeType !== 1 ) {}

					checkSet[i] = isPartStrNotTag || elem && elem.nodeName.toLowerCase() === part ?
						elem || false :
						elem === part;
				}
			}

			if ( isPartStrNotTag ) {
				Sizzle.filter( part, checkSet, true );
			}
		},

		">": function( checkSet, part ) {
			var elem,
				isPartStr = typeof part === "string",
				i = 0,
				l = checkSet.length;

			if ( isPartStr && !rNonWord.test( part ) ) {
				part = part.toLowerCase();

				for ( ; i < l; i++ ) {
					elem = checkSet[i];

					if ( elem ) {
						var parent = elem.parentNode;
						checkSet[i] = parent.nodeName.toLowerCase() === part ? parent : false;
					}
				}

			} else {
				for ( ; i < l; i++ ) {
					elem = checkSet[i];

					if ( elem ) {
						checkSet[i] = isPartStr ?
							elem.parentNode :
							elem.parentNode === part;
					}
				}

				if ( isPartStr ) {
					Sizzle.filter( part, checkSet, true );
				}
			}
		},

		"": function(checkSet, part, isXML){
			var nodeCheck,
				doneName = done++,
				checkFn = dirCheck;

			if ( typeof part === "string" && !rNonWord.test( part ) ) {
				part = part.toLowerCase();
				nodeCheck = part;
				checkFn = dirNodeCheck;
			}

			checkFn( "parentNode", part, doneName, checkSet, nodeCheck, isXML );
		},

		"~": function( checkSet, part, isXML ) {
			var nodeCheck,
				doneName = done++,
				checkFn = dirCheck;

			if ( typeof part === "string" && !rNonWord.test( part ) ) {
				part = part.toLowerCase();
				nodeCheck = part;
				checkFn = dirNodeCheck;
			}

			checkFn( "previousSibling", part, doneName, checkSet, nodeCheck, isXML );
		}
	},

	find: {
		ID: function( match, context, isXML ) {
			if ( typeof context.getElementById !== "undefined" && !isXML ) {
				var m = context.getElementById(match[1]);
				// Check parentNode to catch when Blackberry 4.6 returns
				// nodes that are no longer in the document #6963
				return m && m.parentNode ? [m] : [];
			}
		},

		NAME: function( match, context ) {
			if ( typeof context.getElementsByName !== "undefined" ) {
				var ret = [],
					results = context.getElementsByName( match[1] );

				for ( var i = 0, l = results.length; i < l; i++ ) {
					if ( results[i].getAttribute("name") === match[1] ) {
						ret.push( results[i] );
					}
				}

				return ret.length === 0 ? null : ret;
			}
		},

		TAG: function( match, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( match[1] );
			}
		}
	},
	preFilter: {
		CLASS: function( match, curLoop, inplace, result, not, isXML ) {
			match = " " + match[1].replace( rBackslash, "" ) + " ";

			if ( isXML ) {
				return match;
			}

			for ( var i = 0, elem; (elem = curLoop[i]) != null; i++ ) {
				if ( elem ) {
					if ( not ^ (elem.className && (" " + elem.className + " ").replace(/[\t\n\r]/g, " ").indexOf(match) >= 0) ) {
						if ( !inplace ) {
							result.push( elem );
						}

					} else if ( inplace ) {
						curLoop[i] = false;
					}
				}
			}

			return false;
		},

		ID: function( match ) {
			return match[1].replace( rBackslash, "" );
		},

		TAG: function( match, curLoop ) {
			return match[1].replace( rBackslash, "" ).toLowerCase();
		},

		CHILD: function( match ) {
			if ( match[1] === "nth" ) {
				if ( !match[2] ) {
					Sizzle.error( match[0] );
				}

				match[2] = match[2].replace(/^\+|\s*/g, '');

				// parse equations like 'even', 'odd', '5', '2n', '3n+2', '4n-1', '-n+6'
				var test = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(
					match[2] === "even" && "2n" || match[2] === "odd" && "2n+1" ||
					!/\D/.test( match[2] ) && "0n+" + match[2] || match[2]);

				// calculate the numbers (first)n+(last) including if they are negative
				match[2] = (test[1] + (test[2] || 1)) - 0;
				match[3] = test[3] - 0;
			}
			else if ( match[2] ) {
				Sizzle.error( match[0] );
			}

			// TODO: Move to normal caching system
			match[0] = done++;

			return match;
		},

		ATTR: function( match, curLoop, inplace, result, not, isXML ) {
			var name = match[1] = match[1].replace( rBackslash, "" );

			if ( !isXML && Expr.attrMap[name] ) {
				match[1] = Expr.attrMap[name];
			}

			// Handle if an un-quoted value was used
			match[4] = ( match[4] || match[5] || "" ).replace( rBackslash, "" );

			if ( match[2] === "~=" ) {
				match[4] = " " + match[4] + " ";
			}

			return match;
		},

		PSEUDO: function( match, curLoop, inplace, result, not ) {
			if ( match[1] === "not" ) {
				// If we're dealing with a complex expression, or a simple one
				if ( ( chunker.exec(match[3]) || "" ).length > 1 || /^\w/.test(match[3]) ) {
					match[3] = Sizzle(match[3], null, null, curLoop);

				} else {
					var ret = Sizzle.filter(match[3], curLoop, inplace, true ^ not);

					if ( !inplace ) {
						result.push.apply( result, ret );
					}

					return false;
				}

			} else if ( Expr.match.POS.test( match[0] ) || Expr.match.CHILD.test( match[0] ) ) {
				return true;
			}

			return match;
		},

		POS: function( match ) {
			match.unshift( true );

			return match;
		}
	},

	filters: {
		enabled: function( elem ) {
			return elem.disabled === false && elem.type !== "hidden";
		},

		disabled: function( elem ) {
			return elem.disabled === true;
		},

		checked: function( elem ) {
			return elem.checked === true;
		},

		selected: function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		parent: function( elem ) {
			return !!elem.firstChild;
		},

		empty: function( elem ) {
			return !elem.firstChild;
		},

		has: function( elem, i, match ) {
			return !!Sizzle( match[3], elem ).length;
		},

		header: function( elem ) {
			return (/h\d/i).test( elem.nodeName );
		},

		text: function( elem ) {
			var attr = elem.getAttribute( "type" ), type = elem.type;
			// IE6 and 7 will map elem.type to 'text' for new HTML5 types (search, etc)
			// use getAttribute instead to test this case
			return elem.nodeName.toLowerCase() === "input" && "text" === type && ( attr === type || attr === null );
		},

		radio: function( elem ) {
			return elem.nodeName.toLowerCase() === "input" && "radio" === elem.type;
		},

		checkbox: function( elem ) {
			return elem.nodeName.toLowerCase() === "input" && "checkbox" === elem.type;
		},

		file: function( elem ) {
			return elem.nodeName.toLowerCase() === "input" && "file" === elem.type;
		},

		password: function( elem ) {
			return elem.nodeName.toLowerCase() === "input" && "password" === elem.type;
		},

		submit: function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return (name === "input" || name === "button") && "submit" === elem.type;
		},

		image: function( elem ) {
			return elem.nodeName.toLowerCase() === "input" && "image" === elem.type;
		},

		reset: function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return (name === "input" || name === "button") && "reset" === elem.type;
		},

		button: function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && "button" === elem.type || name === "button";
		},

		input: function( elem ) {
			return (/input|select|textarea|button/i).test( elem.nodeName );
		},

		focus: function( elem ) {
			return elem === elem.ownerDocument.activeElement;
		}
	},
	setFilters: {
		first: function( elem, i ) {
			return i === 0;
		},

		last: function( elem, i, match, array ) {
			return i === array.length - 1;
		},

		even: function( elem, i ) {
			return i % 2 === 0;
		},

		odd: function( elem, i ) {
			return i % 2 === 1;
		},

		lt: function( elem, i, match ) {
			return i < match[3] - 0;
		},

		gt: function( elem, i, match ) {
			return i > match[3] - 0;
		},

		nth: function( elem, i, match ) {
			return match[3] - 0 === i;
		},

		eq: function( elem, i, match ) {
			return match[3] - 0 === i;
		}
	},
	filter: {
		PSEUDO: function( elem, match, i, array ) {
			var name = match[1],
				filter = Expr.filters[ name ];

			if ( filter ) {
				return filter( elem, i, match, array );

			} else if ( name === "contains" ) {
				return (elem.textContent || elem.innerText || getText([ elem ]) || "").indexOf(match[3]) >= 0;

			} else if ( name === "not" ) {
				var not = match[3];

				for ( var j = 0, l = not.length; j < l; j++ ) {
					if ( not[j] === elem ) {
						return false;
					}
				}

				return true;

			} else {
				Sizzle.error( name );
			}
		},

		CHILD: function( elem, match ) {
			var first, last,
				doneName, parent, cache,
				count, diff,
				type = match[1],
				node = elem;

			switch ( type ) {
				case "only":
				case "first":
					while ( (node = node.previousSibling) ) {
						if ( node.nodeType === 1 ) {
							return false;
						}
					}

					if ( type === "first" ) {
						return true;
					}

					node = elem;

					/* falls through */
				case "last":
					while ( (node = node.nextSibling) ) {
						if ( node.nodeType === 1 ) {
							return false;
						}
					}

					return true;

				case "nth":
					first = match[2];
					last = match[3];

					if ( first === 1 && last === 0 ) {
						return true;
					}

					doneName = match[0];
					parent = elem.parentNode;

					if ( parent && (parent[ expando ] !== doneName || !elem.nodeIndex) ) {
						count = 0;

						for ( node = parent.firstChild; node; node = node.nextSibling ) {
							if ( node.nodeType === 1 ) {
								node.nodeIndex = ++count;
							}
						}

						parent[ expando ] = doneName;
					}

					diff = elem.nodeIndex - last;

					if ( first === 0 ) {
						return diff === 0;

					} else {
						return ( diff % first === 0 && diff / first >= 0 );
					}
			}
		},

		ID: function( elem, match ) {
			return elem.nodeType === 1 && elem.getAttribute("id") === match;
		},

		TAG: function( elem, match ) {
			return (match === "*" && elem.nodeType === 1) || !!elem.nodeName && elem.nodeName.toLowerCase() === match;
		},

		CLASS: function( elem, match ) {
			return (" " + (elem.className || elem.getAttribute("class")) + " ")
				.indexOf( match ) > -1;
		},

		ATTR: function( elem, match ) {
			var name = match[1],
				result = Sizzle.attr ?
					Sizzle.attr( elem, name ) :
					Expr.attrHandle[ name ] ?
					Expr.attrHandle[ name ]( elem ) :
					elem[ name ] != null ?
						elem[ name ] :
						elem.getAttribute( name ),
				value = result + "",
				type = match[2],
				check = match[4];

			return result == null ?
				type === "!=" :
				!type && Sizzle.attr ?
				result != null :
				type === "=" ?
				value === check :
				type === "*=" ?
				value.indexOf(check) >= 0 :
				type === "~=" ?
				(" " + value + " ").indexOf(check) >= 0 :
				!check ?
				value && result !== false :
				type === "!=" ?
				value !== check :
				type === "^=" ?
				value.indexOf(check) === 0 :
				type === "$=" ?
				value.substr(value.length - check.length) === check :
				type === "|=" ?
				value === check || value.substr(0, check.length + 1) === check + "-" :
				false;
		},

		POS: function( elem, match, i, array ) {
			var name = match[2],
				filter = Expr.setFilters[ name ];

			if ( filter ) {
				return filter( elem, i, match, array );
			}
		}
	}
};

var origPOS = Expr.match.POS,
	fescape = function(all, num){
		return "\\" + (num - 0 + 1);
	};

for ( var type in Expr.match ) {
	Expr.match[ type ] = new RegExp( Expr.match[ type ].source + (/(?![^\[]*\])(?![^\(]*\))/.source) );
	Expr.leftMatch[ type ] = new RegExp( /(^(?:.|\r|\n)*?)/.source + Expr.match[ type ].source.replace(/\\(\d+)/g, fescape) );
}
// Expose origPOS
// "global" as in regardless of relation to brackets/parens
Expr.match.globalPOS = origPOS;

var makeArray = function( array, results ) {
	array = Array.prototype.slice.call( array, 0 );

	if ( results ) {
		results.push.apply( results, array );
		return results;
	}

	return array;
};

// Perform a simple check to determine if the browser is capable of
// converting a NodeList to an array using builtin methods.
// Also verifies that the returned array holds DOM nodes
// (which is not the case in the Blackberry browser)
try {
	Array.prototype.slice.call( document.documentElement.childNodes, 0 )[0].nodeType;

// Provide a fallback method if it does not work
} catch( e ) {
	makeArray = function( array, results ) {
		var i = 0,
			ret = results || [];

		if ( toString.call(array) === "[object Array]" ) {
			Array.prototype.push.apply( ret, array );

		} else {
			if ( typeof array.length === "number" ) {
				for ( var l = array.length; i < l; i++ ) {
					ret.push( array[i] );
				}

			} else {
				for ( ; array[i]; i++ ) {
					ret.push( array[i] );
				}
			}
		}

		return ret;
	};
}

var sortOrder, siblingCheck;

if ( document.documentElement.compareDocumentPosition ) {
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		if ( !a.compareDocumentPosition || !b.compareDocumentPosition ) {
			return a.compareDocumentPosition ? -1 : 1;
		}

		return a.compareDocumentPosition(b) & 4 ? -1 : 1;
	};

} else {
	sortOrder = function( a, b ) {
		// The nodes are identical, we can exit early
		if ( a === b ) {
			hasDuplicate = true;
			return 0;

		// Fallback to using sourceIndex (in IE) if it's available on both nodes
		} else if ( a.sourceIndex && b.sourceIndex ) {
			return a.sourceIndex - b.sourceIndex;
		}

		var al, bl,
			ap = [],
			bp = [],
			aup = a.parentNode,
			bup = b.parentNode,
			cur = aup;

		// If the nodes are siblings (or identical) we can do a quick check
		if ( aup === bup ) {
			return siblingCheck( a, b );

		// If no parents were found then the nodes are disconnected
		} else if ( !aup ) {
			return -1;

		} else if ( !bup ) {
			return 1;
		}

		// Otherwise they're somewhere else in the tree so we need
		// to build up a full list of the parentNodes for comparison
		while ( cur ) {
			ap.unshift( cur );
			cur = cur.parentNode;
		}

		cur = bup;

		while ( cur ) {
			bp.unshift( cur );
			cur = cur.parentNode;
		}

		al = ap.length;
		bl = bp.length;

		// Start walking down the tree looking for a discrepancy
		for ( var i = 0; i < al && i < bl; i++ ) {
			if ( ap[i] !== bp[i] ) {
				return siblingCheck( ap[i], bp[i] );
			}
		}

		// We ended someplace up the tree so do a sibling check
		return i === al ?
			siblingCheck( a, bp[i], -1 ) :
			siblingCheck( ap[i], b, 1 );
	};

	siblingCheck = function( a, b, ret ) {
		if ( a === b ) {
			return ret;
		}

		var cur = a.nextSibling;

		while ( cur ) {
			if ( cur === b ) {
				return -1;
			}

			cur = cur.nextSibling;
		}

		return 1;
	};
}

// Check to see if the browser returns elements by name when
// querying by getElementById (and provide a workaround)
(function(){
	// We're going to inject a fake input element with a specified name
	var form = document.createElement("div"),
		id = "script" + (new Date()).getTime(),
		root = document.documentElement;

	form.innerHTML = "<a name='" + id + "'/>";

	// Inject it into the root element, check its status, and remove it quickly
	root.insertBefore( form, root.firstChild );

	// The workaround has to do additional checks after a getElementById
	// Which slows things down for other browsers (hence the branching)
	if ( document.getElementById( id ) ) {
		Expr.find.ID = function( match, context, isXML ) {
			if ( typeof context.getElementById !== "undefined" && !isXML ) {
				var m = context.getElementById(match[1]);

				return m ?
					m.id === match[1] || typeof m.getAttributeNode !== "undefined" && m.getAttributeNode("id").nodeValue === match[1] ?
						[m] :
						undefined :
					[];
			}
		};

		Expr.filter.ID = function( elem, match ) {
			var node = typeof elem.getAttributeNode !== "undefined" && elem.getAttributeNode("id");

			return elem.nodeType === 1 && node && node.nodeValue === match;
		};
	}

	root.removeChild( form );

	// release memory in IE
	root = form = null;
})();

(function(){
	// Check to see if the browser returns only elements
	// when doing getElementsByTagName("*")

	// Create a fake element
	var div = document.createElement("div");
	div.appendChild( document.createComment("") );

	// Make sure no comments are found
	if ( div.getElementsByTagName("*").length > 0 ) {
		Expr.find.TAG = function( match, context ) {
			var results = context.getElementsByTagName( match[1] );

			// Filter out possible comments
			if ( match[1] === "*" ) {
				var tmp = [];

				for ( var i = 0; results[i]; i++ ) {
					if ( results[i].nodeType === 1 ) {
						tmp.push( results[i] );
					}
				}

				results = tmp;
			}

			return results;
		};
	}

	// Check to see if an attribute returns normalized href attributes
	div.innerHTML = "<a href='#'></a>";

	if ( div.firstChild && typeof div.firstChild.getAttribute !== "undefined" &&
			div.firstChild.getAttribute("href") !== "#" ) {

		Expr.attrHandle.href = function( elem ) {
			return elem.getAttribute( "href", 2 );
		};
	}

	// release memory in IE
	div = null;
})();

if ( document.querySelectorAll ) {
	(function(){
		var oldSizzle = Sizzle,
			div = document.createElement("div"),
			id = "__sizzle__";

		div.innerHTML = "<p class='TEST'></p>";

		// Safari can't handle uppercase or unicode characters when
		// in quirks mode.
		if ( div.querySelectorAll && div.querySelectorAll(".TEST").length === 0 ) {
			return;
		}

		Sizzle = function( query, context, extra, seed ) {
			context = context || document;

			// Only use querySelectorAll on non-XML documents
			// (ID selectors don't work in non-HTML documents)
			if ( !seed && !Sizzle.isXML(context) ) {
				// See if we find a selector to speed up
				var match = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec( query );

				if ( match && (context.nodeType === 1 || context.nodeType === 9) ) {
					// Speed-up: Sizzle("TAG")
					if ( match[1] ) {
						return makeArray( context.getElementsByTagName( query ), extra );

					// Speed-up: Sizzle(".CLASS")
					} else if ( match[2] && Expr.find.CLASS && context.getElementsByClassName ) {
						return makeArray( context.getElementsByClassName( match[2] ), extra );
					}
				}

				if ( context.nodeType === 9 ) {
					// Speed-up: Sizzle("body")
					// The body element only exists once, optimize finding it
					if ( query === "body" && context.body ) {
						return makeArray( [ context.body ], extra );

					// Speed-up: Sizzle("#ID")
					} else if ( match && match[3] ) {
						var elem = context.getElementById( match[3] );

						// Check parentNode to catch when Blackberry 4.6 returns
						// nodes that are no longer in the document #6963
						if ( elem && elem.parentNode ) {
							// Handle the case where IE and Opera return items
							// by name instead of ID
							if ( elem.id === match[3] ) {
								return makeArray( [ elem ], extra );
							}

						} else {
							return makeArray( [], extra );
						}
					}

					try {
						return makeArray( context.querySelectorAll(query), extra );
					} catch(qsaError) {}

				// qSA works strangely on Element-rooted queries
				// We can work around this by specifying an extra ID on the root
				// and working up from there (Thanks to Andrew Dupont for the technique)
				// IE 8 doesn't work on object elements
				} else if ( context.nodeType === 1 && context.nodeName.toLowerCase() !== "object" ) {
					var oldContext = context,
						old = context.getAttribute( "id" ),
						nid = old || id,
						hasParent = context.parentNode,
						relativeHierarchySelector = /^\s*[+~]/.test( query );

					if ( !old ) {
						context.setAttribute( "id", nid );
					} else {
						nid = nid.replace( /'/g, "\\$&" );
					}
					if ( relativeHierarchySelector && hasParent ) {
						context = context.parentNode;
					}

					try {
						if ( !relativeHierarchySelector || hasParent ) {
							return makeArray( context.querySelectorAll( "[id='" + nid + "'] " + query ), extra );
						}

					} catch(pseudoError) {
					} finally {
						if ( !old ) {
							oldContext.removeAttribute( "id" );
						}
					}
				}
			}

			return oldSizzle(query, context, extra, seed);
		};

		for ( var prop in oldSizzle ) {
			Sizzle[ prop ] = oldSizzle[ prop ];
		}

		// release memory in IE
		div = null;
	})();
}

(function(){
	var html = document.documentElement,
		matches = html.matchesSelector || html.mozMatchesSelector || html.webkitMatchesSelector || html.msMatchesSelector;

	if ( matches ) {
		// Check to see if it's possible to do matchesSelector
		// on a disconnected node (IE 9 fails this)
		var disconnectedMatch = !matches.call( document.createElement( "div" ), "div" ),
			pseudoWorks = false;

		try {
			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( document.documentElement, "[test!='']:sizzle" );

		} catch( pseudoError ) {
			pseudoWorks = true;
		}

		Sizzle.matchesSelector = function( node, expr ) {
			// Make sure that attribute selectors are quoted
			expr = expr.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");

			if ( !Sizzle.isXML( node ) ) {
				try {
					if ( pseudoWorks || !Expr.match.PSEUDO.test( expr ) && !/!=/.test( expr ) ) {
						var ret = matches.call( node, expr );

						// IE 9's matchesSelector returns false on disconnected nodes
						if ( ret || !disconnectedMatch ||
								// As well, disconnected nodes are said to be in a document
								// fragment in IE 9, so check for that
								node.document && node.document.nodeType !== 11 ) {
							return ret;
						}
					}
				} catch(e) {}
			}

			return Sizzle(expr, null, null, [node]).length > 0;
		};
	}
})();

(function(){
	var div = document.createElement("div");

	div.innerHTML = "<div class='test e'></div><div class='test'></div>";

	// Opera can't find a second classname (in 9.6)
	// Also, make sure that getElementsByClassName actually exists
	if ( !div.getElementsByClassName || div.getElementsByClassName("e").length === 0 ) {
		return;
	}

	// Safari caches class attributes, doesn't catch changes (in 3.2)
	div.lastChild.className = "e";

	if ( div.getElementsByClassName("e").length === 1 ) {
		return;
	}

	Expr.order.splice(1, 0, "CLASS");
	Expr.find.CLASS = function( match, context, isXML ) {
		if ( typeof context.getElementsByClassName !== "undefined" && !isXML ) {
			return context.getElementsByClassName(match[1]);
		}
	};

	// release memory in IE
	div = null;
})();

function dirNodeCheck( dir, cur, doneName, checkSet, nodeCheck, isXML ) {
	for ( var i = 0, l = checkSet.length; i < l; i++ ) {
		var elem = checkSet[i];

		if ( elem ) {
			var match = false;

			elem = elem[dir];

			while ( elem ) {
				if ( elem[ expando ] === doneName ) {
					match = checkSet[elem.sizset];
					break;
				}

				if ( elem.nodeType === 1 && !isXML ){
					elem[ expando ] = doneName;
					elem.sizset = i;
				}

				if ( elem.nodeName.toLowerCase() === cur ) {
					match = elem;
					break;
				}

				elem = elem[dir];
			}

			checkSet[i] = match;
		}
	}
}

function dirCheck( dir, cur, doneName, checkSet, nodeCheck, isXML ) {
	for ( var i = 0, l = checkSet.length; i < l; i++ ) {
		var elem = checkSet[i];

		if ( elem ) {
			var match = false;

			elem = elem[dir];

			while ( elem ) {
				if ( elem[ expando ] === doneName ) {
					match = checkSet[elem.sizset];
					break;
				}

				if ( elem.nodeType === 1 ) {
					if ( !isXML ) {
						elem[ expando ] = doneName;
						elem.sizset = i;
					}

					if ( typeof cur !== "string" ) {
						if ( elem === cur ) {
							match = true;
							break;
						}

					} else if ( Sizzle.filter( cur, [elem] ).length > 0 ) {
						match = elem;
						break;
					}
				}

				elem = elem[dir];
			}

			checkSet[i] = match;
		}
	}
}

if ( document.documentElement.contains ) {
	Sizzle.contains = function( a, b ) {
		return a !== b && (a.contains ? a.contains(b) : true);
	};

} else if ( document.documentElement.compareDocumentPosition ) {
	Sizzle.contains = function( a, b ) {
		return !!(a.compareDocumentPosition(b) & 16);
	};

} else {
	Sizzle.contains = function() {
		return false;
	};
}

Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = (elem ? elem.ownerDocument || elem : 0).documentElement;

	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

var posProcess = function( selector, context, seed ) {
	var match,
		tmpSet = [],
		later = "",
		root = context.nodeType ? [context] : context;

	// Position selectors must be done after the filter
	// And so must :not(positional) so we move all PSEUDOs to the end
	while ( (match = Expr.match.PSEUDO.exec( selector )) ) {
		later += match[0];
		selector = selector.replace( Expr.match.PSEUDO, "" );
	}

	selector = Expr.relative[selector] ? selector + "*" : selector;

	for ( var i = 0, l = root.length; i < l; i++ ) {
		Sizzle( selector, root[i], tmpSet, seed );
	}

	return Sizzle.filter( later, tmpSet );
};

// EXPOSE
// Override sizzle attribute retrieval
Sizzle.attr = jQuery.attr;
Sizzle.selectors.attrMap = {};
jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;
jQuery.expr[":"] = jQuery.expr.filters;
jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;


})();


var runtil = /Until$/,
	rparentsprev = /^(?:parents|prevUntil|prevAll)/,
	// Note: This RegExp should be improved, or likely pulled from Sizzle
	rmultiselector = /,/,
	isSimple = /^.[^:#\[\.,]*$/,
	slice = Array.prototype.slice,
	POS = jQuery.expr.match.globalPOS,
	// methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend({
	find: function( selector ) {
		var self = this,
			i, l;

		if ( typeof selector !== "string" ) {
			return jQuery( selector ).filter(function() {
				for ( i = 0, l = self.length; i < l; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			});
		}

		var ret = this.pushStack( "", "find", selector ),
			length, n, r;

		for ( i = 0, l = this.length; i < l; i++ ) {
			length = ret.length;
			jQuery.find( selector, this[i], ret );

			if ( i > 0 ) {
				// Make sure that the results are unique
				for ( n = length; n < ret.length; n++ ) {
					for ( r = 0; r < length; r++ ) {
						if ( ret[r] === ret[n] ) {
							ret.splice(n--, 1);
							break;
						}
					}
				}
			}
		}

		return ret;
	},

	has: function( target ) {
		var targets = jQuery( target );
		return this.filter(function() {
			for ( var i = 0, l = targets.length; i < l; i++ ) {
				if ( jQuery.contains( this, targets[i] ) ) {
					return true;
				}
			}
		});
	},

	not: function( selector ) {
		return this.pushStack( winnow(this, selector, false), "not", selector);
	},

	filter: function( selector ) {
		return this.pushStack( winnow(this, selector, true), "filter", selector );
	},

	is: function( selector ) {
		return !!selector && (
			typeof selector === "string" ?
				// If this is a positional selector, check membership in the returned set
				// so $("p:first").is("p:last") won't return true for a doc with two "p".
				POS.test( selector ) ?
					jQuery( selector, this.context ).index( this[0] ) >= 0 :
					jQuery.filter( selector, this ).length > 0 :
				this.filter( selector ).length > 0 );
	},

	closest: function( selectors, context ) {
		var ret = [], i, l, cur = this[0];

		// Array (deprecated as of jQuery 1.7)
		if ( jQuery.isArray( selectors ) ) {
			var level = 1;

			while ( cur && cur.ownerDocument && cur !== context ) {
				for ( i = 0; i < selectors.length; i++ ) {

					if ( jQuery( cur ).is( selectors[ i ] ) ) {
						ret.push({ selector: selectors[ i ], elem: cur, level: level });
					}
				}

				cur = cur.parentNode;
				level++;
			}

			return ret;
		}

		// String
		var pos = POS.test( selectors ) || typeof selectors !== "string" ?
				jQuery( selectors, context || this.context ) :
				0;

		for ( i = 0, l = this.length; i < l; i++ ) {
			cur = this[i];

			while ( cur ) {
				if ( pos ? pos.index(cur) > -1 : jQuery.find.matchesSelector(cur, selectors) ) {
					ret.push( cur );
					break;

				} else {
					cur = cur.parentNode;
					if ( !cur || !cur.ownerDocument || cur === context || cur.nodeType === 11 ) {
						break;
					}
				}
			}
		}

		ret = ret.length > 1 ? jQuery.unique( ret ) : ret;

		return this.pushStack( ret, "closest", selectors );
	},

	// Determine the position of an element within
	// the matched set of elements
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[0] && this[0].parentNode ) ? this.prevAll().length : -1;
		}

		// index in selector
		if ( typeof elem === "string" ) {
			return jQuery.inArray( this[0], jQuery( elem ) );
		}

		// Locate the position of the desired element
		return jQuery.inArray(
			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[0] : elem, this );
	},

	add: function( selector, context ) {
		var set = typeof selector === "string" ?
				jQuery( selector, context ) :
				jQuery.makeArray( selector && selector.nodeType ? [ selector ] : selector ),
			all = jQuery.merge( this.get(), set );

		return this.pushStack( isDisconnected( set[0] ) || isDisconnected( all[0] ) ?
			all :
			jQuery.unique( all ) );
	},

	andSelf: function() {
		return this.add( this.prevObject );
	}
});

// A painfully simple check to see if an element is disconnected
// from a document (should be improved, where feasible).
function isDisconnected( node ) {
	return !node || !node.parentNode || node.parentNode.nodeType === 11;
}

jQuery.each({
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return jQuery.dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return jQuery.nth( elem, 2, "nextSibling" );
	},
	prev: function( elem ) {
		return jQuery.nth( elem, 2, "previousSibling" );
	},
	nextAll: function( elem ) {
		return jQuery.dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return jQuery.dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return jQuery.dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return jQuery.sibling( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return jQuery.sibling( elem.firstChild );
	},
	contents: function( elem ) {
		return jQuery.nodeName( elem, "iframe" ) ?
			elem.contentDocument || elem.contentWindow.document :
			jQuery.makeArray( elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var ret = jQuery.map( this, fn, until );

		if ( !runtil.test( name ) ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			ret = jQuery.filter( selector, ret );
		}

		ret = this.length > 1 && !guaranteedUnique[ name ] ? jQuery.unique( ret ) : ret;

		if ( (this.length > 1 || rmultiselector.test( selector )) && rparentsprev.test( name ) ) {
			ret = ret.reverse();
		}

		return this.pushStack( ret, name, slice.call( arguments ).join(",") );
	};
});

jQuery.extend({
	filter: function( expr, elems, not ) {
		if ( not ) {
			expr = ":not(" + expr + ")";
		}

		return elems.length === 1 ?
			jQuery.find.matchesSelector(elems[0], expr) ? [ elems[0] ] : [] :
			jQuery.find.matches(expr, elems);
	},

	dir: function( elem, dir, until ) {
		var matched = [],
			cur = elem[ dir ];

		while ( cur && cur.nodeType !== 9 && (until === undefined || cur.nodeType !== 1 || !jQuery( cur ).is( until )) ) {
			if ( cur.nodeType === 1 ) {
				matched.push( cur );
			}
			cur = cur[dir];
		}
		return matched;
	},

	nth: function( cur, result, dir, elem ) {
		result = result || 1;
		var num = 0;

		for ( ; cur; cur = cur[dir] ) {
			if ( cur.nodeType === 1 && ++num === result ) {
				break;
			}
		}

		return cur;
	},

	sibling: function( n, elem ) {
		var r = [];

		for ( ; n; n = n.nextSibling ) {
			if ( n.nodeType === 1 && n !== elem ) {
				r.push( n );
			}
		}

		return r;
	}
});

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, keep ) {

	// Can't pass null or undefined to indexOf in Firefox 4
	// Set to 0 to skip string check
	qualifier = qualifier || 0;

	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep(elements, function( elem, i ) {
			var retVal = !!qualifier.call( elem, i, elem );
			return retVal === keep;
		});

	} else if ( qualifier.nodeType ) {
		return jQuery.grep(elements, function( elem, i ) {
			return ( elem === qualifier ) === keep;
		});

	} else if ( typeof qualifier === "string" ) {
		var filtered = jQuery.grep(elements, function( elem ) {
			return elem.nodeType === 1;
		});

		if ( isSimple.test( qualifier ) ) {
			return jQuery.filter(qualifier, filtered, !keep);
		} else {
			qualifier = jQuery.filter( qualifier, filtered );
		}
	}

	return jQuery.grep(elements, function( elem, i ) {
		return ( jQuery.inArray( elem, qualifier ) >= 0 ) === keep;
	});
}




function createSafeFragment( document ) {
	var list = nodeNames.split( "|" ),
	safeFrag = document.createDocumentFragment();

	if ( safeFrag.createElement ) {
		while ( list.length ) {
			safeFrag.createElement(
				list.pop()
			);
		}
	}
	return safeFrag;
}

var nodeNames = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|" +
		"header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
	rinlinejQuery = / jQuery\d+="(?:\d+|null)"/g,
	rleadingWhitespace = /^\s+/,
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
	rtagName = /<([\w:]+)/,
	rtbody = /<tbody/i,
	rhtml = /<|&#?\w+;/,
	rnoInnerhtml = /<(?:script|style)/i,
	rnocache = /<(?:script|object|embed|option|style)/i,
	rnoshimcache = new RegExp("<(?:" + nodeNames + ")[\\s/>]", "i"),
	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptType = /\/(java|ecma)script/i,
	rcleanScript = /^\s*<!(?:\[CDATA\[|\-\-)/,
	wrapMap = {
		option: [ 1, "<select multiple='multiple'>", "</select>" ],
		legend: [ 1, "<fieldset>", "</fieldset>" ],
		thead: [ 1, "<table>", "</table>" ],
		tr: [ 2, "<table><tbody>", "</tbody></table>" ],
		td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
		col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
		area: [ 1, "<map>", "</map>" ],
		_default: [ 0, "", "" ]
	},
	safeFragment = createSafeFragment( document );

wrapMap.optgroup = wrapMap.option;
wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;

// IE can't serialize <link> and <script> tags normally
if ( !jQuery.support.htmlSerialize ) {
	wrapMap._default = [ 1, "div<div>", "</div>" ];
}

jQuery.fn.extend({
	text: function( value ) {
		return jQuery.access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().append( ( this[0] && this[0].ownerDocument || document ).createTextNode( value ) );
		}, null, value, arguments.length );
	},

	wrapAll: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapAll( html.call(this, i) );
			});
		}

		if ( this[0] ) {
			// The elements to wrap the target around
			var wrap = jQuery( html, this[0].ownerDocument ).eq(0).clone(true);

			if ( this[0].parentNode ) {
				wrap.insertBefore( this[0] );
			}

			wrap.map(function() {
				var elem = this;

				while ( elem.firstChild && elem.firstChild.nodeType === 1 ) {
					elem = elem.firstChild;
				}

				return elem;
			}).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each(function(i) {
				jQuery(this).wrapInner( html.call(this, i) );
			});
		}

		return this.each(function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		});
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each(function(i) {
			jQuery( this ).wrapAll( isFunction ? html.call(this, i) : html );
		});
	},

	unwrap: function() {
		return this.parent().each(function() {
			if ( !jQuery.nodeName( this, "body" ) ) {
				jQuery( this ).replaceWith( this.childNodes );
			}
		}).end();
	},

	append: function() {
		return this.domManip(arguments, true, function( elem ) {
			if ( this.nodeType === 1 ) {
				this.appendChild( elem );
			}
		});
	},

	prepend: function() {
		return this.domManip(arguments, true, function( elem ) {
			if ( this.nodeType === 1 ) {
				this.insertBefore( elem, this.firstChild );
			}
		});
	},

	before: function() {
		if ( this[0] && this[0].parentNode ) {
			return this.domManip(arguments, false, function( elem ) {
				this.parentNode.insertBefore( elem, this );
			});
		} else if ( arguments.length ) {
			var set = jQuery.clean( arguments );
			set.push.apply( set, this.toArray() );
			return this.pushStack( set, "before", arguments );
		}
	},

	after: function() {
		if ( this[0] && this[0].parentNode ) {
			return this.domManip(arguments, false, function( elem ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			});
		} else if ( arguments.length ) {
			var set = this.pushStack( this, "after", arguments );
			set.push.apply( set, jQuery.clean(arguments) );
			return set;
		}
	},

	// keepData is for internal use only--do not document
	remove: function( selector, keepData ) {
		for ( var i = 0, elem; (elem = this[i]) != null; i++ ) {
			if ( !selector || jQuery.filter( selector, [ elem ] ).length ) {
				if ( !keepData && elem.nodeType === 1 ) {
					jQuery.cleanData( elem.getElementsByTagName("*") );
					jQuery.cleanData( [ elem ] );
				}

				if ( elem.parentNode ) {
					elem.parentNode.removeChild( elem );
				}
			}
		}

		return this;
	},

	empty: function() {
		for ( var i = 0, elem; (elem = this[i]) != null; i++ ) {
			// Remove element nodes and prevent memory leaks
			if ( elem.nodeType === 1 ) {
				jQuery.cleanData( elem.getElementsByTagName("*") );
			}

			// Remove any remaining nodes
			while ( elem.firstChild ) {
				elem.removeChild( elem.firstChild );
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function () {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		});
	},

	html: function( value ) {
		return jQuery.access( this, function( value ) {
			var elem = this[0] || {},
				i = 0,
				l = this.length;

			if ( value === undefined ) {
				return elem.nodeType === 1 ?
					elem.innerHTML.replace( rinlinejQuery, "" ) :
					null;
			}


			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				( jQuery.support.leadingWhitespace || !rleadingWhitespace.test( value ) ) &&
				!wrapMap[ ( rtagName.exec( value ) || ["", ""] )[1].toLowerCase() ] ) {

				value = value.replace( rxhtmlTag, "<$1></$2>" );

				try {
					for (; i < l; i++ ) {
						// Remove element nodes and prevent memory leaks
						elem = this[i] || {};
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( elem.getElementsByTagName( "*" ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch(e) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function( value ) {
		if ( this[0] && this[0].parentNode ) {
			// Make sure that the elements are removed from the DOM before they are inserted
			// this can help fix replacing a parent with child elements
			if ( jQuery.isFunction( value ) ) {
				return this.each(function(i) {
					var self = jQuery(this), old = self.html();
					self.replaceWith( value.call( this, i, old ) );
				});
			}

			if ( typeof value !== "string" ) {
				value = jQuery( value ).detach();
			}

			return this.each(function() {
				var next = this.nextSibling,
					parent = this.parentNode;

				jQuery( this ).remove();

				if ( next ) {
					jQuery(next).before( value );
				} else {
					jQuery(parent).append( value );
				}
			});
		} else {
			return this.length ?
				this.pushStack( jQuery(jQuery.isFunction(value) ? value() : value), "replaceWith", value ) :
				this;
		}
	},

	detach: function( selector ) {
		return this.remove( selector, true );
	},

	domManip: function( args, table, callback ) {
		var results, first, fragment, parent,
			value = args[0],
			scripts = [];

		// We can't cloneNode fragments that contain checked, in WebKit
		if ( !jQuery.support.checkClone && arguments.length === 3 && typeof value === "string" && rchecked.test( value ) ) {
			return this.each(function() {
				jQuery(this).domManip( args, table, callback, true );
			});
		}

		if ( jQuery.isFunction(value) ) {
			return this.each(function(i) {
				var self = jQuery(this);
				args[0] = value.call(this, i, table ? self.html() : undefined);
				self.domManip( args, table, callback );
			});
		}

		if ( this[0] ) {
			parent = value && value.parentNode;

			// If we're in a fragment, just use that instead of building a new one
			if ( jQuery.support.parentNode && parent && parent.nodeType === 11 && parent.childNodes.length === this.length ) {
				results = { fragment: parent };

			} else {
				results = jQuery.buildFragment( args, this, scripts );
			}

			fragment = results.fragment;

			if ( fragment.childNodes.length === 1 ) {
				first = fragment = fragment.firstChild;
			} else {
				first = fragment.firstChild;
			}

			if ( first ) {
				table = table && jQuery.nodeName( first, "tr" );

				for ( var i = 0, l = this.length, lastIndex = l - 1; i < l; i++ ) {
					callback.call(
						table ?
							root(this[i], first) :
							this[i],
						// Make sure that we do not leak memory by inadvertently discarding
						// the original fragment (which might have attached data) instead of
						// using it; in addition, use the original fragment object for the last
						// item instead of first because it can end up being emptied incorrectly
						// in certain situations (Bug #8070).
						// Fragments from the fragment cache must always be cloned and never used
						// in place.
						results.cacheable || ( l > 1 && i < lastIndex ) ?
							jQuery.clone( fragment, true, true ) :
							fragment
					);
				}
			}

			if ( scripts.length ) {
				jQuery.each( scripts, function( i, elem ) {
					if ( elem.src ) {
						jQuery.ajax({
							type: "GET",
							global: false,
							url: elem.src,
							async: false,
							dataType: "script"
						});
					} else {
						jQuery.globalEval( ( elem.text || elem.textContent || elem.innerHTML || "" ).replace( rcleanScript, "/*$0*/" ) );
					}

					if ( elem.parentNode ) {
						elem.parentNode.removeChild( elem );
					}
				});
			}
		}

		return this;
	}
});

function root( elem, cur ) {
	return jQuery.nodeName(elem, "table") ?
		(elem.getElementsByTagName("tbody")[0] ||
		elem.appendChild(elem.ownerDocument.createElement("tbody"))) :
		elem;
}

function cloneCopyEvent( src, dest ) {

	if ( dest.nodeType !== 1 || !jQuery.hasData( src ) ) {
		return;
	}

	var type, i, l,
		oldData = jQuery._data( src ),
		curData = jQuery._data( dest, oldData ),
		events = oldData.events;

	if ( events ) {
		delete curData.handle;
		curData.events = {};

		for ( type in events ) {
			for ( i = 0, l = events[ type ].length; i < l; i++ ) {
				jQuery.event.add( dest, type, events[ type ][ i ] );
			}
		}
	}

	// make the cloned public data object a copy from the original
	if ( curData.data ) {
		curData.data = jQuery.extend( {}, curData.data );
	}
}

function cloneFixAttributes( src, dest ) {
	var nodeName;

	// We do not need to do anything for non-Elements
	if ( dest.nodeType !== 1 ) {
		return;
	}

	// clearAttributes removes the attributes, which we don't want,
	// but also removes the attachEvent events, which we *do* want
	if ( dest.clearAttributes ) {
		dest.clearAttributes();
	}

	// mergeAttributes, in contrast, only merges back on the
	// original attributes, not the events
	if ( dest.mergeAttributes ) {
		dest.mergeAttributes( src );
	}

	nodeName = dest.nodeName.toLowerCase();

	// IE6-8 fail to clone children inside object elements that use
	// the proprietary classid attribute value (rather than the type
	// attribute) to identify the type of content to display
	if ( nodeName === "object" ) {
		dest.outerHTML = src.outerHTML;

	} else if ( nodeName === "input" && (src.type === "checkbox" || src.type === "radio") ) {
		// IE6-8 fails to persist the checked state of a cloned checkbox
		// or radio button. Worse, IE6-7 fail to give the cloned element
		// a checked appearance if the defaultChecked value isn't also set
		if ( src.checked ) {
			dest.defaultChecked = dest.checked = src.checked;
		}

		// IE6-7 get confused and end up setting the value of a cloned
		// checkbox/radio button to an empty string instead of "on"
		if ( dest.value !== src.value ) {
			dest.value = src.value;
		}

	// IE6-8 fails to return the selected option to the default selected
	// state when cloning options
	} else if ( nodeName === "option" ) {
		dest.selected = src.defaultSelected;

	// IE6-8 fails to set the defaultValue to the correct value when
	// cloning other types of input fields
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;

	// IE blanks contents when cloning scripts
	} else if ( nodeName === "script" && dest.text !== src.text ) {
		dest.text = src.text;
	}

	// Event data gets referenced instead of copied if the expando
	// gets copied too
	dest.removeAttribute( jQuery.expando );

	// Clear flags for bubbling special change/submit events, they must
	// be reattached when the newly cloned events are first activated
	dest.removeAttribute( "_submit_attached" );
	dest.removeAttribute( "_change_attached" );
}

jQuery.buildFragment = function( args, nodes, scripts ) {
	var fragment, cacheable, cacheresults, doc,
	first = args[ 0 ];

	// nodes may contain either an explicit document object,
	// a jQuery collection or context object.
	// If nodes[0] contains a valid object to assign to doc
	if ( nodes && nodes[0] ) {
		doc = nodes[0].ownerDocument || nodes[0];
	}

	// Ensure that an attr object doesn't incorrectly stand in as a document object
	// Chrome and Firefox seem to allow this to occur and will throw exception
	// Fixes #8950
	if ( !doc.createDocumentFragment ) {
		doc = document;
	}

	// Only cache "small" (1/2 KB) HTML strings that are associated with the main document
	// Cloning options loses the selected state, so don't cache them
	// IE 6 doesn't like it when you put <object> or <embed> elements in a fragment
	// Also, WebKit does not clone 'checked' attributes on cloneNode, so don't cache
	// Lastly, IE6,7,8 will not correctly reuse cached fragments that were created from unknown elems #10501
	if ( args.length === 1 && typeof first === "string" && first.length < 512 && doc === document &&
		first.charAt(0) === "<" && !rnocache.test( first ) &&
		(jQuery.support.checkClone || !rchecked.test( first )) &&
		(jQuery.support.html5Clone || !rnoshimcache.test( first )) ) {

		cacheable = true;

		cacheresults = jQuery.fragments[ first ];
		if ( cacheresults && cacheresults !== 1 ) {
			fragment = cacheresults;
		}
	}

	if ( !fragment ) {
		fragment = doc.createDocumentFragment();
		jQuery.clean( args, doc, fragment, scripts );
	}

	if ( cacheable ) {
		jQuery.fragments[ first ] = cacheresults ? fragment : 1;
	}

	return { fragment: fragment, cacheable: cacheable };
};

jQuery.fragments = {};

jQuery.each({
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var ret = [],
			insert = jQuery( selector ),
			parent = this.length === 1 && this[0].parentNode;

		if ( parent && parent.nodeType === 11 && parent.childNodes.length === 1 && insert.length === 1 ) {
			insert[ original ]( this[0] );
			return this;

		} else {
			for ( var i = 0, l = insert.length; i < l; i++ ) {
				var elems = ( i > 0 ? this.clone(true) : this ).get();
				jQuery( insert[i] )[ original ]( elems );
				ret = ret.concat( elems );
			}

			return this.pushStack( ret, name, insert.selector );
		}
	};
});

function getAll( elem ) {
	if ( typeof elem.getElementsByTagName !== "undefined" ) {
		return elem.getElementsByTagName( "*" );

	} else if ( typeof elem.querySelectorAll !== "undefined" ) {
		return elem.querySelectorAll( "*" );

	} else {
		return [];
	}
}

// Used in clean, fixes the defaultChecked property
function fixDefaultChecked( elem ) {
	if ( elem.type === "checkbox" || elem.type === "radio" ) {
		elem.defaultChecked = elem.checked;
	}
}
// Finds all inputs and passes them to fixDefaultChecked
function findInputs( elem ) {
	var nodeName = ( elem.nodeName || "" ).toLowerCase();
	if ( nodeName === "input" ) {
		fixDefaultChecked( elem );
	// Skip scripts, get other children
	} else if ( nodeName !== "script" && typeof elem.getElementsByTagName !== "undefined" ) {
		jQuery.grep( elem.getElementsByTagName("input"), fixDefaultChecked );
	}
}

// Derived From: http://www.iecss.com/shimprove/javascript/shimprove.1-0-1.js
function shimCloneNode( elem ) {
	var div = document.createElement( "div" );
	safeFragment.appendChild( div );

	div.innerHTML = elem.outerHTML;
	return div.firstChild;
}

jQuery.extend({
	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var srcElements,
			destElements,
			i,
			// IE<=8 does not properly clone detached, unknown element nodes
			clone = jQuery.support.html5Clone || jQuery.isXMLDoc(elem) || !rnoshimcache.test( "<" + elem.nodeName + ">" ) ?
				elem.cloneNode( true ) :
				shimCloneNode( elem );

		if ( (!jQuery.support.noCloneEvent || !jQuery.support.noCloneChecked) &&
				(elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem) ) {
			// IE copies events bound via attachEvent when using cloneNode.
			// Calling detachEvent on the clone will also remove the events
			// from the original. In order to get around this, we use some
			// proprietary methods to clear the events. Thanks to MooTools
			// guys for this hotness.

			cloneFixAttributes( elem, clone );

			// Using Sizzle here is crazy slow, so we use getElementsByTagName instead
			srcElements = getAll( elem );
			destElements = getAll( clone );

			// Weird iteration because IE will replace the length property
			// with an element if you are cloning the body and one of the
			// elements on the page has a name or id of "length"
			for ( i = 0; srcElements[i]; ++i ) {
				// Ensure that the destination node is not null; Fixes #9587
				if ( destElements[i] ) {
					cloneFixAttributes( srcElements[i], destElements[i] );
				}
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			cloneCopyEvent( elem, clone );

			if ( deepDataAndEvents ) {
				srcElements = getAll( elem );
				destElements = getAll( clone );

				for ( i = 0; srcElements[i]; ++i ) {
					cloneCopyEvent( srcElements[i], destElements[i] );
				}
			}
		}

		srcElements = destElements = null;

		// Return the cloned set
		return clone;
	},

	clean: function( elems, context, fragment, scripts ) {
		var checkScriptType, script, j,
				ret = [];

		context = context || document;

		// !context.createElement fails in IE with an error but returns typeof 'object'
		if ( typeof context.createElement === "undefined" ) {
			context = context.ownerDocument || context[0] && context[0].ownerDocument || document;
		}

		for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
			if ( typeof elem === "number" ) {
				elem += "";
			}

			if ( !elem ) {
				continue;
			}

			// Convert html string into DOM nodes
			if ( typeof elem === "string" ) {
				if ( !rhtml.test( elem ) ) {
					elem = context.createTextNode( elem );
				} else {
					// Fix "XHTML"-style tags in all browsers
					elem = elem.replace(rxhtmlTag, "<$1></$2>");

					// Trim whitespace, otherwise indexOf won't work as expected
					var tag = ( rtagName.exec( elem ) || ["", ""] )[1].toLowerCase(),
						wrap = wrapMap[ tag ] || wrapMap._default,
						depth = wrap[0],
						div = context.createElement("div"),
						safeChildNodes = safeFragment.childNodes,
						remove;

					// Append wrapper element to unknown element safe doc fragment
					if ( context === document ) {
						// Use the fragment we've already created for this document
						safeFragment.appendChild( div );
					} else {
						// Use a fragment created with the owner document
						createSafeFragment( context ).appendChild( div );
					}

					// Go to html and back, then peel off extra wrappers
					div.innerHTML = wrap[1] + elem + wrap[2];

					// Move to the right depth
					while ( depth-- ) {
						div = div.lastChild;
					}

					// Remove IE's autoinserted <tbody> from table fragments
					if ( !jQuery.support.tbody ) {

						// String was a <table>, *may* have spurious <tbody>
						var hasBody = rtbody.test(elem),
							tbody = tag === "table" && !hasBody ?
								div.firstChild && div.firstChild.childNodes :

								// String was a bare <thead> or <tfoot>
								wrap[1] === "<table>" && !hasBody ?
									div.childNodes :
									[];

						for ( j = tbody.length - 1; j >= 0 ; --j ) {
							if ( jQuery.nodeName( tbody[ j ], "tbody" ) && !tbody[ j ].childNodes.length ) {
								tbody[ j ].parentNode.removeChild( tbody[ j ] );
							}
						}
					}

					// IE completely kills leading whitespace when innerHTML is used
					if ( !jQuery.support.leadingWhitespace && rleadingWhitespace.test( elem ) ) {
						div.insertBefore( context.createTextNode( rleadingWhitespace.exec(elem)[0] ), div.firstChild );
					}

					elem = div.childNodes;

					// Clear elements from DocumentFragment (safeFragment or otherwise)
					// to avoid hoarding elements. Fixes #11356
					if ( div ) {
						div.parentNode.removeChild( div );

						// Guard against -1 index exceptions in FF3.6
						if ( safeChildNodes.length > 0 ) {
							remove = safeChildNodes[ safeChildNodes.length - 1 ];

							if ( remove && remove.parentNode ) {
								remove.parentNode.removeChild( remove );
							}
						}
					}
				}
			}

			// Resets defaultChecked for any radios and checkboxes
			// about to be appended to the DOM in IE 6/7 (#8060)
			var len;
			if ( !jQuery.support.appendChecked ) {
				if ( elem[0] && typeof (len = elem.length) === "number" ) {
					for ( j = 0; j < len; j++ ) {
						findInputs( elem[j] );
					}
				} else {
					findInputs( elem );
				}
			}

			if ( elem.nodeType ) {
				ret.push( elem );
			} else {
				ret = jQuery.merge( ret, elem );
			}
		}

		if ( fragment ) {
			checkScriptType = function( elem ) {
				return !elem.type || rscriptType.test( elem.type );
			};
			for ( i = 0; ret[i]; i++ ) {
				script = ret[i];
				if ( scripts && jQuery.nodeName( script, "script" ) && (!script.type || rscriptType.test( script.type )) ) {
					scripts.push( script.parentNode ? script.parentNode.removeChild( script ) : script );

				} else {
					if ( script.nodeType === 1 ) {
						var jsTags = jQuery.grep( script.getElementsByTagName( "script" ), checkScriptType );

						ret.splice.apply( ret, [i + 1, 0].concat( jsTags ) );
					}
					fragment.appendChild( script );
				}
			}
		}

		return ret;
	},

	cleanData: function( elems ) {
		var data, id,
			cache = jQuery.cache,
			special = jQuery.event.special,
			deleteExpando = jQuery.support.deleteExpando;

		for ( var i = 0, elem; (elem = elems[i]) != null; i++ ) {
			if ( elem.nodeName && jQuery.noData[elem.nodeName.toLowerCase()] ) {
				continue;
			}

			id = elem[ jQuery.expando ];

			if ( id ) {
				data = cache[ id ];

				if ( data && data.events ) {
					for ( var type in data.events ) {
						if ( special[ type ] ) {
							jQuery.event.remove( elem, type );

						// This is a shortcut to avoid jQuery.event.remove's overhead
						} else {
							jQuery.removeEvent( elem, type, data.handle );
						}
					}

					// Null the DOM reference to avoid IE6/7/8 leak (#7054)
					if ( data.handle ) {
						data.handle.elem = null;
					}
				}

				if ( deleteExpando ) {
					delete elem[ jQuery.expando ];

				} else if ( elem.removeAttribute ) {
					elem.removeAttribute( jQuery.expando );
				}

				delete cache[ id ];
			}
		}
	}
});




var ralpha = /alpha\([^)]*\)/i,
	ropacity = /opacity=([^)]*)/,
	// fixed for IE9, see #8346
	rupper = /([A-Z]|^ms)/g,
	rnum = /^[\-+]?(?:\d*\.)?\d+$/i,
	rnumnonpx = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
	rrelNum = /^([\-+])=([\-+.\de]+)/,
	rmargin = /^margin/,

	cssShow = { position: "absolute", visibility: "hidden", display: "block" },

	// order is important!
	cssExpand = [ "Top", "Right", "Bottom", "Left" ],

	curCSS,

	getComputedStyle,
	currentStyle;

jQuery.fn.css = function( name, value ) {
	return jQuery.access( this, function( elem, name, value ) {
		return value !== undefined ?
			jQuery.style( elem, name, value ) :
			jQuery.css( elem, name );
	}, name, value, arguments.length > 1 );
};

jQuery.extend({
	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {
					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;

				} else {
					return elem.style.opacity;
				}
			}
		}
	},

	// Exclude the following css properties to add px
	cssNumber: {
		"fillOpacity": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		// normalize float css property
		"float": jQuery.support.cssFloat ? "cssFloat" : "styleFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {
		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, origName = jQuery.camelCase( name ),
			style = elem.style, hooks = jQuery.cssHooks[ origName ];

		name = jQuery.cssProps[ origName ] || origName;

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// convert relative number strings (+= or -=) to relative numbers. #7345
			if ( type === "string" && (ret = rrelNum.exec( value )) ) {
				value = ( +( ret[1] + 1) * +ret[2] ) + parseFloat( jQuery.css( elem, name ) );
				// Fixes bug #9237
				type = "number";
			}

			// Make sure that NaN and null values aren't set. See: #7116
			if ( value == null || type === "number" && isNaN( value ) ) {
				return;
			}

			// If a number was passed in, add 'px' to the (except for certain CSS properties)
			if ( type === "number" && !jQuery.cssNumber[ origName ] ) {
				value += "px";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !("set" in hooks) || (value = hooks.set( elem, value )) !== undefined ) {
				// Wrapped to prevent IE from throwing errors when 'invalid' values are provided
				// Fixes bug #5509
				try {
					style[ name ] = value;
				} catch(e) {}
			}

		} else {
			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks && (ret = hooks.get( elem, false, extra )) !== undefined ) {
				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra ) {
		var ret, hooks;

		// Make sure that we're working with the right name
		name = jQuery.camelCase( name );
		hooks = jQuery.cssHooks[ name ];
		name = jQuery.cssProps[ name ] || name;

		// cssFloat needs a special treatment
		if ( name === "cssFloat" ) {
			name = "float";
		}

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks && (ret = hooks.get( elem, true, extra )) !== undefined ) {
			return ret;

		// Otherwise, if a way to get the computed value exists, use that
		} else if ( curCSS ) {
			return curCSS( elem, name );
		}
	},

	// A method for quickly swapping in/out CSS properties to get correct calculations
	swap: function( elem, options, callback ) {
		var old = {},
			ret, name;

		// Remember the old values, and insert the new ones
		for ( name in options ) {
			old[ name ] = elem.style[ name ];
			elem.style[ name ] = options[ name ];
		}

		ret = callback.call( elem );

		// Revert the old values
		for ( name in options ) {
			elem.style[ name ] = old[ name ];
		}

		return ret;
	}
});

// DEPRECATED in 1.3, Use jQuery.css() instead
jQuery.curCSS = jQuery.css;

if ( document.defaultView && document.defaultView.getComputedStyle ) {
	getComputedStyle = function( elem, name ) {
		var ret, defaultView, computedStyle, width,
			style = elem.style;

		name = name.replace( rupper, "-$1" ).toLowerCase();

		if ( (defaultView = elem.ownerDocument.defaultView) &&
				(computedStyle = defaultView.getComputedStyle( elem, null )) ) {

			ret = computedStyle.getPropertyValue( name );
			if ( ret === "" && !jQuery.contains( elem.ownerDocument.documentElement, elem ) ) {
				ret = jQuery.style( elem, name );
			}
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// WebKit uses "computed value (percentage if specified)" instead of "used value" for margins
		// which is against the CSSOM draft spec: http://dev.w3.org/csswg/cssom/#resolved-values
		if ( !jQuery.support.pixelMargin && computedStyle && rmargin.test( name ) && rnumnonpx.test( ret ) ) {
			width = style.width;
			style.width = ret;
			ret = computedStyle.width;
			style.width = width;
		}

		return ret;
	};
}

if ( document.documentElement.currentStyle ) {
	currentStyle = function( elem, name ) {
		var left, rsLeft, uncomputed,
			ret = elem.currentStyle && elem.currentStyle[ name ],
			style = elem.style;

		// Avoid setting ret to empty string here
		// so we don't default to auto
		if ( ret == null && style && (uncomputed = style[ name ]) ) {
			ret = uncomputed;
		}

		// From the awesome hack by Dean Edwards
		// http://erik.eae.net/archives/2007/07/27/18.54.15/#comment-102291

		// If we're not dealing with a regular pixel number
		// but a number that has a weird ending, we need to convert it to pixels
		if ( rnumnonpx.test( ret ) ) {

			// Remember the original values
			left = style.left;
			rsLeft = elem.runtimeStyle && elem.runtimeStyle.left;

			// Put in the new values to get a computed value out
			if ( rsLeft ) {
				elem.runtimeStyle.left = elem.currentStyle.left;
			}
			style.left = name === "fontSize" ? "1em" : ret;
			ret = style.pixelLeft + "px";

			// Revert the changed values
			style.left = left;
			if ( rsLeft ) {
				elem.runtimeStyle.left = rsLeft;
			}
		}

		return ret === "" ? "auto" : ret;
	};
}

curCSS = getComputedStyle || currentStyle;

function getWidthOrHeight( elem, name, extra ) {

	// Start with offset property
	var val = name === "width" ? elem.offsetWidth : elem.offsetHeight,
		i = name === "width" ? 1 : 0,
		len = 4;

	if ( val > 0 ) {
		if ( extra !== "border" ) {
			for ( ; i < len; i += 2 ) {
				if ( !extra ) {
					val -= parseFloat( jQuery.css( elem, "padding" + cssExpand[ i ] ) ) || 0;
				}
				if ( extra === "margin" ) {
					val += parseFloat( jQuery.css( elem, extra + cssExpand[ i ] ) ) || 0;
				} else {
					val -= parseFloat( jQuery.css( elem, "border" + cssExpand[ i ] + "Width" ) ) || 0;
				}
			}
		}

		return val + "px";
	}

	// Fall back to computed then uncomputed css if necessary
	val = curCSS( elem, name );
	if ( val < 0 || val == null ) {
		val = elem.style[ name ];
	}

	// Computed unit is not pixels. Stop here and return.
	if ( rnumnonpx.test(val) ) {
		return val;
	}

	// Normalize "", auto, and prepare for extra
	val = parseFloat( val ) || 0;

	// Add padding, border, margin
	if ( extra ) {
		for ( ; i < len; i += 2 ) {
			val += parseFloat( jQuery.css( elem, "padding" + cssExpand[ i ] ) ) || 0;
			if ( extra !== "padding" ) {
				val += parseFloat( jQuery.css( elem, "border" + cssExpand[ i ] + "Width" ) ) || 0;
			}
			if ( extra === "margin" ) {
				val += parseFloat( jQuery.css( elem, extra + cssExpand[ i ]) ) || 0;
			}
		}
	}

	return val + "px";
}

jQuery.each([ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {
				if ( elem.offsetWidth !== 0 ) {
					return getWidthOrHeight( elem, name, extra );
				} else {
					return jQuery.swap( elem, cssShow, function() {
						return getWidthOrHeight( elem, name, extra );
					});
				}
			}
		},

		set: function( elem, value ) {
			return rnum.test( value ) ?
				value + "px" :
				value;
		}
	};
});

if ( !jQuery.support.opacity ) {
	jQuery.cssHooks.opacity = {
		get: function( elem, computed ) {
			// IE uses filters for opacity
			return ropacity.test( (computed && elem.currentStyle ? elem.currentStyle.filter : elem.style.filter) || "" ) ?
				( parseFloat( RegExp.$1 ) / 100 ) + "" :
				computed ? "1" : "";
		},

		set: function( elem, value ) {
			var style = elem.style,
				currentStyle = elem.currentStyle,
				opacity = jQuery.isNumeric( value ) ? "alpha(opacity=" + value * 100 + ")" : "",
				filter = currentStyle && currentStyle.filter || style.filter || "";

			// IE has trouble with opacity if it does not have layout
			// Force it by setting the zoom level
			style.zoom = 1;

			// if setting opacity to 1, and no other filters exist - attempt to remove filter attribute #6652
			if ( value >= 1 && jQuery.trim( filter.replace( ralpha, "" ) ) === "" ) {

				// Setting style.filter to null, "" & " " still leave "filter:" in the cssText
				// if "filter:" is present at all, clearType is disabled, we want to avoid this
				// style.removeAttribute is IE Only, but so apparently is this code path...
				style.removeAttribute( "filter" );

				// if there there is no filter style applied in a css rule, we are done
				if ( currentStyle && !currentStyle.filter ) {
					return;
				}
			}

			// otherwise, set new filter values
			style.filter = ralpha.test( filter ) ?
				filter.replace( ralpha, opacity ) :
				filter + " " + opacity;
		}
	};
}

jQuery(function() {
	// This hook cannot be added until DOM ready because the support test
	// for it is not run until after DOM ready
	if ( !jQuery.support.reliableMarginRight ) {
		jQuery.cssHooks.marginRight = {
			get: function( elem, computed ) {
				// WebKit Bug 13343 - getComputedStyle returns wrong value for margin-right
				// Work around by temporarily setting element display to inline-block
				return jQuery.swap( elem, { "display": "inline-block" }, function() {
					if ( computed ) {
						return curCSS( elem, "margin-right" );
					} else {
						return elem.style.marginRight;
					}
				});
			}
		};
	}
});

if ( jQuery.expr && jQuery.expr.filters ) {
	jQuery.expr.filters.hidden = function( elem ) {
		var width = elem.offsetWidth,
			height = elem.offsetHeight;

		return ( width === 0 && height === 0 ) || (!jQuery.support.reliableHiddenOffsets && ((elem.style && elem.style.display) || jQuery.css( elem, "display" )) === "none");
	};

	jQuery.expr.filters.visible = function( elem ) {
		return !jQuery.expr.filters.hidden( elem );
	};
}

// These hooks are used by animate to expand properties
jQuery.each({
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {

	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i,

				// assumes a single number if not a string
				parts = typeof value === "string" ? value.split(" ") : [ value ],
				expanded = {};

			for ( i = 0; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};
});




var r20 = /%20/g,
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rhash = /#.*$/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, // IE leaves an \r character at EOL
	rinput = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,
	rquery = /\?/,
	rscript = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
	rselectTextarea = /^(?:select|textarea)/i,
	rspacesAjax = /\s+/,
	rts = /([?&])_=[^&]*/,
	rurl = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,

	// Keep a copy of the old load method
	_load = jQuery.fn.load,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Document location
	ajaxLocation,

	// Document location segments
	ajaxLocParts,

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = ["*/"] + ["*"];

// #8138, IE may throw an exception when accessing
// a field from window.location if document.domain has been set
try {
	ajaxLocation = location.href;
} catch( e ) {
	// Use the href attribute of an A element
	// since IE will modify it given document.location
	ajaxLocation = document.createElement( "a" );
	ajaxLocation.href = "";
	ajaxLocation = ajaxLocation.href;
}

// Segment location into parts
ajaxLocParts = rurl.exec( ajaxLocation.toLowerCase() ) || [];

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		if ( jQuery.isFunction( func ) ) {
			var dataTypes = dataTypeExpression.toLowerCase().split( rspacesAjax ),
				i = 0,
				length = dataTypes.length,
				dataType,
				list,
				placeBefore;

			// For each dataType in the dataTypeExpression
			for ( ; i < length; i++ ) {
				dataType = dataTypes[ i ];
				// We control if we're asked to add before
				// any existing element
				placeBefore = /^\+/.test( dataType );
				if ( placeBefore ) {
					dataType = dataType.substr( 1 ) || "*";
				}
				list = structure[ dataType ] = structure[ dataType ] || [];
				// then we add to the structure accordingly
				list[ placeBefore ? "unshift" : "push" ]( func );
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR,
		dataType /* internal */, inspected /* internal */ ) {

	dataType = dataType || options.dataTypes[ 0 ];
	inspected = inspected || {};

	inspected[ dataType ] = true;

	var list = structure[ dataType ],
		i = 0,
		length = list ? list.length : 0,
		executeOnly = ( structure === prefilters ),
		selection;

	for ( ; i < length && ( executeOnly || !selection ); i++ ) {
		selection = list[ i ]( options, originalOptions, jqXHR );
		// If we got redirected to another dataType
		// we try there if executing only and not done already
		if ( typeof selection === "string" ) {
			if ( !executeOnly || inspected[ selection ] ) {
				selection = undefined;
			} else {
				options.dataTypes.unshift( selection );
				selection = inspectPrefiltersOrTransports(
						structure, options, originalOptions, jqXHR, selection, inspected );
			}
		}
	}
	// If we're only executing or nothing was selected
	// we try the catchall dataType if not done already
	if ( ( executeOnly || !selection ) && !inspected[ "*" ] ) {
		selection = inspectPrefiltersOrTransports(
				structure, options, originalOptions, jqXHR, "*", inspected );
	}
	// unnecessary when only executing (prefilters)
	// but it'll be ignored by the caller in that case
	return selection;
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};
	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}
}

jQuery.fn.extend({
	load: function( url, params, callback ) {
		if ( typeof url !== "string" && _load ) {
			return _load.apply( this, arguments );

		// Don't do a request if no elements are being requested
		} else if ( !this.length ) {
			return this;
		}

		var off = url.indexOf( " " );
		if ( off >= 0 ) {
			var selector = url.slice( off, url.length );
			url = url.slice( 0, off );
		}

		// Default to a GET request
		var type = "GET";

		// If the second parameter was provided
		if ( params ) {
			// If it's a function
			if ( jQuery.isFunction( params ) ) {
				// We assume that it's the callback
				callback = params;
				params = undefined;

			// Otherwise, build a param string
			} else if ( typeof params === "object" ) {
				params = jQuery.param( params, jQuery.ajaxSettings.traditional );
				type = "POST";
			}
		}

		var self = this;

		// Request the remote document
		jQuery.ajax({
			url: url,
			type: type,
			dataType: "html",
			data: params,
			// Complete callback (responseText is used internally)
			complete: function( jqXHR, status, responseText ) {
				// Store the response as specified by the jqXHR object
				responseText = jqXHR.responseText;
				// If successful, inject the HTML into all the matched elements
				if ( jqXHR.isResolved() ) {
					// #4825: Get the actual response in case
					// a dataFilter is present in ajaxSettings
					jqXHR.done(function( r ) {
						responseText = r;
					});
					// See if a selector was specified
					self.html( selector ?
						// Create a dummy div to hold the results
						jQuery("<div>")
							// inject the contents of the document in, removing the scripts
							// to avoid any 'Permission Denied' errors in IE
							.append(responseText.replace(rscript, ""))

							// Locate the specified elements
							.find(selector) :

						// If not, just inject the full result
						responseText );
				}

				if ( callback ) {
					self.each( callback, [ responseText, status, jqXHR ] );
				}
			}
		});

		return this;
	},

	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},

	serializeArray: function() {
		return this.map(function(){
			return this.elements ? jQuery.makeArray( this.elements ) : this;
		})
		.filter(function(){
			return this.name && !this.disabled &&
				( this.checked || rselectTextarea.test( this.nodeName ) ||
					rinput.test( this.type ) );
		})
		.map(function( i, elem ){
			var val = jQuery( this ).val();

			return val == null ?
				null :
				jQuery.isArray( val ) ?
					jQuery.map( val, function( val, i ){
						return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
					}) :
					{ name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		}).get();
	}
});

// Attach a bunch of functions for handling common AJAX events
jQuery.each( "ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split( " " ), function( i, o ){
	jQuery.fn[ o ] = function( f ){
		return this.on( o, f );
	};
});

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {
		// shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		return jQuery.ajax({
			type: method,
			url: url,
			data: data,
			success: callback,
			dataType: type
		});
	};
});

jQuery.extend({

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		if ( settings ) {
			// Building a settings object
			ajaxExtend( target, jQuery.ajaxSettings );
		} else {
			// Extending ajaxSettings
			settings = target;
			target = jQuery.ajaxSettings;
		}
		ajaxExtend( target, settings );
		return target;
	},

	ajaxSettings: {
		url: ajaxLocation,
		isLocal: rlocalProtocol.test( ajaxLocParts[ 1 ] ),
		global: true,
		type: "GET",
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		processData: true,
		async: true,
		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		traditional: false,
		headers: {},
		*/

		accepts: {
			xml: "application/xml, text/xml",
			html: "text/html",
			text: "text/plain",
			json: "application/json, text/javascript",
			"*": allTypes
		},

		contents: {
			xml: /xml/,
			html: /html/,
			json: /json/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText"
		},

		// List of data converters
		// 1) key format is "source_type destination_type" (a single space in-between)
		// 2) the catchall symbol "*" can be used for source_type
		converters: {

			// Convert anything to text
			"* text": window.String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": jQuery.parseJSON,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			context: true,
			url: true
		}
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var // Create the final options object
			s = jQuery.ajaxSetup( {}, options ),
			// Callbacks context
			callbackContext = s.context || s,
			// Context for global events
			// It's the callbackContext if one was provided in the options
			// and if it's a DOM node or a jQuery collection
			globalEventContext = callbackContext !== s &&
				( callbackContext.nodeType || callbackContext instanceof jQuery ) ?
						jQuery( callbackContext ) : jQuery.event,
			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),
			// Status-dependent callbacks
			statusCode = s.statusCode || {},
			// ifModified key
			ifModifiedKey,
			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},
			// Response headers
			responseHeadersString,
			responseHeaders,
			// transport
			transport,
			// timeout handle
			timeoutTimer,
			// Cross-domain detection vars
			parts,
			// The jqXHR state
			state = 0,
			// To know if global events are to be dispatched
			fireGlobals,
			// Loop variable
			i,
			// Fake xhr
			jqXHR = {

				readyState: 0,

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( !state ) {
						var lname = name.toLowerCase();
						name = requestHeadersNames[ lname ] = requestHeadersNames[ lname ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return state === 2 ? responseHeadersString : null;
				},

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( state === 2 ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[1].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match === undefined ? null : match;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( !state ) {
						s.mimeType = type;
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					statusText = statusText || "abort";
					if ( transport ) {
						transport.abort( statusText );
					}
					done( 0, statusText );
					return this;
				}
			};

		// Callback for when everything is done
		// It is defined here because jslint complains if it is declared
		// at the end of the function (which would be more logical and readable)
		function done( status, nativeStatusText, responses, headers ) {

			// Called once
			if ( state === 2 ) {
				return;
			}

			// State is "done" now
			state = 2;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			var isSuccess,
				success,
				error,
				statusText = nativeStatusText,
				response = responses ? ajaxHandleResponses( s, jqXHR, responses ) : undefined,
				lastModified,
				etag;

			// If successful, handle type chaining
			if ( status >= 200 && status < 300 || status === 304 ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {

					if ( ( lastModified = jqXHR.getResponseHeader( "Last-Modified" ) ) ) {
						jQuery.lastModified[ ifModifiedKey ] = lastModified;
					}
					if ( ( etag = jqXHR.getResponseHeader( "Etag" ) ) ) {
						jQuery.etag[ ifModifiedKey ] = etag;
					}
				}

				// If not modified
				if ( status === 304 ) {

					statusText = "notmodified";
					isSuccess = true;

				// If we have data
				} else {

					try {
						success = ajaxConvert( s, response );
						statusText = "success";
						isSuccess = true;
					} catch(e) {
						// We have a parsererror
						statusText = "parsererror";
						error = e;
					}
				}
			} else {
				// We extract error from statusText
				// then normalize statusText and status for non-aborts
				error = statusText;
				if ( !statusText || status ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = "" + ( nativeStatusText || statusText );

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajax" + ( isSuccess ? "Success" : "Error" ),
						[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );
				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		// Attach deferreds
		deferred.promise( jqXHR );
		jqXHR.success = jqXHR.done;
		jqXHR.error = jqXHR.fail;
		jqXHR.complete = completeDeferred.add;

		// Status-dependent callbacks
		jqXHR.statusCode = function( map ) {
			if ( map ) {
				var tmp;
				if ( state < 2 ) {
					for ( tmp in map ) {
						statusCode[ tmp ] = [ statusCode[tmp], map[tmp] ];
					}
				} else {
					tmp = map[ jqXHR.status ];
					jqXHR.then( tmp, tmp );
				}
			}
			return this;
		};

		// Remove hash character (#7531: and string promotion)
		// Add protocol if not provided (#5866: IE7 issue with protocol-less urls)
		// We also use the url parameter if available
		s.url = ( ( url || s.url ) + "" ).replace( rhash, "" ).replace( rprotocol, ajaxLocParts[ 1 ] + "//" );

		// Extract dataTypes list
		s.dataTypes = jQuery.trim( s.dataType || "*" ).toLowerCase().split( rspacesAjax );

		// Determine if a cross-domain request is in order
		if ( s.crossDomain == null ) {
			parts = rurl.exec( s.url.toLowerCase() );
			s.crossDomain = !!( parts &&
				( parts[ 1 ] != ajaxLocParts[ 1 ] || parts[ 2 ] != ajaxLocParts[ 2 ] ||
					( parts[ 3 ] || ( parts[ 1 ] === "http:" ? 80 : 443 ) ) !=
						( ajaxLocParts[ 3 ] || ( ajaxLocParts[ 1 ] === "http:" ? 80 : 443 ) ) )
			);
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( state === 2 ) {
			return false;
		}

		// We can fire global events as of now if asked to
		fireGlobals = s.global;

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// If data is available, append data to url
			if ( s.data ) {
				s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.data;
				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Get ifModifiedKey before adding the anti-cache parameter
			ifModifiedKey = s.url;

			// Add anti-cache in url if needed
			if ( s.cache === false ) {

				var ts = jQuery.now(),
					// try replacing _= if it is there
					ret = s.url.replace( rts, "$1_=" + ts );

				// if nothing was replaced, add timestamp to the end
				s.url = ret + ( ( ret === s.url ) ? ( rquery.test( s.url ) ? "&" : "?" ) + "_=" + ts : "" );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			ifModifiedKey = ifModifiedKey || s.url;
			if ( jQuery.lastModified[ ifModifiedKey ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ ifModifiedKey ] );
			}
			if ( jQuery.etag[ ifModifiedKey ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ ifModifiedKey ] );
			}
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[0] ] ?
				s.accepts[ s.dataTypes[0] ] + ( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend && ( s.beforeSend.call( callbackContext, jqXHR, s ) === false || state === 2 ) ) {
				// Abort if not done already
				jqXHR.abort();
				return false;

		}

		// Install callbacks on deferreds
		for ( i in { success: 1, error: 1, complete: 1 } ) {
			jqXHR[ i ]( s[ i ] );
		}

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;
			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}
			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = setTimeout( function(){
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				state = 1;
				transport.send( requestHeaders, done );
			} catch (e) {
				// Propagate exception as error if not done
				if ( state < 2 ) {
					done( -1, e );
				// Simply rethrow otherwise
				} else {
					throw e;
				}
			}
		}

		return jqXHR;
	},

	// Serialize an array of form elements or a set of
	// key/values into a query string
	param: function( a, traditional ) {
		var s = [],
			add = function( key, value ) {
				// If value is a function, invoke it and return its value
				value = jQuery.isFunction( value ) ? value() : value;
				s[ s.length ] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
			};

		// Set traditional to true for jQuery <= 1.3.2 behavior.
		if ( traditional === undefined ) {
			traditional = jQuery.ajaxSettings.traditional;
		}

		// If an array was passed in, assume that it is an array of form elements.
		if ( jQuery.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {
			// Serialize the form elements
			jQuery.each( a, function() {
				add( this.name, this.value );
			});

		} else {
			// If traditional, encode the "old" way (the way 1.3.2 or older
			// did it), otherwise encode params recursively.
			for ( var prefix in a ) {
				buildParams( prefix, a[ prefix ], traditional, add );
			}
		}

		// Return the resulting serialization
		return s.join( "&" ).replace( r20, "+" );
	}
});

function buildParams( prefix, obj, traditional, add ) {
	if ( jQuery.isArray( obj ) ) {
		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {
				// Treat each array item as a scalar.
				add( prefix, v );

			} else {
				// If array item is non-scalar (array or object), encode its
				// numeric index to resolve deserialization ambiguity issues.
				// Note that rack (as of 1.0.0) can't currently deserialize
				// nested arrays properly, and attempting to do so may cause
				// a server error. Possible fixes are to modify rack's
				// deserialization algorithm or to provide an option or flag
				// to force array serialization to be shallow.
				buildParams( prefix + "[" + ( typeof v === "object" ? i : "" ) + "]", v, traditional, add );
			}
		});

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {
		// Serialize object item.
		for ( var name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {
		// Serialize scalar item.
		add( prefix, obj );
	}
}

// This is still on the jQuery object... for now
// Want to move this to jQuery.ajax some day
jQuery.extend({

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {}

});

/* Handles responses to an ajax request:
 * - sets all responseXXX fields accordingly
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var contents = s.contents,
		dataTypes = s.dataTypes,
		responseFields = s.responseFields,
		ct,
		type,
		finalDataType,
		firstDataType;

	// Fill responseXXX fields
	for ( type in responseFields ) {
		if ( type in responses ) {
			jqXHR[ responseFields[type] ] = responses[ type ];
		}
	}

	// Remove auto dataType and get content-type in the process
	while( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "content-type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {
		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[0] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}
		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

// Chain conversions given the request and the original response
function ajaxConvert( s, response ) {

	// Apply the dataFilter if provided
	if ( s.dataFilter ) {
		response = s.dataFilter( response, s.dataType );
	}

	var dataTypes = s.dataTypes,
		converters = {},
		i,
		key,
		length = dataTypes.length,
		tmp,
		// Current and previous dataTypes
		current = dataTypes[ 0 ],
		prev,
		// Conversion expression
		conversion,
		// Conversion function
		conv,
		// Conversion functions (transitive conversion)
		conv1,
		conv2;

	// For each dataType in the chain
	for ( i = 1; i < length; i++ ) {

		// Create converters map
		// with lowercased keys
		if ( i === 1 ) {
			for ( key in s.converters ) {
				if ( typeof key === "string" ) {
					converters[ key.toLowerCase() ] = s.converters[ key ];
				}
			}
		}

		// Get the dataTypes
		prev = current;
		current = dataTypes[ i ];

		// If current is auto dataType, update it to prev
		if ( current === "*" ) {
			current = prev;
		// If no auto and dataTypes are actually different
		} else if ( prev !== "*" && prev !== current ) {

			// Get the converter
			conversion = prev + " " + current;
			conv = converters[ conversion ] || converters[ "* " + current ];

			// If there is no direct converter, search transitively
			if ( !conv ) {
				conv2 = undefined;
				for ( conv1 in converters ) {
					tmp = conv1.split( " " );
					if ( tmp[ 0 ] === prev || tmp[ 0 ] === "*" ) {
						conv2 = converters[ tmp[1] + " " + current ];
						if ( conv2 ) {
							conv1 = converters[ conv1 ];
							if ( conv1 === true ) {
								conv = conv2;
							} else if ( conv2 === true ) {
								conv = conv1;
							}
							break;
						}
					}
				}
			}
			// If we found no converter, dispatch an error
			if ( !( conv || conv2 ) ) {
				jQuery.error( "No conversion from " + conversion.replace(" "," to ") );
			}
			// If found converter is not an equivalence
			if ( conv !== true ) {
				// Convert with 1 or 2 converters accordingly
				response = conv ? conv( response ) : conv2( conv1(response) );
			}
		}
	}
	return response;
}




var jsc = jQuery.now(),
	jsre = /(\=)\?(&|$)|\?\?/i;

// Default jsonp settings
jQuery.ajaxSetup({
	jsonp: "callback",
	jsonpCallback: function() {
		return jQuery.expando + "_" + ( jsc++ );
	}
});

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var inspectData = ( typeof s.data === "string" ) && /^application\/x\-www\-form\-urlencoded/.test( s.contentType );

	if ( s.dataTypes[ 0 ] === "jsonp" ||
		s.jsonp !== false && ( jsre.test( s.url ) ||
				inspectData && jsre.test( s.data ) ) ) {

		var responseContainer,
			jsonpCallback = s.jsonpCallback =
				jQuery.isFunction( s.jsonpCallback ) ? s.jsonpCallback() : s.jsonpCallback,
			previous = window[ jsonpCallback ],
			url = s.url,
			data = s.data,
			replace = "$1" + jsonpCallback + "$2";

		if ( s.jsonp !== false ) {
			url = url.replace( jsre, replace );
			if ( s.url === url ) {
				if ( inspectData ) {
					data = data.replace( jsre, replace );
				}
				if ( s.data === data ) {
					// Add callback manually
					url += (/\?/.test( url ) ? "&" : "?") + s.jsonp + "=" + jsonpCallback;
				}
			}
		}

		s.url = url;
		s.data = data;

		// Install callback
		window[ jsonpCallback ] = function( response ) {
			responseContainer = [ response ];
		};

		// Clean-up function
		jqXHR.always(function() {
			// Set callback back to previous value
			window[ jsonpCallback ] = previous;
			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( previous ) ) {
				window[ jsonpCallback ]( responseContainer[ 0 ] );
			}
		});

		// Use data converter to retrieve json after script execution
		s.converters["script json"] = function() {
			if ( !responseContainer ) {
				jQuery.error( jsonpCallback + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// force json dataType
		s.dataTypes[ 0 ] = "json";

		// Delegate to script
		return "script";
	}
});




// Install script dataType
jQuery.ajaxSetup({
	accepts: {
		script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /javascript|ecmascript/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
});

// Handle cache's special case and global
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
		s.global = false;
	}
});

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function(s) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {

		var script,
			head = document.head || document.getElementsByTagName( "head" )[0] || document.documentElement;

		return {

			send: function( _, callback ) {

				script = document.createElement( "script" );

				script.async = "async";

				if ( s.scriptCharset ) {
					script.charset = s.scriptCharset;
				}

				script.src = s.url;

				// Attach handlers for all browsers
				script.onload = script.onreadystatechange = function( _, isAbort ) {

					if ( isAbort || !script.readyState || /loaded|complete/.test( script.readyState ) ) {

						// Handle memory leak in IE
						script.onload = script.onreadystatechange = null;

						// Remove the script
						if ( head && script.parentNode ) {
							head.removeChild( script );
						}

						// Dereference the script
						script = undefined;

						// Callback if not abort
						if ( !isAbort ) {
							callback( 200, "success" );
						}
					}
				};
				// Use insertBefore instead of appendChild  to circumvent an IE6 bug.
				// This arises when a base node is used (#2709 and #4378).
				head.insertBefore( script, head.firstChild );
			},

			abort: function() {
				if ( script ) {
					script.onload( 0, 1 );
				}
			}
		};
	}
});




var // #5280: Internet Explorer will keep connections alive if we don't abort on unload
	xhrOnUnloadAbort = window.ActiveXObject ? function() {
		// Abort all pending requests
		for ( var key in xhrCallbacks ) {
			xhrCallbacks[ key ]( 0, 1 );
		}
	} : false,
	xhrId = 0,
	xhrCallbacks;

// Functions to create xhrs
function createStandardXHR() {
	try {
		return new window.XMLHttpRequest();
	} catch( e ) {}
}

function createActiveXHR() {
	try {
		return new window.ActiveXObject( "Microsoft.XMLHTTP" );
	} catch( e ) {}
}

// Create the request object
// (This is still attached to ajaxSettings for backward compatibility)
jQuery.ajaxSettings.xhr = window.ActiveXObject ?
	/* Microsoft failed to properly
	 * implement the XMLHttpRequest in IE7 (can't request local files),
	 * so we use the ActiveXObject when it is available
	 * Additionally XMLHttpRequest can be disabled in IE7/IE8 so
	 * we need a fallback.
	 */
	function() {
		return !this.isLocal && createStandardXHR() || createActiveXHR();
	} :
	// For all other browsers, use the standard XMLHttpRequest object
	createStandardXHR;

// Determine support properties
(function( xhr ) {
	jQuery.extend( jQuery.support, {
		ajax: !!xhr,
		cors: !!xhr && ( "withCredentials" in xhr )
	});
})( jQuery.ajaxSettings.xhr() );

// Create transport if the browser can provide an xhr
if ( jQuery.support.ajax ) {

	jQuery.ajaxTransport(function( s ) {
		// Cross domain only allowed if supported through XMLHttpRequest
		if ( !s.crossDomain || jQuery.support.cors ) {

			var callback;

			return {
				send: function( headers, complete ) {

					// Get a new xhr
					var xhr = s.xhr(),
						handle,
						i;

					// Open the socket
					// Passing null username, generates a login popup on Opera (#2865)
					if ( s.username ) {
						xhr.open( s.type, s.url, s.async, s.username, s.password );
					} else {
						xhr.open( s.type, s.url, s.async );
					}

					// Apply custom fields if provided
					if ( s.xhrFields ) {
						for ( i in s.xhrFields ) {
							xhr[ i ] = s.xhrFields[ i ];
						}
					}

					// Override mime type if needed
					if ( s.mimeType && xhr.overrideMimeType ) {
						xhr.overrideMimeType( s.mimeType );
					}

					// X-Requested-With header
					// For cross-domain requests, seeing as conditions for a preflight are
					// akin to a jigsaw puzzle, we simply never set it to be sure.
					// (it can always be set on a per-request basis or even using ajaxSetup)
					// For same-domain requests, won't change header if already provided.
					if ( !s.crossDomain && !headers["X-Requested-With"] ) {
						headers[ "X-Requested-With" ] = "XMLHttpRequest";
					}

					// Need an extra try/catch for cross domain requests in Firefox 3
					try {
						for ( i in headers ) {
							xhr.setRequestHeader( i, headers[ i ] );
						}
					} catch( _ ) {}

					// Do send the request
					// This may raise an exception which is actually
					// handled in jQuery.ajax (so no try/catch here)
					xhr.send( ( s.hasContent && s.data ) || null );

					// Listener
					callback = function( _, isAbort ) {

						var status,
							statusText,
							responseHeaders,
							responses,
							xml;

						// Firefox throws exceptions when accessing properties
						// of an xhr when a network error occured
						// http://helpful.knobs-dials.com/index.php/Component_returned_failure_code:_0x80040111_(NS_ERROR_NOT_AVAILABLE)
						try {

							// Was never called and is aborted or complete
							if ( callback && ( isAbort || xhr.readyState === 4 ) ) {

								// Only called once
								callback = undefined;

								// Do not keep as active anymore
								if ( handle ) {
									xhr.onreadystatechange = jQuery.noop;
									if ( xhrOnUnloadAbort ) {
										delete xhrCallbacks[ handle ];
									}
								}

								// If it's an abort
								if ( isAbort ) {
									// Abort it manually if needed
									if ( xhr.readyState !== 4 ) {
										xhr.abort();
									}
								} else {
									status = xhr.status;
									responseHeaders = xhr.getAllResponseHeaders();
									responses = {};
									xml = xhr.responseXML;

									// Construct response list
									if ( xml && xml.documentElement /* #4958 */ ) {
										responses.xml = xml;
									}

									// When requesting binary data, IE6-9 will throw an exception
									// on any attempt to access responseText (#11426)
									try {
										responses.text = xhr.responseText;
									} catch( _ ) {
									}

									// Firefox throws an exception when accessing
									// statusText for faulty cross-domain requests
									try {
										statusText = xhr.statusText;
									} catch( e ) {
										// We normalize with Webkit giving an empty statusText
										statusText = "";
									}

									// Filter status for non standard behaviors

									// If the request is local and we have data: assume a success
									// (success with no data won't get notified, that's the best we
									// can do given current implementations)
									if ( !status && s.isLocal && !s.crossDomain ) {
										status = responses.text ? 200 : 404;
									// IE - #1450: sometimes returns 1223 when it should be 204
									} else if ( status === 1223 ) {
										status = 204;
									}
								}
							}
						} catch( firefoxAccessException ) {
							if ( !isAbort ) {
								complete( -1, firefoxAccessException );
							}
						}

						// Call complete if needed
						if ( responses ) {
							complete( status, statusText, responses, responseHeaders );
						}
					};

					// if we're in sync mode or it's in cache
					// and has been retrieved directly (IE6 & IE7)
					// we need to manually fire the callback
					if ( !s.async || xhr.readyState === 4 ) {
						callback();
					} else {
						handle = ++xhrId;
						if ( xhrOnUnloadAbort ) {
							// Create the active xhrs callbacks list if needed
							// and attach the unload handler
							if ( !xhrCallbacks ) {
								xhrCallbacks = {};
								jQuery( window ).unload( xhrOnUnloadAbort );
							}
							// Add to list of active xhrs callbacks
							xhrCallbacks[ handle ] = callback;
						}
						xhr.onreadystatechange = callback;
					}
				},

				abort: function() {
					if ( callback ) {
						callback(0,1);
					}
				}
			};
		}
	});
}




var elemdisplay = {},
	iframe, iframeDoc,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rfxnum = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,
	timerId,
	fxAttrs = [
		// height animations
		[ "height", "marginTop", "marginBottom", "paddingTop", "paddingBottom" ],
		// width animations
		[ "width", "marginLeft", "marginRight", "paddingLeft", "paddingRight" ],
		// opacity animations
		[ "opacity" ]
	],
	fxNow;

jQuery.fn.extend({
	show: function( speed, easing, callback ) {
		var elem, display;

		if ( speed || speed === 0 ) {
			return this.animate( genFx("show", 3), speed, easing, callback );

		} else {
			for ( var i = 0, j = this.length; i < j; i++ ) {
				elem = this[ i ];

				if ( elem.style ) {
					display = elem.style.display;

					// Reset the inline display of this element to learn if it is
					// being hidden by cascaded rules or not
					if ( !jQuery._data(elem, "olddisplay") && display === "none" ) {
						display = elem.style.display = "";
					}

					// Set elements which have been overridden with display: none
					// in a stylesheet to whatever the default browser style is
					// for such an element
					if ( (display === "" && jQuery.css(elem, "display") === "none") ||
						!jQuery.contains( elem.ownerDocument.documentElement, elem ) ) {
						jQuery._data( elem, "olddisplay", defaultDisplay(elem.nodeName) );
					}
				}
			}

			// Set the display of most of the elements in a second loop
			// to avoid the constant reflow
			for ( i = 0; i < j; i++ ) {
				elem = this[ i ];

				if ( elem.style ) {
					display = elem.style.display;

					if ( display === "" || display === "none" ) {
						elem.style.display = jQuery._data( elem, "olddisplay" ) || "";
					}
				}
			}

			return this;
		}
	},

	hide: function( speed, easing, callback ) {
		if ( speed || speed === 0 ) {
			return this.animate( genFx("hide", 3), speed, easing, callback);

		} else {
			var elem, display,
				i = 0,
				j = this.length;

			for ( ; i < j; i++ ) {
				elem = this[i];
				if ( elem.style ) {
					display = jQuery.css( elem, "display" );

					if ( display !== "none" && !jQuery._data( elem, "olddisplay" ) ) {
						jQuery._data( elem, "olddisplay", display );
					}
				}
			}

			// Set the display of the elements in a second loop
			// to avoid the constant reflow
			for ( i = 0; i < j; i++ ) {
				if ( this[i].style ) {
					this[i].style.display = "none";
				}
			}

			return this;
		}
	},

	// Save the old toggle function
	_toggle: jQuery.fn.toggle,

	toggle: function( fn, fn2, callback ) {
		var bool = typeof fn === "boolean";

		if ( jQuery.isFunction(fn) && jQuery.isFunction(fn2) ) {
			this._toggle.apply( this, arguments );

		} else if ( fn == null || bool ) {
			this.each(function() {
				var state = bool ? fn : jQuery(this).is(":hidden");
				jQuery(this)[ state ? "show" : "hide" ]();
			});

		} else {
			this.animate(genFx("toggle", 3), fn, fn2, callback);
		}

		return this;
	},

	fadeTo: function( speed, to, easing, callback ) {
		return this.filter(":hidden").css("opacity", 0).show().end()
					.animate({opacity: to}, speed, easing, callback);
	},

	animate: function( prop, speed, easing, callback ) {
		var optall = jQuery.speed( speed, easing, callback );

		if ( jQuery.isEmptyObject( prop ) ) {
			return this.each( optall.complete, [ false ] );
		}

		// Do not change referenced properties as per-property easing will be lost
		prop = jQuery.extend( {}, prop );

		function doAnimation() {
			// XXX 'this' does not always have a nodeName when running the
			// test suite

			if ( optall.queue === false ) {
				jQuery._mark( this );
			}

			var opt = jQuery.extend( {}, optall ),
				isElement = this.nodeType === 1,
				hidden = isElement && jQuery(this).is(":hidden"),
				name, val, p, e, hooks, replace,
				parts, start, end, unit,
				method;

			// will store per property easing and be used to determine when an animation is complete
			opt.animatedProperties = {};

			// first pass over propertys to expand / normalize
			for ( p in prop ) {
				name = jQuery.camelCase( p );
				if ( p !== name ) {
					prop[ name ] = prop[ p ];
					delete prop[ p ];
				}

				if ( ( hooks = jQuery.cssHooks[ name ] ) && "expand" in hooks ) {
					replace = hooks.expand( prop[ name ] );
					delete prop[ name ];

					// not quite $.extend, this wont overwrite keys already present.
					// also - reusing 'p' from above because we have the correct "name"
					for ( p in replace ) {
						if ( ! ( p in prop ) ) {
							prop[ p ] = replace[ p ];
						}
					}
				}
			}

			for ( name in prop ) {
				val = prop[ name ];
				// easing resolution: per property > opt.specialEasing > opt.easing > 'swing' (default)
				if ( jQuery.isArray( val ) ) {
					opt.animatedProperties[ name ] = val[ 1 ];
					val = prop[ name ] = val[ 0 ];
				} else {
					opt.animatedProperties[ name ] = opt.specialEasing && opt.specialEasing[ name ] || opt.easing || 'swing';
				}

				if ( val === "hide" && hidden || val === "show" && !hidden ) {
					return opt.complete.call( this );
				}

				if ( isElement && ( name === "height" || name === "width" ) ) {
					// Make sure that nothing sneaks out
					// Record all 3 overflow attributes because IE does not
					// change the overflow attribute when overflowX and
					// overflowY are set to the same value
					opt.overflow = [ this.style.overflow, this.style.overflowX, this.style.overflowY ];

					// Set display property to inline-block for height/width
					// animations on inline elements that are having width/height animated
					if ( jQuery.css( this, "display" ) === "inline" &&
							jQuery.css( this, "float" ) === "none" ) {

						// inline-level elements accept inline-block;
						// block-level elements need to be inline with layout
						if ( !jQuery.support.inlineBlockNeedsLayout || defaultDisplay( this.nodeName ) === "inline" ) {
							this.style.display = "inline-block";

						} else {
							this.style.zoom = 1;
						}
					}
				}
			}

			if ( opt.overflow != null ) {
				this.style.overflow = "hidden";
			}

			for ( p in prop ) {
				e = new jQuery.fx( this, opt, p );
				val = prop[ p ];

				if ( rfxtypes.test( val ) ) {

					// Tracks whether to show or hide based on private
					// data attached to the element
					method = jQuery._data( this, "toggle" + p ) || ( val === "toggle" ? hidden ? "show" : "hide" : 0 );
					if ( method ) {
						jQuery._data( this, "toggle" + p, method === "show" ? "hide" : "show" );
						e[ method ]();
					} else {
						e[ val ]();
					}

				} else {
					parts = rfxnum.exec( val );
					start = e.cur();

					if ( parts ) {
						end = parseFloat( parts[2] );
						unit = parts[3] || ( jQuery.cssNumber[ p ] ? "" : "px" );

						// We need to compute starting value
						if ( unit !== "px" ) {
							jQuery.style( this, p, (end || 1) + unit);
							start = ( (end || 1) / e.cur() ) * start;
							jQuery.style( this, p, start + unit);
						}

						// If a +=/-= token was provided, we're doing a relative animation
						if ( parts[1] ) {
							end = ( (parts[ 1 ] === "-=" ? -1 : 1) * end ) + start;
						}

						e.custom( start, end, unit );

					} else {
						e.custom( start, val, "" );
					}
				}
			}

			// For JS strict compliance
			return true;
		}

		return optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},

	stop: function( type, clearQueue, gotoEnd ) {
		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each(function() {
			var index,
				hadTimers = false,
				timers = jQuery.timers,
				data = jQuery._data( this );

			// clear marker counters if we know they won't be
			if ( !gotoEnd ) {
				jQuery._unmark( true, this );
			}

			function stopQueue( elem, data, index ) {
				var hooks = data[ index ];
				jQuery.removeData( elem, index, true );
				hooks.stop( gotoEnd );
			}

			if ( type == null ) {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && index.indexOf(".run") === index.length - 4 ) {
						stopQueue( this, data, index );
					}
				}
			} else if ( data[ index = type + ".run" ] && data[ index ].stop ){
				stopQueue( this, data, index );
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && (type == null || timers[ index ].queue === type) ) {
					if ( gotoEnd ) {

						// force the next step to be the last
						timers[ index ]( true );
					} else {
						timers[ index ].saveState();
					}
					hadTimers = true;
					timers.splice( index, 1 );
				}
			}

			// start the next in the queue if the last step wasn't forced
			// timers currently will call their complete callbacks, which will dequeue
			// but only if they were gotoEnd
			if ( !( gotoEnd && hadTimers ) ) {
				jQuery.dequeue( this, type );
			}
		});
	}

});

// Animations created synchronously will run synchronously
function createFxNow() {
	setTimeout( clearFxNow, 0 );
	return ( fxNow = jQuery.now() );
}

function clearFxNow() {
	fxNow = undefined;
}

// Generate parameters to create a standard animation
function genFx( type, num ) {
	var obj = {};

	jQuery.each( fxAttrs.concat.apply([], fxAttrs.slice( 0, num )), function() {
		obj[ this ] = type;
	});

	return obj;
}

// Generate shortcuts for custom animations
jQuery.each({
	slideDown: genFx( "show", 1 ),
	slideUp: genFx( "hide", 1 ),
	slideToggle: genFx( "toggle", 1 ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
});

jQuery.extend({
	speed: function( speed, easing, fn ) {
		var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
			complete: fn || !fn && easing ||
				jQuery.isFunction( speed ) && speed,
			duration: speed,
			easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
		};

		opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration :
			opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[ opt.duration ] : jQuery.fx.speeds._default;

		// normalize opt.queue - true/undefined/null -> "fx"
		if ( opt.queue == null || opt.queue === true ) {
			opt.queue = "fx";
		}

		// Queueing
		opt.old = opt.complete;

		opt.complete = function( noUnmark ) {
			if ( jQuery.isFunction( opt.old ) ) {
				opt.old.call( this );
			}

			if ( opt.queue ) {
				jQuery.dequeue( this, opt.queue );
			} else if ( noUnmark !== false ) {
				jQuery._unmark( this );
			}
		};

		return opt;
	},

	easing: {
		linear: function( p ) {
			return p;
		},
		swing: function( p ) {
			return ( -Math.cos( p*Math.PI ) / 2 ) + 0.5;
		}
	},

	timers: [],

	fx: function( elem, options, prop ) {
		this.options = options;
		this.elem = elem;
		this.prop = prop;

		options.orig = options.orig || {};
	}

});

jQuery.fx.prototype = {
	// Simple function for setting a style value
	update: function() {
		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		( jQuery.fx.step[ this.prop ] || jQuery.fx.step._default )( this );
	},

	// Get the current size
	cur: function() {
		if ( this.elem[ this.prop ] != null && (!this.elem.style || this.elem.style[ this.prop ] == null) ) {
			return this.elem[ this.prop ];
		}

		var parsed,
			r = jQuery.css( this.elem, this.prop );
		// Empty strings, null, undefined and "auto" are converted to 0,
		// complex values such as "rotate(1rad)" are returned as is,
		// simple values such as "10px" are parsed to Float.
		return isNaN( parsed = parseFloat( r ) ) ? !r || r === "auto" ? 0 : r : parsed;
	},

	// Start an animation from one number to another
	custom: function( from, to, unit ) {
		var self = this,
			fx = jQuery.fx;

		this.startTime = fxNow || createFxNow();
		this.end = to;
		this.now = this.start = from;
		this.pos = this.state = 0;
		this.unit = unit || this.unit || ( jQuery.cssNumber[ this.prop ] ? "" : "px" );

		function t( gotoEnd ) {
			return self.step( gotoEnd );
		}

		t.queue = this.options.queue;
		t.elem = this.elem;
		t.saveState = function() {
			if ( jQuery._data( self.elem, "fxshow" + self.prop ) === undefined ) {
				if ( self.options.hide ) {
					jQuery._data( self.elem, "fxshow" + self.prop, self.start );
				} else if ( self.options.show ) {
					jQuery._data( self.elem, "fxshow" + self.prop, self.end );
				}
			}
		};

		if ( t() && jQuery.timers.push(t) && !timerId ) {
			timerId = setInterval( fx.tick, fx.interval );
		}
	},

	// Simple 'show' function
	show: function() {
		var dataShow = jQuery._data( this.elem, "fxshow" + this.prop );

		// Remember where we started, so that we can go back to it later
		this.options.orig[ this.prop ] = dataShow || jQuery.style( this.elem, this.prop );
		this.options.show = true;

		// Begin the animation
		// Make sure that we start at a small width/height to avoid any flash of content
		if ( dataShow !== undefined ) {
			// This show is picking up where a previous hide or show left off
			this.custom( this.cur(), dataShow );
		} else {
			this.custom( this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur() );
		}

		// Start by showing the element
		jQuery( this.elem ).show();
	},

	// Simple 'hide' function
	hide: function() {
		// Remember where we started, so that we can go back to it later
		this.options.orig[ this.prop ] = jQuery._data( this.elem, "fxshow" + this.prop ) || jQuery.style( this.elem, this.prop );
		this.options.hide = true;

		// Begin the animation
		this.custom( this.cur(), 0 );
	},

	// Each step of an animation
	step: function( gotoEnd ) {
		var p, n, complete,
			t = fxNow || createFxNow(),
			done = true,
			elem = this.elem,
			options = this.options;

		if ( gotoEnd || t >= options.duration + this.startTime ) {
			this.now = this.end;
			this.pos = this.state = 1;
			this.update();

			options.animatedProperties[ this.prop ] = true;

			for ( p in options.animatedProperties ) {
				if ( options.animatedProperties[ p ] !== true ) {
					done = false;
				}
			}

			if ( done ) {
				// Reset the overflow
				if ( options.overflow != null && !jQuery.support.shrinkWrapBlocks ) {

					jQuery.each( [ "", "X", "Y" ], function( index, value ) {
						elem.style[ "overflow" + value ] = options.overflow[ index ];
					});
				}

				// Hide the element if the "hide" operation was done
				if ( options.hide ) {
					jQuery( elem ).hide();
				}

				// Reset the properties, if the item has been hidden or shown
				if ( options.hide || options.show ) {
					for ( p in options.animatedProperties ) {
						jQuery.style( elem, p, options.orig[ p ] );
						jQuery.removeData( elem, "fxshow" + p, true );
						// Toggle data is no longer needed
						jQuery.removeData( elem, "toggle" + p, true );
					}
				}

				// Execute the complete function
				// in the event that the complete function throws an exception
				// we must ensure it won't be called twice. #5684

				complete = options.complete;
				if ( complete ) {

					options.complete = false;
					complete.call( elem );
				}
			}

			return false;

		} else {
			// classical easing cannot be used with an Infinity duration
			if ( options.duration == Infinity ) {
				this.now = t;
			} else {
				n = t - this.startTime;
				this.state = n / options.duration;

				// Perform the easing function, defaults to swing
				this.pos = jQuery.easing[ options.animatedProperties[this.prop] ]( this.state, n, 0, 1, options.duration );
				this.now = this.start + ( (this.end - this.start) * this.pos );
			}
			// Perform the next step of the animation
			this.update();
		}

		return true;
	}
};

jQuery.extend( jQuery.fx, {
	tick: function() {
		var timer,
			timers = jQuery.timers,
			i = 0;

		for ( ; i < timers.length; i++ ) {
			timer = timers[ i ];
			// Checks the timer has not already been removed
			if ( !timer() && timers[ i ] === timer ) {
				timers.splice( i--, 1 );
			}
		}

		if ( !timers.length ) {
			jQuery.fx.stop();
		}
	},

	interval: 13,

	stop: function() {
		clearInterval( timerId );
		timerId = null;
	},

	speeds: {
		slow: 600,
		fast: 200,
		// Default speed
		_default: 400
	},

	step: {
		opacity: function( fx ) {
			jQuery.style( fx.elem, "opacity", fx.now );
		},

		_default: function( fx ) {
			if ( fx.elem.style && fx.elem.style[ fx.prop ] != null ) {
				fx.elem.style[ fx.prop ] = fx.now + fx.unit;
			} else {
				fx.elem[ fx.prop ] = fx.now;
			}
		}
	}
});

// Ensure props that can't be negative don't go there on undershoot easing
jQuery.each( fxAttrs.concat.apply( [], fxAttrs ), function( i, prop ) {
	// exclude marginTop, marginLeft, marginBottom and marginRight from this list
	if ( prop.indexOf( "margin" ) ) {
		jQuery.fx.step[ prop ] = function( fx ) {
			jQuery.style( fx.elem, prop, Math.max(0, fx.now) + fx.unit );
		};
	}
});

if ( jQuery.expr && jQuery.expr.filters ) {
	jQuery.expr.filters.animated = function( elem ) {
		return jQuery.grep(jQuery.timers, function( fn ) {
			return elem === fn.elem;
		}).length;
	};
}

// Try to restore the default display value of an element
function defaultDisplay( nodeName ) {

	if ( !elemdisplay[ nodeName ] ) {

		var body = document.body,
			elem = jQuery( "<" + nodeName + ">" ).appendTo( body ),
			display = elem.css( "display" );
		elem.remove();

		// If the simple way fails,
		// get element's real default display by attaching it to a temp iframe
		if ( display === "none" || display === "" ) {
			// No iframe to use yet, so create it
			if ( !iframe ) {
				iframe = document.createElement( "iframe" );
				iframe.frameBorder = iframe.width = iframe.height = 0;
			}

			body.appendChild( iframe );

			// Create a cacheable copy of the iframe document on first call.
			// IE and Opera will allow us to reuse the iframeDoc without re-writing the fake HTML
			// document to it; WebKit & Firefox won't allow reusing the iframe document.
			if ( !iframeDoc || !iframe.createElement ) {
				iframeDoc = ( iframe.contentWindow || iframe.contentDocument ).document;
				iframeDoc.write( ( jQuery.support.boxModel ? "<!doctype html>" : "" ) + "<html><body>" );
				iframeDoc.close();
			}

			elem = iframeDoc.createElement( nodeName );

			iframeDoc.body.appendChild( elem );

			display = jQuery.css( elem, "display" );
			body.removeChild( iframe );
		}

		// Store the correct default display
		elemdisplay[ nodeName ] = display;
	}

	return elemdisplay[ nodeName ];
}




var getOffset,
	rtable = /^t(?:able|d|h)$/i,
	rroot = /^(?:body|html)$/i;

if ( "getBoundingClientRect" in document.documentElement ) {
	getOffset = function( elem, doc, docElem, box ) {
		try {
			box = elem.getBoundingClientRect();
		} catch(e) {}

		// Make sure we're not dealing with a disconnected DOM node
		if ( !box || !jQuery.contains( docElem, elem ) ) {
			return box ? { top: box.top, left: box.left } : { top: 0, left: 0 };
		}

		var body = doc.body,
			win = getWindow( doc ),
			clientTop  = docElem.clientTop  || body.clientTop  || 0,
			clientLeft = docElem.clientLeft || body.clientLeft || 0,
			scrollTop  = win.pageYOffset || jQuery.support.boxModel && docElem.scrollTop  || body.scrollTop,
			scrollLeft = win.pageXOffset || jQuery.support.boxModel && docElem.scrollLeft || body.scrollLeft,
			top  = box.top  + scrollTop  - clientTop,
			left = box.left + scrollLeft - clientLeft;

		return { top: top, left: left };
	};

} else {
	getOffset = function( elem, doc, docElem ) {
		var computedStyle,
			offsetParent = elem.offsetParent,
			prevOffsetParent = elem,
			body = doc.body,
			defaultView = doc.defaultView,
			prevComputedStyle = defaultView ? defaultView.getComputedStyle( elem, null ) : elem.currentStyle,
			top = elem.offsetTop,
			left = elem.offsetLeft;

		while ( (elem = elem.parentNode) && elem !== body && elem !== docElem ) {
			if ( jQuery.support.fixedPosition && prevComputedStyle.position === "fixed" ) {
				break;
			}

			computedStyle = defaultView ? defaultView.getComputedStyle(elem, null) : elem.currentStyle;
			top  -= elem.scrollTop;
			left -= elem.scrollLeft;

			if ( elem === offsetParent ) {
				top  += elem.offsetTop;
				left += elem.offsetLeft;

				if ( jQuery.support.doesNotAddBorder && !(jQuery.support.doesAddBorderForTableAndCells && rtable.test(elem.nodeName)) ) {
					top  += parseFloat( computedStyle.borderTopWidth  ) || 0;
					left += parseFloat( computedStyle.borderLeftWidth ) || 0;
				}

				prevOffsetParent = offsetParent;
				offsetParent = elem.offsetParent;
			}

			if ( jQuery.support.subtractsBorderForOverflowNotVisible && computedStyle.overflow !== "visible" ) {
				top  += parseFloat( computedStyle.borderTopWidth  ) || 0;
				left += parseFloat( computedStyle.borderLeftWidth ) || 0;
			}

			prevComputedStyle = computedStyle;
		}

		if ( prevComputedStyle.position === "relative" || prevComputedStyle.position === "static" ) {
			top  += body.offsetTop;
			left += body.offsetLeft;
		}

		if ( jQuery.support.fixedPosition && prevComputedStyle.position === "fixed" ) {
			top  += Math.max( docElem.scrollTop, body.scrollTop );
			left += Math.max( docElem.scrollLeft, body.scrollLeft );
		}

		return { top: top, left: left };
	};
}

jQuery.fn.offset = function( options ) {
	if ( arguments.length ) {
		return options === undefined ?
			this :
			this.each(function( i ) {
				jQuery.offset.setOffset( this, options, i );
			});
	}

	var elem = this[0],
		doc = elem && elem.ownerDocument;

	if ( !doc ) {
		return null;
	}

	if ( elem === doc.body ) {
		return jQuery.offset.bodyOffset( elem );
	}

	return getOffset( elem, doc, doc.documentElement );
};

jQuery.offset = {

	bodyOffset: function( body ) {
		var top = body.offsetTop,
			left = body.offsetLeft;

		if ( jQuery.support.doesNotIncludeMarginInBodyOffset ) {
			top  += parseFloat( jQuery.css(body, "marginTop") ) || 0;
			left += parseFloat( jQuery.css(body, "marginLeft") ) || 0;
		}

		return { top: top, left: left };
	},

	setOffset: function( elem, options, i ) {
		var position = jQuery.css( elem, "position" );

		// set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		var curElem = jQuery( elem ),
			curOffset = curElem.offset(),
			curCSSTop = jQuery.css( elem, "top" ),
			curCSSLeft = jQuery.css( elem, "left" ),
			calculatePosition = ( position === "absolute" || position === "fixed" ) && jQuery.inArray("auto", [curCSSTop, curCSSLeft]) > -1,
			props = {}, curPosition = {}, curTop, curLeft;

		// need to be able to calculate position if either top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;
		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {
			options = options.call( elem, i, curOffset );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );
		} else {
			curElem.css( props );
		}
	}
};


jQuery.fn.extend({

	position: function() {
		if ( !this[0] ) {
			return null;
		}

		var elem = this[0],

		// Get *real* offsetParent
		offsetParent = this.offsetParent(),

		// Get correct offsets
		offset       = this.offset(),
		parentOffset = rroot.test(offsetParent[0].nodeName) ? { top: 0, left: 0 } : offsetParent.offset();

		// Subtract element margins
		// note: when an element has margin: auto the offsetLeft and marginLeft
		// are the same in Safari causing offset.left to incorrectly be 0
		offset.top  -= parseFloat( jQuery.css(elem, "marginTop") ) || 0;
		offset.left -= parseFloat( jQuery.css(elem, "marginLeft") ) || 0;

		// Add offsetParent borders
		parentOffset.top  += parseFloat( jQuery.css(offsetParent[0], "borderTopWidth") ) || 0;
		parentOffset.left += parseFloat( jQuery.css(offsetParent[0], "borderLeftWidth") ) || 0;

		// Subtract the two offsets
		return {
			top:  offset.top  - parentOffset.top,
			left: offset.left - parentOffset.left
		};
	},

	offsetParent: function() {
		return this.map(function() {
			var offsetParent = this.offsetParent || document.body;
			while ( offsetParent && (!rroot.test(offsetParent.nodeName) && jQuery.css(offsetParent, "position") === "static") ) {
				offsetParent = offsetParent.offsetParent;
			}
			return offsetParent;
		});
	}
});


// Create scrollLeft and scrollTop methods
jQuery.each( {scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function( method, prop ) {
	var top = /Y/.test( prop );

	jQuery.fn[ method ] = function( val ) {
		return jQuery.access( this, function( elem, method, val ) {
			var win = getWindow( elem );

			if ( val === undefined ) {
				return win ? (prop in win) ? win[ prop ] :
					jQuery.support.boxModel && win.document.documentElement[ method ] ||
						win.document.body[ method ] :
					elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : jQuery( win ).scrollLeft(),
					 top ? val : jQuery( win ).scrollTop()
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length, null );
	};
});

function getWindow( elem ) {
	return jQuery.isWindow( elem ) ?
		elem :
		elem.nodeType === 9 ?
			elem.defaultView || elem.parentWindow :
			false;
}




// Create width, height, innerHeight, innerWidth, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	var clientProp = "client" + name,
		scrollProp = "scroll" + name,
		offsetProp = "offset" + name;

	// innerHeight and innerWidth
	jQuery.fn[ "inner" + name ] = function() {
		var elem = this[0];
		return elem ?
			elem.style ?
			parseFloat( jQuery.css( elem, type, "padding" ) ) :
			this[ type ]() :
			null;
	};

	// outerHeight and outerWidth
	jQuery.fn[ "outer" + name ] = function( margin ) {
		var elem = this[0];
		return elem ?
			elem.style ?
			parseFloat( jQuery.css( elem, type, margin ? "margin" : "border" ) ) :
			this[ type ]() :
			null;
	};

	jQuery.fn[ type ] = function( value ) {
		return jQuery.access( this, function( elem, type, value ) {
			var doc, docElemProp, orig, ret;

			if ( jQuery.isWindow( elem ) ) {
				// 3rd condition allows Nokia support, as it supports the docElem prop but not CSS1Compat
				doc = elem.document;
				docElemProp = doc.documentElement[ clientProp ];
				return jQuery.support.boxModel && docElemProp ||
					doc.body && doc.body[ clientProp ] || docElemProp;
			}

			// Get document width or height
			if ( elem.nodeType === 9 ) {
				// Either scroll[Width/Height] or offset[Width/Height], whichever is greater
				doc = elem.documentElement;

				// when a window > document, IE6 reports a offset[Width/Height] > client[Width/Height]
				// so we can't use max, as it'll choose the incorrect offset[Width/Height]
				// instead we use the correct client[Width/Height]
				// support:IE6
				if ( doc[ clientProp ] >= doc[ scrollProp ] ) {
					return doc[ clientProp ];
				}

				return Math.max(
					elem.body[ scrollProp ], doc[ scrollProp ],
					elem.body[ offsetProp ], doc[ offsetProp ]
				);
			}

			// Get width or height on the element
			if ( value === undefined ) {
				orig = jQuery.css( elem, type );
				ret = parseFloat( orig );
				return jQuery.isNumeric( ret ) ? ret : orig;
			}

			// Set the width or height on the element
			jQuery( elem ).css( type, value );
		}, type, value, arguments.length, null );
	};
});




// Expose jQuery to the global object
window.jQuery = window.$ = jQuery;

// Expose jQuery as an AMD module, but only for AMD loaders that
// understand the issues with loading multiple versions of jQuery
// in a page that all might call define(). The loader will indicate
// they have special allowances for multiple jQuery versions by
// specifying define.amd.jQuery = true. Register as a named module,
// since jQuery can be concatenated with other files that may use define,
// but not use a proper concatenation script that understands anonymous
// AMD modules. A named AMD is safest and most robust way to register.
// Lowercase jquery is used because AMD module names are derived from
// file names, and jQuery is normally delivered in a lowercase file name.
// Do this after creating the global so that if an AMD module wants to call
// noConflict to hide this version of jQuery, it will work.
if ( typeof define === "function" && define.amd && define.amd.jQuery ) {
	define( "jquery", [], function () { return jQuery; } );
}



})( window );
(function($, undefined) {

/**
 * Unobtrusive scripting adapter for jQuery
 *
 * Requires jQuery 1.6.0 or later.
 * https://github.com/rails/jquery-ujs

 * Uploading file using rails.js
 * =============================
 *
 * By default, browsers do not allow files to be uploaded via AJAX. As a result, if there are any non-blank file fields
 * in the remote form, this adapter aborts the AJAX submission and allows the form to submit through standard means.
 *
 * The `ajax:aborted:file` event allows you to bind your own handler to process the form submission however you wish.
 *
 * Ex:
 *     $('form').live('ajax:aborted:file', function(event, elements){
 *       // Implement own remote file-transfer handler here for non-blank file inputs passed in `elements`.
 *       // Returning false in this handler tells rails.js to disallow standard form submission
 *       return false;
 *     });
 *
 * The `ajax:aborted:file` event is fired when a file-type input is detected with a non-blank value.
 *
 * Third-party tools can use this hook to detect when an AJAX file upload is attempted, and then use
 * techniques like the iframe method to upload the file instead.
 *
 * Required fields in rails.js
 * ===========================
 *
 * If any blank required inputs (required="required") are detected in the remote form, the whole form submission
 * is canceled. Note that this is unlike file inputs, which still allow standard (non-AJAX) form submission.
 *
 * The `ajax:aborted:required` event allows you to bind your own handler to inform the user of blank required inputs.
 *
 * !! Note that Opera does not fire the form's submit event if there are blank required inputs, so this event may never
 *    get fired in Opera. This event is what causes other browsers to exhibit the same submit-aborting behavior.
 *
 * Ex:
 *     $('form').live('ajax:aborted:required', function(event, elements){
 *       // Returning false in this handler tells rails.js to submit the form anyway.
 *       // The blank required inputs are passed to this function in `elements`.
 *       return ! confirm("Would you like to submit the form with missing info?");
 *     });
 */

  // Shorthand to make it a little easier to call public rails functions from within rails.js
  var rails;

  $.rails = rails = {
    // Link elements bound by jquery-ujs
    linkClickSelector: 'a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]',

    // Select elements bound by jquery-ujs
    inputChangeSelector: 'select[data-remote], input[data-remote], textarea[data-remote]',

    // Form elements bound by jquery-ujs
    formSubmitSelector: 'form',

    // Form input elements bound by jquery-ujs
    formInputClickSelector: 'form input[type=submit], form input[type=image], form button[type=submit], form button:not(button[type])',

    // Form input elements disabled during form submission
    disableSelector: 'input[data-disable-with], button[data-disable-with], textarea[data-disable-with]',

    // Form input elements re-enabled after form submission
    enableSelector: 'input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled',

    // Form required input elements
    requiredInputSelector: 'input[name][required]:not([disabled]),textarea[name][required]:not([disabled])',

    // Form file input elements
    fileInputSelector: 'input:file',

    // Link onClick disable selector with possible reenable after remote submission
    linkDisableSelector: 'a[data-disable-with]',

    // Make sure that every Ajax request sends the CSRF token
    CSRFProtection: function(xhr) {
      var token = $('meta[name="csrf-token"]').attr('content');
      if (token) xhr.setRequestHeader('X-CSRF-Token', token);
    },

    // Triggers an event on an element and returns false if the event result is false
    fire: function(obj, name, data) {
      var event = $.Event(name);
      obj.trigger(event, data);
      return event.result !== false;
    },

    // Default confirm dialog, may be overridden with custom confirm dialog in $.rails.confirm
    confirm: function(message) {
      return confirm(message);
    },

    // Default ajax function, may be overridden with custom function in $.rails.ajax
    ajax: function(options) {
      return $.ajax(options);
    },

    // Default way to get an element's href. May be overridden at $.rails.href.
    href: function(element) {
      return element.attr('href');
    },

    // Submits "remote" forms and links with ajax
    handleRemote: function(element) {
      var method, url, data, crossDomain, dataType, options;

      if (rails.fire(element, 'ajax:before')) {
        crossDomain = element.data('cross-domain') || null;
        dataType = element.data('type') || ($.ajaxSettings && $.ajaxSettings.dataType);

        if (element.is('form')) {
          method = element.attr('method');
          url = element.attr('action');
          data = element.serializeArray();
          // memoized value from clicked submit button
          var button = element.data('ujs:submit-button');
          if (button) {
            data.push(button);
            element.data('ujs:submit-button', null);
          }
        } else if (element.is(rails.inputChangeSelector)) {
          method = element.data('method');
          url = element.data('url');
          data = element.serialize();
          if (element.data('params')) data = data + "&" + element.data('params');
        } else {
          method = element.data('method');
          url = rails.href(element);
          data = element.data('params') || null;
        }

        options = {
          type: method || 'GET', data: data, dataType: dataType, crossDomain: crossDomain,
          // stopping the "ajax:beforeSend" event will cancel the ajax request
          beforeSend: function(xhr, settings) {
            if (settings.dataType === undefined) {
              xhr.setRequestHeader('accept', '*/*;q=0.5, ' + settings.accepts.script);
            }
            return rails.fire(element, 'ajax:beforeSend', [xhr, settings]);
          },
          success: function(data, status, xhr) {
            element.trigger('ajax:success', [data, status, xhr]);
          },
          complete: function(xhr, status) {
            element.trigger('ajax:complete', [xhr, status]);
          },
          error: function(xhr, status, error) {
            element.trigger('ajax:error', [xhr, status, error]);
          }
        };
        // Only pass url to `ajax` options if not blank
        if (url) { options.url = url; }

        return rails.ajax(options);
      } else {
        return false;
      }
    },

    // Handles "data-method" on links such as:
    // <a href="/users/5" data-method="delete" rel="nofollow" data-confirm="Are you sure?">Delete</a>
    handleMethod: function(link) {
      var href = rails.href(link),
        method = link.data('method'),
        target = link.attr('target'),
        csrf_token = $('meta[name=csrf-token]').attr('content'),
        csrf_param = $('meta[name=csrf-param]').attr('content'),
        form = $('<form method="post" action="' + href + '"></form>'),
        metadata_input = '<input name="_method" value="' + method + '" type="hidden" />';

      if (csrf_param !== undefined && csrf_token !== undefined) {
        metadata_input += '<input name="' + csrf_param + '" value="' + csrf_token + '" type="hidden" />';
      }

      if (target) { form.attr('target', target); }

      form.hide().append(metadata_input).appendTo('body');
      form.submit();
    },

    /* Disables form elements:
      - Caches element value in 'ujs:enable-with' data store
      - Replaces element text with value of 'data-disable-with' attribute
      - Sets disabled property to true
    */
    disableFormElements: function(form) {
      form.find(rails.disableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        element.data('ujs:enable-with', element[method]());
        element[method](element.data('disable-with'));
        element.prop('disabled', true);
      });
    },

    /* Re-enables disabled form elements:
      - Replaces element text with cached value from 'ujs:enable-with' data store (created in `disableFormElements`)
      - Sets disabled property to false
    */
    enableFormElements: function(form) {
      form.find(rails.enableSelector).each(function() {
        var element = $(this), method = element.is('button') ? 'html' : 'val';
        if (element.data('ujs:enable-with')) element[method](element.data('ujs:enable-with'));
        element.prop('disabled', false);
      });
    },

   /* For 'data-confirm' attribute:
      - Fires `confirm` event
      - Shows the confirmation dialog
      - Fires the `confirm:complete` event

      Returns `true` if no function stops the chain and user chose yes; `false` otherwise.
      Attaching a handler to the element's `confirm` event that returns a `falsy` value cancels the confirmation dialog.
      Attaching a handler to the element's `confirm:complete` event that returns a `falsy` value makes this function
      return false. The `confirm:complete` event is fired whether or not the user answered true or false to the dialog.
   */
    allowAction: function(element) {
      var message = element.data('confirm'),
          answer = false, callback;
      if (!message) { return true; }

      if (rails.fire(element, 'confirm')) {
        answer = rails.confirm(message);
        callback = rails.fire(element, 'confirm:complete', [answer]);
      }
      return answer && callback;
    },

    // Helper function which checks for blank inputs in a form that match the specified CSS selector
    blankInputs: function(form, specifiedSelector, nonBlank) {
      var inputs = $(), input,
        selector = specifiedSelector || 'input,textarea';
      form.find(selector).each(function() {
        input = $(this);
        // Collect non-blank inputs if nonBlank option is true, otherwise, collect blank inputs
        if (nonBlank ? input.val() : !input.val()) {
          inputs = inputs.add(input);
        }
      });
      return inputs.length ? inputs : false;
    },

    // Helper function which checks for non-blank inputs in a form that match the specified CSS selector
    nonBlankInputs: function(form, specifiedSelector) {
      return rails.blankInputs(form, specifiedSelector, true); // true specifies nonBlank
    },

    // Helper function, needed to provide consistent behavior in IE
    stopEverything: function(e) {
      $(e.target).trigger('ujs:everythingStopped');
      e.stopImmediatePropagation();
      return false;
    },

    // find all the submit events directly bound to the form and
    // manually invoke them. If anyone returns false then stop the loop
    callFormSubmitBindings: function(form, event) {
      var events = form.data('events'), continuePropagation = true;
      if (events !== undefined && events['submit'] !== undefined) {
        $.each(events['submit'], function(i, obj){
          if (typeof obj.handler === 'function') return continuePropagation = obj.handler(event);
        });
      }
      return continuePropagation;
    },

    //  replace element's html with the 'data-disable-with' after storing original html
    //  and prevent clicking on it
    disableElement: function(element) {
      element.data('ujs:enable-with', element.html()); // store enabled state
      element.html(element.data('disable-with')); // set to disabled state
      element.bind('click.railsDisable', function(e) { // prevent further clicking
        return rails.stopEverything(e)
      });
    },

    // restore element to its original state which was disabled by 'disableElement' above
    enableElement: function(element) {
      if (element.data('ujs:enable-with') !== undefined) {
        element.html(element.data('ujs:enable-with')); // set to old enabled state
        // this should be element.removeData('ujs:enable-with')
        // but, there is currently a bug in jquery which makes hyphenated data attributes not get removed
        element.data('ujs:enable-with', false); // clean up cache
      }
      element.unbind('click.railsDisable'); // enable element
    }

  };

  $.ajaxPrefilter(function(options, originalOptions, xhr){ if ( !options.crossDomain ) { rails.CSRFProtection(xhr); }});

  $(document).delegate(rails.linkDisableSelector, 'ajax:complete', function() {
      rails.enableElement($(this));
  });

  $(document).delegate(rails.linkClickSelector, 'click.rails', function(e) {
    var link = $(this), method = link.data('method'), data = link.data('params');
    if (!rails.allowAction(link)) return rails.stopEverything(e);

    if (link.is(rails.linkDisableSelector)) rails.disableElement(link);

    if (link.data('remote') !== undefined) {
      if ( (e.metaKey || e.ctrlKey) && (!method || method === 'GET') && !data ) { return true; }

      if (rails.handleRemote(link) === false) { rails.enableElement(link); }
      return false;

    } else if (link.data('method')) {
      rails.handleMethod(link);
      return false;
    }
  });

  $(document).delegate(rails.inputChangeSelector, 'change.rails', function(e) {
    var link = $(this);
    if (!rails.allowAction(link)) return rails.stopEverything(e);

    rails.handleRemote(link);
    return false;
  });

  $(document).delegate(rails.formSubmitSelector, 'submit.rails', function(e) {
    var form = $(this),
      remote = form.data('remote') !== undefined,
      blankRequiredInputs = rails.blankInputs(form, rails.requiredInputSelector),
      nonBlankFileInputs = rails.nonBlankInputs(form, rails.fileInputSelector);

    if (!rails.allowAction(form)) return rails.stopEverything(e);

    // skip other logic when required values are missing or file upload is present
    if (blankRequiredInputs && form.attr("novalidate") == undefined && rails.fire(form, 'ajax:aborted:required', [blankRequiredInputs])) {
      return rails.stopEverything(e);
    }

    if (remote) {
      if (nonBlankFileInputs) {
        return rails.fire(form, 'ajax:aborted:file', [nonBlankFileInputs]);
      }

      // If browser does not support submit bubbling, then this live-binding will be called before direct
      // bindings. Therefore, we should directly call any direct bindings before remotely submitting form.
      if (!$.support.submitBubbles && $().jquery < '1.7' && rails.callFormSubmitBindings(form, e) === false) return rails.stopEverything(e);

      rails.handleRemote(form);
      return false;

    } else {
      // slight timeout so that the submit button gets properly serialized
      setTimeout(function(){ rails.disableFormElements(form); }, 13);
    }
  });

  $(document).delegate(rails.formInputClickSelector, 'click.rails', function(event) {
    var button = $(this);

    if (!rails.allowAction(button)) return rails.stopEverything(event);

    // register the pressed submit button
    var name = button.attr('name'),
      data = name ? {name:name, value:button.val()} : null;

    button.closest('form').data('ujs:submit-button', data);
  });

  $(document).delegate(rails.formSubmitSelector, 'ajax:beforeSend.rails', function(event) {
    if (this == event.target) rails.disableFormElements($(this));
  });

  $(document).delegate(rails.formSubmitSelector, 'ajax:complete.rails', function(event) {
    if (this == event.target) rails.enableFormElements($(this));
  });

})( jQuery );
/* ===================================================
 * bootstrap-transition.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#transitions
 * ===================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function( $ ) {

  $(function () {

    "use strict"

    /* CSS TRANSITION SUPPORT (https://gist.github.com/373874)
     * ======================================================= */

    $.support.transition = (function () {
      var thisBody = document.body || document.documentElement
        , thisStyle = thisBody.style
        , support = thisStyle.transition !== undefined || thisStyle.WebkitTransition !== undefined || thisStyle.MozTransition !== undefined || thisStyle.MsTransition !== undefined || thisStyle.OTransition !== undefined

      return support && {
        end: (function () {
          var transitionEnd = "TransitionEnd"
          if ( $.browser.webkit ) {
          	transitionEnd = "webkitTransitionEnd"
          } else if ( $.browser.mozilla ) {
          	transitionEnd = "transitionend"
          } else if ( $.browser.opera ) {
          	transitionEnd = "oTransitionEnd"
          }
          return transitionEnd
        }())
      }
    })()

  })
  
}( window.jQuery )
;
/* ==========================================================
 * bootstrap-alert.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#alerts
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */



!function( $ ){

  "use strict"

 /* ALERT CLASS DEFINITION
  * ====================== */

  var dismiss = '[data-dismiss="alert"]'
    , Alert = function ( el ) {
        $(el).on('click', dismiss, this.close)
      }

  Alert.prototype = {

    constructor: Alert

  , close: function ( e ) {
      var $this = $(this)
        , selector = $this.attr('data-target')
        , $parent

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      $parent = $(selector)
      $parent.trigger('close')

      e && e.preventDefault()

      $parent.length || ($parent = $this.hasClass('alert') ? $this : $this.parent())

      $parent.removeClass('in')

      function removeElement() {
        $parent.remove()
        $parent.trigger('closed')
      }

      $.support.transition && $parent.hasClass('fade') ?
        $parent.on($.support.transition.end, removeElement) :
        removeElement()
    }

  }


 /* ALERT PLUGIN DEFINITION
  * ======================= */

  $.fn.alert = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('alert')
      if (!data) $this.data('alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.alert.Constructor = Alert


 /* ALERT DATA-API
  * ============== */

  $(function () {
    $('body').on('click.alert.data-api', dismiss, Alert.prototype.close)
  })

}( window.jQuery )
;
/* ============================================================
 * bootstrap-button.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#buttons
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function( $ ){

  "use strict"

 /* BUTTON PUBLIC CLASS DEFINITION
  * ============================== */

  var Button = function ( element, options ) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.button.defaults, options)
  }

  Button.prototype = {

      constructor: Button

    , setState: function ( state ) {
        var d = 'disabled'
          , $el = this.$element
          , data = $el.data()
          , val = $el.is('input') ? 'val' : 'html'

        state = state + 'Text'
        data.resetText || $el.data('resetText', $el[val]())

        $el[val](data[state] || this.options[state])

        // push to event loop to allow forms to submit
        setTimeout(function () {
          state == 'loadingText' ?
            $el.addClass(d).attr(d, d) :
            $el.removeClass(d).removeAttr(d)
        }, 0)
      }

    , toggle: function () {
        var $parent = this.$element.parent('[data-toggle="buttons-radio"]')

        $parent && $parent
          .find('.active')
          .removeClass('active')

        this.$element.toggleClass('active')
      }

  }


 /* BUTTON PLUGIN DEFINITION
  * ======================== */

  $.fn.button = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('button')
        , options = typeof option == 'object' && option
      if (!data) $this.data('button', (data = new Button(this, options)))
      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  $.fn.button.defaults = {
    loadingText: 'loading...'
  }

  $.fn.button.Constructor = Button


 /* BUTTON DATA-API
  * =============== */

  $(function () {
    $('body').on('click.button.data-api', '[data-toggle^=button]', function ( e ) {
      $(e.target).button('toggle')
    })
  })

}( window.jQuery )
;
/* ==========================================================
 * bootstrap-carousel.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#carousel
 * ==========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */



!function( $ ){

  "use strict"

 /* CAROUSEL CLASS DEFINITION
  * ========================= */

  var Carousel = function (element, options) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.carousel.defaults, options)
    this.options.slide && this.slide(this.options.slide)
  }

  Carousel.prototype = {

    cycle: function () {
      this.interval = setInterval($.proxy(this.next, this), this.options.interval)
      return this
    }

  , to: function (pos) {
      var $active = this.$element.find('.active')
        , children = $active.parent().children()
        , activePos = children.index($active)
        , that = this

      if (pos > (children.length - 1) || pos < 0) return

      if (this.sliding) {
        return this.$element.one('slid', function () {
          that.to(pos)
        })
      }

      if (activePos == pos) {
        return this.pause().cycle()
      }

      return this.slide(pos > activePos ? 'next' : 'prev', $(children[pos]))
    }

  , pause: function () {
      clearInterval(this.interval)
      return this
    }

  , next: function () {
      if (this.sliding) return
      return this.slide('next')
    }

  , prev: function () {
      if (this.sliding) return
      return this.slide('prev')
    }

  , slide: function (type, next) {
      var $active = this.$element.find('.active')
        , $next = next || $active[type]()
        , isCycling = this.interval
        , direction = type == 'next' ? 'left' : 'right'
        , fallback  = type == 'next' ? 'first' : 'last'
        , that = this

      this.sliding = true

      isCycling && this.pause()

      $next = $next.length ? $next : this.$element.find('.item')[fallback]()

      if (!$.support.transition && this.$element.hasClass('slide')) {
        this.$element.trigger('slide')
        $active.removeClass('active')
        $next.addClass('active')
        this.sliding = false
        this.$element.trigger('slid')
      } else {
        $next.addClass(type)
        $next[0].offsetWidth // force reflow
        $active.addClass(direction)
        $next.addClass(direction)
        this.$element.trigger('slide')
        this.$element.one($.support.transition.end, function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () { that.$element.trigger('slid') }, 0)
        })
      }

      isCycling && this.cycle()

      return this
    }

  }


 /* CAROUSEL PLUGIN DEFINITION
  * ========================== */

  $.fn.carousel = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('carousel')
        , options = typeof option == 'object' && option
      if (!data) $this.data('carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (typeof option == 'string' || (option = options.slide)) data[option]()
      else data.cycle()
    })
  }

  $.fn.carousel.defaults = {
    interval: 5000
  }

  $.fn.carousel.Constructor = Carousel


 /* CAROUSEL DATA-API
  * ================= */

  $(function () {
    $('body').on('click.carousel.data-api', '[data-slide]', function ( e ) {
      var $this = $(this), href
        , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
        , options = !$target.data('modal') && $.extend({}, $target.data(), $this.data())
      $target.carousel(options)
      e.preventDefault()
    })
  })

}( window.jQuery )
;
/* =============================================================
 * bootstrap-collapse.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#collapse
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function( $ ){

  "use strict"

  var Collapse = function ( element, options ) {
  	this.$element = $(element)
    this.options = $.extend({}, $.fn.collapse.defaults, options)

    if (this.options["parent"]) {
      this.$parent = $(this.options["parent"])
    }

    this.options.toggle && this.toggle()
  }

  Collapse.prototype = {

    constructor: Collapse

  , dimension: function () {
      var hasWidth = this.$element.hasClass('width')
      return hasWidth ? 'width' : 'height'
    }

  , show: function () {
      var dimension = this.dimension()
        , scroll = $.camelCase(['scroll', dimension].join('-'))
        , actives = this.$parent && this.$parent.find('.in')
        , hasData

      if (actives && actives.length) {
        hasData = actives.data('collapse')
        actives.collapse('hide')
        hasData || actives.data('collapse', null)
      }

      this.$element[dimension](0)
      this.transition('addClass', 'show', 'shown')
      this.$element[dimension](this.$element[0][scroll])

    }

  , hide: function () {
      var dimension = this.dimension()
      this.reset(this.$element[dimension]())
      this.transition('removeClass', 'hide', 'hidden')
      this.$element[dimension](0)
    }

  , reset: function ( size ) {
      var dimension = this.dimension()

      this.$element
        .removeClass('collapse')
        [dimension](size || 'auto')
        [0].offsetWidth

      this.$element.addClass('collapse')
    }

  , transition: function ( method, startEvent, completeEvent ) {
      var that = this
        , complete = function () {
            if (startEvent == 'show') that.reset()
            that.$element.trigger(completeEvent)
          }

      this.$element
        .trigger(startEvent)
        [method]('in')

      $.support.transition && this.$element.hasClass('collapse') ?
        this.$element.one($.support.transition.end, complete) :
        complete()
  	}

  , toggle: function () {
      this[this.$element.hasClass('in') ? 'hide' : 'show']()
  	}

  }

  /* COLLAPSIBLE PLUGIN DEFINITION
  * ============================== */

  $.fn.collapse = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('collapse')
        , options = typeof option == 'object' && option
      if (!data) $this.data('collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.collapse.defaults = {
    toggle: true
  }

  $.fn.collapse.Constructor = Collapse


 /* COLLAPSIBLE DATA-API
  * ==================== */

  $(function () {
    $('body').on('click.collapse.data-api', '[data-toggle=collapse]', function ( e ) {
      var $this = $(this), href
        , target = $this.attr('data-target')
          || e.preventDefault()
          || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') //strip for ie7
        , option = $(target).data('collapse') ? 'toggle' : $this.data()
      $(target).collapse(option)
    })
  })

}( window.jQuery )
;
/* ============================================================
 * bootstrap-dropdown.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#dropdowns
 * ============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */



!function( $ ){

  "use strict"

 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle="dropdown"]'
    , Dropdown = function ( element ) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function ( e ) {
      var $this = $(this)
        , selector = $this.attr('data-target')
        , $parent
        , isActive

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      $parent = $(selector)
      $parent.length || ($parent = $this.parent())

      isActive = $parent.hasClass('open')

      clearMenus()
      !isActive && $parent.toggleClass('open')

      return false
    }

  }

  function clearMenus() {
    $(toggle).parent().removeClass('open')
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  $.fn.dropdown = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(function () {
    $('html').on('click.dropdown.data-api', clearMenus)
    $('body').on('click.dropdown.data-api', toggle, Dropdown.prototype.toggle)
  })

}( window.jQuery )
;
/* =========================================================
 * bootstrap-modal.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#modals
 * =========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================= */



!function( $ ){

  "use strict"

 /* MODAL CLASS DEFINITION
  * ====================== */

  var Modal = function ( content, options ) {
    this.options = $.extend({}, $.fn.modal.defaults, options)
    this.$element = $(content)
      .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
  }

  Modal.prototype = {

      constructor: Modal

    , toggle: function () {
        return this[!this.isShown ? 'show' : 'hide']()
      }

    , show: function () {
        var that = this

        if (this.isShown) return

        $('body').addClass('modal-open')

        this.isShown = true
        this.$element.trigger('show')

        escape.call(this)
        backdrop.call(this, function () {
          var transition = $.support.transition && that.$element.hasClass('fade')

          !that.$element.parent().length && that.$element.appendTo(document.body) //don't move modals dom position

          that.$element
            .show()

          if (transition) {
            that.$element[0].offsetWidth // force reflow
          }

          that.$element.addClass('in')

          transition ?
            that.$element.one($.support.transition.end, function () { that.$element.trigger('shown') }) :
            that.$element.trigger('shown')

        })
      }

    , hide: function ( e ) {
        e && e.preventDefault()

        if (!this.isShown) return

        var that = this
        this.isShown = false

        $('body').removeClass('modal-open')

        escape.call(this)

        this.$element
          .trigger('hide')
          .removeClass('in')

        $.support.transition && this.$element.hasClass('fade') ?
          hideWithTransition.call(this) :
          hideModal.call(this)
      }

  }


 /* MODAL PRIVATE METHODS
  * ===================== */

  function hideWithTransition() {
    var that = this
      , timeout = setTimeout(function () {
          that.$element.off($.support.transition.end)
          hideModal.call(that)
        }, 500)

    this.$element.one($.support.transition.end, function () {
      clearTimeout(timeout)
      hideModal.call(that)
    })
  }

  function hideModal( that ) {
    this.$element
      .hide()
      .trigger('hidden')

    backdrop.call(this)
  }

  function backdrop( callback ) {
    var that = this
      , animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
        .appendTo(document.body)

      if (this.options.backdrop != 'static') {
        this.$backdrop.click($.proxy(this.hide, this))
      }

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      doAnimate ?
        this.$backdrop.one($.support.transition.end, callback) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      $.support.transition && this.$element.hasClass('fade')?
        this.$backdrop.one($.support.transition.end, $.proxy(removeBackdrop, this)) :
        removeBackdrop.call(this)

    } else if (callback) {
      callback()
    }
  }

  function removeBackdrop() {
    this.$backdrop.remove()
    this.$backdrop = null
  }

  function escape() {
    var that = this
    if (this.isShown && this.options.keyboard) {
      $(document).on('keyup.dismiss.modal', function ( e ) {
        e.which == 27 && that.hide()
      })
    } else if (!this.isShown) {
      $(document).off('keyup.dismiss.modal')
    }
  }


 /* MODAL PLUGIN DEFINITION
  * ======================= */

  $.fn.modal = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('modal')
        , options = typeof option == 'object' && option
      if (!data) $this.data('modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option]()
      else data.show()
    })
  }

  $.fn.modal.defaults = {
      backdrop: true
    , keyboard: true
  }

  $.fn.modal.Constructor = Modal


 /* MODAL DATA-API
  * ============== */

  $(function () {
    $('body').on('click.modal.data-api', '[data-toggle="modal"]', function ( e ) {
      var $this = $(this), href
        , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
        , option = $target.data('modal') ? 'toggle' : $.extend({}, $target.data(), $this.data())

      e.preventDefault()
      $target.modal(option)
    })
  })

}( window.jQuery )
;
/* =============================================================
 * bootstrap-scrollspy.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#scrollspy
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================== */


!function ( $ ) {

  "use strict"

  /* SCROLLSPY CLASS DEFINITION
   * ========================== */

  function ScrollSpy( element, options) {
    var process = $.proxy(this.process, this)
      , $element = $(element).is('body') ? $(window) : $(element)
      , href
    this.options = $.extend({}, $.fn.scrollspy.defaults, options)
    this.$scrollElement = $element.on('scroll.scroll.data-api', process)
    this.selector = (this.options.target
      || ((href = $(element).attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
      || '') + ' .nav li > a'
    this.$body = $('body').on('click.scroll.data-api', this.selector, process)
    this.refresh()
    this.process()
  }

  ScrollSpy.prototype = {

      constructor: ScrollSpy

    , refresh: function () {
        this.targets = this.$body
          .find(this.selector)
          .map(function () {
            var href = $(this).attr('href')
            return /^#\w/.test(href) && $(href).length ? href : null
          })

        this.offsets = $.map(this.targets, function (id) {
          return $(id).position().top
        })
      }

    , process: function () {
        var scrollTop = this.$scrollElement.scrollTop() + this.options.offset
          , offsets = this.offsets
          , targets = this.targets
          , activeTarget = this.activeTarget
          , i

        for (i = offsets.length; i--;) {
          activeTarget != targets[i]
            && scrollTop >= offsets[i]
            && (!offsets[i + 1] || scrollTop <= offsets[i + 1])
            && this.activate( targets[i] )
        }
      }

    , activate: function (target) {
        var active

        this.activeTarget = target

        this.$body
          .find(this.selector).parent('.active')
          .removeClass('active')

        active = this.$body
          .find(this.selector + '[href="' + target + '"]')
          .parent('li')
          .addClass('active')

        if ( active.parent('.dropdown-menu') )  {
          active.closest('li.dropdown').addClass('active')
        }
      }

  }


 /* SCROLLSPY PLUGIN DEFINITION
  * =========================== */

  $.fn.scrollspy = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('scrollspy')
        , options = typeof option == 'object' && option
      if (!data) $this.data('scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.scrollspy.Constructor = ScrollSpy

  $.fn.scrollspy.defaults = {
    offset: 10
  }


 /* SCROLLSPY DATA-API
  * ================== */

  $(function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      $spy.scrollspy($spy.data())
    })
  })

}( window.jQuery )
;
/* ========================================================
 * bootstrap-tab.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#tabs
 * ========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ======================================================== */



!function( $ ){

  "use strict"

 /* TAB CLASS DEFINITION
  * ==================== */

  var Tab = function ( element ) {
    this.element = $(element)
  }

  Tab.prototype = {

    constructor: Tab

  , show: function () {
      var $this = this.element
        , $ul = $this.closest('ul:not(.dropdown-menu)')
        , selector = $this.attr('data-target')
        , previous
        , $target

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      if ( $this.parent('li').hasClass('active') ) return

      previous = $ul.find('.active a').last()[0]

      $this.trigger({
        type: 'show'
      , relatedTarget: previous
      })

      $target = $(selector)

      this.activate($this.parent('li'), $ul)
      this.activate($target, $target.parent(), function () {
        $this.trigger({
          type: 'shown'
        , relatedTarget: previous
        })
      })
    }

  , activate: function ( element, container, callback) {
      var $active = container.find('> .active')
        , transition = callback
            && $.support.transition
            && $active.hasClass('fade')

      function next() {
        $active
          .removeClass('active')
          .find('> .dropdown-menu > .active')
          .removeClass('active')

        element.addClass('active')

        if (transition) {
          element[0].offsetWidth // reflow for transition
          element.addClass('in')
        } else {
          element.removeClass('fade')
        }

        if ( element.parent('.dropdown-menu') ) {
          element.closest('li.dropdown').addClass('active')
        }

        callback && callback()
      }

      transition ?
        $active.one($.support.transition.end, next) :
        next()

      $active.removeClass('in')
    }
  }


 /* TAB PLUGIN DEFINITION
  * ===================== */

  $.fn.tab = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tab')
      if (!data) $this.data('tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tab.Constructor = Tab


 /* TAB DATA-API
  * ============ */

  $(function () {
    $('body').on('click.tab.data-api', '[data-toggle="tab"], [data-toggle="pill"]', function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
  })

}( window.jQuery )
;
/* ===========================================================
 * bootstrap-tooltip.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#tooltips
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */


!function( $ ) {

  "use strict"

 /* TOOLTIP PUBLIC CLASS DEFINITION
  * =============================== */

  var Tooltip = function ( element, options ) {
    this.init('tooltip', element, options)
  }

  Tooltip.prototype = {

    constructor: Tooltip

  , init: function ( type, element, options ) {
      var eventIn
        , eventOut

      this.type = type
      this.$element = $(element)
      this.options = this.getOptions(options)
      this.enabled = true

      if (this.options.trigger != 'manual') {
        eventIn  = this.options.trigger == 'hover' ? 'mouseenter' : 'focus'
        eventOut = this.options.trigger == 'hover' ? 'mouseleave' : 'blur'
        this.$element.on(eventIn, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut, this.options.selector, $.proxy(this.leave, this))
      }

      this.options.selector ?
        (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
        this.fixTitle()
    }

  , getOptions: function ( options ) {
      options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

      if (options.delay && typeof options.delay == 'number') {
        options.delay = {
          show: options.delay
        , hide: options.delay
        }
      }

      return options
    }

  , enter: function ( e ) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (!self.options.delay || !self.options.delay.show) {
        self.show()
      } else {
        self.hoverState = 'in'
        setTimeout(function() {
          if (self.hoverState == 'in') {
            self.show()
          }
        }, self.options.delay.show)
      }
    }

  , leave: function ( e ) {
      var self = $(e.currentTarget)[this.type](this._options).data(this.type)

      if (!self.options.delay || !self.options.delay.hide) {
        self.hide()
      } else {
        self.hoverState = 'out'
        setTimeout(function() {
          if (self.hoverState == 'out') {
            self.hide()
          }
        }, self.options.delay.hide)
      }
    }

  , show: function () {
      var $tip
        , inside
        , pos
        , actualWidth
        , actualHeight
        , placement
        , tp

      if (this.hasContent() && this.enabled) {
        $tip = this.tip()
        this.setContent()

        if (this.options.animation) {
          $tip.addClass('fade')
        }

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        inside = /in/.test(placement)

        $tip
          .remove()
          .css({ top: 0, left: 0, display: 'block' })
          .appendTo(inside ? this.$element : document.body)

        pos = this.getPosition(inside)

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (inside ? placement.split(' ')[1] : placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        $tip
          .css(tp)
          .addClass(placement)
          .addClass('in')
      }
    }

  , setContent: function () {
      var $tip = this.tip()
      $tip.find('.tooltip-inner').html(this.getTitle())
      $tip.removeClass('fade in top bottom left right')
    }

  , hide: function () {
      var that = this
        , $tip = this.tip()

      $tip.removeClass('in')

      function removeWithAnimation() {
        var timeout = setTimeout(function () {
          $tip.off($.support.transition.end).remove()
        }, 500)

        $tip.one($.support.transition.end, function () {
          clearTimeout(timeout)
          $tip.remove()
        })
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        removeWithAnimation() :
        $tip.remove()
    }

  , fixTitle: function () {
      var $e = this.$element
      if ($e.attr('title') || typeof($e.attr('data-original-title')) != 'string') {
        $e.attr('data-original-title', $e.attr('title') || '').removeAttr('title')
      }
    }

  , hasContent: function () {
      return this.getTitle()
    }

  , getPosition: function (inside) {
      return $.extend({}, (inside ? {top: 0, left: 0} : this.$element.offset()), {
        width: this.$element[0].offsetWidth
      , height: this.$element[0].offsetHeight
      })
    }

  , getTitle: function () {
      var title
        , $e = this.$element
        , o = this.options

      title = $e.attr('data-original-title')
        || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

      title = title.toString().replace(/(^\s*|\s*$)/, "")

      return title
    }

  , tip: function () {
      return this.$tip = this.$tip || $(this.options.template)
    }

  , validate: function () {
      if (!this.$element[0].parentNode) {
        this.hide()
        this.$element = null
        this.options = null
      }
    }

  , enable: function () {
      this.enabled = true
    }

  , disable: function () {
      this.enabled = false
    }

  , toggleEnabled: function () {
      this.enabled = !this.enabled
    }

  , toggle: function () {
      this[this.tip().hasClass('in') ? 'hide' : 'show']()
    }

  }


 /* TOOLTIP PLUGIN DEFINITION
  * ========================= */

  $.fn.tooltip = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('tooltip')
        , options = typeof option == 'object' && option
      if (!data) $this.data('tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.tooltip.Constructor = Tooltip

  $.fn.tooltip.defaults = {
    animation: true
  , delay: 0
  , selector: false
  , placement: 'top'
  , trigger: 'hover'
  , title: ''
  , template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>'
  }

}( window.jQuery )
;
/* ===========================================================
 * bootstrap-popover.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#popovers
 * ===========================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =========================================================== */



!function( $ ) {

 "use strict"

  var Popover = function ( element, options ) {
    this.init('popover', element, options)
  }

  /* NOTE: POPOVER EXTENDS BOOTSTRAP-TOOLTIP.js
     ========================================== */

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype, {

    constructor: Popover

  , setContent: function () {
      var $tip = this.tip()
        , title = this.getTitle()
        , content = this.getContent()

      $tip.find('.popover-title')[ $.type(title) == 'object' ? 'append' : 'html' ](title)
      $tip.find('.popover-content > *')[ $.type(content) == 'object' ? 'append' : 'html' ](content)

      $tip.removeClass('fade top bottom left right in')
    }

  , hasContent: function () {
      return this.getTitle() || this.getContent()
    }

  , getContent: function () {
      var content
        , $e = this.$element
        , o = this.options

      content = $e.attr('data-content')
        || (typeof o.content == 'function' ? o.content.call($e[0]) :  o.content)

      content = content.toString().replace(/(^\s*|\s*$)/, "")

      return content
    }

  , tip: function() {
      if (!this.$tip) {
        this.$tip = $(this.options.template)
      }
      return this.$tip
    }

  })


 /* POPOVER PLUGIN DEFINITION
  * ======================= */

  $.fn.popover = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popover')
        , options = typeof option == 'object' && option
      if (!data) $this.data('popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.popover.Constructor = Popover

  $.fn.popover.defaults = $.extend({} , $.fn.tooltip.defaults, {
    placement: 'right'
  , content: ''
  , template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
  })

}( window.jQuery )
;
/* =============================================================
 * bootstrap-typeahead.js v2.0.0
 * http://twitter.github.com/bootstrap/javascript.html#typeahead
 * =============================================================
 * Copyright 2012 Twitter, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */


!function( $ ){

  "use strict"

  var Typeahead = function ( element, options ) {
    this.$element = $(element)
    this.options = $.extend({}, $.fn.typeahead.defaults, options)
    this.matcher = this.options.matcher || this.matcher
    this.sorter = this.options.sorter || this.sorter
    this.highlighter = this.options.highlighter || this.highlighter
    this.$menu = $(this.options.menu).appendTo('body')
    this.source = this.options.source
    this.shown = false
    this.listen()
  }

  Typeahead.prototype = {

    constructor: Typeahead

  , select: function () {
      var val = this.$menu.find('.active').attr('data-value')
      this.$element.val(val)
      return this.hide()
    }

  , show: function () {
      var pos = $.extend({}, this.$element.offset(), {
        height: this.$element[0].offsetHeight
      })

      this.$menu.css({
        top: pos.top + pos.height
      , left: pos.left
      })

      this.$menu.show()
      this.shown = true
      return this
    }

  , hide: function () {
      this.$menu.hide()
      this.shown = false
      return this
    }

  , lookup: function (event) {
      var that = this
        , items
        , q

      this.query = this.$element.val()

      if (!this.query) {
        return this.shown ? this.hide() : this
      }

      items = $.grep(this.source, function (item) {
        if (that.matcher(item)) return item
      })

      items = this.sorter(items)

      if (!items.length) {
        return this.shown ? this.hide() : this
      }

      return this.render(items.slice(0, this.options.items)).show()
    }

  , matcher: function (item) {
      return ~item.toLowerCase().indexOf(this.query.toLowerCase())
    }

  , sorter: function (items) {
      var beginswith = []
        , caseSensitive = []
        , caseInsensitive = []
        , item

      while (item = items.shift()) {
        if (!item.toLowerCase().indexOf(this.query.toLowerCase())) beginswith.push(item)
        else if (~item.indexOf(this.query)) caseSensitive.push(item)
        else caseInsensitive.push(item)
      }

      return beginswith.concat(caseSensitive, caseInsensitive)
    }

  , highlighter: function (item) {
      return item.replace(new RegExp('(' + this.query + ')', 'ig'), function ($1, match) {
        return '<strong>' + match + '</strong>'
      })
    }

  , render: function (items) {
      var that = this

      items = $(items).map(function (i, item) {
        i = $(that.options.item).attr('data-value', item)
        i.find('a').html(that.highlighter(item))
        return i[0]
      })

      items.first().addClass('active')
      this.$menu.html(items)
      return this
    }

  , next: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , next = active.next()

      if (!next.length) {
        next = $(this.$menu.find('li')[0])
      }

      next.addClass('active')
    }

  , prev: function (event) {
      var active = this.$menu.find('.active').removeClass('active')
        , prev = active.prev()

      if (!prev.length) {
        prev = this.$menu.find('li').last()
      }

      prev.addClass('active')
    }

  , listen: function () {
      this.$element
        .on('blur',     $.proxy(this.blur, this))
        .on('keypress', $.proxy(this.keypress, this))
        .on('keyup',    $.proxy(this.keyup, this))

      if ($.browser.webkit || $.browser.msie) {
        this.$element.on('keydown', $.proxy(this.keypress, this))
      }

      this.$menu
        .on('click', $.proxy(this.click, this))
        .on('mouseenter', 'li', $.proxy(this.mouseenter, this))
    }

  , keyup: function (e) {
      e.stopPropagation()
      e.preventDefault()

      switch(e.keyCode) {
        case 40: // down arrow
        case 38: // up arrow
          break

        case 9: // tab
        case 13: // enter
          if (!this.shown) return
          this.select()
          break

        case 27: // escape
          this.hide()
          break

        default:
          this.lookup()
      }

  }

  , keypress: function (e) {
      e.stopPropagation()
      if (!this.shown) return

      switch(e.keyCode) {
        case 9: // tab
        case 13: // enter
        case 27: // escape
          e.preventDefault()
          break

        case 38: // up arrow
          e.preventDefault()
          this.prev()
          break

        case 40: // down arrow
          e.preventDefault()
          this.next()
          break
      }
    }

  , blur: function (e) {
      var that = this
      e.stopPropagation()
      e.preventDefault()
      setTimeout(function () { that.hide() }, 150)
    }

  , click: function (e) {
      e.stopPropagation()
      e.preventDefault()
      this.select()
    }

  , mouseenter: function (e) {
      this.$menu.find('.active').removeClass('active')
      $(e.currentTarget).addClass('active')
    }

  }


  /* TYPEAHEAD PLUGIN DEFINITION
   * =========================== */

  $.fn.typeahead = function ( option ) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('typeahead')
        , options = typeof option == 'object' && option
      if (!data) $this.data('typeahead', (data = new Typeahead(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.typeahead.defaults = {
    source: []
  , items: 8
  , menu: '<ul class="typeahead dropdown-menu"></ul>'
  , item: '<li><a href="#"></a></li>'
  }

  $.fn.typeahead.Constructor = Typeahead


 /* TYPEAHEAD DATA-API
  * ================== */

  $(function () {
    $('body').on('focus.typeahead.data-api', '[data-provide="typeahead"]', function (e) {
      var $this = $(this)
      if ($this.data('typeahead')) return
      e.preventDefault()
      $this.typeahead($this.data())
    })
  })

}( window.jQuery )
;












/*
* Kendo UI Web v2012.2.710 (http://kendoui.com)
* Copyright 2012 Telerik AD. All rights reserved.
*
* Kendo UI Web commercial licenses may be obtained at http://kendoui.com/web-license
* If you do not own a commercial license, this file shall be governed by the
* GNU General Public License (GPL) version 3.
* For GPL requirements, please review: http://www.gnu.org/copyleft/gpl.html
*/

;(function(a,b){function bk(d,e){var f={},g,h;for(g in e)h=bj(d,g),h!==b&&(bf.test(g)&&(h=c.template(a("#"+h).html())),f[g]=h);return f}function bj(d,e){var f;e.indexOf("data")===0&&(e=e.substring(4),e=e.charAt(0).toLowerCase()+e.substring(1)),e=e.replace(bi,"-$1"),f=d.getAttribute("data-"+c.ns+e),f===null?f=b:f==="null"?f=null:f==="true"?f=!0:f==="false"?f=!1:isNaN(parseFloat(f))?bg.test(f)&&!bh.test(f)&&(f=a.parseJSON(f)):f=parseFloat(f);return f}function _(a){return(""+a).replace(Y,"&amp;").replace(Z,"&lt;").replace($,"&gt;")}function X(a,b,c,d){b&&(b=b.split(" "),e(b,function(b,c){a.toggleClass(c,d)}));return a}function W(a,b,c,d,e,f){return T.transitionPromise(a,b,U(c,d,e,f))}function V(b,c,d,e,f){b.each(function(b,g){g=a(g),g.queue(function(){T.promise(g,U(c,d,e,f))})});return b}function U(a,b,c,e){typeof a===r&&(i(b)&&(e=b,b=400,c=!1),i(c)&&(e=c,c=!1),typeof b===v&&(c=b,b=400),a={effects:a,duration:b,reverse:c,complete:e});return d({effects:{},duration:400,reverse:!1,init:h,teardown:h,hide:!1,show:!1},a,{completeCallback:a.complete,complete:h})}function S(a){var b={};e(typeof a=="string"?a.split(" "):a,function(a){b[a]=this});return b}function Q(a,b){b||(b="offset");var c=a[b](),d=m.mobileOS;if(m.touch&&d.ios&&d.flatVersion<410){var e=b=="offset"?c:a.offset(),f=c.left==e.left&&c.top==e.top;if(f)return{top:c.top-window.scrollY,left:c.left-window.scrollX}}return c}function P(b){return a.trim(a(b).contents().filter(function(){return this.nodeType!=8}).html())===""}function O(a){var b=0,c;for(c in a)a.hasOwnProperty(c)&&c!="toJSON"&&b++;return b}function N(a,b){var c,d,e,f;for(c in b)d=b[c],e=typeof d,e===t&&d!==null&&d.constructor!==Array?d instanceof Date?a[c]=new Date(d.getTime()):(f=a[c],typeof f===t?a[c]=f||{}:a[c]={},N(a[c],d)):e!==w&&(a[c]=d);return a}function M(a){var b=1,c=arguments.length;for(b=1;b<c;b++)N(a,arguments[b]);return a}function L(b){var d=a.browser,e;if(!b.parent().hasClass("k-animation-container")){var f=b.css(c.support.transitions.css+"box-shadow")||b.css("box-shadow"),g=f?f.match(p)||[0,0,0,0,0]:[0,0,0,0,0],h=j.max(+g[3],+(g[4]||0)),i=-g[1]+h,k=+g[1]+h,l=+g[2]+h,m=b[0].style.width,o=b[0].style.height,q=n.test(m),r=n.test(o);d.opera&&(i=k=l=5),e=q||r,q||(m=b.outerWidth()),r||(o=b.outerHeight()),b.wrap(a("<div/>").addClass("k-animation-container").css({width:m,height:o,marginLeft:-i,paddingLeft:i,paddingRight:k,paddingBottom:l})),e&&b.css({width:"100%",height:"100%",boxSizing:"border-box",mozBoxSizing:"border-box",webkitBoxSizing:"border-box"})}else{var s=b.parent(".k-animation-container"),t=s[0].style;s.is(":hidden")&&s.show(),e=n.test(t.width)||n.test(t.height),e||s.css({width:b.outerWidth(),height:b.outerHeight()})}d.msie&&j.floor(d.version)<=7&&b.css({zoom:1});return b.parent()}function K(a){return a<10?"0"+a:a}function D(a,b){if(b)return"'"+a.split("'").join("\\'").split('\\"').join('\\\\\\"').replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\t/g,"\\t")+"'";var c=a.charAt(0),d=a.substring(1);return c==="="?"+("+d+")+":c===":"?"+e("+d+")+":";"+a+";o+="}function B(){}var c=window.kendo=window.kendo||{},d=a.extend,e=a.each,f=a.proxy,g=a.isArray,h=a.noop,i=a.isFunction,j=Math,k,l=window.JSON||{},m={},n=/%/,o=/\{(\d+)(:[^\}]+)?\}/g,p=/(\d+?)px\s*(\d+?)px\s*(\d+?)px\s*(\d+?)?/i,q="function",r="string",s="number",t="object",u="null",v="boolean",w="undefined",x={},y={},z=[].slice,A=window.Globalize;B.extend=function(a){var b=function(){},c,e=this,f=a&&a.init?a.init:function(){e.apply(this,arguments)},g;b.prototype=e.prototype,g=f.fn=f.prototype=new b;for(c in a)typeof a[c]!==t||a[c]instanceof Array||a[c]===null?g[c]=a[c]:g[c]=d(!0,{},b.prototype[c],a[c]);g.constructor=f,f.extend=e.extend;return f};var C=B.extend({init:function(){this._events={}},bind:function(a,b,c){var d=this,e,f=typeof a===r?[a]:a,g,h,i,j=typeof b===q,k;for(e=0,g=f.length;e<g;e++)a=f[e],i=j?b:b[a],i&&(c&&(h=i,i=function(){d.unbind(a,i),h.apply(d,arguments)}),k=d._events[a]=d._events[a]||[],k.push(i));return d},one:function(a,b){return this.bind(a,b,!0)},first:function(a,b){var c=this,d,e=typeof a===r?[a]:a,f,g,h=typeof b===q,i;for(d=0,f=e.length;d<f;d++)a=e[d],g=h?b:b[a],g&&(i=c._events[a]=c._events[a]||[],i.unshift(g));return c},trigger:function(a,b){var c=this,d=c._events[a],e,f,g=!1;if(d){b=b||{},b.sender=c,b.preventDefault=function(){g=!0},b.isDefaultPrevented=function(){return g},d=d.slice();for(e=0,f=d.length;e<f;e++)d[e].call(c,b)}return g},unbind:function(a,b){var c=this,d=c._events[a],e,f;if(d)if(b)for(e=0,f=d.length;e<f;e++)d[e]===b&&d.splice(e,1);else c._events[a]=[];return c}}),E=/^\w+/,F=/\$\{([^}]*)\}/g,G=/\\\}/g,H=/__CURLY__/g,I=/\\#/g,J=/__SHARP__/g;k={paramName:"data",useWithBlock:!0,render:function(a,b){var c,d,e="";for(c=0,d=b.length;c<d;c++)e+=a(b[c]);return e},compile:function(b,e){var f=d({},this,e),g=f.paramName,h=g.match(E)[0],j=f.useWithBlock,k="var o,e=kendo.htmlEncode;",l,m;if(i(b)){if(b.length===2)return function(c){return b(a,{data:c}).join("")};return b}k+=j?"with("+g+"){":"",k+="o=",l=b.replace(G,"__CURLY__").replace(F,"#=e($1)#").replace(H,"}").replace(I,"__SHARP__").split("#");for(m=0;m<l.length;m++)k+=D(l[m],m%2===0);k+=j?";}":";",k+="return o;",k=k.replace(J,"#");try{return new Function(h,k)}catch(n){throw new Error(c.format("Invalid template:'{0}' Generated code:'{1}'",b,k))}}},function(){function h(a,d){var i,j,k,l,m=b,n,o=d[a],p;o&&typeof o===t&&typeof o.toJSON===q&&(o=o.toJSON(a)),typeof e===q&&(o=e.call(d,a,o)),p=typeof o;if(p===r)return g(o);if(p===s)return isFinite(o)?String(o):u;if(p===v||p===u)return String(o);if(p===t){if(!o)return u;b+=c,n=[];if(f.apply(o)==="[object Array]"){l=o.length;for(i=0;i<l;i++)n[i]=h(i,o)||u;k=n.length===0?"[]":b?"[\n"+b+n.join(",\n"+b)+"\n"+m+"]":"["+n.join(",")+"]",b=m;return k}if(e&&typeof e===t){l=e.length;for(i=0;i<l;i++)typeof e[i]===r&&(j=e[i],k=h(j,o),k&&n.push(g(j)+(b?": ":":")+k))}else for(j in o)Object.hasOwnProperty.call(o,j)&&(k=h(j,o),k&&n.push(g(j)+(b?": ":":")+k));k=n.length===0?"{}":b?"{\n"+b+n.join(",\n"+b)+"\n"+m+"}":"{"+n.join(",")+"}",b=m;return k}}function g(b){a.lastIndex=0;return a.test(b)?'"'+b.replace(a,function(a){var b=d[a];return typeof b===r?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+b+'"'}var a=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,b,c,d={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},e,f={}.toString;typeof Date.prototype.toJSON!==q&&(Date.prototype.toJSON=function(a){var b=this;return isFinite(b.valueOf())?b.getUTCFullYear()+"-"+K(b.getUTCMonth()+1)+"-"+K(b.getUTCDate())+"T"+K(b.getUTCHours())+":"+K(b.getUTCMinutes())+":"+K(b.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(a){return this.valueOf()}),typeof l.stringify!==q&&(l.stringify=function(a,d,f){var g;b="",c="";if(typeof f===s)for(g=0;g<f;g+=1)c+=" ";else typeof f===r&&(c=f);e=d;if(d&&typeof d!==q&&(typeof d!==t||typeof d.length!==s))throw new Error("JSON.stringify");return h("",{"":a})})}(),function(){function v(a,c,f){f=t(f);var j=f.numberFormat,o=j.groupSize[0],p=j[k],q=j[i],r=j.decimals,s=j.pattern[0],u=[],v,w,x,y,z,A=a<0,B,C,D,E,F=h,G=h,H,I,J,K,L,M,N,O,P,Q=-1,R;if(a===b)return h;if(!isFinite(a))return a;if(!c)return f.name.length?a.toLocaleString():a.toString();z=d.exec(c);if(z){c=z[1].toLowerCase(),w=c==="c",x=c==="p";if(w||x)j=w?j.currency:j.percent,o=j.groupSize[0],p=j[k],q=j[i],r=j.decimals,v=j.symbol,s=j.pattern[A?0:1];y=z[2],y&&(r=+y);if(c==="e")return y?a.toExponential(r):a.toExponential();x&&(a*=100),a=a.toFixed(r),a=a.split(i),B=a[0],C=a[1],A&&(B=B.substring(1)),G=B,D=B.length;if(D>=o){G=h;for(H=0;H<D;H++)H>0&&(D-H)%o===0&&(G+=p),G+=B.charAt(H)}C&&(G+=q+C);if(c==="n"&&!A)return G;a=h;for(H=0,I=s.length;H<I;H++)J=s.charAt(H),J==="n"?a+=G:J==="$"||J==="%"?a+=v:a+=J;return a}A&&(a=-a),c=c.split(";");if(A&&c[1])c=c[1],L=!0;else if(a===0){c=c[2]||c[0];if(c.indexOf(l)==-1&&c.indexOf(m)==-1)return c}else c=c[0];if(c.indexOf("'")>-1||c.indexOf('"')>-1)c=c.replace(e,function(a){u.push(a);return n});w=c.indexOf("$")!=-1,x=c.indexOf("%")!=-1,x&&(a*=100);if(w||x)j=w?j.currency:j.percent,o=j.groupSize[0],p=j[k],q=j[i],r=j.decimals,v=j.symbol;K=c.indexOf(k)>-1,K&&(c=c.replace(g,h)),M=c.indexOf(i),I=c.length,M!=-1?(O=c.lastIndexOf(m),N=c.lastIndexOf(l),C=a.toString().split(i)[1]||h,N>O&&C.length>N-O?H=N:O!=-1&&O>=M&&(H=O),H&&(a=a.toFixed(H-M))):a=a.toFixed(0),N=c.indexOf(l),P=O=c.indexOf(m),N==-1&&O!=-1?Q=O:N!=-1&&O==-1?Q=N:Q=N>O?O:N,N=c.lastIndexOf(l),O=c.lastIndexOf(m),N==-1&&O!=-1?R=O:N!=-1&&O==-1?R=N:R=N>O?N:O,Q==I&&(R=Q);if(Q!=-1){G=a.toString().split(i),B=G[0],C=G[1]||h,D=B.length,E=C.length;if(K)if(D===o&&D<M-P)B=p+B;else if(D>o){G=h;for(H=0;H<D;H++)H>0&&(D-H)%o===0&&(G+=p),G+=B.charAt(H);B=G}a=c.substring(0,Q),A&&!L&&(a+="-");for(H=Q;H<I;H++){J=c.charAt(H);if(M==-1){if(R-H<D){a+=B;break}}else{O!=-1&&O<H&&(F=h),M-H<=D&&M-H>-1&&(a+=B,H=M);if(M===H){a+=(C?q:h)+C,H+=R-M+1;continue}}J===m?(a+=J,F=J):J===l&&(a+=F)}R>=Q&&(a+=c.substring(R+1));if(w||x){G=h;for(H=0,I=a.length;H<I;H++)J=a.charAt(H),G+=J==="$"||J==="%"?v:J;a=G}if(u[0]){I=u.length;for(H=0;H<I;H++)a=a.replace(n,u[H])}}return a}function u(c,d,e){e=t(e);var f=e.calendars.standard,g=f.days,h=f.months;d=f.patterns[d]||d;return d.replace(a,function(a){var d;a==="d"?d=c.getDate():a==="dd"?d=K(c.getDate()):a==="ddd"?d=g.namesAbbr[c.getDay()]:a==="dddd"?d=g.names[c.getDay()]:a==="M"?d=c.getMonth()+1:a==="MM"?d=K(c.getMonth()+1):a==="MMM"?d=h.namesAbbr[c.getMonth()]:a==="MMMM"?d=h.names[c.getMonth()]:a==="yy"?d=K(c.getFullYear()%100):a==="yyyy"?d=c.getFullYear():a==="h"?d=c.getHours()%12||12:a==="hh"?d=K(c.getHours()%12||12):a==="H"?d=c.getHours():a==="HH"?d=K(c.getHours()):a==="m"?d=c.getMinutes():a==="mm"?d=K(c.getMinutes()):a==="s"?d=c.getSeconds():a==="ss"?d=K(c.getSeconds()):a==="f"?d=j.floor(c.getMilliseconds()/100):a==="ff"?d=j.floor(c.getMilliseconds()/10):a==="fff"?d=c.getMilliseconds():a==="tt"&&(d=c.getHours()<12?f.AM[0]:f.PM[0]);return d!==b?d:a.slice(1,a.length-1)})}function t(a){a&&(a=q(a));return a||c.cultures.current}function q(a){if(a){if(a.numberFormat)return a;if(typeof a===r){var b=c.cultures;return b[a]||b[a.split("-")[0]]||null}return null}return null}var a=/dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|HH|H|hh|h|mm|m|fff|ff|f|tt|ss|s|"[^"]*"|'[^']*'/g,d=/^(n|c|p|e)(\d*)$/i,e=/["'].*?["']/g,g=/\,/g,h="",i=".",k=",",l="#",m="0",n="??",p="en-US";c.cultures={"en-US":{name:p,numberFormat:{pattern:["-n"],decimals:2,",":",",".":".",groupSize:[3],percent:{pattern:["-n %","n %"],decimals:2,",":",",".":".",groupSize:[3],symbol:"%"},currency:{pattern:["($n)","$n"],decimals:2,",":",",".":".",groupSize:[3],symbol:"$"}},calendars:{standard:{days:{names:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],namesAbbr:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],namesShort:["Su","Mo","Tu","We","Th","Fr","Sa"]},months:{names:["January","February","March","April","May","June","July","August","September","October","November","December"],namesAbbr:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]},AM:["AM","am","AM"],PM:["PM","pm","PM"],patterns:{d:"M/d/yyyy",D:"dddd, MMMM dd, yyyy",F:"dddd, MMMM dd, yyyy h:mm:ss tt",g:"M/d/yyyy h:mm tt",G:"M/d/yyyy h:mm:ss tt",m:"MMMM dd",M:"MMMM dd",s:"yyyy'-'MM'-'ddTHH':'mm':'ss",t:"h:mm tt",T:"h:mm:ss tt",u:"yyyy'-'MM'-'dd HH':'mm':'ss'Z'",y:"MMMM, yyyy",Y:"MMMM, yyyy"},"/":"/",":":":",firstDay:0}}}},c.culture=function(a){var d=c.cultures,e;if(a!==b)e=q(a)||d[p],e.calendar=e.calendars.standard,d.current=e;else return d.current},c.findCulture=q,c.getCulture=t,c.culture(p);var w=function(a,c,d){if(c){if(a instanceof Date)return u(a,c,d);if(typeof a===s)return v(a,c,d)}return a!==b?a:""};A&&(w=f(A.format,A)),c.format=function(a){var b=arguments;return a.replace(o,function(a,c,d){var e=b[parseInt(c,10)+1];return w(e,d?d.substring(1):"")})},c._extractFormat=function(a){a.slice(0,3)==="{0:"&&(a=a.slice(3,a.length-1));return a},c.toString=w}(),function(){function m(a,b,c){if(!a)return null;var d=function(a){var c=0;while(b[t]===a)c++,t++;c>0&&(t-=1);return c},f=function(b){var c=new RegExp("^\\d{1,"+b+"}"),d=a.substr(u,b).match(c);if(d){d=d[0],u+=d.length;return parseInt(d,10)}return null},g=function(b){var c=0,d=b.length,e,f;for(;c<d;c++){e=b[c],f=e.length;if(a.substr(u,f)==e){u+=f;return c+1}}return null},i=function(){var c=!1;a.charAt(u)===b[t]&&(u++,c=!0);return c},k=c.calendars.standard,m=null,n=null,o=null,p=null,q=null,r=null,s=null,t=0,u=0,v=!1,w=new Date,x=30,y=w.getFullYear(),z,A,B,C,D,E,F,G,H,I,J,K;b||(b="d"),C=k.patterns[b],C&&(b=C),b=b.split(""),B=b.length;for(;t<B;t++){z=b[t];if(v)z==="'"?v=!1:i();else if(z==="d"){A=d("d"),o=A<3?f(2):g(k.days[A==3?"namesAbbr":"names"]);if(o===null||j(o,1,31))return null}else if(z==="M"){A=d("M"),n=A<3?f(2):g(k.months[A==3?"namesAbbr":"names"]);if(n===null||j(n,1,12))return null;n-=1}else if(z==="y")A=d("y"),m=f(A<3?2:4),m===null&&(m=y),m<x&&(m=y-y%100+m);else if(z==="h"){d("h"),p=f(2),p==12&&(p=0);if(p===null||j(p,0,11))return null}else if(z==="H"){d("H"),p=f(2);if(p===null||j(p,0,23))return null}else if(z==="m"){d("m"),q=f(2);if(q===null||j(q,0,59))return null}else if(z==="s"){d("s"),r=f(2);if(r===null||j(r,0,59))return null}else if(z==="f"){A=d("f"),s=f(A);if(s===null||j(s,0,999))return null}else if(z==="t"){A=d("t"),H=k.AM,I=k.PM,A===1&&(H=l(H),I=l(I)),D=g(I);if(!D&&!g(H))return null}else if(z==="z"){E=!0,A=d("z");if(a.substr(u,1)==="Z"){if(!F)return null;i();continue}G=a.substr(u,6).match(A>2?h:e);if(!G)return null;G=G[0],u=G.length,G=G.split(":"),J=parseInt(G[0],10);if(j(J,-12,13))return null;if(A>2){K=parseInt(G[1],10);if(isNaN(K)||j(K,0,59))return null}}else z==="T"?F=i():z==="'"?(v=!0,i()):i()}D&&p<12&&(p+=12),o===null&&(o=1);if(E){J&&(p+=-J),K&&(q+=-K);return new Date(Date.UTC(m,n,o,p,q,r,s))}return new Date(m,n,o,p,q,r,s)}function l(b){return a.map(b,k)}function k(a){return a.charAt(0)}function j(a,b,c){return!(a>=b&&a<=c)}var b=/\u00A0/g,d=/[eE][\-+]?[0-9]+/,e=/[+|\-]\d{1,2}/,h=/[+|\-]\d{1,2}:\d{2}/,i=["G","g","d","F","D","y","m","T","t"];c.parseDate=function(a,b,d){if(a instanceof Date)return a;var e=0,f=null,h,j;d=c.getCulture(d);if(!b){b=[],j=d.calendar.patterns,h=i.length;for(;e<h;e++)b[e]=j[i[e]];b[e]="ddd MMM dd yyyy HH:mm:ss",b[++e]="yyyy-MM-ddTHH:mm:ss.fffzzz",b[++e]="yyyy-MM-ddTHH:mm:sszzz",b[++e]="yyyy-MM-ddTHH:mmzzz",b[++e]="yyyy-MM-ddTHH:mmzz",b[++e]="yyyy-MM-dd",e=0}b=g(b)?b:[b],h=b.length;for(;e<h;e++){f=m(a,b[e],d);if(f)return f}return f},c.parseInt=function(a,b){var d=c.parseFloat(a,b);d&&(d=d|0);return d},c.parseFloat=function(a,e,f){if(!a&&a!==0)return null;if(typeof a===s)return a;a=a.toString(),e=c.getCulture(e);var g=e.numberFormat,h=g.percent,i=g.currency,j=i.symbol,k=h.symbol,l=a.indexOf("-")>-1,m,n;if(d.test(a)){a=parseFloat(a),isNaN(a)&&(a=null);return a}a.indexOf(j)>-1||f&&f.toLowerCase().indexOf("c")>-1?(g=i,m=g.pattern[0].replace("$",j).split("n"),a.indexOf(m[0])>-1&&a.indexOf(m[1])>-1&&(a=a.replace(m[0],"").replace(m[1],""),l=!0)):a.indexOf(k)>-1&&(n=!0,g=h,j=k),a=a.replace("-","").replace(j,"").replace(b," ").split(g[","].replace(b," ")).join("").replace(g["."],"."),a=parseFloat(a),isNaN(a)?a=null:l&&(a*=-1),a&&n&&(a/=100);return a},A&&(c.parseDate=f(A.parseDate,A),c.parseFloat=f(A.parseFloat,A))}(),function(){m.scrollbar=function(){var a=document.createElement("div"),b;a.style.cssText="overflow:scroll;overflow-x:hidden;zoom:1;clear:both",a.innerHTML="&nbsp;",document.body.appendChild(a),b=a.offsetWidth-a.scrollWidth,document.body.removeChild(a);return b};var a=document.createElement("table");try{a.innerHTML="<tr><td></td></tr>",m.tbodyInnerHtml=!0}catch(c){m.tbodyInnerHtml=!1}m.touch="ontouchstart"in window,m.pointers=navigator.msPointerEnabled;var d=m.transitions=!1,f=m.transforms=!1;m.hasHW3D="WebKitCSSMatrix"in window&&"m11"in new window.WebKitCSSMatrix||"MozPerspective"in document.documentElement.style||"msPerspective"in document.documentElement.style,m.hasNativeScrolling=typeof document.documentElement.style.webkitOverflowScrolling=="string",e(["Moz","webkit","O","ms"],function(){var b=this.toString(),c=typeof a.style[b+"Transition"]===r;if(c||typeof a.style[b+"Transform"]===r){var e=b.toLowerCase();f={css:"-"+e+"-",prefix:b,event:e==="o"||e==="webkit"?e:""},c&&(d=f,d.event=d.event?d.event+"TransitionEnd":"transitionend");return!1}}),m.transforms=f,m.transitions=d,m.devicePixelRatio=window.devicePixelRatio===b?1:window.devicePixelRatio,m.detectOS=function(a){var c=!1,d,e=[],f={fire:/(Silk)\/(\d+)\.(\d+(\.\d+)?)/,android:/(Android|Android.*(?:Opera|Firefox).*?\/)\s*(\d+)\.(\d+(\.\d+)?)/,iphone:/(iPhone|iPod).*OS\s+(\d+)[\._]([\d\._]+)/,ipad:/(iPad).*OS\s+(\d+)[\._]([\d_]+)/,meego:/(MeeGo).+NokiaBrowser\/(\d+)\.([\d\._]+)/,webos:/(webOS)\/(\d+)\.(\d+(\.\d+)?)/,blackberry:/(BlackBerry).*?Version\/(\d+)\.(\d+(\.\d+)?)/,playbook:/(PlayBook).*?Tablet\s*OS\s*(\d+)\.(\d+(\.\d+)?)/,winphone:/(IEMobile)\/(\d+)\.(\d+(\.\d+)?)/,windows:/(MSIE)\s+(\d+)\.(\d+(\.\d+)?)/},g={ios:/^i(phone|pad|pod)$/i,android:/^android|fire$/i,blackberry:/^blackberry|playbook/i,windows:/windows|winphone/},h={tablet:/playbook|ipad|fire/i},i={omini:/Opera\sMini/i,omobile:/Opera\sMobi/i,firefox:/Firefox|Fennec/i,mobilesafari:/version\/.*safari/i,webkit:/webkit/i,ie:/MSIE|Windows\sPhone/i},j=function(a,c,d){for(var e in c)if(c.hasOwnProperty(e)&&c[e].test(a))return e;return d!==b?d:a};for(var k in f)if(f.hasOwnProperty(k)){e=a.match(f[k]);if(e){if(k=="windows"&&"plugins"in navigator)return!1;c={},c.device=k,c.tablet=j(k,h,!1),c.browser=j(a,i,"default"),c.name=j(k,g),c[c.name]=!0,c.majorVersion=e[2],c.minorVersion=e[3].replace("_","."),d=c.minorVersion.replace(".","").substr(0,2),c.flatVersion=c.majorVersion+d+Array(3-(d.length<3?d.length:2)).join("0"),c.appMode=window.navigator.standalone||/file|local/.test(window.location.protocol)||typeof window.PhoneGap!==w||typeof window.cordova!==w,c.android&&m.devicePixelRatio<1.5&&(window.outerWidth>800||window.outerHeight>800)&&(c.tablet=k);break}}return c},m.mobileOS=m.detectOS(navigator.userAgent),m.zoomLevel=function(){return m.touch?document.documentElement.clientWidth/window.innerWidth:1},m.eventCapture=document.documentElement.addEventListener,m.placeholder="placeholder"in document.createElement("input"),m.stableSort=function(){var a=[0,1,2,3,4,5,6,7,8,9,10,11,12].sort(function(){return 0});return a[0]===0&&a[1]===1&&a[2]===2&&a[3]===3&&a[4]===4&&a[5]===5&&a[6]===6&&a[7]===7&&a[8]===8&&a[9]===9&&a[10]===10&&a[11]===11&&a[12]===12}()}();var R={left:{reverse:"right"},right:{reverse:"left"},down:{reverse:"up"},up:{reverse:"down"},top:{reverse:"bottom"},bottom:{reverse:"top"},"in":{reverse:"out"},out:{reverse:"in"}},T={promise:function(a,b){b.show&&a.css({display:a.data("olddisplay")||"block"}).css("display"),b.hide&&a.data("olddisplay",a.css("display")).hide(),b.init&&b.init(),b.completeCallback&&b.completeCallback(a),a.dequeue()},transitionPromise:function(a,b,d){var e=c.wrap(a);e.append(b),a.hide(),b.show(),d.completeCallback&&d.completeCallback(a);return a}};d(a.fn,{kendoStop:function(a,b){return this.stop(a,b)},kendoAnimate:function(a,b,c,d){return V(this,a,b,c,d)},kendoAnimateTo:function(a,b,c,d,e){return W(this,a,b,c,d,e)}}),d(a.fn,{kendoAddClass:function(a,b){return X(this,a,b,!0)},kendoRemoveClass:function(a,b){return X(this,a,b,!1)},kendoToggleClass:function(a,b,c){return X(this,a,b,c)}});var Y=/&/g,Z=/</g,$=/>/g,ba=function(a){var b=typeof a.pageX==w?a.originalEvent:a;return{idx:m.pointers?b.pointerId:0,x:b.pageX,y:b.pageY}},bb=function(a){return a.target};m.touch&&(ba=function(a,b){var c=a.changedTouches||a.originalEvent.changedTouches;if(b){var d=null;e(c,function(a,c){b==c.identifier&&(d={idx:c.identifier,x:c.pageX,y:c.pageY})});return d}return{idx:c[0].identifier,x:c[0].pageX,y:c[0].pageY}},bb=function(a){var b="originalEvent"in a?a.originalEvent.changedTouches:"changedTouches"in a?a.changedTouches:null;return b?document.elementFromPoint(b[0].clientX,b[0].clientY):null},e(["swipe","swipeLeft","swipeRight","swipeUp","swipeDown","doubleTap","tap"],function(b,c){a.fn[c]=function(a){return this.bind(c,a)}})),m.touch?(m.mousedown="touchstart",m.mouseup="touchend",m.mousemove="touchmove",m.mousecancel="touchcancel",m.resize="orientationchange"):m.pointers?(m.mousemove="MSPointerMove",m.mousedown="MSPointerDown",m.mouseup="MSPointerUp",m.mousecancel="MSPointerCancel",m.resize="orientationchange resize"):(m.mousemove="mousemove",m.mousedown="mousedown",m.mouseup="mouseup",m.mousecancel="mouseleave",m.resize="resize");var bc=function(a){var b="d",c,d,e,f,g=1;for(d=0,e=a.length;d<e;d++)f=a[d],f!==""&&(c=f.indexOf("["),c!==0&&(c==-1?f="."+f:(g++,f="."+f.substring(0,c)+" || {})"+f.substring(c))),g++,b+=f+(d<e-1?" || {})":")"));return Array(g).join("(")+b},bd=/^([a-z]+:)?\/\//i;d(c,{ui:c.ui||{},fx:c.fx||T,data:c.data||{},mobile:c.mobile||{},dataviz:c.dataviz||{ui:{}},keys:{INSERT:45,DELETE:46,BACKSPACE:8,TAB:9,ENTER:13,ESC:27,LEFT:37,UP:38,RIGHT:39,DOWN:40,END:35,HOME:36,SPACEBAR:32,PAGEUP:33,PAGEDOWN:34,F2:113,F10:121,F12:123},support:m,animate:V,ns:"",attr:function(a){return"data-"+c.ns+a},wrap:L,deepExtend:M,size:O,isNodeEmpty:P,getOffset:Q,parseEffects:S,toggleClass:X,directions:R,Observable:C,Class:B,Template:k,template:f(k.compile,k),render:f(k.render,k),stringify:f(l.stringify,l),touchLocation:ba,eventTarget:bb,htmlEncode:_,isLocalUrl:function(a){return a&&!bd.test(a)},expr:function(a,b){a=a||"",a&&a.charAt(0)!=="["&&(a="."+a),b?a=bc(a.split(".")):a="d"+a;return a},getter:function(a,b){return x[a]=x[a]||new Function("d","return "+c.expr(a,b))},setter:function(a){return y[a]=y[a]||new Function("d,value","d."+a+"=value")},accessor:function(a){return{get:c.getter(a),set:c.setter(a)}},guid:function(){var a="",b,c;for(b=0;b<32;b++){c=j.random()*16|0;if(b==8||b==12||b==16||b==20)a+="-";a+=(b==12?4:b==16?c&3|8:c).toString(16)}return a},roleSelector:function(a){return a.replace(/(\S+)/g,"["+c.attr("role")+"=$1],").slice(0,-1)},logToConsole:function(a){var b=window.console;typeof b!="undefined"&&b.log&&b.log(a)}});var be=C.extend({init:function(b,e){var f=this;f.element=a(b),C.fn.init.call(f),f.options=d(!0,{},f.options,e),f.element.attr(c.attr("role"))||f.element.attr(c.attr("role"),(f.options.name||"").toLowerCase()),f.element.data("kendo"+f.options.prefix+f.options.name,f),f.bind(f.events,f.options)},events:[],options:{prefix:""},setOptions:function(b){a.extend(this.options,b),this.bind(this.events,b)}});c.notify=h;var bf=/template$/i,bg=/^\s*(?:\{(?:.|\n)*\}|\[(?:.|\n)*\])\s*$/,bh=/^\{(\d+)(:[^\}]+)?\}/,bi=/([A-Z])/g;c.initWidget=function(d,e,f){var g,h,i,j,k,l,m,n;f?f.roles&&(f=f.roles):f=c.ui.roles,d=d.nodeType?d:d[0],l=d.getAttribute("data-"+c.ns+"role");if(!!l){i=f[l];if(!i)return;n=bj(d,"dataSource"),e=a.extend({},bk(d,i.fn.options),e),n&&(typeof n===r?e.dataSource=c.getter(n)(window):e.dataSource=n);for(j=0,k=i.fn.events.length;j<k;j++)h=i.fn.events[j],m=bj(d,h),m!==b&&(e[h]=c.getter(m)(window));g=a(d).data("kendo"+i.fn.options.name),g?g.setOptions(e):g=new i(d,e);return g}},c.init=function(b){var e=z.call(arguments,1),f;e[0]||(e=[c.ui,c.dataviz.ui]),f=a.map(e,function(a){return a.roles}).reverse(),f=d.apply(null,[{}].concat(f)),a(b).find("[data-"+c.ns+"role]").andSelf().each(function(){c.initWidget(this,{},f)})},c.parseOptions=bk,d(c.ui,{Widget:be,roles:{},progress:function(b,c){var d=b.find(".k-loading-mask");c?d.length||(d=a("<div class='k-loading-mask'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'/><div class='k-loading-color'/></div>").width("100%").height("100%").prependTo(b).css({top:b.scrollTop(),left:b.scrollLeft()})):d&&d.remove()},plugin:function(d,e,f){var g=d.fn.options.name,h;e=e||c.ui,f=f||"",e[g]=d,e.roles[g.toLowerCase()]=d,h="getKendo"+f+g,g="kendo"+f+g,a.fn[g]=function(e){var f=this,h;typeof e===r?(h=z.call(arguments,1),this.each(function(){var d=a.data(this,g),i,j;if(!d)throw new Error(c.format("Cannot call method '{0}' of {1} before it is initialized",e,g));i=d[e];if(typeof i!==q)throw new Error(c.format("Cannot find method '{0}' of {1}",e,g));j=i.apply(d,h);if(j!==b){f=j;return!1}})):this.each(function(){new d(this,e)});return f},a.fn[h]=function(){return this.data(g)}}});var bl=be.extend({init:function(a,b){be.fn.init.call(this,a,b),this.wrapper=this.element},options:{prefix:"Mobile"},events:[],viewShow:a.noop,view:function(){var a=this.element.closest(c.roleSelector("view")+","+c.roleSelector("splitview"));return a.data("kendoMobileView")||a.data("kendoMobileSplitView")}});d(c.mobile,{init:function(a){c.init(a,c.mobile.ui,c.ui,c.dataviz.ui)},ui:{Widget:bl,roles:{},plugin:function(a){c.ui.plugin(a,c.mobile.ui,"Mobile")}}}),c.touchScroller=function(a,b){if(m.touch&&c.mobile.ui.Scroller&&!a.data("kendoMobileScroller")){a.kendoMobileScroller(b);return a.data("kendoMobileScroller")}return!1},c.preventDefault=function(a){a.preventDefault()},c.widgetInstance=function(a,b){var d=b.roles[a.data(c.ns+"role")];if(d)return a.data("kendo"+d.fn.options.prefix+d.fn.options.name)},c.onResize=function(b){var c=b;m.mobileOS.android&&(c=function(){setTimeout(b,200)}),a(window).on(m.resize,c)},c.data=function(a,b){return a.data(c.ns+b)}})(jQuery),function(a,b){function ba(a,b,d){var e=c.directions[b],f=d?c.directions[e.reverse]:e;return{direction:f,offset:-f.modifier*(f.vertical?a.outerHeight():a.outerWidth())}}function _(a,b){if(k){var c=a.css(M);if(c==C)return b=="scale"?1:0;var d=c.match(new RegExp(b+"\\s*\\(([\\d\\w\\.]+)")),e=0;d?e=Q(d[1]):(d=c.match(p)||[0,0,0,0,0],b=b.toLowerCase(),r.test(b)?e=parseFloat(d[3]/d[2]):b=="translatey"?e=parseFloat(d[4]/d[2]):b=="scale"?e=parseFloat(d[2]):b=="rotate"&&(e=parseFloat(Math.atan2(d[2],d[1]))));return e}return parseFloat(a.css(b))}function $(c,d,e){var f=[],g={},h,i,l,m,n;for(i in d)h=i.toLowerCase(),n=k&&v.indexOf(h)!=-1,!j.hasHW3D&&n&&w.indexOf(h)==-1?delete d[i]:(m=!1,a.isFunction(d[i])?(l=d[i](c,e),l!==b&&(m=l)):m=d[i],m!==!1&&(n?f.push(i+"("+m+")"):g[i]=m));f.length&&(g[M]=f.join(" "));return g}function Z(c,d,e){var f,g;for(f in d)a.isFunction(d[f])&&(g=d[f](c,e),g!==b?d[f]=g:delete d[f]);return d}function Y(a){for(var b in a)v.indexOf(b)!=-1&&w.indexOf(b)==-1&&delete a[b];return a}function X(a){var b=a.object,c=0;!a||(b.css(a.setup).css(L),b.css(a.CSS).css(M),i.mozilla&&(b.one(l.event,function(){W(b,a)}),c=50),b.data(I,setTimeout(W,a.duration+c,b,a)))}function W(a,b){a.data(I)&&(clearTimeout(a.data(I)),a.removeData(I)),a.css(L,"").css(L),a.dequeue(),b.complete.call(a)}function V(a){var b=[];for(var c in a)b.push(c);return b}function U(b){var d=b.effects,e;d==="zoom"&&(d="zoomIn fadeIn"),d==="slide"&&(d="slide:left"),d==="fade"&&(d="fadeIn"),d==="overlay"&&(d="slideIn:left"),/^overlay:(.+)$/.test(d)&&(d="slideIn:"+RegExp.$1),e=b.reverse&&/^(slide:)/.test(d),e&&delete b.reverse,b.effects=a.extend(c.parseEffects(d,e),{show:!0});return b}function T(a){a.effects.slideIn=a.effects.slide,delete a.effects.slide,delete a.complete;return a}function S(a,b){var c={};if(b){if(document.defaultView&&document.defaultView.getComputedStyle){var d=document.defaultView.getComputedStyle(a,"");e(b,function(a,b){c[b]=d.getPropertyValue(b)})}else if(a.currentStyle){var f=a.currentStyle;e(b,function(a,b){c[b]=f[b.replace(/\-(\w)/g,function(a,b){return b.toUpperCase()})]})}}else c=document.defaultView.getComputedStyle(a,"");return c}function R(a,b){return Q(a.css(b))}function Q(a){return parseInt(a,10)}var c=window.kendo,d=c.fx,e=a.each,f=a.extend,g=a.proxy,h=c.size,i=a.browser,j=c.support,k=j.transforms,l=j.transitions,m={scale:0,scalex:0,scaley:0,scale3d:0},n={translate:0,translatex:0,translatey:0,translate3d:0},o=typeof document.documentElement.style.zoom!="undefined"&&!k,p=/matrix3?d?\s*\(.*,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?/i,q=/^(-?[\d\.\-]+)?[\w\s]*,?\s*(-?[\d\.\-]+)?[\w\s]*/i,r=/translatex?$/i,s=/(zoom|fade|expand)(\w+)/,t=/(zoom|fade|expand)/,u=/[xy]$/i,v=["perspective","rotate","rotatex","rotatey","rotatez","rotate3d","scale","scalex","scaley","scalez","scale3d","skew","skewx","skewy","translate","translatex","translatey","translatez","translate3d","matrix","matrix3d"],w=["rotate","scale","scalex","scaley","skew","skewx","skewy","translate","translatex","translatey","matrix"],x={rotate:"deg",scale:"",skew:"px",translate:"px"},y=k.css,z=Math.round,A="",B="px",C="none",D="auto",E="width",F="height",G="hidden",H="origin",I="abortId",J="overflow",K="translate",L=y+"transition",M=y+"transform",N=y+"perspective",O=y+"backface-visibility";c.directions={left:{reverse:"right",property:"left",transition:"translatex",vertical:!1,modifier:-1},right:{reverse:"left",property:"left",transition:"translatex",vertical:!1,modifier:1},down:{reverse:"up",property:"top",transition:"translatey",vertical:!0,modifier:1},up:{reverse:"down",property:"top",transition:"translatey",vertical:!0,modifier:-1},top:{reverse:"bottom"},bottom:{reverse:"top"},"in":{reverse:"out",modifier:-1},out:{reverse:"in",modifier:1}},f(a.fn,{kendoStop:function(a,b){return l?c.fx.stopQueue(this,a||!1,b||!1):this.stop(a,b)}});if(k&&!l){e(w,function(b,c){a.fn[c]=function(b){if(typeof b=="undefined")return _(this,c);var d=a(this)[0],e=c+"("+b+x[c.replace(u,"")]+")";d.style.cssText.indexOf(M)==-1?a(this).css(M,e):d.style.cssText=d.style.cssText.replace(new RegExp(c+"\\(.*?\\)","i"),e);return this},a.fx.step[c]=function(b){a(b.elem)[c](b.now)}});var P=a.fx.prototype.cur;a.fx.prototype.cur=function(){if(w.indexOf(this.prop)!=-1)return parseFloat(a(this.elem)[this.prop]());return P.apply(this,arguments)}}c.toggleClass=function(a,b,c,d){b&&(b=b.split(" "),l&&(c=f({exclusive:"all",duration:400,ease:"ease-out"},c),a.css(L,c.exclusive+" "+c.duration+"ms "+c.ease),setTimeout(function(){a.css(L,"").css(F)},c.duration)),e(b,function(b,c){a.toggleClass(c,d)}));return a},c.parseEffects=function(a,b){var d={};typeof a=="string"?e(a.split(" "),function(a,e){var f=!t.test(e),g=e.replace(s,function(a,b,c){return b+":"+c.toLowerCase()}),h=g.split(":"),i=h[1],j={};h.length>1&&(j.direction=b&&f?c.directions[i].reverse:i),d[h[0]]=j}):e(a,function(a){var e=this.direction;e&&b&&!t.test(a)&&(this.direction=c.directions[e].reverse),d[a]=this});return d},l&&f(c.fx,{transition:function(b,c,d){d=f({duration:200,ease:"ease-out",complete:null,exclusive:"all"},d),d.duration=a.fx?a.fx.speeds[d.duration]||d.duration:d.duration;var e=$(b,c,d),g={keys:V(e),CSS:e,object:b,setup:{},duration:d.duration,complete:d.complete};g.setup[L]=d.exclusive+" "+d.duration+"ms "+d.ease;var h=b.data("keys")||[];a.merge(h,g.keys),b.data("keys",a.unique(h)),X(g)},stopQueue:function(a,b,c){a.data(I)&&(clearTimeout(a.data(I)),a.removeData(I));var d=this,e,f=a.data("keys"),g=c===!1&&f;g&&(e=S(a[0],f)),a.css(L,"").css(L),g&&a.css(e),a.removeData("keys"),d.complete&&d.complete.call(a),a.stop(b);return a}}),c.fx.promise=function(b,d){var g=[],i;i=c.parseEffects(d.effects),d.effects=i,b.data("animating",!0);var j={keep:[],restore:[]},m={},n,p={setup:[],teardown:[]},q={},r=a.Deferred(function(g){if(h(i)){var o=f({},d,{complete:g.resolve});e(i,function(b,g){var h=c.fx[b];if(h){var i=c.directions[g.direction];g.direction&&i&&(g.direction=d.reverse?i.reverse:g.direction),o=f(!0,o,g),e(p,function(a){h[a]&&p[a].push(h[a])}),e(j,function(b){h[b]&&a.merge(j[b],h[b])}),h.css&&(m=f(m,h.css))}});if(p.setup.length){e(a.unique(j.keep),function(a,c){b.data(c)||b.data(c,b.css(c))}),d.show&&(m=f(m,{display:b.data("olddisplay")||"block"})),k&&!d.reset&&(m=Z(b,m,o),n=b.data("targetTransform"),n&&(m=f(n,m))),m=$(b,m,o),k&&!l&&(m=Y(m)),b.css(m).css(M),e(p.setup,function(){q=f(q,this(b,o))}),c.fx.animate&&(d.init(),b.data("targetTransform",q),c.fx.animate(b,q,o));return}}else d.show&&(b.css({display:b.data("olddisplay")||"block"}).css("display"),d.init());g.resolve()}).promise();g.push(r),a.when.apply(null,g).then(function(){b.removeData("animating").dequeue(),d.hide&&b.data("olddisplay",b.css("display")).hide();if(h(i)){var c=function(){e(a.unique(j.restore),function(a,c){b.css(c,b.data(c))})};c(),o&&!k&&setTimeout(c,0),e(p.teardown,function(){this(b,d)})}d.completeCallback&&d.completeCallback(b)})},c.fx.transitionPromise=function(a,b,d){c.fx.animateTo(a,b,d);return a},f(c.fx,{animate:function(c,g,h){var j=h.transition!==!1;delete h.transition,l&&"transition"in d&&j?d.transition(c,g,h):k?c.animate(Y(g),{queue:!1,show:!1,hide:!1,duration:h.duration,complete:h.complete}):c.each(function(){var c=a(this),d={};e(v,function(a,e){var h,i=g?g[e]+" ":null;if(i){var j=g;if(e in m&&g[e]!==b){h=i.match(q);if(o){var l=(1-h[1])/2;f(j,{zoom:+h[1],marginLeft:c.width()*l,marginTop:c.height()*l})}else k&&f(j,{scale:+h[0]})}else if(e in n&&g[e]!==b){var p=c.css("position"),r=p=="absolute"||p=="fixed";c.data(K)||(r?c.data(K,{top:R(c,"top")||0,left:R(c,"left")||0,bottom:R(c,"bottom"),right:R(c,"right")}):c.data(K,{top:R(c,"marginTop")||0,left:R(c,"marginLeft")||0}));var s=c.data(K);h=i.match(q);if(h){var t=e==K+"y"?+null:+h[1],u=e==K+"y"?+h[1]:+h[2];r?(isNaN(s.right)?isNaN(t)||f(j,{left:s.left+t}):isNaN(t)||f(j,{right:s.right-t}),isNaN(s.bottom)?isNaN(u)||f(j,{top:s.top+u}):isNaN(u)||f(j,{bottom:s.bottom-u})):(isNaN(t)||f(j,{marginLeft:s.left+t}),isNaN(u)||f(j,{marginTop:s.top+u}))}}!k&&e!="scale"&&e in j&&delete j[e],j&&f(d,j)}}),i.msie&&delete d.scale,c.animate(d,{queue:!1,show:!1,hide:!1,duration:h.duration,complete:h.complete})})},animateTo:function(b,c,d){function h(a){c[0].style.cssText="",b[0].style.cssText="",j.mobileOS.android||f.css(J,g),d.completeCallback&&d.completeCallback.call(b,a)}var e,f=b.parents().filter(c.parents()).first(),g;d=U(d),j.mobileOS.android||(g=f.css(J),f.css(J,"hidden")),a.each(d.effects,function(a,b){e=e||b.direction}),d.complete=i.msie?function(){setTimeout(h,0)}:h,d.reset=!0,"slide"in d.effects?(b.kendoAnimate(d),c.kendoAnimate(T(d))):(d.reverse?b:c).kendoAnimate(d)},fade:{keep:["opacity"],css:{opacity:function(a,b){var c=a[0].style.opacity;return b.effects.fade.direction=="in"&&(!c||c==1)?0:1}},restore:["opacity"],setup:function(a,b){return f({opacity:b.effects.fade.direction=="out"?0:1},b.properties)}},zoom:{css:{scale:function(a,b){var c=_(a,"scale");return b.effects.zoom.direction=="in"?c!=1?c:"0.01":1},zoom:function(a,c){var d=a[0].style.zoom;return c.effects.zoom.direction=="in"&&o?d?d:"0.01":b}},setup:function(a,b){var c=b.effects.zoom.direction=="out";if(o){var d=i.version,e=a[0].currentStyle,g=e.width.indexOf("%")!=-1?a.parent().width():a.width(),h=e.height.indexOf("%")!=-1?a.parent().height():Q(e.height),j=d<9&&b.effects.fade?0:(1-Q(a.css("zoom"))/100)/2;a.css({marginLeft:g*(d<8?0:j),marginTop:h*j})}return f({scale:c?.01:1},b.properties)}},slide:{setup:function(a,b){var c=b.reverse,d={},e=ba(a,b.effects.slide.direction,c),g=k&&b.transition!==!1?e.direction.transition:e.direction.property;e.offset/=-(b.divisor||1);if(!c){var h=a.data(H);!h&&h!==0&&a.data(H,_(a,g))}d[g]=c?a.data(H)||0:(a.data(H)||0)+e.offset+B;return f(d,b.properties)}},slideMargin:{setup:function(a,b){var c=a.data(H),d=b.offset,e,g={},h=b.reverse;!h&&!c&&c!==0&&a.data(H,parseFloat(a.css("margin-"+b.axis))),e=a.data(H)||0,g["margin-"+b.axis]=h?e:e+d;return f(g,b.properties)}},slideTo:{setup:function(a,b){var c=(b.offset+"").split(","),d={},e=b.reverse;k&&b.transition!==!1?(d.translatex=e?0:c[0],d.translatey=e?0:c[1]):(d.left=e?0:c[0],d.top=e?0:c[1]),a.css("left");return f(d,b.properties)}},slideIn:{css:{translatex:function(a,c){var d=ba(a,c.effects.slideIn.direction,c.reverse);return d.direction.transition=="translatex"?(c.reverse?0:d.offset)+B:b},translatey:function(a,c){var d=ba(a,c.effects.slideIn.direction,c.reverse);return d.direction.transition=="translatey"?(c.reverse?0:d.offset)+B:b}},setup:function(a,b){var c=b.reverse,d=ba(a,b.effects.slideIn.direction,c),e={};k&&b.transition!==!1?e[d.direction.transition]=(c?d.offset:0)+B:(c||a.css(d.direction.property,d.offset+B),e[d.direction.property]=(c?d.offset:0)+B,a.css(d.direction.property));return f(e,b.properties)}},expand:{keep:[J],css:{overflow:G},restore:[J],setup:function(a,c){var d=c.reverse,e=c.effects.expand.direction,g=(e?e=="vertical":!0)?F:E,h=a[0].style[g],i=a.data(g),j=parseFloat(i||h)||z(a.css(g,D)[g]()),k={};k[g]=(d?0:j)+B,a.css(g,d?j:0).css(g),i===b&&a.data(g,h);return f(k,c.properties)},teardown:function(a,b){var c=b.effects.expand.direction,d=(c?c=="vertical":!0)?F:E,e=a.data(d);(e==D||e===A)&&setTimeout(function(){a.css(d,D).css(d)},0)}},flip:{css:{rotatex:function(a,c){return c.effects.flip.direction=="vertical"?c.reverse?"180deg":"0deg":b},rotatey:function(a,c){return c.effects.flip.direction=="horizontal"?c.reverse?"180deg":"0deg":b}},setup:function(a,b){var d=b.effects.flip.direction=="horizontal"?"rotatey":"rotatex",e=b.reverse,g=a.parent(),h=b.degree,i=b.face,k=b.back,l=d+(e?"(180deg)":"(0deg)"),m=d+(e?"(0deg)":"(180deg)"),n={};j.hasHW3D?(g.css(N)==C&&g.css(N,500),a.css(y+"transform-style","preserve-3d"),i.css(O,G).css(M,l).css("z-index",e?0:-1),k.css(O,G).css(M,m).css("z-index",e?-1:0),n[d]=(e?"-":"")+(h?h:180)+"deg"):c.size(b.effects)==1&&(b.duration=0),i.show(),k.show();return f(n,b.properties)},teardown:function(b,c){c[c.reverse?"back":"face"].hide(),j.hasHW3D&&a().add(c.face).add(c.back).add(b).css(O,"")}},simple:{setup:function(a,b){return b.properties}}}),c.fx.expandVertical=c.fx.expand;var bb=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(a){setTimeout(a,1e3/60)},bc=c.Class.extend({init:function(){var a=this;a._tickProxy=g(a._tick,a),a._started=!1},tick:a.noop,done:a.noop,onEnd:a.noop,onCancel:a.noop,start:function(){this._started=!0,bb(this._tickProxy)},cancel:function(){this._started=!1,this.onCancel()},_tick:function(){var a=this;!a._started||(a.tick(),a.done()?(a._started=!1,a.onEnd()):bb(a._tickProxy))}}),bd=bc.extend({init:function(a){var b=this;f(b,a),bc.fn.init.call(b)},done:function(){return this.timePassed()>=this.duration},timePassed:function(){return Math.min(this.duration,+(new Date)-this.startDate)},moveTo:function(a){var b=this,c=b.movable;b.initial=c[b.axis],b.delta=a.location-b.initial,b.duration=a.duration||300,b.tick=b._easeProxy(a.ease),b.startDate=+(new Date),b.start()},_easeProxy:function(a){var b=this;return function(){b.movable.moveAxis(b.axis,a(b.timePassed(),b.initial,b.delta,b.duration))}}});f(bd,{easeOutExpo:function(a,b,c,d){return a==d?b+c:c*(-Math.pow(2,-10*a/d)+1)+b},easeOutBack:function(a,b,c,d,e){e=1.70158;return c*((a=a/d-1)*a*((e+1)*a+e)+1)+b}}),d.Animation=bc,d.Transition=bd}(jQuery),function(a,b){function h(d){var f=[],g=d.logic||"and",i,j,k,l,m,n,o,p,q=d.filters;for(i=0,j=q.length;i<j;i++)d=q[i],k=d.field,o=d.value,n=d.operator,d.filters?d=h(d):(p=d.ignoreCase,k=k.replace(/\./g,"/"),d=e[n],d&&o!==b&&(l=a.type(o),l==="string"?(m="'{1}'",o=o.replace(/'/g,"''"),p===!0&&(k="tolower("+k+")")):l==="date"?m="datetime'{1:yyyy-MM-ddTHH:mm:ss}'":m="{1}",d.length>3?d!=="substringof"?m="{0}({2},"+m+")":m="{0}("+m+",{2})":m="{2} {0} "+m,d=c.format(m,d,o,k))),f.push(d);d=f.join(" "+g+" "),f.length>1&&(d="("+d+")");return d}var c=window.kendo,d=a.extend,e={eq:"eq",neq:"ne",gt:"gt",gte:"ge",lt:"lt",lte:"le",contains:"substringof",endswith:"endswith",startswith:"startswith"},f={pageSize:a.noop,page:a.noop,filter:function(a,b){b&&(a.$filter=h(b))},sort:function(b,c){var d=a.map(c,function(a){var b=a.field.replace(/\./g,"/");a.dir==="desc"&&(b+=" desc");return b}).join(",");d&&(b.$orderby=d)},skip:function(a,b){b&&(a.$skip=b)},take:function(a,b){b&&(a.$top=b)}},g={read:{dataType:"jsonp"}};d(!0,c.data,{schemas:{odata:{type:"json",data:function(a){return a.d.results||[a.d]},total:"d.__count"}},transports:{odata:{read:{cache:!0,dataType:"jsonp",jsonp:"$callback"},update:{cache:!0,dataType:"json",contentType:"application/json",type:"PUT"},create:{cache:!0,dataType:"json",contentType:"application/json",type:"POST"},destroy:{cache:!0,dataType:"json",type:"DELETE"},parameterMap:function(a,b){var d,e,h,i;a=a||{},b=b||"read",i=(this.options||g)[b],i=i?i.dataType:"json";if(b==="read"){d={$inlinecount:"allpages"},i!="json"&&(d.$format="json");for(h in a)f[h]?f[h](d,a[h]):d[h]=a[h]}else{if(i!=="json")throw new Error("Only json dataType can be used for "+b+" operation.");if(b!=="destroy"){for(h in a)e=a[h],typeof e=="number"&&(a[h]=e+"");d=c.stringify(a)}}return d}}}})}(jQuery),function(a,b){var c=window.kendo,d=a.isArray,e=a.isPlainObject,f=a.map,g=a.each,h=a.extend,i=c.getter,j=c.Class,k=j.extend({init:function(a){var b=this,i=a.total,j=a.model,k=a.data;if(j){if(e(j)){j.fields&&g(j.fields,function(a,c){e(c)&&c.field?c=h(c,{field:b.getter(c.field)}):c={field:b.getter(c)},j.fields[a]=c});var l=j.id;if(l){var m={};m[b.xpathToMember(l,!0)]={field:b.getter(l)},j.fields=h(m,j.fields),j.id=b.xpathToMember(l)}j=c.data.Model.define(j)}b.model=j}i&&(i=b.getter(i),b.total=function(a){return parseInt(i(a),10)}),k&&(k=b.xpathToMember(k),b.data=function(a){var c=b.evaluate(a,k),e;c=d(c)?c:[c];if(b.model&&j.fields){e=new b.model;return f(c,function(a){if(a){var b={},c;for(c in j.fields)b[c]=e._parse(c,j.fields[c].field(a));return b}})}return c})},total:function(a){return this.data(a).length},errors:function(a){return a?a.errors:null},parseDOM:function(a){var c={},e,f,g,h,i,j,k=a.attributes,l=k.length,m;for(m=0;m<l;m++)j=k[m],c["@"+j.nodeName]=j.nodeValue;for(f=a.firstChild;f;f=f.nextSibling)g=f.nodeType,g===3||g===4?c["#text"]=f.nodeValue:g===1&&(e=this.parseDOM(f),h=f.nodeName,i=c[h],d(i)?i.push(e):i!==b?i=[i,e]:i=e,c[h]=i);return c},evaluate:function(a,b){var c=b.split("."),e,f,g,h,i;while(e=c.shift()){a=a[e];if(d(a)){f=[],b=c.join(".");for(i=0,g=a.length;i<g;i++)h=this.evaluate(a[i],b),h=d(h)?h:[h],f.push.apply(f,h);return f}}return a},parse:function(b){var c,d,e={};c=b.documentElement||a.parseXML(b).documentElement,d=this.parseDOM(c),e[c.nodeName]=d;return e},xpathToMember:function(a,b){if(!a)return"";a=a.replace(/^\//,"").replace(/\//g,".");if(a.indexOf("@")>=0)return a.replace(/\.?(@.*)/,b?"$1":'["$1"]');if(a.indexOf("text()")>=0)return a.replace(/(\.?text\(\))/,b?"#text":'["#text"]');return a},getter:function(a){return i(this.xpathToMember(a),!0)}});a.extend(!0,c.data,{XmlDataReader:k,readers:{xml:k}})}(jQuery),function(a,b){function bE(b,c){var d=a(b).children(),e,f,g=[],h,i=c[0].field,j=c[1]&&c[1].field,k=c[2]&&c[2].field,l=c[3]&&c[3].field,m,n,o,p,q;for(e=0,f=d.length;e<f;e++)h={},m=d.eq(e),o=m[0].firstChild,q=m.children(),b=q.filter("ul"),q=q.filter(":not(ul)"),n=m.attr("data-id"),n&&(h.id=n),o&&(h[i]=o.nodeType==3?o.nodeValue:q.text()),j&&(h[j]=q.find("a").attr("href")),l&&(h[l]=q.find("img").attr("src")),k&&(p=q.find(".k-sprite").prop("className"),h[k]=p&&a.trim(p.replace("k-sprite",""))),b.length&&(h.items=bE(b.eq(0),c)),m.attr("data-hasChildren")=="true"&&(h.hasChildren=!0),g.push(h);return g}function bB(b,c){var d=a(b)[0].tBodies[0],e=d?d.rows:[],f,g,h,i=c.length,j=[],k,l,m,n;for(f=0,g=e.length;f<g;f++){l={},n=!0,k=e[f].cells;for(h=0;h<i;h++)m=k[h],m.nodeName.toLowerCase()!=="th"&&(n=!1,l[c[h].field]=m.innerHTML);n||j.push(l)}return j}function bA(b,c){var d=a(b)[0].children,e,f,g=[],h,i=c[0],j=c[1],k,l;for(e=0,f=d.length;e<f;e++)h={},l=d[e],h[i.field]=l.text,k=l.attributes.value,k&&k.specified?k=l.value:k=l.text,h[j.field]=k,g.push(h);return g}function by(a,b){var c,d;for(c=0,d=a.length;c<d;c++)if(b(a[c]))return c;return-1}function bx(a,b){if(b)return by(a,function(a){return a.uid==b.uid});return-1}function bw(a,b){if(b)return by(a,function(a){return a[b.idField]===b.id});return-1}function bv(a,b){var c,d,e,f;for(e=a.length-1,f=0;e>=f;e--)d=a[e],c={value:b.get(d.field),field:d.field,items:c?[c]:[b],hasSubgroups:!!c,aggregates:{}};return c}function bu(a,b){var c,d;for(c=0,d=a.length;c<d;c++)if(a[c].uid==b.uid){b=a[c],a.splice(c,1);return b}}function bt(a,b){var c,d;for(c=0,d=a.length;c<d;c++)if(a[c].hasSubgroups){if(bt(a[c].items,b))return!0}else if(b(a[c].items,a[c]))return!0}function bs(a,b){var c,d,e,f;if(b)for(c=0,d=a.length;c<d;c++)e=a[c],f=e.items,e.hasSubgroups?bs(f,b):f.length&&!(f[0]instanceof b)&&(f.type=b,f.wrapAll(f,f))}function br(a){var b,c,d=[];for(b=0,c=a.length;b<c;b++)a[b].hasSubgroups?d=d.concat(br(a[b].items)):d=d.concat(a[b].items.slice());return d}function bp(a,b,c,d){return function(e){e=a(e),e&&!g(d)&&(M.call(e)!=="[object Array]"&&!(e instanceof Q)&&(e=[e]),c(e,d,new b));return e||[]}}function bo(a,b,c){var d,e,f;for(e=0,f=a.length;e<f;e++)d=a[e],d.value=c._parse(d.field,d.value),d.hasSubgroups?bo(d.items,b,c):bn(d.items,b,c)}function bn(a,b,c){var d,e,f,g;for(f=0,g=a.length;f<g;f++){d=a[f];for(e in b)d[e]=c._parse(e,b[e](d))}}function bj(a,b){b=b||{};var c=new Z(a),d=b.aggregate,e=b.filter;e&&(c=c.filter(e));return c.aggregate(d)}function bi(a,c){c=c||{};var d=new Z(a),e=c.group,f=bd(e||[]).concat($(c.sort||[])),g,h=c.filter,i=c.skip,j=c.take;h&&(d=d.filter(h),g=d.toArray().length),f&&(d=d.sort(f),e&&(a=d.toArray())),i!==b&&j!==b&&(d=d.range(i,j)),e&&(d=d.group(e,a));return{total:g,data:d.toArray()}}function bh(a){var b,c=a.length,d=Array(c);for(b=0;b<c;b++)d[b]=a[b].toJSON();return d}function bf(a,b,c,d,e){b=b||[];var f,g,h,i=b.length;for(f=0;f<i;f++){g=b[f],h=g.aggregate;var j=g.field;a[j]=a[j]||{},a[j][h]=bg[h.toLowerCase()](a[j][h],c,n.accessor(j),d,e)}}function be(a,b){if(a&&a.getTime&&b&&b.getTime)return a.getTime()===b.getTime();return a===b}function bd(a,c){var d=typeof a===q?{field:a,dir:c}:a,e=h(d)?d:d!==b?[d]:[];return k(e,function(a){return{field:a.field,dir:a.dir||"asc",aggregates:a.aggregates}})}function bc(a){return h(a)?a:[a]}function bb(a){if(a&&!g(a)){if(h(a)||!a.filters)a={logic:"and",filters:h(a)?a:[a]};ba(a);return a}}function ba(a){var b,c,d,e,f=a.filters;if(f)for(b=0,c=f.length;b<c;b++)d=f[b],e=d.operator,e&&typeof e===q&&(d.operator=_[e.toLowerCase()]||e),ba(d)}function $(a,c){if(a){var d=typeof a===q?{field:a,dir:c}:a,e=h(d)?d:d!==b?[d]:[];return i(e,function(a){return!!a.dir})}}function Z(a){this.data=a||[]}function S(b,c){if(b===c)return!0;var d=a.type(b),e=a.type(c),f;if(d!==e)return!1;if(d==="date")return b.getTime()===c.getTime();if(d!=="object"&&d!=="array")return!1;for(f in b)if(!S(b[f],c[f]))return!1;return!0}var c=a.extend,d=a.proxy,e=a.isFunction,f=a.isPlainObject,g=a.isEmptyObject,h=a.isArray,i=a.grep,j=a.ajax,k,l=a.each,m=a.noop,n=window.kendo,o=n.Observable,p=n.Class,q="string",r="function",s="create",t="read",u="update",v="destroy",w="change",x="get",y="error",z="requestStart",A=[s,t,u,v],B=function(a){return a},C=n.getter,D=n.stringify,E=Math,F=[].push,G=[].join,H=[].pop,I=[].splice,J=[].shift,K=[].slice,L=[].unshift,M={}.toString,N=n.support.stableSort,O=/^\/Date\((.*?)\)\/$/,P=/(?=['\\])/g,Q=o.extend({init:function(a,b){var c=this;c.type=b||R,o.fn.init.call(c),c.length=a.length,c.wrapAll(a,c)},toJSON:function(){var a,b=this.length,c,d=Array(b);for(a=0;a<b;a++)c=this[a],c instanceof R&&(c=c.toJSON()),d[a]=c;return d},parent:m,wrapAll:function(a,b){var c=this,d,e,f=function(){return c};b=b||[];for(d=0,e=a.length;d<e;d++)b[d]=c.wrap(a[d],f);return b},wrap:function(a,b){var c=this,d;a!==null&&M.call(a)==="[object Object]"&&(d=a instanceof c.type||a instanceof V,d||(a=a instanceof R?a.toJSON():a,a=new c.type(a)),a.parent=b,a.bind(w,function(a){c.trigger(w,{field:a.field,node:a.node,index:a.index,items:a.items||[this],action:a.action||"itemchange"})}));return a},push:function(){var a=this.length,b=this.wrapAll(arguments),c;c=F.apply(this,b),this.trigger(w,{action:"add",index:a,items:b});return c},slice:K,join:G,pop:function(){var a=this.length,b=H.apply(this);a&&this.trigger(w,{action:"remove",index:a-1,items:[b]});return b},splice:function(a,b,c){var d=this.wrapAll(K.call(arguments,2)),e,f,g;e=I.apply(this,[a,b].concat(d));if(e.length){this.trigger(w,{action:"remove",index:a,items:e});for(f=0,g=e.length;f<g;f++)e[f].children&&e[f].unbind(w)}c&&this.trigger(w,{action:"add",index:a,items:d});return e},shift:function(){var a=this.length,b=J.apply(this);a&&this.trigger(w,{action:"remove",index:0,items:[b]});return b},unshift:function(){var a=this.wrapAll(arguments),b;b=L.apply(this,a),this.trigger(w,{action:"add",index:0,items:a});return b},indexOf:function(a){var b=this,c,d;for(c=0,d=b.length;c<d;c++)if(b[c]===a)return c;return-1}}),R=o.extend({init:function(a){var b=this,c,d,e=function(){return b},f;o.fn.init.call(this);for(d in a)c=a[d],d.charAt(0)!="_"&&(f=M.call(c),c=b.wrap(c,d,e)),b[d]=c;b.uid=n.guid()},shouldSerialize:function(a){return this.hasOwnProperty(a)&&a!=="_events"&&typeof this[a]!==r&&a!=="uid"},toJSON:function(){var a={},b,c;for(c in this)if(this.shouldSerialize(c)){b=this[c];if(b instanceof R||b instanceof Q)b=b.toJSON();a[c]=b}return a},get:function(a){var b=this,c;b.trigger(x,{field:a}),a==="this"?c=b:c=n.getter(a,!0)(b);return c},_set:function(a,b){var c=this;if(a.indexOf(".")){var d=a.split("."),e="";while(d.length>1){e+=d.shift();var f=n.getter(e,!0)(c);if(f instanceof R){f.set(d.join("."),b);return}e+="."}}n.setter(a)(c,b)},set:function(a,b){var c=this,d=c[a],e=function(){return c};d!==b&&(c.trigger("set",{field:a,value:b})||(c._set(a,c.wrap(b,a,e)),c.trigger(w,{field:a})))},parent:m,wrap:function(a,b,c){var d=this,e=M.call(a),f=a instanceof Q;a===null||e!=="[object Object]"||a instanceof bz||!!f?a===null||e!=="[object Array]"&&!f?a!==null&&a instanceof bz&&(a._parent=c):(f||(a=new Q(a)),a.parent=c,function(b){a.bind(w,function(a){d.trigger(w,{field:b,index:a.index,items:a.items,action:a.action})})}(b)):(a instanceof R||(a=new R(a)),a.parent=c,function(b){a.bind(x,function(a){a.field=b+"."+a.field,d.trigger(x,a)}),a.bind(w,function(a){a.field=b+"."+a.field,d.trigger(w,a)})}(b));return a}}),T={number:function(a){return n.parseFloat(a)},date:function(a){if(typeof a===q){var b=O.exec(a);if(b)return new Date(parseInt(b[1],10))}return n.parseDate(a)},"boolean":function(a){if(typeof a===q)return a.toLowerCase()==="true";return!!a},string:function(a){return a+""},"default":function(a){return a}},U={string:"",number:0,date:new Date,"boolean":!1,"default":""},V=R.extend({init:function(c){var d=this;if(!c||a.isEmptyObject(c))c=a.extend({},d.defaults,c);R.fn.init.call(d,c),d.dirty=!1,d.idField&&(d.id=d.get(d.idField),d.id===b&&(d.id=d._defaultId))},shouldSerialize:function(a){return R.fn.shouldSerialize.call(this,a)&&a!=="uid"&&(this.idField==="id"||a!=="id")&&a!=="dirty"&&a!=="_accessors"},_parse:function(a,b){var c=this,d;a=(c.fields||{})[a],a&&(d=a.parse,!d&&a.type&&(d=T[a.type.toLowerCase()]));return d?d(b):b},editable:function(a){a=(this.fields||{})[a];return a?a.editable!==!1:!0},set:function(a,b,c){var d=this;d.editable(a)&&(b=d._parse(a,b),S(b,d.get(a))||(d.dirty=!0,R.fn.set.call(d,a,b,c)))},accept:function(a){var b=this;c(b,a),b.idField&&(b.id=b.get(b.idField)),b.dirty=!1},isNew:function(){return this.id===this._defaultId}});V.define=function(a,d){d===b&&(d=a,a=V);var e,f=c({},{defaults:{}},d),g,h,i,j,k=f.id;k&&(f.idField=k),f.id&&delete f.id,k&&(f.defaults[k]=f._defaultId="");for(g in f.fields)h=f.fields[g],i=h.type||"default",j=null,g=typeof h.field===q?h.field:g,h.nullable||(j=f.defaults[g]=h.defaultValue!==b?h.defaultValue:U[i.toLowerCase()]),d.id===g&&(f._defaultId=j),f.defaults[g]=j,h.parse=h.parse||T[i];e=a.extend(f),e.define=function(a){return V.define(e,a)},f.fields&&(e.fields=f.fields,e.idField=f.idField);return e};var W={selector:function(a){return e(a)?a:C(a)},asc:function(a){var b=this.selector(a);return function(a,c){a=b(a),c=b(c);return a>c?1:a<c?-1:0}},desc:function(a){var b=this.selector(a);return function(a,c){a=b(a),c=b(c);return a<c?1:a>c?-1:0}},create:function(a){return this[a.dir.toLowerCase()](a.field)},combine:function(a){return function(b,c){var d=a[0](b,c),e,f;for(e=1,f=a.length;e<f;e++)d=d||a[e](b,c);return d}}},X=c({},W,{asc:function(a){var b=this.selector(a);return function(a,c){var d=b(a),e=b(c);if(d===e)return a.__position-c.__position;return d>e?1:d<e?-1:0}},desc:function(a){var b=this.selector(a);return function(a,c){var d=b(a),e=b(c);if(d===e)return a.__position-c.__position;return d<e?1:d>e?-1:0}}});k=function(a,b){var c,d=a.length,e=Array(d);for(c=0;c<d;c++)e[c]=b(a[c],c,a);return e};var Y=function(){function b(b,c,d,e){var f;d!=null&&(typeof d===q&&(d=a(d),f=O.exec(d),f?d=new Date(+f[1]):e?(d="'"+d.toLowerCase()+"'",c="("+c+" || '').toLowerCase()"):d="'"+d+"'"),d.getTime&&(c="("+c+"?"+c+".getTime():"+c+")",d=d.getTime()));return c+" "+b+" "+d}function a(a){return a.replace(P,"\\")}return{eq:function(a,c,d){return b("==",a,c,d)},neq:function(a,c,d){return b("!=",a,c,d)},gt:function(a,c,d){return b(">",a,c,d)},gte:function(a,c,d){return b(">=",a,c,d)},lt:function(a,c,d){return b("<",a,c,d)},lte:function(a,c,d){return b("<=",a,c,d)},startswith:function(b,c,d){d&&(b=b+".toLowerCase()",c&&(c=c.toLowerCase())),c&&(c=a(c));return b+".lastIndexOf('"+c+"', 0) == 0"},endswith:function(b,c,d){d&&(b=b+".toLowerCase()",c&&(c=c.toLowerCase())),c&&(c=a(c));return b+".lastIndexOf('"+c+"') == "+b+".length - "+(c||"").length},contains:function(b,c,d){d&&(b="("+b+" || '').toLowerCase()",c&&(c=c.toLowerCase())),c&&(c=a(c));return b+".indexOf('"+c+"') >= 0"},doesnotcontain:function(b,c,d){d&&(b="("+b+" || '').toLowerCase()",c&&(c=c.toLowerCase())),c&&(c=a(c));return b+".indexOf('"+c+"') == -1"}}}();Z.filterExpr=function(a){var c=[],d={and:" && ",or:" || "},e,f,g,h,i=[],j=[],k,l,m=a.filters;for(e=0,f=m.length;e<f;e++)g=m[e],k=g.field,l=g.operator,g.filters?(h=Z.filterExpr(g),g=h.expression.replace(/__o\[(\d+)\]/g,function(a,b){b=+b;return"__o["+(j.length+b)+"]"}).replace(/__f\[(\d+)\]/g,function(a,b){b=+b;return"__f["+(i.length+b)+"]"}),j.push.apply(j,h.operators),i.push.apply(i,h.fields)):(typeof k===r?(h="__f["+i.length+"](d)",i.push(k)):h=n.expr(k),typeof l===r?(g="__o["+j.length+"]("+h+", "+g.value+")",j.push(l)):g=Y[(l||"eq").toLowerCase()](h,g.value,g.ignoreCase!==b?g.ignoreCase:!0)),c.push(g);return{expression:"("+c.join(d[a.logic])+")",fields:i,operators:j}};var _={"==":"eq",equals:"eq",isequalto:"eq",equalto:"eq",equal:"eq","!=":"neq",ne:"neq",notequals:"neq",isnotequalto:"neq",notequalto:"neq",notequal:"neq","<":"lt",islessthan:"lt",lessthan:"lt",less:"lt","<=":"lte",le:"lte",islessthanorequalto:"lte",lessthanequal:"lte",">":"gt",isgreaterthan:"gt",greaterthan:"gt",greater:"gt",">=":"gte",isgreaterthanorequalto:"gte",greaterthanequal:"gte",ge:"gte",notsubstringof:"doesnotcontain"};Z.normalizeFilter=bb,Z.prototype={toArray:function(){return this.data},range:function(a,b){return new Z(this.data.slice(a,a+b))},skip:function(a){return new Z(this.data.slice(a))},take:function(a){return new Z(this.data.slice(0,a))},select:function(a){return new Z(k(this.data,a))},orderBy:function(a){var b=this.data.slice(0),c=e(a)||!a?W.asc(a):a.compare;return new Z(b.sort(c))},orderByDescending:function(a){return new Z(this.data.slice(0).sort(W.desc(a)))},sort:function(a,b,c){var d,e,f=$(a,b),g=[];c=c||W;if(f.length){for(d=0,e=f.length;d<e;d++)g.push(c.create(f[d]));return this.orderBy({compare:c.combine(g)})}return this},filter:function(a){var b,c,d,e,f,g=this.data,h,i,j=[],k;a=bb(a);if(!a||a.filters.length===0)return this;e=Z.filterExpr(a),h=e.fields,i=e.operators,f=k=new Function("d, __f, __o","return "+e.expression);if(h.length||i.length)k=function(a){return f(a,h,i)};for(b=0,d=g.length;b<d;b++)c=g[b],k(c)&&j.push(c);return new Z(j)},group:function(a,b){a=bd(a||[]),b=b||this.data;var c=this,d=new Z(c.data),e;a.length>0&&(e=a[0],d=d.groupBy(e).select(function(c){var d=(new Z(b)).filter([{field:c.field,operator:"eq",value:c.value}]);return{field:c.field,value:c.value,items:a.length>1?(new Z(c.items)).group(a.slice(1),d.toArray()).toArray():c.items,hasSubgroups:a.length>1,aggregates:d.aggregate(e.aggregates)}}));return d},groupBy:function(a){if(g(a)||!this.data.length)return new Z([]);var b=a.field,c=this._sortForGrouping(b,a.dir||"asc"),d=n.accessor(b),e,f=d.get(c[0],b),h={field:b,value:f,items:[]},i,j,k,l=[h];for(j=0,k=c.length;j<k;j++)e=c[j],i=d.get(e,b),be(f,i)||(f=i,h={field:b,value:f,items:[]},l.push(h)),h.items.push(e);return new Z(l)},_sortForGrouping:function(a,b){var c,d,e=this.data;if(!N){for(c=0,d=e.length;c<d;c++)e[c].__position=c;e=(new Z(e)).sort(a,b,X).toArray();for(c=0,d=e.length;c<d;c++)delete e[c].__position;return e}return this.sort(a,b).toArray()},aggregate:function(a){var b,c,d={};if(a&&a.length)for(b=0,c=this.data.length;b<c;b++)bf(d,a,this.data[b],b,c);return d}};var bg={sum:function(a,b,c){return(a||0)+c.get(b)},count:function(a,b,c){return(a||0)+1},average:function(a,b,c,d,e){a=(a||0)+c.get(b),d==e-1&&(a=a/e);return a},max:function(a,b,c){var d=c.get(b);a=a||0,a<d&&(a=d);return a},min:function(a,b,c){var d=c.get(b);a=a||d,a>d&&(a=d);return a}},bk=p.extend({init:function(a){this.data=a.data},read:function(a){a.success(this.data)},update:function(a){a.success(a.data)},create:function(a){a.success(a.data)},destroy:m}),bl=p.extend({init:function(a){var b=this,d;a=b.options=c({},b.options,a),l(A,function(b,c){typeof a[c]===q&&(a[c]={url:a[c]})}),b.cache=a.cache?bm.create(a.cache):{find:m,add:m},d=a.parameterMap,b.parameterMap=e(d)?d:function(a){var b={};l(a,function(a,c){a in d&&(a=d[a],f(a)&&(c=a.value(c),a=a.key)),b[a]=c});return b}},options:{parameterMap:B},create:function(a){return j(this.setup(a,s))},read:function(c){var d=this,e,f,g,h=d.cache;c=d.setup(c,t),e=c.success||m,f=c.error||m,g=h.find(c.data),g!==b?e(g):(c.success=function(a){h.add(c.data,a),e(a)},a.ajax(c))},update:function(a){return j(this.setup(a,u))},destroy:function(a){return j(this.setup(a,v))},setup:function(a,b){a=a||{};var d=this,f,g=d.options[b],h=e(g.data)?g.data(a.data):g.data;a=c(!0,{},g,a),f=c(!0,{},h,a.data),a.data=d.parameterMap(f,b),e(a.url)&&(a.url=a.url(f));return a}}),bm=p.extend({init:function(){this._store={}},add:function(a,c){a!==b&&(this._store[D(a)]=c)},find:function(a){return this._store[D(a)]},clear:function(){this._store={}},remove:function(a){delete this._store[D(a)]}});bm.create=function(a){var b={inmemory:function(){return new bm}};if(f(a)&&e(a.find))return a;if(a===!0)return new bm;return b[a]()};var bq=p.extend({init:function(a){var b=this,c,e,g,h;a=a||{};for(c in a)e=a[c],b[c]=typeof e===q?C(e):e;h=a.modelBase||V;if(f(b.model)){b.model=g=h.define(b.model);var i=d(b.data,b),j=d(b.groups,b),k={};g.fields&&l(g.fields,function(a,b){f(b)&&b.field?k[b.field]=C(b.field):k[a]=C(a)}),b.data=bp(i,g,bn,k),b.groups=bp(j,g,bo,k)}},errors:function(a){return a?a.errors:null},parse:B,data:B,total:function(a){return a.length},groups:B,status:function(a){return a.status},aggregates:function(){return{}}}),bz=o.extend({init:function(a){var d=this,g,h,i;a&&(i=a.data),a=d.options=c({},d.options,a),c(d,{_map:{},_prefetch:{},_data:[],_ranges:[],_view:[],_pristine:[],_destroyed:[],_pageSize:a.pageSize,_page:a.page||(a.pageSize?1:b),_sort:$(a.sort),_filter:bb(a.filter),_group:bd(a.group),_aggregate:a.aggregate,_total:a.total}),o.fn.init.call(d),h=a.transport,h?(h.read=typeof h.read===q?{url:h.read}:h.read,a.type&&(n.data.transports[a.type]&&!f(n.data.transports[a.type])?d.transport=new n.data.transports[a.type](c(h,{data:i})):h=c(!0,{},n.data.transports[a.type],h),a.schema=c(!0,{},n.data.schemas[a.type],a.schema)),d.transport||(d.transport=e(h.read)?h:new bl(h))):d.transport=new bk({data:a.data}),d.reader=new n.data.readers[a.schema.type||"json"](a.schema),g=d.reader.model||{},d._data=d._observe(d._data),d.bind([y,w,z],a)},options:{data:[],schema:{modelBase:V},serverSorting:!1,serverPaging:!1,serverFiltering:!1,serverGrouping:!1,serverAggregates:!1,sendAllFields:!0,batch:!1},_flatData:function(a){if(this.options.serverGrouping&&this.group().length)return br(a);return a},get:function(a){var b,c,d=this._flatData(this._data);for(b=0,c=d.length;b<c;b++)if(d[b].id==a)return d[b]},getByUid:function(a){var b,c,d=this._flatData(this._data);for(b=0,c=d.length;b<c;b++)if(d[b].uid==a)return d[b]},sync:function(){var b=this,c,d,e=[],f=[],g=b._destroyed,h=b._flatData(b._data);if(!!b.reader.model){for(c=0,d=h.length;c<d;c++)h[c].isNew()?e.push(h[c]):h[c].dirty&&f.push(h[c]);var i=b._send("create",e);i.push.apply(i,b._send("update",f)),i.push.apply(i,b._send("destroy",g)),a.when.apply(null,i).then(function(){var a,c;for(a=0,c=arguments.length;a<c;a++)b._accept(arguments[a]);b._change()})}},_accept:function(b){var d=this,e=b.models,f=b.response,g=0,h=d.options.serverGrouping&&d.group()&&d.group().length,i=d.reader.data(d._pristine),j=b.type,k;if(f){f=d.reader.parse(f);if(d._handleCustomErrors(f))return;f=d.reader.data(f),a.isArray(f)||(f=[f])}else f=a.map(e,function(a){return a.toJSON()});j==="destroy"&&(d._destroyed=[]);for(g=0,k=e.length;g<k;g++)j!=="destroy"?(e[g].accept(f[g]),j==="create"?i.push(h?bv(d.group(),e[g]):e[g]):j==="update"&&(h?d._updatePristineGroupModel(e[g],f[g]):c(i[d._pristineIndex(e[g])],f[g]))):h?d._removePristineGroupModel(e[g]):i.splice(d._pristineIndex(e[g]),1)},_pristineIndex:function(a){var b=this,c,d,e=b.reader.data(b._pristine);for(c=0,d=e.length;c<d;c++)if(e[c][a.idField]===a.id)return c;return-1},_updatePristineGroupModel:function(a,b){var d=this.reader.groups(this._pristine),e;bt(d,function(d,f){e=bw(d,a);if(e>-1){c(!0,d[e],b);return!0}})},_removePristineGroupModel:function(a){var b=this.reader.groups(this._pristine),c;bt(b,function(b,d){c=bw(b,a);if(c>-1){b.splice(c,1);return!0}})},_promise:function(b,d,e){var f=this,g=f.transport;return a.Deferred(function(a){g[e].call(g,c({success:function(b){a.resolve({response:b,models:d,type:e})},error:function(b){a.reject(b),f.trigger(y,b)}},b))}).promise()},_send:function(a,b){var c=this,d,e,f=[];if(c.options.batch)b.length&&f.push(c._promise({data:{models:bh(b)}},b,a));else for(d=0,e=b.length;d<e;d++)f.push(c._promise({data:b[d].toJSON()},[b[d]],a));return f},add:function(a){return this.insert(this._data.length,a)},insert:function(a,b){b||(b=a,a=0),b instanceof V||(this.reader.model?b=new this.reader.model(b):b=new R(b)),this.options.serverGrouping&&this.group()&&this.group().length?this._data.splice(a,0,bv(this.group(),b)):this._data.splice(a,0,b);return b},cancelChanges:function(a){var b=this,d,e=b.options.serverGrouping&&b.group()&&b.group().length,f=e?b.reader.groups:b.reader.data,g=f(b._pristine),h;a instanceof n.data.Model?e?b._cancelGroupModel(a):(h=b.indexOf(a),d=b._pristineIndex(a),h!=-1&&(d!=-1&&!a.isNew()?c(!0,b._data[h],g[d]):b._data.splice(h,1))):(b._destroyed=[],b._data=b._observe(g),b._change())},read:function(a){var b=this,c=b._params(a);b._queueRequest(c,function(){b.trigger(z),b._ranges=[],b.transport.read({data:c,success:d(b.success,b),error:d(b.error,b)})})},_cancelGroupModel:function(a){var b=this.reader.groups(this._pristine),d,e;bt(b,function(b,c){e=bw(b,a);if(e>-1){d=b[e];return!0}}),e>-1&&bt(this._data,function(b,f){e=bx(b,a),e>-1&&(a.isNew()?b.splice(e,1):c(!0,b[e],d))})},indexOf:function(a){return bx(this._data,a)},_params:function(a){var b=this,d=c({take:b.take(),skip:b.skip(),page:b.page(),pageSize:b.pageSize(),sort:b._sort,filter:b._filter,group:b._group,aggregate:b._aggregate},a);b.options.serverPaging||(delete d.take,delete d.skip,delete d.page,delete d.pageSize);return d},_queueRequest:function(a,c){var e=this;e._requestInProgress?e._pending={callback:d(c,e),options:a}:(e._requestInProgress=!0,e._pending=b,c())},_dequeueRequest:function(){var a=this;a._requestInProgress=!1,a._pending&&a._queueRequest(a._pending.options,a._pending.callback)},remove:function(a){var b=this._data;if(this.options.serverGrouping&&this.group()&&this.group().length)return this._removeGroupItem(b,a);return bu(b,a)},_removeGroupItem:function(a,b){var c;bt(a,function(a,d){c=bu(a,b);if(c)return!0});return b},error:function(a,b,c){this._dequeueRequest(),this.trigger(y,{xhr:a,status:b,errorThrown:c})},_handleCustomErrors:function(a){if(this.reader.errors){var b=this.reader.errors(a);if(b){this.trigger(y,{xhr:null,status:"customerror",errorThrown:"custom error",errors:b});return!0}}return!1},_parent:m,success:function(b){var c=this,d=c.options,e=d.serverGrouping===!0&&c._group&&c._group.length>0;b=c.reader.parse(b);if(!c._handleCustomErrors(b)){c._pristine=f(b)?a.extend(!0,{},b):b.slice(0),c._total=c.reader.total(b),c._aggregate&&d.serverAggregates&&(c._aggregateResult=c.reader.aggregates(b)),e?b=c.reader.groups(b):b=c.reader.data(b),c._data=c._observe(b);var g=c._skip||0,h=g+c._data.length;c._ranges.push({start:g,end:h,data:c._data}),c._ranges.sort(function(a,b){return a.start-b.start}),c._dequeueRequest(),c._process(c._data)}},_observe:function(a){var b=this,c=b.reader.model,e=!1;c&&a.length&&(e=!(a[0]instanceof c)),a instanceof Q?e&&(a.type=b.reader.model,a.wrapAll(a,a)):(a=new Q(a,b.reader.model),a.parent=function(){return b._parent()}),b.group()&&b.group().length&&b.options.serverGrouping&&bs(a,c);return a.bind(w,d(b._change,b))},_change:function(a){var b=this,c,d,e=a?a.action:"";if(e==="remove")for(c=0,d=a.items.length;c<d;c++)(!a.items[c].isNew||!a.items[c].isNew())&&b._destroyed.push(a.items[c]);if(!b.options.autoSync||e!=="add"&&e!=="remove"&&e!=="itemchange"){var f=b._total||b.reader.total(b._pristine);e==="add"?f++:e==="remove"?f--:e!=="itemchange"&&!b.options.serverPaging&&(f=b.reader.total(b._pristine)),b._total=f,b._process(b._data,a)}else b.sync()},_process:function(a,c){var d=this,e={},f;d.options.serverPaging!==!0&&(e.skip=d._skip,e.take=d._take||d._pageSize,e.skip===b&&d._page!==b&&d._pageSize!==b&&(e.skip=(d._page-1)*d._pageSize)),d.options.serverSorting!==!0&&(e.sort=d._sort),d.options.serverFiltering!==!0&&(e.filter=d._filter),d.options.serverGrouping!==!0&&(e.group=d._group),d.options.serverAggregates!==!0&&(e.aggregate=d._aggregate,d._aggregateResult=bj(a,e)),f=bi(a,e),d._view=f.data,f.total!==b&&!d.options.serverFiltering&&(d._total=f.total),c=c||{},c.items=c.items||d._view,d.trigger(w,c)},at:function(a){return this._data[a]},data:function(a){var c=this;if(a!==b)c._data=this._observe(a),c._total=c._data.length,c._process(c._data);else return c._data},view:function(){return this._view},query:function(a){var c=this,d,e=c.options.serverSorting||c.options.serverPaging||c.options.serverFiltering||c.options.serverGrouping||c.options.serverAggregates;a!==b&&(c._pageSize=a.pageSize,c._page=a.page,c._sort=a.sort,c._filter=a.filter,c._group=a.group,c._aggregate=a.aggregate,c._skip=a.skip,c._take=a.take,c._skip===b&&(c._skip=c.skip(),a.skip=c.skip()),c._take===b&&c._pageSize!==b&&(c._take=c._pageSize,a.take=c._take),a.sort&&(c._sort=a.sort=$(a.sort)),a.filter&&(c._filter=a.filter=bb(a.filter)),a.group&&(c._group=a.group=bd(a.group)),a.aggregate&&(c._aggregate=a.aggregate=bc(a.aggregate))),e||c._data===b||c._data.length===0?c.read(a):(c.trigger(z),d=bi(c._data,a),c.options.serverFiltering||(d.total!==b?c._total=d.total:c._total=c._data.length),c._view=d.data,c._aggregateResult=bj(c._data,a),c.trigger(w,{items:d.data}))},fetch:function(a){var b=this;a&&e(a)&&b.one(w,a),b._query()},_query:function(a){var b=this;b.query(c({},{page:b.page(),pageSize:b.pageSize(),sort:b.sort(),filter:b.filter(),group:b.group(),aggregate:b.aggregate()},a))},next:function(){var a=this,b=a.page(),c=a.total();!b||(c?a.page(b+1):(a._skip=b*a.take(),a._query({page:b+1})))},prev:function(){var a=this,b=a.page(),c=a.total();!!b&&b!==1&&(c?a.page(b-1):(a._skip=a._skip-a.take(),a._query({page:b-1})))},page:function(a){var c=this,d;if(a!==b)a=E.max(E.min(E.max(a,1),c.totalPages()),1),c._query({page:a});else{d=c.skip();return d!==b?E.round((d||0)/(c.take()||1))+1:b}},pageSize:function(a){var c=this;if(a!==b)c._query({pageSize:a,page:1});else return c.take()},sort:function(a){var c=this;if(a!==b)c._query({sort:a});else return c._sort},filter:function(a){var c=this;if(a===b)return c._filter;c._query({filter:a,page:1})},group:function(a){var c=this;if(a!==b)c._query({group:a});else return c._group},total:function(){return this._total||0},aggregate:function(a){var c=this;if(a!==b)c._query({aggregate:a});else return c._aggregate},aggregates:function(){return this._aggregateResult},totalPages:function(){var a=this,b=a.pageSize()||a.total();return E.ceil((a.total()||0)/b)},inRange:function(a,b){var c=this,d=E.min(a+b,c.total());if(!c.options.serverPaging&&c.data.length>0)return!0;return c._findRange(a,d).length>0},range:function(a,c){a=E.min(a||0,this.total());var d=this,e=E.max(E.floor(a/c),0)*c,f=E.min(e+c,d.total()),g;g=d._findRange(a,E.min(a+c,d.total()));if(g.length){d._skip=a>d.skip()?E.min(f,(d.totalPages()-1)*d.take()):e,d._take=c;var h=d.options.serverPaging,i=d.options.serverSorting;try{d.options.serverPaging=!0,d.options.serverSorting=!0,h&&(d._data=g=d._observe(g)),d._process(g)}finally{d.options.serverPaging=h,d.options.serverSorting=i}}else c!==b&&(d._rangeExists(e,f)?e<a&&d.prefetch(f,c,function(){d.range(a,c)}):d.prefetch(e,c,function(){a>e&&f<d.total()&&!d._rangeExists(f,E.min(f+c,d.total()))?d.prefetch(f,c,function(){d.range(a,c)}):d.range(a,c)}))},_findRange:function(a,c){var d=this,e=d._ranges,f,g=[],h,i,j,k,l,m,n,o=d.options,p=o.serverSorting||o.serverPaging||o.serverFiltering||o.serverGrouping||o.serverAggregates,q;for(h=0,q=e.length;h<q;h++){f=e[h];if(a>=f.start&&a<=f.end){var r=0;for(i=h;i<q;i++){f=e[i];if(f.data.length&&a+r>=f.start){l=f.data,m=f.end,p||(n=bi(f.data,{sort:d.sort(),filter:d.filter()}),l=n.data,n.total!==b&&(m=n.total)),j=0,a+r>f.start&&(j=a+r-f.start),k=l.length,m>c&&(k=k-(m-c)),r+=k-j,g=g.concat(l.slice(j,k));if(c<=f.end&&r==c-a)return g}}break}}return[]},skip:function(){var a=this;if(a._skip===b)return a._page!==b?(a._page-1)*(a.take()||1):b;return a._skip},take:function(){var a=this;return a._take||a._pageSize},prefetch:function(a,b,c){var d=this,e=E.min(a+b,d.total()),f={start:a,end:e,data:[]},g={take:b,skip:a,page:a/b+1,pageSize:b,sort:d._sort,filter:d._filter,group:d._group,aggregate:d._aggregate};d._rangeExists(a,e)?c&&c():(clearTimeout(d._timeout),d._timeout=setTimeout(function(){d._queueRequest(g,function(){d.transport.read({data:g,success:function(b){d._dequeueRequest();var e=!1;for(var g=0,h=d._ranges.length;g<h;g++)if(d._ranges[g].start===a){e=!0,f=d._ranges[g];break}e||d._ranges.push(f),b=d.reader.parse(b),f.data=d._observe(d.reader.data(b)),f.end=f.start+f.data.length,d._ranges.sort(function(a,b){return a.start-b.start}),d._total=d.reader.total(b),c&&c()}})})},100))},_rangeExists:function(a,b){var c=this,d=c._ranges,e,f;for(e=0,f=d.length;e<f;e++)if(d[e].start<=a&&d[e].end>=b)return!0;return!1}});bz.create=function(a){a=a&&a.push?{data:a}:a;var b=a||{},d=b.data,e=b.fields,f=b.table,h=b.select,i,j,k={},l;!d&&e&&!b.transport&&(f?d=bB(f,e):h&&(d=bA(h,e)));if(n.data.Model&&e&&(!b.schema||!b.schema.model)){for(i=0,j=e.length;i<j;i++)l=e[i],l.type&&(k[l.field]=l);g(k)||(b.schema=c(!0,b.schema,{model:{fields:k}}))}b.data=d;return b instanceof bz?b:new bz(b)};var bC=V.define({init:function(a){var b=this,d=b.hasChildren||a.hasChildren,f="items",g={};n.data.Model.fn.init.call(b,a),typeof b.children===q&&(f=b.children),g=c({schema:{data:f,model:{hasChildren:d}}},b.children,{data:a}),d||(d=g.schema.data),typeof d===q&&(d=n.getter(d)),e(d)&&(b.hasChildren=!!d.call(b,b)),b.children=new bD(g),b.children._parent=function(){return b},b.children.bind(w,function(a){a.node=a.node||b,b.trigger(w,a)}),b._loaded=!!a&&!!a[f]},hasChildren:!1,level:function(){var a=this.parentNode(),b=0;while(a)b++,a=a.parentNode();return b},load:function(){var a=this,c={};c[a.idField||"id"]=a.id,a._loaded||(a.children._data=b),a.children.one(w,function(){a._loaded=!0}).query(c)},parentNode:function(){var a=this.parent();return a.parent()},loaded:function(a){if(a!==b)this._loaded=a;else return this._loaded},shouldSerialize:function(a){return V.fn.shouldSerialize.call(this,a)&&a!=="children"&&a!=="_loaded"&&a!=="hasChildren"}}),bD=bz.extend({init:function(a){var b=bC.define({children:a});bz.fn.init.call(this,c(!0,{},{schema:{modelBase:b,model:b}},a))},remove:function(a){var b=a.parentNode(),c=this,d;b&&(c=b.children),d=bz.fn.remove.call(c,a),b&&!c.data().length&&(b.hasChildren=!1);return d},insert:function(a,b){var c=this._parent();c&&(c.hasChildren=!0);return bz.fn.insert.call(this,a,b)},getByUid:function(a){var b,c,d,e;d=bz.fn.getByUid.call(this,a);if(d)return d;e=this._flatData(this.data());for(b=0,c=e.length;b<c;b++){d=e[b].children.getByUid(a);if(d)return d}}});bD.create=function(a){a=a&&a.push?{data:a}:a;var b=a||{},c=b.data,d=b.fields,e=b.list;!c&&d&&!b.transport&&e&&(c=bE(e,d)),b.data=c;return b instanceof bD?b:new bD(b)},c(!0,n.data,{readers:{json:bq},Query:Z,DataSource:bz,HierarchicalDataSource:bD,Node:bC,ObservableObject:R,ObservableArray:Q,LocalTransport:bk,RemoteTransport:bl,Cache:bm,DataReader:bq,Model:V})}(jQuery),function(a,b){function F(a,b){var c=a.element,d=c[0].kendoBindingTarget;d&&B(c,d.source,b)}function E(b){var c,d;b=a(b);for(c=0,d=b.length;c<d;c++)D(b[c])}function D(a){var b,c,d=a.children;C(a);if(d)for(b=0,c=d.length;b<c;b++)D(d[b])}function C(b){var c=b.kendoBindingTarget;c&&(c.destroy(),a.support.deleteExpando?delete b.kendoBindingTarget:b.removeAttribute?b.removeAttribute("kendoBindingTarget"):b.kendoBindingTarget=null)}function B(b,d,e){var f,g;d=c.observable(d),b=a(b);for(f=0,g=b.length;f<g;f++)A(b[f],d,e)}function A(a,b,d){var e=a.getAttribute("data-"+c.ns+"role"),f,g=a.getAttribute("data-"+c.ns+"bind"),h=a.children,i=!0,j,k={},l;d||(d=c.ui),(e||g)&&C(a),e&&(l=v(e,a,d)),g&&(g=y(g.replace(x,"")),l||(k=c.parseOptions(a,{textField:"",valueField:"",template:"",valueUpdate:n}),l=new s(a,k)),l.source=b,j=z(g,b,o),k.template&&(j.template=new q(b,"",k.template)),j.click&&(g.events=g.events||{},g.events.click=g.click,delete j.click),j.source&&(i=!1),g.attr&&(j.attr=z(g.attr,b,o)),g.style&&(j.style=z(g.style,b,o)),g.events&&(j.events=z(g.events,b,p)),l.bind(j)),l&&(a.kendoBindingTarget=l);if(i&&h)for(f=0;f<h.length;f++)A(h[f],b,d)}function z(a,b,c){var d,e={};for(d in a)e[d]=new c(b,a[d]);return e}function y(a){var b={},c,d,e,f,g,h,i;i=a.match(w);for(c=0,d=i.length;c<d;c++)e=i[c],f=e.indexOf(":"),g=e.substring(0,f),h=e.substring(f+1),h.charAt(0)=="{"&&(h=y(h)),b[g]=h;return b}function v(a,b,d){var e=d.roles,f=e[a];if(f)return new t(c.initWidget(b,f.options,e))}function u(a){var b,c,d=[];for(b=0,c=a.length;b<c;b++)a[b].hasSubgroups?d=d.concat(u(a[b].items)):d=d.concat(a[b].items);return d}var c=window.kendo,d=c.Observable,e=c.data.ObservableObject,f=c.data.ObservableArray,g={}.toString,h={},i=c.Class,j,k=a.proxy,l="value",m="checked",n="change";(function(){var a=document.createElement("a");a.innerText!==undefined?j="innerText":a.textContent!==undefined&&(j="textContent")})();var o=d.extend({init:function(a,b){var c=this;d.fn.init.call(c),c.source=a,c.path=b,c.dependencies={},c.dependencies[b]=!0,c.observable=c.source instanceof d,c._access=function(a){c.dependencies[a.field]=!0},c.observable&&(c._change=function(a){c.change(a)},c.source.bind(n,c._change))},change:function(a){var b,c,d,e=this;if(e.path==="this")e.trigger(n,a);else for(b in e.dependencies){c=b.indexOf(a.field);if(c===0){d=b.charAt(a.field.length);if(!d||d==="."||d==="["){e.trigger(n,a);break}}}},start:function(){this.observable&&this.source.bind("get",this._access)},stop:function(){this.observable&&this.source.unbind("get",this._access)},get:function(){var a=this,b=a.source,c,d=a.path,f=b;a.start();if(a.observable){f=b.get(d);while(f===undefined&&b)b=b.parent(),b instanceof e&&(f=b.get(d));typeof f=="function"&&(c=d.lastIndexOf("."),c>0&&(b=b.get(d.substring(0,c))),f=k(f,b),f=f(a.source)),b&&b!==a.source&&b.unbind(n,a._change).bind(n,a._change)}a.stop();return f},set:function(a){this.source.set(this.path,a)},destroy:function(){this.observable&&this.source.unbind(n,this._change)}}),p=o.extend({get:function(){var a=this.source,b=this.path,c;c=a.get(b);while(!c&&a)a=a.parent(),a instanceof e&&(c=a.get(b));return k(c,a)}}),q=o.extend({init:function(a,b,c){var d=this;o.fn.init.call(d,a,b),d.template=c},render:function(a){var b;this.start(),b=c.render(this.template,a),this.stop();return b}}),r=i.extend({init:function(a,b,c){this.element=a,this.bindings=b,this.options=c},bind:function(a,b){var c=this;a=b?a[b]:a,a.bind(n,function(a){c.refresh(b||a)}),c.refresh(b)},destroy:function(){}});h.attr=r.extend({refresh:function(a){this.element.setAttribute(a,this.bindings.attr[a].get())}}),h.style=r.extend({refresh:function(a){this.element.style[a]=this.bindings.style[a].get()}}),h.enabled=r.extend({refresh:function(){this.bindings.enabled.get()?this.element.removeAttribute("disabled"):this.element.setAttribute("disabled","disabled")}}),h.disabled=r.extend({refresh:function(){this.bindings.disabled.get()?this.element.setAttribute("disabled","disabled"):this.element.removeAttribute("disabled")}}),h.events=r.extend({init:function(a,b,c){r.fn.init.call(this,a,b,c),this.handlers={}},refresh:function(b){var c=this.bindings.events[b],d=this.handlers[b]=c.get();a(this.element).bind(b,c.source,d)},destroy:function(){var b=a(this.element),c;for(c in this.handlers)b.unbind(c,this.handlers[c])}}),h.text=r.extend({refresh:function(){var a=this.bindings.text.get();a==null&&(a=""),this.element[j]=a}}),h.visible=r.extend({refresh:function(){this.bindings.visible.get()?this.element.style.display="":this.element.style.display="none"}}),h.invisible=r.extend({refresh:function(){this.bindings.invisible.get()?this.element.style.display="none":this.element.style.display=""}}),h.html=r.extend({refresh:function(){this.element.innerHTML=this.bindings.html.get()}}),h.value=r.extend({init:function(b,c,d){r.fn.init.call(this,b,c,d),this._change=k(this.change,this),this.eventName=d.valueUpdate||n,a(this.element).bind(this.eventName,this._change),this._initChange=!1},change:function(){this._initChange=this.eventName!=n,this.bindings[l].set(this.element.value),this._initChange=!1},refresh:function(){if(!this._initChange){var a=this.bindings[l].get();a==null&&(a=""),this.element.value=a}this._initChange=!1},destroy:function(){a(this.element).unbind(this.eventName,this._change)}}),h.source=r.extend({init:function(a,b,c){r.fn.init.call(this,a,b,c)},refresh:function(a){var b=this,c=b.bindings.source.get();c instanceof f?(a=a||{},a.action=="add"?b.add(a.index,a.items):a.action=="remove"?b.remove(a.index,a.items):a.action!="itemchange"&&b.render()):b.render()},container:function(){var a=this.element;a.nodeName.toLowerCase()=="table"&&(a.tBodies[0]||a.appendChild(document.createElement("tbody")),a=a.tBodies[0]);return a},template:function(){var a=this.options,b=a.template,d=this.container().nodeName.toLowerCase();b||(d=="select"?a.valueField||a.textField?b=c.format('<option value="#:{0}#">#:{1}#</option>',a.valueField||a.textField,a.textField||a.valueField):b="<option>#:data#</option>":d=="tbody"?b="<tr><td>#:data#</td></tr>":d=="ul"||d=="ol"?b="<li>#:data#</li>":b="#:data#",b=c.template(b));return b},destroy:function(){var a=this.bindings.source.get();a.unbind(n,this._change)},add:function(b,d){var e=this.container(),f,g,h,i=e.cloneNode(!1),j=e.children[b];a(i).html(c.render(this.template(),d));if(i.children.length)for(f=0,g=d.length;f<g;f++)h=i.children[0],e.insertBefore(h,j||null),A(h,d[f])},remove:function(a,b){var c,d=this.container();for(c=0;c<b.length;c++)d.removeChild(d.children[a])},render:function(){var b=this.bindings.source.get(),d,e,h=this.container(),i=this.template(),j;!(b instanceof f)&&g.call(b)!=="[object Array]"&&(b.parent&&(j=b.parent),b=new f([b]),b.parent&&(b.parent=j));if(this.bindings.template){a(h).html(this.bindings.template.render(b));if(h.children.length)for(d=0,e=b.length;d<e;d++)A(h.children[d],b[d])}else a(h).html(c.render(i,b))}}),h.input={checked:r.extend({init:function(b,c,d){r.fn.init.call(this,b,c,d),this._change=k(this.change,this),a(this.element).change(this._change)},change:function(){var a=this.element,b=this.value();if(a.type=="radio")this.bindings[m].set(b);else if(a.type=="checkbox"){var c=this.bindings[m].get(),d;c instanceof f?b!==!1&&b!==!0&&(d=c.indexOf(b),d>-1?c.splice(d,1):c.push(b)):this.bindings[m].set(b)}},refresh:function(){var a=this.bindings[m].get(),b=this.element;b.type=="checkbox"?(a instanceof f&&a.indexOf(this.value(b))>=0&&(a=!0),b.checked=a===!0):b.type=="radio"&&a!=null&&b.value===a.toString()&&(b.checked=!0)},value:function(){var a=this.element,b=a.value;a.type=="checkbox"&&(b=="on"||b=="off"||b==="true")&&(b=a.checked);return b},destroy:function(){a(this.element).unbind(n,this._change)}})},h.select={value:r.extend({init:function(b,c,d){r.fn.init.call(this,b,c,d),this._change=k(this.change,this),a(this.element).change(this._change)},change:function(){var a=[],b=this.element,c,d=this.options.valueField||this.options.textField,g,h,i,j,k;for(j=0,k=b.options.length;j<k;j++)g=b.options[j],g.selected&&(i=g.attributes.value,i&&i.specified?i=g.value:i=g.text,a.push(i));if(d){c=this.bindings.source.get();for(h=0;h<a.length;h++)for(j=0,k=c.length;j<k;j++)if(c[j].get(d)==a[h]){a[h]=c[j];break}}i=this.bindings[l].get(),i instanceof f?i.splice.apply(i,[0,i.length].concat(a)):i instanceof e||!d?this.bindings[l].set(a[0]):this.bindings[l].set(a[0].get(d))},refresh:function(){var a,b=this.element,c=b.options,d=this.bindings[l].get(),g=d,h=this.options.valueField||this.options.textField,i;g instanceof f||(g=new f([d]));for(var j=0;j<g.length;j++){d=g[j],h&&d instanceof e&&(d=d.get(h));for(a=0;a<c.length;a++)i=c[a].value,i===""&&d!==""&&(i=c[a].text),i==d&&(c[a].selected=!0)}},destroy:function(){a(this.element).unbind(n,this._change)}})},h.widget={events:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a,this.handlers={}},refresh:function(a){var b=this.bindings.events[a],c=b.get();this.handlers[a]=function(a){a.data=b.source,c(a)},this.widget.bind(a,this.handlers[a])},destroy:function(){var a;for(a in this.handlers)this.widget.unbind(a,this.handlers[a])}}),checked:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a,this._change=k(this.change,this),this.widget.bind(n,this._change)},change:function(){this.bindings[m].set(this.value())},refresh:function(){this.widget.check(this.bindings[m].get()===!0)},value:function(){var a=this.element,b=a.value;if(b=="on"||b=="off")b=a.checked;return b},destroy:function(){this.widget.unbind(n,this._change)}}),visible:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a},refresh:function(){var a=this.bindings.visible.get();this.widget.wrapper[0].style.display=a?"":"none"}}),invisible:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a},refresh:function(){var a=this.bindings.invisible.get();this.widget.wrapper[0].style.display=a?"none":""}}),enabled:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a},refresh:function(){this.widget.enable&&this.widget.enable(this.bindings.enabled.get())}}),disabled:r.extend({init:function(a,b,c){r.fn.init.call(this,a.element[0],b,c),this.widget=a},refresh:function(){this.widget.enable&&this.widget.enable(!this.bindings.disabled.get())}}),source:r.extend({init:function(a,b,c){var d=this;r.fn.init.call(d,a.element[0],b,c),d.widget=a,d._dataBinding=k(d.dataBinding,d),d._dataBound=k(d.dataBound,d),d._itemChange=k(d.itemChange,d)},itemChange:function(a){A(a.item[0],a.data,a.ns||c.ui)},dataBinding:function(){var a,b,c=this.widget,d=c.items();for(a=0,b=d.length;a<b;a++)D(d[a])},dataBound:function(a){var b,d,e=this.widget,f=e.items(),g=e.dataSource,h=g.view(),i=a.ns||c.ui,j=g.group()||[];if(f.length){j.length&&(h=u(h));for(b=0,d=h.length;b<d;b++)A(f[b],h[b],i)}},refresh:function(a){var b=this,d,e=b.widget;a=a||{},a.action||(b.destroy(),e.bind("dataBinding",b._dataBinding),e.bind("dataBound",b._dataBound),e.bind("itemChange",b._itemChange),e.dataSource instanceof c.data.DataSource&&(d=b.bindings.source.get(),d instanceof c.data.DataSource?e.setDataSource(d):e.dataSource.data(d)))},destroy:function(){var a=this.widget;a.unbind("dataBinding",this._dataBinding),a.unbind("dataBound",this._dataBound),a.unbind("itemChange",this._itemChange)}}),value:r.extend({init:function(b,c,d){r.fn.init.call(this,b.element[0],c,d),this.widget=b,this._change=a.proxy(this.change,this),this.widget.first(n,this._change);var f=this.bindings.value.get();this._valueIsObservableObject=f==null||f instanceof e},change:function(){var a=this.widget.value(),b,d,e=this.options.dataValueField||this.options.dataTextField;if(e){var f,g=this._valueIsObservableObject;this.bindings.source&&(f=this.bindings.source.get());if(a===""&&g)a=null;else{if(!f||f instanceof c.data.DataSource)f=this.widget.dataSource.view();for(b=0,d=f.length;b<d;b++)if(f[b].get(e)==a){g?a=f[b]:a=f[b].get(e);break}}}this.bindings.value.set(a)},refresh:function(){var a=this.options.dataValueField||this.options.dataTextField,b=this.bindings.value.get();a&&b instanceof e&&(b=b.get(a)),this.widget.value(b)},destroy:function(){this.widget.unbind(n,this._change)}})};var s=i.extend({init:function(a,b){this.target=a,this.options=b,this.toDestroy=[]},bind:function(a){var b=this.target.nodeName.toLowerCase(),c,d=h[b]||{};for(c in a)this.applyBinding(c,a,d)},applyBinding:function(a,b,c){var d=c[a]||h[a],e=this.toDestroy,f,g=b[a];if(d){d=new d(this.target,b,this.options),e.push(d);if(g instanceof o)d.bind(g),e.push(g);else for(f in g)d.bind(g,f),e.push(g[f])}else if(a!=="template")throw new Error("The "+a+" binding is not supported by the "+this.target.nodeName.toLowerCase()+" element")},destroy:function(){var a,b,c=this.toDestroy;for(a=0,b=c.length;a<b;a++)c[a].destroy()}}),t=s.extend({bind:function(a){var b=this,c,d=!1,e=!1;for(c in a)c==l?d=!0:c=="source"?e=!0:b.applyBinding(c,a);e&&b.applyBinding("source",a),d&&b.applyBinding(l,a)},applyBinding:function(a,b){var c=h.widget[a],d=this.toDestroy,e,f=b[a];if(!c)throw new Error("The "+a+" binding is not supported by the "+this.target.options.name+" widget");c=new c(this.target,b,this.target.options),d.push(c);if(f instanceof o)c.bind(f),d.push(f);else for(e in f)c.bind(f,e),d.push(f[e])}}),w=/[A-Za-z0-9_\-]+:(\{([^}]*)\}|[^,}]+)/g,x=/\s/g;c.unbind=E,c.bind=B,c.data.binders=h,c.data.Binder=r,c.notify=F,c.observable=function(a){a instanceof e||(a=new e(a));return a}}(jQuery),function(a,b){function u(a){return a.replace(/&amp/g,"&amp;").replace(/&quot;/g,'"').replace(/&#39;/g,"'").replace(/&lt;/g,"<").replace(/&gt;/g,">")}function t(b){var d=c.ui.validator.ruleResolvers||{},e={},f;for(f in d)a.extend(!0,e,d[f].resolve(b));return e}var c=window.kendo,d=c.ui.Widget,e="k-invalid-msg",f="k-invalid",g=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,h=/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,i=":input:not(:button,[type=submit],[type=reset])",j="[type=number],[type=range]",k="blur",l="name",m="form",n="novalidate",o=a.proxy,p=function(a,b){typeof b=="string"&&(b=new RegExp("^(?:"+b+")$"));return b.test(a)},q=function(a,b,c){var d=a.val();if(a.filter(b).length&&d!=="")return p(d,c);return!0},r=function(a,c){if(a.length)return a[0].attributes[c]!==b;return!1},s=/(\[|\]|\$|\.|\:|\+)/g;c.ui.validator||(c.ui.validator={rules:{},messages:{}});var v=d.extend({init:function(b,e){var f=this,g=t(b);e=e||{},e.rules=a.extend({},c.ui.validator.rules,g.rules,e.rules),e.messages=a.extend({},c.ui.validator.messages,g.messages,e.messages),d.fn.init.call(f,b,e),f._errorTemplate=c.template(f.options.errorTemplate),f.element.is(m)&&f.element.attr(n,n),f._errors={},f._attachEvents()},options:{name:"Validator",errorTemplate:'<span class="k-widget k-tooltip k-tooltip-validation"><span class="k-icon k-warning"> </span> ${message}</span>',messages:{required:"{0} is required",pattern:"{0} is not valid",min:"{0} should be greater than or equal to {1}",max:"{0} should be smaller than or equal to {1}",step:"{0} is not valid",email:"{0} is not valid email",url:"{0} is not valid URL",date:"{0} is not valid date"},rules:{required:function(a){var b=a.filter("[type=checkbox]").length&&a.attr("checked")!=="checked",c=a.val();return!r(a,"required")||c!==""&&!!c&&!b},pattern:function(a){if(a.filter("[type=text],[type=email],[type=url],[type=tel],[type=search],[type=password]").filter("[pattern]").length&&a.val()!=="")return p(a.val(),a.attr("pattern"));return!0},min:function(a){if(a.filter(j+",["+c.attr("type")+"=number]").filter("[min]").length&&a.val()!==""){var b=parseFloat(a.attr("min"))||0,d=parseFloat(a.val());return b<=d}return!0},max:function(a){if(a.filter(j+",["+c.attr("type")+"=number]").filter("[max]").length&&a.val()!==""){var b=parseFloat(a.attr("max"))||0,d=parseFloat(a.val());return b>=d}return!0},step:function(a){if(a.filter(j+",["+c.attr("type")+"=number]").filter("[step]").length&&a.val()!==""){var b=parseFloat(a.attr("min"))||0,d=parseFloat(a.attr("step"))||0,e=parseFloat(a.val());return(e-b)*10%(d*10)/100===0}return!0},email:function(a){return q(a,"[type=email],["+c.attr("type")+"=email]",g)},url:function(a){return q(a,"[type=url],["+c.attr("type")+"=url]",h)},date:function(a){if(a.filter("[type^=date],["+c.attr("type")+"=date]").length&&a.val()!=="")return c.parseDate(a.val(),a.attr(c.attr("format")))!==null;return!0}},validateOnBlur:!0},_submit:function(a){if(!this.validate()){a.stopPropagation(),a.stopImmediatePropagation(),a.preventDefault();return!1}return!0},_attachEvents:function(){var b=this;b.element.is(m)&&b.element.submit(o(b._submit,b)),b.options.validateOnBlur&&(b.element.is(i)?b.element.bind(k,function(){b.validateInput(b.element)}):b.element.delegate(i,k,function(){b.validateInput(a(this))}))},validate:function(){var a=this,b,c,d=!1,e;a._errors={};if(!a.element.is(i)){b=a.element.find(i);for(c=0,e=b.length;c<e;c++)a.validateInput(b.eq(c))||(d=!0);return!d}return a.validateInput(a.element)},validateInput:function(b){b=a(b);var c=this,d=c._errorTemplate,g=c._checkValidity(b),h=g.valid,i="."+e,j=b.attr(l)||"",k=c._findMessageContainer(j).add(b.next(i)).hide(),m;if(!h){m=c._extractMessage(b,g.key),c._errors[j]=m;var n=a(d({message:u(m)}));c._decorateMessageContainer(n,j),k.replaceWith(n).length||n.insertAfter(b),n.show()}b.toggleClass(f,!h);return h},_findMessageContainer:function(a){var b=c.ui.validator.messageLocators,d,f=this.element.find("."+e+"["+c.attr("for")+"="+a.replace(s,"\\$1")+"]");for(d in b)f=f.add(b[d].locate(this.element,a));return f},_decorateMessageContainer:function(a,b){var d=c.ui.validator.messageLocators,f;a.addClass(e).attr(c.attr("for"),b||"");for(f in d)d[f].decorate(a,b)},_extractMessage:function(b,d){var e=this,f=e.options.messages[d],g=b.attr(l);f=a.isFunction(f)?f(b):f;return c.format(b.attr(c.attr(d+"-msg"))||b.attr("validationMessage")||b.attr("title")||f||"",g,b.attr(d))},_checkValidity:function(a){var b=this.options.rules,c;for(c in b)if(!b[c](a))return{valid:!1,key:c};return{valid:!0}},errors:function(){var a=[],b=this._errors,c;for(c in b)a.push(b[c]);return a}});c.ui.plugin(v)}(jQuery),function(a,b){function P(b){b.preventDefault();var c=a(b.target),d=c.closest(".k-widget").parent();d[0]||(d=c.parent()),d.trigger(b.type)}function O(a,b){return a.replace(/ /g,b+" ")}function N(a,b){var c=a.offset(),d=c.left+L(a,"borderLeftWidth")+L(a,"paddingLeft"),e=c.top+L(a,"borderTopWidth")+L(a,"paddingTop"),f=d+a.width()-b.outerWidth(!0),g=e+a.height()-b.outerHeight(!0);return{x:{min:d,max:f},y:{min:e,max:g}}}function M(a,b){return Math.min(Math.max(a,b.min),b.max)}function L(a,b){return parseInt(a.css(b),10)||0}function K(a){return f.elementFromPoint(a.x.client,a.y.client)}function J(b,c){try{return a.contains(b,c)||b==c}catch(d){return!1}}var c=window.kendo,d=c.support,e=d.pointers,f=window.document,g=a(f.documentElement),h=c.Class,i=c.ui.Widget,j=c.Observable,k=a.proxy,l=a.now,m=a.extend,n=c.getOffset,o={},p={},q,r=d.mobileOS&&d.mobileOS.android,s="mousedown",t="mousemove",u="mouseup mouseleave",v="keyup",w="change",x="dragstart",y="drag",z="dragend",A="dragcancel",B="dragenter",C="dragleave",D="drop",E="start",F="move",G="end",H="cancel",I="tap";d.touch&&(s="touchstart",t="touchmove",u="touchend touchcancel"),e&&(s="MSPointerDown",t="MSPointerMove",u="MSPointerUp MSPointerCancel");var Q=h.extend({init:function(a){this.axis=a},start:function(a,b){var c=this,d=a["page"+c.axis];c.startLocation=c.location=d,c.client=a["client"+c.axis],c.velocity=c.delta=0,c.timeStamp=b},move:function(a,b){var c=this,d=a["page"+c.axis];if(!!d||!r)c.delta=d-c.location,c.location=d,c.client=a["client"+c.axis],c.initialDelta=d-c.startLocation,c.velocity=c.delta/(b-c.timeStamp),c.timeStamp=b}}),R=j.extend({init:function(b,f){var h=this,i={},l,n,o="."+c.guid();f=f||{},l=h.filter=f.filter,h.threshold=f.threshold||0,b=a(b),j.fn.init.call(h),i[O(t,o)]=k(h._move,h),i[O(u,o)]=k(h._end,h),m(h,{x:new Q("X"),y:new Q("Y"),element:b,surface:f.global?g:f.surface||b,stopPropagation:f.stopPropagation,pressed:!1,eventMap:i,ns:o}),b.on(s,l,k(h._start,h)).on("dragstart",l,c.preventDefault),e&&b.css("-ms-touch-action","pinch-zoom double-tap-zoom");if(!f.allowSelection){var p=["mousedown selectstart",l,P];l instanceof a&&p.splice(2,0,null),b.on.apply(b,p)}d.eventCapture&&(n=function(a){h.moved&&a.preventDefault()},h.surface[0].addEventListener(d.mouseup,n,!0)),h.bind([I,E,F,G,H],f)},capture:function(){R.captured=!0},cancel:function(){this._cancel(),this.trigger(H)},skip:function(){this._cancel()},_cancel:function(){var a=this;a.moved=a.pressed=!1,a.surface.off(a.ns)},_start:function(b){var c=this,f=c.filter,g=b.originalEvent,h,i=b;if(!c.pressed){f?c.target=a(b.target).is(f)?a(b.target):a(b.target).closest(f):c.target=c.element;if(!c.target.length)return;c.currentTarget=b.currentTarget,c.stopPropagation&&b.stopPropagation(),c.pressed=!0,c.moved=!1,c.startTime=null,d.touch&&(h=g.changedTouches[0],c.touchID=h.identifier,i=h),e&&(c.touchID=g.pointerId,i=g),c._perAxis(E,i,l()),c.surface.off(c.eventMap).on(c.eventMap),R.captured=!1}},_move:function(a){var b=this,c,d,e;!b.pressed||b._withEvent(a,function(f){b._perAxis(F,f,l());if(!b.moved){c=b.x.initialDelta,d=b.y.initialDelta,e=Math.sqrt(c*c+d*d);if(e<=b.threshold)return;if(!R.captured)b.startTime=l(),b._trigger(E,a),b.moved=!0;else return b._cancel()}b.pressed&&b._trigger(F,a)})},_end:function(a){var b=this;!b.pressed||b._withEvent(a,function(){b.moved?(b.endTime=l(),b._trigger(G,a),b.moved=!1):b._trigger(I,a),b._cancel()})},_perAxis:function(a,b,c){this.x[a](b,c),this.y[a](b,c)},_trigger:function(a,b){var c={x:this.x,y:this.y,target:this.target,event:b};this.trigger(a,c)&&b.preventDefault()},_withEvent:function(a,b){var c=this,f=c.touchID,g=a.originalEvent,h,i;if(d.touch){h=g.changedTouches,i=h.length;while(i){i--;if(h[i].identifier===f)return b(h[i])}}else{if(!e)return b(a);if(f===g.pointerId)return b(g)}}}),S=j.extend({init:function(b,c){var d=this,e=b[0];d.capture=!1,e.addEventListener(s,k(d._press,d),!0),a.each(u.split(" "),function(){e.addEventListener(this,k(d._release,d),!0)}),j.fn.init.call(d),d.bind(["press","release"],c||{})},_press:function(a){var b=this;b.trigger("press"),b.capture&&a.preventDefault()},_release:function(a){var b=this;b.trigger("release"),b.capture&&(a.preventDefault(),b.cancelCapture())},captureNext:function(){this.capture=!0},cancelCapture:function(){this.capture=!1}}),T=j.extend({init:function(b){var c=this;j.fn.init.call(c),a.extend(c,b),c.max=0,c.horizontal?(c.measure="width",c.scrollSize="scrollWidth",c.axis="x"):(c.measure="height",c.scrollSize="scrollHeight",c.axis="y")},outOfBounds:function(a){return a>this.max||a<this.min},present:function(){return this.max-this.min},getSize:function(){return this.container[this.measure]()},getTotal:function(){return this.element[0][this.scrollSize]},update:function(a){var b=this;b.size=b.getSize(),b.total=b.getTotal(),b.min=Math.min(b.max,b.size-b.total),a||b.trigger(w,b)}}),U=j.extend({init:function(a){var b=this,d=k(b.refresh,b);j.fn.init.call(b),b.x=new T(m({horizontal:!0},a)),b.y=new T(m({horizontal:!1},a)),b.bind(w,a),c.onResize(d)},present:function(){return this.x.present()||this.y.present()},refresh:function(){this.x.update(),this.y.update(),this.trigger(w)}}),V=j.extend({init:function(a){var b=this;m(b,a),j.fn.init.call(b)},dragMove:function(a){var b=this,c=b.dimension,d=b.axis,e=b.movable,f=e[d]+a;if(!!c.present()){if(f<c.min&&a<0||f>c.max&&a>0)a*=b.resistance;e.translateAxis(d,a),b.trigger(w,b)}}}),W=h.extend({init:function(a){var b=this,c,d,e;m(b,{elastic:!0},a),e=b.elastic?.5:0,b.x=c=new V({axis:"x",dimension:b.dimensions.x,resistance:e,movable:b.movable}),b.y=d=new V({axis:"y",dimension:b.dimensions.y,resistance:e,movable:b.movable}),b.drag.bind(["move","end"],{move:function(a){c.dimension.present()||d.dimension.present()?(c.dragMove(a.x.delta),d.dragMove(a.y.delta),a.preventDefault()):b.drag.skip()},end:function(a){a.preventDefault()}})}}),X=d.transitions.prefix+"Transform",Y=Math.round,Z;d.hasHW3D?Z=function(a,b){return"translate3d("+Y(a)+"px,"+Y(b)+"px,0)"}:Z=function(a,b){return"translate("+Y(a)+"px,"+Y(b)+"px)"};var $=j.extend({init:function(b){var c=this;j.fn.init.call(c),c.element=a(b),c.x=0,c.y=0,c._saveCoordinates(Z(c.x,c.y))},translateAxis:function(a,b){this[a]+=b,this.refresh()},translate:function(a){this.x+=a.x,this.y+=a.y,this.refresh()},moveAxis:function(a,b){this[a]=b,this.refresh()},moveTo:function(a){m(this,a),this.refresh()},refresh:function(){var a=this,b=Z(a.x,a.y);b!=a.coordinates&&(a.element[0].style[X]=b,a._saveCoordinates(b),a.trigger(w))},_saveCoordinates:function(a){this.coordinates=a}}),_=i.extend({init:function(a,b){var c=this;i.fn.init.call(c,a,b);var d=c.options.group;d in p?p[d].push(c):p[d]=[c]},events:[B,C,D],options:{name:"DropTarget",group:"default"},_trigger:function(a,b){var c=this,d=o[c.options.group];if(d)return c.trigger(a,m({},b.event,{draggable:d}))},_over:function(a){this._trigger(B,a)},_out:function(a){this._trigger(C,a)},_drop:function(a){var b=this,c=o[b.options.group];c&&(c.dropped=!b._trigger(D,a))}}),ba=i.extend({init:function(a,b){var d=this;i.fn.init.call(d,a,b),d.drag=new R(d.element,{global:!0,stopPropagation:!0,filter:d.options.filter,threshold:d.options.distance,start:k(d._start,d),move:k(d._drag,d),end:k(d._end,d),cancel:k(d._cancel,d)}),d.destroy=k(d._destroy,d),d.captureEscape=function(a){a.keyCode===c.keys.ESC&&(d._trigger(A,{event:a}),d.drag.cancel())}},events:[x,y,z,A],options:{name:"Draggable",distance:5,group:"default",cursorOffset:null,axis:null,container:null,dropped:!1},_start:function(b){var c=this,d=c.options,e=d.container,g=d.hint;c.currentTarget=c.drag.target,c.currentTargetOffset=n(c.currentTarget);if(g){c.hint=a.isFunction(g)?a(g(c.currentTarget)):g;var h=n(c.currentTarget);c.hintOffset=h,c.hint.css({position:"absolute",zIndex:2e4,left:h.left,top:h.top}).appendTo(f.body)}o[d.group]=c,c.dropped=!1,e&&(c.boundaries=N(e,c.hint)),c._trigger(x,b)&&(c.drag.cancel(),c.destroy()),a(f).on(v,c.captureEscape)},updateHint:function(b){var c=this,d,e=c.options,f=c.boundaries,g=e.axis,h=c.options.cursorOffset;h?d={left:b.x.location+h.left,top:b.y.location+h.top}:(c.hintOffset.left+=b.x.delta,c.hintOffset.top+=b.y.delta,d=a.extend({},c.hintOffset)),f&&(d.top=M(d.top,f.y),d.left=M(d.left,f.x)),g==="x"?delete d.top:g==="y"&&delete d.left,c.hint.css(d)},_drag:function(a){var b=this;a.preventDefault(),b._withDropTarget(a,function(b){if(!b)q&&(q._trigger(C,a),q=null);else{if(q){if(b.element[0]===q.element[0])return;q._trigger(C,a)}b._trigger(B,a),q=b}}),b._trigger(y,a),b.hint&&b.updateHint(a)},_end:function(a){var b=this;b._withDropTarget(a,function(b){b&&(b._drop(a),q=null)}),b._trigger(z,a),b._cancel(a.event)},_cancel:function(a){var b=this;b.hint&&!b.dropped?b.hint.animate(b.currentTargetOffset,"fast",b.destroy):b.destroy()},_trigger:function(a,b){var c=this;return c.trigger(a,m({},b.event,{x:b.x,y:b.y,currentTarget:c.currentTarget}))},_withDropTarget:function(a,b){var c=this,d,e,f,g=c.options,h=p[g.group],i=0,j=h&&h.length;if(j){d=K(a),c.hint&&J(c.hint,d)&&(c.hint.hide(),d=K(a),c.hint.show());outer:while(d){for(i=0;i<j;i++){e=h[i];if(e.element[0]===d){f=e;break outer}}d=d.parentNode}b(f)}},_destroy:function(){var b=this;b.hint&&b.hint.remove(),delete o[b.options.group],b.trigger("destroy"),a(f).off(v,b.captureEscape)}});c.ui.plugin(_),c.ui.plugin(ba),c.Drag=R,c.Tap=S,c.containerBoundaries=N,m(c.ui,{Pane:W,PaneDimensions:U,Movable:$})}(jQuery),function(a,b){var c=window.kendo,d=c.mobile,e=c.fx,f=d.ui,g=a.proxy,h=a.extend,i=f.Widget,j=c.Class,k=c.ui.Movable,l=c.ui.Pane,m=c.ui.PaneDimensions,n=e.Transition,o=e.Animation,p=500,q=.7,r=.93,s=.5,t="km-scroller-release",u="km-scroller-refresh",v="pull",w="change",x="resize",y="scroll",z=o.extend({init:function(a){var b=this;o.fn.init.call(b),h(b,a,{transition:new n({axis:a.axis,movable:a.movable,onEnd:function(){b._end()}})}),b.tap.bind("press",function(){b.cancel()}),b.drag.bind("end",g(b.start,b)),b.drag.bind("tap",g(b.onEnd,b))},onCancel:function(){this.transition.cancel()},freeze:function(a){var b=this;b.cancel(),b._moveTo(a)},onEnd:function(){var a=this;a._outOfBounds()?a._snapBack():a._end()},done:function(){return Math.abs(this.velocity)<1},start:function(){var a=this;!a.dimension.present()||(a._outOfBounds()?a._snapBack():(a.velocity=a.drag[a.axis].velocity*16,a.velocity&&(a.tap.captureNext(),o.fn.start.call(a))))},tick:function(){var a=this,b=a.dimension,c=a._outOfBounds()?s:r,d=a.velocity*=c,e=a.movable[a.axis]+d;!a.elastic&&b.outOfBounds(e)&&(e=Math.max(Math.min(e,b.max),b.min),a.velocity=0),a.movable.moveAxis(a.axis,e)},_end:function(){this.tap.cancelCapture(),this.end()},_outOfBounds:function(){return this.dimension.outOfBounds(this.movable[this.axis])},_snapBack:function(){var a=this,b=a.dimension,c=a.movable[a.axis]>b.max?b.max:b.min;a._moveTo(c)},_moveTo:function(a){this.transition.moveTo({location:a,duration:p,ease:n.easeOutExpo})}}),A=j.extend({init:function(b){var c=this,d=b.axis==="x",e=a('<div class="km-touch-scrollbar km-'+(d?"horizontal":"vertical")+'-scrollbar" />');h(c,b,{element:e,elementSize:0,movable:new k(e),scrollMovable:b.movable,size:d?"width":"height"}),c.scrollMovable.bind(w,g(c._move,c)),c.container.append(e)},_move:function(){var a=this,b=a.axis,c=a.dimension,d=c.size,e=a.scrollMovable,f=d/c.total,g=Math.round(-e[b]*f),h=Math.round(d*f);g+h>d?h=d-g:g<0&&(h+=g,g=0),a.elementSize!=h&&(a.element.css(a.size,h+"px"),a.elementSize=h),a.movable.moveAxis(b,g)},show:function(){this.element.css({opacity:q,visibility:"visible"})},hide:function(){this.element.css({opacity:0})}}),B=i.extend({init:function(b,d){var e=this;i.fn.init.call(e,b,d),b=e.element,b.css("overflow","hidden").addClass("km-scroll-wrapper").wrapInner('<div class="km-scroll-container"/>').prepend('<div class="km-scroll-header"/>');var f=b.children().eq(1),g=new c.Tap(b),j=new k(f),n=new m({element:f,container:b,change:function(){e.trigger(x)}}),o=new c.Drag(b,{allowSelection:!0,start:function(a){n.refresh(),n.present()?o.capture():o.cancel()}}),p=new l({movable:j,dimensions:n,drag:o,elastic:e.options.elastic});j.bind(w,function(){e.scrollTop=-j.y,e.scrollLeft=-j.x,e.trigger(y,{scrollTop:e.scrollTop,scrollLeft:e.scrollLeft})}),h(e,{movable:j,dimensions:n,drag:o,pane:p,tap:g,pulled:!1,scrollElement:f,fixedContainer:b.children().first()}),e._initAxis("x"),e._initAxis("y"),n.refresh(),e.options.pullToRefresh&&e._initPullToRefresh(),c.onResize(a.proxy(e.reset,e))},scrollHeight:function(){return this.scrollElement[0].scrollHeight},scrollWidth:function(){return this.scrollElement[0].scrollWidth},options:{name:"Scroller",pullOffset:140,elastic:!0,pullTemplate:"Pull to refresh",releaseTemplate:"Release to refresh",refreshTemplate:"Refreshing"},events:[v,y,x],setOptions:function(a){var b=this;i.fn.setOptions.call(b,a),a.pullToRefresh&&b._initPullToRefresh()},reset:function(){this.movable.moveTo({x:0,y:0})},scrollTo:function(a,b){this.movable.moveTo({x:a,y:b})},pullHandled:function(){var a=this;a.refreshHint.removeClass(u),a.hintContainer.html(a.pullTemplate({})),a.yinertia.onEnd(),a.xinertia.onEnd()},_initPullToRefresh:function(){var a=this;a.pullTemplate=c.template(a.options.pullTemplate),a.releaseTemplate=c.template(a.options.releaseTemplate),a.refreshTemplate=c.template(a.options.refreshTemplate),a.scrollElement.prepend('<span class="km-scroller-pull"><span class="km-icon"></span><span class="km-template">'+a.pullTemplate({})+"</span></span>"),a.refreshHint=a.scrollElement.children().first(),a.hintContainer=a.refreshHint.children(".km-template"),a.pane.y.bind("change",g(a._paneChange,a)),a.drag.bind("end",g(a._dragEnd,a))},_dragEnd:function(){var a=this;!a.pulled||(a.pulled=!1,a.refreshHint.removeClass(t).addClass(u),a.hintContainer.html(a.refreshTemplate({})),a.trigger("pull"),a.yinertia.freeze(a.options.pullOffset/2))},_paneChange:function(){var a=this;a.movable.y/s>a.options.pullOffset?a.pulled||(a.pulled=!0,a.refreshHint.removeClass(u).addClass(t),a.hintContainer.html(a.releaseTemplate({}))):a.pulled&&(a.pulled=!1,a.refreshHint.removeClass(t),a.hintContainer.html(a.pullTemplate({})))},_initAxis:function(a){var b=this,c=b.movable,d=b.dimensions[a],e=b.tap,f=new A({axis:a,movable:c,dimension:d,container:b.element}),g=new z({axis:a,movable:c,tap:e,drag:b.drag,dimension:d,elastic:b.options.elastic,end:function(){f.hide()}});b[a+"inertia"]=g,b.pane[a].bind(w,function(){f.show()})}});f.plugin(B)}(jQuery),function(a,b){function j(a){return a.position().top+3}var c=window.kendo,d=c.ui.Widget,e=a.proxy,f=c.template('<div class="k-group-indicator" data-#=data.ns#field="${data.field}" data-#=data.ns#title="${data.title || ""}" data-#=data.ns#dir="${data.dir || "asc"}"><a href="\\#" class="k-link"><span class="k-icon k-si-arrow-${(data.dir || "asc") == "asc" ? "n" : "s"}">(sorted ${(data.dir || "asc") == "asc" ? "ascending": "descending"})</span>${data.title ? data.title: data.field}</a><a class="k-button k-button-icon k-button-bare"><span class="k-icon k-group-delete"></span></a></div>',{useWithBlock:!1}),g=function(b){return a('<div class="k-header k-drag-clue" />').css({width:b.width(),paddingLeft:b.css("paddingLeft"),paddingRight:b.css("paddingRight"),lineHeight:b.height()+"px",paddingTop:b.css("paddingTop"),paddingBottom:b.css("paddingBottom")}).html(b.attr(c.attr("title"))||b.attr(c.attr("field"))).prepend('<span class="k-icon k-drag-status k-denied" />')},h=a('<div class="k-grouping-dropclue"/>'),i=/(\[|\]|\$|\.|\:|\+)/g,k=d.extend({init:function(b,f){var i=this,k,l=c.guid(),m=e(i._intializePositions,i),n,o=i._dropCuePositions=[];d.fn.init.call(i,b,f),n=i.options.draggable||new c.ui.Draggable(i.element,{filter:i.options.filter,hint:g,group:l}),k=i.groupContainer=a(i.options.groupContainer,i.element).kendoDropTarget({group:n.options.group,dragenter:function(a){i._canDrag(a.draggable.currentTarget)&&(a.draggable.hint.find(".k-drag-status").removeClass("k-denied").addClass("k-add"),h.css({top:j(k),left:0}).appendTo(k))},dragleave:function(a){a.draggable.hint.find(".k-drag-status").removeClass("k-add").addClass("k-denied"),h.remove()},drop:function(b){var d=b.draggable.currentTarget,e=d.attr(c.attr("field")),f=d.attr(c.attr("title")),g=i.indicator(e),j=i._dropCuePositions,k=j[j.length-1],l;if(!!d.hasClass("k-group-indicator")||!!i._canDrag(d))k?(l=i._dropCuePosition(h.offset().left+parseInt(k.element.css("marginLeft"),10)+parseInt(k.element.css("marginRight"),10)),l&&i._canDrop(a(g),l.element,l.left)&&(l.before?l.element.before(g||i.buildIndicator(e,f)):l.element.after(g||i.buildIndicator(e,f)),i._change())):(i.groupContainer.append(i.buildIndicator(e,f)),i._change())}}).kendoDraggable({filter:"div.k-group-indicator",hint:g,group:n.options.group,dragcancel:e(i._dragCancel,i),dragstart:function(a){var b=a.currentTarget,c=parseInt(b.css("marginLeft"),10),d=b.position().left-c;m(),h.css({top:j(k),left:d}).appendTo(k),this.hint.find(".k-drag-status").removeClass("k-denied").addClass("k-add")},dragend:function(){i._dragEnd(this)},drag:e(i._drag,i)}).delegate(".k-button","click",function(b){b.preventDefault(),i._removeIndicator(a(this).parent())}).delegate(".k-link","click",function(b){var d=a(this).parent(),e=i.buildIndicator(d.attr(c.attr("field")),d.attr(c.attr("title")),d.attr(c.attr("dir"))=="asc"?"desc":"asc");d.before(e).remove(),i._change(),b.preventDefault()}),n.bind(["dragend","dragcancel","dragstart","drag"],{dragend:function(){i._dragEnd(this)},dragcancel:e(i._dragCancel,i),dragstart:function(a){var b,c,d;!i.options.allowDrag&&!i._canDrag(a.currentTarget)?a.preventDefault():(m(),o.length?(b=o[o.length-1].element,c=parseInt(b.css("marginRight"),10),d=b.position().left+b.outerWidth()+c):d=0)},drag:e(i._drag,i)}),i.dataSource=i.options.dataSource,i.dataSource&&(i._refreshHandler=e(i.refresh,i),i.dataSource.bind("change",i._refreshHandler))},refresh:function(){var b=this,d=b.dataSource;b.groupContainer.empty().append(a.map(d.group()||[],function(a){var d=a.field.replace(i,"\\$1"),e=b.element.find(b.options.filter).filter("["+c.attr("field")+"="+d+"]");return b.buildIndicator(a.field,e.attr(c.attr("title")),a.dir)}).join("")),b._invalidateGroupContainer()},destroy:function(){var a=this;a.dataSource&&a._refreshHandler&&a.dataSource.unbind("change",a._refreshHandler)},options:{name:"Groupable",filter:"th",messages:{empty:"Drag a column header and drop it here to group by that column"}},indicator:function(b){var d=a(".k-group-indicator",this.groupContainer);return a.grep(d,function(d){return a(d).attr(c.attr("field"))===b})[0]},buildIndicator:function(a,b,d){return f({field:a,dir:d,title:b,ns:c.ns})},descriptors:function(){var b=this,d=a(".k-group-indicator",b.groupContainer),e,f,g,h,i;e=b.element.find(b.options.filter).map(function(){var b=a(this),d=b.attr(c.attr("aggregates")),e=b.attr(c.attr("field"));if(d&&d!==""){f=d.split(","),d=[];for(h=0,i=f.length;h<i;h++)d.push({field:e,aggregate:f[h]})}return d}).toArray();return a.map(d,function(b){b=a(b),g=b.attr(c.attr("field"));return{field:g,dir:b.attr(c.attr("dir")),aggregates:e||[]}})},_removeIndicator:function(a){var b=this;a.remove(),b._invalidateGroupContainer(),b._change()},_change:function(){var a=this;a.dataSource&&a.dataSource.group(a.descriptors())},_dropCuePosition:function(b){var c=this._dropCuePositions;if(!!h.is(":visible")&&c.length!==0){b=Math.ceil(b);var d=c[c.length-1],e=d.right,f=parseInt(d.element.css("marginLeft"),10),g=parseInt(d.element.css("marginRight"),10);b>=e?b={left:d.element.position().left+d.element.outerWidth()+g,element:d.element,before:!1}:(b=a.grep(c,function(a){return a.left<=b&&b<=a.right})[0],b&&(b={left:b.element.position().left-f,element:b.element,before:!0}));return b}},_drag:function(a){var b=c.touchLocation(a),d=this._dropCuePosition(b.x);d&&h.css({left:d.left})},_canDrag:function(a){return a.attr(c.attr("groupable"))!="false"&&(a.hasClass("k-group-indicator")||!this.indicator(a.attr(c.attr("field"))))},_canDrop:function(a,b,c){var d=a.next();return a[0]!==b[0]&&(!d[0]||b[0]!==d[0]||c>d.position().left)},_dragEnd:function(b){var d=this,e=b.currentTarget.attr(c.attr("field")),f=d.indicator(e);b!==d.options.draggable&&!b.dropped&&f&&d._removeIndicator(a(f)),d._dragCancel()},_dragCancel:function(){h.remove(),this._dropCuePositions=[]},_intializePositions:function(){var b=this,c=a(".k-group-indicator",b.groupContainer),d;b._dropCuePositions=a.map(c,function(b){b=a(b),d=b.offset().left;return{left:parseInt(d,10),right:parseInt(d+b.outerWidth(),10),element:b}})},_invalidateGroupContainer:function(){var a=this.groupContainer;a.is(":empty")&&a.html(this.options.messages.empty)}});c.ui.plugin(k)}(jQuery),function(a,b){function g(b,c){b=a(b),c?b.find(".k-drag-status").removeClass("k-add").addClass("k-denied"):b.find(".k-drag-status").removeClass("k-denied").addClass("k-add")}var c=window.kendo,d=c.ui.Widget,e="change",f="k-reorderable",h=d.extend({init:function(b,h){var i=this,j,k=c.guid()+"-reorderable";d.fn.init.call(i,b,h),b=i.element.addClass(f),h=i.options,j=h.draggable||new c.ui.Draggable(b,{group:k,filter:h.filter,hint:h.hint}),i.reorderDropCue=a('<div class="k-reorder-cue"><div class="k-icon k-i-arrow-s"></div><div class="k-icon k-i-arrow-n"></div></div>'),b.find(j.options.filter).kendoDropTarget({group:j.options.group,dragenter:function(a){if(!!i._draggable){var c=this.element,d=c[0]===i._draggable[0];g(a.draggable.hint,d),d||i.reorderDropCue.css({height:c.outerHeight(),top:b.offset().top,left:c.offset().left+(c.index()>i._draggable.index()?c.outerWidth():0)}).appendTo(document.body)}},dragleave:function(a){g(a.draggable.hint,!0),i.reorderDropCue.remove()},drop:function(){if(!!i._draggable){var a=i._draggable[0],c=this.element[0],d;a!==c&&(d=b.find(j.options.filter),i.trigger(e,{element:i._draggable,oldIndex:d.index(a),newIndex:d.index(c)}))}}}),j.bind(["dragcancel","dragend","dragstart"],{dragcancel:function(){i.reorderDropCue.remove(),i._draggable=null},dragend:function(){i.reorderDropCue.remove(),i._draggable=null},dragstart:function(a){i._draggable=a.currentTarget}})},options:{name:"Reorderable",filter:"*"},events:[e]});c.ui.plugin(h)}(jQuery),function(a,b){var c=window.kendo,d=c.ui,e=d.Widget,f=a.proxy,g=a.isFunction,h=a.extend,i="horizontal",j="vertical",k="start",l="resize",m="resizeend",n=e.extend({init:function(a,b){var c=this;e.fn.init.call(c,a,b),c.orientation=c.options.orientation.toLowerCase()!=j?i:j,c._positionMouse=c.orientation==i?"x":"y",c._position=c.orientation==i?"left":"top",c._sizingDom=c.orientation==i?"outerWidth":"outerHeight",new d.Draggable(a,{distance:0,filter:b.handle,drag:f(c._resize,c),dragstart:f(c._start,c),dragend:f(c._stop,c)})},events:[l,m,k],options:{name:"Resizable",orientation:i},_max:function(a){var c=this,d=c.hint?c.hint[c._sizingDom]():0,e=c.options.max;return g(e)?e(a):e!==b?c._initialElementPosition+e-d:e},_min:function(a){var c=this,d=c.options.min;return g(d)?d(a):d!==b?c._initialElementPosition+d:d},_start:function(b){var c=this,d=c.options.hint,e=a(b.currentTarget);c._initialMousePosition=b[c._positionMouse].location,c._initialElementPosition=e.position()[c._position],d&&(c.hint=g(d)?a(d(e)):d,c.hint.css({position:"absolute"}).css(c._position,c._initialElementPosition).appendTo(c.element)),c.trigger(k,b),c._maxPosition=c._max(b),c._minPosition=c._min(b),a(document.body).css("cursor",e.css("cursor"))},_resize:function(c){var d=this,e=a(c.currentTarget),f=d._maxPosition,g=d._minPosition,i=d._initialElementPosition+(c[d._positionMouse].location-d._initialMousePosition),j;j=g!==b?Math.max(g,i):i,d.position=j=f!==b?Math.min(f,j):j,d.hint&&d.hint.toggleClass(d.options.invalidClass||"",j==f||j==g).css(d._position,j),d.trigger(l,h(c,{position:j}))},_stop:function(b){var c=this;c.hint&&c.hint.remove(),c.trigger(m,h(b,{position:c.position})),a(document.body).css("cursor","")}});c.ui.plugin(n)}(jQuery),function(a,b){var c=window.kendo,d=a.proxy,e="data-dir",f="asc",g="single",h="data-field",i="desc",j=".k-link",k=c.ui.Widget,l=k.extend({init:function(a,b){var c=this,e;k.fn.init.call(c,a,b),c.dataSource=c.options.dataSource.bind("change",d(c.refresh,c)),e=c.element.find(j),e[0]||(e=c.element.wrapInner('<a class="k-link" href="#"/>').find(j)),c.link=e,c.element.click(d(c._click,c))},options:{name:"Sortable",mode:g,allowUnsort:!0},refresh:function(){var b=this,c=b.dataSource.sort()||[],d,g,j,k,l=b.element,m=l.attr(h);l.removeAttr(e);for(d=0,g=c.length;d<g;d++)j=c[d],m==j.field&&l.attr(e,j.dir);k=l.attr(e),l.find(".k-i-arrow-n,.k-i-arrow-s").remove(),k===f?a('<span class="k-icon k-i-arrow-n" />').appendTo(b.link):k===i&&a('<span class="k-icon k-i-arrow-s" />').appendTo(b.link)},_click:function(a){var c=this,d=c.element,j=d.attr(h),k=d.attr(e),l=c.options,m=c.dataSource.sort()||[],n,o;k===f?k=i:k===i&&l.allowUnsort?k=b:k=f;if(l.mode===g)m=[{field:j,dir:k}];else if(l.mode==="multiple"){for(n=0,o=m.length;n<o;n++)if(m[n].field===j){m.splice(n,1);break}m.push({field:j,dir:k})}a.preventDefault(),c.dataSource.sort(m)}});c.ui.plugin(l)}(jQuery),function(a,b){var c=window.kendo,d=c.support.touch,e=c.ui.Widget,f=a.proxy,g=d?"touchend":"mouseup",h=d?"touchstart":"mousedown",i=d?"touchmove":"mousemove",j="k-state-selected",k="k-state-selecting",l="k-selectable",m="selectstart",n=a(document),o="change",p="k-state-unselecting",q=e.extend({init:function(b,c){var d=this;e.fn.init.call(d,b,c),d._marquee=a("<div class='k-marquee'></div>"),d._lastActive=null,d._moveDelegate=f(d._move,d),d._upDelegate=f(d._up,d),d.element.addClass(l),d.element.delegate("."+l+" "+d.options.filter,h,f(d._down,d))},events:[o],options:{name:"Selectable",filter:">*",multiple:!1},_collide:function(a,b){var c=a.offset(),d={left:c.left,top:c.top,right:c.left+a.outerWidth(),bottom:c.top+a.outerHeight()};return!(d.left>b.right||d.right<b.left||d.top>b.bottom||d.bottom<b.top)},_position:function(a){var b=this._originalPosition,c=b.x,d=b.y,e=a.pageX,f=a.pageY,g;c>e&&(g=e,e=c,c=g),d>f&&(g=f,f=d,d=g);return{top:d,right:e,left:c,bottom:f}},_down:function(b){var d=this,e,f=b.ctrlKey,h=b.shiftKey,o=!d.options.multiple;d._downTarget=a(b.currentTarget),d._shiftPressed=h;if(d._downTarget.closest("."+l)[0]===d.element[0]){n.unbind(g,d._upDelegate).bind(g,d._upDelegate),d._originalPosition={x:b.pageX,y:b.pageY},!o&&a(b.target).is(":not(:input, a)")&&(n.unbind(i,d._moveDelegate).bind(i,d._moveDelegate).unbind(m,!1).bind(m,!1),c.support.touch||b.preventDefault()),o||(a("body").append(d._marquee),d._marquee.css({left:b.clientX+1,top:b.clientY+1,width:0,height:0})),e=d._downTarget.hasClass(j),(o||!f&&!h)&&d.element.find(d.options.filter+"."+j).not(d._downTarget).removeClass(j),f&&(d._lastActive=d._downTarget);if(e&&(f||h))d._downTarget.addClass(j),h||d._downTarget.addClass(p);else if(!c.support.touch||!o)d._downTarget.addClass(k),o&&d._downTarget.hasClass(j)&&d._downTarget.removeClass(k)}},_move:function(b){var c=this,d=c._position(b),e=b.ctrlKey,f,g;c._marquee.css({left:d.left,top:d.top,width:d.right-d.left,height:d.bottom-d.top}),c.element.find(c.options.filter).each(function(){f=a(this),g=c._collide(f,d),g?f.hasClass(j)?c._downTarget[0]!==f[0]&&e&&f.removeClass(j).addClass(p):!f.hasClass(k)&&!f.hasClass(p)&&f.addClass(k):f.hasClass(k)?f.removeClass(k):e&&f.hasClass(p)&&f.removeClass(p).addClass(j)})},_up:function(a){var b=this,d=b.options,e=!d.multiple;n.unbind(m,!1).unbind(i,b._moveDelegate).unbind(g,b._upDelegate),e||b._marquee.remove(),c.support.touch&&e&&b._downTarget.addClass(k),!e&&b._shiftPressed===!0?b.selectRange(b._firstSelectee(),b._downTarget):(b.element.find(d.filter+"."+p).removeClass(p).removeClass(j),b.value(b.element.find(d.filter+"."+k))),b._shiftPressed||(b._lastActive=b._downTarget),b._downTarget=null,b._shiftPressed=!1},value:function(a){var b=this,c=f(b._selectElement,b);if(a)a.each(function(){c(this)}),b.trigger(o,{});else return b.element.find(b.options.filter+"."+j)},_firstSelectee:function(){var a=this,b;if(a._lastActive!==null)return a._lastActive;b=a.value();return b.length>0?b[0]:a.element.find(a.options.filter)},_selectElement:function(b){var c=a(b),d=this.trigger("select",{element:b});c.removeClass(k),d||c.addClass(j)},clear:function(){var a=this;a.element.find(a.options.filter+"."+j).removeClass(j)},selectRange:function(b,c){var d=this,e=!1,g=f(d._selectElement,d),h;b=a(b)[0],c=a(c)[0],d.element.find(d.options.filter).each(function(){h=a(this);if(e)g(this),e=this!==c;else if(this===b)e=b!==c,g(this);else if(this===c){var d=b;b=c,c=d,e=!0,g(this)}else h.removeClass(j)}),d.trigger(o,{})}});c.ui.plugin(q)}(jQuery),function(a,b){function v(a,b,c){r(a,h,c,b>=c)}function u(a,b,c){r(a,j,Math.min(c,b+1),b>=c)}function t(a,b,c){r(a,i,Math.max(1,b-1),b<=1)}function s(a,b,c){r(a,g,1,b<=1)}function r(a,b,d,e){a.find(b).parent().attr(c.attr("page"),d).toggleClass("k-state-disabled",e)}function q(a,b){return o({className:a.substring(1),text:b})}function p(a,b,d,e){return a({idx:b,text:d,ns:c.ns,numeric:e})}var c=window.kendo,d=c.ui,e=d.Widget,f=a.proxy,g=".k-i-seek-w",h=".k-i-seek-e",i=".k-i-arrow-w",j=".k-i-arrow-e",k="change",l="click",m="keydown",n="disabled",o=c.template('<a href="\\#" title="#=text#" class="k-link"><span class="k-icon #= className #">#=text#</span></a>'),w=e.extend({init:function(b,d){var n=this,o,p;e.fn.init.call(n,b,d),d=n.options,n.dataSource=d.dataSource,n.linkTemplate=c.template(n.options.linkTemplate),n.selectTemplate=c.template(n.options.selectTemplate),o=n.page(),p=n.totalPages(),n._refreshHandler=f(n.refresh,n),n.dataSource.bind(k,n._refreshHandler),d.previousNext&&(n.element.find(g).length||(n.element.append(q(g,d.messages.first)),s(n.element,o,p)),n.element.find(i).length||(n.element.append(q(i,d.messages.previous)),t(n.element,o,p))),d.numeric&&(n.list=n.element.find(".k-pager-numbers"),n.list.length||(n.list=a('<ul class="k-pager-numbers k-reset" />').appendTo(n.element))),d.input&&(n.element.find(".k-pager-input").length||n.element.append('<span class="k-pager-input k-label">'+d.messages.page+'<input class="k-textbox">'+c.format(d.messages.of,p)+"</span>"),n._keydownHandler=f(n._keydown,n),n.element.on(m,".k-pager-input input",n._keydownHandler)),d.previousNext&&(n.element.find(j).length||(n.element.append(q(j,d.messages.next)),u(n.element,o,p)),n.element.find(h).length||(n.element.append(q(h,d.messages.last)),v(n.element,o,p))),d.pageSizes&&(n.element.find(".k-pager-sizes").length||a('<span class="k-pager-sizes k-label"><select/>'+d.messages.itemsPerPage+"</span>").appendTo(n.element).find("select").html(a.map(a.isArray(d.pageSizes)?d.pageSizes:[5,10,20],function(a){return"<option>"+a+"</option>"}).join("")).end().appendTo(n.element),n.element.find(".k-pager-sizes select").val(n.pageSize()),c.ui.DropDownList&&n.element.find(".k-pager-sizes select").kendoDropDownList(),n._changeHandler=f(n._change,n),n.element.on(k,".k-pager-sizes select",n._changeHandler)),d.refresh&&(n.element.find(".k-pager-refresh").length||n.element.append('<a href="#" class="k-pager-refresh k-link"  title="'+d.messages.refresh+'"><span class="k-icon k-i-refresh">'+d.messages.refresh+"</span></a>"),n._reloadHandler=f(n._refreshClick,n),n.element.on(l,".k-pager-refresh",n._reloadHandler)),d.info&&(n.element.find(".k-pager-info").length||n.element.append('<span class="k-pager-info k-label" />')),n._clickHandler=f(n._click,n),n.element.on(l,"a",n._clickHandler),d.autoBind&&n.refresh()},destroy:function(){var a=this;a.element.off(l,"a",a._clickHandler),a.element.off(m,".k-pager-input input",a._keydownHandler),a.element.off(k,".k-pager-sizes select",a._changeHandler),a.element.off(l,".k-pager-refresh",a._reloadHandler),a.dataSource.unbind(k,a._refreshHandler)},events:[k],options:{name:"Pager",selectTemplate:'<li><span class="k-state-selected">#=text#</span></li>',linkTemplate:'<li><a href="\\#" class="k-link" data-#=ns#page="#=idx#">#=text#</a></li>',buttonCount:10,autoBind:!0,numeric:!0,info:!0,input:!1,previousNext:!0,pageSizes:!1,refresh:!1,messages:{display:"{0} - {1} of {2} items",empty:"No items to display",page:"Page",of:"of {0}",itemsPerPage:"items per page",first:"Go to the first page",previous:"Go to the previous page",next:"Go to the next page",last:"Go to the last page",refresh:"Refresh"}},refresh:function(){var a=this,b,d,e=1,f="",g,h=a.page(),i=a.options,j=a.pageSize(),k=a.dataSource.total(),l=a.totalPages(),m=a.linkTemplate,o=i.buttonCount;if(i.numeric){h>o&&(g=h%o,e=g===0?h-o+1:h-g+1),d=Math.min(e+o-1,l),e>1&&(f+=p(m,e-1,"...",!1));for(b=e;b<=d;b++)f+=p(b==h?a.selectTemplate:m,b,b,!0);d<l&&(f+=p(m,b,"...",!1)),f===""&&(f=a.selectTemplate({text:0})),a.list.html(f)}i.info&&(k>0?f=c.format(i.messages.display,(h-1)*j+1,Math.min(h*j,k),k):f=i.messages.empty,a.element.find(".k-pager-info").html(f)),i.input&&a.element.find(".k-pager-input").html(a.options.messages.page+'<input class="k-textbox">'+c.format(i.messages.of,l)).find("input").val(h).attr(n,k<1).toggleClass("k-state-disabled",k<1),i.previousNext&&(s(a.element,h,l),t(a.element,h,l),u(a.element,h,l),v(a.element,h,l)),i.pageSizes&&a.element.find(".k-pager-sizes select").val(j)},_keydown:function(a){if(a.keyCode===c.keys.ENTER){var b=this.element.find(".k-pager-input").find("input"),d=parseInt(b.val(),10);if(isNaN(d)||d<1||d>this.totalPages())d=this.page();b.val(d),this.page(d)}},_refreshClick:function(a){a.preventDefault(),this.dataSource.read()},_change:function(a){var b=parseInt(a.currentTarget.value,10);isNaN(b)||this.dataSource.pageSize(b)},_click:function(b){var d=a(b.currentTarget);b.preventDefault(),d.is(".k-state-disabled")||this.page(d.attr(c.attr("page")))},totalPages:function(){return Math.ceil((this.dataSource.total()||0)/this.pageSize())},pageSize:function(){return this.dataSource.pageSize()||this.dataSource.total()},page:function(a){if(a!==b)this.dataSource.page(a),this.trigger(k,{index:a});else return this.dataSource.total()>0?this.dataSource.page():0}});d.plugin(w)}(jQuery),function(a,b){function M(b,c){return b===c||a.contains(b,c)}var c=window.kendo,d=c.ui,e=d.Widget,f=c.support,g=f.touch,h=c.getOffset,i=a.browser.msie&&a.browser.version<9,j="open",k="close",l="deactivate",m="activate",n="center",o="left",p="right",q="top",r="bottom",s="absolute",t="hidden",u="body",v="location",w="position",x="visible",y="fitted",z="effects",A="k-state-active",B="k-state-border",C=".k-picker-wrap, .k-dropdown-wrap, .k-link",D=g?"touchstart":"mousedown",E=a(document),F=a(window),G=a(document.documentElement),H="resize scroll",I=f.transitions.css,J=I+"transform",K=a.extend,L=["font-family","font-size","font-stretch","font-style","font-weight","line-height"],N=e.extend({init:function(b,d){var f=this,g;e.fn.init.call(f,b,d),b=f.element,d=f.options,f.collisions=d.collision?d.collision.split(" "):[],f.collisions.length===1&&f.collisions.push(f.collisions[0]),g=a(f.options.anchor).closest(".k-popup,.k-group"),d.appendTo=a(a(d.appendTo)[0]||g[0]||u),f.element.hide().addClass("k-popup k-group k-reset").css({position:s}).appendTo(d.appendTo).bind("mouseenter mouseleave",function(a){f._hovered=a.type==="mouseenter"}),f.wrapper=a(),d.animation===!1&&(d.animation={open:{show:!0,effects:{}},close:{hide:!0,effects:{}}}),K(d.animation.open,{complete:function(){f.wrapper.css({overflow:x}),f.trigger(m)}}),K(d.animation.close,{complete:function(){f.wrapper.hide();var e=f.wrapper.data(v),g=a(d.anchor),h,i;e&&f.wrapper.css(e),d.anchor!=u&&(h=g.hasClass(B+"-down")?"down":"up",i=B+"-"+h,g.removeClass(i).children(C).removeClass(A).removeClass(i),b.removeClass(B+"-"+c.directions[h].reverse)),f._closing=!1,f.trigger(l)}}),f._mousedownProxy=function(a){f._mousedown(a)},f._currentWidth=E.width(),f._resizeProxy=function(a){f._resize(a)},d.toggleTarget&&a(d.toggleTarget).bind(d.toggleEvent,a.proxy(f.toggle,f))},events:[j,m,k,l],options:{name:"Popup",toggleEvent:"click",origin:r+" "+o,position:q+" "+o,anchor:u,collision:"flip fit",viewport:window,animation:{open:{effects:"slideIn:down",transition:!0,duration:200,show:!0},close:{duration:100,show:!1,hide:!0}}},open:function(b,d){var e=this,h={isFixed:!isNaN(parseInt(d,10)),x:b,y:d},i=e.element,k=e.options,l="down",m,n,o=a(k.anchor),p,r;if(!e.visible()){for(r=0;r<L.length;r++)p=L[r],i.css(p,o.css(p));if(i.data("animating")||e.trigger(j))return;G.unbind(D,e._mousedownProxy).bind(D,e._mousedownProxy),g||F.unbind(H,e._resizeProxy).bind(H,e._resizeProxy),e.wrapper=n=c.wrap(i).css({overflow:t,display:"block",position:s}),f.mobileOS.android&&n.add(o).css(J,"translatez(0)"),n.css(w),k.appendTo==u&&n.css(q,"-10000px"),m=K(!0,{},k.animation.open),e.flipped=e._position(h),m.effects=c.parseEffects(m.effects,e.flipped),l=m.effects.slideIn?m.effects.slideIn.direction:l;if(k.anchor!=u){var v=B+"-"+l;i.addClass(B+"-"+c.directions[l].reverse),o.addClass(v).children(C).addClass(A).addClass(v)}i.data(z,m.effects).kendoStop(!0).kendoAnimate(m)}},toggle:function(){var a=this;a[a.visible()?k:j]()},visible:function(){return this.element.is(":"+x)},close:function(){var b=this,d=b.options,e,f,g;if(b.visible()){if(b._closing||b.trigger(k))return;b.element.find(".k-popup").each(function(){var b=a(this),c=b.data("kendoPopup");c&&c.close()}),G.unbind(D,b._mousedownProxy),F.unbind(H,b._resizeProxy),e=K(!0,{},d.animation.close),f=b.element.data(z),g=e.effects,b.wrapper=c.wrap(b.element).css({overflow:t}),!g&&!c.size(g)&&f&&c.size(f)&&(e.effects=f,e.reverse=!0),b._closing=!0,b.element.kendoStop(!0).kendoAnimate(e)}},_resize:function(a){var b=this;if(i){var c=E.width();if(c==b._currentWidth)return;b._currentWidth=c}b._hovered||b.close()},_mousedown:function(b){var d=this,e=d.element[0],f=d.options,g=a(f.anchor)[0],h=f.toggleTarget,i=c.eventTarget(b),j=a(i).closest(".k-popup")[0];(!j||j===d.element[0])&&!M(e,i)&&!M(g,i)&&(!h||!M(a(h)[0],i))&&d.close()},_fit:function(a,b,c){var d=0;a+b>c&&(d=c-(a+b)),a<0&&(d=-a);return d},_flip:function(a,b,c,d,e,f,g){var h=0;g=g||b,f!==e&&f!==n&&e!==n&&(a+g>d&&(h+=-(c+b)),a+h<0&&(h+=c+b));return h},_position:function(b){var c=this,d=c.element,e=c.wrapper,g=c.options,i=a(g.viewport),j=a(i).offset(),k=a(g.anchor),l=g.origin.toLowerCase().split(" "),m=g.position.toLowerCase().split(" "),n=c.collisions,o=f.zoomLevel(),p=10002,q=k.parents().filter(e.siblings());if(q[0]){var r=Number(a(q).css("zIndex"));r&&(p=r+1)}e.css("zIndex",p),b&&b.isFixed?e.css({left:b.x,top:b.y}):e.css(c._align(l,m));var s=h(e,w),t=h(e),u=k.offsetParent().parent(".k-animation-container");u.length&&u.data(y)&&(s=h(e,w),t=h(e)),i[0]===window?(t.top-=window.pageYOffset||document.documentElement.scrollTop||0,t.left-=window.pageXOffset||document.documentElement.scrollLeft||0):(t.top-=j.top,t.left-=j.left),c.wrapper.data(v)||e.data(v,K({},s));var x=K({},t),z=K({},s);n[0]==="fit"&&(z.top+=c._fit(x.top,e.outerHeight(),i.height()/o)),n[1]==="fit"&&(z.left+=c._fit(x.left,e.outerWidth(),i.width()/o)),z.left!=s.left||z.top!=s.top?e.data(y,!0):e.removeData(y);var A=K({},z);n[0]==="flip"&&(z.top+=c._flip(x.top,d.outerHeight(),k.outerHeight(),i.height()/o,l[0],m[0],e.outerHeight())),n[1]==="flip"&&(z.left+=c._flip(x.left,d.outerWidth(),k.outerWidth(),i.width()/o,l[1],m[1],e.outerWidth())),e.css(z);return z.left!=A.left||z.top!=A.top},_align:function(b,c){var d=this,e=d.wrapper,f=a(d.options.anchor),g=b[0],i=b[1],j=c[0],k=c[1],l=h(f),m=a(d.options.appendTo),o,q=e.outerWidth(),s=e.outerHeight(),t=f.outerWidth(),u=f.outerHeight(),v=l.top,w=l.left,x=Math.round;m[0]!=document.body&&(o=h(m),v-=o.top,w-=o.left),g===r&&(v+=u),g===n&&(v+=x(u/2)),j===r&&(v-=s),j===n&&(v-=x(s/2)),i===p&&(w+=t),i===n&&(w+=x(t/2)),k===p&&(w-=q),k===n&&(w-=x(q/2));return{top:v,left:w}}});d.plugin(N)}(jQuery),function(a,b){function y(b,c){b.filters&&(b.filters=a.grep(b.filters,function(a){y(a,c);return a.filters?a.filters.length:a.field!=c}))}var c=window.kendo,d=c.ui,e=d.Widget,f=c.keys,g=c.support.touch,h="id",i="li",j=g?"touchend":"click",k="change",l="character",m="k-state-focused",n="k-state-hover",o="k-loading",p="open",q="close",r="select",s="selected",t=a.extend,u=a.proxy,v=a.browser.msie&&parseInt(a.browser.version,10)<9,w=/"/g,x=e.extend({init:function(b,d){var f=this,g,k;e.fn.init.call(f,b,d),f._template(),f.ul=a('<ul unselectable="on" class="k-list k-reset"/>').css({overflow:c.support.touch?"":"auto"}).delegate(i,"mouseenter",function(){a(this).addClass(n)}).delegate(i,"mouseleave",function(){a(this).removeClass(n)}).delegate(i,j,u(f._click,f)),f.list=k=a("<div class='k-list-container'/>").append(f.ul).mousedown(function(a){a.preventDefault()}),g=f.element.attr(h),g&&k.attr(h,g+"-list"),a(document.documentElement).bind("mousedown",u(f._mousedown,f))},items:function(){return this.ul[0].children},current:function(a){var c=this;if(a!==b)c._current&&c._current.removeClass(m),a&&(a.addClass(m),c._scroll(a)),c._current=a;else return c._current},dataItem:function(a){var c=this;a===b&&(a=c.selectedIndex);return c._data()[a]},_accessors:function(){var a=this,b=a.element,d=a.options,e=c.getter,f=b.attr(c.attr("text-field")),g=b.attr(c.attr("value-field"));f&&(d.dataTextField=f),g&&(d.dataValueField=g),a._text=e(d.dataTextField),a._value=e(d.dataValueField)},_blur:function(){var a=this;a._change(),a.close()},_change:function(){var a=this,c=a.selectedIndex,d=a.value(),e;d!==a._old?e=!0:c!==b&&c!==a._oldIndex&&(e=!0),e&&(a._old=d,a._oldIndex=c,a.trigger(k),a.element.trigger(k))},_click:function(b){b.isDefaultPrevented()||(this._accept(a(b.currentTarget)),b.type==="touchend"&&b.preventDefault())},_data:function(){return this.dataSource.view()},_enable:function(){var a=this,b=a.options;a.element.prop("disabled")&&(b.enable=!1),a.enable(b.enable)},_focus:function(a){var b=this;b.popup.visible()&&a&&b.trigger(r,{item:a})?b.close():(b._select(a),b._blur())},_height:function(a){if(a){var b=this,c=b.list,d=b.popup.visible(),e=b.options.height;c=c.add(c.parent(".k-animation-container")).show().height(b.ul[0].scrollHeight>e?e:"auto"),d||c.hide()}},_adjustListWidth:function(){var b=this.list,c=b[0].style.width,d=this.wrapper,e,f;c||(e=window.getComputedStyle?window.getComputedStyle(d[0],null):0,f=e?parseFloat(e.width):d.outerWidth(),e&&(a.browser.mozilla||a.browser.msie)&&(f+=parseFloat(e.paddingLeft)+parseFloat(e.paddingRight)+parseFloat(e.borderLeftWidth)+parseFloat(e.borderRightWidth)),c=f-(b.outerWidth()-b.width())),b.css({fontFamily:d.css("font-family"),width:c});return!0},_popup:function(){var a=this,b=a.list,e=a.options,f=a.wrapper,g=!1;a.popup=new d.Popup(b,t({},e.popup,{anchor:f,open:function(b){g||(g=a._adjustListWidth()),a.trigger(p)&&b.preventDefault()},close:function(b){a.trigger(q)&&b.preventDefault()},animation:e.animation})),a._touchScroller=c.touchScroller(a.popup.element)},_makeUnselectable:function(a){v&&this.list.find("*").attr("unselectable","on")},_toggleHover:function(b){g||a(b.currentTarget).toggleClass(n,b.type==="mouseenter")},_toggle:function(a){var c=this;a=a!==b?a:!c.popup.visible(),!g&&c._focused[0]!==document.activeElement&&c._focused.focus(),c[a?p:q]()},_scroll:function(a){if(!!a){a[0]&&(a=a[0]);var b=this.ul[0],c=a.offsetTop,d=a.offsetHeight,e=b.scrollTop,f=b.clientHeight,g=c+d;b.scrollTop=e>c?c:g>e+f?g-f:e}},_template:function(){var a=this,b=a.options,d=b.template,e=b.dataSource;a.element.is(r)&&a.element[0].length&&(e||(b.dataTextField=b.dataTextField||"text",b.dataValueField=b.dataValueField||"value")),d?(d=c.template(d),a.template=function(a){return'<li unselectable="on" class="k-item">'+d(a)+"</li>"}):a.template=c.template('<li unselectable="on" class="k-item">${data'+(b.dataTextField?".":"")+b.dataTextField+"}</li>",{useWithBlock:!1})}});t(x,{caret:function(a){var b,c=a.ownerDocument.selection;c?b=Math.abs(c.createRange().moveStart(l,-a.value.length)):b=a.selectionStart;return b},selectText:function(a,b,c){try{if(a.createTextRange){a.focus();var d=a.createTextRange();d.collapse(!0),d.moveStart(l,b),d.moveEnd(l,c-b),d.select()}else a.setSelectionRange(b,c)}catch(e){}},inArray:function(a,b){var c,d,e=b.children;if(!a||a.parentNode!==b)return-1;for(c=0,d=e.length;c<d;c++)if(a===e[c])return c;return-1}}),c.ui.List=x,d.Select=x.extend({init:function(a,b){x.fn.init.call(this,a,b)},setDataSource:function(a){this.options.dataSource=a,this._dataSource(),this.options.autoBind&&this._selectItem()},close:function(){this.popup.close()},_accessor:function(a,c){var d=this.element,e=d.is(r),f,g;d=d[0];if(a===b){e?(g=d.selectedIndex,g>-1&&(f=d.options[g],f&&(a=f.value))):a=d.value;return a}e?d.selectedIndex=c:d.value=a},_hideBusy:function(){var a=this;clearTimeout(a._busy),a._arrow.removeClass(o),a._busy=null},_showBusy:function(){var a=this;a._busy||(a._busy=setTimeout(function(){a._arrow.addClass(o)},100))},_dataSource:function(){var b=this,d=b.element,e=b.options,f=e.dataSource||{},g;f=a.isArray(f)?{data:f}:f,d.is(r)&&(g=d[0].selectedIndex,g>-1&&(e.index=g),f.select=d,f.fields=[{field:e.dataTextField},{field:e.dataValueField}]),b.dataSource&&b._refreshHandler?b.dataSource.unbind(k,b._refreshHandler).unbind("requestStart",b._requestStartHandler):(b._refreshHandler=u(b.refresh,b),b._requestStartHandler=u(b._showBusy,b)),b.dataSource=c.data.DataSource.create(f).bind(k,b._refreshHandler).bind("requestStart",b._requestStartHandler)},_index:function(a){var c=this,d,e,f=c._data(),g;for(d=0,e=f.length;d<e;d++){g=c._value(f[d]),g===b&&(g=c._text(f[d]));if(g==a)return d}return-1},_get:function(b){var c=this,d=c._data(),e,f;if(typeof b=="function")for(e=0,f=d.length;e<f;e++)if(b(d[e])){b=e;break}if(typeof b=="number"){if(b<0)return a();b=a(c.ul[0].children[b])}b&&b.nodeType&&(b=a(b));return b},_move:function(a){var b=this,c=a.keyCode,d=b.ul[0],e=b._current,g=c===f.DOWN,h;c===f.UP||g?(a.altKey?b.toggle(g):g?(b._select(e?e[0].nextSibling:d.firstChild),a.preventDefault()):(b._select(e?e[0].previousSibling:d.lastChild),a.preventDefault()),h=!0):c===f.ENTER||c===f.TAB?(b.popup.visible()&&a.preventDefault(),b._accept(e),h=!0):c===f.ESC&&(b.close(),h=!0);return h},_valueOnFetch:function(a){var b=this;if(!b.ul[0].firstChild&&!b._fetch){b.dataSource.one(k,function(){b._fetch=!0,b.value(a)}).fetch();return!0}b._fetch=!1},_options:function(a,c){var d=this,e=d.element,f=e[0].selectedIndex,g=a.length,h="",i,j,k,l,m=0;c&&(h=c,m=1);for(;m<g;m++)i="<option",j=a[m],k=d._text(j),l=d._value(j),l!==b&&(l+="",l.indexOf('"')!==-1&&(l=l.replace(w,"&quot;")),i+=' value="'+l+'"'),i+=">",k!==b&&(i+=k),i+="</option>",h+=i;e.html(h),e[0].selectedIndex=f},_reset:function(){var a=this,b=a.element;b.closest("form").bind("reset",function(){setTimeout(function(){a.value(b[0].value)})})},_cascade:function(){var b=this,c=b.options,d=c.cascadeFrom,e,f,g,h,i,j;if(d){e=a("#"+d).data("kendo"+c.name);if(!e)return;j=b.dataSource,g=e.options.dataValueField,h=function(){b.value(""),b.enable(!1)},i=function(){var a=b.value();a?(b.value(a),b.selectedIndex==-1&&b.value("")):b.select(c.index),b.trigger(s),b.enable()},f=function(){var a=e.dataItem(),b=a?e._value(a):null,c,d;b?(c=j.filter()||{},y(c,g),d=c.filters||[],d.push({field:g,operator:"eq",value:b}),j.one(k,i).filter(d)):h()},e.bind("cascade",h).bind(k,function(){f(),b.trigger("cascade")}).one(s,function(){f()}),f()}}}),d.Select.removeFiltersForField=y}(jQuery),function(a,b){function ba(a,b){if(a)return a.getFullYear()===b.getFullYear()&&a.getMonth()===b.getMonth()&&a.getDate()===b.getDate();return!1}function _(a,b){for(var c=0,d=b.length;c<d;c++)if(a===+b[c])return!0;return!1}function $(a){o&&a.find("*").attr("unselectable","on")}function Z(a){var c=N[a.start],d=N[a.depth],e=i(a.culture);a.format=g(a.format||e.calendars.standard.patterns.d),isNaN(c)&&(c=0,a.start=t);if(d===b||d>c)a.depth=t}function Y(a){return i(a).calendars.standard}function X(a){a.preventDefault()}function W(b){b.stopImmediatePropagation(),a(this).toggleClass(y,b.type=="mouseenter")}function V(a,b,c){b=b instanceof M?b.getFullYear():a.getFullYear()+c*b,a.setFullYear(b)}function U(a,b){return a.slice(b).concat(a.slice(0,b))}function T(a,b,c){return+a>=+b&&+a<=+c}function S(a,b,c){var d=new M;d=new M(d.getFullYear(),d.getMonth(),d.getDate()),a&&(d=new M(a)),b>d?d=new M(b):c<d&&(d=new M(c));return d}function R(a,b,c){var d=a.getFullYear(),e=b.getFullYear(),f=e,g=0;c&&(e=e-e%c,f=e-e%c+c-1),d>f?g=1:d<e&&(g=-1);return g}function Q(a){var b=0,c,d=a.min,e=a.max,f=a.start,g=a.setter,h=a.build,i=a.cells||12,j=a.perRow||4,k=a.content||m,l=a.empty||n,o=a.html||'<table class="k-content k-meta-view" cellspacing="0"><tbody><tr>';for(;b<i;b++)b>0&&b%j===0&&(o+="</tr><tr>"),c=h(f,b),o+=T(f,d,e)?k(c):l(c),g(f,1);return o+"</tr></tbody></table>"}var c=window.kendo,d=c.ui,e=d.Widget,f=c.parseDate,g=c._extractFormat,h=c.template,i=c.getCulture,j=c.support.touch,k=c.support.transitions,l=k?k.css+"transform-origin":"",m=h('<td#=data.cssClass#><a class="k-link" href="\\#" data-#=data.ns#value="#=data.dateString#">#=data.value#</a></td>',{useWithBlock:!1}),n=h("<td>&nbsp;</td>",{useWithBlock:!1}),o=a.browser.msie&&parseInt(a.browser.version,10)<9,p=j?"touchend":"click",q="min",r="left",s="slide",t="month",u="century",v="change",w="navigate",x="value",y="k-state-hover",z="k-state-disabled",A="k-other-month",B=' class="'+A+'"',C="k-nav-today",D="td:has(.k-link)",E=j?"touchstart":"mouseenter",F=j?"touchend":"mouseleave",G=6e4,H=864e5,I="_prevArrow",J="_nextArrow",K=a.proxy,L=a.extend,M=Date,N={month:0,year:1,decade:2,century:3},O=e.extend({init:function(a,b){var d=this,f;e.fn.init.call(d,a,b),a=d.wrapper=d.element,b=d.options,b.url=window.unescape(b.url),a.addClass("k-widget k-calendar"),d._templates(),d._header(),d._footer(d.footer),a.delegate(D,E+" "+F,W).delegate(D,p,K(d._click,d)),f=b.value,Z(b),d._index=N[b.start],d._current=new M(S(f,b.min,b.max)),d.value(f),c.notify(d)},options:{name:"Calendar",value:null,min:new M(1900,0,1),max:new M(2099,11,31),dates:[],url:"",culture:"",footer:"",format:"",month:{},start:t,depth:t,animation:{horizontal:{effects:s,duration:500,divisor:2},vertical:{effects:"zoomIn",duration:400}}},events:[v,w],setOptions:function(a){Z(a),e.fn.setOptions.call(this,a)},min:function(a){return this._option(q,a)},max:function(a){return this._option("max",a)},navigateToPast:function(){this._navigate(I,-1)},navigateToFuture:function(){this._navigate(J,1)},navigateUp:function(){var a=this,b=a._index;a._title.hasClass(z)||a.navigate(a._current,++b)},navigateDown:function(a){var b=this,c=b._index,d=b.options.depth;if(!!a){if(c===N[d]){+b._value!=+a&&(b.value(a),b.trigger(v));return}b.navigate(a,--c)}},navigate:function(c,d){d=isNaN(d)?N[d]:d;var e=this,f=e.options,g=f.culture,h=f.min,i=f.max,j=e._title,k=e._table,l=e._value,m=e._current,n=c&&+c>+m,o=d!==b&&d!==e._index,p,q,r;c?e._current=c=new M(S(c,h,i)):c=m,d===b?d=e._index:e._index=d,e._view=q=P.views[d],r=q.compare,j.toggleClass(z,d===N[u]),e[I].toggleClass(z,r(c,h)<1),e[J].toggleClass(z,r(c,i)>-1);if(!k||e._changeView)j.html(q.title(c,g)),e._table=p=a(q.content(L({min:h,max:i,date:c,url:f.url,dates:f.dates,format:f.format,culture:g},e[q.name]))),$(p),e._animate({from:k,to:p,vertical:o,future:n}),e.trigger(w);d===N[f.depth]&&l&&e._class("k-state-selected",q.toDateString(l)),e._changeView=!0},value:function(a){var c=this,d=c._view,e=c.options,g=e.min,h=e.max;if(a===b)return c._value;a=f(a,e.format,e.culture),a!==null&&(a=new M(a),T(a,g,h)||(a=null)),c._value=a,c._changeView=!a||d&&d.compare(a,c._current)!==0,c.navigate(a)},_animate:function(a){var b=this,c=a.from,d=a.to;c?c.parent().data("animating")?(c.parent().kendoStop(!0,!0).remove(),c.remove(),d.insertAfter(b.element[0].firstChild)):!c.is(":visible")||b.options.animation===!1?(d.insertAfter(c),c.remove()):b[a.vertical?"_vertical":"_horizontal"](c,d,a.future):d.insertAfter(b.element[0].firstChild)},_horizontal:function(a,b,c){var d=this,e=d.options.animation.horizontal,f=e.effects,g=a.outerWidth();f&&f.indexOf(s)!=-1&&(a.add(b).css({width:g}),a.wrap("<div/>"),a.parent().css({position:"relative",width:g*2,"float":r,left:c?0:-g}),b[c?"insertAfter":"insertBefore"](a),L(e,{effects:s+":"+(c?r:"right"),complete:function(){a.remove(),b.unwrap()}}),a.parent().kendoStop(!0,!0).kendoAnimate(e))},_vertical:function(a,b){var c=this,d=c.options.animation.vertical,e=d.effects,f,g;e&&e.indexOf("zoom")!=-1&&(b.css({position:"absolute",top:a.prev().outerHeight(),left:0}).insertBefore(a),l&&(f=c._cellByDate(c._view.toDateString(c._current)),g=f.position(),g=g.left+parseInt(f.width()/2,10)+"px"+" "+(g.top+parseInt(f.height()/2,10)+"px"),b.css(l,g)),a.kendoStop(!0,!0).kendoAnimate({effects:"fadeOut",duration:600,complete:function(){a.remove(),b.css({position:"static",top:0,left:0})}}),b.kendoStop(!0,!0).kendoAnimate(d))},_cellByDate:function(b){return this._table.find("td:not(."+A+")").filter(function(){return a(this.firstChild).attr(c.attr(x))===b})},_class:function(b,d){this._table.find("td:not(."+A+")").removeClass(b).filter(function(){return a(this.firstChild).attr(c.attr(x))===d}).addClass(b)},_click:function(b){var d=this,e=d.options,f=d._current,g=a(b.currentTarget.firstChild),h=g.attr(c.attr(x)).split("/");h=new M(h[0],h[1],h[2]),g[0].href.indexOf("#")!=-1&&b.preventDefault(),g.parent().hasClass(A)?f=h:d._view.setDate(f,h),d.navigateDown(S(f,e.min,e.max))},_focus:function(a){var b=this,c=b._view;c.compare(a,b._current)!==0?b.navigate(a):b._current=a,b._class("k-state-focused",c.toDateString(a))},_footer:function(b){var d=this,e=d.element,f=new M,g=e.find(".k-footer");b?(g[0]||(g=a('<div class="k-footer"><a href="#" class="k-link k-nav-today"></a></div>').appendTo(e)),d._today=g.show().find(".k-link").html(b(f)).attr("title",c.toString(f,"D",d.options.culture)),d._toggle()):(d._toggle(!1),g.hide())},_header:function(){var a=this,b=a.element,c;b.find(".k-header")[0]||b.html('<div class="k-header"><a href="#" class="k-link k-nav-prev"><span class="k-icon k-i-arrow-w"></span></a><a href="#" class="k-link k-nav-fast"></a><a href="#" class="k-link k-nav-next"><span class="k-icon k-i-arrow-e"></span></a></div>'),c=b.find(".k-link").bind(E+" "+F,W).click(!1),a._title=c.eq(1).bind(p,K(a.navigateUp,a)),a[I]=c.eq(0).bind(p,K(a.navigateToPast,a)),a[J]=c.eq(2).bind(p,K(a.navigateToFuture,a))},_navigate:function(a,b){var c=this,d=c._index+1,e=new M(c._current);a=c[a],a.hasClass(z)||(d>3?e.setFullYear(e.getFullYear()+100*b):P.views[d].setDate(e,b),c.navigate(e))},_option:function(a,c){var d=this,e=d.options,g=+d._value,h,i;if(c===b)return e[a];c=f(c,e.format,e.culture);!c||(e[a]=new M(c),i=d._view.compare(c,d._current),a===q?(h=+c>g,i=i>-1):(h=g>+c,i=i<1),h?d.value(null):i&&d.navigate(),d._toggle())},_toggle:function(a){var c=this,d=c.options,e=c._today;a===b&&(a=T(new M,d.min,d.max)),e&&(e.unbind(p),a?e.addClass(C).removeClass(z).bind(p,K(c._todayClick,c)):e.removeClass(C).addClass(z).bind(p,X))},_todayClick:function(a){var b=this,c=N[b.options.depth],d=new M;a.preventDefault(),b._view.compare(b._current,d)===0&&b._index==c&&(b._changeView=!1),b._value=d,b.navigate(d,c),b.trigger(v)},_templates:function(){var a=this,b=a.options,d=b.footer,e=b.month,f=e.content,g=e.empty;a.month={content:h('<td#=data.cssClass#><a class="k-link#=data.linkClass#" href="#=data.url#" '+c.attr("value")+'="#=data.dateString#" title="#=data.title#">'+(f||"#=data.value#")+"</a></td>",{useWithBlock:!!f}),empty:h("<td>"+(g||"&nbsp;")+"</td>",{useWithBlock:!!g})},d!==!1&&(a.footer=h(d||'#= kendo.toString(data,"D","'+b.culture+'") #',{useWithBlock:!1}))}});d.plugin(O);var P={firstDayOfMonth:function(a){return new M(a.getFullYear(),a.getMonth(),1)},firstVisibleDay:function(a,b){b=b||c.culture().calendar;var d=b.firstDay,e=new M(a.getFullYear(),a.getMonth(),0,a.getHours(),a.getMinutes(),a.getSeconds(),a.getMilliseconds());while(e.getDay()!=d)P.setTime(e,-1*H);return e},setTime:function(a,b){var c=a.getTimezoneOffset(),d=new M(a.getTime()+b),e=d.getTimezoneOffset()-c;a.setTime(d.getTime()+e*G)},views:[{name:t,title:function(a,b){return Y(b).months.names[a.getMonth()]+" "+a.getFullYear()},content:function(a){var b=this,d=0,e=a.min,f=a.max,g=a.date,h=a.dates,i=a.format,j=a.culture,k=a.url,l=k&&h[0],m=Y(j),n=m.firstDay,o=m.days,p=U(o.names,n),q=U(o.namesShort,n),r=P.firstVisibleDay(g,m),s=b.first(g),t=b.last(g),u=b.toDateString,v=new M,w='<table class="k-content" cellspacing="0"><thead><tr>';for(;d<7;d++)w+='<th scope="col" title="'+p[d]+'">'+q[d]+"</th>";v=+(new M(v.getFullYear(),v.getMonth(),v.getDate()));return Q({cells:42,perRow:7,html:w+="</tr></thead><tbody><tr>",start:new M(r.getFullYear(),r.getMonth(),r.getDate()),min:new M(e.getFullYear(),e.getMonth(),e.getDate()),max:new M(f.getFullYear(),f.getMonth(),f.getDate()),content:a.content,empty:a.empty,setter:b.setDate,build:function(a,b){var d=[],e=a.getDay(),f="",g="#";(a<s||a>t)&&d.push(A),+a===v&&d.push("k-today"),(e===0||e===6)&&d.push("k-weekend"),l&&_(+a,h)&&(g=k.replace("{0}",c.toString(a,i,j)),f=" k-action-link");return{date:a,dates:h,ns:c.ns,title:c.toString(a,"D",j),value:a.getDate(),dateString:u(a),cssClass:d[0]?' class="'+d.join(" ")+'"':"",linkClass:f,url:g}}})},first:function(a){return P.firstDayOfMonth(a)},last:function(a){return new M(a.getFullYear(),a.getMonth()+1,0)},compare:function(a,b){var c,d=a.getMonth(),e=a.getFullYear(),f=b.getMonth(),g=b.getFullYear();e>g?c=1:e<g?c=-1:c=d==f?0:d>f?1:-1;return c},setDate:function(a,b){b instanceof M?a.setFullYear(b.getFullYear(),b.getMonth(),b.getDate()):P.setTime(a,b*H)},toDateString:function(a){return a.getFullYear()+"/"+a.getMonth()+"/"+a.getDate()}},{name:"year",title:function(a){return a.getFullYear()},content:function(a){var b=Y(a.culture).months.namesAbbr,d=this.toDateString,e=a.min,f=a.max;return Q({min:new M(e.getFullYear(),e.getMonth(),1),max:new M(f.getFullYear(),f.getMonth(),1),start:new M(a.date.getFullYear(),0,1),setter:this.setDate,build:function(a){return{value:b[a.getMonth()],ns:c.ns,dateString:d(a),cssClass:""}}})},first:function(a){return new M(a.getFullYear(),0,a.getDate())},last:function(a){return new M(a.getFullYear(),11,a.getDate())},compare:function(a,b){return R(a,b)},setDate:function(a,b){if(b instanceof M)a.setFullYear(b.getFullYear(),b.getMonth(),a.getDate());else{var c=a.getMonth()+b;a.setMonth(c),c>11&&(c-=12),c>0&&a.getMonth()!=c&&a.setDate(0)}},toDateString:function(a){return a.getFullYear()+"/"+a.getMonth()+"/1"}},{name:"decade",title:function(a){var b=a.getFullYear();b=b-b%10;return b+"-"+(b+9)},content:function(a){var b=a.date.getFullYear(),d=this.toDateString;return Q({start:new M(b-b%10-1,0,1),min:new M(a.min.getFullYear(),0,1),max:new M(a.max.getFullYear(),0,1),setter:this.setDate,build:function(a,b){return{value:a.getFullYear(),ns:c.ns,dateString:d(a),cssClass:b===0||b==11?B:""}}})},first:function(a){var b=a.getFullYear();return new M(b-b%10,a.getMonth(),a.getDate())},last:function(a){var b=a.getFullYear();return new M(b-b%10+9,a.getMonth(),a.getDate())},compare:function(a,b){return R(a,b,10)},setDate:function(a,b){V(a,b,1)},toDateString:function(a){return a.getFullYear()+"/0/1"}},{name:u,title:function(a){var b=a.getFullYear();b=b-b%100;return b+"-"+(b+99)},content:function(a){var b=a.date.getFullYear(),d=a.min.getFullYear(),e=a.max.getFullYear(),f=this.toDateString;d=d-d%10,e=e-e%10,e-d<10&&(e=d+9);return Q({start:new M(b-b%100-10,0,1),min:new M(d,0,1),max:new M(e,0,1),setter:this.setDate,build:function(a,b){var d=a.getFullYear();return{value:d+" - "+(d+9),ns:c.ns,dateString:f(a),cssClass:b===0||b==11?B:""}}})},first:function(a){var b=a.getFullYear();return new M(b-b%100,a.getMonth(),a.getDate())},last:function(a){var b=a.getFullYear();return new M(b-b%100+99,a.getMonth(),a.getDate())},compare:function(a,b){return R(a,b,100)},setDate:function(a,b){V(a,b,10)},toDateString:function(a){var b=a.getFullYear();return b-b%10+"/0/1"}}]};P.isEqualDatePart=ba,P.makeUnselectable=$,P.restrictValue=S,P.isInRange=T,P.normalize=Z,P.viewsEnum=N,c.calendar=P}(jQuery),function(a,b){function M(a){a.preventDefault()}function L(b){var c=b.parseFormats;E.normalize(b),c=a.isArray(c)?c:[c],c.splice(0,0,b.format),b.parseFormats=c}var c=window.kendo,d=c.ui,e=c.support.touch,f=d.Widget,g=c.parseDate,h=c.keys,i=c.template,j="<div />",k="<span />",l=e?"touchend":"click",m=l+".datepicker",n="open",o="close",p="change",q="navigate",r="dateView",s="disabled",t="k-state-default",u="k-state-focused",v="k-state-selected",w="k-state-disabled",x="k-state-hover",y="mouseenter mouseleave",z=e?"touchstart":"mousedown",A="min",B="max",C="month",D="first",E=c.calendar,F=E.isInRange,G=E.restrictValue,H=E.isEqualDatePart,I=a.extend,J=a.proxy,K=Date,N=function(b){var c=this,e=document.body,f=O.sharedCalendar;f||(f=O.sharedCalendar=new d.Calendar(a(j).hide().appendTo(e)),E.makeUnselectable(f.element)),c.calendar=f,c.options=b=b||{},c.popup=new d.Popup(a(j).addClass("k-calendar-container").appendTo(e),I(b.popup,b,{name:"Popup"})),c._templates(),c.value(b.value)};N.prototype={_calendar:function(){var a=this,b=a.popup,c=a.options,d=a.calendar,f=d.element;f.data(r)!==a&&(f.appendTo(b.element).data(r,a).undelegate(m).delegate("td:has(.k-link)",m,J(a._click,a)).unbind(z).bind(z,M).show(),d.unbind(p).bind(p,c),e||d.unbind(q).bind(q,J(a._navigate,a)),d.month=a.month,d.options.depth=c.depth,d.options.culture=c.culture,d._footer(a.footer),d.min(c.min),d.max(c.max),d.navigate(a._value,c.start),a.value(a._value))},open:function(){var a=this;a._calendar(),a.popup.open()},close:function(){this.popup.close()},min:function(a){this._option(A,a)},max:function(a){this._option(B,a)},toggle:function(){var a=this;a[a.popup.visible()?o:n]()},move:function(a){var b=this,c=b.options,d=new K(b._current),e=b.calendar,f=e._index,g=e._view,i=a.keyCode,j,k,l;if(i==h.ESC)b.close();else{if(a.altKey){i==h.DOWN?(b.open(),k=!0):i==h.UP&&(b.close(),k=!0);return}if(!b.popup.visible()||e._table.parent().data("animating"))return;if(a.ctrlKey)i==h.RIGHT?(e.navigateToFuture(),k=!0):i==h.LEFT?(e.navigateToPast(),k=!0):i==h.UP?(e.navigateUp(),k=!0):i==h.DOWN&&(b._navigateDown(),k=!0);else{i==h.RIGHT?(j=1,k=!0):i==h.LEFT?(j=-1,k=!0):i==h.UP?(j=f===0?-7:-4,k=!0):i==h.DOWN?(j=f===0?7:4,k=!0):i==h.ENTER?(b._navigateDown(),k=!0):i==h.HOME||i==h.END?(l=i==h.HOME?D:"last",d=g[l](d),k=!0):i==h.PAGEUP?(k=!0,e.navigateToPast()):i==h.PAGEDOWN&&(k=!0,e.navigateToFuture());if(j||l)l||g.setDate(d,j),b._current=d=G(d,c.min,c.max),e._focus(d)}k&&a.preventDefault()}},value:function(a){var b=this,c=b.calendar,d=b.options;b._value=a,b._current=new K(G(a,d.min,d.max)),c.element.data(r)===b&&(c._focus(b._current),c.value(a))},_click:function(a){a.currentTarget.className.indexOf(v)!==-1&&this.close()},_navigate:function(){var a=this,b=a.calendar;a._current=new K(b._current),b._focus(b._current)},_navigateDown:function(){var a=this,b=a.calendar,d=b._current,e=b._table.find("."+u),f=e.children(":"+D).attr(c.attr("value")).split("/");f=new K(f[0],f[1],f[2]);!e[0]||e.hasClass(v)?a.close():(b._view.setDate(d,f),b.navigateDown(d))},_option:function(a,b){var c=this,d=c.options,e=c.calendar;d[a]=b,e.element.data(r)===c&&e[a](b)},_templates:function(){var a=this,b=a.options,d=b.footer,e=b.month||{},f=e.content,g=e.empty;a.month={content:i('<td#=data.cssClass#><a class="k-link" href="\\#" '+c.attr("value")+'="#=data.dateString#" title="#=data.title#">'+(f||"#=data.value#")+"</a></td>",{useWithBlock:!!f}),empty:i("<td>"+(g||"&nbsp;")+"</td>",{useWithBlock:!!g})},d!==!1&&(a.footer=i(d||'#= kendo.toString(data,"D","'+b.culture+'") #',{useWithBlock:!1}))}},N.normalize=L,c.DateView=N;var O=f.extend({init:function(a,b){var d=this;f.fn.init.call(d,a,b),a=d.element,b=d.options,L(b),d._wrapper(),d.dateView=new N(I({},b,{anchor:d.wrapper,change:function(){d._change(this.value()),d.close()},close:function(a){d.trigger(o)&&a.preventDefault()},open:function(a){d.trigger(n)&&a.preventDefault()}})),d._icon(),e||(a[0].type="text"),a.addClass("k-input").bind({keydown:J(d._keydown,d),focus:function(a){d._inputWrapper.addClass(u)},blur:J(d._blur,d)}).closest("form").bind("reset",function(){d.value(a[0].defaultValue)}),d.enable(!a.is("[disabled]")),d.value(b.value||d.element.val()),c.notify(d)},events:[n,o,p],options:{name:"DatePicker",value:null,footer:"",format:"",culture:"",parseFormats:[],min:new Date(1900,0,1),max:new Date(2099,11,31),start:C,depth:C,animation:{},month:{}},setOptions:function(a){var b=this;f.fn.setOptions.call(b,a),L(b.options),I(b.dateView.options,b.options)},enable:function(a){var b=this,c=b._dateIcon.unbind(l+" "+z),d=b._inputWrapper.unbind(y),e=b.element;a===!1?(d.removeClass(t).addClass(w),e.attr(s,s)):(d.addClass(t).removeClass(w).bind(y,b._toggleHover),e.removeAttr(s),c.bind(l,J(b._click,b)).bind(z,M))},open:function(){this.dateView.open()},close:function(){this.dateView.close()},min:function(a){return this._option(A,a)},max:function(a){return this._option(B,a)},value:function(a){var c=this;if(a===b)return c._value;c._old=c._update(a)},_toggleHover:function(b){e||a(b.currentTarget).toggleClass(x,b.type==="mouseenter")},_blur:function(){var a=this;a.close(),a._change(a.element.val()),a._inputWrapper.removeClass(u)},_click:function(){var a=this,b=a.element;a.dateView.toggle(),!e&&b[0]!==document.activeElement&&b.focus()},_change:function(a){var b=this;a=b._update(a),+b._old!=+a&&(b._old=a,b.trigger(p),b.element.trigger(p))},_keydown:function(a){var b=this,c=b.dateView;!c.popup.visible()&&a.keyCode==h.ENTER?b._change(b.element.val()):c.move(a)},_icon:function(){var b=this,c=b.element,d;d=c.next("span.k-select"),d[0]||(d=a('<span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-calendar">select</span></span>').insertAfter(c)),b._dateIcon=d},_option:function(a,c){var d=this,e=d.options;if(c===b)return e[a];c=g(c,e.parseFormats,e.culture);!c||(e[a]=new K(c),d.dateView[a](c))},_update:function(a){var b=this,d=b.options,e=d.min,f=d.max,h=g(a,d.parseFormats,d.culture);if(+h===+b._value)return h;h!==null&&H(h,e)?h=G(h,e,f):F(h,e,f)||(h=null),b._value=h,b.dateView.value(h),b.element.val(h?c.toString(h,d.format,d.culture):a);return h},_wrapper:function(){var b=this,c=b.element,d;d=c.parents(".k-datepicker"),d[0]||(d=c.wrap(k).parent().addClass("k-picker-wrap k-state-default"),d=d.wrap(k).parent()),d[0].style.cssText=c[0].style.cssText,c.css({width:"100%",height:c[0].style.height}),b.wrapper=d.addClass("k-widget k-datepicker k-header"),b._inputWrapper=a(d[0].firstChild)}});d.plugin(O)}(jQuery),function(a,b){function x(a){var b=a.value.length;s(a,b,b)}function w(a,b,c,d){var e=b.split(d);e.splice(u(a,b,d),1,c),d&&e[e.length-1]!==""&&e.push("");return e.join(d)}function v(a,b,c){return b.split(c)[u(a,b,c)]}function u(a,b,c){return c?b.substring(0,a).split(c).length-1:0}var c=window.kendo,d=c.support,e=d.placeholder,f=c.ui,g=c.keys,h=c.data.DataSource,i=f.List,j="change",k="k-state-default",l="disabled",m="k-state-focused",n="k-state-selected",o="k-state-disabled",p="k-state-hover",q="mouseenter mouseleave",r=i.caret,s=i.selectText,t=a.proxy,y=i.extend({init:function(b,d){var f=this,g;d=a.isArray(d)?{dataSource:d}:d,i.fn.init.call(f,b,d),b=f.element,d=f.options,d.placeholder=d.placeholder||b.attr("placeholder"),e&&b.attr("placeholder",d.placeholder),f._wrapper(),f._accessors(),f._dataSource(),b[0].type="text",g=f.wrapper,b.attr("autocomplete","off").addClass("k-input").bind({keydown:t(f._keydown,f),paste:t(f._search,f),focus:function(){f._prev=f.value(),f._placeholder(!1),g.addClass(m),clearTimeout(f._bluring)},blur:function(){f._change(),f._placeholder(),g.removeClass(m)}}),f._enable(),f._popup(),f._old=f.value(),f._placeholder(),c.notify(f)},options:{name:"AutoComplete",suggest:!1,template:"",dataTextField:"",minLength:1,delay:200,height:200,filter:"startswith",ignoreCase:!0,highlightFirst:!1,separator:null,placeholder:"",animation:{}},_dataSource:function(){var a=this;a.dataSource&&a._refreshHandler?a.dataSource.unbind(j,a._refreshHandler):a._refreshHandler=t(a.refresh,a),a.dataSource=h.create(a.options.dataSource).bind(j,a._refreshHandler)},setDataSource:function(a){this.options.dataSource=a,this._dataSource()},events:["open","close",j,"select","dataBinding","dataBound"],setOptions:function(a){i.fn.setOptions.call(this,a),this._template(),this._accessors()},enable:function(a){var b=this,c=b.element,d=b.wrapper;a===!1?(d.removeClass(k).addClass(o).unbind(q),c.attr(l,l)):(d.removeClass(o).addClass(k).bind(q,b._toggleHover),c.removeAttr(l))},close:function(){var a=this;a._current=null,a.popup.close()},refresh:function(){var b=this,d=b.ul[0],e=b.popup,f=b.options,g=b._data(),h=g.length;b.trigger("dataBinding"),d.innerHTML=c.render(b.template,g),b._height(h),h&&(f.highlightFirst&&b.current(a(d.firstChild)),f.suggest&&b.suggest(a(d.firstChild))),b._open&&(b._open=!1,e[h?"open":"close"]()),b._touchScroller&&b._touchScroller.reset(),b._makeUnselectable(),b.trigger("dataBound")},select:function(a){this._select(a)},search:function(a){var b=this,c=b.options,d=c.ignoreCase,e=c.separator,f;a=a||b.value(),b._current=null,clearTimeout(b._typing),e&&(a=v(r(b.element[0]),a,e)),f=a.length,f?f>=b.options.minLength&&(b._open=!0,b.dataSource.filter({value:d?a.toLowerCase():a,operator:c.filter,field:c.dataTextField,ignoreCase:d})):b.popup.close()},suggest:function(a){var c=this,d=c._last,e=c.value(),f=c.element[0],h=r(f),j=c.options.separator,k=e.split(j),l=u(h,e,j),m=h,n;d==g.BACKSPACE||d==g.DELETE?c._last=b:(a=a||"",typeof a!="string"&&(n=i.inArray(a[0],c.ul[0]),n>-1?a=c._text(c._data()[n]):a=""),h<=0&&(h=e.toLowerCase().indexOf(a.toLowerCase())+1),n=e.substring(0,h).lastIndexOf(j),n=n>-1?h-(n+j.length):h,e=k[l].substring(0,n),a&&(n=a.toLowerCase().indexOf(e.toLowerCase()),n>-1&&(a=a.substring(n+e.length),m=h+a.length,e+=a),j&&k[k.length-1]!==""&&k.push("")),k[l]=e,c.value(k.join(j||"")),s(f,h,m))},value:function(a){var c=this,d=c.element[0];if(a!==b)d.value=a,c._placeholder();else{a=d.value;if(d.className.indexOf("k-readonly")>-1)return a===c.options.placeholder?"":a;return a}},_accept:function(a){var b=this;b._focus(a),x(b.element[0])},_keydown:function(b){var c=this,d=c.ul[0],e=b.keyCode,f=c._current,h=c.popup.visible();c._last=e,e===g.DOWN?(h&&c._move(f?f.next():a(d.firstChild)),b.preventDefault()):e===g.UP?(h&&c._move(f?f.prev():a(d.lastChild)),b.preventDefault()):e===g.ENTER||e===g.TAB?(c.popup.visible()&&b.preventDefault(),c._accept(f)):e===g.ESC?c.close():c._search()},_move:function(a){var b=this;a=a[0]?a:null,b.current(a),b.options.suggest&&b.suggest(a)},_placeholder:function(a){if(!e){var c=this,d=c.element,f=c.options.placeholder,g;if(f){g=d.val(),a===b&&(a=!g),a||(g!==f?f=g:f="");if(g===c._old&&!a)return;d.toggleClass("k-readonly",a).val(f)}}},_search:function(){var a=this;clearTimeout(a._typing),a._typing=setTimeout(function(){a._prev!==a.value()&&(a._prev=a.value(),a.search())},a.options.delay)},_select:function(b){var c=this,d=c.options.separator,e=c._data(),f,g;b=a(b),b[0]&&!b.hasClass(n)&&(g=i.inArray(b[0],c.ul[0]),g>-1&&(e=e[g],f=c._text(e),d&&(f=w(r(c.element[0]),c.value(),f,d)),c.value(f),c.current(b.addClass(n))))},_toggleHover:function(b){d.touch||a(b.currentTarget).toggleClass(p,b.type==="mouseenter")},_wrapper:function(){var a=this,b=a.element,c=b[0],d;d=b.parent(),d.is("span.k-widget")||(d=b.wrap("<span />").parent()),d[0].style.cssText=c.style.cssText,b.css({width:"100%",height:c.style.height}),a._focused=a.element,a.wrapper=d.addClass("k-widget k-autocomplete k-header").addClass(c.className)}});f.plugin(y)}(jQuery),function(a,b){function r(a,b,c){var d=0,e=b.length-1,f;for(;d<e;++d)f=b[d],f in a||(a[f]={}),a=a[f];a[b[e]]=c}var c=window.kendo,d=c.ui,e=d.Select,f=c.support.mobileOS,g="disabled",h="change",i="select",j="k-state-focused",k="k-state-default",l="k-state-disabled",m="k-state-selected",n="tabIndex",o="mouseenter mouseleave",p=a.proxy,q=e.extend({init:function(d,f){var g=this,h=f&&f.index,j,k,l;f=a.isArray(f)?{dataSource:f}:f,e.fn.init.call(g,d,f),f=g.options,d=g.element.focus(function(){g.wrapper.focus()}),g._reset(),g._word="",g._wrapper(),g._span(),g._popup(),g._mobile(),g._accessors(),g._dataSource(),g._enable(),g._cascade(),g.selectedIndex=-1,h!==b&&(f.index=h),f.autoBind?g._selectItem():(l=f.text,l||(j=g._optionLabelText(f.optionLabel),k=j&&f.index===0,d.is(i)?k?l=j:l=d.children(":selected").text():!d[0].value&&k&&(l=j)),g.text(l)),c.notify(g)},options:{name:"DropDownList",enable:!0,index:0,autoBind:!0,text:"",template:"",delay:500,height:200,dataTextField:"",dataValueField:"",optionLabel:"",cascadeFrom:"",ignoreCase:!0,animation:{}},events:["open","close",h,"select","dataBinding","dataBound"],setOptions:function(a){e.fn.setOptions.call(this,a),this._template(),this._accessors()},enable:function(a){var b=this,c=b.element,d=b.wrapper.unbind(".dropdownlist"),e=b._inputWrapper.unbind(o);a===!1?(c.attr(g,g),e.removeClass(k).addClass(l)):(c.removeAttr(g,g),e.addClass(k).removeClass(l).bind(o,b._toggleHover),d.bind({"click.dropdownlist":function(a){a.preventDefault(),b.toggle()},"keydown.dropdownlist":p(b._keydown,b),"keypress.dropdownlist":p(b._keypress,b),"focusin.dropdownlist":function(){e.addClass(j)},"focusout.dropdownlist":function(a){b._blur(),e.removeClass(j)}}))},open:function(){var a=this;a.ul[0].firstChild?(a.popup.open(),a._scroll(a._current)):(a._open=!0,a._selectItem())},toggle:function(a){this._toggle(a)},refresh:function(){var a=this,b=a._data(),d=b.length,e=a.options.optionLabel;a.trigger("dataBinding"),a._current&&a.current(null),a.ul[0].innerHTML=c.render(a.template,b),a._height(d),a.element.is(i)&&(e&&d&&(e=a._optionLabelText(e),e='<option value="">'+e+"</option>"),a._options(b,e)),a._open&&(a._open=!1,a.toggle(!!d)),a._hideBusy(),a._makeUnselectable(),a.trigger("dataBound")},search:function(a){if(a){var c=this,d=c.options.ignoreCase;d&&(a=a.toLowerCase()),c._select(function(e){var f=c._text(e);if(f!==b){f=f+"",d&&(f=f.toLowerCase());return f.indexOf(a)===0}})}},select:function(a){var c=this;if(a===b)return c.selectedIndex;c._select(a),c._old=c._accessor(),c._oldIndex=c.selectedIndex},text:function(a){var c=this.span;if(a!==b)c.text(a);else return c.text()},value:function(a){var c=this,d;if(a===b)return c._accessor();a!==null&&(a=a.toString());if(!a||!c._valueOnFetch(a))d=c._index(a),c.select(d>-1?d:0)},_selectItem:function(){var a=this;a.dataSource.one(h,function(){var b=a.options.value||a.value();b?a.value(b):a.select(a.options.index),a.trigger("selected")}).fetch()},_accept:function(a){this._focus(a)},_optionLabelText:function(){var a=this.options,b=a.dataTextField,c=a.optionLabel;if(c&&b&&typeof c=="object")return this._text(c);return c},_data:function(){var a=this,b=a.options,d=b.optionLabel,e=b.dataTextField,f=b.dataValueField,g=a.dataSource.view(),h=g.length,i=d,j=0;if(d&&h){typeof d=="object"?i=d:e&&(i={},e=e.split("."),f=f.split("."),r(i,f,""),r(i,e,d)),i=new c.data.ObservableArray([i]);for(;j<h;j++)i.push(g[j]);g=i}return g},_keydown:function(a){var b=this,d=a.keyCode,e=c.keys,f=b.ul[0];b._move(a),d===e.HOME?(a.preventDefault(),b._select(f.firstChild)):d===e.END&&(a.preventDefault(),b._select(f.lastChild))},_keypress:function(a){var b=this;setTimeout(function(){b._word+=String.fromCharCode(a.keyCode||a.charCode),b._search()})},_popup:function(){e.fn._popup.call(this),this.popup.one("open",function(){this.wrapper=c.wrap(this.element).addClass("km-popup")})},_search:function(){var a=this;clearTimeout(a._typing),a._typing=setTimeout(function(){a._word=""},a.options.delay),a.search(a._word)},_select:function(a){var c=this,e=c._current,f=c._data(),g,h,i;a=c._get(a),a&&a[0]&&!a.hasClass(m)&&(e&&e.removeClass(m),i=d.List.inArray(a[0],c.ul[0]),i>-1&&(f=f[i],h=c._text(f),g=c._value(f),c.selectedIndex=i,c.text(h),c._accessor(g!==b?g:h,i),c.current(a.addClass(m))))},_mobile:function(){var a=this,b=a.popup,c=b.element.parents(".km-root").eq(0);c.length&&f&&(b.options.animation.open.effects=f.android||f.meego?"fadeIn":f.ios?"slideIn:up":b.options.animation.open.effects)},_span:function(){var b=this,c=b.wrapper,d=".k-input",e;e=c.find(d),e[0]||(c.append('<span unselectable="on" class="k-dropdown-wrap k-state-default"><span unselectable="on" class="k-input">&nbsp;</span><span class="k-select"><span class="k-icon k-i-arrow-s">select</span></span></span>').append(b.element),e=c.find(d)),b.span=e,b._inputWrapper=a(c[0].firstChild),b._arrow=c.find(".k-icon").mousedown(function(a){a.preventDefault()})},_wrapper:function(){var a=this,b=a.element,c=b[0],d=b.attr(n),e;e=b.parent(),e.is("span.k-widget")||(e=b.wrap("<span />").parent()),e.attr(n,d||0),e[0].style.cssText=c.style.cssText,b.hide(),a._focused=a.wrapper=e.attr("unselectable","on").addClass("k-widget k-dropdown k-header").addClass(c.className)}});d.plugin(q)}(jQuery),function(a,b){var c=window.kendo,d=c.ui,e=d.List,f=d.Select,g=c.support,h=g.placeholder,i=f.removeFiltersForField,j=c.keys,k=g.touch?"touchend":"click",l="disabled",m="change",n="k-state-default",o="k-state-disabled",p="k-state-focused",q="mousedown",r="select",s="k-state-selected",t="filter",u="accept",v="rebind",w="mouseenter mouseleave",x=null,y=a.proxy,z=f.extend({init:function(b,d){var e=this,g,h;d=a.isArray(d)?{dataSource:d}:d,f.fn.init.call(e,b,d),d=e.options,b=e.element.focus(function(){e.input.focus()}),d.placeholder=d.placeholder||b.attr("placeholder"),e._reset(),e._wrapper(),e._input(),e._popup(),e._accessors(),e._dataSource(),e._enable(),e._cascade(),g=e._inputWrapper,e.input.bind({keydown:y(e._keydown,e),focus:function(){g.addClass(p),e._placeholder(!1)},blur:function(){g.removeClass(p),clearTimeout(e._typing),e.text(e.text()),e._placeholder(),e._blur()}}),e._oldIndex=e.selectedIndex=-1,e._old=e.value(),d.autoBind?e._selectItem():(h=d.text,!h&&b.is(r)&&(h=b.children(":selected").text()),h&&e.input.val(h)),h||e._placeholder(),c.notify(e)},options:{name:"ComboBox",enable:!0,index:-1,autoBind:!0,delay:200,dataTextField:"",dataValueField:"",minLength:0,height:200,highlightFirst:!0,template:"",filter:"none",placeholder:"",suggest:!1,ignoreCase:!0,animation:{}},events:["open","close",m,"select","dataBinding","dataBound"],setOptions:function(a){f.fn.setOptions.call(this,a),this._template(),this._accessors()},current:function(a){var c=this,d=c._current;if(a===b)return d;d&&d.removeClass(s),f.fn.current.call(c,a)},enable:function(a){var b=this,c=b.input.add(b.element),d=b._inputWrapper.unbind(w),e=b._arrow.parent().unbind(k+" "+q);a===!1?(d.removeClass(n).addClass(o),c.attr(l,l)):(d.removeClass(o).addClass(n).bind(w,b._toggleHover),c.removeAttr(l),e.bind(k,function(){b.toggle()}).bind(q,function(a){a.preventDefault()}))},open:function(){var a=this,b=a.dataSource.options.serverFiltering;a.popup.visible()||(!a.ul[0].firstChild||a._state===u&&!b?(a._open=!0,a._state=v,a._selectItem()):(a.popup.open(),a._scroll(a._current)))},refresh:function(){var b=this,d=b.ul[0],e=b.options,f=b.value(),g=b._data(),h=g.length;b.trigger("dataBinding"),d.innerHTML=c.render(b.template,g),b._height(h),b.element.is(r)&&(b._options(g),f&&b._state===v&&(b._state="",b.value(f))),h&&(e.highlightFirst&&b.current(a(d.firstChild)),e.suggest&&b.input.val()&&b.suggest(a(d.firstChild))),b._open&&(b._open=!1,b.toggle(!!h)),b._touchScroller&&b._touchScroller.reset(),b._makeUnselectable(),b._hideBusy(),b.trigger("dataBound")},select:function(a){var c=this;if(a===b)return c.selectedIndex;c._select(a),c._old=c._accessor(),c._oldIndex=c.selectedIndex},search:function(a){a=typeof a=="string"?a:this.text();var b=this,c=a.length,d=b.options,e=d.ignoreCase,f=d.filter,g=d.dataTextField,h,j;clearTimeout(b._typing),c>=d.minLength&&(f==="none"?b._filter(a):(b._open=!0,b._state=t,j=b.dataSource.filter()||{},i(j,g),h=j.filters||[],h.push({value:e?a.toLowerCase():a,field:g,operator:f,ignoreCase:e}),b.dataSource.filter(h)))},suggest:function(a){var c=this,d=c.input[0],f=c.text(),g=e.caret(d),h=c._last,i;if(h==j.BACKSPACE||h==j.DELETE)c._last=b;else{a=a||"",typeof a!="string"&&(i=e.inArray(a[0],c.ul[0]),i>-1?a=c._text(c.dataSource.view()[i]):a=""),g<=0&&(g=f.toLowerCase().indexOf(a.toLowerCase())+1),a?(i=a.toLowerCase().indexOf(f.toLowerCase()),i>-1&&(f+=a.substring(i+f.length))):f=f.substring(0,g);if(f.length!==g||!a)d.value=f,e.selectText(d,g,f.length)}},text:function(a){a=a===null?"":a;var c=this,d=c._text,e=c.input[0],f=c.options.ignoreCase,g=a,h;if(a===b)return e.value;h=c.dataItem();if(!h||d(h)!==a)f&&(g=g.toLowerCase()),c._select(function(a){a=d(a),f&&(a=(a+"").toLowerCase());return a===g}),c.selectedIndex<0&&(c._custom(a),e.value=a)},toggle:function(a){var b=this;b._toggle(a)},value:function(a){var c=this,d;if(a===b)return c._accessor();a!==null&&(a=a.toString());if(!a||!c._valueOnFetch(a))d=c._index(a),d>-1?c.select(d):(c.current(x),c._custom(a),c.text(a),c._placeholder()),c._old=c._accessor(),c._oldIndex=c.selectedIndex},_accept:function(a){var b=this;a&&b.popup.visible()?(b._state===t&&(b._state=u),b._focus(a)):(b.text(b.text()),b._change())},_custom:function(b){var c=this,d=c.element,e=c._option;d.is(r)?(e||(e=c._option=a("<option/>"),d.append(e)),e.text(b),e[0].selected=!0):d.val(b)},_filter:function(a){var c=this,d=c.options,e=c.dataSource,f=d.ignoreCase,g=function(d){var e=c._text(d);if(e!==b){e=e+"";if(e!==""&&a==="")return!1;f&&(e=e.toLowerCase());return e.indexOf(a)===0}};f&&(a=a.toLowerCase());c.ul[0].firstChild?(c._highlight(g)!==-1&&(d.suggest&&c._current&&c.suggest(c._current),c.open()),c._hideBusy()):e.one(m,function(){e.data()[0]&&c.search(a)}).fetch()},_highlight:function(c){var d=this,f;if(c===b||c===null)return-1;c=d._get(c),f=e.inArray(c[0],d.ul[0]),f==-1&&(d.options.highlightFirst&&!d.text()?c=a(d.ul[0].firstChild):c=x),d.current(c);return f},_input:function(){var b=this,c=b.element[0],d=c.tabIndex,e=b.wrapper,f=".k-input",g,i,j=c.name||"";j&&(j='name="'+j+'_input" '),g=e.find(f),g[0]||(e.append('<span unselectable="on" class="k-dropdown-wrap k-state-default"><input '+j+'class="k-input" type="text" autocomplete="off"/><span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-arrow-s">select</span></span></span>').append(b.element),g=e.find(f)),i=g[0],i.tabIndex=d,i.style.cssText=c.style.cssText,c.maxLength>-1&&(i.maxLength=c.maxLength),g.addClass(c.className).val(c.value).css({width:"100%",height:c.style.height}).show(),h&&g.attr("placeholder",b.options.placeholder),b._focused=b.input=g,b._arrow=e.find(".k-icon"),b._inputWrapper=a(e[0].firstChild)},_keydown:function(a){var b=this,c=a.keyCode,d=b.input;b._last=c,clearTimeout(b._typing),c==j.TAB?(b.text(d.val()),b._state===t&&b.selectedIndex>-1&&(b._state=u)):b._move(a)||b._search()},_placeholder:function(a){if(!h){var c=this,d=c.input,e=c.options.placeholder,f;if(e){f=c.value(),a===b&&(a=!f),d.toggleClass("k-readonly",a);if(!a)if(!f)e="";else return;d.val(e)}}},_search:function(){var a=this;a._typing=setTimeout(function(){var b=a.text();a._prev!==b&&(a._prev=b,a.search(b))},a.options.delay)},_select:function(a){var c=this,d,e,f=c._highlight(a),g=c._data();c.selectedIndex=f,f!==-1&&(c._current.addClass(s),g=g[f],d=c._text(g),e=c._value(g),c._prev=c.input[0].value=d,c._accessor(e!==b?e:d,f),c._placeholder())},_selectItem:function(){var a=this,b=a.options,c=a.dataSource,d=c.filter()||{};i(d,a.options.dataTextField),a.dataSource.one(m,function(){var c=b.value||a.value();c?a.value(c):a.select(b.index),a.trigger("selected")}).filter(d)},_wrapper:function(){var a=this,b=a.element,c;c=b.parent(),c.is("span.k-widget")||(c=b.hide().wrap("<span />").parent()),c[0].style.cssText=b[0].style.cssText,a.wrapper=c.addClass("k-widget k-combobox k-header").addClass(b[0].className).show()}});d.plugin(z)}(jQuery),function(a,b){function s(a){String.prototype.trim?a=a.trim():a=a.replace(/^\s\s*/,"").replace(/\s\s*$/,"");return a.replace(/&nbsp;/gi,"")}var c=window.kendo,d=c.ui,e=a.proxy,f=a.extend,g=a.grep,h=a.map,i=a.inArray,j="k-state-selected",k="asc",l="desc",m="click",n="change",o="kendoPopup",p="kendoFilterMenu",q="kendoMenu",r=d.Widget,t=r.extend({init:function(b,d){var f=this,g;r.fn.init.call(f,b,d),b=f.element,d=f.options,f.owner=d.owner,f.field=b.attr(c.attr("field")),g=b.find(".k-header-column-menu"),g[0]||(g=b.prepend('<a class="k-header-column-menu" href="#"><span class="k-icon k-i-arrowhead-s"/></a>').find(".k-header-column-menu")),f._clickHandler=e(f._click,f),g.click(f._clickHandler),f.link=g,f.wrapper=a('<div class="k-column-menu"/>'),f.wrapper.html(c.template(u)({ns:c.ns,messages:d.messages,sortable:d.sortable,filterable:d.filterable,columns:f._ownerColumns(),showColumns:d.columns})),f.popup=f.wrapper[o]({anchor:g,open:e(f._open,f)}).data(o),f._menu(),f._sort(),f._columns(),f._filter()},options:{name:"ColumnMenu",messages:{sortAscending:"Sort Ascending",sortDescending:"Sort Descending",filter:"Filter",columns:"Columns"},columns:!0,sortable:!0,filterable:!0},destroy:function(){var a=this;a.filterMenu&&(a.filterMenu.destroy(),a.filterMenu=null),a.wrapper.children().removeData(q),a.wrapper.removeData(o).remove(),a.link.unbind(m,a._clickHandler),a.element.removeData("kendoColumnMenu"),a.columns=null},close:function(){this.menu.close(),this.popup.close()},_click:function(a){a.preventDefault(),a.stopPropagation(),this.popup.toggle()},_open:function(){a(".k-column-menu").not(this.wrapper).each(function(){a(this).data(o).close()})},_ownerColumns:function(){var a=this.owner.columns,b=g(a,function(a){var b=!0,c=s(a.title||"");if(a.menu===!1||!a.field&&!c.length)b=!1;return b});return h(b,function(b){return{field:b.field,title:b.title||b.field,hidden:b.hidden,index:i(b,a)}})},_menu:function(){this.menu=this.wrapper.children()[q]({orientation:"vertical",closeOnClick:!1}).data(q)},_sort:function(){var b=this;b.options.sortable&&(b.refresh(),b.options.dataSource.bind(n,e(b.refresh,b)),b.menu.element.delegate(".k-sort-asc, .k-sort-desc",m,function(){var c=a(this),d=c.hasClass("k-sort-asc")?k:l;c.parent().find(".k-sort-"+(d==k?l:k)).removeClass(j),b._sortDataSource(c,d),b.close()}))},_sortDataSource:function(a,c){var d=this,e=d.options.sortable,f=d.options.dataSource,g,h,i=f.sort()||[];a.hasClass(j)&&e&&e.allowUnsort!==!1?(a.removeClass(j),c=b):a.addClass(j);if(e===!0||e.mode==="single")i=[{field:d.field,dir:c}];else{for(g=0,h=i.length;g<h;g++)if(i[g].field===d.field){i.splice(g,1);break}i.push({field:d.field,dir:c})}f.sort(i)},_columns:function(){var b=this;b.options.columns&&(b._updateColumnsMenu(),b.owner.bind(["columnHide","columnShow"],function(){b._updateColumnsMenu()}),b.wrapper.delegate("[type=checkbox]",n,function(d){var e=a(this),f=parseInt(e.attr(c.attr("index")),10);e.is(":checked")?b.owner.showColumn(f):b.owner.hideColumn(f)}))},_updateColumnsMenu:function(){var a=this._ownerColumns(),b=h(a,function(a){return"["+c.attr("index")+"="+a.index+"]"}).join(","),d=g(a,function(a){return!a.hidden}),e=h(d,function(a){return"["+c.attr("index")+"="+a.index+"]"}).join(",");this.wrapper.find(b).attr("checked",!1),this.wrapper.find(e).attr("checked",!0).attr("disabled",d.length==1)},_filter:function(){var a=this,b=a.options;b.filterable!==!1&&(a.filterMenu=a.wrapper.find(".k-filterable")[p](f(!0,{},{appendToElement:!0,dataSource:b.dataSource,values:b.values,field:a.field},b.filterable)).data(p))},refresh:function(){var a=this,b=a.options.dataSource.sort()||[],c,d=a.field,e,f;a.wrapper.find(".k-sort-asc, .k-sort-desc").removeClass(j);for(e=0,f=b.length;e<f;e++)c=b[e],d==c.field&&a.wrapper.find(".k-sort-"+c.dir).addClass(j)}}),u='<ul>#if(sortable){#<li class="k-item k-sort-asc"><span class="k-link"><span class="k-sprite k-i-sort-asc"></span>${messages.sortAscending}</span></li><li class="k-item k-sort-desc"><span class="k-link"><span class="k-sprite k-i-sort-desc"></span>${messages.sortDescending}</span></li>#if(showColumns || filterable){#<li class="k-separator"></li>#}##}##if(showColumns){#<li class="k-item k-columns-item"><span class="k-link"><span class="k-sprite k-i-columns"></span>${messages.columns}</span><ul>#for (var col in columns) {#<li><label><input type="checkbox" data-#=ns#field="#=columns[col].field#" data-#=ns#index="#=columns[col].index#"/>#=columns[col].title#</label></li>#}#</ul></li>#if(filterable){#<li class="k-separator"></li>#}##}##if(filterable){#<li class="k-item k-filter-item"><span class="k-link"><span class="k-sprite k-filter"></span>${messages.filter}</span><ul><li><div class="k-filterable"></div></li></ul></li>#}#</ul>';d.plugin(t)}(jQuery),function(a,b){function bm(a,b){var c,d,e;if(typeof a===U&&a===b)return a;if(j(a)&&a.name===b)return a;if(n(a))for(c=0,d=a.length;c<d;c++){e=a[c];if(typeof e===U&&e===b||e.name===b)return e}return null}function bk(b,d,e,f){var g=b.find(">colgroup"),h,i=l(d,function(a){h=a.width;if(h&&parseInt(h,10)!==0)return c.format('<col style="width:{0}"/>',typeof h===U?h:h+"px");return"<col />"});(e||g.find(".k-hierarchy-col").length)&&i.splice(0,0,'<col class="k-hierarchy-col" />'),g.length&&g.remove(),g=a("<colgroup/>").append(a(Array(f+1).join('<col class="k-group-col">')+i.join(""))),b.prepend(g)}function bj(a){a=a||{};var b=a.style;b&&(a.style=b.replace(/((.*)?)(display\s*:\s*none)\s*;?/i,"$1"));return a}function bi(a){a=a||{};var b=a.style;b?(b=b.replace(/((.*)?display)(.*)?:([^;]*)/i,"$1:none"),b===a.style&&(b=b.replace(/(.*)?/i,"display:none;$1"))):b="display:none";return k({},a,{style:b})}function bh(a){return m(a,function(a){return!a.hidden})}function bg(a,b,c){var d,e,f,g;c=n(c)?c:[c];for(d=0,e=c.length;d<e;d++)f=c[d],j(f)&&f.click&&(g=f.name||f.text,b.on(Q,"a.k-grid-"+g,{commandName:g},p(f.click,a)))}function bf(a,b,c){var d=a.eq(b),e=a.eq(c);d[b>c?"insertBefore":"insertAfter"](e)}function be(a){var b,c,d={},e={};if(!r(a)){n(a)||(a=[a]);for(b=0,c=a.length;b<c;b++)d[a[b].aggregate]=0,e[a[b].field]=d}return e}function bd(b,c){a("th, th .k-grid-filter, th .k-link",b).add(document.body).css("cursor",c)}function bc(b){var c=0;a("> .k-grouping-header, > .k-grid-toolbar",b).each(function(){c+=this.offsetHeight});return c}function ba(a){var b,c=" ";if(a){if(typeof a===U)return a;for(b in a)c+=b+'="'+a[b]+'"'}return c}function _(a){return Array(a+1).join('<td class="k-group-cell">&nbsp;</td>')}var c=window.kendo,d=c.ui,e=c.data.DataSource,f=d.Groupable,g=c.support.tbodyInnerHtml,h=d.Widget,i=c.keys,j=a.isPlainObject,k=a.extend,l=a.map,m=a.grep,n=a.isArray,o=a.inArray,p=a.proxy,q=a.isFunction,r=a.isEmptyObject,s=Math,t="requestStart",u="error",v="tbody>tr:not(.k-grouping-row,.k-detail-row,.k-group-footer):visible",w=":not(.k-group-cell,.k-hierarchy-cell):visible",x="tbody>tr:not(.k-grouping-row,.k-detail-row,.k-group-footer) > td:not(.k-group-cell,.k-hierarchy-cell)",y=v+">td"+w,z=y+":first",A="edit",B="save",C="remove",D="detailInit",E="change",F="columnHide",G="columnShow",H="saveChanges",I="dataBound",J="detailExpand",K="detailCollapse",L="k-state-focused",M="k-focusable",N="k-state-selected",O="columnResize",P="columnReorder",Q="click",R="height",S="tabIndex",T="function",U="string",V="Are you sure you want to delete this record?",W=/(\}|\#)/ig,X=3,Y=/#/ig,Z='<a class="k-button k-button-icontext #=className#" #=attr# href="\\#"><span class="#=iconClass# #=imageClass#"></span>#=text#</a>',$=h.extend({init:function(a,b){var c=this;h.fn.init.call(c,a,b),c._refreshHandler=p(c.refresh,c),c.setDataSource(b.dataSource),c.wrap()},setDataSource:function(a){var b=this;b.dataSource&&b.dataSource.unbind(E,b._refreshHandler),b.dataSource=a,b.dataSource.bind(E,b._refreshHandler)},options:{name:"VirtualScrollable",itemHeight:a.noop},wrap:function(){var b=this,d=c.support.scrollbar()+1,e=b.element;e.css({width:"auto",paddingRight:d,overflow:"hidden"}),b.content=e.children().first(),b.wrapper=b.content.wrap('<div class="k-virtual-scrollable-wrap"/>').parent().bind("DOMMouseScroll",p(b._wheelScroll,b)).bind("mousewheel",p(b._wheelScroll,b)),c.support.touch&&(b.drag=new c.Drag(b.wrapper,{global:!0,move:function(a){b.verticalScrollbar.scrollTop(b.verticalScrollbar.scrollTop()-a.y.delta),a.preventDefault()}})),b.verticalScrollbar=a('<div class="k-scrollbar k-scrollbar-vertical" />').css({width:d}).appendTo(e).bind("scroll",p(b._scroll,b))},_wheelScroll:function(b){var c=this,d=c.verticalScrollbar.scrollTop(),e=b.originalEvent,f;b.preventDefault(),e.wheelDelta?f=e.wheelDelta:e.detail?f=-e.detail*10:a.browser.opera&&(f=-e.wheelDelta),c.verticalScrollbar.scrollTop(d+ -f)},_scroll:function(a){var b=this,c=a.currentTarget.scrollTop,d=b.dataSource,e=b.itemHeight,f=d.skip()||0,g=b._rangeStart||f,h=b.element.innerHeight(),i=!!(b._scrollbarTop&&b._scrollbarTop>c),j=s.max(s.floor(c/e),0),k=s.max(j+s.floor(h/e),0);b._scrollTop=c-g*e,b._scrollbarTop=c,b._fetch(j,k,i)||(b.wrapper[0].scrollTop=b._scrollTop)},_fetch:function(a,b,c){var d=this,e=d.dataSource,f=d.itemHeight,g=e.take(),h=d._rangeStart||e.skip()||0,i=s.floor(a/g)*g,j=!1,k=.33;a<h?(j=!0,h=s.max(0,b-g),d._scrollTop=(a-h)*f,d._page(h,g)):b>=h+g&&!c?(j=!0,h=a,d._scrollTop=f,d._page(h,g)):d._fetching||(a<i+g-g*k&&a>g&&e.prefetch(i-g,g),b>i+g*k&&e.prefetch(i+g,g));return j},_page:function(a,b){var d=this,e=d.dataSource;clearTimeout(d._timeout),d._fetching=!0,d._rangeStart=a,e.inRange(a,b)?e.range(a,b):(c.ui.progress(d.wrapper.parent(),!0),d._timeout=setTimeout(function(){e.range(a,b)},100))},refresh:function(){var a=this,b="",d=25e4,e=a.dataSource,f=a._rangeStart,g=c.support.scrollbar(),h=a.wrapper[0],i,j,k;c.ui.progress(a.wrapper.parent(),!1),clearTimeout(a._timeout),k=a.itemHeight=a.options.itemHeight()||0;var l=h.scrollWidth>h.offsetWidth?g:0;i=e.total()*k+l;for(j=0;j<s.floor(i/d);j++)b+='<div style="width:1px;height:'+d+'px"></div>';i%d&&(b+='<div style="width:1px;height:'+i%d+'px"></div>'),a.verticalScrollbar.html(b),h.scrollTop=a._scrollTop,a.drag&&a.drag.cancel(),f&&!a._fetching&&(a._rangeStart=e.skip()),a._fetching=!1}}),bb={create:{text:"Add new record",imageClass:"k-add",className:"k-grid-add",iconClass:"k-icon"},cancel:{text:"Cancel changes",imageClass:"k-cancel",className:"k-grid-cancel-changes",iconClass:"k-icon"},save:{text:"Save changes",imageClass:"k-update",className:"k-grid-save-changes",iconClass:"k-icon"},destroy:{text:"Delete",imageClass:"k-delete",className:"k-grid-delete",iconClass:"k-icon"},edit:{text:"Edit",imageClass:"k-edit",className:"k-grid-edit",iconClass:"k-icon"},update:{text:"Update",imageClass:"k-update",className:"k-grid-update",iconClass:"k-icon"},canceledit:{text:"Cancel",imageClass:"k-cancel",className:"k-grid-cancel",iconClass:"k-icon"}},bl=h.extend({init:function(a,b){var d=this;b=n(b)?{dataSource:b}:b,h.fn.init.call(d,a,b),d._element(),d._columns(d.options.columns),d._dataSource(),d._tbody(),d._pageable(),d._thead(),d._groupable(),d._toolbar(),d._setContentHeight(),d._templates(),d._navigatable(),d._selectable(),d._details(),d._editable(),d._attachCustomCommandsEvent(),d.options.autoBind&&d.dataSource.fetch(),c.notify(d)},events:[E,"dataBinding",I,J,K,D,A,B,C,H,O,P,G,F],setDataSource:function(a){var b=this;b.options.dataSource=a,b._dataSource(),b._pageable(),b.options.groupable&&b._groupable(),b._thead(),b.virtualScrollable&&b.virtualScrollable.setDataSource(b.options.dataSource),b.options.autoBind&&a.fetch()},options:{name:"Grid",columns:[],toolbar:null,autoBind:!0,filterable:!1,scrollable:!0,sortable:!1,selectable:!1,navigatable:!1,pageable:!1,editable:!1,groupable:!1,rowTemplate:"",altRowTemplate:"",dataSource:{},height:null,resizable:!1,reorderable:!1,columnMenu:!1},setOptions:function(a){var b=this;h.fn.setOptions.call(this,a),b._templates()},items:function(){return this.tbody.children(":not(.k-grouping-row,.k-detail-row,.k-group-footer)")},_attachCustomCommandsEvent:function(){var a=this,b=a.columns||[],c,d,e;for(d=0,e=b.length;d<e;d++)c=b[d].command,c&&bg(a,a.wrapper,c)},_element:function(){var b=this,c=b.element;c.is("table")||(b.options.scrollable?c=b.element.find("> .k-grid-content > table"):c=b.element.children("table"),c.length||(c=a("<table />").appendTo(b.element))),b.table=c.attr("cellspacing",0),b._wrapper()},_positionColumnResizeHandle:function(b){var c=this,d=c.options.scrollable,e=c.resizeHandle,f;c.thead.on("mousemove","th:not(.k-group-cell,.k-hierarchy-cell)",function(g){var h=a(this),i=h.offset().left+this.offsetWidth;g.clientX>i-X&&g.clientX<i+X?(bd(c.wrapper,h.css("cursor")),e||(e=c.resizeHandle=a('<div class="k-resize-handle"/>'),b.append(e)),f=this.offsetWidth,h.prevAll().each(function(){f+=this.offsetWidth}),e.css({top:d?0:bc(c.wrapper),left:f-X,height:h.outerHeight(),width:X*3}).data("th",h).show()):(bd(c.wrapper,""),e&&e.hide())})},_resizable:function(){var b=this,c=b.options,d,e,f,g,h;c.resizable&&(d=c.scrollable?b.wrapper.find(".k-grid-header-wrap"):b.wrapper,b._positionColumnResizeHandle(d),d.kendoResizable({handle:".k-resize-handle",hint:function(c){return a('<div class="k-grid-resize-indicator" />').css({height:c.data("th").outerHeight()+b.tbody.attr("clientHeight")})},start:function(d){var i=a(d.currentTarget).data("th"),j=a.inArray(i[0],i.parent().children(":visible")),k=b.tbody.parent(),l=b.footer||a();bd(b.wrapper,i.css("cursor")),c.scrollable?h=b.thead.parent().find("col:eq("+j+")").add(k.children("colgroup").find("col:eq("+j+")")).add(l.find("colgroup").find("col:eq("+j+")")):h=k.children("colgroup").find("col:eq("+j+")"),e=d.x.location,f=i.outerWidth(),g=b.tbody.outerWidth()},resize:function(d){var i=f+d.x.location-e,j=b.footer||a();i>10&&(h.css("width",i),c.scrollable&&(b._footerWidth=g+d.x.location-e,b.tbody.parent().add(b.thead.parent()).add(j.find("table")).css("width",b._footerWidth)))},resizeend:function(c){var d=a(c.currentTarget).data("th"),e=d.outerWidth(),g;bd(b.wrapper,""),f!=e&&(g=b.columns[d.parent().find("th:not(.k-group-cell,.k-hierarchy-cell)").index(d)],g.width=e,b.trigger(O,{column:g,oldWidth:f,newWidth:e})),b.resizeHandle.hide()}}))},_draggable:function(){var b=this;b.options.reorderable&&(b._draggableInstance=b.thead.kendoDraggable({group:c.guid(),filter:".k-header:not(.k-group-cell,.k-hierarchy-cell):visible["+c.attr("field")+"]",hint:function(b){return a('<div class="k-header k-drag-clue" />').css({width:b.width(),paddingLeft:b.css("paddingLeft"),paddingRight:b.css("paddingRight"),lineHeight:b.height()+"px",paddingTop:b.css("paddingTop"),paddingBottom:b.css("paddingBottom")}).html(b.attr(c.attr("title"))||b.attr(c.attr("field"))).prepend('<span class="k-icon k-drag-status k-denied" />')}}).data("kendoDraggable"))},_reorderable:function(){var a=this;a.options.reorderable&&a.thead.kendoReorderable({draggable:a._draggableInstance,change:function(b){var c=bh(a.columns)[b.oldIndex];a.trigger(P,{newIndex:b.newIndex,oldIndex:b.oldIndex,column:c}),a.reorderColumn(b.newIndex,c)}})},reorderColumn:function(b,c){var d=this,e=a.inArray(c,d.columns),f=a.inArray(c,bh(d.columns)),g,h,i,j=d.footer||d.wrapper.find(".k-grid-footer");if(e!==b){d.columns.splice(e,1),d.columns.splice(b,0,c),d._templates(),bf(d.thead.prev().find("col:not(.k-group-col,.k-hierarchy-col)"),f,b),d.options.scrollable&&bf(d.tbody.prev().find("col:not(.k-group-col,.k-hierarchy-col)"),f,b),bf(d.thead.find(".k-header:not(.k-group-cell,.k-hierarchy-cell)"),e,b),j&&j.length&&(bf(j.find(".k-grid-footer-wrap>table>colgroup>col:not(.k-group-col,.k-hierarchy-col)"),f,b),bf(j.find(".k-footer-template>td:not(.k-group-cell,.k-hierarchy-cell)"),e,b)),g=d.tbody.children(":not(.k-grouping-row,.k-detail-row)");for(h=0,i=g.length;h<i;h+=1)bf(g.eq(h).find(">td:not(.k-group-cell,.k-hierarchy-cell)"),e,b)}},cellIndex:function(b){return a(b).parent().find("td:not(.k-group-cell,.k-hierarchy-cell)").index(b)},_modelForContainer:function(b){b=a(b),!b.is("tr")&&this._editMode()!=="popup"&&(b=b.closest("tr"));var d=b.attr(c.attr("uid"));return this.dataSource.getByUid(d)},_editable:function(){var b=this,c=b.options.editable,d=function(){var c=document.activeElement,d=b._editContainer;d&&!a.contains(d[0],c)&&d[0]!==c&&!a(c).closest(".k-animation-container").length&&b.editable.end()&&b.closeCell()};if(c){var e=b._editMode();e==="incell"?c.update!==!1&&(b.wrapper.delegate("tr:not(.k-grouping-row) > td:not(.k-hierarchy-cell,.k-detail-cell,.k-group-cell,.k-edit-cell,:has(a.k-grid-delete))",Q,function(c){var d=a(this);d.closest("tbody")[0]===b.tbody[0]&&!a(c.target).is(":input")&&(b.editable?b.editable.end()&&(b.closeCell(),b.editCell(d)):b.editCell(d))}),b.wrapper.bind("focusin",function(a){clearTimeout(b.timer),b.timer=null}),b.wrapper.bind("focusout",function(a){b.timer=setTimeout(d,1)})):c.update!==!1&&b.wrapper.delegate("tbody>tr:not(.k-detail-row,.k-grouping-row):visible a.k-grid-edit",Q,function(c){c.preventDefault(),b.editRow(a(this).closest("tr"))}),c.destroy!==!1&&b.wrapper.delegate("tbody>tr:not(.k-detail-row,.k-grouping-row):visible a.k-grid-delete",Q,function(c){c.preventDefault(),b.removeRow(a(this).closest("tr"))})}},editCell:function(a){var b=this,c=b.columns[b.cellIndex(a)],d=b._modelForContainer(a);d&&(!d.editable||d.editable(c.field))&&!c.command&&c.field&&(b._attachModelChange(d),b._editContainer=a,b.editable=a.addClass("k-edit-cell").kendoEditable({fields:{field:c.field,format:c.format,editor:c.editor,values:c.values},model:d,change:function(c){b.trigger(B,{values:c.values,container:a,model:d})&&c.preventDefault()}}).data("kendoEditable"),a.parent().addClass("k-grid-edit-row"),b.trigger(A,{container:a,model:d}))},_destroyEditable:function(){var a=this;a.editable&&(a._detachModelChange(),a.editable.destroy(),delete a.editable,a._editMode()==="popup"&&a._editContainer.data("kendoWindow").close(),a._editContainer=null)},_attachModelChange:function(a){var b=this;b._modelChangeHandler=function(a){b._modelChange({field:a.field,model:this})},a.bind("change",b._modelChangeHandler)},_detachModelChange:function(){var a=this,b=a._editContainer,c=a._modelForContainer(b);c&&c.unbind(E,a._modelChangeHandler)},closeCell:function(){var b=this,d=b._editContainer.removeClass("k-edit-cell"),e=d.closest("tr").attr(c.attr("uid")),f=b.columns[b.cellIndex(d)],g=b.dataSource.getByUid(e);d.parent().removeClass("k-grid-edit-row"),b._destroyEditable(),b._displayCell(d,f,g),d.hasClass("k-dirty-cell")&&a('<span class="k-dirty"/>').prependTo(d)},_displayCell:function(a,b,d){var e=this,f={storage:{},count:0},g=k({},c.Template,e.options.templateSettings),h=c.template(e._cellTmpl(b,f),g);f.count>0&&(h=p(h,f.storage)),a.empty().html(h(d))},removeRow:function(b){var c=this,d,e;!c._confirmation()||(b=a(b).hide(),d=c._modelForContainer(b),d&&!c.trigger(C,{row:b,model:d})&&(c.dataSource.remove(d),e=c._editMode(),(e==="inline"||e==="popup")&&c.dataSource.sync()))},_editMode:function(){var a="incell",b=this.options.editable;b!==!0&&(typeof b=="string"?a=b:a=b.mode||a);return a},editRow:function(b){var c=this,d=c._modelForContainer(b),e=c._editMode(),f;c.cancelRow(),d&&(c._attachModelChange(d),e==="popup"?c._createPopupEditor(d):e==="inline"?c._createInlineEditor(b,d):e==="incell"&&a(b).children(w).each(function(){var b=a(this),e=c.columns[b.index()];d=c._modelForContainer(b);if(d&&(!d.editable||d.editable(e.field))&&e.field){c.editCell(b);return!1}}),f=c._editContainer,f.delegate("a.k-grid-cancel",Q,function(a){a.preventDefault(),c.cancelRow()}),f.delegate("a.k-grid-update",Q,function(a){a.preventDefault(),c.saveRow()}))},_createPopupEditor:function(b){var d=this,e="<div "+c.attr("uid")+'="'+b.uid+'"><div class="k-edit-form-container">',f,g,h=[],i,l,m,n,o,q,r=d.options.editable,s=j(r)?r.window:{},t=k({},c.Template,d.options.templateSettings);if(r.template){e+=c.template(window.unescape(r.template),t)(b);for(i=0,l=d.columns.length;i<l;i++)f=d.columns[i],f.command&&(g=bm(f.command,"edit"))}else for(i=0,l=d.columns.length;i<l;i++){f=d.columns[i];if(!f.command){e+='<div class="k-edit-label"><label for="'+f.field+'">'+(f.title||f.field||"")+"</label></div>";if((!b.editable||b.editable(f.field))&&f.field)h.push({field:f.field,format:f.format,editor:f.editor,values:f.values}),e+="<div "+c.attr("container-for")+'="'+f.field+'" class="k-edit-field"></div>';else{var u={storage:{},count:0};m=c.template(d._cellTmpl(f,u),t),u.count>0&&(m=p(m,u.storage)),e+='<div class="k-edit-field">'+m(b)+"</div>"}}else f.command&&(g=bm(f.command,"edit"))}g&&j(g)&&(g.text&&j(g.text)&&(n=g.text.update,o=g.text.cancel),g.attr&&(q=g.attr)),e+=d._createButton({name:"update",text:n,attr:q})+d._createButton({name:"canceledit",text:o,attr:q}),e+="</div></div>";var v=d._editContainer=a(e).appendTo(d.wrapper).eq(0).kendoWindow(k({modal:!0,resizable:!1,draggable:!0,title:"Edit",visible:!1},s)),w=v.data("kendoWindow");w.wrapper.delegate(".k-i-close","click",function(){d.cancelRow()}),d.editable=d._editContainer.kendoEditable({fields:h,model:b,clearContainer:!1}).data("kendoEditable"),w.center().open(),d.trigger(A,{container:v,model:b})},_createInlineEditor:function(b,c){var d=this,e,f,g,h=[];b.children(":not(.k-group-cell,.k-hierarchy-cell)").each(function(){f=a(this),e=d.columns[d.cellIndex(f)];if(!e.command&&e.field&&(!c.editable||c.editable(e.field)))h.push({field:e.field,format:e.format,editor:e.editor,values:e.values}),f.attr("data-container-for",e.field),f.empty();else if(e.command){g=bm(e.command,"edit");if(g){f.empty();var b,i,k;j(g)&&(g.text&&j(g.text)&&(b=g.text.update,i=g.text.cancel),g.attr&&(k=g.attr)),a(d._createButton({name:"update",text:b,attr:k})+d._createButton({name:"canceledit",text:i,attr:k})).appendTo(f)}}}),d._editContainer=b,d.editable=b.addClass("k-grid-edit-row").kendoEditable({fields:h,model:c,clearContainer:!1}).data("kendoEditable"),d.trigger(A,{container:b,model:c})},cancelRow:function(){var a=this,b=a._editContainer,d;b&&(d=a._modelForContainer(b),a.dataSource.cancelChanges(d),a._editMode()!=="popup"?a._displayRow(b):a._displayRow(a.items().filter("["+c.attr("uid")+"="+d.uid+"]")),a._destroyEditable())},saveRow:function(){var a=this,b=a._editContainer,c=a._modelForContainer(b),d=a.editable;b&&d&&d.end()&&!a.trigger(B,{container:b,model:c})&&(a._editMode()!=="popup"&&a._displayRow(b),a._destroyEditable(),a.dataSource.sync())},_displayRow:function(b){var c=this,d=c._modelForContainer(b);d&&b.replaceWith(a((b.hasClass("k-alt")?c.altRowTemplate:c.rowTemplate)(d)))},_showMessage:function(a){return window.confirm(a)},_confirmation:function(){var a=this,b=a.options.editable,c=b===!0||typeof b===U?V:b.confirmation;return c!==!1&&c!=null?a._showMessage(c):!0},cancelChanges:function(){this.dataSource.cancelChanges()},saveChanges:function(){var a=this;(a.editable&&a.editable.end()||!a.editable)&&!a.trigger(H)&&a.dataSource.sync()},addRow:function(){var a=this,b,d=a.dataSource,e=a._editMode(),f=a.options.editable.createAt||"",g=d.pageSize(),h=d.view()||[];if(a.editable&&a.editable.end()||!a.editable){e!="incell"&&a.cancelRow(),b=d.indexOf(h[0]),f.toLowerCase()=="bottom"&&(b+=h.length,g&&!d.options.serverPaging&&g<=h.length&&(b-=1)),b<0&&(b=0);var i=d.insert(b,{}),j=i.uid,k=a.table.find("tr["+c.attr("uid")+"="+j+"]"),l=k.children("td:not(.k-group-cell,.k-hierarchy-cell)").first();e!=="inline"&&e!=="popup"||!k.length?l.length&&a.editCell(l):a.editRow(k)}},_toolbar:function(){var b=this,d=b.wrapper,e=b.options.toolbar,f=b.options.editable,g,h;e&&(g=b.wrapper.find(".k-grid-toolbar"),g.length||(e=q(e)?e({}):typeof e===U?e:b._toolbarTmpl(e).replace(Y,"\\#"),h=p(c.template(e),b),g=a('<div class="k-toolbar k-grid-toolbar" />').html(h({})).prependTo(d)),f&&f.create!==!1&&g.delegate(".k-grid-add",Q,function(a){a.preventDefault(),b.addRow()}).delegate(".k-grid-cancel-changes",Q,function(a){a.preventDefault(),b.cancelChanges()}).delegate(".k-grid-save-changes",Q,function(a){a.preventDefault(),b.saveChanges()}))},_toolbarTmpl:function(a){var b=this,c,d,e="";if(n(a))for(c=0,d=a.length;c<d;c++)e+=b._createButton(a[c]);return e},_createButton:function(a){var b=a.template||Z,d=typeof a===U?a:a.name||a.text,e={className:"k-grid-"+d,text:d,imageClass:"",attr:"",iconClass:""};if(!d&&(!j(a)||!a.template))throw new Error("Custom commands should have name specified");j(a)?(a.className&&(a.className+=" "+e.className),d==="edit"&&j(a.text)&&(a=k(!0,{},a),a.text=a.text.edit),a.attr&&j(a.attr)&&(a.attr=ba(a.attr)),e=k(!0,e,bb[d],a)):e=k(!0,e,bb[d]);return c.template(b)(e)},_groupable:function(){var b=this,d=b.wrapper,e=b.options.groupable;b.groupable||b.table.delegate(".k-grouping-row .k-i-collapse, .k-grouping-row .k-i-expand",Q,function(c){var d=a(this),e=d.closest("tr");d.hasClass("k-i-collapse")?b.collapseGroup(e):b.expandGroup(e),c.preventDefault(),c.stopPropagation()}),e&&(d.has("div.k-grouping-header")[0]||a("<div />").addClass("k-grouping-header").html("&nbsp;").prependTo(d),b.groupable&&b.groupable.destroy(),b.groupable=new f(d,k({},e,{draggable:b._draggableInstance,groupContainer:">div.k-grouping-header",dataSource:b.dataSource,filter:".k-header:not(.k-group-cell,.k-hierarchy-cell):visible["+c.attr("field")+"]",allowDrag:b.options.reorderable})))},_selectable:function(){var a=this,b,d,e=a.options.selectable;e&&(b=typeof e===U&&e.toLowerCase().indexOf("multiple")>-1,d=typeof e===U&&e.toLowerCase().indexOf("cell")>-1,a.selectable=new c.ui.Selectable(a.table,{filter:">"+(d?x:"tbody>tr:not(.k-grouping-row,.k-detail-row,.k-group-footer)"),multiple:b,change:function(){a.trigger(E)}}),a.options.navigatable&&a.wrapper.keydown(function(c){var e=a.current();c.keyCode===i.SPACEBAR&&c.target==a.wrapper[0]&&!e.hasClass("k-edit-cell")&&(c.preventDefault(),c.stopPropagation(),e=d?e:e.parent(),b?c.ctrlKey?e.hasClass(N)&&(e.removeClass(N),e=null):a.selectable.clear():a.selectable.clear(),a.selectable.value(e))}))},clearSelection:function(){var a=this;a.selectable.clear(),a.trigger(E)},select:function(b){var c=this,d=c.selectable;b=a(b);if(b.length)d.options.multiple||(d.clear(),b=b.first()),d.value(b);else return d.value()},current:function(a){var c=this,d=c.options.scrollable,e=c._current;a!==b&&a.length&&(!e||e[0]!==a[0])&&(a.addClass(L),e&&e.removeClass(L),c._current=a,a.length&&d&&(c._scrollTo(a.parent()[0],c.content[0]),d.virtual?c._scrollTo(a[0],c.content.find(">.k-virtual-scrollable-wrap")[0]):c._scrollTo(a[0],c.content[0])));return c._current},_scrollTo:function(a,b){var c=a.tagName.toLowerCase()==="td",d=a[c?"offsetLeft":"offsetTop"],e=a[c?"offsetWidth":"offsetHeight"],f=b[c?"scrollLeft":"scrollTop"],g=b[c?"clientWidth":"clientHeight"],h=d+e;b[c?"scrollLeft":"scrollTop"]=f>d?d:h>f+g?h-g:f},_navigatable:function(){var b=this,c=b.wrapper,d=b.table.addClass(M),e=p(b.current,b),f="."+M+" "+y,g=a.browser,h=function(d){var f=a(d.currentTarget);f.closest("tbody")[0]===b.tbody[0]&&(e(f),a(d.target).is(":button,a,:input,a>.k-icon,textarea,span.k-icon,.k-input")||setTimeout(function(){c.focus()}),d.stopPropagation())};b.options.navigatable&&(c.bind({focus:function(){var a=b._current;a&&a.is(":visible")?a.addClass(L):e(b.table.find(z))},focusout:function(a){b._current&&b._current.removeClass(L),a.stopPropagation()},keydown:function(f){var h=f.keyCode,j=b.current(),k=f.shiftKey,l=b.dataSource,m=b.options.pageable,n=!a(f.target).is(":button,a,:input,a>.t-icon"),o=b._editMode()=="incell",p,q,r=!1;n&&i.UP===h?(e(j?j.parent().prevAll(v).last().children(":eq("+j.index()+"),:eq(0)").last():d.find(z)),r=!0):n&&i.DOWN===h?(e(j?j.parent().nextAll(v).first().children(":eq("+j.index()+"),:eq(0)").last():d.find(z)),r=!0):n&&i.LEFT===h?(e(j?j.prevAll(w+":first"):d.find(z)),r=!0):n&&i.RIGHT===h?(e(j?j.nextAll(":visible:first"):d.find(z)),r=!0):n&&m&&i.PAGEDOWN==h?(b._current=null,l.page(l.page()+1),r=!0):n&&m&&i.PAGEUP==h?(b._current=null,l.page(l.page()-1),r=!0):b.options.editable&&(j=j?j:d.find(z),i.ENTER==h||i.F2==h?(b._handleEditing(j),r=!0):i.TAB==h&&o?(q=k?j.prevAll(w+":first"):j.nextAll(":visible:first"),q.length||(q=j.parent()[k?"prevAll":"nextAll"]("tr:not(.k-grouping-row,.k-detail-row):visible").children(w+(k?":last":":first"))),q.length&&(b._handleEditing(j,q),r=!0)):i.ESC==h&&b._editContainer&&(b._editContainer.has(j[0])||j[0]===b._editContainer[0])&&(o?b.closeCell():(p=b.items().index(j.parent()),document.activeElement.blur(),b.cancelRow(),p>=0&&b.current(b.items().eq(p).children().filter(w).first())),g.msie&&parseInt(g.version,10)<9&&document.body.focus(),c.focus(),r=!0)),r&&(f.preventDefault(),f.stopPropagation())}}),c.delegate(f,"mousedown",h))},_handleEditing:function(b,c){var d=this,e=d._editMode(),f=d._editContainer,g,h;e=="incell"?h=b.hasClass("k-edit-cell"):h=b.parent().hasClass("k-grid-edit-row");if(d.editable){a.contains(f[0],document.activeElement)&&a(document.activeElement).blur();if(d.editable.end())e=="incell"?d.closeCell():(b.parent()[0]===f[0]?g=d.items().index(b.parent()):g=d.items().index(f),d.saveRow(),d.current(d.items().eq(g).children().filter(w).first()),h=!0);else{e=="incell"?d.current(f):d.current(f.children().filter(w).first()),f.find(":input:visible:first").focus();return}}c&&d.current(c),d.wrapper.focus();if(!h&&!c||c)e=="incell"?d.editCell(d.current()):d.editRow(d.current().parent())},_wrapper:function(){var a=this,b=a.table,c=a.options.height,d=a.element;d.is("div")||(d=d.wrap("<div/>").parent()),a.wrapper=d.addClass("k-grid k-widget").attr(S,s.max(b.attr(S)||0,0)),b.removeAttr(S),c&&(a.wrapper.css(R,c),b.css(R,"auto"))},_tbody:function(){var b=this,c=b.table,d;d=c.find(">tbody"),d.length||(d=a("<tbody/>").appendTo(c)),b.tbody=d},_scrollable:function(){var b=this,d,e,f=b.options,g=f.scrollable,h=c.support.scrollbar();if(g){d=b.wrapper.children(".k-grid-header"),d[0]||(d=a('<div class="k-grid-header" />').insertBefore(b.table)),d.css("padding-right",g.virtual?h+1:h),e=a('<table cellspacing="0" />'),e.append(b.thead),d.empty().append(a('<div class="k-grid-header-wrap" />').append(e)),b.content=b.table.parent(),b.content.is(".k-virtual-scrollable-wrap")&&(b.content=b.content.parent()),b.content.is(".k-grid-content, .k-virtual-scrollable-wrap")||(b.content=b.table.wrap('<div class="k-grid-content" />').parent()),g!==!0&&g.virtual&&!b.virtualScrollable&&(b.virtualScrollable=new $(b.content,{dataSource:b.dataSource,itemHeight:p(b._averageRowHeight,b)})),b.scrollables=d.children(".k-grid-header-wrap");if(g.virtual)b.content.find(">.k-virtual-scrollable-wrap").bind("scroll",function(){b.scrollables.scrollLeft(this.scrollLeft)});else{b.content.bind("scroll",function(){b.scrollables.scrollLeft(this.scrollLeft)});var i=c.touchScroller(b.content);i&&i.movable&&i.movable.bind("change",function(a){b.scrollables.scrollLeft(-a.sender.x)})}}},_setContentHeight:function(){var a=this,b=a.options,d=a.wrapper.innerHeight(),e=a.wrapper.children(".k-grid-header"),f=c.support.scrollbar();if(b.scrollable){d-=e.outerHeight(),a.pager&&(d-=a.pager.element.outerHeight()),b.groupable&&(d-=a.wrapper.children(".k-grouping-header").outerHeight()),b.toolbar&&(d-=a.wrapper.children(".k-grid-toolbar").outerHeight()),a.footerTemplate&&(d-=a.wrapper.children(".k-grid-footer").outerHeight());var g=function(a){var b,c;if(a[0].style.height)return!0;b=a.height(),a.height("auto"),c=a.height();if(b!=c){a.height("");return!0}a.height("");return!1};g(a.wrapper)&&(d>f*2?a.content.height(d):a.content.height(f*2+1))}},_averageRowHeight:function(){var a=this,b=a._rowHeight;a._rowHeight||(a._rowHeight=b=a.table.outerHeight()/a.table[0].rows.length,a._sum=b,a._measures=1);var c=a.table.outerHeight()/a.table[0].rows.length;b!==c&&(a._measures++,a._sum+=c,a._rowHeight=a._sum/a._measures);return b},_dataSource:function(){var a=this,c=a.options,d,f=c.dataSource;f=n(f)?{data:f}:f,j(f)&&(k(f,{table:a.table,fields:a.columns}),d=c.pageable,j(d)&&d.pageSize!==b&&(f.pageSize=d.pageSize)),a.dataSource&&a._refreshHandler?a.dataSource.unbind(E,a._refreshHandler).unbind(t,a._requestStartHandler).unbind(u,a._errorHandler):(a._refreshHandler=p(a.refresh,a),a._requestStartHandler=p(a._requestStart,a),a._errorHandler=p(a._error,a)),a.dataSource=e.create(f).bind(E,a._refreshHandler).bind(t,a._requestStartHandler).bind(u,a._errorHandler)},_error:function(){this._progress(!1)},_requestStart:function(){this._progress(!0)},_modelChange:function(b){var e=this,f=b.model,g=e.tbody.find("tr["+c.attr("uid")+"="+f.uid+"]"),h,i,j=g.hasClass("k-alt"),k,l,m;if(g.children(".k-edit-cell").length&&!e.options.rowTemplate)g.children(":not(.k-group-cell,.k-hierarchy-cell)").each(function(){h=a(this),i=e.columns[e.cellIndex(h)],i.field===b.field&&(h.hasClass("k-edit-cell")?h.addClass("k-dirty-cell"):(e._displayCell(h,i,f),a('<span class="k-dirty"/>').prependTo(h)))});else if(!g.hasClass("k-grid-edit-row")){k=a((j?e.altRowTemplate:e.rowTemplate)(f)),g.replaceWith(k);for(l=0,m=e.columns.length;l<m;l++)i=e.columns[l],i.field===b.field&&(h=k.children(":not(.k-group-cell,.k-hierarchy-cell)").eq(l),a('<span class="k-dirty"/>').prependTo(h));e.trigger("itemChange",{item:k,data:f,ns:d})}},_pageable:function(){var b=this,d,e=b.options.pageable;e&&(d=b.wrapper.children("div.k-grid-pager"),d.length||(d=a('<div class="k-pager-wrap k-grid-pager"/>').appendTo(b.wrapper)),b.pager&&b.pager.destroy(),typeof e=="object"&&e instanceof c.ui.Pager?b.pager=e:b.pager=new c.ui.Pager(d,k({},e,{dataSource:b.dataSource})))},_footer:function(){var b=this,c=b.dataSource.aggregates(),d="",e=b.footerTemplate,f=b.options,g;if(e){c=r(c)?be(b.dataSource.aggregate()):c,d=a(b._wrapFooter(e(c))),g=b.footer||b.wrapper.find(".k-grid-footer");if(g.length){var h=d;g.replaceWith(h),b.footer=h}else f.scrollable?b.footer=f.pageable?d.insertBefore(b.wrapper.children("div.k-grid-pager")):d.appendTo(b.wrapper):b.footer=d.insertBefore(b.tbody);if(f.scrollable){var i=!1;b.scrollables.each(function(){a(this).hasClass("k-grid-footer-wrap")&&(i=!0)}),i||(b.scrollables=b.scrollables.add(b.footer.children(".k-grid-footer-wrap")))}f.resizable&&b._footerWidth&&b.footer.find("table").css("width",b._footerWidth)}},_wrapFooter:function(b){var c=this,d="";if(c.options.scrollable){d=a('<div class="k-grid-footer"><div class="k-grid-footer-wrap"><table cellspacing="0"><tbody>'+b+"</tbody></table></div></div>"),c._appendCols(d.find("table"));return d}return'<tfoot class="k-grid-footer">'+b+"</tfoot>"},_columnMenu:function(){var b=this,d,e=b.columns,f,g=b.options,h=g.columnMenu,i,j,l,m;h&&(typeof h=="boolean"&&(h={}),b.thead.find("th:not(.k-hierarchy-cell,.k-group-cell)").each(function(n){f=e[n],m=a(this),!f.command&&(f.field||m.attr("data-"+c.ns+"field"))&&(d=m.data("kendoColumnMenu"),d&&d.destroy(),j=f.sortable!==!1&&h.sortable!==!1?g.sortable:!1,l=g.filterable&&f.filterable!==!1&&h.filterable!==!1?k({},f.filterable,g.filterable):!1,i={dataSource:b.dataSource,values:f.values,columns:h.columns,sortable:j,filterable:l,messages:h.messages,owner:b},m.kendoColumnMenu(i))}))},_filterable:function(){var b=this,d=b.columns,e,f,g=b.options.filterable;g&&!b.options.columnMenu&&b.thead.find("th:not(.k-hierarchy-cell,.k-group-cell)").each(function(h){e=a(this),d[h].filterable!==!1&&!d[h].command&&(d[h].field||e.attr("data-"+c.ns+"field"))&&(f=e.data("kendoFilterMenu"),f&&f.destroy(),e.kendoFilterMenu(k(!0,{},g,d[h].filterable,{dataSource:b.dataSource,values:d[h].values})))})},_sortable:function(){var b=this,d=b.columns,e,f=b.options.sortable;f&&b.thead.find("th:not(.k-hierarchy-cell,.k-group-cell)").each(function(g){e=d[g],e.sortable!==!1&&!e.command&&e.field&&a(this).attr("data-"+c.ns+"field",e.field).kendoSortable(k({},f,{dataSource:b.dataSource}))})},_columns:function(b){var d=this,e=d.table,f,g=e.find("col"),h=d.options.dataSource;b=b.length?b:l(e.find("th"),function(b,d){b=a(b);var e=b.attr(c.attr("sortable")),f=b.attr(c.attr("filterable")),h=b.attr(c.attr("type")),i=b.attr(c.attr("groupable")),j=b.attr(c.attr("field")),k=b.attr(c.attr("menu"));j||(j=b.text().replace(/\s|[^A-z0-9]/g,""));return{field:j,type:h,sortable:e!=="false",filterable:f!=="false",groupable:i!=="false",menu:k,template:b.attr(c.attr("template")),width:g.eq(d).css("width")}}),f=!(d.table.find("tbody tr").length>0&&(!h||!h.transport)),d.columns=l(b,function(a){a=typeof a===U?{field:a}:a,a.hidden&&(a.attributes=bi(a.attributes),a.footerAttributes=bi(a.footerAttributes),a.headerAttributes=bi(a.headerAttributes));return k({encoded:f},a)})},_tmpl:function(a,b){var d=this,e=k({},c.Template,d.options.templateSettings),f,g=d.columns.length,h,i={storage:{},count:0},j,l,m=d._hasDetails(),n=[],o=d.dataSource.group().length;if(!a){a="<tr",b&&n.push("k-alt"),m&&n.push("k-master-row"),n.length&&(a+=' class="'+n.join(" ")+'"'),g&&(a+=" "+c.attr("uid")+'="#=uid#"'),a+=">",o>0&&(a+=_(o)),m&&(a+='<td class="k-hierarchy-cell"><a class="k-icon k-plus" href="\\#"></a></td>');for(f=0;f<g;f++)j=d.columns[f],h=j.template,l=typeof h,a+="<td"+ba(j.attributes)+">",a+=d._cellTmpl(j,i),a+="</td>";a+="</tr>"}a=c.template(a,e);if(i.count>0)return p(a,i.storage);return a},_headerCellText:function(a){var b=this,d=k({},c.Template,b.options.templateSettings),e=a.headerTemplate,f=typeof e,g=a.title||a.field||"";f===T?g=c.template(e,d)({}):f===U&&(g=e);return g},_cellTmpl:function(a,b){var d=this,e=k({},c.Template,d.options.templateSettings),f=a.template,g=e.paramName,h="",i,l,m=a.format,o=typeof f,p=a.values;if(a.command){if(n(a.command)){for(i=0,l=a.command.length;i<l;i++)h+=d._createButton(a.command[i]);return h.replace(Y,"\\#")}return d._createButton(a.command).replace(Y,"\\#")}o===T?(b.storage["tmpl"+b.count]=f,h+="#=this.tmpl"+b.count+"("+g+")#",b.count++):o===U?h+=f:p&&p.length&&j(p[0])&&"value"in p[0]?(h+="#var v ="+c.stringify(p)+"#",h+="#for (var idx=0,length=v.length;idx<length;idx++) {#",h+="#if (v[idx].value == ",e.useWithBlock||(h+=g+"."),h+=a.field,h+=") { #",h+="${v[idx].text}",h+="#break;#",h+="#}#",h+="#}#"):(h+=a.encoded?"${":"#=",m&&(h+='kendo.format("'+m.replace(W,"\\$1")+'",'),e.useWithBlock||(h+=g+"."),h+=a.field,m&&(h+=")"),h+=a.encoded?"}":"#");return h},_templates:function(){var b=this,c=b.options,d=b.dataSource,e=d.group(),f=d.aggregate();b.rowTemplate=b._tmpl(c.rowTemplate),b.altRowTemplate=b._tmpl(c.altRowTemplate||c.rowTemplate,!0),b._hasDetails()&&(b.detailTemplate=b._detailTmpl(c.detailTemplate||""));if(!r(f)||m(b.columns,function(a){return a.footerTemplate}).length)b.footerTemplate=b._footerTmpl(f,"footerTemplate","k-footer-template");e.length&&m(b.columns,function(a){return a.groupFooterTemplate}).length&&(f=a.map(e,function(a){return a.aggregates}),b.groupFooterTemplate=b._footerTmpl(f,"groupFooterTemplate","k-group-footer"))},_footerTmpl:function(a,b,d){var e=this,f=k({},c.Template,e.options.templateSettings),g=f.paramName,h="",i,j,l=e.columns,m,n,o={},q=0,r={},s=e.dataSource,t=s.group().length,u=be(a),v;h+='<tr class="'+d+'">',t>0&&(h+=_(t)),e._hasDetails()&&(h+='<td class="k-hierarchy-cell">&nbsp;</td>');for(i=0,j=e.columns.length;i<j;i++)v=l[i],m=v[b],n=typeof m,h+="<td"+ba(v.footerAttributes)+">",m?(n!==T&&(r=u[v.field]?k({},f,{paramName:g+"."+v.field}):{},m=c.template(m,r)),o["tmpl"+q]=m,h+="#=this.tmpl"+q+"("+g+")#",q++):h+="&nbsp;",h+="</td>";h+="</tr>",h=c.template(h,f);if(q>0)return p(h,o);return h},_detailTmpl:function(a){var b=this,d="",e=k({},c.Template,b.options.templateSettings),f=e.paramName,g={},h=0,i=b.dataSource.group().length,j=bh(b.columns).length,l=typeof a;d+='<tr class="k-detail-row">',i>0&&(d+=_(i)),d+='<td class="k-hierarchy-cell"></td><td class="k-detail-cell"'+(j?' colspan="'+j+'"':"")+">",l===T?(g["tmpl"+h]=a,d+="#=this.tmpl"+h+"("+f+")#",h++):d+=a,d+="</td></tr>",d=c.template(d,e);if(h>0)return p(d,g);return d},_hasDetails:function(){var a=this;return a.options.detailTemplate!==b||(a._events[D]||[]).length},_details:function(){var b=this;b.table.delegate(".k-hierarchy-cell .k-plus, .k-hierarchy-cell .k-minus",Q,function(c){var d=a(this),e=d.hasClass("k-plus"),f=d.closest("tr.k-master-row"),g,h=b.detailTemplate,i,j=b._hasDetails();d.toggleClass("k-plus",!e).toggleClass("k-minus",e),j&&!f.next().hasClass("k-detail-row")&&(i=b.dataItem(f),a(h(i)).addClass(f.hasClass("k-alt")?"k-alt":"").insertAfter(f),b.trigger(D,{masterRow:f,detailRow:f.next(),data:i,detailCell:f.next().find(".k-detail-cell")})),g=f.next(),b.trigger(e?J:K,{masterRow:f,detailRow:g}),g.toggle(e),c.preventDefault();return!1})},dataItem:function(b){return this._data[this.tbody.find("> tr:not(.k-grouping-row,.k-detail-row,.k-group-footer)").index(a(b))]},expandRow:function(b){a(b).find("> td .k-plus, > td .k-i-expand").click()},collapseRow:function(b){a(b).find("> td .k-minus, > td .k-i-collapse").click()},_thead:function(){var d=this,e=d.columns,f=d._hasDetails()&&e.length,g,h,i="",j=d.table.find(">thead"),k,l,m;j.length||(j=a("<thead/>").insertBefore(d.tbody)),k=d.element.find("tr:has(th):first"),k.length||(k=j.children().first(),k.length||(k=a("<tr/>")));if(!k.children().length){f&&(i+='<th class="k-hierarchy-cell">&nbsp;</th>');for(g=0,h=e.length;g<h;g++)m=e[g],l=d._headerCellText(m),m.command?i+="<th"+ba(m.headerAttributes)+">"+l+"</th>":(i+="<th "+c.attr("field")+"='"+(m.field||"")+"' ",m.title&&(i+=c.attr("title")+'="'+m.title.replace(/'/g,"'")+'" '),m.groupable!==b&&(i+=c.attr("groupable")+"='"+m.groupable+"' "),m.aggregates&&(i+=c.attr("aggregates")+"='"+m.aggregates+"'"),i+=ba(m.headerAttributes),i+=">"+l+"</th>");k.html(i)}else f&&!k.find(".k-hierarchy-cell")[0]&&k.prepend('<th class="k-hierarchy-cell">&nbsp;</th>');k.find("th").addClass("k-header"),d.options.scrollable||j.addClass("k-grid-header"),k.appendTo(j),d.thead=j,d._sortable(),d._filterable(),d._scrollable(),d._updateCols(),d._resizable(),d._draggable(),d._reorderable(),d._columnMenu()},_updateCols:function(){var a=this;a._appendCols(a.thead.parent().add(a.table))},_appendCols:function(a){var b=this;bk(a,bh(b.columns),b._hasDetails(),b.dataSource.group().length)},_autoColumns:function(a){if(a&&a.toJSON){var b=this,c;a=a.toJSON();for(c in a)b.columns.push({field:c});b._thead(),b._templates()}},_rowsHtml:function(a){var b=this,c="",d,e,f=b.rowTemplate,g=b.altRowTemplate;for(d=0,e=a.length;d<e;d++)d%2?c+=g(a[d]):c+=f(a[d]),b._data.push(a[d]);return c},_groupRowHtml:function(a,b,d){var e=this,f="",g,h,i=a.field,j=m(e.columns,function(a){return a.field==i})[0]||{},l=j.format?c.format(j.format,a.value):a.value,n=j.groupHeaderTemplate,o=(j.title||i)+": "+l,p=k({},{field:a.field,value:a.value},a.aggregates[a.field]),q=a.items;n&&(o=typeof n===T?n(p):c.template(n)(p)),f+='<tr class="k-grouping-row">'+_(d)+'<td colspan="'+b+'">'+'<p class="k-reset">'+'<a class="k-icon k-i-collapse" href="#"></a>'+o+"</p></td></tr>";if(a.hasSubgroups)for(g=0,h=q.length;g<h;g++)f+=e._groupRowHtml(q[g],b-1,d+1);else f+=e._rowsHtml(q);e.groupFooterTemplate&&(f+=e.groupFooterTemplate(a.aggregates));return f},collapseGroup:function(b){b=a(b).find(".k-icon").addClass("k-i-expand").removeClass("k-i-collapse").end();var c=b.find(".k-group-cell").length,d=1,e,f;b.nextAll("tr").each(function(){f=a(this),e=f.find(".k-group-cell").length,f.hasClass("k-grouping-row")?d++:f.hasClass("k-group-footer")&&d--;if(e<=c||f.hasClass("k-group-footer")&&d<0)return!1;f.hide()})},expandGroup:function(b){b=a(b).find(".k-icon").addClass("k-i-collapse").removeClass("k-i-expand").end();var c=this,d=b.find(".k-group-cell").length,e,f,g=1;b.nextAll("tr").each(function(){e=a(this),f=e.find(".k-group-cell").length;if(f<=d)return!1;f==d+1&&!e.hasClass("k-detail-row")&&(e.show(),e.hasClass("k-grouping-row")&&e.find(".k-icon").hasClass("k-i-collapse")&&c.expandGroup(e),e.hasClass("k-master-row")&&e.find(".k-icon").hasClass("k-minus")&&e.next().show()),e.hasClass("k-grouping-row")&&g++,e.hasClass("k-group-footer")&&(g==1?e.show():g--)})},_updateHeader:function(b){var c=this,d=c.thead.find("th.k-group-cell"),e=d.length;b>e?a(Array(b-e+1).join('<th class="k-group-cell k-header">&nbsp;</th>')).prependTo(c.thead.find("tr")):b<e&&(e=e-b,a(m(d,function(a,b){return e>b})).remove())},_firstDataItem:function(a,b){a&&b&&(a.hasSubgroups?a=this._firstDataItem(a.items[0],b):a=a.items[0]);return a},hideColumn:function(b){var c=this,d,e,f,g,h,i,j=0,k,l=c.footer||c.wrapper.find(".k-grid-footer"),n=c.columns,p;typeof b=="number"?b=n[b]:b=m(n,function(a){return a.field===b})[0];if(!!b&&!b.hidden){p=o(b,bh(n)),b.hidden=!0,b.attributes=bi(b.attributes),b.footerAttributes=bi(b.footerAttributes),b.headerAttributes=bi(b.headerAttributes),c._templates(),c._updateCols(),c.thead.find(">tr>th:not(.k-hierarchy-cell,.k-group-cell):visible").eq(p).hide(),l&&(c._appendCols(l.find("table:first")),l.find(".k-footer-template>td:not(.k-hierarchy-cell,.k-group-cell):visible").eq(p).hide()),d=c.tbody.children();for(g=0,k=d.length;g<k;g+=1)e=d.eq(g),e.is(".k-grouping-row,.k-detail-row")?(f=e.children(":not(.k-group-cell):first,.k-detail-cell").last(),f.attr("colspan",parseInt(f.attr("colspan"),10)-1)):(e.hasClass("k-grid-edit-row")&&(f=e.children(".k-edit-container")[0])&&(f=a(f),f.attr("colspan",parseInt(f.attr("colspan"),10)-1),f.find("col").eq(p).remove(),e=f.find("tr:first")),e.children(":not(.k-group-cell,.k-hierarchy-cell):visible").eq(p).hide());h=c.thead.prev().find("col");for(g=0,k=h.length;g<k;g+=1){i=h[g].style.width;if(i&&i.indexOf("%")==-1)j+=parseInt(i,10);else{j=0;break}}j&&a(">.k-grid-header table:first,>.k-grid-footer table:first",c.wrapper).add(c.table).width(j),c.trigger(F,{column:b})}},showColumn:function(b){var c=this,d,e,f,g,h,i,j,k,l,n=c.columns,p=c.footer||c.wrapper.find(".k-grid-footer"),q;typeof b=="number"?b=n[b]:b=m(n,function(a){return a.field===b})[0];if(!!b&&!!b.hidden){q=o(b,n),b.hidden=!1,b.attributes=bj(b.attributes),b.footerAttributes=bj(b.footerAttributes),b.headerAttributes=bj(b.headerAttributes),c._templates(),c._updateCols(),c.thead.find(">tr>th:not(.k-hierarchy-cell,.k-group-cell)").eq(q).show(),p&&(c._appendCols(p.find("table:first")),p.find(".k-footer-template>td:not(.k-hierarchy-cell,.k-group-cell)").eq(q).show()),d=c.tbody.children();for(e=0,f=d.length;e<f;e+=1)g=d.eq(e),g.is(".k-grouping-row,.k-detail-row")?(h=g.children(":not(.k-group-cell):first,.k-detail-cell").last(),h.attr("colspan",parseInt(h.attr("colspan"),10)+1)):(g.hasClass("k-grid-edit-row")&&(h=g.children(".k-edit-container")[0])&&(h=a(h),h.attr("colspan",parseInt(h.attr("colspan"),10)+1),bk(h.find(">form>table"),bh(n),!1,0),g=h.find("tr:first")),g.children(":not(.k-group-cell,.k-hierarchy-cell)").eq(q).show());i=a(">.k-grid-header table:first,>.k-grid-footer table:first",c.wrapper).add(c.table);if(!b.width)i.width("");else{j=0,l=c.thead.prev().find("col");for(e=0,f=l.length;e<f;e+=1){k=l[e].style.width;if(k.indexOf("%")>-1){j=0;break}j+=parseInt(k,10)}j&&i.width(j)}c.trigger(G,{column:b})}},_progress:function(a){var b=this,d=b.element.is("table")?b.element.parent():b.content&&b.content.length?b.content:b.element;c.ui.progress(d,a)},refresh:function(b){var c=this,d,e,f="",h=c.dataSource.view(),i,j,k,l=c.current(),m=(c.dataSource.group()||[]).length,n=m+bh(c.columns).length;if(!b||b.action!=="itemchange"||!c.editable){c.trigger("dataBinding"),l&&l.hasClass("k-state-focused")&&(k=c.items().index(l.parent())),c._destroyEditable(),c._progress(!1),c._data=[],c.columns.length||(c._autoColumns(c._firstDataItem(h[0],m)),n=m+c.columns.length),c._group=m>0||c._group,c._group&&(c._templates(),c._updateCols(),c._updateHeader(m),c._group=m>0);if(m>0){c.detailTemplate&&n++;for(e=0,d=h.length;e<d;e++)f+=c._groupRowHtml(h[e],n,0)}else f+=c._rowsHtml(h);g?c.tbody[0].innerHTML=f:(j=document.createElement("div"),j.innerHTML="<table><tbody>"+f+"</tbody></table>",i=j.firstChild.firstChild,c.table[0].replaceChild(i,c.tbody[0]),c.tbody=a(i)),c._footer(),c._setContentHeight(),k>=0&&c.current(c.items().eq(k).children().filter(w).first()),c.trigger(I)}}});d.plugin(bl),d.plugin($)}(jQuery),function(a,b){var c=window.kendo,d="change",e="dataBound",f="dataBinding",g=c.ui.Widget,h=c.keys,i=">*",j="requestStart",k="error",l="k-state-focused",m="k-focusable",n="k-state-selected",o="k-edit-item",p="string",q="edit",r="remove",s="save",t="click",u=a.proxy,v=c.ui.progress,w=c.data.DataSource,x=g.extend({init:function(b,d){var e=this;d=a.isArray(d)?{dataSource:d}:d,g.fn.init.call(e,b,d),d=e.options,e.wrapper=e.element,e._element(),e._dataSource(),e.template=c.template(d.template||""),e.altTemplate=c.template(d.altTemplate||d.template),e.editTemplate=c.template(d.editTemplate||""),e._navigatable(),e._selectable(),e._pageable(),e._crudHandlers(),e.options.autoBind&&e.dataSource.fetch(),c.notify(e)},events:[d,f,e,q,r,s],options:{name:"ListView",autoBind:!0,selectable:!1,navigatable:!1,template:"",altTemplate:"",editTemplate:""},items:function(){return this.element.find(i)},setDataSource:function(a){this.options.dataSource=a,this._dataSource(),this.options.autoBind&&a.fetch()},_dataSource:function(){var a=this;a.dataSource&&a._refreshHandler?a.dataSource.unbind(d,a._refreshHandler).unbind(j,a._requestStartHandler).unbind(k,a._errorHandler):(a._refreshHandler=u(a.refresh,a),a._requestStartHandler=u(a._requestStart,a),a._errorHandler=u(a._error,a)),a.dataSource=w.create(a.options.dataSource).bind(d,a._refreshHandler).bind(j,a._requestStartHandler).bind(k,a._errorHandler)},_requestStart:function(){v(this.element,!0)},_error:function(){v(this.element,!1)},_element:function(){this.element.addClass("k-widget k-listview")},refresh:function(b){var d=this,g=d.dataSource.view(),h,i,j,k="",l,m,n=d.template,o=d.altTemplate;if(b&&b.action==="itemchange")d.editable||(h=b.items[0],l=g.indexOf(h),l>=0&&(j=a(n(h)),d.items().eq(l).replaceWith(j),d.trigger("itemChange",{item:j,data:h})));else{d.trigger(f),d._destroyEditable();for(l=0,m=g.length;l<m;l++)l%2?k+=o(g[l]):k+=n(g[l]);d.element.html(k),i=d.items();for(l=0,m=g.length;l<m;l++)i.eq(l).attr(c.attr("uid"),g[l].uid);d.trigger(e)}},_pageable:function(){var b=this,c=b.options.pageable,d,e;a.isPlainObject(c)&&(e=c.pagerId,d=a.extend({},c,{dataSource:b.dataSource,pagerId:null}),a("#"+e).kendoPager(d))},_selectable:function(){var a=this,b,e,f=a.options.selectable,g=a.options.navigatable;f&&(b=typeof f===p&&f.toLowerCase().indexOf("multiple")>-1,a.selectable=new c.ui.Selectable(a.element,{multiple:b,filter:i,change:function(){a.trigger(d)}}),g&&a.element.keydown(function(c){c.keyCode===h.SPACEBAR&&(e=a.current(),c.preventDefault(),b?c.ctrlKey?e.hasClass(n)&&(e.removeClass(n),e=null):a.selectable.clear():a.selectable.clear(),a.selectable.value(e))}))},current:function(a){var c=this,d=c._current;a!==b&&a.length&&(!d||d[0]!==a[0])&&(a.addClass(l),d&&d.removeClass(l),c._current=a);return c._current},_navigatable:function(){var b=this,c=b.options.navigatable,d=b.element,e=u(b.current,b),f=function(b){e(a(b.currentTarget)),a(b.target).is(":button,a,:input,a>.k-icon,textarea")||d.focus()};c&&(d.attr("tabIndex",Math.max(d.attr("tabIndex")||0,0)),d.bind({focus:function(){var a=b._current;a&&a.is(":visible")?a.addClass(l):e(d.find(i).first())},focusout:function(){b._current&&b._current.removeClass(l)},keydown:function(a){var c=a.keyCode,e=b.current();h.UP===c?b.current(e?e.prev():d.find(i).first()):h.DOWN===c?b.current(e?e.next():d.find(i).first()):h.PAGEUP==c?(b._current=null,b.dataSource.page(b.dataSource.page()-1)):h.PAGEDOWN==c&&(b._current=null,b.dataSource.page(b.dataSource.page()+1))}}),d.addClass(m).delegate("."+m+i,"mousedown",f))},clearSelection:function(){var a=this;a.selectable.clear(),a.trigger(d)},select:function(b){var c=this,d=c.selectable;b=a(b);if(b.length)d.options.multiple||(d.clear(),b=b.first()),d.value(b);else return d.value()},_destroyEditable:function(){var a=this;a.editable&&(a.editable.destroy(),delete a.editable)},_modelFromElement:function(a){var b=a.attr(c.attr("uid"));return this.dataSource.getByUid(b)},_closeEditable:function(b){var d=this,e=d.editable,f,g,h=!0;e&&(b&&(h=e.end()),h&&(f=d._modelFromElement(e.element),g=a(d.template(f)).attr(c.attr("uid"),f.uid),d._destroyEditable(),e.element.replaceWith(g)));return h},edit:function(b){var d=this,e=d._modelFromElement(b),f=a(d.editTemplate(e)).addClass(o);d.cancel(),f.attr(c.attr("uid"),e.uid),b.replaceWith(f),d.editable=f.kendoEditable({model:e,clearContainer:!1,errorTemplate:!1}).data("kendoEditable"),d.trigger(q,{model:e,item:f})},save:function(){var a=this,b=a.editable.element,c=a._modelFromElement(b);!a.trigger(s,{model:c,item:b})&&a._closeEditable(!0)&&a.dataSource.sync()},remove:function(a){var b=this,c=b.dataSource,d=b._modelFromElement(a);b.trigger(r,{model:d,item:a})||(a.hide(),c.remove(d),c.sync())},add:function(){var a=this,b=a.dataSource,c=b.indexOf((b.view()||[])[0]);c<0&&(c=0),a.cancel(),b.insert(c,{}),a.edit(a.element.children().first())},cancel:function(){var a=this,b=a.dataSource;a.editable&&(b.cancelChanges(a._modelFromElement(a.editable.element)),a._closeEditable(!1))},_crudHandlers:function(){var b=this;b.element.on(t,".k-edit-button",function(d){var e=a(this).closest("["+c.attr("uid")+"]");b.edit(e),d.preventDefault()}),b.element.on(t,".k-delete-button",function(d){var e=a(this).closest("["+c.attr("uid")+"]");b.remove(e),d.preventDefault()}),b.element.on(t,".k-update-button",function(a){b.save(),a.preventDefault()}),b.element.on(t,".k-cancel-button",function(a){b.cancel(),a.preventDefault()})}});c.ui.plugin(x)}(jQuery),function(a,b){var c=window.kendo,d=c.Class,e=c.ui.Widget,f=c.support.mobileOS,g=a.extend,h=c.deepExtend,i=c.keys,j=d.extend({init:function(a){var b=this;b.options=a},getHtml:function(){var a=this.options;return c.template(a.template)({cssClass:a.cssClass,tooltip:a.title,initialValue:a.initialValue})}}),k={select:function(a){a.trigger("select",{})},editorWrapperTemplate:'<table cellspacing="4" cellpadding="0" class="k-widget k-editor k-header"><tbody><tr><td class="k-editor-toolbar-wrap"><ul class="k-editor-toolbar"></ul></td></tr><tr><td class="k-editable-area"></td></tr></tbody></table>',buttonTemplate:'<li class="k-editor-button"><a href="" class="k-tool-icon #= cssClass #" unselectable="on" title="#= tooltip #">#= tooltip #</a></li>',colorPickerTemplate:'<li class="k-editor-colorpicker"><div class="k-widget k-colorpicker k-header #= cssClass #"><span class="k-tool-icon"><span class="k-selected-color"></span></span><span class="k-icon k-i-arrow-s"></span></div></li>',comboBoxTemplate:'<li class="k-editor-combobox"><select title="#= tooltip #" class="#= cssClass #"></select></li>',dropDownListTemplate:'<li class="k-editor-selectbox"><select title="#= tooltip #" class="#= cssClass #"></select></li>',focusable:".k-colorpicker,a.k-tool-icon:not(.k-state-disabled),.k-selectbox, .k-combobox .k-input",wrapTextarea:function(b){var c=b.width(),d=b.height(),e=k.editorWrapperTemplate,f=a(e).insertBefore(b).width(c).height(d),g=f.find(".k-editable-area");b.appendTo(g).addClass("k-content k-raw-content").hide();return b.closest(".k-editor")},renderTools:function(b,d){var e={},f,h,i,j=b._nativeTools,l,m,n=a(b.element).closest(".k-editor").find(".k-editor-toolbar");if(d)for(i=0;i<d.length;i++){f=d[i],a.isPlainObject(f)?f.name&&b.tools[f.name]?(a.extend(b.tools[f.name].options,f),e[f.name]=b.tools[f.name],m=e[f.name].options):(m=g({cssClass:"k-i-custom",type:"button",tooltip:""},f),m.name&&(m.cssClass="k-"+(m.name=="custom"?"i-custom":m.name)),m.template||m.type=="button"&&(m.template=k.buttonTemplate)):b.tools[f]&&(e[f]=b.tools[f],m=e[f].options);if(!m)continue;l=m.template,l&&(l.getHtml?l=l.getHtml():(a.isFunction(l)||(l=c.template(l)),l=l(m)),l.indexOf("<li")!==0&&(l="<li class='k-editor-template'>"+l+"</li>"),h=a(l).appendTo(n),m.type=="button"&&m.exec&&h.find(".k-tool-icon").click(a.proxy(m.exec,b.element[0])))}for(i=0;i<j.length;i++)e[j[i]]||(e[j[i]]=b.tools[j[i]]);b.options.tools=e},decorateStyleToolItems:function(b){var d=b.data.closest(".k-editor").find(".k-style").data("kendoSelectBox");if(!!d){var e=d.dataSource.view();d.list.find(".k-item").each(function(d,f){var g=a(f),h=g.text(),i=c.ui.editor.Dom.inlineStyle(b.data.data("kendoEditor").document,"span",{className:e[d].value});g.html('<span unselectable="on" style="display:block;'+i+'">'+h+"</span>")})}},createContentElement:function(b,c){var d,e,f,g=b.closest(".k-rtl").length?"direction:rtl;":"";b.hide(),d=a("<iframe />",{src:'javascript:"<html></html>"',frameBorder:"0"}).css("display","").addClass("k-content").insertBefore(b)[0],e=d.contentWindow||d,c.length>0&&a(d).one("load",b,k.decorateStyleToolItems),f=e.document||d.contentDocument,f.designMode="On",f.open(),f.write("<!DOCTYPE html><html><head><meta charset='utf-8' /><style>html,body{padding:0;margin:0;font-family:Verdana,Geneva,sans-serif;background:#fff;}html{font-size:100%}body{font-size:.75em;line-height:1.5;padding-top:1px;margin-top:-1px;word-wrap: break-word;-webkit-nbsp-mode: space;-webkit-line-break: after-white-space;"+g+"}"+"h1{font-size:2em;margin:.67em 0}h2{font-size:1.5em}h3{font-size:1.16em}h4{font-size:1em}h5{font-size:.83em}h6{font-size:.7em}"+"p{margin:0 0 1em;padding:0 .2em}.k-marker{display:none;}.k-paste-container{position:absolute;left:-10000px;width:1px;height:1px;overflow:hidden}"+"ul,ol{padding-left:2.5em}"+"a{color:#00a}"+"code{font-size:1.23em}"+"</style>"+a.map(c,function(a){return"<link rel='stylesheet' href='"+a+"'>"}).join("")+"</head><body></body></html>"),f.close();return e},initializeContentElement:function(b){var c=!0;b.window=k.createContentElement(a(b.textarea),b.options.stylesheets),b.document=b.window.contentDocument||b.window.document,b.body=b.document.body,a(b.document).bind({keydown:function(a){if(a.keyCode===i.F10)setTimeout(function(){var a="tabIndex",c=b.wrapper,d=c.attr(a);c.attr(a,d||0).focus().find("li:has("+m+")").first().focus(),!d&&d!==0&&c.removeAttr(a)},100),a.preventDefault();else{var d=b.keyboard.toolFromShortcut(b.options.tools,a);if(d){a.preventDefault(),/undo|redo/.test(d)||b.keyboard.endTyping(!0),b.exec(d);return!1}if(b.keyboard.isTypingKey(a)&&b.pendingFormats.hasPending())if(c)c=!1;else{var e=b.getRange();b.pendingFormats.apply(e),b.selectRange(e)}b.keyboard.clearTimeout(),b.keyboard.keydown(a)}},keyup:function(d){var e=[8,9,33,34,35,36,37,38,39,40,40,45,46];if(a.inArray(d.keyCode,e)>-1||d.keyCode==65&&d.ctrlKey&&!d.altKey&&!d.shiftKey)b.pendingFormats.clear(),l(b);if(b.keyboard.isTypingKey(d)){if(b.pendingFormats.hasPending()){var f=b.getRange();b.pendingFormats.apply(f),b.selectRange(f)}}else c=!0;b.keyboard.keyup(d)},mousedown:function(c){b.pendingFormats.clear();var d=a(c.target);!a.browser.gecko&&c.which==2&&d.is("a[href]")&&window.open(d.attr("href"),"_new")},mouseup:function(){l(b)}}),a(b.window).bind("blur",function(){var a=b.textarea.value,c=b.encodedValue();b.update(),c!=a&&b.trigger("change")}),a(b.body).bind("cut paste",function(a){b.clipboard["on"+a.type](a)})},formatByName:function(b,c){for(var d=0;d<c.length;d++)if(a.inArray(b,c[d].tags)>=0)return c[d]},registerTool:function(a,b){var c=s.fn._tools;c[a]=b,c[a].options&&c[a].options.template&&(c[a].options.template.options.cssClass="k-"+a)},registerFormat:function(a,b){c.ui.Editor.fn.options.formats[a]=b}},l=k.select,m=k.focusable,n=k.wrapTextarea,o=k.renderTools,p=k.initializeContentElement,q={bold:"Bold",italic:"Italic",underline:"Underline",strikethrough:"Strikethrough",superscript:"Superscript",subscript:"Subscript",justifyCenter:"Center text",justifyLeft:"Align text left",justifyRight:"Align text right",justifyFull:"Justify",insertUnorderedList:"Insert unordered list",insertOrderedList:"Insert ordered list",indent:"Indent",outdent:"Outdent",createLink:"Insert hyperlink",unlink:"Remove hyperlink",insertImage:"Insert image",insertHtml:"Insert HTML",fontName:"Select font family",fontNameInherit:"(inherited font)",fontSize:"Select font size",fontSizeInherit:"(inherited size)",formatBlock:"Format",style:"Styles",emptyFolder:"Empty Folder",uploadFile:"Upload",orderBy:"Arrange by:",orderBySize:"Size",orderByName:"Name",invalidFileType:'The selected file "{0}" is not valid. Supported file types are {1}.',deleteFile:'Are you sure you want to delete "{0}"?',overwriteFile:'A file with name "{0}" already exists in the current directory. Do you want to overwrite it?',directoryNotFound:"A directory with this name was not found."},r=!f||f.ios&&f.flatVersion>=500||!f.ios&&typeof document.documentElement.contentEditable!="undefined",s=e.extend({init:function(b,d){function t(a,b){if(!b.key)return a;var c=a+" (";b.ctrl&&(c+="Ctrl + "),b.shift&&(c+="Shift + "),b.alt&&(c+="Alt + "),c+=b.key+")";return c}function s(b){var c=a.grep(b.className.split(" "),function(a){return!/^k-(widget|tool-icon|state-hover|header|combobox|dropdown|selectbox|colorpicker)$/i.test(a)});return c[0]?c[0].substring(c[0].lastIndexOf("-")+1):"custom"}if(!!r){var f=this,g,j,k=c.ui.editor;e.fn.init.call(f,b,d),f.tools=h({},c.ui.Editor.fn._tools),f.options=h({},f.options,d),b=a(b),b.closest("form").bind("submit",function(){f.update()});for(var l in f.tools)f.tools[l].name=l.toLowerCase();f.textarea=b.attr("autocomplete","off")[0],g=f.wrapper=n(b),o(f,f.options.tools),p(f),f.keyboard=new k.Keyboard([new k.TypingHandler(f),new k.SystemHandler(f)]),f.clipboard=new k.Clipboard(this),f.pendingFormats=new k.PendingFormats(this),f.undoRedoStack=new k.UndoRedoStack,d&&d.value?j=d.value:j=b.val().replace(/[\r\n\v\f\t ]+/ig," "),f.value(j);var u=".k-editor-toolbar > li > *, .k-editor-toolbar > li select",v=".k-editor-button .k-tool-icon",w=v+":not(.k-state-disabled)",x=v+".k-state-disabled";g.find(".k-combobox .k-input").keydown(function(b){var c=a(this).closest(".k-combobox").data("kendoComboBox"),d=b.keyCode;d==i.RIGHT||d==i.LEFT?c.close():d==i.DOWN&&(c.dropDown.isOpened()||(b.stopImmediatePropagation(),c.open()))}),g.delegate(w,"mouseenter",function(){a(this).addClass("k-state-hover")}).delegate(w,"mouseleave",function(){a(this).removeClass("k-state-hover")}).delegate(v,"mousedown",!1).delegate(m,"keydown",function(b){var c=a(this).closest("li"),d="li:has("+m+")",e,g=b.keyCode;if(g==i.RIGHT)e=c.nextAll(d).first().find(m);else if(g==i.LEFT)e=c.prevAll(d).last().find(m);else if(g==i.ESC)e=f;else if(g==i.TAB&&!b.ctrlKey&&!b.altKey)if(b.shiftKey){e=c.prevAll(d).last().find(m);if(e.length)b.preventDefault();else return}else b.preventDefault(),e=c.nextAll(d).first().find(m),e.length||(e=f);e&&e.focus()}).delegate(w,"click",function(a){a.preventDefault(),a.stopPropagation(),f.exec(s(this))}).delegate(x,"click",function(a){a.preventDefault()}).find(u).each(function(){var b=s(this),c=f.options,d=c.tools[b],e=c.messages[b],g=a(this);if(!!d){if(b=="fontSize"||b=="fontName"){var h=c.messages[b+"Inherit"]||q[b+"Inherit"];g.find("input").val(h).end().find("span.k-input").text(h).end()}d.initialize(g,{title:t(e,d),editor:f})}}),f.bind("select",function(){var b=f.getRange(),c=k.RangeUtils.textNodes(b);c.length||(c=[b.startContainer]),g.find(u).each(function(){var b=f.options.tools[s(this)];b&&b.update(a(this),c,f.pendingFormats)})}),a(document).bind("DOMNodeInserted",function(b){var c=f.wrapper;if(a.contains(b.target,c[0])||c[0]==b.target)f.textarea.value=f.value(),c.find("iframe").remove(),p(f)}).bind("mousedown",function(a){try{f.keyboard.isTypingInProgress()&&f.keyboard.endTyping(!0),f.selectionRestorePoint||(f.selectionRestorePoint=new k.RestorePoint(f.getRange()))}catch(a){}}),c.notify(f)}},events:["select","change","execute","error","paste"],options:{name:"Editor",messages:q,formats:{},encoded:!0,stylesheets:[],dialogOptions:{modal:!0,resizable:!1,draggable:!0,animation:!1},fontName:[{text:"Arial",value:"Arial,Helvetica,sans-serif"},{text:"Courier New",value:"'Courier New',Courier,monospace"},{text:"Georgia",value:"Georgia,serif"},{text:"Impact",value:"Impact,Charcoal,sans-serif"},{text:"Lucida Console",value:"'Lucida Console',Monaco,monospace"},{text:"Tahoma",value:"Tahoma,Geneva,sans-serif"},{text:"Times New Roman",value:"'Times New Roman',Times,serif"},{text:"Trebuchet MS",value:"'Trebuchet MS',Helvetica,sans-serif"},{text:"Verdana",value:"Verdana,Geneva,sans-serif"}],fontSize:[{text:"1 (8pt)",value:"xx-small"},{text:"2 (10pt)",value:"x-small"},{text:"3 (12pt)",value:"small"},{text:"4 (14pt)",value:"medium"},{text:"5 (18pt)",value:"large"},{text:"6 (24pt)",value:"x-large"},{text:"7 (36pt)",value:"xx-large"}],formatBlock:[{text:"Paragraph",value:"p"},{text:"Quotation",value:"blockquote"},{text:"Heading 1",value:"h1"},{text:"Heading 2",value:"h2"},{text:"Heading 3",value:"h3"},{text:"Heading 4",value:"h4"},{text:"Heading 5",value:"h5"},{text:"Heading 6",value:"h6"}],tools:["bold","italic","underline","strikethrough","fontName","fontSize","foreColor","backColor","justifyLeft","justifyCenter","justifyRight","justifyFull","insertUnorderedList","insertOrderedList","indent","outdent","formatBlock","createLink","unlink","insertImage"]},_nativeTools:["insertLineBreak","insertParagraph","redo","undo","insertHtml"],_tools:{undo:{options:{key:"Z",ctrl:!0}},redo:{options:{key:"Y",ctrl:!0}}},tools:{},value:function(d){var e=this.body,f=c.ui.editor.Dom;if(d===b)return c.ui.editor.Serializer.domToXhtml(e);this.pendingFormats.clear(),d=(d||"").replace(/<!\[CDATA\[(.*)?\]\]>/g,"<!--[CDATA[$1]]-->").replace(/<script([^>]*)>(.*)?<\/script>/ig,"<telerik:script $1>$2</telerik:script>").replace(/(<\/?img[^>]*>)[\r\n\v\f\t ]+/ig,"$1"),a.browser.msie||(d=d.replace(/<p([^>]*)>(\s*)?<\/p>/ig,'<p $1><br _moz_dirty="" /></p>'));if(a.browser.msie&&parseInt(a.browser.version,10)<9){d="<br/>"+d;var g="originalsrc",h="originalhref";d=d.replace(/href\s*=\s*(?:'|")?([^'">\s]*)(?:'|")?/,h+'="$1"'),d=d.replace(/src\s*=\s*(?:'|")?([^'">\s]*)(?:'|")?/,g+'="$1"'),e.innerHTML=d,f.remove(e.firstChild),a(e).find("telerik\\:script,script,link,img,a").each(function(){var a=this;a[h]&&(a.setAttribute("href",a[h]),a.removeAttribute(h)),a[g]&&(a.setAttribute("src",a[g]),a.removeAttribute(g))})}else e.innerHTML=d,a.browser.msie&&f.normalize(e);this.selectionRestorePoint=null,this.update()},focus:function(){this.window.focus()},update:function(a){this.textarea.value=a||this.options.encoded?this.encodedValue():this.value()},encodedValue:function(){return c.ui.editor.Dom.encode(this.value())},createRange:function(a){return c.ui.editor.RangeUtils.createRange(a||this.document)},getSelection:function(){return c.ui.editor.SelectionUtils.selectionFromDocument(this.document)},selectRange:function(a){this.focus();var b=this.getSelection();b.removeAllRanges(),b.addRange(a)},getRange:function(){var a=this.getSelection(),b=a.rangeCount>0?a.getRangeAt(0):this.createRange(),c=this.document;b.startContainer==c&&b.endContainer==c&&!b.startOffset&&!b.endOffset&&(b.setStart(this.body,0),b.collapse(!0));return b},selectedHtml:function(){return c.ui.editor.Serializer.domToXhtml(this.getRange().cloneContents())},paste:function(a){this.clipboard.paste(a)},exec:function(b,c){var d=this,e,f,h,i="",j;b=b.toLowerCase(),d.keyboard.isTypingInProgress()||(d.focus(),e=d.getRange(),f=d.document.body);for(h in d.options.tools)if(h.toLowerCase()==b){i=d.options.tools[h];break}if(i){e=d.getRange();if(!/undo|redo/i.test(b)&&i.willDelayExecution(e)){j=a.extend({},i),a.extend(j.options,{params:c}),d.pendingFormats.toggle(j),l(d);return}var k=i.command?i.command(g({range:e},c)):null;d.trigger("execute",{name:b,command:k});if(/undo|redo/i.test(b))d.undoRedoStack[b]();else if(k){k.managesUndoRedo||d.undoRedoStack.push(k),k.editor=d,k.exec();if(k.async){k.change=a.proxy(function(){l(d)},d);return}}l(d)}}});c.ui.plugin(s);var t=d.extend({init:function(a){this.options=a},initialize:function(a,b){a.attr({unselectable:"on",title:b.title})},command:function(a){return new this.options.command(a)},update:function(){},willDelayExecution:function(){return!1}});t.exec=function(a,b,c){a.exec(b,{value:c})};var u=t.extend({init:function(a){t.fn.init.call(this,a)},command:function(a){var b=this;return new c.ui.editor.FormatCommand(g(a,{formatter:b.options.formatter}))},update:function(a,b,c){var d=c.isPending(this.name),e=this.options.finder.isFormatted(b),f=d?!e:e;a.toggleClass("k-state-active",f)}});g(c.ui,{editor:{ToolTemplate:j,EditorUtils:k,Tool:t,FormatTool:u}})}(jQuery),function(a){function k(a){var b={},c,d;for(c=0,d=a.length;c<d;c++)b[a[c]]=!0;return b}var b=window.kendo,c=a.map,d=a.extend,e="style",f="float",g="cssFloat",h="styleFloat",i="class",j="k-marker",l=k("area,base,basefont,br,col,frame,hr,img,input,isindex,link,meta,param,embed".split(",")),m="div,p,h1,h2,h3,h4,h5,h6,address,applet,blockquote,button,center,dd,dir,dl,dt,fieldset,form,frameset,hr,iframe,isindex,li,map,menu,noframes,noscript,object,ol,pre,script,table,tbody,td,tfoot,th,thead,tr,ul".split(","),n=k(m),o="span,em,a,abbr,acronym,applet,b,basefont,bdo,big,br,button,cite,code,del,dfn,font,i,iframe,img,input,ins,kbd,label,map,object,q,s,samp,script,select,small,strike,strong,sub,sup,textarea,tt,u,var".split(","),p=k(o),q=k("checked,compact,declare,defer,disabled,ismap,multiple,nohref,noresize,noshade,nowrap,readonly,selected".split(",")),r=function(a){a.nodeType==1&&a.normalize()};a.browser.msie&&parseInt(a.browser.version,10)>=8&&(r=function(a){if(a.nodeType==1&&a.firstChild){var b=a.firstChild,c=b;for(;;){c=c.nextSibling;if(!c)break;c.nodeType==3&&b.nodeType==3&&(c.nodeValue=b.nodeValue+c.nodeValue,A.remove(b)),b=c}}});var s=/^\s+$/,t=/rgb\s*\(\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*\)/i,u=/&/g,v=/</g,w=/>/g,x=/\u00a0/g,y=/\ufeff/g,z="color,padding-left,padding-right,padding-top,padding-bottom,background-color,background-attachment,background-image,background-position,background-repeat,border-top-style,border-top-width,border-top-color,border-bottom-style,border-bottom-width,border-bottom-color,border-left-style,border-left-width,border-left-color,border-right-style,border-right-width,border-right-color,font-family,font-size,font-style,font-variant,font-weight,line-height".split(","),A={findNodeIndex:function(a){var b=0;for(;;){a=a.previousSibling;if(!a)break;b++}return b},isDataNode:function(a){return a&&a.nodeValue!==null&&a.data!==null},isAncestorOf:function(b,c){try{return!A.isDataNode(b)&&(a.contains(b,A.isDataNode(c)?c.parentNode:c)||c.parentNode==b)}catch(d){return!1}},isAncestorOrSelf:function(a,b){return A.isAncestorOf(a,b)||a==b},findClosestAncestor:function(a,b){if(A.isAncestorOf(a,b))while(b&&b.parentNode!=a)b=b.parentNode;return b},getNodeLength:function(a){return A.isDataNode(a)?a.length:a.childNodes.length},splitDataNode:function(a,b){var c=a.cloneNode(!1);a.deleteData(b,a.length),c.deleteData(0,b),A.insertAfter(c,a)},attrEquals:function(b,c){for(var d in c){var e=b[d];d==f&&(e=b[a.support.cssFloat?g:h]);if(typeof e=="object"){if(!A.attrEquals(e,c[d]))return!1}else if(e!=c[d])return!1}return!0},blockParentOrBody:function(a){return A.parentOfType(a,m)||a.ownerDocument.body},blockParents:function(b){var c=[],d,e;for(d=0,e=b.length;d<e;d++){var f=A.parentOfType(b[d],A.blockElements);f&&a.inArray(f,c)<0&&c.push(f)}return c},windowFromDocument:function(a){return a.defaultView||a.parentWindow},normalize:r,blockElements:m,inlineElements:o,empty:l,fillAttrs:q,toHex:function(a){var b=t.exec(a);if(!b)return a;return"#"+c(b.slice(1),function(a){a=parseInt(a,10).toString(16);return a.length>1?a:"0"+a}).join("")},encode:function(a){return a.replace(u,"&amp;").replace(v,"&lt;").replace(w,"&gt;").replace(x,"&nbsp;")},name:function(a){return a.nodeName.toLowerCase()},significantChildNodes:function(b){return a.grep(b.childNodes,function(a){return a.nodeType!=3||!A.isWhitespace(a)})},lastTextNode:function(a){var b=null;if(a.nodeType==3)return a;for(var c=a.lastChild;c;c=c.previousSibling){b=A.lastTextNode(c);if(b)return b}return b},is:function(a,b){return A.name(a)==b},isMarker:function(a){return a.className==j},isWhitespace:function(a){return s.test(a.nodeValue)},isBlock:function(a){return n[A.name(a)]},isEmpty:function(a){return l[A.name(a)]},isInline:function(a){return p[A.name(a)]},scrollTo:function(b){b.ownerDocument.body.scrollTop=a(A.isDataNode(b)?b.parentNode:b).offset().top},insertAt:function(a,b,c){a.insertBefore(b,a.childNodes[c]||null)},insertBefore:function(a,b){return b.parentNode?b.parentNode.insertBefore(a,b):b},insertAfter:function(a,b){return b.parentNode.insertBefore(a,b.nextSibling)},remove:function(a){a.parentNode.removeChild(a)},trim:function(a){for(var b=a.childNodes.length-1;b>=0;b--){var c=a.childNodes[b];A.isDataNode(c)?(c.nodeValue.replace(y,"").length||A.remove(c),A.isWhitespace(c)&&A.insertBefore(c,a)):c.className!=j&&(A.trim(c),!c.childNodes.length&&!A.isEmpty(c)&&A.remove(c))}return a},parentOfType:function(a,b){do a=a.parentNode;while(a&&!A.ofType(a,b));return a},ofType:function(b,c){return a.inArray(A.name(b),c)>=0},changeTag:function(a,b){var c=A.create(a.ownerDocument,b),d=a.attributes,f,g,h,j,k;for(f=0,g=d.length;f<g;f++)k=d[f],k.specified&&(h=k.nodeName,j=k.nodeValue,h==i?c.className=j:h==e?c.style.cssText=a.style.cssText:c.setAttribute(h,j));while(a.firstChild)c.appendChild(a.firstChild);A.insertBefore(c,a),A.remove(a);return c},wrap:function(a,b){A.insertBefore(b,a),b.appendChild(a);return b},unwrap:function(a){var b=a.parentNode;while(a.firstChild)b.insertBefore(a.firstChild,a);b.removeChild(a)},create:function(a,b,c){return A.attr(a.createElement(b),c)},attr:function(a,b){b=d({},b),b&&e in b&&(A.style(a,b.style),delete b.style);return d(a,b)},style:function(b,c){a(b).css(c||{})},unstyle:function(b,c){for(var d in c)d==f&&(d=a.support.cssFloat?g:h),b.style[d]="";b.style.cssText===""&&b.removeAttribute(e)},inlineStyle:function(b,d,e){var f=a(A.create(b,d,e)),g;b.body.appendChild(f[0]),g=c(z,function(b){return a.browser.msie&&b=="line-height"&&f.css(b)=="1px"?"line-height:1.5":b+":"+f.css(b)}).join(";"),f.remove();return g},removeClass:function(b,c){var d=" "+b.className+" ",e=c.split(" "),f,g;for(f=0,g=e.length;f<g;f++)d=d.replace(" "+e[f]+" "," ");d=a.trim(d),d.length?b.className=d:b.removeAttribute(i)},commonAncestor:function(){var a=arguments.length,b=[],c=Infinity,d=null,e,f,g,h,i;if(!a)return null;if(a==1)return arguments[0];for(e=0;e<a;e++){f=[],g=arguments[e];while(g)f.push(g),g=g.parentNode;b.push(f.reverse()),c=Math.min(c,f.length)}if(a==1)return b[0][0];for(e=0;e<c;e++){h=b[0][e];for(i=1;i<a;i++)if(h!=b[i][e])return d;d=h}return d}};b.ui.editor.Dom=A}(jQuery),function(a,b){var c=window.kendo,d=c.ui.editor,e=d.Dom,f=a.extend,g="xx-small,x-small,small,medium,large,x-large,xx-large".split(","),h=/"/g,i=/<br[^>]*>/i,j=/<p><\/p>/i,k={domToXhtml:function(c){function m(b,c){var g=b.nodeType,h,i,j,m,n;if(g==1){h=e.name(b);if(!h||b.attributes._moz_dirty&&e.is(b,"br"))return;i=f[h];if(i){i.start(b),l(b),i.end(b);return}d.push("<"),d.push(h),k(b),e.empty[h]?d.push(" />"):(d.push(">"),l(b,c||e.is(b,"pre")),d.push("</"),d.push(h),d.push(">"))}else if(g==3){m=b.nodeValue;if(!c&&a.support.leadingWhitespace){j=b.parentNode,n=b.previousSibling,n||(n=(e.isInline(j)?j:b).previousSibling);if(!n||n.innerHTML===""||e.isBlock(n))m=m.replace(/^[\r\n\v\f\t ]+/,"");m=m.replace(/ +/," ")}d.push(e.encode(m))}else g==4?(d.push("<![CDATA["),d.push(b.data),d.push("]]>")):g==8&&(b.data.indexOf("[CDATA[")<0?(d.push("<!--"),d.push(b.data),d.push("-->")):(d.push("<!"),d.push(b.data),d.push(">")))}function l(a,b){for(var c=a.firstChild;c;c=c.nextSibling)m(c,b)}function k(c){var f=[],g=c.attributes,i,j,k,l=a.trim;if(e.is(c,"img")){var m=c.style.width,n=c.style.height,o=a(c);m&&(o.attr("width",parseInt(m,10)),e.unstyle(c,{width:b})),n&&(o.attr("height",parseInt(n,10)),e.unstyle(c,{height:b}))}for(j=0,k=g.length;j<k;j++){i=g[j];var p=i.nodeName;(i.specified||p=="value"&&!c.value||p=="type"&&i.nodeValue=="text")&&p.indexOf("_moz")<0&&p!="complete"&&p!="altHtml"&&f.push(i)}if(!!f.length){f.sort(function(a,b){return a.nodeName>b.nodeName?1:a.nodeName<b.nodeName?-1:0});for(j=0,k=f.length;j<k;j++){i=f[j];var q=i.nodeName,r=i.nodeValue;d.push(" "),d.push(q),d.push('="');if(q=="style"){var s=l(r||c.style.cssText).split(";");for(var t=0,u=s.length;t<u;t++){var v=s[t];if(v.length){var w=v.split(":"),x=l(w[0].toLowerCase()),y=l(w[1]);if(x=="font-size-adjust"||x=="font-stretch")continue;x.indexOf("color")>=0&&(y=e.toHex(y)),x.indexOf("font")>=0&&(y=y.replace(h,"'")),d.push(x),d.push(":"),d.push(y),d.push(";")}}}else q=="src"||q=="href"?d.push(c.getAttribute(q,2)):d.push(e.fillAttrs[q]?q:r);d.push('"')}}}var d=[],f={"telerik:script":{start:function(a){d.push("<script"),k(a),d.push(">")},end:function(){d.push("</script>")}},b:{start:function(){d.push("<strong>")},end:function(){d.push("</strong>")}},i:{start:function(){d.push("<em>")},end:function(){d.push("</em>")}},u:{start:function(){d.push('<span style="text-decoration:underline;">')},end:function(){d.push("</span>")}},iframe:{start:function(a){d.push("<iframe"),k(a),d.push(">")},end:function(){d.push("</iframe>")}},font:{start:function(a){d.push('<span style="');var b=a.getAttribute("color"),c=g[a.getAttribute("size")],f=a.getAttribute("face");b&&(d.push("color:"),d.push(e.toHex(b)),d.push(";")),f&&(d.push("font-face:"),d.push(f),d.push(";")),c&&(d.push("font-size:"),d.push(c),d.push(";")),d.push('">')},end:function(a){d.push("</span>")}}};l(c),d=d.join("");if(d.replace(i,"").replace(j,"")==="")return"";return d}};f(d,{Serializer:k})}(jQuery),function(a){function t(a,b,c){var d=f.create(b.ownerDocument,"a"),e=a.duplicate();e.collapse(c);var g=e.parentElement();do g.insertBefore(d,d.previousSibling),e.moveToElementText(d);while(e.compareEndPoints(c?"StartToStart":"StartToEnd",a)>0&&d.previousSibling);e.setEndPoint(c?"EndToStart":"EndToEnd",a);var i=d.nextSibling;i?(f.remove(d),h(i)?b[c?"setStart":"setEnd"](i,e.text.length):b[c?"setStartBefore":"setEndBefore"](i)):(i=d.previousSibling,i&&h(i)?(b.setEnd(i,i.nodeValue.length),f.remove(d)):(b.selectNodeContents(g),f.remove(d),b.endOffset-=1))}function s(a,b,c){var d=b[c?"startContainer":"endContainer"],e=b[c?"startOffset":"endOffset"],g=0,i=h(d)?d:d.childNodes[e]||null,j=h(d)?d.parentNode:d;if(d.nodeType==3||d.nodeType==4)g=e;var k=j.insertBefore(f.create(b.ownerDocument,"a"),i),l=b.ownerDocument.body.createTextRange();l.moveToElementText(k),f.remove(k),l[c?"moveStart":"moveEnd"]("character",g),l.collapse(!1),a.setEndPoint(c?"StartToStart":"EndToStart",l)}function p(a){a.collapsed=a.startContainer==a.endContainer&&a.startOffset==a.endOffset;var b=a.startContainer;while(b&&b!=a.endContainer&&!f.isAncestorOf(b,a.endContainer))b=b.parentNode;a.commonAncestorContainer=b}function o(a,b){function c(a){try{return n(a.startContainer,a.endContainer,a.startOffset,a.endOffset)<0}catch(b){return!0}}c(a)&&(b?(a.commonAncestorContainer=a.endContainer=a.startContainer,a.endOffset=a.startOffset):(a.commonAncestorContainer=a.startContainer=a.endContainer,a.startOffset=a.endOffset),a.collapsed=!0)}function n(a,b,c,d){if(a==b)return d-c;var e=b;while(e&&e.parentNode!=a)e=e.parentNode;if(e)return g(e)-c;e=a;while(e&&e.parentNode!=b)e=e.parentNode;if(e)return d-g(e)-1;var h=f.commonAncestor(a,b),i=a;while(i&&i.parentNode!=h)i=i.parentNode;i||(i=h);var j=b;while(j&&j.parentNode!=h)j=j.parentNode;j||(j=h);if(i==j)return 0;return g(j)-g(i)}var b=window.kendo,c=b.Class,d=a.extend,e=b.ui.editor,f=e.Dom,g=f.findNodeIndex,h=f.isDataNode,i=f.findClosestAncestor,j=f.getNodeLength,k=f.normalize,l={selectionFromWindow:function(b){if(a.browser.msie&&a.browser.version<9)return new r(b.document);return b.getSelection()},selectionFromRange:function(a){var b=y.documentFromRange(a);return l.selectionFromDocument(b)},selectionFromDocument:function(a){return l.selectionFromWindow(f.windowFromDocument(a))}},m=c.extend({init:function(b){a.extend(this,{ownerDocument:b,startContainer:b,endContainer:b,commonAncestorContainer:b,startOffset:0,endOffset:0,collapsed:!0})},setStart:function(a,b){this.startContainer=a,this.startOffset=b,p(this),o(this,!0)},setEnd:function(a,b){this.endContainer=a,this.endOffset=b,p(this),o(this,!1)},setStartBefore:function(a){this.setStart(a.parentNode,g(a))},setStartAfter:function(a){this.setStart(a.parentNode,g(a)+1)},setEndBefore:function(a){this.setEnd(a.parentNode,g(a))},setEndAfter:function(a){this.setEnd(a.parentNode,g(a)+1)},selectNode:function(a){this.setStartBefore(a),this.setEndAfter(a)},selectNodeContents:function(a){this.setStart(a,0),this.setEnd(a,a[a.nodeType===1?"childNodes":"nodeValue"].length)},collapse:function(a){var b=this;a?b.setEnd(b.startContainer,b.startOffset):b.setStart(b.endContainer,b.endOffset)},deleteContents:function(){var a=this,b=a.cloneRange();a.startContainer!=a.commonAncestorContainer&&a.setStartAfter(i(a.commonAncestorContainer,a.startContainer)),a.collapse(!0),function c(a){while(a.next())a.hasPartialSubtree()?c(a.getSubtreeIterator()):a.remove()}(new q(b))},cloneContents:function(){var a=y.documentFromRange(this);return function b(c){var d,e=a.createDocumentFragment();while(d=c.next())d=d.cloneNode(!c.hasPartialSubtree()),c.hasPartialSubtree()&&d.appendChild(b(c.getSubtreeIterator())),e.appendChild(d);return e}(new q(this))},extractContents:function(){var a=this,b=a.cloneRange();a.startContainer!=a.commonAncestorContainer&&a.setStartAfter(i(a.commonAncestorContainer,a.startContainer)),a.collapse(!0);var c=y.documentFromRange(a);return function d(b){var e,f=c.createDocumentFragment();while(e=b.next())b.hasPartialSubtree()?(e=e.cloneNode(!1),e.appendChild(d(b.getSubtreeIterator()))):b.remove(a.originalRange),f.appendChild(e);return f}(new q(b))},insertNode:function(a){var b=this;h(b.startContainer)?(b.startOffset!=b.startContainer.nodeValue.length&&f.splitDataNode(b.startContainer,b.startOffset),f.insertAfter(a,b.startContainer)):f.insertAt(b.startContainer,a,b.startOffset),b.setStart(b.startContainer,b.startOffset)},cloneRange:function(){return a.extend(new m(this.ownerDocument),{startContainer:this.startContainer,endContainer:this.endContainer,commonAncestorContainer:this.commonAncestorContainer,startOffset:this.startOffset,endOffset:this.endOffset,collapsed:this.collapsed,originalRange:this})},toString:function(){var a=this.startContainer.nodeName,b=this.endContainer.nodeName;return[a=="#text"?this.startContainer.nodeValue:a,"(",this.startOffset,") : ",b=="#text"?this.endContainer.nodeValue:b,"(",this.endOffset,")"].join("")}}),q=c.extend({init:function(b){a.extend(this,{range:b,_current:null,_next:null,_end:null});if(!b.collapsed){var c=b.commonAncestorContainer;this._next=b.startContainer==c&&!h(b.startContainer)?b.startContainer.childNodes[b.startOffset]:i(c,b.startContainer),this._end=b.endContainer==c&&!h(b.endContainer)?b.endContainer.childNodes[b.endOffset]:i(c,b.endContainer).nextSibling}},hasNext:function(){return!!this._next},next:function(){var a=this,b=a._current=a._next;a._next=a._current&&a._current.nextSibling!=a._end?a._current.nextSibling:null,h(a._current)&&(a.range.endContainer==a._current&&(b=b.cloneNode(!0),b.deleteData(a.range.endOffset,b.length-a.range.endOffset)),a.range.startContainer==a._current&&(b=b.cloneNode(!0),b.deleteData(0,a.range.startOffset)));return b},traverse:function(a){function d(){b._current=b._next,b._next=b._current&&b._current.nextSibling!=b._end?b._current.nextSibling:null;return b._current}var b=this,c;while(c=d())b.hasPartialSubtree()?b.getSubtreeIterator().traverse(a):a(c);return c},remove:function(a){var b=this,c=b.range.startContainer==b._current,d=b.range.endContainer==b._current,e,i,j;if(h(b._current)&&(c||d))e=c?b.range.startOffset:0,i=d?b.range.endOffset:b._current.length,j=i-e,a&&(c||d)&&(b._current==a.startContainer&&e<=a.startOffset&&(a.startOffset-=j),b._current==a.endContainer&&i<=a.endOffset&&(a.endOffset-=j)),b._current.deleteData(e,j);else{var k=b._current.parentNode;if(a&&(b.range.startContainer==k||b.range.endContainer==k)){var l=g(b._current);k==a.startContainer&&l<=a.startOffset&&(a.startOffset-=1),k==a.endContainer&&l<a.endOffset&&(a.endOffset-=1)}f.remove(b._current)}},hasPartialSubtree:function(){return!h(this._current)&&(f.isAncestorOrSelf(this._current,this.range.startContainer)||f.isAncestorOrSelf(this._current,this.range.endContainer))},getSubtreeIterator:function(){var a=this,b=a.range.cloneRange();b.selectNodeContents(a._current),f.isAncestorOrSelf(a._current,a.range.startContainer)&&b.setStart(a.range.startContainer,a.range.startOffset),f.isAncestorOrSelf(a._current,a.range.endContainer)&&b.setEnd(a.range.endContainer,a.range.endOffset);return new q(b)}}),r=c.extend({init:function(a){this.ownerDocument=a,this.rangeCount=1},addRange:function(a){var b=this.ownerDocument.body.createTextRange();s(b,a,!1),s(b,a,!0),b.select()},removeAllRanges:function(){this.ownerDocument.selection.empty()},getRangeAt:function(){var a,b=new m(this.ownerDocument),c=this.ownerDocument.selection,d;try{a=c.createRange(),d=a.item?a.item(0):a.parentElement();if(d.ownerDocument!=this.ownerDocument)return b}catch(e){return b}if(c.type=="Control")b.selectNode(a.item(0));else{t(a,b,!0),t(a,b,!1),b.startContainer.nodeType==9&&b.setStart(b.endContainer,b.startOffset),b.endContainer.nodeType==9&&b.setEnd(b.startContainer,b.endOffset),a.compareEndPoints("StartToEnd",a)===0&&b.collapse(!1);var f=b.startContainer,i=b.endContainer,k=this.ownerDocument.body;if(!b.collapsed&&b.startOffset===0&&b.endOffset==j(b.endContainer)&&(f!=i||!h(f)||f.parentNode!=k)){var l=!1,n=!1;while(g(f)===0&&f==f.parentNode.firstChild&&f!=k)f=f.parentNode,l=!0;while(g(i)==j(i.parentNode)-1&&i==i.parentNode.lastChild&&i!=k)i=i.parentNode,n=!0;f==k&&i==k&&l&&n&&(b.setStart(f,0),b.setEnd(i,j(k)))}}return b}}),u=c.extend({init:function(a){this.enumerate=function(){function c(a){if(f.is(a,"img")||a.nodeType==3&&!f.isWhitespace(a))b.push(a);else{a=a.firstChild;while(a)c(a),a=a.nextSibling}}var b=[];(new q(a)).traverse(c);return b}}}),v=c.extend({init:function(a){var b=this;b.range=a,b.rootNode=y.documentFromRange(a),b.body=b.rootNode.body,b.html=b.body.innerHTML,b.startContainer=b.nodeToPath(a.startContainer),b.endContainer=b.nodeToPath(a.endContainer),b.startOffset=b.offset(a.startContainer,a.startOffset),b.endOffset=b.offset(a.endContainer,a.endOffset)},index:function(a){var b=0,c=a.nodeType;while(a=a.previousSibling){var d=a.nodeType;(d!=3||c!=d)&&b++,c=d}return b},offset:function(a,b){if(a.nodeType==3)while((a=a.previousSibling)&&a.nodeType==3)b+=a.nodeValue.length;return b},nodeToPath:function(a){var b=[];while(a!=this.rootNode)b.push(this.index(a)),a=a.parentNode;return b},toRangePoint:function(a,b,c,d){var e=this.rootNode,f=c.length,g=d;while(f--)e=e.childNodes[c[f]];while(e.nodeType==3&&e.nodeValue.length<g)g-=e.nodeValue.length,e=e.nextSibling;a[b?"setStart":"setEnd"](e,g)},toRange:function(){var a=this,b=a.range.cloneRange();a.toRangePoint(b,!0,a.startContainer,a.startOffset),a.toRangePoint(b,!1,a.endContainer,a.endOffset);return b}}),w=c.extend({init:function(){this.caret=null},addCaret:function(a){var b=this;b.caret=f.create(y.documentFromRange(a),"span",{className:"k-marker"}),a.insertNode(b.caret),a.selectNode(b.caret);return b.caret},removeCaret:function(b){var c=this,d=c.caret.previousSibling,e=0;d&&(e=h(d)?d.nodeValue.length:g(d));var i=c.caret.parentNode,j=d?g(d):0;f.remove(c.caret),k(i);var l=i.childNodes[j];if(h(l))b.setStart(l,e);else if(l){var m=f.lastTextNode(l);m?b.setStart(m,m.nodeValue.length):b[d?"setStartAfter":"setStartBefore"](l)}else!a.browser.msie&&!i.innerHTML&&(i.innerHTML='<br _moz_dirty="" />'),b.selectNodeContents(i);b.collapse(!0)},add:function(a,b){var c=this;b&&a.collapsed&&(c.addCaret(a),a=y.expand(a));var d=a.cloneRange();d.collapse(!1),c.end=f.create(y.documentFromRange(a),"span",{className:"k-marker"}),d.insertNode(c.end),d=a.cloneRange(),d.collapse(!0),c.start=c.end.cloneNode(!0),d.insertNode(c.start),a.setStartBefore(c.start),a.setEndAfter(c.end),k(a.commonAncestorContainer);return a},remove:function(a){var b=this,c=b.start,d=b.end,e,i,j;k(a.commonAncestorContainer);while(!c.nextSibling&&c.parentNode)c=c.parentNode;while(!d.previousSibling&&d.parentNode)d=d.parentNode;e=c.previousSibling&&c.previousSibling.nodeType==3&&c.nextSibling&&c.nextSibling.nodeType==3,i=d.previousSibling&&d.previousSibling.nodeType==3&&d.nextSibling&&d.nextSibling.nodeType==3,j=e&&i,c=c.nextSibling,d=d.previousSibling;var l=!1,m=!1;c==b.end&&(m=!!b.start.previousSibling,c=d=b.start.previousSibling||b.end.nextSibling,l=!0),f.remove(b.start),f.remove(b.end);if(!c||!d)a.selectNodeContents(a.commonAncestorContainer),a.collapse(!0);else{var n=l?h(c)?c.nodeValue.length:c.childNodes.length:0,o=h(d)?d.nodeValue.length:d.childNodes.length;if(c.nodeType==3)while(c.previousSibling&&c.previousSibling.nodeType==3)c=c.previousSibling,n+=c.nodeValue.length;if(d.nodeType==3)while(d.previousSibling&&d.previousSibling.nodeType==3)d=d.previousSibling,o+=d.nodeValue.length;var p=g(c),q=c.parentNode,r=g(d),s=d.parentNode;for(var t=c;t.previousSibling;t=t.previousSibling)t.nodeType==3&&t.previousSibling.nodeType==3&&p--;for(var u=d;u.previousSibling;u=u.previousSibling)u.nodeType==3&&u.previousSibling.nodeType==3&&r--;k(q),c.nodeType==3&&(c=q.childNodes[p]),k(s),d.nodeType==3&&(d=s.childNodes[r]),l?(c.nodeType==3?a.setStart(c,n):a[m?"setStartAfter":"setStartBefore"](c),a.collapse(!0)):(c.nodeType==3?a.setStart(c,n):a.setStartBefore(c),d.nodeType==3?a.setEnd(d,o):a.setEndAfter(d)),b.caret&&b.removeCaret(a)}}}),x=/[\u0009-\u000d]|\u0020|\u00a0|\ufeff|\.|,|;|:|!|\(|\)|\?/,y={nodes:function(a){var b=y.textNodes(a);b.length||(a.selectNodeContents(a.commonAncestorContainer),b=y.textNodes(a),b.length||(b=f.significantChildNodes(a.commonAncestorContainer)));return b},textNodes:function(a){return(new u(a)).enumerate()},documentFromRange:function(a){var b=a.startContainer;return b.nodeType==9?b:b.ownerDocument},createRange:function(b){if(a.browser.msie&&a.browser.version<9)return new m(b);return b.createRange()},selectRange:function(a){var b=y.image(a);b&&(a.setStartAfter(b),a.setEndAfter(b));var c=l.selectionFromRange(a);c.removeAllRanges(),c.addRange(a)},split:function(a,b,c){function d(d){var e=a.cloneRange();e.collapse(d),e[d?"setStartBefore":"setEndAfter"](b);var g=e.extractContents();c&&(g=f.trim(g)),f[d?"insertBefore":"insertAfter"](g,b)}d(!0),d(!1)},getMarkers:function(a){var b=[];(new q(a)).traverse(function(a){a.className=="k-marker"&&b.push(a)});return b},image:function(a){var b=[];(new q(a)).traverse(function(a){f.is(a,"img")&&b.push(a)});if(b.length==1)return b[0]},expand:function(a){var b=a.cloneRange(),c=b.startContainer.childNodes[b.startOffset===0?0:b.startOffset-1],d=b.endContainer.childNodes[b.endOffset];if(!h(c)||!h(d))return b;var e=c.nodeValue,f=d.nodeValue;if(!e||!f)return b;var g=e.split("").reverse().join("").search(x),i=f.search(x);if(!g||!i)return b;i=i==-1?f.length:i,g=g==-1?0:e.length-g,b.setStart(c,g),b.setEnd(d,i);return b},isExpandable:function(a){var b=a.startContainer,c=y.documentFromRange(a);if(b==c||b==c.body)return!1;var d=a.cloneRange(),e=b.nodeValue;if(!e)return!1;var f=e.substring(0,d.startOffset),g=e.substring(d.startOffset),h=0,i=0;f&&(h=f.split("").reverse().join("").search(x)),g&&(i=g.search(x));return h&&i}};d(e,{SelectionUtils:l,W3CRange:m,RangeIterator:q,W3CSelection:r,RangeEnumerator:u,RestorePoint:v,Marker:w,RangeUtils:y})}(jQuery),function(a){var b=window.kendo,c=b.Class,d=b.ui.editor,e=d.EditorUtils,f=e.registerTool,g=d.Dom,h=d.RangeUtils,i=h.selectRange,j=d.Tool,k=d.ToolTemplate,l=d.RestorePoint,m=d.Marker,n=a.extend,o=c.extend({init:function(a){var b=this;b.options=a,b.restorePoint=new l(a.range),b.marker=new m,b.formatter=a.formatter},getRange:function(){return this.restorePoint.toRange()},lockRange:function(a){return this.marker.add(this.getRange(),a)},releaseRange:function(a){this.marker.remove(a),i(a)},undo:function(){var a=this.restorePoint;a.body.innerHTML=a.html,i(a.toRange())},redo:function(){this.exec()},exec:function(){var a=this,b=a.lockRange(!0);a.formatter.editor=a.editor,a.formatter.toggle(b),a.releaseRange(b)}}),p=c.extend({init:function(a,b){this.body=a.body,this.startRestorePoint=a,this.endRestorePoint=b},redo:function(){this.body.innerHTML=this.endRestorePoint.html,i(this.endRestorePoint.toRange())},undo:function(){this.body.innerHTML=this.startRestorePoint.html,i(this.startRestorePoint.toRange())}}),q=o.extend({init:function(a){o.fn.init.call(this,a),this.managesUndoRedo=!0},exec:function(){var a=this.editor,b=a.getRange(),c=new l(b);a.clipboard.paste(this.options.value||""),a.undoRedoStack.push(new p(c,new l(a.getRange()))),a.focus()}}),r=j.extend({initialize:function(a,b){var c=b.editor;new d.SelectBox(a,{dataSource:c.options.insertHtml||[],dataTextField:"text",dataValueField:"value",change:function(a){j.exec(c,"insertHtml",this.value())},title:c.options.messages.insertHtml,highlightFirst:!1})},command:function(a){return new q(a)},update:function(a,b){var c=a.data("kendoSelectBox")||a.find("select").data("kendoSelectBox");c.close(),c.value(c.options.title)}}),s=c.extend({init:function(){this.stack=[],this.currentCommandIndex=-1},push:function(a){var b=this;b.stack=b.stack.slice(0,b.currentCommandIndex+1),b.currentCommandIndex=b.stack.push(a)-1},undo:function(){this.canUndo()&&this.stack[this.currentCommandIndex--].undo()},redo:function(){this.canRedo()&&this.stack[++this.currentCommandIndex].redo()},canUndo:function(){return this.currentCommandIndex>=0},canRedo:function(){return this.currentCommandIndex!=this.stack.length-1}}),t=c.extend({init:function(a){this.editor=a},keydown:function(a){var b=this,c=b.editor,d=c.keyboard,e=d.isTypingKey(a);if(e&&!d.isTypingInProgress()){var f=c.getRange();b.startRestorePoint=new l(f),d.startTyping(function(){c.selectionRestorePoint=b.endRestorePoint=new l(c.getRange()),c.undoRedoStack.push(new p(b.startRestorePoint,b.endRestorePoint))});return!0}return!1},keyup:function(a){var b=this.editor.keyboard;if(b.isTypingInProgress()){b.endTyping();return!0}return!1}}),u=c.extend({init:function(a){this.editor=a,this.systemCommandIsInProgress=!1},createUndoCommand:function(){var a=this;a.endRestorePoint=new l(a.editor.getRange()),a.editor.undoRedoStack.push(new p(a.startRestorePoint,a.endRestorePoint)),a.startRestorePoint=a.endRestorePoint},changed:function(){if(this.startRestorePoint)return this.startRestorePoint.html!=this.editor.body.innerHTML;return!1},keydown:function(a){var b=this,c=b.editor,d=c.keyboard;if(d.isModifierKey(a)){d.isTypingInProgress()&&d.endTyping(!0),b.startRestorePoint=new l(c.getRange());return!0}if(d.isSystem(a)){b.systemCommandIsInProgress=!0,b.changed()&&(b.systemCommandIsInProgress=!1,b.createUndoCommand());return!0}return!1},keyup:function(a){var b=this;if(b.systemCommandIsInProgress&&b.changed()){b.systemCommandIsInProgress=!1,b.createUndoCommand(a);return!0}return!1}}),v=c.extend({init:function(a){this.handlers=a,this.typingInProgress=!1},isCharacter:function(a){return a>=48&&a<=90||a>=96&&a<=111||a>=186&&a<=192||a>=219&&a<=222},toolFromShortcut:function(b,c){var d=String.fromCharCode(c.keyCode),e,f;for(e in b){f=a.extend({ctrl:!1,alt:!1,shift:!1},b[e].options);if((f.key==d||f.key==c.keyCode)&&f.ctrl==c.ctrlKey&&f.alt==c.altKey&&f.shift==c.shiftKey)return e}},isTypingKey:function(a){var b=a.keyCode;return this.isCharacter(b)&&!a.ctrlKey&&!a.altKey||b==32||b==13||b==8||b==46&&!a.shiftKey&&!a.ctrlKey&&!a.altKey},isModifierKey:function(a){var b=a.keyCode;return b==17&&!a.shiftKey&&!a.altKey||b==16&&!a.ctrlKey&&!a.altKey||b==18&&!a.ctrlKey&&!a.shiftKey},isSystem:function(a){return a.keyCode==46&&a.ctrlKey&&!a.altKey&&!a.shiftKey},startTyping:function(a){this.onEndTyping=a,this.typingInProgress=!0},stopTyping:function(){this.typingInProgress=!1,this.onEndTyping&&this.onEndTyping()},endTyping:function(b){var c=this;c.clearTimeout(),b?c.stopTyping():c.timeout=window.setTimeout(a.proxy(c.stopTyping,c),1e3)},isTypingInProgress:function(){return this.typingInProgress},clearTimeout:function(){window.clearTimeout(this.timeout)},notify:function(a,b){var c,d=this.handlers;for(c=0;c<d.length;c++)if(d[c][b](a))break},keydown:function(a){this.notify(a,"keydown")},keyup:function(a){this.notify(a,"keyup")}}),w=c.extend({init:function(a){this.editor=a,this.cleaners=[new x]},htmlToFragment:function(a){var b=this.editor,c=b.document,d=g.create(c,"div"),e=c.createDocumentFragment();d.innerHTML=a;while(d.firstChild)e.appendChild(d.firstChild);return e},isBlock:function(a){return/<(div|p|ul|ol|table|h[1-6])/i.test(a)},oncut:function(a){var b=this.editor,c=new l(b.getRange());setTimeout(function(){b.undoRedoStack.push(new p(c,new l(b.getRange())))})},onpaste:function(b){var c=this.editor,e=c.getRange(),f="﻿",h=new l(e),j=g.create(c.document,"div",{className:"k-paste-container",innerHTML:f});c.body.appendChild(j);if(c.body.createTextRange){b.preventDefault();var k=c.createRange();k.selectNodeContents(j),c.selectRange(k);var m=c.body.createTextRange();m.moveToElementText(j),a(c.body).unbind("paste"),m.execCommand("Paste"),a(c.body).bind("paste",a.proxy(arguments.callee,this))}else{var n=c.createRange();n.selectNodeContents(j),i(n)}setTimeout(function(){var a,b={html:""};i(e),g.remove(j),j.lastChild&&g.is(j.lastChild,"br")&&g.remove(j.lastChild),a=j.innerHTML,a!=f&&(b.html=a),c.trigger("paste",b),c.clipboard.paste(b.html,!0),c.undoRedoStack.push(new p(h,new l(c.getRange()))),d.EditorUtils.select(c)})},splittableParent:function(a,b){var c,d;if(a)return g.parentOfType(b,["p","ul","ol"])||b.parentNode;c=b.parentNode,d=b.ownerDocument.body;if(g.isInline(c))while(c.parentNode!=d&&!g.isBlock(c.parentNode))c=c.parentNode;return c},paste:function(a,b){var c=this.editor,d,e;for(d=0,e=this.cleaners.length;d<e;d++)this.cleaners[d].applicable(a)&&(a=this.cleaners[d].clean(a));b&&(a=a.replace(/(<br>(\s|&nbsp;)*)+(<\/?(div|p|li|col|t))/ig,"$3"),a=a.replace(/<(a|span)[^>]*><\/\1>/ig,"")),a=a.replace(/^<li/i,"<ul><li").replace(/li>$/g,"li></ul>");var f=this.isBlock(a),j=c.getRange();j.deleteContents(),j.startContainer==c.document&&j.selectNodeContents(c.body);var k=new m,l=k.addCaret(j),n=this.splittableParent(f,l),o=!1;!/body|td/.test(g.name(n))&&(f||g.isInline(n))&&(j.selectNode(l),h.split(j,n,!0),o=!0);var p=this.htmlToFragment(a);if(p.firstChild&&p.firstChild.className==="k-paste-container"){var q=[];for(d=0,e=p.childNodes.length;d<e;d++)q.push(p.childNodes[d].innerHTML);p=this.htmlToFragment(q.join("<br />"))}j.insertNode(p),n=this.splittableParent(f,l);if(o){while(l.parentNode!=n)g.unwrap(l.parentNode);g.unwrap(l.parentNode)}g.normalize(j.commonAncestorContainer),l.style.display="inline",g.scrollTo(l),k.removeCaret(j),i(j)}}),x=c.extend({init:function(){this.replacements=[/<\?xml[^>]*>/gi,"",/<!--(.|\n)*?-->/g,"",/&quot;/g,"'",/(?:<br>&nbsp;[\s\r\n]+|<br>)*(<\/?(h[1-6]|hr|p|div|table|tbody|thead|tfoot|th|tr|td|li|ol|ul|caption|address|pre|form|blockquote|dl|dt|dd|dir|fieldset)[^>]*>)(?:<br>&nbsp;[\s\r\n]+|<br>)*/g,"$1",/<br><br>/g,"<BR><BR>",/<br>/g," ",/<table([^>]*)>(\s|&nbsp;)+<t/gi,"<table$1><t",/<tr[^>]*>(\s|&nbsp;)*<\/tr>/gi,"",/<tbody[^>]*>(\s|&nbsp;)*<\/tbody>/gi,"",/<table[^>]*>(\s|&nbsp;)*<\/table>/gi,"",/<BR><BR>/g,"<br>",/^\s*(&nbsp;)+/gi,"",/(&nbsp;|<br[^>]*>)+\s*$/gi,"",/mso-[^;"]*;?/ig,"",/<(\/?)b(\s[^>]*)?>/ig,"<$1strong$2>",/<(\/?)i(\s[^>]*)?>/ig,"<$1em$2>",/<\/?(meta|link|style|o:|v:|x:)[^>]*>((?:.|\n)*?<\/(meta|link|style|o:|v:|x:)[^>]*>)?/ig,"",/style=(["|'])\s*\1/g,""]},applicable:function(a){return/class="?Mso|style="[^"]*mso-/i.test(a)},listType:function(a){if(/^[\u2022\u00b7\u00a7\u00d8o]\u00a0+/.test(a))return"ul";if(/^\s*\w+[\.\)]\u00a0{2,}/.test(a))return"ol"},lists:function(b){var c=g.create(document,"div",{innerHTML:b}),d=a(g.blockElements.join(","),c),e=-1,f,h={ul:{},ol:{}},i=c;for(var j=0;j<d.length;j++){var k=d[j];b=k.innerHTML.replace(/<\/?\w+[^>]*>/g,"").replace(/&nbsp;/g," ");var l=this.listType(b);if(!l||g.name(k)!="p"){k.innerHTML?(h={ul:{},ol:{}},i=c,e=-1):g.remove(k);continue}var m=parseFloat(k.style.marginLeft||0),n=h[l][m];if(m>e||!n)n=g.create(document,l),i==c?g.insertBefore(n,k):i.appendChild(n),h[l][m]=n;if(f!=l)for(var o in h)for(var p in h[o])a.contains(n,h[o][p])&&delete h[o][p];g.remove(k.firstChild),i=g.create(document,"li",{innerHTML:k.innerHTML}),n.appendChild(i),g.remove(k),e=m,f=l}return c.innerHTML},stripEmptyAnchors:function(a){return a.replace(/<a([^>]*)>\s*<\/a>/ig,function(a,b){if(!b||b.indexOf("href")<0)return"";return a})},clean:function(a){var b=this,c=b.replacements,d,e;for(d=0,e=c.length;d<e;d+=2)a=a.replace(c[d],c[d+1]);a=b.stripEmptyAnchors(a),a=b.lists(a),a=a.replace(/\s+class="?[^"\s>]*"?/ig,"");return a}});n(d,{Command:o,GenericCommand:p,InsertHtmlCommand:q,InsertHtmlTool:r,UndoRedoStack:s,TypingHandler:t,SystemHandler:u,Keyboard:v,Clipboard:w,MSWordFormatCleaner:x}),f("insertHtml",new r({template:new k({template:e.dropDownListTemplate,title:"Insert HTML",initialValue:"Insert HTML"})}))}(jQuery),function(a){function t(a){return a.collapsed&&!k.isExpandable(a)}var b=window.kendo,c=b.Class,d=b.ui.editor,e=b.ui.Editor.fn.options.formats,f=d.EditorUtils,g=d.Tool,h=d.ToolTemplate,i=d.FormatTool,j=d.Dom,k=d.RangeUtils,l=a.extend,m=d.EditorUtils.registerTool,n=d.EditorUtils.registerFormat,o="k-marker",p=c.extend({init:function(a){this.format=a},numberOfSiblings:function(a){var b=0,c=0,d=0,e=a.parentNode,f;for(f=e.firstChild;f;f=f.nextSibling)f!=a&&(f.className==o?d++:f.nodeType==3?b++:c++);return d>1&&e.firstChild.className==o&&e.lastChild.className==o?0:c+b},findSuitable:function(a,b){if(!b&&this.numberOfSiblings(a)>0)return null;return j.parentOfType(a,this.format[0].tags)},findFormat:function(a){var b=this.format,c=j.attrEquals,d,e,f,g,h;for(d=0,e=b.length;d<e;d++){f=a,g=b[d].tags,h=b[d].attr;if(f&&j.ofType(f,g)&&c(f,h))return f;while(f){f=j.parentOfType(f,g);if(f&&c(f,h))return f}}return null},isFormatted:function(a){var b,c;for(b=0,c=a.length;b<c;b++)if(this.findFormat(a[b]))return!0;return!1}}),q=c.extend({init:function(a,b){var c=this;c.finder=new p(a),c.attributes=l({},a[0].attr,b),c.tag=a[0].tags[0]},wrap:function(a){return j.wrap(a,j.create(a.ownerDocument,this.tag,this.attributes))},activate:function(a,b){var c=this;c.finder.isFormatted(b)?(c.split(a),c.remove(b)):c.apply(b)},toggle:function(a){var b=k.textNodes(a);b.length>0&&this.activate(a,b)},apply:function(a){var b=this,c=[],d,e,f,g;for(d=0,e=a.length;d<e;d++)f=a[d],g=b.finder.findSuitable(f),g?j.attr(g,b.attributes):g=b.wrap(f),c.push(g);b.consolidate(c)},remove:function(a){var b=this,c,d,e;for(c=0,d=a.length;c<d;c++)e=b.finder.findFormat(a[c]),e&&(b.attributes&&b.attributes.style?(j.unstyle(e,b.attributes.style),e.style.cssText||j.unwrap(e)):j.unwrap(e))},split:function(a){var b=k.textNodes(a),c=b.length,d,e;if(c>0)for(d=0;d<c;d++)e=this.finder.findFormat(b[d]),e&&k.split(a,e,!0)},consolidate:function(a){var b,c;while(a.length>1){b=a.pop(),c=a[a.length-1],b.previousSibling&&b.previousSibling.className==o&&c.appendChild(b.previousSibling);if(b.tagName==c.tagName&&b.previousSibling==c&&b.style.cssText==c.style.cssText){while(b.firstChild)c.appendChild(b.firstChild);j.remove(b)}}}}),r=p.extend({init:function(a,b){var c=this;c.format=a,c.greedyProperty=b,p.fn.init.call(c,a)},getInlineCssValue:function(b){var c=b.attributes,d=a.trim,e,f,g,h,i,k,l,m,n,o,p,q;if(!!c){for(e=0,f=c.length;e<f;e++){g=c[e],h=g.nodeName,i=g.nodeValue;if(g.specified&&h=="style"){k=d(i||b.style.cssText).split(";");for(m=0,n=k.length;m<n;m++){l=k[m];if(l.length){o=l.split(":"),p=d(o[0].toLowerCase()),q=d(o[1]);if(p!=this.greedyProperty)continue;return p.indexOf("color")>=0?j.toHex(q):q}}}}return}},getFormatInner:function(b){var c=a(j.isDataNode(b)?b.parentNode:b),d=c.parents().andSelf(),e,f,g;for(e=0,f=d.length;e<f;e++){g=this.greedyProperty=="className"?d[e].className:this.getInlineCssValue(d[e]);if(g)return g}return"inherit"},getFormat:function(a){var b=this.getFormatInner(a[0]),c,d;for(c=1,d=a.length;c<d;c++)if(b!=this.getFormatInner(a[c]))return"";return b},isFormatted:function(a){return this.getFormat(a)!==""}}),s=q.extend({init:function(a,b,c){var d=this;q.fn.init.call(d,a,b),d.greedyProperty=c,d.values=b,d.finder=new r(a,c)},activate:function(a,b){var c=this,d,e=c.greedyProperty,f="apply";c.split(a),e&&(d=e.replace(/-([a-z])/,function(a,b){return b.toUpperCase()}),c.values.style[d]=="inherit"&&(f="remove")),c[f](b)}}),u=i.extend({init:function(a){i.fn.init.call(this,l(a,{finder:new p(a.format),formatter:function(){return new q(a.format)}})),this.willDelayExecution=t}}),v=g.extend({init:function(c){var d=this;g.fn.init.call(d,c),d.type=a.browser.msie||b.support.touch?"kendoDropDownList":"kendoComboBox",d.format=[{tags:["span"]}],d.finder=new r(d.format,c.cssAttr)},command:function(a){var b=this.options,c=this.format,e={};return new d.FormatCommand(l(a,{formatter:function(){e[b.domAttr]=a.value;return new s(c,{style:e},b.cssAttr)}}))},willDelayExecution:t,update:function(a,b,c){var d=this,e=a.data(d.type),f=c.getPending(d.name),g=f&&f.params?f.params.value:d.finder.getFormat(b);e.close(),e.value(g)},initialize:function(a,b){var c=b.editor,d=this.options,e=d.name,f,h=[];d.defaultValue&&(h=[{text:c.options.messages[d.defaultValue[0].text],value:d.defaultValue[0].value}]),f=h.concat(d.items?d.items:c.options[e]),a[this.type]({dataTextField:"text",dataValueField:"value",dataSource:f,change:function(a){g.exec(c,e,this.value())},highlightFirst:!1}),a.closest(".k-widget").removeClass("k-"+e).find("*").andSelf().attr("unselectable","on"),a.data(this.type).value("inherit")}}),w=g.extend({init:function(a){g.fn.init.call(this,a),this.options=a,this.format=[{tags:["span"]}]},update:function(a){a.data("kendoColorPicker").close()},command:function(a){var b=this.options,c=this.format,e={};return new d.FormatCommand(l(a,{formatter:function(){e[b.domAttr]=a.value;return new s(c,{style:e},b.cssAttr)}}))},willDelayExecution:t,initialize:function(a,b){var c=b.editor,e=this.name;new d.ColorPicker(a,{value:"#000000",change:function(a){g.exec(c,e,a.value)}})}}),x=g.extend({init:function(a){var b=this;g.fn.init.call(b,a),b.format=[{tags:["span"]}],b.finder=new r(b.format,"className")},command:function(a){var b=this.format;return new d.FormatCommand(l(a,{formatter:function(){return new s(b,{className:a.value})}}))},update:function(a,b){var c=a.data("kendoSelectBox");c.close(),c.value(this.finder.getFormat(b))},initialize:function(a,b){var c=b.editor;new d.SelectBox(a,{dataTextField:"text",dataValueField:"value",dataSource:c.options.style,title:c.options.messages.style,change:function(a){g.exec(c,"style",this.value())},highlightFirst:!1}),a.closest(".k-widget").removeClass("k-"+this.name).find("*").andSelf().attr("unselectable","on")}});l(d,{InlineFormatFinder:p,InlineFormatter:q,GreedyInlineFormatFinder:r,GreedyInlineFormatter:s,InlineFormatTool:u,FontTool:v,ColorTool:w,StyleTool:x}),m("style",new d.StyleTool({template:new h({template:f.dropDownListTemplate,title:"Styles"})})),n("bold",[{tags:["strong"]},{tags:["span"],attr:{style:{fontWeight:"bold"}}}]),m("bold",new u({key:"B",ctrl:!0,format:e.bold,template:new h({template:f.buttonTemplate,title:"Bold"})})),n("italic",[{tags:["em"]},{tags:["span"],attr:{style:{fontStyle:"italic"}}}]),m("italic",new u({key:"I",ctrl:!0,format:e.italic,template:new h({template:f.buttonTemplate,title:"Italic"})})),n("underline",[{tags:["span"],attr:{style:{textDecoration:"underline"}}}]),m("underline",new u({key:"U",ctrl:!0,format:e.underline,template:new h({template:f.buttonTemplate,title:"Underline"})})),n("strikethrough",[{tags:["del"]},{tags:["span"],attr:{style:{textDecoration:"line-through"}}}]),m("strikethrough",new u({format:e.strikethrough,template:new h({template:f.buttonTemplate,title:"Strikethrough"})})),n("superscript",[{tags:["sup"]}]),m("superscript",new u({format:e.superscript,template:new h({template:f.buttonTemplate,title:"Superscript"})})),n("subscript",[{tags:["sub"]}]),m("subscript",new u({format:e.subscript,template:new h({template:f.buttonTemplate,title:"Subscript"})})),m("foreColor",new w({cssAttr:"color",domAttr:"color",name:"foreColor",template:new h({template:f.colorPickerTemplate,title:"Color"})})),m("backColor",new w({cssAttr:"background-color",domAttr:"backgroundColor",name:"backColor",template:new h({template:f.colorPickerTemplate,title:"Background Color"})})),m("fontName",new v({cssAttr:"font-family",domAttr:"fontFamily",name:"fontName",defaultValue:[{text:"fontNameInherit",value:"inherit"}],template:new h({template:f.comboBoxTemplate,title:"Font Name"})})),m("fontSize",new v({cssAttr:"font-size",domAttr:"fontSize",name:"fontSize",defaultValue:[{text:"fontSizeInherit",value:"inherit"}],template:new h({template:f.comboBoxTemplate,title:"Font Size"})}))}(jQuery),function(a){var b=window.kendo,c=b.Class,d=a.extend,e=b.ui.editor,f=b.ui.Editor.fn.options.formats,g=e.Dom,h=e.Command,i=e.Tool,j=e.ToolTemplate,k=e.FormatTool,l=e.EditorUtils,m=l.registerTool,n=l.registerFormat,o=e.RangeUtils,p=c.extend({init:function(a){this.format=a},contains:function(a,b){var c,d,e;for(c=0,d=b.length;c<d;c++){e=b[c];if(!e||!g.isAncestorOrSelf(a,e))return!1}return!0},findSuitable:function(b){var c=this.format,d=[],e,f,h;for(e=0,f=b.length;e<f;e++){h=g.ofType(b[e],c[0].tags)?b[e]:g.parentOfType(b[e],c[0].tags);if(!h)return[];a.inArray(h,d)<0&&d.push(h)}for(e=0,f=d.length;e<f;e++)if(this.contains(d[e],d))return[d[e]];return d},findFormat:function(a){var b=this.format,c,d,e,f,h;for(c=0,d=b.length;c<d;c++){e=a,f=b[c].tags,h=b[c].attr;while(e){if(g.ofType(e,f)&&g.attrEquals(e,h))return e;e=e.parentNode}}return null},getFormat:function(a){var b=this,c=function(a){return b.findFormat(g.isDataNode(a)?a.parentNode:a)},d=c(a[0]),e,f;if(!d)return"";for(e=1,f=a.length;e<f;e++)if(d!=c(a[e]))return"";return d.nodeName.toLowerCase()},isFormatted:function(a){for(var b=0,c=a.length;b<c;b++)if(!this.findFormat(a[b]))return!1;return!0}}),q=c.extend({init:function(a,b){this.format=a,this.values=b,this.finder=new p(a)},wrap:function(a,b,c){var d=c.length==1?g.blockParentOrBody(c[0]):g.commonAncestor.apply(null,c);g.isInline(d)&&(d=g.blockParentOrBody(d));var e=g.significantChildNodes(d),f=g.findNodeIndex(e[0]),h=g.create(d.ownerDocument,a,b),i,j;for(i=0;i<e.length;i++){j=e[i];if(g.isBlock(j)){g.attr(j,b),h.childNodes.length&&(g.insertBefore(h,j),h=h.cloneNode(!1)),f=g.findNodeIndex(j)+1;continue}h.appendChild(j)}h.firstChild&&g.insertAt(d,h,f)},apply:function(a){var b=this,c=g.is(a[0],"img")?[a[0]]:b.finder.findSuitable(a),e=c.length?l.formatByName(g.name(c[0]),b.format):b.format[0],f=e.tags[0],h=d({},e.attr,b.values),i,j;if(c.length)for(i=0,j=c.length;i<j;i++)g.attr(c[i],h);else b.wrap(f,h,a)},remove:function(a){var b,c,d,e;for(b=0,c=a.length;b<c;b++)d=this.finder.findFormat(a[b]),d&&(g.ofType(d,["p","img","li"])?(e=l.formatByName(g.name(d),this.format),e.attr.style&&g.unstyle(d,e.attr.style),e.attr.className&&g.removeClass(d,e.attr.className)):g.unwrap(d))},toggle:function(a){var b=this,c=o.nodes(a);b.finder.isFormatted(c)?b.remove(c):b.apply(c)}}),r=c.extend({init:function(a,b){var c=this;c.format=a,c.values=b,c.finder=new p(a)},apply:function(a){var b=this.format,c=g.blockParents(a),d=b[0].tags[0],f,h,i,j,k;if(c.length)for(f=0,h=c.length;f<h;f++)g.is(c[f],"li")?(i=c[f].parentNode,j=new e.ListFormatter(i.nodeName.toLowerCase(),d),k=this.editor.createRange(),k.selectNode(c[f]),j.toggle(k)):g.changeTag(c[f],d);else(new q(b,this.values)).apply(a)},toggle:function(a){var b=o.textNodes(a);b.length||(a.selectNodeContents(a.commonAncestorContainer),b=o.textNodes(a),b.length||(b=g.significantChildNodes(a.commonAncestorContainer))),this.apply(b)}}),s=h.extend({init:function(a){a.formatter=a.formatter(),h.fn.init.call(this,a)}}),t=k.extend({init:function(a){k.fn.init.call(this,d(a,{finder:new p(a.format),formatter:function(){return new q(a.format)}}))}}),u=i.extend({init:function(a){i.fn.init.call(this,a),this.finder=new p([{tags:g.blockElements}])},command:function(a){return new s(d(a,{formatter:function(){return new r([{tags:[a.value]}],{})}}))},update:function(a,b){var c;a.is("select")?c=a.data("kendoSelectBox"):c=a.find("select").data("kendoSelectBox"),c.close(),c.value(this.finder.getFormat(b))},initialize:function(a,b){var c=b.editor,d="formatBlock";new e.SelectBox(a,{dataTextField:"text",dataValueField:"value",dataSource:this.options.items?this.options.items:c.options.formatBlock,title:c.options.messages.formatBlock,change:function(a){i.exec(c,d,this.value())},highlightFirst:!1}),a.closest(".k-widget").removeClass("k-"+d).find("*").andSelf().attr("unselectable","on")}});d(e,{BlockFormatFinder:p,BlockFormatter:q,GreedyBlockFormatter:r,FormatCommand:s,BlockFormatTool:t,FormatBlockTool:u}),m("formatBlock",new u({template:new j({template:l.dropDownListTemplate})})),n("justifyLeft",[{tags:g.blockElements,attr:{style:{textAlign:"left"}}},{tags:["img"],attr:{style:{"float":"left"}}}]),m("justifyLeft",new t({format:f.justifyLeft,template:new j({template:l.buttonTemplate,title:"Justify Left"})})),n("justifyCenter",[{tags:g.blockElements,attr:{style:{textAlign:"center"}}},{tags:["img"],attr:{style:{display:"block",marginLeft:"auto",marginRight:"auto"}}}]),m("justifyCenter",new t({format:f.justifyCenter,template:new j({template:l.buttonTemplate,title:"Justify Center"})})),n("justifyRight",[{tags:g.blockElements,attr:{style:{textAlign:"right"}}},{tags:["img"],attr:{style:{"float":"right"}}}]),m("justifyRight",new t({format:f.justifyRight,template:new j({template:l.buttonTemplate,title:"Justify Right"})})),n("justifyFull",[{tags:g.blockElements,attr:{style:{textAlign:"justify"}}}]),m("justifyFull",new t({format:f.justifyFull,template:new j({template:l.buttonTemplate,title:"Justify Full"})}))}(jQuery),function(a){var b=window.kendo,c=a.extend,d=b.ui.editor,e=d.Dom,f=d.Command,g=d.Tool,h=d.BlockFormatter,i=e.normalize,j=d.RangeUtils,k=d.EditorUtils.registerTool,l=f.extend({init:function(a){this.options=a,f.fn.init.call(this,a)},exec:function(){function v(a){a.firstChild&&e.is(a.firstChild,"br")&&e.remove(a.firstChild),e.isDataNode(a)&&!a.nodeValue&&(a=a.parentNode);if(a&&!e.is(a,"img")){while(a.firstChild&&a.firstChild.nodeType==1)a=a.firstChild;a.innerHTML||(a.innerHTML=l)}}var b=this.getRange(),c=j.documentFromRange(b),d,f,g,k,l=a.browser.msie?"":'<br _moz_dirty="" />',m,n,o,p,q,r="p,h1,h2,h3,h4,h5,h6".split(","),s=e.parentOfType(b.startContainer,r),t=e.parentOfType(b.endContainer,r),u=s&&!t||!s&&t;b.deleteContents(),n=e.create(c,"a"),b.insertNode(n),n.parentNode||(k=b.commonAncestorContainer,k.innerHTML="",k.appendChild(n)),i(n.parentNode),o=e.parentOfType(n,["li"]),p=e.parentOfType(n,"h1,h2,h3,h4,h5,h6".split(",")),o?(q=b.cloneRange(),q.selectNode(o),j.textNodes(q).length||(m=e.create(c,"p"),o.nextSibling&&j.split(q,o.parentNode),e.insertAfter(m,o.parentNode),e.remove(o.parentNode.childNodes.length==1?o.parentNode:o),m.innerHTML=l,g=m)):p&&!n.nextSibling&&(m=e.create(c,"p"),e.insertAfter(m,p),m.innerHTML=l,e.remove(n),g=m),g||(!o&&!p&&(new h([{tags:["p"]}])).apply([n]),b.selectNode(n),d=e.parentOfType(n,[o?"li":p?e.name(p):"p"]),j.split(b,d,u),f=d.previousSibling,e.is(f,"li")&&f.firstChild&&!e.is(f.firstChild,"br")&&(f=f.firstChild),g=d.nextSibling,e.is(g,"li")&&g.firstChild&&!e.is(g.firstChild,"br")&&(g=g.firstChild),e.remove(d),v(f),v(g),i(f)),i(g);if(e.is(g,"img"))b.setStartBefore(g);else{b.selectNodeContents(g);var w=j.textNodes(b)[0];w&&b.selectNodeContents(w)}b.collapse(!0),e.scrollTo(g),j.selectRange(b)}}),m=f.extend({init:function(a){this.options=a,f.fn.init.call(this,a)},exec:function(){var b=this.getRange();b.deleteContents();var c=e.create(j.documentFromRange(b),"br");b.insertNode(c),i(c.parentNode);if(!a.browser.msie&&(!c.nextSibling||e.isWhitespace(c.nextSibling))){var d=c.cloneNode(!0);d.setAttribute("_moz_dirty",""),e.insertAfter(d,c)}b.setStartAfter(c),b.collapse(!0),j.selectRange(b)}});c(d,{ParagraphCommand:l,NewLineCommand:m}),k("insertLineBreak",new g({key:13,shift:!0,command:m})),k("insertParagraph",new g({key:13,command:l}))}(jQuery),function(a){var b=window.kendo,c=b.Class,d=a.extend,e=b.ui.editor,f=e.Dom,g=e.RangeUtils,h=e.EditorUtils,i=e.Command,j=e.ToolTemplate,k=e.FormatTool,l=e.BlockFormatFinder,m=g.textNodes,n=e.EditorUtils.registerTool,o=l.extend({init:function(a){this.tag=a;var b=this.tags=[a=="ul"?"ol":"ul",a];l.fn.init.call(this,[{tags:b}])},isFormatted:function(a){var b=[],c;for(var d=0;d<a.length;d++)(c=this.findFormat(a[d]))&&f.name(c)==this.tag&&b.push(c);if(b.length<1)return!1;if(b.length!=a.length)return!1;for(d=0;d<b.length;d++){if(b[d].parentNode!=c.parentNode)break;if(b[d]!=c)return!1}return!0},findSuitable:function(a){var b=f.parentOfType(a[0],this.tags);if(b&&f.name(b)==this.tag)return b;return null}}),p=c.extend({init:function(a,b){var c=this;c.finder=new o(a),c.tag=a,c.unwrapTag=b},wrap:function(a,b){var c=f.create(a.ownerDocument,"li"),d,e;for(d=0;d<b.length;d++){e=b[d];if(f.is(e,"li")){a.appendChild(e);continue}if(f.is(e,"ul")||f.is(e,"ol")){while(e.firstChild)a.appendChild(e.firstChild);continue}if(f.is(e,"td")){while(e.firstChild)c.appendChild(e.firstChild);a.appendChild(c),e.appendChild(a),a=a.cloneNode(!1),c=c.cloneNode(!1);continue}c.appendChild(e),f.isBlock(e)&&(a.appendChild(c),f.unwrap(e),c=c.cloneNode(!1))}c.firstChild&&a.appendChild(c)},containsAny:function(a,b){for(var c=0;c<b.length;c++)if(f.isAncestorOrSelf(a,b[c]))return!0;return!1},suitable:function(a,b){if(a.className=="k-marker"){var c=a.nextSibling;if(c&&f.isBlock(c))return!1;c=a.previousSibling;if(c&&f.isBlock(c))return!1}return this.containsAny(a,b)||f.isInline(a)||a.nodeType==3},split:function(b){var c=m(b),d,e;if(c.length){d=f.parentOfType(c[0],["li"]),e=f.parentOfType(c[c.length-1],["li"]),b.setStartBefore(d),b.setEndAfter(e);for(var h=0,i=c.length;h<i;h++){var j=this.finder.findFormat(c[h]);if(j){var k=a(j).parents("ul,ol");k[0]?g.split(b,k.last()[0],!0):g.split(b,j,!0)}}}},merge:function(a,b){var c=b.previousSibling,d;while(c&&(c.className=="k-marker"||c.nodeType==3&&f.isWhitespace(c)))c=c.previousSibling;if(c&&f.name(c)==a){while(b.firstChild)c.appendChild(b.firstChild);f.remove(b),b=c}d=b.nextSibling;while(d&&(d.className=="k-marker"||d.nodeType==3&&f.isWhitespace(d)))d=d.nextSibling;if(d&&f.name(d)==a){while(b.lastChild)d.insertBefore(b.lastChild,d.firstChild);f.remove(b)}},applyOnSection:function(b,c){function j(){g.push(this)}var d=this.tag,e;c.length==1?e=f.parentOfType(c[0],["ul","ol"]):e=f.commonAncestor.apply(null,c),e||(e=f.parentOfType(c[0],["td"])||c[0].ownerDocument.body),f.isInline(e)&&(e=f.blockParentOrBody(e));var g=[],h=this.finder.findSuitable(c);h||(h=(new o(d=="ul"?"ol":"ul")).findSuitable(c));var i=f.significantChildNodes(e);i.length||(i=c),/table|tbody/.test(f.name(e))&&(i=a.map(c,function(a){return f.parentOfType(a,["td"])}));for(var k=0;k<i.length;k++){var l=i[k],m=f.name(l);this.suitable(l,c)&&(!h||!f.isAncestorOrSelf(h,l))&&(!h||m!="ul"&&m!="ol"?g.push(l):(a.each(l.childNodes,j),f.remove(l)))}g.length==i.length&&e!=c[0].ownerDocument.body&&!/table|tbody|tr|td/.test(f.name(e))&&(g=[e]),h||(h=f.create(e.ownerDocument,d),f.insertBefore(h,g[0])),this.wrap(h,g),f.is(h,d)||f.changeTag(h,d),this.merge(d,h)},apply:function(a){var b=0,c=[],d,e,g;do g=f.parentOfType(a[b],["td","body"]),!d||g!=d?(d&&c.push({section:d,nodes:e}),e=[a[b]],d=g):e.push(a[b]),b++;while(b<a.length);c.push({section:d,nodes:e});for(b=0;b<c.length;b++)this.applyOnSection(c[b].section,c[b].nodes)},unwrap:function(b){var c=b.ownerDocument.createDocumentFragment(),d=this.unwrapTag,e,g,h,i;for(g=b.firstChild;g;g=g.nextSibling){h=f.create(b.ownerDocument,d||"p");while(g.firstChild)i=g.firstChild,f.isBlock(i)?(h.firstChild&&(c.appendChild(h),h=f.create(b.ownerDocument,d||"p")),c.appendChild(i)):h.appendChild(i);h.firstChild&&c.appendChild(h)}e=a(b).parents("ul,ol"),e[0]?(f.insertAfter(c,e.last()[0]),e.last().remove()):f.insertAfter(c,b),f.remove(b)},remove:function(a){var b;for(var c=0,d=a.length;c<d;c++)b=this.finder.findFormat(a[c]),b&&this.unwrap(b)},toggle:function(a){var b=this,c=m(a),d=a.commonAncestorContainer;if(!c.length){a.selectNodeContents(d),c=m(a);if(!c.length){var e=d.ownerDocument.createTextNode("");a.startContainer.appendChild(e),c=[e],a.selectNode(e.parentNode)}}b.finder.isFormatted(c)?(b.split(a),b.remove(c)):b.apply(c)}}),q=i.extend({init:function(a){a.formatter=new p(a.tag),i.fn.init.call(this,a)}}),r=k.extend({init:function(a){this.options=a,k.fn.init.call(this,d(a,{finder:new o(a.tag)}))},command:function(a){return new q(d(a,{tag:this.options.tag}))}});d(e,{ListFormatFinder:o,ListFormatter:p,ListCommand:q,ListTool:r}),n("insertUnorderedList",new r({tag:"ul",template:new j({template:h.buttonTemplate,title:"Remove Link"})})),n("insertOrderedList",new r({tag:"ol",template:new j({template:h.buttonTemplate,title:"Remove Link"})}))}(jQuery),function(a,b){var c=window.kendo,d=c.Class,e=a.extend,f=c.ui.editor,g=f.Dom,h=f.RangeUtils,i=f.EditorUtils,j=f.Command,k=f.Tool,l=f.ToolTemplate,m=f.InlineFormatter,n=f.InlineFormatFinder,o=h.textNodes,p=f.EditorUtils.registerTool,q=d.extend({findSuitable:function(a){return g.parentOfType(a,["a"])}}),r=d.extend({init:function(){this.finder=new q},apply:function(a,b){var c=o(a);if(b.innerHTML){var d=h.getMarkers(a),e=h.documentFromRange(a);a.deleteContents();var f=g.create(e,"a",b);a.insertNode(f),d.length>1&&(g.insertAfter(d[d.length-1],f),g.insertAfter(d[1],f),g[c.length>0?"insertBefore":"insertAfter"](d[0],f))}else{var i=new m([{tags:["a"]}],b);i.finder=this.finder,i.apply(c)}}}),s=j.extend({init:function(a){a.formatter={toggle:function(a){(new m([{tags:["a"]}])).remove(o(a))}},this.options=a,j.fn.init.call(this,a)}}),t=j.extend({init:function(a){var b=this;b.options=a,j.fn.init.call(b,a),b.attributes=null,b.async=!0,b.formatter=new r},exec:function(){function j(a){a.preventDefault(),n.destroy(),g.windowFromDocument(h.documentFromRange(b)).focus(),f.releaseRange(b)}function i(c){var d=a("#k-editor-link-url",n.element).val();if(d&&d!="http://"){f.attributes={href:d};var g=a("#k-editor-link-title",n.element).val();g&&(f.attributes.title=g);var h=a("#k-editor-link-text",n.element).val();h!==e&&(f.attributes.innerHTML=h||d);var i=a("#k-editor-link-target",n.element).is(":checked");i&&(f.attributes.target="_blank"),f.formatter.apply(b,f.attributes)}j(c),f.change&&f.change()}var b=this.getRange(),c=b.collapsed;b=this.lockRange(!0);var d=o(b),e=null,f=this,k=d.length?f.formatter.finder.findSuitable(d[0]):null,l=d.length<=1||d.length==2&&c,m='<div class="k-editor-dialog"><ol><li class="k-form-text-row"><label for="k-editor-link-url">Web address</label><input type="text" class="k-input" id="k-editor-link-url"/></li>'+(l?'<li class="k-form-text-row"><label for="k-editor-link-text">Text</label><input type="text" class="k-input" id="k-editor-link-text"/></li>':"")+'<li class="k-form-text-row"><label for="k-editor-link-title">Tooltip</label><input type="text" class="k-input" id="k-editor-link-title"/></li>'+'<li class="k-form-checkbox-row"><input type="checkbox" id="k-editor-link-target"/><label for="k-editor-link-target">Open link in new window</label></li>'+"</ol>"+'<div class="k-button-wrapper">'+'<button class="k-dialog-insert k-button">Insert</button>'+"&nbsp;or&nbsp;"+'<a href="#" class="k-dialog-close k-link">Close</a>'+"</div>"+"</div>",n=a(m).appendTo(document.body).kendoWindow(a.extend({},this.editor.options.dialogOptions,{title:"Insert link",close:j})).hide().find(".k-dialog-insert").click(i).end().find(".k-dialog-close").click(j).end().find(".k-form-text-row input").keydown(function(a){a.keyCode==13?i(a):a.keyCode==27&&j(a)}).end().find("#k-editor-link-url").val(k?k.getAttribute("href",2):"http://").end().find("#k-editor-link-text").val(d.length>0?d.length==1?d[0].nodeValue:d[0].nodeValue+d[1].nodeValue:"").end().find("#k-editor-link-title").val(k?k.title:"").end().find("#k-editor-link-target").attr("checked",k?k.target=="_blank":!1).end().show().data("kendoWindow").center();l&&d.length>0&&(e=a("#k-editor-link-text",n.element).val()),a("#k-editor-link-url",n.element).focus().select()},redo:function(){var a=this,b=a.lockRange(!0);a.formatter.apply(b,a.attributes),a.releaseRange(b)}}),u=k.extend({init:function(b){this.options=b,this.finder=new n([{tags:["a"]}]),k.fn.init.call(this,a.extend(b,{command:s}))},initialize:function(a){a.attr("unselectable","on").addClass("k-state-disabled")},update:function(a,b){a.toggleClass("k-state-disabled",!this.finder.isFormatted(b)).removeClass("k-state-hover")}});e(c.ui.editor,{LinkFormatFinder:q,LinkFormatter:r,UnlinkCommand:s,LinkCommand:t,UnlinkTool:u}),p("createLink",new k({key:"K",ctrl:!0,command:t,template:new l({template:i.buttonTemplate,title:"Create Link"})})),p("unlink",new u({key:"K",ctrl:!0,shift:!0,template:new l({template:i.buttonTemplate,title:"Remove Link"})}))}(jQuery),function(a,b){var c=window.kendo,d=a.extend,e=c.ui.editor,f=e.EditorUtils,g=e.Dom,h=f.registerTool,i=e.ToolTemplate,j=e.RangeUtils,k=e.Command,l=c.keys,m="Insert Image",n="#k-editor-image-url",o="#k-editor-image-title",p=k.extend({init:function(a){var b=this;k.fn.init.call(b,a),b.async=!0,b.attributes={}},insertImage:function(a,b){var c=this.attributes;if(c.src&&c.src!="http://"){if(!a){a=g.create(j.documentFromRange(b),"img",c),a.onload=a.onerror=function(){a.removeAttribute("complete"),a.removeAttribute("width"),a.removeAttribute("height")},b.deleteContents(),b.insertNode(a),b.setStartAfter(a),b.setEndAfter(a),j.selectRange(b);return!0}g.attr(a,c)}return!1},redo:function(){var a=this,b=a.lockRange();a.insertImage(j.image(b),b)||a.releaseRange(b)},exec:function(){function q(a){a.keyCode==l.ENTER?k(a):a.keyCode==l.ESC&&p(a)}function p(a){a.preventDefault(),i.destroy(),g.windowFromDocument(j.documentFromRange(c)).focus(),e||b.releaseRange(c)}function k(d){b.attributes={src:a(n,i.element).val(),alt:a(o,i.element).val()},e=b.insertImage(f,c),p(d),b.change&&b.change()}var b=this,c=b.lockRange(),e=!1,f=j.image(c),h,i;h='<div class="k-editor-dialog"><ol><li class="k-form-text-row"><label for="k-editor-image-url">Web address</label><input type="text" class="k-input" id="k-editor-image-url"/></li><li class="k-form-text-row"><label for="k-editor-image-title">Tooltip</label><input type="text" class="k-input" id="k-editor-image-title"/></li></ol><div class="k-button-wrapper"><button class="k-dialog-insert k-button">Insert</button>&nbsp;or&nbsp;<a href="#" class="k-dialog-close k-link">Close</a></div></div>',i=a(h).appendTo(document.body).kendoWindow(d({},b.editor.options.dialogOptions,{title:m,close:p,activate:function(){}})).hide().find(".k-dialog-insert").click(k).end().find(".k-dialog-close").click(p).end().find(".k-form-text-row input").keydown(q).end().find(n).val(f?f.getAttribute("src",2):"http://").end().find(o).val(f?f.alt:"").end().show().data("kendoWindow").center(),a(n,i.element).focus().select()}});c.ui.editor.ImageCommand=p,h("insertImage",new e.Tool({command:p,template:new i({template:f.buttonTemplate,title:m})}))}(jQuery),function(a,b){var c=window.kendo,d=c.ui.Widget,e=c.ui.DropDownList,f=c.ui.editor,g=f.Dom,h="change",i="k-state-selected",j="."+i,k=".k-selected-color",l="unselectable",m="background-color",n=c.keys,o=c.template('<div class="k-colorpicker-popup"><ul class="k-reset"># for(var i = 0; i < colors.length; i++) { #<li class="k-item #= colors[i] == value ? "k-state-selected" : "" #"><div style="background-color:\\##= colors[i] #"></div></li># } #</ul></div>'),p=d.extend({init:function(b,c){var e=this;d.fn.init.call(e,b,c),b=e.element,c=e.options,e._value=c.value,e.popup=a(o({colors:c.colors,value:c.value.substring(1)})).kendoPopup({anchor:b,toggleTarget:b.find(".k-icon")}).delegate(".k-item","click",function(b){e.select(a(b.currentTarget).find("div").css(m))}).find("*").attr(l,"on").end().data("kendoPopup"),b.attr("tabIndex",0).keydown(function(a){e.keydown(a)}).focus(function(){b.css("outline","1px dotted #000")}).blur(function(){b.css("outline","")}).delegate(".k-tool-icon","click",function(){e.select()}).find("*").attr(l,"on"),e._value&&b.find(k).css(m,e._value)},options:{name:"ColorPicker",colors:"000000,7f7f7f,880015,ed1c24,ff7f27,fff200,22b14c,00a2e8,3f48cc,a349a4,ffffff,c3c3c3,b97a57,ffaec9,ffc90e,efe4b0,b5e61d,99d9ea,7092be,c8bfe7".split(","),value:null},events:[h],select:function(a){var b=this;a?(a=g.toHex(a),b.trigger(h,{value:a})||(b.value(a),b.close())):b.trigger(h,{value:b._value})},open:function(){this.popup.open()},close:function(){this.popup.close()},toggle:function(){this.popup.toggle()},keydown:function(a){var b=this,c=b.popup.element,d=b.popup.visible(),e,f,g,h=!1,k=a.keyCode;k==n.DOWN?(d?(e=c.find(j),e[0]?f=e.next():f=c.find("li:first"),f[0]&&(e.removeClass(i),f.addClass(i))):b.open(),h=!0):k==n.UP?(d&&(e=c.find(j),g=e.prev(),g[0]&&(e.removeClass(i),g.addClass(i))),h=!0):k==n.TAB||k==n.RIGHT||k==n.LEFT?b.close():k==n.ENTER&&(c.find(j).click(),h=!0),h&&a.preventDefault()},value:function(a){var c=this;if(a===b)return c._value;a=g.toHex(a),c._value=a,c.element.find(k).css(m,a)}}),q=e.extend({init:function(a,b){var c=this;e.fn.init.call(c,a,b),c.value(c.options.title)},options:{name:"SelectBox"},value:function(a){var c=this,d=e.fn.value.call(c,a);if(a===b)return d;a!==e.fn.value.call(c)&&(c.text(c.options.title),c._current.removeClass("k-state-selected"),c.current(null),c._oldIndex=c.selectedIndex=-1)}});c.ui.editor.ColorPicker=p,c.ui.editor.SelectBox=q}(jQuery),function(a,b){function q(a,c){var d=g.name(a)!="td"?"marginLeft":"paddingLeft";if(c===b)return a.style[d]||0;c>0?a.style[d]=c+"px":(a.style[d]="",a.style.cssText||a.removeAttribute("style"))}var c=window.kendo,d=c.Class,e=a.extend,f=c.ui.editor,g=f.Dom,h=f.EditorUtils,i=h.registerTool,j=f.Command,k=f.Tool,l=f.ToolTemplate,m=f.RangeUtils,n=g.blockElements,o=f.BlockFormatFinder,p=f.BlockFormatter,r=d.extend({init:function(){this.finder=new o([{tags:g.blockElements}])},apply:function(b){var c=this.finder.findSuitable(b),d=[],e,f,h,i,j;if(c.length){for(e=0,f=c.length;e<f;e++)g.is(c[e],"li")?a(c[e]).index()?a.inArray(c[e].parentNode,d)<0&&d.push(c[e]):d.push(c[e].parentNode):d.push(c[e]);while(d.length){h=d.shift();if(g.is(h,"li")){i=h.parentNode,j=a(h).prev("li");var k=j.find("ul,ol").last(),l=a(h).children("ul,ol")[0];if(l&&j[0])k[0]?(k.append(h),k.append(a(l).children()),g.remove(l)):(j.append(l),l.insertBefore(h,l.firstChild));else{l=j.children("ul,ol")[0],l||(l=g.create(h.ownerDocument,g.name(i)),j.append(l));while(h&&h.parentNode==i)l.appendChild(h),h=d.shift()}}else{var m=parseInt(q(h),10)+30;q(h,m);for(var n=0;n<d.length;n++)a.contains(h,d[n])&&d.splice(n,1)}}}else{var o=new p([{tags:"p"}],{style:{marginLeft:30}});o.apply(b)}},remove:function(b){var c=this.finder.findSuitable(b),d,e,f,g,h,i,j,k;for(e=0,f=c.length;e<f;e++){j=a(c[e]);if(j.is("li")){g=j.parent(),h=g.parent();if(h.is("li,ul,ol")&&!q(g[0])){if(d&&a.contains(d,h[0]))continue;i=j.nextAll("li"),i.length&&a(g[0].cloneNode(!1)).appendTo(j).append(i),h.is("li")?j.insertAfter(h):j.appendTo(h),g.children("li").length||g.remove();continue}if(d==g[0])continue;d=g[0]}else d=c[e];k=parseInt(q(d),10)-30,q(d,k)}}}),s=j.extend({init:function(a){a.formatter={toggle:function(a){(new r).apply(m.nodes(a))}},j.fn.init.call(this,a)}}),t=j.extend({init:function(a){a.formatter={toggle:function(a){(new r).remove(m.nodes(a))}},j.fn.init.call(this,a)}}),u=k.extend({init:function(a){k.fn.init.call(this,e(a,{command:t})),this.finder=new o([{tags:n}])},initialize:function(a){a.attr("unselectable","on").addClass("k-state-disabled")},update:function(b,c){var d=this.finder.findSuitable(c),e,f,h,i;for(h=0,i=d.length;h<i;h++){e=q(d[h]),e||(f=a(d[h]).parents("ul,ol").length,e=g.is(d[h],"li")&&(f>1||q(d[h].parentNode))||g.ofType(d[h],["ul","ol"])&&f>0);if(e){b.removeClass("k-state-disabled");return}}b.addClass("k-state-disabled").removeClass("k-state-hover")}});e(f,{IndentFormatter:r,IndentCommand:s,OutdentCommand:t,OutdentTool:u}),i("indent",new k({command:s,template:new l({template:h.buttonTemplate,title:"Indent"})})),i("outdent",new u({template:new l({template:h.buttonTemplate,title:"Outdent"})}))}(jQuery),function(a,b){var c=window.kendo,d=a.extend,e=c.ui.editor,f=e.Dom,g=e.RangeUtils,h=e.EditorUtils,i=e.Command,j=e.Tool,k=e.ToolTemplate,l=i.extend({init:function(a){var b=this;b.options=a,i.fn.init.call(b,a),b.attributes=null,b.async=!0},exec:function(){function m(a){a.preventDefault(),h.data("kendoWindow").destroy(),f.windowFromDocument(g.documentFromRange(e)).focus()}function k(a){c.value(h.find(j).val()),m(a),b.change&&b.change()}var b=this,c=b.editor,e=c.getRange(),h=a(l.template).appendTo(document.body),i=l.indent(c.value()),j=".k-editor-textarea";h.kendoWindow(d({},c.options.dialogOptions,{title:"View HTML",close:m})).hide().find(j).val(i).end().find(".k-dialog-update").click(k).end().find(".k-dialog-close").click(m).end().show().data("kendoWindow").center(),h.find(j).focus()}});d(l,{template:"<div class='k-editor-dialog'><textarea class='k-editor-textarea k-input'></textarea><div class='k-button-wrapper'><button class='k-dialog-update k-button'>Update</button>&nbsp;or&nbsp;<a href='#' class='k-dialog-close k-link'>Close</a></div></div>",indent:function(a){return a.replace(/<\/(p|li|ul|ol|h[1-6]|table|tr|td|th)>/ig,"</$1>\n").replace(/<(ul|ol)([^>]*)><li/ig,"<$1$2>\n<li").replace(/<br \/>/ig,"<br />\n").replace(/\n$/,"")}}),c.ui.editor.ViewHtmlCommand=l,e.EditorUtils.registerTool("viewHtml",new j({command:l,template:new k({template:h.buttonTemplate,title:"View HTML"})}))}(jQuery),function(a){var b=window.kendo,c=b.Class,d=a.extend,e=b.ui.editor,f=e.RangeUtils,g=e.Marker,h=c.extend({init:function(a){this.editor=a,this.formats=[]},apply:function(a){if(!!this.hasPending()){var b=new g;b.addCaret(a);var c=a.startContainer.childNodes[a.startOffset],e=c.previousSibling;e.nodeValue||(e=e.previousSibling),a.setStart(e,e.nodeValue.length-1),b.add(a);if(!f.textNodes(a).length){b.remove(a),a.collapse(!0),this.editor.selectRange(a);return}var h=b.end.previousSibling.previousSibling,i,j=this.formats;for(var k=0;k<j.length;k++){i=j[k];var l=i.command(d({range:a},i.options.params));l.editor=this.editor,l.exec(),a.selectNode(h)}b.remove(a),h.parentNode&&(a.setStart(h,1),a.collapse(!0)),this.clear(),this.editor.selectRange(a)}},hasPending:function(){return this.formats.length>0},isPending:function(a){return!!this.getPending(a)},getPending:function(a){var b=this.formats;for(var c=0;c<b.length;c++)if(b[c].name==a)return b[c];return},toggle:function(a){var b=this.formats;for(var c=0;c<b.length;c++)if(b[c].name==a.name){b[c].params&&b[c].params.value!=a.params.value?b[c].params.value=a.params.value:b.splice(c,1);return}b.push(a)},clear:function(){this.formats=[]}});d(e,{PendingFormats:h})}(jQuery),function(a,b){function E(c,d){var e,f=d!==b;if(document.selection){a(c).is(":visible")&&c.focus(),e=document.selection.createRange();if(f)e.move("character",d),e.select();else{var g=c.createTextRange(),h=g.duplicate();g.moveToBookmark(e.getBookmark()),h.setEndPoint("EndToStart",g),d=h.text.length}}else c.selectionStart!==b&&(f?(c.focus(),c.setSelectionRange(d,d)):d=c.selectionStart);return d}function D(a,b){return'<span unselectable="on" class="k-link"><span unselectable="on" class="k-icon k-i-arrow-'+a+'" title="'+b+'">'+b+"</span></span>"}var c=window.kendo,d=c.keys,e=c.ui,f=e.Widget,g=c.parseFloat,h=c.support.placeholder,i=c.support.touch,j=c.getCulture,k="change",l="disabled",m="k-input",n="spin",o="touchend",p=i?"touchstart":"mousedown",q=i?"touchmove "+o:"mouseup mouseleave",r="k-state-default",s="k-state-focused",t="k-state-hover",u="mouseenter mouseleave",v=".",w="k-state-selected",x="k-state-disabled",y=i?"number":"text",z=null,A=a.proxy,B={190:".",188:","},C=f.extend({init:function(a,d){var e=this,g=d&&d.step!==b,h,i,j,k,l;f.fn.init.call(e,a,d),d=e.options,a=e.element.addClass(m).bind({keydown:A(e._keydown,e),paste:A(e._paste,e),blur:A(e._focusout,e)}),a.closest("form").bind("reset",function(){setTimeout(function(){e.value(a[0].value)})}),d.placeholder=d.placeholder||a.attr("placeholder"),e._wrapper(),e._arrows(),e._input(),e._text.focus(A(e._click,e)),h=e.min(a.attr("min")),i=e.max(a.attr("max")),j=e._parse(a.attr("step")),d.min===z&&h!==z&&(d.min=h),d.max===z&&i!==z&&(d.max=i),!g&&j!==z&&(d.step=j),l=d.format,l.slice(0,3)==="{0:"&&(d.format=l.slice(3,l.length-1)),k=d.value,e.value(k!==z?k:a.val()),e.enable(!a.is("[disabled]")),c.notify(e)},options:{name:"NumericTextBox",decimals:z,min:z,max:z,value:z,step:1,culture:"",format:"n",spinners:!0,placeholder:"",upArrowText:"Increase value",downArrowText:"Decrease value"},events:[k,n],enable:function(a){var b=this,c=b._text.add(b.element),d=b._inputWrapper.unbind(u),e=b._upArrow.unbind(p),f=b._downArrow.unbind(p);b._toggleText(!0),a===!1?(d.removeClass(r).addClass(x),c.attr(l,l)):(d.addClass(r).removeClass(x).bind(u,b._toggleHover),c.removeAttr(l),e.bind(p,function(a){a.preventDefault(),b._spin(1),b._upArrow.addClass(w)}),f.bind(p,function(a){a.preventDefault(),b._spin(-1),b._downArrow.addClass(w)}))},min:function(a){return this._option("min",a)},max:function(a){return this._option("max",a)},step:function(a){return this._option("step",a)},value:function(a){var c=this,d;if(a===b)return c._value;a=c._parse(a),d=c._adjust(a);a===d&&(c._update(a),c._old=c._value)},_adjust:function(a){var b=this,c=b.options,d=c.min,e=c.max;if(a===z)return a;d!==z&&a<d?a=d:e!==z&&a>e&&(a=e);return a},_arrows:function(){var b=this,d,e=b.options,f=e.spinners,g=b.element;d=g.siblings(".k-icon"),d[0]||(d=a(D("n",e.upArrowText)+D("s",e.downArrowText)).insertAfter(g),d.wrapAll('<span class="k-select"/>')),d.bind(q,function(a){(!i||c.eventTarget(a)!=a.currentTarget||a.type===o)&&clearTimeout(b._spinning),d.removeClass(w)}),f||(d.toggle(f),b._inputWrapper.addClass("k-expand-padding")),b._upArrow=d.eq(0),b._downArrow=d.eq(1)},_blur:function(){var a=this;a._toggleText(!0),a._change(a.element.val())},_click:function(a){var b=this;clearTimeout(b._focusing),b._focusing=setTimeout(function(){var c=a.target,d=E(c),e=c.value.substring(0,d),f=b._format(b.options.format),g=f[","],h=new RegExp("\\"+g,"g"),i=new RegExp("([\\d\\"+g+"]+)(\\"+f[v]+")?(\\d+)?"),j=i.exec(e),k=0;j&&(k=j[0].replace(h,"").length,e.indexOf("(")!=-1&&b._value<0&&k++),b._focusin(),E(b.element[0],k)})},_change:function(a){var b=this;b._update(a),a=b._value,b._old!=a&&(b._old=a,b.trigger(k),b.element.trigger(k))},_culture:function(a){return a||j(this.options.culture)},_focusin:function(){var a=this;a._toggleText(!1),a.element.focus(),a._inputWrapper.addClass(s)},_focusout:function(){var a=this;clearTimeout(a._focusing),a._inputWrapper.removeClass(s),a._blur()},_format:function(a,b){var c=this._culture(b).numberFormat;a=a.toLowerCase(),a.indexOf("c")>-1?c=c.currency:a.indexOf("p")>-1&&(c=c.percent);return c},_input:function(){var b=this,c="k-formatted-value",d=b.element.show()[0],e=b.wrapper,f;f=e.find(v+c),f[0]||(f=a("<input />").insertBefore(d).addClass(c)),d.type=y,f[0].type="text",f[0].style.cssText=d.style.cssText,f.attr("placeholder",b.options.placeholder),b._text=f.attr("readonly",!0).addClass(d.className)},_keydown:function(a){var b=this,c=a.keyCode;c==d.DOWN?b._step(-1):c==d.UP?b._step(1):c==d.ENTER&&b._change(b.element.val()),b._prevent(c)&&!a.ctrlKey&&a.preventDefault()},_paste:function(a){var b=this,c=a.target,d=c.value;setTimeout(function(){b._parse(c.value)===z&&b._update(d)})},_prevent:function(a){var b=this,c=b.element[0],e=c.value,f=b.options,g=f.min,h=b._format(f.format),i=h[v],j=f.decimals,k=E(c),l=!0,m;j===z&&(j=h.decimals),a>16&&a<21||a>32&&a<37||a>47&&a<58||a>95&&a<106||a==d.INSERT||a==d.DELETE||a==d.LEFT||a==d.RIGHT||a==d.TAB||a==d.BACKSPACE||a==d.ENTER?l=!1:B[a]===i&&j>0&&e.indexOf(i)==-1?l=!1:!(g===z||g<0)||e.indexOf("-")!=-1||a!=189&&a!=109||k!==0?a==110&&j>0&&e.indexOf(i)==-1&&(m=e.substring(k),c.value=e.substring(0,k)+i+m):l=!1;return l},_option:function(a,c){var d=this,e=d.options;if(c===b)return e[a];c=d._parse(c);if(!!c||a!=="step")e[a]=d._parse(c)},_spin:function(a,b){var c=this;b=b||500,clearTimeout(c._spinning),c._spinning=setTimeout(function(){c._spin(a,50)},b),c._step(a)},_step:function(a){var b=this,c=b.element,d=b._parse(c.val())||0;document.activeElement!=c[0]&&b._focusin(),d+=b.options.step*a,b._update(b._adjust(d)),b.trigger(n)},_toggleHover:function(b){i||a(b.currentTarget).toggleClass(t,b.type==="mouseenter")},_toggleText:function(a){var b=this;a=!!a,b._text.toggle(a),b.element.toggle(!a)},_parse:function(a,b){return g(a,this._culture(b),this.options.format)},_update:function(a){var b=this,d=b.options,e=d.format,f=d.decimals,g=b._culture(),h=b._format(e,g),i;f===z&&(f=h.decimals),a=b._parse(a,g),i=a!==z,i&&(a=parseFloat(a.toFixed(f))),b._value=a=b._adjust(a),b._placeholder(c.toString(a,e,g)),b.element.val(i?a.toString().replace(v,h[v]):"")},_placeholder:function(a){this._text.val(a),!h&&!a&&this._text.val(this.options.placeholder)},_wrapper:function(){var b=this,c=b.element,d;d=c.parents(".k-numerictextbox"),d.is("span.k-numerictextbox")||(d=c.hide().wrap('<span class="k-numeric-wrap k-state-default" />').parent(),d=d.wrap("<span/>").parent()),d[0].style.cssText=c[0].style.cssText,c[0].style.width="",b.wrapper=d.addClass("k-widget k-numerictextbox").show(),b._inputWrapper=a(d[0].firstChild)}});e.plugin(C)}(jQuery),function(a,b){function P(b){b=a(b),b.filter(".k-first:not(:first-child)").removeClass(t),b.filter(".k-last:not(:last-child)").removeClass(p),b.filter(":first-child").addClass(t),b.filter(":last-child").addClass(p)}function O(b){b=a(b),b.find(".k-icon").remove(),b.filter(":has(.k-group)").children(".k-link:not(:has([class*=k-i-arrow]))").each(function(){var b=a(this),c=b.parent().parent();b.append("<span class='k-icon "+(c.hasClass(n+"-horizontal")?"k-i-arrow-s":"k-i-arrow-e")+"'/>")})}function N(b){b=a(b),b.addClass("k-item").children(l).addClass(u),b.children("a").addClass(o).children(l).addClass(u),b.filter(":not([disabled])").addClass(A),b.filter(".k-separator:empty").append("&nbsp;"),b.filter("li[disabled]").addClass(B).removeAttr("disabled"),b.children("a").filter(":focus").parent().addClass("k-state-active"),b.children("."+o).length||b.contents().filter(function(){return!this.nodeName.match(k)&&(this.nodeType!=3||!!a.trim(this.nodeValue))}).wrapAll("<span class='"+o+"'/>"),O(b),P(b)}function M(b,c){try{return a.contains(b,c)}catch(d){return!1}}function L(a,b){a=a.split(" ")[!b+0]||a;var d={origin:["bottom","left"],position:["top","left"]},e=/left|right/.test(a);e?(d.origin=["top",a],d.position[1]=c.directions[a].reverse):(d.origin[0]=a,d.position[0]=c.directions[a].reverse),d.origin=d.origin.join(" "),d.position=d.position.join(" ");return d}function K(a,b){a=a.split(" ")[!b+0]||a;return a.replace("top","up").replace("bottom","down")}var c=window.kendo,d=c.ui,e=c.support.touch,f=a.extend,g=a.proxy,h=a.each,i=c.template,j=d.Widget,k=/^(ul|a|div)$/i,l="img",m="open",n="k-menu",o="k-link",p="k-last",q="close",r=e?"touchend":"click",s="timer",t="k-first",u="k-image",v="select",w="zIndex",x="mouseenter",y="mouseleave",z="kendoPopup",A="k-state-default",B="k-state-disabled",C=".k-group",D=":not(.k-list) > .k-item",E=".k-item.k-state-disabled",F=".k-item:not(.k-state-disabled)",G=".k-item:not(.k-state-disabled) > .k-link",H="div:not(.k-animation-container,.k-list-container)",I={content:i("<div class='k-content k-group'>#= content(item) #</div>"),group:i("<ul class='#= groupCssClass(group) #'#= groupAttributes(group) #>#= renderItems(data) #</ul>"),itemWrapper:i("<#= tag(item) # class='#= textClass(item) #'#= textAttributes(item) #>#= image(item) ##= sprite(item) ##= text(item) ##= arrow(data) #</#= tag(item) #>"),item:i("<li class='#= wrapperCssClass(group, item) #'>#= itemWrapper(data) ## if (item.items) { ##= subGroup({ items: item.items, menu: menu, group: { expanded: item.expanded } }) ## } #</li>"),image:i("<img class='k-image' alt='' src='#= imageUrl #' />"),arrow:i("<span class='#= arrowClass(item, group) #'></span>"),sprite:i("<span class='k-sprite #= spriteCssClass #'></span>"),empty:i("")},J={wrapperCssClass:function(a,b){var c="k-item",d=b.index;b.enabled===!1?c+=" k-state-disabled":c+=" k-state-default",a.firstLevel&&d===0&&(c+=" k-first"),d==a.length-1&&(c+=" k-last");return c},textClass:function(a){return o},textAttributes:function(a){return a.url?" href='"+a.url+"'":""},arrowClass:function(a,b){var c="k-icon";b.horizontal?c+=" k-i-arrow-s":c+=" k-i-arrow-e";return c},text:function(a){return a.encoded===!1?a.text:c.htmlEncode(a.text)},tag:function(a){return a.url?"a":"span"},groupAttributes:function(a){return a.expanded!==!0?" style='display:none'":""},groupCssClass:function(a){return"k-group"},content:function(a){return a.content?a.content:"&nbsp;"}},Q=j.extend({init:function(b,d){var f=this;j.fn.init.call(f,b,d),b=f.wrapper=f.element,d=f.options,d.dataSource&&(f.element.empty(),f.append(d.dataSource,b)),f._updateClasses(),d.animation===!1&&(d.animation={open:{show:!0,effects:{}},close:{hide:!0,effects:{}}}),f.nextItemZIndex=100,b.delegate(E,r,!1).delegate(F,r,g(f._click,f)),e?(d.openOnClick=!0,b.delegate(G,"touchstart touchend",f._toggleHover)):b.delegate(F,x,g(f._mouseenter,f)).delegate(F,y,g(f._mouseleave,f)).delegate(G,x+" "+y,f._toggleHover),d.openOnClick&&(f.clicked=!1,a(document).click(g(f._documentClick,f))),c.notify(f)},events:[m,q,v],options:{name:"Menu",animation:{open:{duration:200,show:!0},close:{duration:100}},orientation:"horizontal",direction:"default",openOnClick:!1,closeOnClick:!0,hoverDelay:100},enable:function(a,b){this._toggleDisabled(a,b!==!1);return this},disable:function(a){this._toggleDisabled(a,!1);return this},append:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.length?c.find("> .k-group, .k-animation-container > .k-group"):null);h(d.items,function(b){d.group.append(this);var c=d.contents[b];c&&a(this).append(c),O(this)}),O(c),P(d.group.find(".k-first, .k-last").add(d.items));return this},insertBefore:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.parent());h(d.items,function(b){c.before(this);var e=d.contents[b];e&&a(this).append(e),O(this),P(this)}),P(c);return this},insertAfter:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.parent());h(d.items,function(b){c.after(this);var e=d.contents[b];e&&a(this).append(e),O(this),P(this)}),P(c);return this},_insert:function(b,c,d){var e=this,g,h,i=[];if(!c||!c.length)d=e.element;var j=a.isPlainObject(b),k={firstLevel:d.hasClass(n),horizontal:d.hasClass(n+"-horizontal"),expanded:!0,length:d.children().length};c&&!d.length&&(d=a(Q.renderGroup({group:k})).appendTo(c)),j||a.isArray(b)?(g=a.map(j?[b]:b,function(b,c){return typeof b=="string"?a(b):a(Q.renderItem({group:k,item:f(b,{index:c})}))}),i=a.map(j?[b]:b,function(b,c){return b.content||b.contentUrl?a(Q.renderContent({item:f(b,{index:c})})):!1})):(g=a(b),h=g.find("> ul").addClass("k-group"),g=g.filter("li"),g.add(h.find("> li")).each(function(){N(this)}));return{items:g,group:d,contents:i}},remove:function(a){a=this.element.find(a);var b=this,c=a.parentsUntil(b.element,D),d=a.parent("ul");a.remove();if(d&&!d.children(D).length){var e=d.parent(".k-animation-container");e.length?e.remove():d.remove()}c.length&&(c=c.eq(0),O(c),P(c));return b},open:function(c){var d=this,e=d.options,g=e.orientation=="horizontal",h=e.direction;c=d.element.find(c),/^(top|bottom|default)$/.test(h)&&(h=g?(h+" right").replace("default","bottom"):"right"),c.siblings().find(">.k-popup:visible,>.k-animation-container>.k-popup:visible").each(function(){var b=a(this).data("kendoPopup");b&&b.close()}),c.each(function(){var c=a(this);clearTimeout(c.data(s)),c.data(s,setTimeout(function(){var a=c.find(".k-group:first:hidden"),i;if(a[0]&&d.trigger(m,{item:c[0]})===!1){c.data(w,c.css(w)),c.css(w,d.nextItemZIndex++),i=a.data(z);var j=c.parent().hasClass(n),k=j&&g,l=L(h,j),o=e.animation.open.effects,p=o!==b?o:"slideIn:"+K(h,j);i?(i=a.data(z),i.options.origin=l.origin,i.options.position=l.position,i.options.animation.open.effects=p):i=a.kendoPopup({origin:l.origin,position:l.position,collision:e.popupCollision!==b?e.popupCollision:k?"fit":"fit flip",anchor:c,appendTo:c,animation:{open:f(!0,{effects:p},e.animation.open),close:e.animation.close},close:function(a){var b=a.sender.wrapper.parent();d.trigger(q,{item:b[0]})===!1?(b.css(w,b.data(w)),b.removeData(w)):a.preventDefault()}}).data(z),i.open()}},d.options.hoverDelay))});return d},close:function(b){var c=this;b=c.element.find(b),b[0]||(b=c.element.find(">.k-item")),b.each(function(){var b=a(this);clearTimeout(b.data(s)),b.data(s,setTimeout(function(){var a=b.find(".k-group:first:visible"),c;a[0]&&(c=a.data(z),c.close())},c.options.hoverDelay))});return c},_toggleDisabled:function(b,c){b=this.element.find(b),b.each(function(){a(this).toggleClass(A,c).toggleClass(B,!c)})},_toggleHover:function(b){var d=a(c.eventTarget(b)).closest(D);d.parents("li."+B).length||d.toggleClass("k-state-hover",b.type==x||b.type=="touchstart")},_updateClasses:function(){var a=this;a.element.addClass("k-widget k-reset k-header "+n).addClass(n+"-"+a.options.orientation);var b=a.element.find("li > ul").addClass("k-group").end().find("> li,.k-group > li");b.each(function(){N(this)})},_mouseenter:function(b){var c=this,d=a(b.currentTarget),e=d.children(".k-animation-container").length||d.children(C).length;b.delegateTarget==d.parents(".k-menu")[0]&&((!c.options.openOnClick||c.clicked)&&!M(b.currentTarget,b.relatedTarget)&&e&&c.open(d),c.options.openOnClick&&c.clicked&&d.siblings().each(g(function(a,b){c.close(b)},c)))},_mouseleave:function(b){var c=this,d=a(b.currentTarget),e=d.children(".k-animation-container").length||d.children(C).length;d.parentsUntil(".k-animation-container",".k-list-container,.k-calendar-container")[0]?b.stopImmediatePropagation():!c.options.openOnClick&&!M(b.currentTarget,b.relatedTarget)&&e&&c.close(d)},_click:function(b){var d=this,f,h=a(c.eventTarget(b)),i=h.closest("."+o),j=i.attr("href"),k=h.closest(D),l=!!j&&j.charAt(j.length-1)!="#";if(!k.children(H)[0]){if(k.hasClass(B)){b.preventDefault();return}e&&k.siblings().each(g(function(a,b){d.close(b)},d)),!b.handled&&d.trigger(v,{item:k[0]})&&b.preventDefault(),b.handled=!0,d.options.closeOnClick&&!(j&&j.length>0)&&!k.children(C+",.k-animation-container").length&&d.close(i.parentsUntil(d.element,D));if((!k.parent().hasClass(n)||!d.options.openOnClick)&&!e)return;l||b.preventDefault(),d.clicked=!0,f=k.children(".k-animation-container, .k-group").is(":visible")?q:m,d[f](k)}},_documentClick:function(a){var b=this;M(b.element[0],a.target)||(b.clicked=!1)}});f(Q,{renderItem:function(a){a=f({menu:{},group:{}},a);var b=I.empty,c=a.item;return I.item(f(a,{image:c.imageUrl?I.image:b,sprite:c.spriteCssClass?I.sprite:b,itemWrapper:I.itemWrapper,arrow:c.items||c.content?I.arrow:b,subGroup:Q.renderGroup},J))},renderGroup:function(a){return I.group(f({renderItems:function(a){var b="",c=0,d=a.items,e=d?d.length:0,g=f({length:e},a.group);for(;c<e;c++)b+=Q.renderItem(f(a,{group:g,item:f({index:c},d[c])}));return b}},a,J))},renderContent:function(a){return I.content(f(a,J))}}),c.ui.plugin(Q)}(jQuery),function(a,b){function p(a){var b,c,d,e,f,g;if(a&&a.length){g=[];for(b=0,c=a.length;b<c;b++)d=a[b],f=d.text||d.value||d,e=d.value==null?d.text||d:d.value,g[b]={text:f,value:e}}return g}function o(a){var b=(a.model.fields||a.model)[a.field],d=m(b),e=b?b.validation:{},f,j=c.attr("type"),k=c.attr("bind"),n,o={name:a.field};for(f in e)n=e[f],i(f,l)>=0?o[j]=f:g(n)||(o[f]=h(n)?n.value||f:n),o[c.attr(f+"-msg")]=n.message;i(d,l)>=0&&(o[j]=d),o[k]=(d==="boolean"?"checked:":"value:")+a.field;return o}function n(b){b.find(":input:not(:button, ["+c.attr("role")+"=upload], ["+c.attr("skip")+"]), select").each(function(){var b=c.attr("bind"),d=this.getAttribute(b)||"",e=this.type==="checkbox"?"checked:":"value:",f=this.name;d.indexOf(e)===-1&&f&&(d+=(d.length?",":"")+e+f,a(this).attr(b,d))})}function m(b){b=b!=null?b:"";return b.type||a.type(b)||"string"}var c=window.kendo,d=c.ui,e=d.Widget,f=a.extend,g=a.isFunction,h=a.isPlainObject,i=a.inArray,j='<div class="k-widget k-tooltip k-tooltip-validation" style="margin:0.5em"><span class="k-icon k-warning"> </span>${message}<div class="k-callout k-callout-n"></div></div>',k="change",l=["url","email","number","date","boolean"],q={number:function(b,d){var e=o(d);a('<input type="text"/>').attr(e).appendTo(b).kendoNumericTextBox({format:d.format}),a("<span "+c.attr("for")+'="'+d.field+'" class="k-invalid-msg"/>').hide().appendTo(b)},date:function(b,d){var e=o(d);e[c.attr("format")]=d.format,a('<input type="text"/>').attr(e).appendTo(b).kendoDatePicker({format:d.format}),a("<span "+c.attr("for")+'="'+d.field+'" class="k-invalid-msg"/>').hide().appendTo(b)},string:function(b,c){var d=o(c);a('<input type="text" class="k-input k-textbox"/>').attr(d).appendTo(b)},"boolean":function(b,c){var d=o(c);a('<input type="checkbox" />').attr(d).appendTo(b)},values:function(b,d){var e=o(d);a("<select "+c.attr("text-field")+'="text"'+c.attr("value-field")+'="value"'+c.attr("source")+"='"+c.stringify(p(d.values))+"'"+c.attr("role")+'="dropdownlist"/>').attr(e).appendTo(b),a("<span "+c.attr("for")+'="'+d.field+'" class="k-invalid-msg"/>').hide().appendTo(b)}},r=e.extend({init:function(b,c){var d=this;e.fn.init.call(d,b,c),d._validateProxy=a.proxy(d._validate,d),d.refresh()},events:[k],options:{name:"Editable",editors:q,clearContainer:!0,errorTemplate:j},editor:function(a,b){var c=this,d=c.options.editors,e=h(a),g=e?a.field:a,i=c.options.model||{},j=e&&a.values,k=j?"values":m(b),l=e&&a.editor,n=l?a.editor:d[k],o=c.element.find("[data-container-for="+g+"]");n=n?n:d.string,l&&typeof a.editor=="string"&&(n=function(b){b.append(a.editor)}),o=o.length?o:c.element,n(o,f(!0,{},e?a:{field:g},{model:i}))},_validate:function(b){var d=this,e=typeof b.value=="boolean",f,g=d._validationEventInProgress,h={};h[b.field]=b.value,f=a(":input["+c.attr("bind")+'="'+(e?"checked:":"value:")+b.field+'"]',d.element);try{d._validationEventInProgress=!0,(!d.validatable.validateInput(f)||!g&&d.trigger(k,{values:h}))&&b.preventDefault()}finally{d._validationEventInProgress=!1}},end:function(){return this.validatable.validate()},destroy:function(){this.options.model.unbind("set",this._validateProxy),c.unbind(this.element),this.element.removeData("kendoValidator").removeData("kendoEditable")},refresh:function(){var d=this,e,f,i=d.options.fields||[],j=d.options.clearContainer?d.element.empty():d.element,k=d.options.model||{},l={};a.isArray(i)||(i=[i]);for(e=0,f=i.length;e<f;e++){var m=i[e],o=h(m),p=o?m.field:m,q=(k.fields||k)[p],r=q?q.validation||{}:{};for(var s in r)g(r[s])&&(l[s]=r[s]);d.editor(m,q)}n(j),c.bind(j,d.options.model),d.options.model.bind("set",d._validateProxy),d.validatable=j.kendoValidator({validateOnBlur:!1,errorTemplate:d.options.errorTemplate||b,rules:l}).data("kendoValidator"),j.find(":input:visible:first").focus()}});d.plugin(r)}(jQuery),function(a,b){function o(a){var b,c,d,e,f,g;if(a&&a.length){g=[];for(b=0,c=a.length;b<c;b++)d=a[b],f=d.text||d.value||d,e=d.value==null?d.text||d:d.value,g[b]={text:f,value:e}}return g}function n(b,c){b.filters&&(b.filters=a.grep(b.filters,function(a){n(a,c);return a.filters?a.filters.length:a.field!=c}))}var c=window.kendo,d=c.ui,e="kendoNumericTextBox",f="kendoDatePicker",g=a.proxy,h="kendoPopup",i="Is equal to",j="Is not equal to",k=d.Widget,l='<div><div class="k-filter-help-text">#=messages.info#</div><label><input type="radio" data-#=ns#bind="checked: filters[0].value" value="true" name="filters[0].value"/>#=messages.isTrue#</label><label><input type="radio" data-#=ns#bind="checked: filters[0].value" value="false" name="filters[0].value"/>#=messages.isFalse#</label><button type="submit" class="k-button">#=messages.filter#</button><button type="reset" class="k-button">#=messages.clear#</button></div>',m='<div><div class="k-filter-help-text">#=messages.info#</div><select data-#=ns#bind="value: filters[0].operator" data-#=ns#role="dropdownlist">#for(var op in operators){#<option value="#=op#">#=operators[op]#</option>#}#</select>#if(values){#<select data-#=ns#bind="value:filters[0].value" data-#=ns#text-field="text" data-#=ns#value-field="value" data-#=ns#source=\'#=kendo.stringify(values)#\' data-#=ns#role="dropdownlist" data-#=ns#option-label="#=messages.selectValue#"></select>#}else{#<input data-#=ns#bind="value:filters[0].value" class="k-textbox" type="text" data-#=ns#type="#=type#"/>#}##if(extra){#<select class="k-filter-and" data-#=ns#bind="value: logic" data-#=ns#role="dropdownlist"><option value="and">#=messages.and#</option><option value="or">#=messages.or#</option></select><select data-#=ns#bind="value: filters[1].operator" data-#=ns#role="dropdownlist">#for(var op in operators){#<option value="#=op#">#=operators[op]#</option>#}#</select>#if(values){#<select data-#=ns#bind="value:filters[1].value" data-#=ns#text-field="text" data-#=ns#value-field="value" data-#=ns#source=\'#=kendo.stringify(values)#\' data-#=ns#role="dropdownlist" data-#=ns#option-label="#=messages.selectValue#"></select>#}else{#<input data-#=ns#bind="value: filters[1].value" class="k-textbox" type="text" data-#=ns#type="#=type#"/>#}##}#<button type="submit" class="k-button">#=messages.filter#</button><button type="reset" class="k-button">#=messages.clear#</button></div>',p=k.extend({init:function(b,d){var i=this,j="string",n,p,q;k.fn.init.call(i,b,d),q=d.operators||{},b=i.element,d=i.options,d.appendToElement?i.link=a():(n=b.addClass("k-filterable").find(".k-grid-filter"),n[0]||(n=b.prepend('<a class="k-grid-filter" href="#"><span class="k-icon k-filter"/></a>').find(".k-grid-filter")),i._clickHandler=g(i._click,i),n.click(i._clickHandler)),i.dataSource=d.dataSource.bind("change",g(i.refresh,i)),i.field=d.field||b.attr(c.attr("field")),i.model=i.dataSource.reader.model,i._parse=function(a){return a+""},i.model&&i.model.fields&&(p=i.model.fields[i.field],p&&(j=p.type||"string",i._parse=g(p.parse,p))),d.values&&(j="enums"),q=q[j]||d.operators[j],i.form=a('<form class="k-filter-menu"/>'),i.form.html(c.template(j==="boolean"?l:m)({field:i.field,ns:c.ns,messages:d.messages,extra:d.extra,operators:q,type:j,values:o(d.values)})),d.appendToElement?(b.append(i.form),i.popup=i.element.closest(".k-popup").data(h)):(i.popup=i.form[h]({anchor:n,open:g(i._open,i)}).data(h),i.link=n),i.form.bind({submit:g(i._submit,i),reset:g(i._reset,i)}).find("["+c.attr("type")+"=number]").removeClass("k-textbox")[e]().end().find("["+c.attr("type")+"=date]").removeClass("k-textbox")[f](),i.refresh()},refresh:function(){var a=this,b=a.dataSource.filter()||{filters:[],logic:"and"};a.filterModel=c.observable({logic:"and",filters:[{field:a.field,operator:"eq",value:""},{field:a.field,operator:"eq",value:""}]}),c.bind(a.form,a.filterModel),a._bind(b)?a.link.addClass("k-state-active"):a.link.removeClass("k-state-active")},destroy:function(){c.unbind(this.form),this.form.remove(),this.form.removeData(h),this.link.unbind("click",this._clickHandler),this.element.removeData("kendoFilterMenu")},_bind:function(a){var b=this,c=a.filters,d,e,f=!1,g=0,h=b.filterModel,i,j;for(d=0,e=c.length;d<e;d++)j=c[d],j.field==b.field?(h.set("logic",a.logic),i=h.filters[g],i.set("value",b._parse(j.value)),i.set("operator",j.operator),g++,f=!0):j.filters&&(f=f||b._bind(j));return f},_merge:function(b){var c=this,d=b.logic||"and",e=b.filters,f,g=c.dataSource.filter()||{filters:[],logic:"and"},h,i;n(g,c.field),e=a.grep(e,function(a){return a.value!==""});for(h=0,i=e.length;h<i;h++)f=e[h],f.value=c._parse(f.value);e.length&&(g.filters.length?(b.filters=e,g.logic!=="and"&&(g.filters=[{logic:g.logic,filters:g.filters}],g.logic="and"),e.length>1?g.filters.push(b):g.filters.push(e[0])):(g.filters=e,g.logic=d));return g},filter:function(a){a=this._merge(a),a.filters.length&&this.dataSource.filter(a)},clear:function(){var b=this,c=b.dataSource.filter()||{filters:[]};c.filters=a.grep(c.filters,function(c){if(c.filters){c.filters=a.grep(c.filters,function(a){return a.field!=b.field});return c.filters.length}return c.field!=b.field}),c.filters.length||(c=null),b.dataSource.filter(c)},_submit:function(a){var b=this;a.preventDefault(),b.filter(b.filterModel.toJSON()),b.popup.close()},_reset:function(a){this.clear(),this.popup.close()},_click:function(a){a.preventDefault(),a.stopPropagation(),this.popup.toggle()},_open:function(){var b;a(".k-filter-menu").not(this.form).each(function(){b=a(this).data(h),b&&b.close()})},options:{name:"FilterMenu",extra:!0,appendToElement:!1,type:"string",operators:{string:{eq:i,neq:j,startswith:"Starts with",contains:"Contains",doesnotcontain:"Does not contain",endswith:"Ends with"},number:{eq:i,neq:j,gte:"Is greater than or equal to",gt:"Is greater than",lte:"Is less than or equal to",lt:"Is less than"},date:{eq:i,neq:j,gte:"Is after or equal to",gt:"Is after",lte:"Is before or equal to",lt:"Is before"},enums:{eq:i,neq:j}},messages:{info:"Show items with value that:",isTrue:"is true",isFalse:"is false",filter:"Filter",clear:"Clear",and:"And",or:"Or",selectValue:"-Select value-"}}});d.plugin(p)}(jQuery),function(a,b){function U(a,b){a.find(J).removeClass(E.substr(1)),a.find(K).removeClass(G.substr(1)),b.addClass(E.substr(1)),b.parentsUntil(a,p).filter(":has(.k-header)").addClass(G.substr(1))}function T(b){b=a(b),b.filter(".k-first:not(:first-child)").removeClass(r),b.filter(".k-last:not(:last-child)").removeClass(l),b.filter(":first-child").addClass(r),b.filter(":last-child").addClass(l)}function S(b){b=a(b),b.children(".k-link").children(".k-icon").remove(),b.filter(":has(.k-panel),:has(.k-content)").children(".k-link:not(:has([class*=k-i-arrow]))").each(function(){var b=a(this),c=b.parent();b.append("<span class='k-icon "+(c.hasClass(B.substr(1))?"k-i-arrow-n k-panelbar-collapse":"k-i-arrow-s k-panelbar-expand")+"'/>")})}function R(b,c){b=a(b).addClass("k-item"),b.children(j).addClass(q),b.children("a").addClass(m).children(j).addClass(q),b.filter(":not([disabled]):not([class*=k-state])").addClass("k-state-default"),b.filter("li[disabled]").addClass("k-state-disabled").removeAttr("disabled"),b.filter(":not([class*=k-state])").children("a").filter(":focus").parent().addClass(B.substr(1)),b.find(">div").addClass(u).css({display:"none"}),b.each(function(){var b=a(this);b.children("."+m).length||b.contents().filter(function(){return!this.nodeName.match(i)&&(this.nodeType!=3||!!a.trim(this.nodeValue))}).wrapAll("<span class='"+m+"'/>")}),c.find(" > li > ."+m).addClass("k-header")}var c=window.kendo,d=c.ui,e=a.extend,f=a.each,g=c.template,h=d.Widget,i=/^(ul|a|div)$/i,j="img",k="href",l="k-last",m="k-link",n="error",o="click",p=".k-item",q="k-image",r="k-first",s="expand",t="select",u="k-content",v="activate",w="collapse",x="contentUrl",y="mouseenter",z="mouseleave",A="contentLoad",B=".k-state-active",C="> .k-panel",D="> .k-content",E=".k-state-selected",F=".k-state-disabled",G=".k-state-highlighted",H=p+":not(.k-state-disabled) .k-link",I=p+".k-state-disabled .k-link",J="> li > .k-state-selected, .k-panel > li > .k-state-selected",K="> .k-state-highlighted, .k-panel > .k-state-highlighted",L="k-state-default",M=":visible",N=":empty",O="single",P={content:g("<div class='k-content'#= contentAttributes(data) #>#= content(item) #</div>"),group:g("<ul class='#= groupCssClass(group) #'#= groupAttributes(group) #>#= renderItems(data) #</ul>"),itemWrapper:g("<#= tag(item) # class='#= textClass(item, group) #'#= contentUrl(item) ##= textAttributes(item) #>#= image(item) ##= sprite(item) ##= text(item) ##= arrow(data) #</#= tag(item) #>"),item:g("<li class='#= wrapperCssClass(group, item) #'>#= itemWrapper(data) ## if (item.items) { ##= subGroup({ items: item.items, panelBar: panelBar, group: { expanded: item.expanded } }) ## } #</li>"),image:g("<img class='k-image' alt='' src='#= imageUrl #' />"),arrow:g("<span class='#= arrowClass(item) #'></span>"),sprite:g("<span class='k-sprite #= spriteCssClass #'></span>"),empty:g("")},Q={wrapperCssClass:function(a,b){var c="k-item",d=b.index;b.enabled===!1?c+=" k-state-disabled":b.expanded===!0?c+=" k-state-active":c+=" k-state-default",d===0&&(c+=" k-first"),d==a.length-1&&(c+=" k-last");return c},textClass:function(a,b){var c=m;b.firstLevel&&(c+=" k-header");return c},textAttributes:function(a){return a.url?" href='"+a.url+"'":""},arrowClass:function(a){var b="k-icon";b+=a.expanded?" k-i-arrow-n k-panelbar-collapse":" k-i-arrow-s k-panelbar-expand";return b},text:function(a){return a.encoded===!1?a.text:c.htmlEncode(a.text)},tag:function(a){return a.url?"a":"span"},groupAttributes:function(a){return a.expanded!==!0?" style='display:none'":""},groupCssClass:function(a){return"k-group k-panel"},contentAttributes:function(a){return a.item.expanded!==!0?" style='display:none'":""},content:function(a){return a.content?a.content:a.contentUrl?"":"&nbsp;"},contentUrl:function(a){return a.contentUrl?c.attr("content-url")+'="'+a.contentUrl+'"':""}},V=h.extend({init:function(b,d){var e=this,f;h.fn.init.call(e,b,d),b=e.wrapper=e.element,d=e.options,d.dataSource&&(e.element.empty(),e.append(d.dataSource,b)),e._updateClasses(),d.animation===!1&&(d.animation={expand:{show:!0,effects:{}},collapse:{hide:!0,effects:{}}}),b.delegate(H,o,a.proxy(e._click,e)).delegate(H,y+" "+z,e._toggleHover).delegate(I,o,!1),d.contentUrls&&b.find("> .k-item").each(function(b,c){a(c).find("."+m).data(x,d.contentUrls[b])}),f=b.find("li"+B+" > ."+u),f.length>0&&e.expand(f.parent(),!1),c.notify(e)},events:[s,w,t,v,n,A],options:{name:"PanelBar",animation:{expand:{effects:"expand:vertical",duration:200,show:!0},collapse:{duration:200}},expandMode:"multiple"},expand:function(b,c){var d=this,e={};c=c!==!1,b=this.element.find(b),b.each(function(f,g){g=a(g);var h=g.find(C).add(g.find(D));if(!g.hasClass(F)&&h.length>0){if(d.options.expandMode==O&&d._collapseAllExpanded(g))return d;b.find(G).removeClass(G.substr(1)),g.addClass(G.substr(1)),c||(e=d.options.animation,d.options.animation={expand:{show:!0,effects:{}},collapse:{hide:!0,effects:{}}}),d._triggerEvent(s,g)||d._toggleItem(g,!1,null),c||(d.options.animation=e)}});return d},collapse:function(b,c){var d=this,e={};c=c!==!1,b=d.element.find(b),b.each(function(b,f){f=a(f);var g=f.find(C).add(f.find(D));!f.hasClass(F)&&g.is(M)&&(f.removeClass(G.substr(1)),c||(e=d.options.animation,d.options.animation={expand:{show:!0,effects:{}},collapse:{hide:!0,effects:{}}}),d._triggerEvent(w,f)||d._toggleItem(f,!0,null),c||(d.options.animation=e))});return d},_toggleDisabled:function(a,b){a=this.element.find(a),a.toggleClass(L,b).toggleClass(F.substr(1),!b)},select:function(b){var c=this;b=c.element.find(b);if(arguments.length===0)return c.element.find(J).parent();b.each(function(b,d){d=a(d);var e=d.children("."+m);if(d.is(F))return c;U(c.element,e)});return c},enable:function(a,b){this._toggleDisabled(a,b!==!1);return this},disable:function(a){this._toggleDisabled(a,!1);return this},append:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.length?c.find(C):null);f(d.items,function(b){d.group.append(this);var c=d.contents[b];c&&a(this).append(c),T(this)}),S(c),T(d.group.find(".k-first, .k-last")),d.group.height("auto");return this},insertBefore:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.parent());f(d.items,function(b){c.before(this);var e=d.contents[b];e&&a(this).append(e),T(this)}),T(c),d.group.height("auto");return this},insertAfter:function(b,c){c=this.element.find(c);var d=this._insert(b,c,c.parent());f(d.items,function(b){c.after(this);var e=d.contents[b];e&&a(this).append(e),T(this)}),T(c),d.group.height("auto");return this},remove:function(a){a=this.element.find(a);var b=this,c=a.parentsUntil(b.element,p),d=a.parent("ul");a.remove(),d&&!d.hasClass("k-panelbar")&&!d.children(p).length&&d.remove(),c.length&&(c=c.eq(0),S(c),T(c));return b},reload:function(b){var c=this;b=c.element.find(b),b.each(function(){var b=a(this);c._ajaxRequest(b,b.children("."+u),!b.is(M))})},_insert:function(b,c,d){var f=this,g,h=[];if(!c||!c.length)d=f.element;var i=a.isPlainObject(b),j={firstLevel:d.hasClass("k-panelbar"),expanded:d.parent().hasClass("k-state-active"),length:d.children().length};c&&!d.length&&(d=a(V.renderGroup({group:j})).appendTo(c)),i||a.isArray(b)?(g=a.map(i?[b]:b,function(b,c){return typeof b=="string"?a(b):a(V.renderItem({group:j,item:e(b,{index:c})}))}),h=a.map(i?[b]:b,function(b,c){return b.content||b.contentUrl?a(V.renderContent({item:e(b,{index:c})})):!1})):(g=a(b),R(g,f.element));return{items:g,group:d,contents:h}},_toggleHover:function(b){var c=a(b.currentTarget);c.parents("li"+F).length||c.toggleClass("k-state-hover",b.type==y)},_updateClasses:function(){var b=this;b.element.addClass("k-widget k-reset k-header k-panelbar");var c=b.element.find("li > ul").not(function(){return a(this).parentsUntil(".k-panelbar","div").length}).addClass("k-group k-panel").add(b.element),d=c.find("> li:not("+B+") > ul").css({display:"none"}).end().find("> li");d.each(function(){R(this,b.element)}),S(d),T(d)},_click:function(b){var c=this,d=a(b.currentTarget),e=c.element;if(!d.parents("li"+F).length){if(d.closest(".k-widget")[0]!=e[0])return;var f=d.closest("."+m),g=f.closest(p);U(e,f);var h=g.find(C).add(g.find(D)),i=f.attr(k),j=f.data(x)||i&&(i.charAt(i.length-1)=="#"||i.indexOf("#"+c.element[0].id+"-")!=-1);if(h.data("animating"))return;c._triggerEvent(t,g)&&b.preventDefault();if(j||h.length)b.preventDefault();else return;if(c.options.expandMode==O&&c._collapseAllExpanded(g))return;if(h.length){var l=h.is(M);c._triggerEvent(l?w:s,g)||c._toggleItem(g,l,b)}}},_toggleItem:function(a,b,c){var d=this,e=a.find(C);if(e.length)this._toggleGroup(e,b),c&&c.preventDefault();else{var f=a.find("> ."+u);f.length&&(c&&c.preventDefault(),f.is(N)?d._ajaxRequest(a,f,b):d._toggleGroup(f,b))}},_toggleGroup:function(a,b){var c=this,d=c.options.animation,f=d.expand,g=e({},d.collapse),h=g&&"effects"in g;a.is(M)==b&&(a.parent().toggleClass(L,b).toggleClass(B.substr(1),!b).find("> .k-link > .k-icon").toggleClass("k-i-arrow-n",!b).toggleClass("k-panelbar-collapse",!b).toggleClass("k-i-arrow-s",b).toggleClass("k-panelbar-expand",b),b?f=e(h?g:e({reverse:!0},f),{show:!1,hide:!0}):f=e({complete:function(a){c._triggerEvent(v,a.closest(p))}},f),a.kendoStop(!0,!0).kendoAnimate(f))},_collapseAllExpanded:function(b){var c=this,d,e=!1;if(b.find("> ."+m).hasClass("k-header")){var f=b.find(C).add(b.find(D));f.is(M)&&(e=!0),!f.is(M)&&f.length!==0&&(d=a(c.element).children(),d.find(C).add(d.find(D)).filter(function(){return a(this).is(M)}).each(function(b,d){d=a(d),e=c._triggerEvent(w,d.closest(p)),e||c._toggleGroup(d,!0)}));return e}},_ajaxRequest:function(b,c,d){var e=this,f=b.find(".k-panelbar-collapse, .k-panelbar-expand"),g=b.find("."+m),h=setTimeout(function(){f.addClass("k-loading")},100),i={};a.ajax({type:"GET",cache:!1,url:g.data(x)||g.attr(k),dataType:"html",data:i,error:function(a,b){f.removeClass("k-loading"),e.trigger(n,{xhr:a,status:b})&&this.complete()},complete:function(){clearTimeout(h),f.removeClass("k-loading")},success:function(a,f){c.html(a),e._toggleGroup(c,d),e.trigger(A,{item:b[0],contentElement:c[0]})}})},_triggerEvent:function(a,b){var c=this;return c.trigger(a,{item:b[0]})}});e(V,{renderItem:function(a){a=e({panelBar:{},group:{}},a);var b=P.empty,c=a.item;return P.item(e(a,{image:c.imageUrl?P.image:b,sprite:c.spriteCssClass?P.sprite:b,itemWrapper:P.itemWrapper,arrow:c.items||c.content||c.contentUrl?P.arrow:b,subGroup:V.renderGroup},Q))},renderGroup:function(a){return P.group(e({renderItems:function(a){var b="",c=0,d=a.items,f=d?d.length:0,g=e({length:f},a.group);for(;c<f;c++)b+=V.renderItem(e(a,{group:g,item:e({index:c},d[c])}));return b}},a,Q))},renderContent:function(a){return P.content(e(a,Q))}}),c.ui.plugin(V)}(jQuery),function(a,b){function M(a){var b=a.children(".k-item");b.filter(".k-first:not(:first-child)").removeClass(t),b.filter(".k-last:not(:last-child)").removeClass(o),b.filter(":first-child").addClass(t),b.filter(":last-child").addClass(o)}function L(b){b.children(l).addClass(s),b.children("a").addClass(n).children(l).addClass(s),b.filter(":not([disabled]):not([class*=k-state-disabled])").addClass(F),b.filter("li[disabled]").addClass(E).removeAttr("disabled"),b.filter(":not([class*=k-state])").children("a").filter(":focus").parent().addClass(G+" "+I),b.each(function(){var b=a(this);b.children("."+n).length||b.contents().filter(function(){return!this.nodeName.match(k)&&(this.nodeType!=3||!!g(this.nodeValue))}).wrapAll("<a class='"+n+"'/>")})}var c=window.kendo,d=c.ui,e=a.map,f=a.each,g=a.trim,h=a.extend,i=c.template,j=d.Widget,k=/^(a|div)$/i,l="img",m="href",n="k-link",o="k-last",p="click",q="error",r=":empty",s="k-image",t="k-first",u="select",v="activate",w="k-content",x="contentUrl",y="mouseenter",z="mouseleave",A="contentLoad",B=".k-tabstrip-items > .k-item:not(.k-state-disabled)",C=".k-tabstrip-items > .k-item:not(.k-state-disabled):not(.k-state-active)",D=".k-tabstrip-items > .k-state-disabled .k-link",E="k-state-disabled",F="k-state-default",G="k-state-active",H="k-state-hover",I="k-tab-on-top",J={content:i("<div class='k-content'#= contentAttributes(data) #>#= content(item) #</div>"),itemWrapper:i("<#= tag(item) # class='k-link'#= contentUrl(item) ##= textAttributes(item) #>#= image(item) ##= sprite(item) ##= text(item) #</#= tag(item) #>"),item:i("<li class='#= wrapperCssClass(group, item) #'>#= itemWrapper(data) #</li>"),image:i("<img class='k-image' alt='' src='#= imageUrl #' />"),sprite:i("<span class='k-sprite #= spriteCssClass #'></span>"),empty:i("")},K={wrapperCssClass:function(a,b){var c="k-item",d=b.index;b.enabled===!1?c+=" k-state-disabled":c+=" k-state-default",d===0&&(c+=" k-first"),d==a.length-1&&(c+=" k-last");return c},textAttributes:function(a){return a.url?" href='"+a.url+"'":""},text:function(a){return a.encoded===!1?a.text:c.htmlEncode(a.text)},tag:function(a){return a.url?"a":"span"},contentAttributes:function(a){return a.active!==!0?" style='display:none'":""},content:function(a){return a.content?a.content:a.contentUrl?"":"&nbsp;"},contentUrl:function(a){return a.contentUrl?c.attr("content-url")+'="'+a.contentUrl+'"':""}},N=j.extend({init:function(b,d){var e=this;e._animations(d),j.fn.init.call(e,b,d),e.element.is("ul")?e.wrapper=e.element.wrapAll("<div />").parent():e.wrapper=e.element,d=e.options,e.wrapper.delegate(B,p,a.proxy(e._click,e)).delegate(C,y+" "+z,e._toggleHover).delegate(D,p,!1),e._updateClasses(),e._dataSource(),d.dataSource&&e.dataSource.fetch(),e.options.contentUrls&&e.wrapper.find(".k-tabstrip-items > .k-item").each(function(b,c){a(c).find(">."+n).data(x,e.options.contentUrls[b])});var f=e.wrapper.find("li."+G),g=a(e.contentElement(f.parent().children().index(f)));g.length>0&&g[0].childNodes.length===0&&e.activateTab(f.eq(0)),c.notify(e)},_dataSource:function(){var b=this;b.dataSource&&b._refreshHandler?b.dataSource.unbind("change",b._refreshHandler):b._refreshHandler=a.proxy(b.refresh,b),b.dataSource=c.data.DataSource.create(b.options.dataSource).bind("change",b._refreshHandler)},setDataSource:function(a){this.options.dataSource=a,this._dataSource(),a.fetch()},_animations:function(a){a&&"animation"in a&&!a.animation&&(a.animation={open:{effects:{},show:!0},close:{effects:{}}})},refresh:function(a){var b=this,d=b.options,e=c.getter(d.dataTextField),f=c.getter(d.dataContentField),g=c.getter(d.dataContentUrlField),h=c.getter(d.dataImageUrlField),i=c.getter(d.dataUrlField),j=c.getter(d.dataSpriteCssClass),k,l=[],m,n,o=b.dataSource.view(),p;a=a||{},n=a.action,n&&(o=a.items);for(k=0,p=o.length;k<p;k++)m={text:e(o[k])},d.dataContentField&&(m.content=f(o[k])),d.dataContentUrlField&&(m.contentUrl=g(o[k])),d.dataUrlField&&(m.url=i(o[k])),d.dataImageUrlField&&(m.imageUrl=h(o[k])),d.dataSpriteCssClass&&(m.spriteCssClass=j(o[k])),l[k]=m;if(a.action=="add")a.index<b.tabGroup.children().length?b.insertBefore(l,b.tabGroup.children().eq(a.index)):b.append(l);else if(a.action=="remove")for(k=0;k<o.length;k++)b.remove(a.index);else a.action=="itemchange"?(k=b.dataSource.view().indexOf(o[0]),a.field===d.dataTextField&&b.tabGroup.children().eq(k).find(".k-link").text(o[0].get(a.field))):(b.trigger("dataBinding"),b.append(l),b.trigger("dataBound"))},value:function(c){var d=this;if(c!==b)c!=d.value()&&d.tabGroup.children().each(function(){a.trim(a(this).text())==c&&d.select(this)});else return d.select().text()},items:function(){return this.tabGroup[0].children},setOptions:function(a){var b=this.options.animation;this._animations(a),a.animation=h(!0,b,a.animation),j.fn.setOptions.call(this,a)},events:[u,v,q,A,"change","dataBinding","dataBound"],options:{name:"TabStrip",dataTextField:"",dataContentField:"",dataImageUrlField:"",dataUrlField:"",dataSpriteCssClass:"",dataContentUrlField:"",animation:{open:{effects:"expand:vertical fadeIn",duration:200,show:!0},close:{duration:200}},collapsible:!1},select:function(b){var c=this;if(arguments.length===0)return c.wrapper.find("li."+G);isNaN(b)||(b=c.tabGroup.children().get(b)),b=c.element.find(b),a(b).each(function(b,d){d=a(d),!d.hasClass(G)&&!c.trigger(u,{item:d[0],contentElement:c.contentElement(d.index())})&&c.activateTab(d)});return c},enable:function(a,b){this._toggleDisabled(a,b!==!1);return this},disable:function(a){this._toggleDisabled(a,!1);return this},reload:function(b){b=this.tabGroup.find(b);var c=this;b.each(function(){var b=a(this),d=b.find("."+n).data(x);d&&c.ajaxRequest(b,a(c.contentElement(b.index())),null,d)});return c},append:function(a){var b=this,c=b._create(a);f(c.tabs,function(a){b.tabGroup.append(this),b.wrapper.append(c.contents[a])}),M(b.tabGroup),b._updateContentElements();return b},insertBefore:function(b,c){var d=this,e=d._create(b),g=a(d.contentElement(c.index()));f(e.tabs,function(a){c.before(this),g.before(e.contents[a])}),M(d.tabGroup),d._updateContentElements();return d},insertAfter:function(b,c){var d=this,e=d._create(b),g=a(d.contentElement(c.index()));f(e.tabs,function(a){c.after(this),g.after(e.contents[a])}),M(d.tabGroup),d._updateContentElements();return d},remove:function(b){var c=this,d=typeof b,e;d==="string"?b=c.tabGroup.find(b):d==="number"&&(b=c.tabGroup.children().eq(b)),e=a(c.contentElement(b.index())),e.remove(),b.remove(),c._updateContentElements();return c},_create:function(b){var c=a.isPlainObject(b),d=this,f,g;c||a.isArray(b)?(b=a.isArray(b)?b:[b],f=e(b,function(b,c){return a(N.renderItem({group:d.tabGroup,item:h(b,{index:c})}))}),g=e(b,function(b,c){if(b.content||b.contentUrl)return a(N.renderContent({item:h(b,{index:c})}))})):(f=a(b),g=a("<div class='"+w+"'/>"),L(f));return{tabs:f,contents:g}},_toggleDisabled:function(b,c){b=this.tabGroup.find(b),b.each(function(){a(this).toggleClass(F,c).toggleClass(E,!c)})},_updateClasses:function(){var c=this,d,e,f;c.wrapper.addClass("k-widget k-header k-tabstrip"),c.tabGroup=c.wrapper.children("ul").addClass("k-tabstrip-items k-reset"),c.tabGroup[0]||(c.tabGroup=a("<ul class='k-tabstrip-items k-reset'/>").appendTo(c.wrapper)),d=c.tabGroup.find("li").addClass("k-item"),d.length&&(e=d.filter("."+G).index(),f=e>=0?e:b,c.tabGroup.contents().filter(function(){return this.nodeType==3&&!g(this.nodeValue)}).remove()),d.eq(e).addClass(I),c.contentElements=c.wrapper.children("div"),c.contentElements.addClass(w).eq(f).addClass(G).css({display:"block"}),d.length&&(L(d),M(c.tabGroup),c._updateContentElements())},_updateContentElements:function(){var b=this,d=b.options.contentUrls||[],e=b.element.attr("id"),f=b.wrapper.children("div");b.tabGroup.find(".k-item").each(function(c){var g=f.eq(c),h=e+"-"+(c+1);!g.length&&d[c]?a("<div id='"+h+"' class='"+w+"'/>").appendTo(b.wrapper):g.attr("id",h)}),b.contentElements=b.contentAnimators=b.wrapper.children("div"),c.support.touch&&c.mobile.ui.Scroller&&(c.touchScroller(b.contentElements),b.contentElements=b.contentElements.children(".km-scroll-container"))},_toggleHover:function(b){a(b.currentTarget).toggleClass(H,b.type==y)},_click:function(b){var c=this,d=a(b.currentTarget),e=d.find("."+n),f=e.attr(m),g=c.options.collapsible,h=a(c.contentElement(d.index()));if(d.closest(".k-widget")[0]==c.wrapper[0]){if(d.is("."+E+(g?"":",."+G))){b.preventDefault();return}if(c.tabGroup.children("[data-animating], [data-in-request]").length)return;if(c.trigger(u,{item:d[0],contentElement:h[0]}))b.preventDefault();else{var i=e.data(x)||f&&(f.charAt(f.length-1)=="#"||f.indexOf("#"+c.element[0].id+"-")!=-1);if(!f||i)b.preventDefault();else return;if(g&&d.is("."+G)){c.deactivateTab(d),b.preventDefault();return}c.activateTab(d)&&b.preventDefault()}}},deactivateTab:function(a){var b=this,d=b.options.animation,e=d.open,f=h({},d.close),g=f&&"effects"in f;a=b.tabGroup.find(a),f=h(g?f:h({reverse:!0},e),{show:!1,hide:!0}),c.size(e.effects)?(a.kendoAddClass(F,{duration:e.duration}),a.kendoRemoveClass(G,{duration:e.duration})):(a.addClass(F),a.removeClass(G)),b.contentAnimators.filter("."+G).kendoStop(!0,!0).kendoAnimate(f).removeClass(G)},activateTab:function(b){b=this.tabGroup.find(b);var d=this,e=d.options.animation,f=e.open,g=h({},e.close),i=g&&"effects"in g,j=b.parent().children(),k=j.filter("."+G),l=j.index(b);g=h(i?g:h({reverse:!0},f),{show:!1,hide:!0}),c.size(f.effects)?(k.kendoRemoveClass(G,{duration:g.duration}),b.kendoRemoveClass(H,{duration:g.duration})):(k.removeClass(G),b.removeClass(H));var m=d.contentAnimators;if(m.length===0){k.removeClass(I),b.addClass(I).css("z-index"),b.addClass(G),d.trigger("change");return!1}var o=m.filter("."+G),p=a(d.contentElement(l));if(p.length===0){o.removeClass(G).kendoStop(!0,!0).kendoAnimate(g);return!1}b.attr("data-animating",!0);var q=(b.children("."+n).data(x)||!1)&&p.is(r),s=function(){k.removeClass(I),b.addClass(I).css("z-index"),c.size(f.effects)?(k.kendoAddClass(F,{duration:f.duration}),b.kendoAddClass(G,{duration:f.duration})):(k.addClass(F),b.addClass(G)),p.closest(".k-content").addClass(G).kendoStop(!0,!0).kendoAnimate(h({init:function(){d.trigger(v,{item:b[0],contentElement:p[0]})}},f,{complete:function(){b.removeAttr("data-animating")}}))},t=function(){q?d.ajaxRequest(b,p,function(){s(),d.trigger("change")}):(s(),d.trigger("change"))};o.removeClass(G),o.length?o.kendoStop(!0,!0).kendoAnimate(h({complete:t},g)):t();return!0},contentElement:function(a){if(!isNaN(a-0)){var b=this.contentElements,c=new RegExp("-"+(a+1)+"$");for(var d=0,e=b.length;d<e;d++)if(c.test(b.closest(".k-content")[d].id))return b[d]}},ajaxRequest:function(b,c,d,e){b=this.tabGroup.find(b);if(!b.find(".k-loading").length){var f=this,g=b.find("."+n),h={},i=null,j=setTimeout(function(){i=a("<span class='k-icon k-loading'/>").prependTo(g)},100);b.attr("data-in-request",!0),a.ajax({type:"GET",cache:!1,url:e||g.data(x)||g.attr(m),dataType:"html",data:h,error:function(a,c){b.removeAttr("data-animating"),f.trigger("error",{xhr:a,status:c})&&this.complete()},complete:function(){b.removeAttr("data-in-request"),clearTimeout(j),i!==null&&i.remove()},success:function(a,e){c.html(a),d&&d.call(f,c),f.trigger(A,{item:b[0],contentElement:c[0]})}})}}});h(N,{renderItem:function(a){a=h({tabStrip:{},group:{}},a);var b=J.empty,c=a.item;return J.item(h(a,{image:c.imageUrl?J.image:b,sprite:c.spriteCssClass?J.sprite:b,itemWrapper:J.itemWrapper},K))},renderContent:function(a){return J.content(h(a,K))}}),c.ui.plugin(N)}(jQuery),function(a,b){function K(a){a.preventDefault()}function J(a){var b=a.parseFormats;a.format=f(a.format||c.getCulture(a.culture).calendars.standard.patterns.t),b=y(b)?b:[b],b.splice(0,0,a.format),a.parseFormats=b}function H(a,b,c){var d=G(b),e=G(c),f;if(!a||d==e)return!0;f=G(a),d>f&&(f+=v),e<d&&(e+=v);return f>=d&&f<=e}function G(a){return a.getHours()*60*u+a.getMinutes()*u+a.getSeconds()*1e3+a.getMilliseconds()}function F(){var a=new B,b=new B(a.getFullYear(),a.getMonth(),a.getDate(),0,0,0),c=new B(a.getFullYear(),a.getMonth(),a.getDate(),12,0,0);return-1*(b.getTimezoneOffset()-c.getTimezoneOffset())}function E(a,b,c){var d=a.getTimezoneOffset(),e;a.setTime(a.getTime()+b),c||(e=a.getTimezoneOffset()-d,a.setTime(a.getTime()+e*u))}var c=window.kendo,d=c.support.touch,e=c.keys,f=c._extractFormat,g=c.ui,h=g.Widget,i="open",j="close",k="change",l=d?"touchend":"click",m="k-state-default",n="disabled",o="li",p="<span/>",q="k-state-focused",r="k-state-hover",s="mouseenter mouseleave",t="mousedown",u=6e4,v=864e5,w="k-state-selected",x="k-state-disabled",y=a.isArray,z=a.extend,A=a.proxy,B=Date,C=new B;C=new B(C.getFullYear(),C.getMonth(),C.getDate(),0,0,0);var D=function(b){var d=this;d.options=b,d.ul=a('<ul unselectable="on" class="k-list k-reset"/>').css({overflow:c.support.touch?"":"auto"}).delegate(o,l,A(d._click,d)).delegate(o,"mouseenter",function(){a(this).addClass(r)}).delegate(o,"mouseleave",function(){a(this).removeClass(r)}),d.list=a("<div class='k-list-container'/>").append(d.ul).mousedown(K),d._popup(),d.template=c.template('<li class="k-item" unselectable="on">#=data#</li>',{useWithBlock:!1})};D.prototype={current:function(c){var d=this;if(c!==b)d._current&&d._current.removeClass(w),c&&(c=a(c),c.addClass(w),d.scroll(c[0])),d._current=c;else return d._current},close:function(){this.popup.close()},open:function(){var a=this;a.ul[0].firstChild||a.bind(),a.popup.open(),a._current&&a.scroll(a._current[0])},dataBind:function(a){var b=this,d=b.options,e=d.format,f=c.toString,g=b.template,h=a.length,i=0,j,k="";for(;i<h;i++)j=a[i],H(j,d.min,d.max)&&(k+=g(f(j,e,d.culture)));b._html(k,h)},refresh:function(){var a=this,b=a.options,d=b.format,e=F(),f=e<0,g=b.min,h=b.max,i=G(g),j=G(h),k=b.interval*u,l=c.toString,m=a.template,n=new B(g),o=0,p,q="";f?p=(v+e*u)/k:p=v/k,i!=j&&(i>j&&(j+=v),p=(j-i)/k+1);for(;o<p;o++)o&&E(n,k,f),j&&G(n)>j&&(n=new B(h)),q+=m(l(n,d,b.culture));a._html(q,p)},bind:function(){var a=this,b=a.options.dates;b&&b[0]?a.dataBind(b):a.refresh()},_html:function(a,b){var c=this;c.ul[0].innerHTML=a,c._height(b),c.current(null),c.select(c._value)},scroll:function(a){if(!!a){var b=this.ul[0],c=a.offsetTop,d=a.offsetHeight,e=b.scrollTop,f=b.clientHeight,g=c+d;b.scrollTop=e>c?c:g>e+f?g-f:e}},select:function(b){var d=this,e=d.options,f=d._current;b instanceof Date&&(b=c.toString(b,e.format,e.culture)),typeof b=="string"&&(!f||f.text()!==b?(b=a.grep(d.ul[0].childNodes,function(a){return(a.textContent||a.innerText)==b}),b=b[0]?b:null):b=f),d.current(b)},toggle:function(){var a=this;a.popup.visible()?a.close():a.open()},value:function(a){var b=this;b._value=a,b.ul[0].firstChild&&b.select(a)},_click:function(b){var c=this,d=a(b.currentTarget);b.isDefaultPrevented()||(c.select(d),c.options.change(d.text(),!0),c.close())},_height:function(a){if(a){var b=this,c=b.list,d=c.parent(".k-animation-container"),e=b.options.height;c.add(d).show().height(b.ul[0].scrollHeight>e?e:"auto").hide()}},_parse:function(a){var b=this,d=b.options,e=b._value||C;if(a instanceof B)return a;a=c.parseDate(a,d.parseFormats,d.culture),a&&(a=new B(e.getFullYear(),e.getMonth(),e.getDate(),a.getHours(),a.getMinutes(),a.getSeconds(),a.getMilliseconds()));return a},_popup:function(){var a=this,b=a.list,d=a.options,e=d.anchor,f;a.popup=new g.Popup(b,z(d.popup,{anchor:e,open:d.open,close:d.close,animation:d.animation})),f=e.outerWidth()-(b.outerWidth()-b.width()),b.css({fontFamily:e.css("font-family"),width:f}),c.touchScroller(a.popup.element)},move:function(a){var b=this,c=a.keyCode,d=b.ul[0],f=b._current,g=c===e.DOWN;if(c===e.UP||g){if(a.altKey){b.toggle(g);return}g?f=f?f[0].nextSibling:d.firstChild:f=f?f[0].previousSibling:d.lastChild,f&&b.select(f),b.options.change(b._current.text()),a.preventDefault()}else if(c===e.ENTER||c===e.TAB||c===e.ESC)a.preventDefault(),f&&b.options.change(f.text(),!0),b.close()}},D.getMilliseconds=G,c.TimeView=D;var I=h.extend({init:function(a,b){var e=this;h.fn.init.call(e,a,b),a=e.element,b=e.options,J(b),e._wrapper(),e.timeView=new D(z({},b,{anchor:e.wrapper,format:b.format,change:function(b,c){c?e._change(b):a.val(b)},open:function(a){e.trigger(i)&&a.preventDefault()},close:function(a){e.trigger(j)&&a.preventDefault()}})),e._icon(),d||(a[0].type="text"),a.addClass("k-input").bind({keydown:A(e._keydown,e),focus:function(a){e._inputWrapper.addClass(q)},blur:A(e._blur,e)}).closest("form").bind("reset",function(){e.value(a[0].defaultValue)}),e.enable(!a.is("[disabled]")),e.value(b.value||a.val()),c.notify(e)},options:{name:"TimePicker",min:C,max:C,format:"",dates:[],parseFormats:[],value:null,interval:30,height:200,animation:{}},events:[i,j,k],setOptions:function(a){var b=this;h.fn.setOptions.call(b,a),J(b.options),z(b.timeView.options,b.options),b.timeView.ul[0].innerHTML=""},dataBind:function(a){y(a)&&this.timeView.dataBind(a)},enable:function(a){var b=this,c=b.element,d=b._arrow.unbind(l+" "+t),e=b._inputWrapper.unbind(s);a===!1?(e.removeClass(m).addClass(x),c.attr(n,n)):(e.removeClass(x).addClass(m).bind(s,b._toggleHover),c.removeAttr(n),d.bind(l,A(b._click,b)).bind(t,K))},close:function(){this.timeView.close()},open:function(){this.timeView.open()},min:function(a){return this._option("min",a)},max:function(a){return this._option("max",a)},value:function(a){var c=this;if(a===b)return c._value;c._old=c._update(a)},_blur:function(){var a=this;a.close(),a._change(a.element.val()),a._inputWrapper.removeClass(q)},_click:function(){var a=this,b=a.element;a.timeView.toggle(),!d&&b[0]!==document.activeElement&&b.focus()},_change:function(a){var b=this;a=b._update(a),+b._old!=+a&&(b._old=a,b.trigger(k),b.element.trigger(k))},_icon:function(){var b=this,c=b.element,d;d=c.next("span.k-select"),d[0]||(d=a('<span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-clock">select</span></span>').insertAfter(c)),b._arrow=d},_keydown:function(a){var b=this,c=a.keyCode,d=b.timeView;d.popup.visible()||a.altKey?d.move(a):c===e.ENTER&&b._change(b.element.val())},_option:function(a,c){var d=this,e=d.options;if(c===b)return e[a];c=d.timeView._parse(c);!c||(c=new B(c),e[a]=c,d.timeView.options[a]=c,d.timeView.bind())},_toggleHover:function(b){d||a(b.currentTarget).toggleClass(r,b.type==="mouseenter")},_update:function(a){var b=this,d=b.options,e=b.timeView._parse(a);H(e,d.min,d.max)||(e=null),b._value=e,b.element.val(e?c.toString(e,d.format,d.culture):a),b.timeView.value(e);return e},_wrapper:function(){var b=this,c=b.element,d;d=c.parents(".k-timepicker"),d[0]||(d=c.wrap(p).parent().addClass("k-picker-wrap k-state-default"),d=d.wrap(p).parent()),d[0].style.cssText=c[0].style.cssText,c.css({width:"100%",height:c[0].style.height}),b.wrapper=d.addClass("k-widget k-timepicker k-header"),b._inputWrapper=a(d[0].firstChild)}});g.plugin(I)}(jQuery),function(a,b){function K(a){var b=c.getCulture(a.culture).calendars.standard.patterns;a.format=g(a.format||b.g),a.timeFormat=g(a.timeFormat||b.t),c.DateView.normalize(a),a.parseFormats.splice(1,0,a.timeFormat)}function J(a){a.preventDefault()}var c=window.kendo,d=c.TimeView,e=c.support.touch,f=c.parseDate,g=c._extractFormat,h=c.calendar,i=h.isInRange,j=h.restrictValue,k=h.isEqualDatePart,l=d.getMilliseconds,m=c.ui,n=m.Widget,o="open",p="close",q="change",r=e?"touchend":"click",s="disabled",t="k-state-default",u="k-state-focused",v="k-state-hover",w="k-state-disabled",x="mouseenter mouseleave",y=e?"touchstart":"mousedown",z=r+" "+y,A="month",B="<span/>",C=Date,D=new C(1900,0,1),E=new C(2099,11,31),F={view:"date"},G={view:"time"},H=a.extend,I=n.extend({init:function(b,d){var f=this;n.fn.init.call(f,b,d),b=f.element,d=f.options,K(d),f._wrapper(),f._icons(),f._views(),e||(b[0].type="text"),b.addClass("k-input").bind({keydown:a.proxy(f._keydown,f),focus:function(){f._inputWrapper.addClass(u)},blur:function(){f._inputWrapper.removeClass(u),f._change(b.val()),f.close("date"),f.close("time")}}).closest("form").bind("reset",function(){f.value(b[0].defaultValue)}),f._midnight=l(d.min)+l(d.max)===0,f.enable(!b.is("[disabled]")),f.value(d.value||b.val()),c.notify(f)},options:{name:"DateTimePicker",value:null,format:"",timeFormat:"",culture:"",parseFormats:[],dates:[],min:new C(D),max:new C(E),interval:30,height:200,footer:"",start:A,depth:A,animation:{},month:{}},events:[o,p,q],setOptions:function(a){var b=this;n.fn.setOptions.call(b,a),K(b.options),H(b.dateView.options,b.options),H(b.timeView.options,b.options),b.timeView.ul[0].innerHTML=""},enable:function(a){var b=this,c=b._dateIcon.unbind(z),d=b._timeIcon.unbind(z),f=b._inputWrapper.unbind(x),g=b.element;a===!1?(f.removeClass(t).addClass(w),g.attr(s,s)):(f.addClass(t).removeClass(w).bind(x,b._toggleHover),g.removeAttr(s),c.bind({click:function(){b.toggle("date"),!e&&g[0]!==document.activeElement&&g.focus()},mousedown:J}),d.bind({click:function(){b.toggle("time"),!e&&g[0]!==document.activeElement&&g.focus()},mousedown:J}))},close:function(a){a!=="time"&&(a="date"),this[a+"View"].close()},open:function(a){a!=="time"&&(a="date"),this[a+"View"].open()},min:function(a){return this._option("min",a)},max:function(a){return this._option("max",a)},toggle:function(a){var b="timeView";a!=="time"?a="date":b="dateView",this[a+"View"].toggle(),this[b].close()},value:function(a){var c=this;if(a===b)return c._value;c._old=c._update(a)},_change:function(a){var b=this;a=b._update(a),+b._old!=+a&&(b._old=a,b.trigger(q),b.element.trigger(q))},_option:function(a,c){var d=this,e=d.options,g=d.timeView,h=g.options,i=d._value||d._old;if(c===b)return e[a];c=f(c,e.parseFormats,e.culture);if(!!c){e[a]=new C(c),d.dateView[a](c),d._midnight=l(e.min)+l(e.max)===0;if(i&&k(c,i)){if(d._midnight&&a=="max"){h[a]=E,g.dataBind([E]);return}h[a]=c}else h.max=E,h.min=D;g.bind()}},_toggleHover:function(b){e||a(b.currentTarget).toggleClass(v,b.type==="mouseenter")},_update:function(a){var b=this,d=b.options,e=d.min,g=d.max,h=b.timeView,l=f(a,d.parseFormats,d.culture),m,n,o,p;if(+l===+b._value)return l;l!==null&&k(l,e)?l=j(l,e,g):i(l,e,g)||(l=null),b._value=l,h.value(l),b.dateView.value(l),l&&(o=b._old,n=h.options,k(l,e)&&(n.min=e,n.max=E,m=!0),k(l,g)&&(b._midnight?(h.dataBind([E]),p=!0):(n.max=g,m||(n.min=D),m=!0)),!p&&(!o&&m||o&&!k(o,l))&&(m||(n.max=E,n.min=D),h.bind())),b.element.val(l?c.toString(l,d.format,d.culture):a);return l},_keydown:function(a){var b=this,d=b.dateView,e=b.timeView,f=d.popup.visible();a.altKey&&a.keyCode===c.keys.DOWN?b.toggle(f?"time":"date"):f?d.move(a):e.popup.visible()?e.move(a):a.keyCode===c.keys.ENTER&&b._change(b.element.val())},_views:function(){var a=this,b=a.options;a.dateView=new c.DateView(H({},b,{anchor:a.wrapper,change:function(){var c=this.value(),d=+c,e=+b.min,f=+b.max,g;if(d===e||d===f)g=new C(a._value),g.setFullYear(c.getFullYear()),g.setMonth(c.getMonth()),g.setDate(c.getDate()),i(g,e,f)&&(c=g);a._change(c),a.close("date")},close:function(b){a.trigger(p,F)&&b.preventDefault()},open:function(b){a.trigger(o,F)&&b.preventDefault()}})),a.timeView=new d({anchor:a.wrapper,animation:b.animation,dates:b.dates,format:b.timeFormat,culture:b.culture,height:b.height,interval:b.interval,min:new C(D),max:new C(E),parseFormats:b.parseFormats,value:b.value,change:function(d,e){d=a.timeView._parse(d),d<b.min?(d=new C(b.min),a.timeView.options.min=d):d>b.max&&(d=new C(b.max),a.timeView.options.max=d),e?(a._timeSelected=!0,a._change(d)):a.element.val(c.toString(d,b.format,b.culture))},close:function(b){a.trigger(p,G)&&b.preventDefault()},open:function(b){a.trigger(o,G)&&b.preventDefault()}})},_icons:function(){var b=this,c=b.element,d;d=c.next("span.k-select"),d[0]||(d=a('<span unselectable="on" class="k-select"><span unselectable="on" class="k-icon k-i-calendar">select</span><span unselectable="on" class="k-icon k-i-clock">select</span></span>').insertAfter(c),d=d.children()),b._dateIcon=d.eq(0),b._timeIcon=d.eq(1)},_wrapper:function(){var b=this,c=b.element,d;d=c.parents(".k-datetimepicker"),d[0]||(d=c.wrap(B).parent().addClass("k-picker-wrap k-state-default"),d=d.wrap(B).parent()),d[0].style.cssText=c[0].style.cssText,c.css({width:"100%",height:c[0].style.height}),b.wrapper=d.addClass("k-widget k-datetimepicker k-header"),b._inputWrapper=a(d[0].firstChild)}});m.plugin(I)}(jQuery),function(a,b){function K(a){var b=this;b.treeview=a,b._draggable=new d.Draggable(a.element,{filter:"div:not(.k-state-disabled) .k-in",hint:function(a){return z.dragClue({text:a.text()})},cursorOffset:{left:10,top:c.support.touch?-40/c.support.zoomLevel():10},dragstart:k(b.dragstart,b),dragcancel:k(b.dragcancel,b),drag:k(b.drag,b),dragend:k(b.dragend,b)})}function J(a,b,c){var d=a.children("div"),e=a.children("ul");a.hasClass("k-treeview")||(c=f({expanded:e.css("display")!="none",index:a.index(),enabled:!d.children(".k-in").hasClass("k-state-disabled")},c),b=f({firstLevel:a.parent().parent().hasClass(w),length:a.parent().children().length},b),a.removeClass("k-first k-last").addClass(A.wrapperCssClass(b,c)),d.removeClass("k-top k-mid k-bot").addClass(A.cssClass(b,c)),d.children(".k-in").removeClass("k-in k-state-default k-state-disabled").addClass(A.textClass(c)),e.length&&(d.children(".k-icon").removeClass("k-plus k-minus k-plus-disabled k-minus-disabled").addClass(A.toggleButtonClass(c)),e.addClass("k-group")))}function I(b){var c=b.children("div"),d=b.children("ul"),e=c.children(".k-icon"),f=c.children(".k-in"),g,h;if(!b.hasClass("k-treeview")){c.length||(c=a("<div />").prependTo(b));if(!e.length&&d.length)e=a("<span class='k-icon' />").prependTo(c);else if(!d.length||!d.children().length)e.remove(),d.remove();if(!f.length){f=a("<span class='k-in' />").appendTo(c)[0],g=c[0].nextSibling,f=c.find(".k-in")[0];while(g&&g.nodeName.toLowerCase()!="ul")h=g,g=g.nextSibling,h.nodeType==3&&(h.nodeValue=a.trim(h.nodeValue)),f.appendChild(h)}}}function H(b){return a(b).closest("[data-role=treeview]").data("kendoTreeView")}function G(a){return function(b){var c=b.children(".k-animation-container");c.length||(c=b);return c.children(a)}}var c=window.kendo,d=c.ui,e=c.data,f=a.extend,g=c.template,h=a.isArray,i=d.Widget,j=e.HierarchicalDataSource,k=a.proxy,l="select",m="expand",n="change",o="collapse",p="dragstart",q="drag",r="drop",s="dragend",t="click",u="visibility",v="k-state-hover",w="k-treeview",x=":visible",y=".k-item",z,A,B,C,D,E={text:"dataTextField",url:"dataUrlField",spriteCssClass:"dataSpriteCssClassField",imageUrl:"dataImageUrlField"},F=function(a){return typeof HTMLElement=="object"?a instanceof HTMLElement:a&&typeof a=="object"&&a.nodeType===1&&typeof a.nodeName=="string"};C=G(".k-group"),D=G(".k-group,.k-content"),z={dragClue:g("<div class='k-header k-drag-clue'><span class='k-icon k-drag-status'></span>#= text #</div>"),group:g("<ul class='#= r.groupCssClass(group) #'#= r.groupAttributes(group) #>#= renderItems(data) #</ul>")},B=i.extend({init:function(b,c){var d=this,e=".k-in:not(.k-state-selected,.k-state-disabled)",f="mouseenter",j,l=!1;h(c)&&(j=!0,c={dataSource:c}),c&&typeof c.loadOnDemand=="undefined"&&h(c.dataSource)&&(c.loadOnDemand=!1),i.prototype.init.call(d,b,c),b=d.element,c=d.options,l=b.is("ul")||b.hasClass(w),l&&(c.dataSource.list=b.is("ul")?b:b.children("ul")),d._animation(),d._accessors(),c.template&&typeof c.template=="string"?c.template=g(c.template):c.template||(c.template=d._textTemplate()),c.checkboxTemplate&&typeof c.checkboxTemplate=="string"&&(c.checkboxTemplate=g(c.checkboxTemplate)),d.templates={item:d._itemTemplate(),loading:d._loadingTemplate()},b.hasClass(w)?(d.wrapper=b,d.root=b.children("ul").eq(0)):(d._wrapper(),l&&(d.root=b,d._group(d.wrapper))),d._dataSource(l),d.wrapper.on(f,".k-in.k-state-selected",function(a){a.preventDefault()}).on(f,e,function(){a(this).addClass(v)}).on("mouseleave",e,function(){a(this).removeClass(v)}).on(t,e,k(d._nodeClick,d)).on("dblclick","div:not(.k-state-disabled) .k-in",k(d._toggleButtonClick,d)).on(t,".k-plus,.k-minus",k(d._toggleButtonClick,d)),c.dragAndDrop&&(d.dragging=new K(d)),l?d._attachUids():c.autoBind&&(d._progress(!0),d.dataSource.read())},_attachUids:function(b,d){var e=this,f,g=c.attr("uid");b=b||e.root,d=d||e.dataSource,f=d.view(),b.children("li").each(function(b,c){c=a(c).attr(g,f[b].uid),e._attachUids(c.children("ul"),f[b].children)})},_animation:function(){var a=this.options;a.animation===!1&&(a.animation={expand:{show:!0,effects:{}},collapse:{hide:!0,effects:{}}})},_dataSource:function(a){function e(a){for(var b=0;b<a.length;b++)a[b].children.read(),e(a[b].children.view())}var b=this,c=b.options,d=c.dataSource;d=h(d)?{data:d}:d,b.dataSource&&b._refreshHandler?b.dataSource.unbind(n,b._refreshHandler):b._refreshHandler=k(b.refresh,b),d.fields||(d.fields=[{field:"text"},{field:"url"},{field:"spriteCssClass"},{field:"imageUrl"}]),b.dataSource=j.create(d),a&&(b.dataSource.read(),e(b.dataSource.view())),b.dataSource.bind(n,b._refreshHandler)},events:[p,q,r,s,m,o,l],options:{name:"TreeView",dataSource:{},animation:{expand:{effects:"expand:vertical",duration:200,show:!0},collapse:{duration:100}},dragAndDrop:!1,autoBind:!0,loadOnDemand:!0},_accessors:function(){var a=this,b=a.options,d,e,f,g=a.element;for(d in E)e=b[E[d]],f=g.attr(c.attr(d+"-field")),f&&(e=f),e||(e=d),h(e)||(e=[e]),b[E[d]]=e},_fieldFor:function(a){var b=this.options[E[a]],c=b.length;return c===0?"'"+a+"'":c==1?"'"+b[0]+"'":"['"+b.join("','")+"']"+"[item.level() < "+c+" ? item.level() : "+(c-1)+"]"},_textTemplate:function(){var a=this,b=function(b){return"item["+a._fieldFor(b)+"]"},c="# var text = "+b("text")+"; #"+"# if (typeof item.encoded != 'undefined' && item.encoded === false) {#"+"#= text #"+"# } else { #"+"#: text #"+"# } #";return g(c)},_loadingTemplate:function(){return g("<div class='k-icon k-loading' /> Loading...")},_itemTemplate:function(){var a=this,b=function(b){return"item["+a._fieldFor(b)+"]"},d="<li class='#= r.wrapperCssClass(group, item) #' "+c.attr("uid")+"='#= item.uid #'"+">"+"<div class='#= r.cssClass(group, item) #'>"+"# if (item.hasChildren) { #"+"<span class='#= r.toggleButtonClass(item) #'></span>"+"# } #"+"# if (treeview.checkboxTemplate) { #"+"<span class='k-checkbox'>"+"#= treeview.checkboxTemplate(data) #"+"</span>"+"# } #"+"# var url = "+b("url")+"; #"+"# var tag = url ? 'a' : 'span'; #"+"# var textAttr = url ? ' href=\\'' + url + '\\'' : ''; #"+"<#=tag# class='#= r.textClass(item) #'#= textAttr #>"+"# var imageUrl = "+b("imageUrl")+"; #"+"# if (imageUrl) { #"+"<img class='k-image' alt='' src='#= imageUrl #'>"+"# } #"+"# var spriteCssClass = "+b("spriteCssClass")+"; #"+"# if (spriteCssClass) { #"+"<span class='k-sprite #= spriteCssClass #'></span>"+"# } #"+"#= treeview.template(data) #"+"</#=tag#>"+"</div>"+"</li>";return g(d)},setOptions:function(a){var b=this;"dragAndDrop"in a&&a.dragAndDrop&&!b.options.dragAndDrop&&(b.dragging=new K(b)),i.fn.setOptions.call(b,a),b._animation()},_trigger:function(a,b){return this.trigger(a,{node:b.closest(y)[0]})},_toggleButtonClick:function(b){this.toggle(a(b.target).closest(y))},_nodeClick:function(b){var c=this,d=a(b.target),e=D(d.closest(y)),f=d.attr("href"),g;f?g=f=="#"||f.indexOf("#"+this.element.id+"-")>=0:g=e.length&&!e.children().length,g&&b.preventDefault(),!d.hasClass(".k-state-selected")&&!c._trigger("select",d)&&c.select(d)},_wrapper:function(){var a=this,b=a.element,c,d,e="k-widget k-treeview k-reset";b.is("div")?(c=b,d=c.children("ul").eq(0)):(c=b.wrap("<div />").parent(),d=b),a.wrapper=c.addClass(e),a.root=d},_group:function(a){var b=this,c=a.hasClass(w),d={firstLevel:c,expanded:c||b._expanded(a)},e=a.children("ul");e.addClass(A.groupCssClass(d)).css("display",d.expanded?"":"none"),b._nodes(e,d)},_nodes:function(b,c){var d=this,e=b.children("li"),g;c=f({length:e.length},c),e.each(function(b,e){e=a(e),g={index:b,expanded:d._expanded(e)},I(e),J(e,c,g),d._group(e)})},_processNodes:function(b,c){var d=this;d.element.find(b).each(function(b,e){c.call(d,b,a(e).closest(y))})},dataItem:function(b){var d=a(b).closest(y).attr(c.attr("uid")),e=this.dataSource;return e&&e.getByUid(d)},_insertNode:function(b,c,d,e,f){var g=this,h=C(d),i=h.children().length+1,j,k={firstLevel:d.hasClass(w),expanded:!f,length:i},l,m,n,o="",p=function(a,b){a.appendTo(b)};for(m=0;m<b.length;m++)n=b[m],n.index=c+m,o+=g._renderItem({group:k,item:n});l=a(o);if(!!l.length){h.length||(h=a(g._renderGroup({group:k})).appendTo(d)),e(l,h),d.hasClass("k-item")&&(I(d),J(d)),J(l.prev()),J(l.next());for(m=0;m<b.length;m++)n=b[m],j=n.children.data(),j.length&&g._insertNode(j,n.index,l.eq(m),p,!g._expanded(l.eq(m)));return l}},refresh:function(a){function j(a,c,d){var e=C(c),f=e.children();typeof g=="undefined"&&(g=f.length),b._insertNode(a,g,c,function(a,b){g==f.length?a.appendTo(b):a.insertBefore(f.eq(g))},d)}var b=this,c=b.wrapper,d=a.node,e=a.action,f=a.items,g=a.index,h=b.options.loadOnDemand,i;if(!a.field){d&&(c=b.findByUid(d.uid),b._progress(c,!1)),e=="add"?j(f,c):e=="remove"?b._remove(b.findByUid(f[0].uid),!1):d?(C(c).empty(),j(f,c,!0),b._expanded(c)&&(J(c,{},{expanded:!0}),C(c).css("display","block"))):b.root=b.wrapper.html(b._renderGroup({items:f,group:{firstLevel:!0,expanded:!0}})).children("ul");if(!h)for(i=0;i<f.length;i++)f[i].load()}},expand:function(a){this._processNodes(a,function(a,b){var c=D(b);c.length>0&&!c.is(x)&&this.toggle(b)})},collapse:function(a){this._processNodes(a,function(a,b){var c=D(b);c.length>0&&c.is(x)&&this.toggle(b)})},enable:function(a,b){b=arguments.length==2?!!b:!0,this._processNodes(a,function(a,c){var d=!D(c).is(x);b||(this.collapse(c),d=!0),J(c,{},{enabled:b,expanded:!d})})},select:function(b){var c=this.element;if(!arguments.length)return c.find(".k-state-selected").closest(y);b=a(b,c).closest(y),b.length&&(c.find(".k-in").removeClass("k-state-hover k-state-selected"),b.find(".k-in:first").addClass("k-state-selected"))},toggle:function(b){b=a(b);if(!!b.find(">div>.k-icon").is(".k-minus,.k-plus,.k-minus-disabled,.k-plus-disabled")){var c=this,d=D(b),e=!d.is(x),g=c.options,h=g.animation||{},i=h.expand,j=f({},h.collapse),k=j&&"effects"in j,l=c.dataItem(b);if(d.data("animating"))return;e||(i=f(k?j:f({reverse:!0},i),{show:!1,hide:!0})),c._trigger(e?"expand":"collapse",b)||(c._expanded(b,e),d.children().length>0?(J(b,{},{expanded:e}),e||d.css("height",d.height()).css("height"),d.kendoStop(!0,!0).kendoAnimate(f(i,{complete:function(){e&&d.css("height","")}}))):l&&(g.loadOnDemand&&c._progress(b,!0),l.load()))}},_expanded:function(a,b){var d=c.attr("expanded"),e=this.dataItem(a);if(arguments.length==1)return a.attr(d)==="true"||e&&e.expanded;e&&e.set("expanded",b),b?a.attr(d,"true"):a.removeAttr(d)},_progress:function(a,b){var c=this.element;arguments.length==1?(b=a,b?c.html(this.templates.loading):c.empty()):a.find("> div > .k-icon").toggleClass("k-loading",b)},text:function(b){return a(b).closest(y).find(">div>.k-in").text()},_dataSourceMove:function(b,d,f,g){var i=this,j=H(b),k,l,m,n,o=H(f||d),p=o.dataSource;f&&(m=o.dataItem(f),m.loaded()||(m.load(),i._expanded(f,!0)),f!=i.root&&(p=m.children));if(b instanceof a||F(b))b=a(b),k=j.dataSource,l=k.getByUid(b.attr(c.attr("uid"))),l=k.remove(l),l=g(p,l);else if(h(b)||b instanceof e.ObservableArray)for(n=0;n<b.length;n++)l=g(p,b[n]);else l=g(p,b);return i.findByUid(l.uid)},insertAfter:function(a,b){var c=b.parent(),d;c.parent().is("li")&&(d=c.parent());return this._dataSourceMove(a,c,d,function(a,c){return a.insert(b.index()+1,c)})},insertBefore:function(a,b){var c=b.parent(),d;c.parent().is("li")&&(d=c.parent());return this._dataSourceMove(a,c,d,function(a,c){return a.insert(b.index(),c)})},append:function(a,b){var c=this,d=c.root;b&&(d=C(b));return c._dataSourceMove(a,d,b,function(a,d){b&&c._expanded(b,!0);return a.add(d)})},_remove:function(b,c){var d,e,f;b=a(b,this.element),d=b.parent().parent(),e=b.prev(),f=b.next(),b[c?"detach":"remove"](),d.hasClass("k-item")&&(I(d),J(d)),J(e),J(f);return b},remove:function(a){var b=this.dataItem(a);b&&this.dataSource.remove(b)},detach:function(a){return this._remove(a,!0)},findByText:function(b){return a(this.element).find(".k-in").filter(function(c,d){return a(d).text()==b}).closest(y)},findByUid:function(a){return this.element.find(".k-item["+c.attr("uid")+"="+a+"]")},_renderItem:function(a){a.group||(a.group={}),a.treeview=this.options,a.r=A;return this.templates.item(a)},_renderGroup:function(a){var b=this;a.renderItems=function(a){var c="",d=0,e=a.items,f=e?e.length:0,g=a.group;g.length=f;for(;d<f;d++)a.group=g,a.item=e[d],a.item.index=d,c+=b._renderItem(a);return c},a.r=A;return z.group(a)}}),K.prototype={_hintStatus:function(b){var c=this._draggable.hint.find(".k-drag-status")[0];if(b)c.className="k-icon k-drag-status "+b;else return a.trim(c.className.replace(/k-(icon|drag-status)/g,""))},dragstart:function(b){var c=this,d=c.treeview,e=c.sourceNode=b.currentTarget.closest(y);d.trigger(p,{sourceNode:e[0]})&&b.preventDefault(),c.dropHint=a("<div class='k-drop-hint' />").css(u,"hidden").appendTo(d.element)},drag:function(b){var d=this,e=d.treeview,f=d.sourceNode,g=d.dropTarget=a(c.eventTarget(b)),h,i,j,k,l,m,n,o,p,r;g.closest(".k-treeview").length?a.contains(f[0],g[0])?h="k-denied":(h="k-insert-middle",d.dropHint.css(u,"visible"),i=g.closest(".k-top,.k-mid,.k-bot"),i.length>0?(k=i.outerHeight(),l=i.offset().top,m=g.closest(".k-in"),n=k/(m.length>0?4:2),o=b.pageY<l+n,p=l+k-n<b.pageY,r=m.length>0&&!o&&!p,m.toggleClass(v,r),d.dropHint.css(u,r?"hidden":"visible"),r?h="k-add":(j=i.position(),j.top+=o?0:k,d.dropHint.css(j)[o?"prependTo":"appendTo"](g.closest(y).children("div:first")),o&&i.hasClass("k-top")&&(h="k-insert-top"),p&&i.hasClass("k-bot")&&(h="k-insert-bottom"))):g[0]!=d.dropHint[0]&&(h="k-denied")):h="k-denied",e.trigger(q,{sourceNode:f[0],dropTarget:g[0],pageY:b.pageY,pageX:b.pageX,statusClass:h.substring(2),setStatusClass:function(a){h=a}}),h.indexOf("k-insert")!==0&&d.dropHint.css(u,"hidden"),d._hintStatus(h)},dragcancel:function(a){this.dropHint.remove()},dragend:function(a){var b=this,c=b.treeview,d="over",e=b.sourceNode,f,g=b.dropHint,h,i;g.css(u)=="visible"?(d=g.prevAll(".k-in").length>0?"after":"before",f=g.closest(y)):b.dropTarget&&(f=b.dropTarget.closest(y)),h=b._hintStatus()!="k-denied",i=c.trigger(r,{sourceNode:e[0],destinationNode:f[0],valid:h,setValid:function(a){h=a},dropTarget:a.target,dropPosition:d}),g.remove();!h||i?b._draggable.dropped=h:(b._draggable.dropped=!0,d=="over"?(c.append(e,f),c.expand(f)):d=="before"?c.insertBefore(e,f):d=="after"&&c.insertAfter(e,f),c.trigger(s,{sourceNode:e[0],destinationNode:f[0],dropPosition:d}))}},A={wrapperCssClass:function(a,b){var c="k-item",d=b.index;a.firstLevel&&d===0&&(c+=" k-first"),d==a.length-1&&(c+=" k-last");return c},cssClass:function(a,b){var c="",d=b.index,e=a.length-1;a.firstLevel&&d===0&&(c+="k-top "),d===0&&d!=e?c+="k-top":d==e?c+="k-bot":c+="k-mid";return c},textClass:function(a){var b="k-in";a.enabled===!1&&(b+=" k-state-disabled"),a.selected===!0&&(b+=" k-state-selected");return b},toggleButtonClass:function(a){var b="k-icon";a.expanded!==!0?b+=" k-plus":b+=" k-minus",a.enabled===!1&&(b+="-disabled");return b},groupAttributes:function(a){return a.expanded!==!0?" style='display:none'":""},groupCssClass:function(a){var b="k-group";a.firstLevel&&(b+=" k-treeview-lines");return b}},d.plugin(B)}(jQuery),function(a,b){function R(a){return typeof a!==F}function P(a,c){var d=h(a.getAttribute(c));d===null&&(d=b);return d}function O(a){a=parseFloat(a,10);var b=k.pow(10,D||0);return k.round(a*b)/b}function N(a){return(a+"").replace(".",c.cultures.current.numberFormat["."])}function M(a){return function(){return a}}function L(a){return function(b){return b+a}}function K(a){var b=a.is("input")?1:2;return"<div class='k-slider-track'><div class='k-slider-selection'><!-- --></div><a href='#' class='k-draghandle' title='Drag'>Drag</a>"+(b>1?"<a href='#' class='k-draghandle' title='Drag'>Drag</a>":"")+"</div>"}function J(a,b){var c="<ul class='k-reset k-slider-items'>",d=k.floor(O(b/a.smallStep))+1,e;for(e=0;e<d;e++)c+="<li class='k-tick'>&nbsp;</li>";c+="</ul>";return c}function I(a,b,c){var d="";b=="increase"?d=c?"k-i-arrow-e":"k-i-arrow-n":d=c?"k-i-arrow-w":"k-i-arrow-s";return"<a class='k-button k-button-"+b+"'><span class='k-icon "+d+"' title='"+a[b+"ButtonTitle"]+"'>"+a[b+"ButtonTitle"]+"</span></a>"}function H(a,b,c){var d=c?" k-slider-horizontal":" k-slider-vertical",e=a.style?a.style:b.attr("style"),f=b.attr("class")?" "+b.attr("class"):"",g="";a.tickPlacement=="bottomRight"?g=" k-slider-bottomright":a.tickPlacement=="topLeft"&&(g=" k-slider-topleft"),e=e?" style='"+e+"'":"";return"<div class='k-widget k-slider"+d+f+"'"+e+">"+"<div class='k-slider-wrap"+(a.showButtons?" k-slider-buttons":"")+g+"'></div></div>"}var c=window.kendo,d=c.ui.Widget,e=c.ui.Draggable,f=a.extend,g=c.format,h=c.parseFloat,i=a.proxy,j=a.isArray,k=Math,l=c.support,m=l.touch,n=l.pointers,o="change",p="slide",q=m?"touchstart":"mousedown",r=m?"touchstart":n?"MSPointerDown":"mousedown",s=m?"touchend":"mouseup",t="moveSelection",u="keydown",v="click",w="mouseover",x=".k-draghandle",y=".k-slider-track",z=".k-tick",A="k-state-selected",B="k-state-default",C="k-state-disabled",D=3,E="disabled",F="undefined",G=d.extend({init:function(a,b){var e=this;d.fn.init.call(e,a,b),b=e.options,e._distance=b.max-b.min,e._isHorizontal=b.orientation=="horizontal",e._position=e._isHorizontal?"left":"bottom",e._size=e._isHorizontal?"width":"height",e._outerSize=e._isHorizontal?"outerWidth":"outerHeight",b.tooltip.format=b.tooltip.enabled?b.tooltip.format||"{0}":"{0}",e._createHtml(),e.wrapper=e.element.closest(".k-slider"),e._trackDiv=e.wrapper.find(y),e._setTrackDivWidth(),e._maxSelection=e._trackDiv[e._size]();var f=e._maxSelection/((b.max-b.min)/b.smallStep),g=e._calculateItemsWidth(k.floor(e._distance/b.smallStep));b.tickPlacement!="none"&&f>=2&&(e._trackDiv.before(J(b,e._distance)),e._setItemsWidth(g),e._setItemsTitle(),e._setItemsLargeTick()),e._calculateSteps(g),e[b.enabled?"enable":"disable"](),e._keyMap={37:L(-b.smallStep),40:L(-b.smallStep),39:L(+b.smallStep),38:L(+b.smallStep),35:M(b.max),36:M(b.min),33:L(+b.largeStep),34:L(-b.largeStep)},c.notify(e)},events:[o,p],options:{enabled:!0,min:0,max:10,smallStep:1,largeStep:5,orientation:"horizontal",tickPlacement:"both",tooltip:{enabled:!0,format:"{0}"}},_setTrackDivWidth:function(){var a=this,b=parseFloat(a._trackDiv.css(a._position),10)*2;a._trackDiv[a._size](a.wrapper[a._size]()-2-b)},_setItemsWidth:function(b){var c=this,d=c.options,e=0,f=b.length-1,g=c.wrapper.find(z),h,i=0,j=2,k=g.length,l=0;for(h=0;h<k-2;h++)a(g[h+1])[c._size](b[h]);c._isHorizontal?(a(g[e]).addClass("k-first")[c._size](b[f-1]),a(g[f]).addClass("k-last")[c._size](b[f])):(a(g[f]).addClass("k-first")[c._size](b[f]),a(g[e]).addClass("k-last")[c._size](b[f-1]));if(c._distance%d.smallStep!==0&&!c._isHorizontal){for(h=0;h<b.length;h++)l+=b[h];i=c._maxSelection-l,i+=parseFloat(c._trackDiv.css(c._position),10)+j,c.wrapper.find(".k-slider-items").css("padding-top",i)}},_setItemsTitle:function(){var b=this,c=b.options,d=b.wrapper.find(z),e=c.min,f=d.length,h=b._isHorizontal?0:f-1,i=b._isHorizontal?f:-1,j=b._isHorizontal?1:-1;for(;h-i!==0;h+=j)a(d[h]).attr("title",g(c.tooltip.format,O(e))),e+=c.smallStep},_setItemsLargeTick:function(){var b=this,c=b.options,d,e=b.wrapper.find(z),f={},g=O(c.largeStep/c.smallStep);if(1e3*c.largeStep%(1e3*c.smallStep)===0)if(b._isHorizontal)for(d=0;d<e.length;d=O(d+g))f=a(e[d]),f.addClass("k-tick-large").html("<span class='k-label'>"+f.attr("title")+"</span>");else for(d=e.length-1;d>=0;d=O(d-g))f=a(e[d]),f.addClass("k-tick-large").html("<span class='k-label'>"+f.attr("title")+"</span>"),d!==0&&d!==e.length-1&&f.css("line-height",f[b._size]()+"px")},_calculateItemsWidth:function(a){var b=this,c=b.options,d=parseFloat(b._trackDiv.css(b._size))+1,e=d/b._distance,f,g,h;b._distance/c.smallStep-k.floor(b._distance/c.smallStep)>0&&(d-=b._distance%c.smallStep*e),f=d/a,g=[];for(h=0;h<a-1;h++)g[h]=f;g[a-1]=g[a]=f/2;return b._roundWidths(g)},_roundWidths:function(a){var b=0,c=a.length,d;for(d=0;d<c;d++)b+=a[d]-k.floor(a[d]),a[d]=k.floor(a[d]);b=k.round(b);return this._addAdditionalSize(b,a)},_addAdditionalSize:function(a,b){if(a===0)return b;var c=parseFloat(b.length-1)/parseFloat(a==1?a:a-1),d;for(d=0;d<a;d++)b[parseInt(k.round(c*d),10)]+=1;return b},_calculateSteps:function(a){var b=this,c=b.options,d=c.min,e=0,f=k.ceil(b._distance/c.smallStep),g=1,h;f+=b._distance/c.smallStep%1===0?1:0,a.splice(0,0,a[f-2]*2),a.splice(f-1,1,a.pop()*2),b._pixelSteps=[e],b._values=[d];if(f!==0){while(g<f)e+=(a[g-1]+a[g])/2,b._pixelSteps[g]=e,b._values[g]=d+=c.smallStep,g++;h=b._distance%c.smallStep===0?f-1:f,b._pixelSteps[h]=b._maxSelection,b._values[h]=c.max}},_getValueFromPosition:function(a,b){var c=this,d=c.options,e=k.max(d.smallStep*(c._maxSelection/c._distance),0),f=0,g=e/2,h;c._isHorizontal?f=a-b.startPoint:f=b.startPoint-a;if(c._maxSelection-(parseInt(c._maxSelection%e,10)-3)/2<f)return d.max;for(h=0;h<c._pixelSteps.length;h++)if(k.abs(c._pixelSteps[h]-f)-1<=g)return O(c._values[h])},_getDragableArea:function(){var a=this,b=a._trackDiv.offset().left,c=a._trackDiv.offset().top;return{startPoint:a._isHorizontal?b:c+a._maxSelection,endPoint:a._isHorizontal?b+a._maxSelection:c}},_createHtml:function(){var a=this,b=a.element,c=a.options,d=b.find("input");d.length==2?(d.eq(0).val(c.selectionStart),d.eq(1).val(c.selectionEnd)):b.val(c.value),b.wrap(H(c,b,a._isHorizontal)).hide(),c.showButtons&&b.before(I(c,"increase",a._isHorizontal)).before(I(c,"decrease",a._isHorizontal)),b.before(K(b))}}),Q=function(a){return{idx:0,x:a.pageX,y:a.pageY}};l.pointers&&(Q=function(a){return{idx:0,x:a.originalEvent.clientX,y:a.originalEvent.clientY}}),l.touch&&(Q=function(b,c){var d=b.changedTouches||b.originalEvent.changedTouches;if(c){var e=null;a.each(d,function(a,b){c==b.identifier&&(e={idx:b.identifier,x:b.pageX,y:b.pageY})});return e}return{idx:d[0].identifier,x:d[0].pageX,y:d[0].pageY}});var S=G.extend({init:function(c,d){var e=this,g;c.type="text",d=f({},{value:P(c,"value"),min:P(c,"min"),max:P(c,"max"),smallStep:P(c,"step")},d),c=a(c),d&&d.enabled===b&&(d.enabled=!c.is("[disabled]")),G.fn.init.call(e,c,d),d=e.options,R(d.value)||(d.value=d.min,c.val(d.min)),g=e.wrapper.find(x),new S.Selection(g,e,d),e._drag=new S.Drag(g,"",e,d)},options:{name:"Slider",showButtons:!0,increaseButtonTitle:"Increase",decreaseButtonTitle:"Decrease",tooltip:{format:"{0}"}},enable:function(b){var d=this,e=d.options,f,g;d.disable();if(b!==!1){d.wrapper.removeClass(C).addClass(B),d.wrapper.find("input").removeAttr(E),f=function(b){var c=Q(b),e=d._isHorizontal?c.x:c.y,f=d._getDragableArea(),g=a(b.target);g.hasClass("k-draghandle")?g.addClass(A):(d._update(d._getValueFromPosition(e,f)),d._drag.dragstart(b))},d.wrapper.find(z+", "+y).bind(r,f).end().bind(r,function(){a(document.documentElement).one("selectstart",c.preventDefault)}),d.wrapper.find(x).bind(s,function(b){a(b.target).removeClass(A)}).bind(v,function(a){a.preventDefault()}),g=i(function(a){d._setValueInRange(d._nextValueByIndex(d._valueIndex+a*1))},d);if(e.showButtons){var h=i(function(a,b){if(a.which===1||m&&a.which===0)g(b),this.timeout=setTimeout(i(function(){this.timer=setInterval(function(){g(b)},60)},this),200)},d);d.wrapper.find(".k-button").bind(s,i(function(a){this._clearTimer()},d)).bind(w,function(b){a(b.currentTarget).addClass("k-state-hover")}).bind("mouseout",i(function(b){a(b.currentTarget).removeClass("k-state-hover"),this._clearTimer()},d)).eq(0).bind(q,i(function(a){h(a,1)},d)).click(!1).end().eq(1).bind(q,i(function(a){h(a,-1)},d)).click(!1)}d.wrapper.find(x).bind(u,i(this._keydown,d)),e.enabled=!0}},disable:function(){var b=this;b.wrapper.removeClass(B).addClass(C),a(b.element).attr(E,E),b.wrapper.find(".k-button").unbind(q).bind(q,c.preventDefault).unbind(s).bind(s,c.preventDefault).unbind("mouseleave").bind("mouseleave",c.preventDefault).unbind(w).bind(w,c.preventDefault),b.wrapper.find(z+", "+y).unbind(r),b.wrapper.find(x).unbind(s).unbind(u).unbind(v).bind(u,!1),b.options.enabled=!1},_update:function(a){var b=this,c=b.value()!=a;b.value(a),c&&b.trigger(o,{value:b.options.value})},value:function(a){var b=this,c=b.options;a=O(a);if(isNaN(a))return c.value;a>=c.min&&a<=c.max&&c.value!=a&&(b.element.attr("value",N(a)),c.value=a,b._refresh())},_refresh:function(){this.trigger(t,{value:this.options.value})},_clearTimer:function(a){clearTimeout(this.timeout),clearInterval(this.timer)},_keydown:function(a){var b=this;a.keyCode in b._keyMap&&(b._setValueInRange(b._keyMap[a.keyCode](b.options.value)),a.preventDefault())},_setValueInRange:function(a){var b=this,c=b.options;a=O(a);isNaN(a)?b._update(c.min):(a=k.max(k.min(a,c.max),c.min),b._update(a))},_nextValueByIndex:function(a){var b=this._values.length;return this._values[k.max(0,k.min(a,b-1))]}});S.Selection=function(a,b,c){function d(d){var e=d-c.min,f=b._valueIndex=k.ceil(O(e/c.smallStep)),g=parseInt(b._pixelSteps[f],10),h=b._trackDiv.find(".k-slider-selection"),i=parseInt(a[b._outerSize]()/2,10);h[b._size](g),a.css(b._position,g-i)}d(c.value),b.bind([o,p,t],function(a){d(parseFloat(a.value,10))})},S.Drag=function(a,b,c,d){var f=this;f.owner=c,f.options=d,f.dragHandle=a,f.dragHandleSize=a[c._outerSize](),f.type=b,f.draggable=new e(a,{threshold:0,dragstart:i(f._dragstart,f),drag:i(f.drag,f),dragend:i(f.dragend,f),dragcancel:i(f.dragcancel,f)}),a.click(!1)},S.Drag.prototype={dragstart:function(a){this.draggable.drag._start(a)},_dragstart:function(b){var d=this,e=d.owner,f=d.options,h=f.tooltip,i="",j,l,m;f.enabled?(e.element.unbind(w),d.dragHandle.addClass(A),d.dragableArea=e._getDragableArea(),d.step=k.max(f.smallStep*(e._maxSelection/e._distance),0),d.type?(d.selectionStart=f.selectionStart,d.selectionEnd=f.selectionEnd,e._setZIndex(d.type)):d.oldVal=d.val=f.value,h.enabled&&(h.template&&(j=d.tooltipTemplate=c.template(h.template)),d.tooltipDiv=a("<div class='k-widget k-tooltip'><!-- --></div>").appendTo(document.body),d.type?d.tooltipTemplate?i=j({selectionStart:d.selectionStart,selectionEnd:d.selectionEnd}):(l=g(h.format,d.selectionStart),m=g(h.format,d.selectionEnd),i=l+" - "+m):(d.tooltipInnerDiv="<div class='k-callout k-callout-"+(e._isHorizontal?"s":"e")+"'><!-- --></div>",d.tooltipTemplate?i=j({value:d.val}):i=g(h.format,d.val),i+=d.tooltipInnerDiv),d.tooltipDiv.html(i),d.moveTooltip())):b.preventDefault()},drag:function(a){var b=this,c=b.owner,d=b.options,e=a.x.location,f=a.y.location,h=b.dragableArea.startPoint,i=b.dragableArea.endPoint,j=d.tooltip,k="",l=b.tooltipTemplate,m,n,o;a.preventDefault(),c._isHorizontal?b.val=b.constrainValue(e,h,i,e>=i):b.val=b.constrainValue(f,i,h,f<=i),b.oldVal!=b.val&&(b.oldVal=b.val,b.type?(b.type=="firstHandle"?b.val<b.selectionEnd?b.selectionStart=b.val:b.selectionStart=b.selectionEnd=b.val:b.val>b.selectionStart?b.selectionEnd=b.val:b.selectionStart=b.selectionEnd=b.val,m={values:[b.selectionStart,b.selectionEnd],value:[b.selectionStart,b.selectionEnd]}):m={value:b.val},c.trigger(p,m),j.enabled&&(b.type?b.tooltipTemplate?k=l({selectionStart:b.selectionStart,selectionEnd:b.selectionEnd}):(n=g(j.format,b.selectionStart),o=g(j.format,b.selectionEnd),k=n+" - "+o):(b.tooltipTemplate?k=l({value:b.val}):k=g(j.format,b.val),k+=b.tooltipInnerDiv),b.tooltipDiv.html(k),b.moveTooltip()))},dragcancel:function(a){this.owner._refresh();return this._end()},dragend:function(a){var b=this,c=b.owner;b.type?c._update(b.selectionStart,b.selectionEnd):c._update(b.val);return b._end()},_end:function(){var a=this,b=a.owner;b.options.tooltip.enabled&&b.options.enabled&&a.tooltipDiv.remove(),a.dragHandle.removeClass(A),b.element.bind(w);return!1},moveTooltip:function(){var a=this,b=a.owner,c=0,d=0,e=a.dragHandle.offset(),f=4,g=a.tooltipDiv.find(".k-callout"),h,i,j;a.type?(h=b.wrapper.find(x),i=h.eq(0).offset(),j=h.eq(1).offset(),b._isHorizontal?(c=j.top,d=i.left+(j.left-i.left)/2):(c=i.top+(j.top-i.top)/2,d=j.left)):(c=e.top,d=e.left),b._isHorizontal?(d-=parseInt((a.tooltipDiv.outerWidth()-a.dragHandle[b._outerSize]())/2,10),c-=a.tooltipDiv.outerHeight()+g.height()+f):(c-=parseInt((a.tooltipDiv.outerHeight()-a.dragHandle[b._outerSize]())/2,10),d-=a.tooltipDiv.outerWidth()+g.width()+f),a.tooltipDiv.css({top:c,left:d})},constrainValue:function(a,b,c,d){var e=this,f=0;b<a&&a<c?f=e.owner._getValueFromPosition(a,e.dragableArea):d?f=e.options.max:f=e.options.min;return f}},c.ui.plugin(S);var T=G.extend({init:function(c,d){var e=this,g=a(c).find("input"),h=g.eq(0)[0],i=g.eq(1)[0];h.type="text",i.type="text",d=f({},{selectionStart:P(h,"value"),min:P(h,"min"),max:P(h,"max"),smallStep:P(h,"step")},{selectionEnd:P(i,"value"),min:P(i,"min"),max:P(i,"max"),smallStep:P(i,"step")},d),d&&d.enabled===b&&(d.enabled=!g.is("[disabled]")),G.fn.init.call(e,c,d),d=e.options,R(d.selectionStart)||(d.selectionStart=d.min,g.eq(0).val(d.min)),R(d.selectionEnd)||(d.selectionEnd=d.max,g.eq(1).val(d.max));var j=e.wrapper.find(x);new T.Selection(j,e,d),e._firstHandleDrag=new S.Drag(j.eq(0),"firstHandle",e,d),e._lastHandleDrag=new S.Drag(j.eq(1),"lastHandle",e,d)},options:{name:"RangeSlider",tooltip:{format:"{0}"}},enable:function(b){var d=this,e=d.options,f;d.disable();b!==!1&&(d.wrapper.removeClass(C).addClass(B),d.wrapper.find("input").removeAttr(E),f=function(b){var c=Q(b),f=d._isHorizontal?c.x:c.y,g=d._getDragableArea(),h=d._getValueFromPosition(f,g),i=a(b.target);i.hasClass("k-draghandle")?i.addClass(A):h<e.selectionStart?(d._setValueInRange(h,e.selectionEnd),d._firstHandleDrag.dragstart(b)):h>d.selectionEnd?(d._setValueInRange(e.selectionStart,h),d._lastHandleDrag.dragstart(b)):h-e.selectionStart<=e.selectionEnd-h?(d._setValueInRange(h,e.selectionEnd),d._firstHandleDrag.dragstart(b)):(d._setValueInRange(e.selectionStart,h),d._lastHandleDrag.dragstart(b))},d.wrapper.find(z+", "+y).bind(r,f).end().bind(r,function(){a(document.documentElement).one("selectstart",c.preventDefault)}),d.wrapper.find(x).bind(s,function(b){a(b.target).removeClass(A)}).bind(v,function(a){a.preventDefault()}),d.wrapper.find(x).eq(0).bind(u,i(function(a){this._keydown(a,"firstHandle")},d)).end().eq(1).bind(u,i(function(a){this._keydown(a,"lastHandle")},d)),d.options.enabled=!0)},disable:function(){var a=this;a.wrapper.removeClass(B).addClass(C),a.wrapper.find("input").attr(E,E),a.wrapper.find(z+", "+y).unbind(r),a.wrapper.find(x).unbind(s).unbind(u).unbind(v).bind(u,c.preventDefault),a.options.enabled=!1},_keydown:function(a,b){var c=this,d=c.options.selectionStart,e=c.options.selectionEnd;a.keyCode in c._keyMap&&(b=="firstHandle"?(d=c._keyMap[a.keyCode](d),d>e&&(e=d)):(e=c._keyMap[a.keyCode](e),d>e&&(d=e)),c._setValueInRange(d,e),a.preventDefault())},_update:function(a,b){var c=this,d=c.value(),e=d[0]!=a||d[1]!=b;c.value([a,b]),e&&c.trigger(o,{values:[a,b],value:[a,b]})},value:function(a){return a&&a.length?this._value(a[0],a[1]):this._value()},_value:function(a,b){var c=this,d=c.options,e=d.selectionStart,f=d.selectionEnd;if(isNaN(a)&&isNaN(b))return[e,f];a=O(a),b=O(b),a>=d.min&&a<=d.max&&b>=d.min&&b<=d.max&&a<=b&&(e!=a||f!=b)&&(c.element.find("input").eq(0).attr("value",N(a)).end().eq(1).attr("value",N(b)),d.selectionStart=a,d.selectionEnd=b,c._refresh())},values:function(a,b){return j(a)?this._value(a[0],a[1]):this._value(a,b)},_refresh:function(){var a=this,b=a.options;a.trigger(t,{values:[b.selectionStart,b.selectionEnd],value:[b.selectionStart,b.selectionEnd]}),b.selectionStart==b.max&&b.selectionEnd==b.max&&a._setZIndex("firstHandle")},_setValueInRange:function(a,b){var c=this.options;a=k.max(k.min(a,c.max),c.min),b=k.max(k.min(b,c.max),c.min),a==c.max&&b==c.max&&this._setZIndex("firstHandle"),this._update(k.min(a,b),k.max(a,b))},_setZIndex:function(b){this.wrapper.find(x).each(function(c){a(this).css("z-index",b=="firstHandle"?1-c:c)})}});T.Selection=function(a,b,c){function e(a,c){var d=0,e=0,f=b._trackDiv.find(".k-slider-selection");d=k.abs(a-c),e=a<c?a:c,f[b._size](d),f.css(b._position,e-1)}function d(d){d=d||[];var f=d[0]-c.min,g=d[1]-c.min,h=k.ceil(O(f/c.smallStep)),i=k.ceil(O(g/c.smallStep)),j=b._pixelSteps[h],l=b._pixelSteps[i],m=parseInt(a.eq(0)[b._outerSize]()/2,10);a.eq(0).css(b._position,j-m).end().eq(1).css(b._position,l-m),e(j,l)}d(b.value()),b.bind([o,p,t],function(a){d(a.values)})},c.ui.plugin(T)}(jQuery),function(a,b){function D(a){var b=this,d=a.orientation;b.owner=a,b._element=a.element,b.orientation=d,e(b,d===o?C:B),b._resizable=new c.ui.Resizable(a.element,{orientation:d,handle:a.element.children(".k-splitbar-draggable-"+d),hint:f(b._createHint,b),start:f(b._start,b),max:f(b._max,b),min:f(b._min,b),invalidClass:"k-restricted-size-"+d,resizeend:f(b._stop,b)})}function z(b,c){return function(d,e){var f=a(d).data(s);if(arguments.length==1)return f[b];f[b]=e;if(c){var g=this.element.data("kendoSplitter");g.trigger(m)}}}function y(a){return!w(a)&&!x(a)}function x(a){return h.test(a)}function w(a){return i.test(a)}var c=window.kendo,d=c.ui,e=a.extend,f=a.proxy,g=d.Widget,h=/^\d+(\.\d+)?px$/i,i=/^\d+(\.\d+)?%$/i,j="expand",k="collapse",l="contentLoad",m="resize",n="layoutChange",o="horizontal",p="vertical",q="mouseenter",r="click",s="pane",t="mouseleave",u="k-"+s,v="."+u,A=g.extend({init:function(b,c){var d=this,e=function(){d.trigger(m)};g.fn.init.call(d,b,c),d.wrapper=d.element,d.orientation=d.options.orientation.toLowerCase()!=p?o:p,d.bind(m,f(d._resize,d)),d._initPanes(),d._attachEvents(),a(window).resize(e),d.resizing=new D(d),d.element.triggerHandler("init.kendoSplitter")},events:[j,k,l,m,n],_attachEvents:function(){var b=this,c=b.options.orientation,d=".k-splitbar-draggable-"+c,e=".k-splitbar .k-icon:not(.k-resize-handle)",g=function(){b.trigger(m)};b.element.delegate(d,q,function(){a(this).addClass("k-splitbar-"+b.orientation+"-hover")}).delegate(d,t,function(){a(this).removeClass("k-splitbar-"+b.orientation+"-hover")}).delegate(d,"mousedown",function(){b._contentFrames(this).after("<div class='k-overlay' />")}).delegate(d,"mouseup",function(){b._contentFrames(this).next(".k-overlay").remove()}).delegate(e,q,function(){a(this).addClass("k-state-hover")}).delegate(e,t,function(){a(this).removeClass("k-state-hover")}).delegate(".k-splitbar .k-collapse-next, .k-splitbar .k-collapse-prev",r,b._arrowClick(k)).delegate(".k-splitbar .k-expand-next, .k-splitbar .k-expand-prev",r,b._arrowClick(j)).delegate(".k-splitbar","dblclick",f(b._dbclick,b)).parent().closest(".k-splitter").each(function(){var b=a(this),c=b.data("kendoSplitter");c?c.bind(m,g):b.one("init.kendoSplitter",function(){a(this).data("kendoSplitter").bind(m,g),g()})})},options:{name:"Splitter",orientation:o},_initPanes:function(){var b=this,c=b.options.panes||[];b.element.addClass("k-widget").addClass("k-splitter").children().addClass(u).each(function(d,e){var f=c&&c[d];e=a(e),e.data(s,f?f:{}).toggleClass("k-scrollable",f?f.scrollable!==!1:!0),b.ajaxRequest(e)}).end(),b.trigger(m)},ajaxRequest:function(b,d,e){b=a(b);var f=this,g=b.data(s);d=d||g.contentUrl,d&&(b.append("<span class='k-icon k-loading k-pane-loading' />"),c.isLocalUrl(d)?a.ajax({url:d,data:e||{},type:"GET",dataType:"html",success:function(a){b.html(a),f.trigger(l,{pane:b[0]})}}):b.removeClass("k-scrollable").html("<iframe src='"+d+"' frameborder='0' class='k-content-frame'>"+"This page requires frames in order to show content"+"</iframe>"))},_triggerAction:function(a,b){this.trigger(a,{pane:b[0]})||this[a](b[0])},_dbclick:function(b){var c=this,d=a(b.target),e;if(d.closest(".k-splitter")[0]==c.element[0]){e=d.children(".k-icon:not(.k-resize-handle)");if(e.length!==1)return;e.is(".k-collapse-prev")?c._triggerAction(k,d.prev()):e.is(".k-collapse-next")?c._triggerAction(k,d.next()):e.is(".k-expand-prev")?c._triggerAction(j,d.prev()):e.is(".k-expand-next")&&c._triggerAction(j,d.next())}},_arrowClick:function(b){var c=this;return function(d){var e=a(d.target),f;e.closest(".k-splitter")[0]==c.element[0]&&(e.is(".k-"+b+"-prev")?f=e.parent().prev():f=e.parent().next(),c._triggerAction(b,f))}},_updateSplitBar:function(a,b,c){var d=function(a,b){return b?"<div class='k-icon "+a+"' />":""},e=this.orientation,f=b.resizable!==!1&&c.resizable!==!1,g=b.collapsible,h=b.collapsed,i=c.collapsible,j=c.collapsed;a.addClass("k-splitbar k-state-default k-splitbar-"+e).removeClass("k-splitbar-"+e+"-hover").toggleClass("k-splitbar-draggable-"+e,f&&!h&&!j).toggleClass("k-splitbar-static-"+e,!f&&!g&&!i).html(d("k-collapse-prev",g&&!h&&!j)+d("k-expand-prev",g&&h&&!j)+d("k-resize-handle",f)+d("k-collapse-next",i&&!j&&!h)+d("k-expand-next",i&&j&&!h))},_updateSplitBars:function(){var b=this;this.element.children(".k-splitbar").each(function(){var c=a(this),d=c.prev(v).data(s),e=c.next(v).data(s);!e||b._updateSplitBar(c,d,e)})},_contentFrames:function(b){return a(b).siblings(v).find("> .k-content-frame")},_resize:function(){var b=this,c=b.element,d=c.children(":not(.k-splitbar)"),e=b.orientation==o,f=c.children(".k-splitbar"),g=f.length,h=e?"width":"height",i=c[h]();g===0?(g=d.length-1,d.slice(0,g).after("<div class='k-splitbar' />"),b._updateSplitBars(),f=c.children(".k-splitbar")):b._updateSplitBars(),f.each(function(){i-=this[e?"offsetWidth":"offsetHeight"]});var j=0,k=0,l=a();d.css({position:"absolute",top:0})[h](function(){var b=a(this).data(s)||{},c;if(b.collapsed)c=0,a(this).css("overflow","hidden");else{if(y(b.size)){l=l.add(this);return}c=parseInt(b.size,10),w(b.size)&&(c=Math.floor(c*i/100))}k++,j+=c;return c}),i-=j;var m=l.length,p=Math.floor(i/m);l.slice(0,m-1).css(h,p).end().eq(m-1).css(h,i-(m-1)*p);var q=0,r=e?"height":"width",t=e?"left":"top",u=e?"offsetWidth":"offsetHeight";if(m===0){var v=d.filter(function(){return!(a(this).data(s)||{}).collapsed}).last();v[h](i+v[0][u])}c.children().css(r,c[r]()).each(function(a,b){b.style[t]=Math.floor(q)+"px",q+=b[u]}),b.trigger(n)},toggle:function(c,d){var e;c=a(c),e=c.data(s),arguments.length==1&&(d=e.collapsed===b?!1:e.collapsed),e.collapsed=!d,e.collapsed?c.css("overflow","hidden"):c.css("overflow",""),this.trigger(m)},collapse:function(a){this.toggle(a,!1)},expand:function(a){this.toggle(a,!0)},size:z("size",!0),min:z("min"),max:z("max")});d.plugin(A);var B={sizingProperty:"height",sizingDomProperty:"offsetHeight",alternateSizingProperty:"width",positioningProperty:"top",mousePositioningProperty:"pageY"},C={sizingProperty:"width",sizingDomProperty:"offsetWidth",alternateSizingProperty:"height",positioningProperty:"left",mousePositioningProperty:"pageX"};D.prototype={_createHint:function(b){var c=this;return a("<div class='k-ghost-splitbar k-ghost-splitbar-"+c.orientation+" k-state-default' />").css(c.alternateSizingProperty,b[c.alternateSizingProperty]())},_start:function(b){var c=this,d=a(b.currentTarget),e=d.prev(),f=d.next(),g=e.data(s),h=f.data(s),i=parseInt(e[0].style[c.positioningProperty],10),j=parseInt(f[0].style[c.positioningProperty],10)+f[0][c.sizingDomProperty]-d[0][c.sizingDomProperty],k=c._element.css(c.sizingProperty),l=function(a){var b=parseInt(a,10);return(x(a)?b:k*b/100)||0},m=l(g.min),n=l(g.max)||j-i,o=l(h.min),p=l(h.max)||j-i;c.previousPane=e,c.nextPane=f,c._maxPosition=Math.min(j-o,i+n),c._minPosition=Math.max(i+m,j-p)},_max:function(a){return this._maxPosition},_min:function(a){return this._minPosition},_stop:function(b){var d=this,e=a(b.currentTarget),f=d.owner;f._contentFrames(e).next(".k-overlay").remove();if(b.keyCode!==c.keys.ESC){var g=b.position,h=e.prev(),i=e.next(),j=h.data(s),k=i.data(s),l=g-parseInt(h[0].style[d.positioningProperty],10),n=parseInt(i[0].style[d.positioningProperty],10)+i[0][d.sizingDomProperty]-g-e[0][d.sizingDomProperty],o=d._element.children(v).filter(function(){return y(a(this).data(s).size)}).length;if(!y(j.size)||o>1)y(j.size)&&o--,j.size=l+"px";if(!y(k.size)||o>1)k.size=n+"px";f.trigger(m)}return!1}}}(jQuery),function(a,b){function F(){var c={},d=a("meta[name=csrf-token]").attr("content"),e=a("meta[name=csrf-param]").attr("content");a("input[name^='__RequestVerificationToken']").each(function(){c[this.name]=this.value}),e!==b&&d!==b&&(c[e]=d);return c}function E(b){return a(b.target).closest(".k-file")}function D(a){return a.children(".k-icon").is(".k-loading, .k-success, .k-fail")}function C(a,b,c){var d,e;a.bind("dragenter",function(a){b(),e=new Date,d||(d=setInterval(function(){var a=new Date-e;a>100&&(c(),clearInterval(d),d=null)},100))}).bind("dragover",function(a){e=new Date})}function B(a){a.stopPropagation(),a.preventDefault()}function A(b,c,d){var e=!1,f="";try{f=a.parseJSON(b),e=!0}catch(g){d()}e&&c(f)}function z(b,c,d){if(!!c._supportsRemove()){var f=b.data("fileNames"),g=a.map(f,function(a){return a.name});c._submitRemove(g,d,function(a,d,e){c._removeFileEntry(b),c.trigger(i,{operation:"remove",files:f,response:a,XMLHttpRequest:e})},function(a,b,b){var d=c.trigger(j,{operation:"remove",files:f,XMLHttpRequest:a});e("Server response: "+a.responseText)})}}function y(a){var b=a.lastIndexOf("\\");return b!=-1?a.substr(b+1):a}function x(a){var b=a.match(f);return b?b[0]:""}function w(a){var b=a.name||a.fileName;return{name:b,extension:x(b),size:a.size||a.fileSize,rawFile:a}}function v(b){return a.map(b,function(a){return w(a)})}function u(a){var b=a[0];return b.files?v(b.files):[{name:y(b.value),extension:x(b.value),size:null}]}function t(b){return a.map(u(b),function(a){return a.name}).join(", ")}var c=window.kendo,d=c.ui.Widget,e=c.logToConsole,f=/\.([^\.]+)$/,g="select",h="upload",i="success",j="error",k="complete",l="cancel",m="load",n="progress",o="remove",p=d.extend({init:function(c,e){var f=this;d.fn.init.call(f,c,e),f.name=c.name,f.multiple=f.options.multiple,f.localization=f.options.localization;var g=f.element;f.wrapper=g.closest(".k-upload"),f.wrapper.length==0&&(f.wrapper=f._wrapInput(g)),f._activeInput(g),f.toggle(f.options.enabled),g.closest("form").bind({submit:a.proxy(f._onParentFormSubmit,f),reset:a.proxy(f._onParentFormReset,f)}),f.options.async.saveUrl!=b?(f._module=f._supportsFormData()?new s(f):new r(f),f._async=!0):f._module=new q(f),f._supportsDrop()&&f._setupDropZone(),f.wrapper.delegate(".k-upload-action","click",a.proxy(f._onFileAction,f)).delegate(".k-upload-selected","click",a.proxy(f._onUploadSelected,f)).delegate(".k-file","t:progress",a.proxy(f._onFileProgress,f)).delegate(".k-file","t:upload-success",a.proxy(f._onUploadSuccess,f)).delegate(".k-file","t:upload-error",a.proxy(f._onUploadError,f))},events:[g,h,i,j,k,l,n,o],options:{name:"Upload",enabled:!0,multiple:!0,showFileList:!0,async:{removeVerb:"POST",autoUpload:!0},localization:{select:"Select...",cancel:"Cancel",retry:"Retry",remove:"Remove",uploadSelectedFiles:"Upload files",dropFilesHere:"drop files here to upload",statusUploading:"uploading",statusUploaded:"uploaded",statusFailed:"failed"}},setOptions:function(a){var b=this,c=b.element;d.fn.setOptions.call(b,a),b.multiple=b.options.multiple,c.attr("multiple",b._supportsMultiple()?b.multiple:!1),b.toggle(b.options.enabled)},enable:function(a){a=typeof a=="undefined"?!0:a,this.toggle(a)},disable:function(){this.toggle(!1)},toggle:function(a){a=typeof a=="undefined"?a:!a,this.wrapper.toggleClass("k-state-disabled",a)},_addInput:function(b){var c=this;b.insertAfter(c.element).data("kendoUpload",c),a(c.element).hide().removeAttr("id"),c._activeInput(b)},_activeInput:function(b){var c=this,d=c.wrapper;c.element=b,b.attr("multiple",c._supportsMultiple()?c.multiple:!1).attr("autocomplete","off").click(function(a){d.hasClass("k-state-disabled")&&a.preventDefault()}).change(a.proxy(c._onInputChange,c))},_onInputChange:function(b){var c=this,d=a(b.target),e=c.trigger(g,{files:u(d)});e?(c._addInput(d.clone().val("")),d.remove()):d.trigger("t:select")},_onDrop:function(b){var c=b.originalEvent.dataTransfer,d=this,e=c.files;B(b);if(e.length>0){var f=d.trigger(g,{files:v(e)});f||a(".k-dropzone",d.wrapper).trigger("t:select",[e])}},_enqueueFile:function(b,c){var d=this,e,f,g=a(".k-upload-files",d.wrapper);g.length==0&&(g=a("<ul class='k-upload-files k-reset'></ul>").appendTo(d.wrapper),d.options.showFileList||g.hide()),e=a(".k-file",g),f=a("<li class='k-file'><span class='k-filename' title='"+b+"'>"+b+"</span></li>").appendTo(g).data(c),d._async&&f.prepend("<span class='k-icon'></span>"),d.multiple||e.trigger("t:remove");return f},_removeFileEntry:function(b){var c=b.closest(".k-upload-files"),d;b.remove(),d=a(".k-file",c),d.find("> .k-fail").length===d.length&&this._hideUploadButton(),d.length==0&&c.remove()},_fileAction:function(a,b){var c={remove:"k-delete",cancel:"k-cancel",retry:"k-retry"};!c.hasOwnProperty(b)||(this._clearFileAction(a),a.append(this._renderAction(c[b],this.localization[b]).addClass("k-upload-action")))},_fileState:function(a,b){var c=this.localization,d={uploading:{cssClass:"k-loading",text:c.statusUploading},uploaded:{cssClass:"k-success",text:c.statusUploaded},failed:{cssClass:"k-fail",text:c.statusFailed}},e=d[b];if(e){var f=a.children(".k-icon").text(e.text);f[0].className="k-icon "+e.cssClass}},_renderAction:function(b,c){return b!=""?a("<button type='button' class='k-button k-button-icontext'><span class='k-icon "+b+"'></span>"+c+"</button>"):a("<button type='button' class='k-button'>"+c+"</button>")},_clearFileAction:function(a){a.find(".k-upload-action").remove()},_onFileAction:function(b){var c=this;if(!c.wrapper.hasClass("k-state-disabled")){var d=a(b.target).closest(".k-upload-action"),e=d.find(".k-icon"),f=d.closest(".k-file"),g={files:f.data("fileNames")};e.hasClass("k-delete")?c.trigger(o,g)||f.trigger("t:remove",g.data):e.hasClass("k-cancel")?(c.trigger(l,g),f.trigger("t:cancel"),this._checkAllComplete()):e.hasClass("k-retry")&&f.trigger("t:retry")}return!1},_onUploadSelected:function(){this.wrapper.trigger("t:saveSelected");return!1},_onFileProgress:function(b,c){var d=a(".k-progress-status",b.target);d.length==0&&(d=a("<span class='k-progress'><span class='k-state-selected k-progress-status' style='width: 0;'></span></span>").appendTo(a(".k-filename",b.target)).find(".k-progress-status")),d.width(c+"%"),this.trigger(n,{files:E(b).data("fileNames"),percentComplete:c})},_onUploadSuccess:function(a,b,c){var d=E(a);this._fileState(d,"uploaded"),this.trigger(i,{files:d.data("fileNames"),response:b,operation:"upload",XMLHttpRequest:c}),this._supportsRemove()?this._fileAction(d,o):this._clearFileAction(d),this._checkAllComplete()},_onUploadError:function(a,b){var c=E(a);this._fileState(c,"failed"),this._fileAction(c,"retry");var d=this.trigger(j,{operation:"upload",files:c.data("fileNames"),XMLHttpRequest:b});e("Server response: "+b.responseText),this._checkAllComplete()},_showUploadButton:function(){var b=a(".k-upload-selected",this.wrapper);b.length==0&&(b=this._renderAction("",this.localization.uploadSelectedFiles).addClass("k-upload-selected")),this.wrapper.append(b)},_hideUploadButton:function(){a(".k-upload-selected",this.wrapper).remove()},_onParentFormSubmit:function(){var b=this,c=b.element;c.trigger("t:abort");if(!c.value){var d=a(c);d.attr("disabled","disabled"),window.setTimeout(function(){d.removeAttr("disabled")},0)}},_onParentFormReset:function(){a(".k-upload-files",this.wrapper).remove()},_supportsFormData:function(){return typeof FormData!="undefined"},_supportsMultiple:function(){return!a.browser.opera},_supportsDrop:function(){var a=this._userAgent().toLowerCase(),c=/chrome/.test(a),d=!c&&/safari/.test(a),e=d&&/windows/.test(a);return!e&&this._supportsFormData()&&this.options.async.saveUrl!=b},_userAgent:function(){return navigator.userAgent},_setupDropZone:function(){a(".k-upload-button",this.wrapper).wrap("<div class='k-dropzone'></div>");var b=a(".k-dropzone",this.wrapper).append(a("<em>"+this.localization.dropFilesHere+"</em>")).bind({dragenter:B,dragover:function(a){a.preventDefault()},drop:a.proxy(this._onDrop,this)});C(b,function(){b.addClass("k-dropzone-hovered")},function(){b.removeClass("k-dropzone-hovered")}),C(a(document),function(){b.addClass("k-dropzone-active")},function(){b.removeClass("k-dropzone-active")})},_supportsRemove:function(){return this.options.async.removeUrl!=b},_submitRemove:function(b,c,d,e){var f=this,g=f.options.async.removeField||"fileNames",h=a.extend(c,F());h[g]=b,a.ajax({type:this.options.async.removeVerb,dataType:"json",url:this.options.async.removeUrl,traditional:!0,data:h,success:d,error:e})},_wrapInput:function(a){a.wrap("<div class='k-widget k-upload'><div class='k-button k-upload-button'></div></div>"),a.closest(".k-button").append("<span>"+this.localization.select+"</span>");return a.closest(".k-upload")},_checkAllComplete:function(){a(".k-file .k-icon.k-loading",this.wrapper).length==0&&this.trigger(k)}}),q=function(b){this.name="syncUploadModule",this.element=b.wrapper,this.upload=b,this.element.bind("t:select",a.proxy(this.onSelect,this)).bind("t:remove",a.proxy(this.onRemove,this)).closest("form").attr("enctype","multipart/form-data").attr("encoding","multipart/form-data")};q.prototype={onSelect:function(b){var c=this.upload,d=a(b.target);c._addInput(d.clone().val(""));var e=c._enqueueFile(t(d),{relatedInput:d,fileNames:u(d)});c._fileAction(e,o)},onRemove:function(a){var b=E(a);b.data("relatedInput").remove(),this.upload._removeFileEntry(b)}};var r=function(b){this.name="iframeUploadModule",this.element=b.wrapper,this.upload=b,this.iframes=[],this.element.bind("t:select",a.proxy(this.onSelect,this)).bind("t:cancel",a.proxy(this.onCancel,this)).bind("t:retry",a.proxy(this.onRetry,this)).bind("t:remove",a.proxy(this.onRemove,this)).bind("t:saveSelected",a.proxy(this.onSaveSelected,this)).bind("t:abort",a.proxy(this.onAbort,this))};p._frameId=0,r.prototype={onSelect:function(b){var c=this.upload,d=a(b.target),e=this.prepareUpload(d);c.options.async.autoUpload?this.performUpload(e):(c._supportsRemove()&&this.upload._fileAction(e,o),c._showUploadButton())},prepareUpload:function(b){var c=this.upload,d=a(c.element),e=c.options.async.saveField||b.attr("name");c._addInput(b.clone().val("")),b.attr("name",e);var f=this.createFrame(c.name+"_"+p._frameId++);this.registerFrame(f);var g=this.createForm(c.options.async.saveUrl,f.attr("name")).append(d),h=c._enqueueFile(t(b),{frame:f,relatedInput:d,fileNames:u(b)});f.data({form:g,file:h});return h},performUpload:function(b){var c={files:b.data("fileNames")},d=b.data("frame"),e=this.upload;if(!e.trigger(h,c)){e._hideUploadButton(),d.appendTo(document.body);var f=d.data("form").appendTo(document.body);c.data=a.extend({},c.data,F());for(var g in c.data){var i=f.find("input[name='"+g+"']");i.length==0&&(i=a("<input>",{type:"hidden",name:g}).appendTo(f)),i.val(c.data[g])}e._fileAction(b,l),e._fileState(b,"uploading"),d.one("load",a.proxy(this.onIframeLoad,this)),f[0].submit()}else e._removeFileEntry(d.data("file")),this.cleanupFrame(d),this.unregisterFrame(d)},onSaveSelected:function(b){var c=this;a(".k-file",this.element).each(function(){var b=a(this),d=D(b);d||c.performUpload(b)})},onIframeLoad:function(b){var c=a(b.target);try{var d=c.contents().text()}catch(b){d="Error trying to get server response: "+b}this.processResponse(c,d)},processResponse:function(b,c){var d=b.data("file"),e=this,f={responseText:c};A(c,function(c){a.extend(f,{statusText:"OK",status:"200"}),d.trigger("t:progress",[100]),d.trigger("t:upload-success",[c,f]),e.cleanupFrame(b),e.unregisterFrame(b)},function(){a.extend(f,{statusText:"error",status:"500"}),d.trigger("t:upload-error",[f])})},onCancel:function(b){var c=a(b.target).data("frame");this.stopFrameSubmit(c),this.cleanupFrame(c),this.unregisterFrame(c),this.upload._removeFileEntry(c.data("file"))},onRetry:function(a){var b=E(a);this.performUpload(b)},onRemove:function(a,b){var c=E(a),d=c.data("frame");d?(this.unregisterFrame(d),this.upload._removeFileEntry(c),this.cleanupFrame(d)):z(c,this.upload,b)},onAbort:function(){var b=this.element,c=this;a.each(this.iframes,function(){a("input",this.data("form")).appendTo(b),c.stopFrameSubmit(this[0]),this.data("form").remove(),this.remove()}),this.iframes=[]},createFrame:function(b){return a("<iframe name='"+b+"'"+" id='"+b+"'"+" style='display:none;' />")},createForm:function(b,c){return a("<form enctype='multipart/form-data' method='POST' action='"+b+"'"+" target='"+c+"'"+"/>")},stopFrameSubmit:function(a){typeof a.stop!="undefined"?a.stop():a.document&&(a.document.execCommand("Stop"),a.contentWindow.location.href=a.contentWindow.location.href)},registerFrame:function(a){this.iframes.push(a)},unregisterFrame:function(b){this.iframes=a.grep(this.iframes,function(a){return a.attr("name")!=b.attr("name")})},cleanupFrame:function(a){var b=a.data("form");a.data("file").data("frame",null),setTimeout(function(){b.remove(),a.remove()},1)}};var s=function(b){this.name="formDataUploadModule",this.element=b.wrapper,this.upload=b,this.element.bind("t:select",a.proxy(this.onSelect,this)).bind("t:cancel",a.proxy(this.onCancel,this)).bind("t:remove",a.proxy(this.onRemove,this)).bind("t:retry",a.proxy(this.onRetry,this)).bind("t:saveSelected",a.proxy(this.onSaveSelected,this)).bind("t:abort",a.proxy(this.onAbort,this))};s.prototype={onSelect:function(b,c){var d=this.upload,e=this,f=a(b.target),g=c?v(c):this.inputFiles(f),h=this.prepareUpload(f,g);a.each(h,function(){d.options.async.autoUpload?e.performUpload(this):(d._supportsRemove()&&d._fileAction(this,o),d._showUploadButton())})},prepareUpload:function(b,c){var d=this.enqueueFiles(c);b.is("input")&&(a.each(d,function(){a(this).data("relatedInput",b)}),b.data("relatedFileEntries",d),this.upload._addInput(b.clone().val("")));return d},enqueueFiles:function(a){var b=this.upload;fileEntries=[];for(var c=0;c<a.length;c++){var d=a[c],e=d.name,f=b._enqueueFile(e,{fileNames:[d]});f.data("file",d),fileEntries.push(f)}return fileEntries},inputFiles:function(a){return u(a)},performUpload:function(b){var c=this.upload,d=this.createFormData(b.data("file")),e={files:b.data("fileNames")};if(!c.trigger(h,e)){c._fileAction(b,l),c._hideUploadButton(),e.data=a.extend({},e.data,F());for(var f in e.data)d.append(f,e.data[f]);c._fileState(b,"uploading"),this.postFormData(this.upload.options.async.saveUrl,d,b)}else this.removeFileEntry(b)},onSaveSelected:function(b){var c=this;a(".k-file",this.element).each(function(){var b=a(this),d=D(b);d||c.performUpload(b)})},onCancel:function(a){var b=E(a);this.stopUploadRequest(b),this.removeFileEntry(b)},onRetry:function(a){var b=E(a);this.performUpload(b)},onRemove:function(a,b){var c=E(a);c.children(".k-icon").is(".k-success")?z(c,this.upload,b):this.removeFileEntry(c)},postFormData:function(a,b,c){var d=new XMLHttpRequest,e=this;c.data("request",d),d.addEventListener("load",function(a){e.onRequestSuccess.call(e,a,c)},!1),d.addEventListener(j,function(a){e.onRequestError.call(e,a,c)},!1),d.upload.addEventListener("progress",function(a){e.onRequestProgress.call(e,a,c)},!1),d.open("POST",a),d.withCredentials="true",d.send(b)},createFormData:function(a){var b=new FormData,c=this.upload;b.append(c.options.async.saveField||c.name,a.rawFile);return b},onRequestSuccess:function(a,b){function e(){b.trigger("t:upload-error",[c])}var c=a.target,d=this;c.status>=200&&c.status<=299?A(c.responseText,function(a){b.trigger("t:progress",[100]),b.trigger("t:upload-success",[a,c]),d.cleanupFileEntry(b)},e):e()},onRequestError:function(a,b){var c=a.target;b.trigger("t:upload-error",[c])},cleanupFileEntry:function(b){var c=b.data("relatedInput"),d=!0;c&&(a.each(c.data("relatedFileEntries")||[],function(){this.parent().length>0&&this[0]!=b[0]&&(d=d&&this.children(".k-icon").is(".k-success"))}),d&&c.remove())},removeFileEntry:function(a){this.cleanupFileEntry(a),this.upload._removeFileEntry(a)},onRequestProgress:function(a,b){var c=Math.round(a.loaded*100/a.total);b.trigger("t:progress",[c])},stopUploadRequest:function(a){a.data("request").abort()}},c.ui.plugin(p)}(jQuery),function(a,b){function Q(a){var b=this;b.owner=a,b._draggable=new e(a.wrapper,{filter:n,group:a.wrapper.id+"-moving",dragstart:g(b.dragstart,b),drag:g(b.drag,b),dragend:g(b.dragend,b),dragcancel:g(b.dragcancel,b)})}function P(a){var b=this;b.owner=a,b._draggable=new e(a.wrapper,{filter:p,group:a.wrapper.id+"-resizing",dragstart:g(b.dragstart,b),drag:g(b.drag,b),dragend:g(b.dragend,b)})}function N(a,b){return function(){var a=this,c=a.wrapper,d=c[0].style,e=a.options;if(!e.isMaximized&&!e.isMinimized){a.restoreOptions={width:d.width,height:d.height},c.find(p).hide().end().find(I).parent().hide().eq(0).before(l.action({name:"Restore"})),b.call(a);return a}}}function M(){return a(m).filter(function(){var b=a(this);return b.is(u)&&L(b).options.modal}).sort(function(b,c){return+a(b).css("zIndex")- +a(c).css("zIndex")})}function L(a){return a.children(o).data("kendoWindow")}function K(a,b,c){return Math.max(Math.min(a,c),b)}var c=window.kendo,d=c.ui.Widget,e=c.ui.Draggable,f=a.isPlainObject,g=a.proxy,h=a.extend,i=a.each,j=c.template,k="body",l,m=".k-window",n=".k-window-titlebar",o=".k-window-content",p=".k-resize-handle",q=".k-overlay",r="k-content-frame",s="k-loading",t="k-state-hover",u=":visible",v="hidden",w="cursor",x="open",y="activate",z="deactivate",A="close",B="refresh",C="resize",D="dragstart",E="dragend",F="error",G="overflow",H="zIndex",I=".k-window-actions .k-i-minimize,.k-window-actions .k-i-maximize",J=c.isLocalUrl,O=d.extend({init:function(b,e){var h=this,j,k,o,p,q=!1,r;d.fn.init.call(h,b,e),e=h.options,b=h.element,r=e.content,h.appendTo=a(e.appendTo||document.body),h._animations(),f(r)||(r=e.content={url:r}),b.parent().is(h.appendTo)||(b.is(u)?(k=b.offset(),q=!0):(o=b.css("visibility"),p=b.css("display"),b.css({visibility:v,display:""}),k=b.offset(),b.css({visibility:o,display:p}))),typeof e.visible=="undefined"&&(e.visible=b.is(u)),j=h.wrapper=b.closest(m);if(!b.is(".k-content")||!j[0])b.addClass("k-window-content k-content"),h._createWindow(b,e),j=h.wrapper=b.closest(m),h._dimensions();k&&j.css({top:k.top,left:k.left}),r&&h.refresh(r),h.toFront(),e.visible&&e.modal&&h._overlay(j.is(u)).css({opacity:.5}),j.on({mouseenter:function(){a(this).addClass(t)},mouseleave:function(){a(this).removeClass(t)},click:g(h._windowActionHandler,h)},".k-window-titlebar .k-window-action"),e.resizable&&(j.on("dblclick",n,g(h.toggleMaximization,h)),i("n e s w se sw ne nw".split(" "),function(a,b){j.append(l.resizeHandle(b))}),h.resizing=new P(h)),e.draggable&&(h.dragging=new Q(h)),j.add(j.find(".k-resize-handle,.k-window-titlebar")).on("mousedown",g(h.toFront,h)),h.touchScroller=c.touchScroller(b),a(window).resize(g(h._onDocumentResize,h)),e.visible&&(h.trigger(x),h.trigger(y)),c.notify(h)},_dimensions:function(){var a=this,b=a.wrapper,c=a.element,d=a.options;a.title(d.title),d.width&&b.width(d.width),d.height&&b.height(d.height),i(["minWidth","minHeight","maxWidth","maxHeight"],function(a,b){var e=d[b];e&&e!=Infinity&&c.css(b,e)}),d.visible||b.hide()},_animations:function(){var a=this.options;a.animation===!1&&(a.animation={open:{show:!0,effects:{}},close:{hide:!0,effects:{}}})},setOptions:function(a){d.fn.setOptions.call(this,a),this._animations(),this._dimensions()},events:[x,y,z,A,B,C,D,E,F],options:{name:"Window",animation:{open:{effects:{zoom:{direction:"in"},fade:{direction:"in"}},duration:350,show:!0},close:{effects:{zoom:{direction:"out",properties:{scale:.7}},fade:{direction:"out"}},duration:350,hide:!0}},title:"",actions:["Close"],modal:!1,resizable:!0,draggable:!0,minWidth:90,minHeight:50,maxWidth:Infinity,maxHeight:Infinity},_overlay:function(b){var c=this.appendTo.children(".k-overlay"),d=this.wrapper;c.length||(c=a("<div class='k-overlay' />")),c.insertBefore(d[0]).toggle(b).css(H,parseInt(d.css(H),10)-1);return c},_windowActionHandler:function(b){var c=a(b.target).closest(".k-window-action").find(".k-icon"),d=this;i({"k-i-close":d.close,"k-i-maximize":d.maximize,"k-i-minimize":d.minimize,"k-i-restore":d.restore,"k-i-refresh":d.refresh},function(a,e){if(c.hasClass(a)){b.preventDefault(),e.call(d);return!1}})},center:function(){var b=this.wrapper,c=a(window);b.css({left:c.scrollLeft()+Math.max(0,(c.width()-b.width())/2),top:c.scrollTop()+Math.max(0,(c.height()-b.height())/2)});return this},title:function(a){var b=this,c=b.wrapper,d=b.options,e=c.find(n),f=e.children(".k-window-title"),g=e.outerHeight();if(!arguments.length)return f.text();a===!1?(c.addClass("k-window-titleless"),e.remove()):(e.length||c.prepend(l.titlebar(h(l,d))),c.css("padding-top",g),e.css("margin-top",-g)),f.text(a);return b},content:function(a){var b=this.wrapper.children(o);if(!a)return b.html();b.html(a);return this},open:function(){var b=this,c=b.wrapper,d=b.options,e=d.animation.open,f=c.children(o),g=f.css(G),h;b.trigger(x)||(b.toFront(),d.visible=!0,d.modal&&(h=b._overlay(!1),e.duration?h.kendoStop().kendoAnimate({effects:{fade:{direction:"out",properties:{opacity:.5}}},duration:e.duration,show:!0}):h.css("opacity",.5).show()),c.is(u)||(f.css(G,v),c.show().kendoStop().kendoAnimate({effects:e.effects,duration:e.duration,complete:function(){b.trigger(y),f.css(G,g)}}))),d.isMaximized&&(b._documentScrollTop=a(document).scrollTop(),a("html, body").css(G,v));return b},close:function(){var c=this,d=c.wrapper,e=c.options,f=e.animation.open,g=e.animation.close,h,i,j;d.is(u)&&!c.trigger(A)&&(e.visible=!1,h=M(),i=e.modal&&h.length==1,j=e.modal?c._overlay(!0):a(b),i?g.duration?j.kendoStop().kendoAnimate({effects:{fadeOut:{properties:{opacity:0}}},duration:g.duration,hide:!0}):j.hide():h.length&&L(h.eq(h.length-2))._overlay(!0),d.kendoStop().kendoAnimate({effects:g.effects||f.effects,reverse:g.reverse===!0,duration:g.duration,complete:function(){d.hide(),c.trigger(z)}})),c.options.isMaximized&&(a("html, body").css(G,""),this._documentScrollTop&&this._documentScrollTop>0&&a(document).scrollTop(this._documentScrollTop));return c},toFront:function(){var b=this,c=b.wrapper,d=c[0],e=+c.css(H),f=e;a(m).each(function(b,c){var f=a(c),g=f.css(H),h=f.find(o);isNaN(g)||(e=Math.max(+g,e)),c!=d&&h.find("> ."+r).length>0&&h.append(l.overlay)});if(e==10001||f<e)c.css(H,e+2),b.element.find("> .k-overlay").remove();return b},toggleMaximization:function(){return this[this.options.isMaximized?"restore":"maximize"]()},restore:function(){var b=this,c=b.options,d=b.restoreOptions;if(!!c.isMaximized||!!c.isMinimized){b.wrapper.css({position:"absolute",left:d.left,top:d.top,width:d.width,height:d.height}).find(".k-window-content,.k-resize-handle").show().end().find(".k-window-titlebar .k-i-restore").parent().remove().end().end().find(I).parent().show(),a("html, body").css(G,""),this._documentScrollTop&&this._documentScrollTop>0&&a(document).scrollTop(this._documentScrollTop),c.isMaximized=c.isMinimized=!1,b.trigger(C);return b}},maximize:N("maximize",function(){var b=this,c=b.wrapper,d=c.position();h(b.restoreOptions,{left:d.left,top:d.top}),c.css({left:0,top:0,position:"fixed"}),this._documentScrollTop=a(document).scrollTop(),a("html, body").css(G,v),b.options.isMaximized=!0,b._onDocumentResize()}),minimize:N("minimize",function(){var a=this;a.wrapper.css("height",""),a.element.hide(),a.options.isMinimized=!0}),_onDocumentResize:function(){var b=this,c=b.wrapper,d=a(window);!b.options.isMaximized||(c.css({width:d.width(),height:d.height()-parseInt(c.css("padding-top"),10)}),b.trigger(C))},refresh:function(b){var c=this,d=c.options,e=a(c.element),g,i=d.iframe,k;f(b)||(b={url:b}),b=h({},d.content,b),k=b.url,k?(typeof i=="undefined"&&(i=!J(k)),i?(g=e.find("."+r)[0],g?g.src=k||g.src:e.html(l.contentFrame(h({},d,{content:b})))):c._ajaxRequest(b)):b.template&&c.content(j(b.template)({}));return c},_ajaxRequest:function(b){var c=this,d=b.template,e=c.wrapper.find(".k-window-titlebar .k-i-refresh"),f=setTimeout(function(){e.addClass(s)},100);a.ajax(h({type:"GET",dataType:"html",cache:!1,error:g(function(a,b){c.trigger(F)},c),complete:function(){clearTimeout(f),e.removeClass(s)},success:g(function(a,b){d&&(a=j(d)(a||{})),c.element.html(a),c.trigger(B)},c)},b))},destroy:function(){var a=this,b,c;a.wrapper.remove(),b=M(),c=a.options.modal&&!b.length,c?a._overlay(!1).remove():b.length>0&&L(b.eq(b.length-2))._overlay(!0)},_createWindow:function(){var b=this,c=b.element,d=b.options,e,f;d.scrollable===!1&&c.attr("style","overflow:hidden;"),d.iframe&&c.html(l.contentFrame(d)),f=a(l.wrapper(d)),d.title!==!1&&f.append(l.titlebar(h(l,d))),f.toggleClass("k-rtl",!!b.element.closest(".k-rtl").length),e=c.find("iframe").map(function(a){var b=this.getAttribute("src");this.src="";return b}),f.appendTo(b.appendTo).append(c).find("iframe").each(function(a){this.src=e[a]}),c.show()}});l={wrapper:j("<div class='k-widget k-window' />"),action:j("<a href='\\#' class='k-window-action k-link'><span class='k-icon k-i-#= name.toLowerCase() #'>#= name #</span></a>"),titlebar:j("<div class='k-window-titlebar k-header'>&nbsp;<span class='k-window-title'>#= title #</span><div class='k-window-actions k-header'># for (var i = 0; i < actions.length; i++) { ##= action({ name: actions[i] }) ## } #</div></div>"),overlay:"<div class='k-overlay' />",contentFrame:j("<iframe src='#= content.url #' title='#= title #' frameborder='0' class='"+r+"'>"+"This page requires frames in order to show content"+"</iframe>"),resizeHandle:j("<div class='k-resize-handle k-resize-#= data #'></div>")},P.prototype={dragstart:function(b){var c=this,d=c.owner,e=d.wrapper;c.elementPadding=parseInt(d.wrapper.css("padding-top"),10),c.initialCursorPosition=e.offset(),c.resizeDirection=b.currentTarget.prop("className").replace("k-resize-handle k-resize-",""),c.initialSize={width:e.width(),height:e.height()},c.containerOffset=d.appendTo.offset(),e.append(l.overlay).find(p).not(b.currentTarget).hide(),a(k).css(w,b.currentTarget.css(w))},drag:function(a){var b=this,c=b.owner,d=c.wrapper,e=c.options,f=b.resizeDirection,g=b.containerOffset,h=b.initialCursorPosition,i=b.initialSize,j,k,l,m,n=a.x.location,o=a.y.location;f.indexOf("e")>=0?(j=n-h.left,d.width(K(j,e.minWidth,e.maxWidth))):f.indexOf("w")>=0&&(m=h.left+i.width,j=K(m-n,e.minWidth,e.maxWidth),d.css({left:m-j-g.left,width:j})),f.indexOf("s")>=0?(k=o-h.top-b.elementPadding,d.height(K(k,e.minHeight,e.maxHeight))):f.indexOf("n")>=0&&(l=h.top+i.height,k=K(l-o,e.minHeight,e.maxHeight),d.css({top:l-k-g.top,height:k})),c.trigger(C)},dragend:function(b){var c=this,d=c.owner,e=d.wrapper;e.find(q).remove().end().find(p).not(b.currentTarget).show(),a(k).css(w,""),d.touchScroller&&d.touchScroller.reset(),b.keyCode==27&&e.css(c.initialCursorPosition).css(c.initialSize);return!1}},Q.prototype={dragstart:function(b){var c=this.owner,d=c.element,e=d.find(".k-window-actions"),f=c.appendTo.offset();c.trigger(D),c.initialWindowPosition=c.wrapper.position(),c.startPosition={left:b.x.client-c.initialWindowPosition.left,top:b.y.client-c.initialWindowPosition.top},e.length>0?c.minLeftPosition=e.outerWidth()+parseInt(e.css("right"),10)-d.outerWidth():c.minLeftPosition=20-d.outerWidth(),c.minLeftPosition-=f.left,c.minTopPosition=-f.top,c.wrapper.append(l.overlay).find(p).hide(),a(k).css(w,b.currentTarget.css(w))},drag:function(b){var c=this.owner,d={left:Math.max(b.x.client-c.startPosition.left,c.minLeftPosition),top:Math.max(b.y.client-c.startPosition.top,c.minTopPosition)};a(c.wrapper).css(d)},dragcancel:function(b){var c=this.owner;c.wrapper.find(p).show().end().find(q).remove(),a(k).css(w,""),b.currentTarget.closest(m).css(c.initialWindowPosition)},dragend:function(b){var c=this.owner;c.wrapper.find(p).show().end().find(q).remove(),a(k).css(w,""),c.trigger(E);return!1}},c.ui.plugin(O)}(jQuery);
/*
* Kendo UI Web v2012.2.710 (http://kendoui.com)
* Copyright 2012 Telerik AD. All rights reserved.
*
* Kendo UI Web commercial licenses may be obtained at http://kendoui.com/web-license
* If you do not own a commercial license, this file shall be governed by the
* GNU General Public License (GPL) version 3.
* For GPL requirements, please review: http://www.gnu.org/copyleft/gpl.html
*/

;(function(a,b){kendo.cultures["it-IT"]={name:"it-IT",numberFormat:{pattern:["-n"],decimals:2,",":".",".":",",groupSize:[3],percent:{pattern:["-n%","n%"],decimals:2,",":".",".":",",groupSize:[3],symbol:"%"},currency:{pattern:["-$ n","$ n"],decimals:2,",":".",".":",",groupSize:[3],symbol:"€"}},calendars:{standard:{days:{names:["domenica","lunedì","martedì","mercoledì","giovedì","venerdì","sabato"],namesAbbr:["dom","lun","mar","mer","gio","ven","sab"],namesShort:["do","lu","ma","me","gi","ve","sa"]},months:{names:["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre",""],namesAbbr:["gen","feb","mar","apr","mag","giu","lug","ago","set","ott","nov","dic",""]},AM:[""],PM:[""],patterns:{d:"dd/MM/yyyy",D:"dddd d MMMM yyyy",F:"dddd d MMMM yyyy HH:mm:ss",g:"dd/MM/yyyy HH:mm",G:"dd/MM/yyyy HH:mm:ss",m:"dd MMMM",M:"dd MMMM",s:"yyyy'-'MM'-'dd'T'HH':'mm':'ss",t:"HH:mm",T:"HH:mm:ss",u:"yyyy'-'MM'-'dd HH':'mm':'ss'Z'",y:"MMMM yyyy",Y:"MMMM yyyy"},"/":"/",":":":",firstDay:1}}}})(this);
/*
* Kendo UI Web v2012.2.710 (http://kendoui.com)
* Copyright 2012 Telerik AD. All rights reserved.
*
* Kendo UI Web commercial licenses may be obtained at http://kendoui.com/web-license
* If you do not own a commercial license, this file shall be governed by the
* GNU General Public License (GPL) version 3.
* For GPL requirements, please review: http://www.gnu.org/copyleft/gpl.html
*/

;(function(a,b){kendo.cultures["it-CH"]={name:"it-CH",numberFormat:{pattern:["-n"],decimals:2,",":"'",".":".",groupSize:[3],percent:{pattern:["-n%","n%"],decimals:2,",":"'",".":".",groupSize:[3],symbol:"%"},currency:{pattern:["$-n","$ n"],decimals:2,",":"'",".":".",groupSize:[3],symbol:"fr."}},calendars:{standard:{days:{names:["domenica","lunedì","martedì","mercoledì","giovedì","venerdì","sabato"],namesAbbr:["dom","lun","mar","mer","gio","ven","sab"],namesShort:["do","lu","ma","me","gi","ve","sa"]},months:{names:["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre",""],namesAbbr:["gen","feb","mar","apr","mag","giu","lug","ago","set","ott","nov","dic",""]},AM:[""],PM:[""],patterns:{d:"dd.MM.yyyy",D:"dddd, d. MMMM yyyy",F:"dddd, d. MMMM yyyy HH:mm:ss",g:"dd.MM.yyyy HH:mm",G:"dd.MM.yyyy HH:mm:ss",m:"d. MMMM",M:"d. MMMM",s:"yyyy'-'MM'-'dd'T'HH':'mm':'ss",t:"HH:mm",T:"HH:mm:ss",u:"yyyy'-'MM'-'dd HH':'mm':'ss'Z'",y:"MMMM yyyy",Y:"MMMM yyyy"},"/":".",":":":",firstDay:1}}}})(this);
/*
* Kendo UI Web v2012.2.710 (http://kendoui.com)
* Copyright 2012 Telerik AD. All rights reserved.
*
* Kendo UI Web commercial licenses may be obtained at http://kendoui.com/web-license
* If you do not own a commercial license, this file shall be governed by the
* GNU General Public License (GPL) version 3.
* For GPL requirements, please review: http://www.gnu.org/copyleft/gpl.html
*/

;(function(a,b){kendo.cultures.it={name:"it",numberFormat:{pattern:["-n"],decimals:2,",":".",".":",",groupSize:[3],percent:{pattern:["-n%","n%"],decimals:2,",":".",".":",",groupSize:[3],symbol:"%"},currency:{pattern:["-$ n","$ n"],decimals:2,",":".",".":",",groupSize:[3],symbol:"€"}},calendars:{standard:{days:{names:["domenica","lunedì","martedì","mercoledì","giovedì","venerdì","sabato"],namesAbbr:["dom","lun","mar","mer","gio","ven","sab"],namesShort:["do","lu","ma","me","gi","ve","sa"]},months:{names:["gennaio","febbraio","marzo","aprile","maggio","giugno","luglio","agosto","settembre","ottobre","novembre","dicembre",""],namesAbbr:["gen","feb","mar","apr","mag","giu","lug","ago","set","ott","nov","dic",""]},AM:[""],PM:[""],patterns:{d:"dd/MM/yyyy",D:"dddd d MMMM yyyy",F:"dddd d MMMM yyyy HH:mm:ss",g:"dd/MM/yyyy HH:mm",G:"dd/MM/yyyy HH:mm:ss",m:"dd MMMM",M:"dd MMMM",s:"yyyy'-'MM'-'dd'T'HH':'mm':'ss",t:"HH:mm",T:"HH:mm:ss",u:"yyyy'-'MM'-'dd HH':'mm':'ss'Z'",y:"MMMM yyyy",Y:"MMMM yyyy"},"/":"/",":":":",firstDay:1}}}})(this);
(function() {



}).call(this);
(function() {



}).call(this);
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//






;
