class PaginationListLinkRenderer < WillPaginate::ViewHelpers::LinkRenderer

  protected

    def page_number(page)
      unless page == current_page
        tag(:a, link(page, page, :rel => rel_value(page)))
      else
        tag(:a, page, :class => "current")
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        tag(:a, link(text, page), :class => classname)
      else
        tag(:a, text, :class => classname + ' disabled')
      end
    end

    def html_container(html)
      tag(:span, html, container_attributes)
    end

end