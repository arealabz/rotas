module ApplicationHelper

	def get_element_image(ele)
		if ele.media.count == 0
			"/assets/content/img800.jpg"
		else
			ele.media.first.upload.filename.normal.url
		end
	end

	def get_element_description(ele)
		if ele.media.count == 0
			""
		else
			ele.media.first.descrizione
		end
	end

	def print_friendly_url(title)
		URI.escape(title.downcase.gsub(" ", "-"))
	end

	def print_portfolio_link(idc,idp)
		elemento = Mod.find(idp)
		categoria = ModCat.find(idc)

		ele_title = URI.escape( elemento.sml.where("fklang=?", "it").first.title.downcase.gsub(" ", "-") )
		cat_title = URI.escape( categoria.sml.where('fklang=? AND fkparent=?', "it", categoria.id).first.title.downcase.gsub(" ", "-") )

		"/portfolio/#{cat_title}-#{ele_title}-#{idc}-#{idp}"
	end

end
