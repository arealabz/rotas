module Admin::ApplicationHelper
	
  def full_title(page_title)
		base_title = "Rotas | "
		if page_title.empty?
			base_title
		else
			"#{base_title} | #{page_title}"
		end	
	end

	def get_element_title(mod)
		#logger.info mod.inspect
		mod.sml.where("fklang=?", "it").first.title
	end

	def find_published(mod)
		if mod.published == 1
			"icon-eye-open"
		else
			"icon-eye-close"
		end
	end

	def published_class(mod) 
		if mod.published != 1
			"color:#CCC;font-style:italic"
		end

	end

	def breadcrumb(id, last_divider = false, list_element = false)
		html = ""
		tree = ModCat.getCatTree(id, []).reverse

		tree.each_with_index do |ele, index|
			html += "<li><a href=\"/admin/mods?idserv=#{ele[:idserv]}&idp=#{ele[:id]}\">#{ele[:title]}</a></li> "
			if (index+1) == tree.count && !last_divider
				html += ""
			elsif (index+1) == tree.count && list_element
				html += "<span class='divider'> > </span><a href='/admin/mods/list?idserv=#{ele[:idserv]}&idc=#{ele[:id]}'>Elementi #{ele[:title]}</a></li>"
			else
				html += "<span class='divider'> > </span></li>"
			end
		end
		
		html
	end

	def print_file_name(filename)
		filename.split("/").last
	end

	def get_media_description(ele,up)
		media = Media.find_by_fkmod_and_fkfile(ele.id,up.id)

		media.descrizione
	end

	def get_video_description(ele,up)
		media = MediaVideo.find_by_fkmod_and_fkvideo(ele.id,up.id)

		media.descrizione
	end

	def print_ico_child(parent)
		ico = "icon-plus"
		if Mod.find_all_by_fkcat_and_f_del_and_published(parent, "0", "1").count > 0
			ico = "icon-file"
		end

		return ico
	end

end	