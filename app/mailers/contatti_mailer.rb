class ContattiMailer < ActionMailer::Base
  default from: "roberto@informaticamente.org"

  def contact_email(user)
    @user = user

    mail(to: user['email'], subject: 'Contatto dal sito')
  end
end
