--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.unique_schema_migrations;
ALTER TABLE ONLY public.videos DROP CONSTRAINT videos_pkey;
ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
ALTER TABLE ONLY public.uploads DROP CONSTRAINT uploads_pkey;
ALTER TABLE ONLY public.upload_folders DROP CONSTRAINT upload_folders_pkey;
ALTER TABLE ONLY public.translations DROP CONSTRAINT translations_pkey;
ALTER TABLE ONLY public.translation_smls DROP CONSTRAINT translation_smls_pkey;
ALTER TABLE ONLY public.static_pages DROP CONSTRAINT static_pages_pkey;
ALTER TABLE ONLY public.static_page_smls DROP CONSTRAINT static_page_smls_pkey;
ALTER TABLE ONLY public.servizis DROP CONSTRAINT servizis_pkey;
ALTER TABLE ONLY public.mods DROP CONSTRAINT mods_pkey;
ALTER TABLE ONLY public.mod_smls DROP CONSTRAINT mod_smls_pkey;
ALTER TABLE ONLY public.mod_cats DROP CONSTRAINT mod_cats_pkey;
ALTER TABLE ONLY public.mod_cat_smls DROP CONSTRAINT mod_cat_smls_pkey;
ALTER TABLE ONLY public.media_videos DROP CONSTRAINT media_videos_pkey;
ALTER TABLE ONLY public.media DROP CONSTRAINT media_pkey;
ALTER TABLE ONLY public.lingues DROP CONSTRAINT lingues_pkey;
ALTER TABLE public.videos ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.uploads ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.upload_folders ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.translations ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.translation_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.static_pages ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.static_page_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.servizis ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mods ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_cats ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.mod_cat_smls ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.media_videos ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.media ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.lingues ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.videos_id_seq;
DROP TABLE public.videos;
DROP SEQUENCE public.users_id_seq;
DROP TABLE public.users;
DROP SEQUENCE public.uploads_id_seq;
DROP TABLE public.uploads;
DROP SEQUENCE public.upload_folders_id_seq;
DROP TABLE public.upload_folders;
DROP SEQUENCE public.translations_id_seq;
DROP TABLE public.translations;
DROP SEQUENCE public.translation_smls_id_seq;
DROP TABLE public.translation_smls;
DROP SEQUENCE public.static_pages_id_seq;
DROP TABLE public.static_pages;
DROP SEQUENCE public.static_page_smls_id_seq;
DROP TABLE public.static_page_smls;
DROP SEQUENCE public.servizis_id_seq;
DROP TABLE public.servizis;
DROP TABLE public.schema_migrations;
DROP SEQUENCE public.mods_id_seq;
DROP TABLE public.mods;
DROP SEQUENCE public.mod_smls_id_seq;
DROP TABLE public.mod_smls;
DROP SEQUENCE public.mod_cats_id_seq;
DROP TABLE public.mod_cats;
DROP SEQUENCE public.mod_cat_smls_id_seq;
DROP TABLE public.mod_cat_smls;
DROP SEQUENCE public.media_videos_id_seq;
DROP TABLE public.media_videos;
DROP SEQUENCE public.media_id_seq;
DROP TABLE public.media;
DROP SEQUENCE public.lingues_id_seq;
DROP TABLE public.lingues;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: lingues; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lingues (
    id integer NOT NULL,
    cod character varying(255),
    lingua character varying(255),
    active integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: lingues_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lingues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lingues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lingues_id_seq OWNED BY lingues.id;


--
-- Name: lingues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('lingues_id_seq', 1, true);


--
-- Name: media; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE media (
    id integer NOT NULL,
    fkfile integer,
    fkmod integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    descrizione text,
    ordine integer DEFAULT 0
);


--
-- Name: media_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE media_id_seq OWNED BY media.id;


--
-- Name: media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('media_id_seq', 183, true);


--
-- Name: media_videos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE media_videos (
    id integer NOT NULL,
    fkvideo integer,
    fkmod integer,
    descrizione text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: media_videos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE media_videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: media_videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE media_videos_id_seq OWNED BY media_videos.id;


--
-- Name: media_videos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('media_videos_id_seq', 21, true);


--
-- Name: mod_cat_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_cat_smls (
    id integer NOT NULL,
    fkparent integer,
    fklang character varying(255),
    title character varying(255),
    abstract text,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_cat_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_cat_smls_id_seq OWNED BY mod_cat_smls.id;


--
-- Name: mod_cat_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_cat_smls_id_seq', 11, true);


--
-- Name: mod_cats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_cats (
    id integer NOT NULL,
    idserv integer,
    f_del integer,
    ordine integer,
    fkparent integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published integer,
    settori character varying,
    applicazioni character varying
);


--
-- Name: mod_cats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_cats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_cats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_cats_id_seq OWNED BY mod_cats.id;


--
-- Name: mod_cats_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_cats_id_seq', 13, true);


--
-- Name: mod_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mod_smls (
    id integer NOT NULL,
    fkparent integer,
    fklang character varying(255),
    url_title character varying(255),
    title character varying(255),
    abstract text,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: mod_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mod_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mod_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mod_smls_id_seq OWNED BY mod_smls.id;


--
-- Name: mod_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mod_smls_id_seq', 60, true);


--
-- Name: mods; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mods (
    id integer NOT NULL,
    idserv integer,
    fkcat integer,
    home character varying(255),
    f_del integer,
    ordine integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    published integer,
    data_evento character varying(255),
    etc character varying(255),
    settori character varying,
    applicazioni character varying
);


--
-- Name: mods_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mods_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mods_id_seq OWNED BY mods.id;


--
-- Name: mods_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('mods_id_seq', 60, true);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: servizis; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE servizis (
    id integer NOT NULL,
    fkparent integer,
    nome character varying(255),
    label character varying(255),
    enabled integer,
    ordine integer,
    path character varying(255),
    skip_cat integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: servizis_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE servizis_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: servizis_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE servizis_id_seq OWNED BY servizis.id;


--
-- Name: servizis_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('servizis_id_seq', 7, true);


--
-- Name: static_page_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE static_page_smls (
    id integer NOT NULL,
    title character varying(255),
    subtitle character varying(255),
    header text,
    testo text,
    footer text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    static_page_id integer,
    fklang character varying(255)
);


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE static_page_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE static_page_smls_id_seq OWNED BY static_page_smls.id;


--
-- Name: static_page_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('static_page_smls_id_seq', 13, true);


--
-- Name: static_pages; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE static_pages (
    id integer NOT NULL,
    f_del integer,
    published integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    foto character varying(255),
    foto_home character varying(255),
    url_pagina character varying(255),
    home_page integer,
    ordine integer,
    related_cat character varying(255)
);


--
-- Name: static_pages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE static_pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: static_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE static_pages_id_seq OWNED BY static_pages.id;


--
-- Name: static_pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('static_pages_id_seq', 13, true);


--
-- Name: translation_smls; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE translation_smls (
    id integer NOT NULL,
    translation_id integer,
    testo text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fklang character varying(255)
);


--
-- Name: translation_smls_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE translation_smls_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: translation_smls_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE translation_smls_id_seq OWNED BY translation_smls.id;


--
-- Name: translation_smls_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('translation_smls_id_seq', 1, false);


--
-- Name: translations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE translations (
    id integer NOT NULL,
    etc character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: translations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE translations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: translations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE translations_id_seq OWNED BY translations.id;


--
-- Name: translations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('translations_id_seq', 1, false);


--
-- Name: upload_folders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE upload_folders (
    id integer NOT NULL,
    fkparent integer,
    nome character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    f_del integer DEFAULT 0
);


--
-- Name: upload_folders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE upload_folders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: upload_folders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE upload_folders_id_seq OWNED BY upload_folders.id;


--
-- Name: upload_folders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('upload_folders_id_seq', 14, true);


--
-- Name: uploads; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE uploads (
    id integer NOT NULL,
    filename character varying(255),
    ext character varying(255),
    content_type character varying(255),
    file_size integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fkfolder integer
);


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE uploads_id_seq OWNED BY uploads.id;


--
-- Name: uploads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('uploads_id_seq', 40, true);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255),
    email character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    password_digest character varying(255),
    remember_token character varying(255),
    level integer,
    active integer
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: videos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE videos (
    id integer NOT NULL,
    url text,
    img text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    title character varying(255),
    url_name character varying(255)
);


--
-- Name: videos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE videos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: videos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE videos_id_seq OWNED BY videos.id;


--
-- Name: videos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('videos_id_seq', 20, true);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lingues ALTER COLUMN id SET DEFAULT nextval('lingues_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY media ALTER COLUMN id SET DEFAULT nextval('media_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY media_videos ALTER COLUMN id SET DEFAULT nextval('media_videos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_cat_smls ALTER COLUMN id SET DEFAULT nextval('mod_cat_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_cats ALTER COLUMN id SET DEFAULT nextval('mod_cats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mod_smls ALTER COLUMN id SET DEFAULT nextval('mod_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mods ALTER COLUMN id SET DEFAULT nextval('mods_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY servizis ALTER COLUMN id SET DEFAULT nextval('servizis_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY static_page_smls ALTER COLUMN id SET DEFAULT nextval('static_page_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY static_pages ALTER COLUMN id SET DEFAULT nextval('static_pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY translation_smls ALTER COLUMN id SET DEFAULT nextval('translation_smls_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY translations ALTER COLUMN id SET DEFAULT nextval('translations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY upload_folders ALTER COLUMN id SET DEFAULT nextval('upload_folders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY uploads ALTER COLUMN id SET DEFAULT nextval('uploads_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY videos ALTER COLUMN id SET DEFAULT nextval('videos_id_seq'::regclass);


--
-- Data for Name: lingues; Type: TABLE DATA; Schema: public; Owner: -
--

COPY lingues (id, cod, lingua, active, created_at, updated_at) FROM stdin;
1	it	Italiano	1	2012-11-08 10:06:53.342303	2012-11-08 10:06:53.342303
\.


--
-- Data for Name: media; Type: TABLE DATA; Schema: public; Owner: -
--

COPY media (id, fkfile, fkmod, created_at, updated_at, descrizione, ordine) FROM stdin;
164	29	11	2012-11-22 15:47:17.365991	2012-11-22 15:47:17.365991	\N	0
165	28	12	2012-11-22 15:47:24.297727	2012-11-22 15:47:24.297727	\N	0
166	28	3	2012-11-22 15:51:24.27114	2012-11-22 15:51:24.27114	\N	0
167	29	3	2012-11-22 15:51:24.276778	2012-11-22 15:51:24.276778	\N	0
168	36	13	2012-12-13 16:02:56.525261	2012-12-13 16:02:56.525261	\N	0
169	32	14	2012-12-13 16:03:32.389903	2012-12-13 16:03:32.389903	\N	0
170	35	15	2012-12-13 16:03:57.555728	2012-12-13 16:03:57.555728	\N	0
172	39	18	2012-12-13 16:05:16.45786	2012-12-13 16:05:16.45786	\N	0
173	31	19	2012-12-13 16:05:43.604711	2012-12-13 16:05:43.604711	\N	0
174	38	20	2012-12-13 16:06:19.328594	2012-12-13 16:06:19.328594	\N	0
175	37	21	2012-12-13 16:06:54.65078	2012-12-13 16:06:54.65078	\N	0
176	33	22	2012-12-13 16:07:18.724664	2012-12-13 16:07:18.724664	\N	0
177	38	35	2013-03-14 10:53:06.949932	2013-03-14 10:53:06.949932	\N	0
178	34	36	2013-03-14 10:54:01.719634	2013-03-14 10:54:01.719634	\N	0
179	35	36	2013-03-14 10:54:01.724431	2013-03-14 10:54:01.724431	\N	0
180	36	36	2013-03-14 10:54:01.7276	2013-03-14 10:54:01.7276	\N	0
181	34	16	2013-07-08 10:02:46.489531	2013-07-08 10:06:32.635469	fasdf	2
182	36	16	2013-07-08 10:02:46.494886	2013-07-08 10:06:34.322149	asdfasdf	1
183	40	23	2013-07-15 15:15:22.758363	2013-07-15 15:16:08.98777	FARMACEUTICA	0
\.


--
-- Data for Name: media_videos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY media_videos (id, fkvideo, fkmod, descrizione, created_at, updated_at) FROM stdin;
8	16	8	rugby	2012-11-16 16:53:59.107313	2012-11-16 16:53:59.107313
9	17	8	spot	2012-11-16 16:54:05.729308	2012-11-16 16:54:05.729308
12	18	2	torta	2012-11-16 16:54:22.051043	2012-11-16 16:54:22.051043
13	19	2	mercato	2012-11-16 16:54:30.381283	2012-11-16 16:54:30.381283
21	18	10	test	2012-11-16 17:06:28.862281	2012-11-16 17:06:28.862281
\.


--
-- Data for Name: mod_cat_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_cat_smls (id, fkparent, fklang, title, abstract, description, created_at, updated_at) FROM stdin;
1	1	it	Home Page			2012-11-08 10:12:19.847346	2012-11-08 10:12:19.847346
6	8	it	new tech			2012-11-12 13:55:13.337737	2012-11-12 13:55:13.337737
7	9	it	etc			2012-11-12 13:55:17.420343	2012-11-12 13:55:17.420343
8	10	it	banner home			2012-11-22 15:25:57.793287	2012-11-22 15:25:57.793287
9	11	it	Categoria principale settori			2013-03-11 14:07:03.01533	2013-03-11 14:07:03.01533
10	12	it	Categoria principale applicazioni			2013-03-11 14:10:19.433234	2013-03-11 14:10:19.433234
2	2	it	Enologia			2012-11-08 10:25:33.883798	2013-03-11 14:25:01.759672
3	5	it	Industria			2012-11-08 11:06:18.469259	2013-03-11 14:26:37.473871
4	6	it	RFID			2012-11-12 13:55:03.310651	2013-03-11 14:26:47.03269
5	7	it	Special Apps			2012-11-12 13:55:08.476802	2013-03-11 14:26:56.333184
11	13	it	Categoria blog			2013-03-14 10:47:17.960801	2013-03-14 10:47:17.960801
\.


--
-- Data for Name: mod_cats; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_cats (id, idserv, f_del, ordine, fkparent, created_at, updated_at, published, settori, applicazioni) FROM stdin;
1	2	1	\N	0	2012-11-08 10:12:19.836172	2012-11-09 10:45:35.180418	\N	\N	\N
10	4	0	\N	0	2012-11-22 15:25:57.785238	2012-11-22 15:25:57.785238	1	\N	\N
11	5	0	\N	0	2013-03-11 14:07:03.009558	2013-03-11 14:07:03.009558	1	\N	\N
12	6	0	\N	0	2013-03-11 14:10:19.430324	2013-03-11 14:10:19.430324	1	\N	\N
8	3	1	\N	0	2012-11-12 13:55:13.333891	2013-03-11 14:27:00.719572	1	\N	\N
9	3	1	\N	0	2012-11-12 13:55:17.416183	2013-03-11 14:27:03.131103	1	\N	\N
13	7	0	\N	0	2013-03-14 10:47:17.951971	2013-03-14 10:47:17.951971	1	\N	\N
2	3	0	1	0	2012-11-08 10:25:33.877003	2013-03-14 13:08:20.67714	1	23#24#26	31#30
7	3	0	4	0	2012-11-12 13:55:08.472597	2013-03-14 13:08:22.578045	1	\N	\N
5	3	0	2	0	2012-11-08 11:06:18.44113	2013-07-08 08:12:37.260733	1	23#24	34#33
6	3	0	3	0	2012-11-12 13:55:03.16869	2013-07-08 08:12:37.265127	1	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
\.


--
-- Data for Name: mod_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mod_smls (id, fkparent, fklang, url_title, title, abstract, description, created_at, updated_at) FROM stdin;
1	1	it	Slogan	Slogan	Costruiamo soluzioni	\N	2012-11-08 10:12:43.966402	2012-11-08 10:12:43.966402
2	2	it	Primo-elemento-industria	Primo elemento industria	\N	testo primo elemento industria	2012-11-08 10:26:01.80017	2012-11-08 10:26:01.80017
3	3	it	Elemento-industria	Elemento industria	\N	testo elemento industria	2012-11-08 10:49:54.741358	2012-11-08 10:49:54.741358
4	4	it	test	test	\N	test	2012-11-08 10:57:28.557509	2012-11-08 10:57:28.557509
5	5	it	Ampliamenti	Ampliamenti	\N	asdf asdfa sf e	2012-11-08 10:58:55.264961	2012-11-08 10:58:55.264961
6	6	it	asfgsdg	asfgsdg	\N	sdfgsdgrgr	2012-11-08 11:00:40.802295	2012-11-08 11:00:40.802295
7	7	it	atatrert-a	atatrert a	\N	a fga r r rr r	2012-11-08 11:02:02.49621	2012-11-08 11:02:02.49621
8	8	it	test	test	\N	test	2012-11-08 15:33:04.201089	2012-11-08 15:33:04.201089
9	9	it	tartwert-wer	tartwert wer	\N	t wert wert werte	2012-11-08 17:04:10.565961	2012-11-08 17:04:10.565961
10	10	it	test-NFC	test NFC	\N	test	2012-11-12 16:54:42.801657	2012-11-12 16:54:42.801657
11	11	it	Il-team	Il team	\N	il team	2012-11-22 15:26:14.012826	2012-11-22 15:36:23.616491
12	12	it	banner-2	banner 2	\N		2012-11-22 15:38:11.842039	2012-11-22 15:38:11.842039
13	13	it	Industry-01	Industry 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:02:30.019362	2012-12-13 16:02:30.019362
14	14	it	Industry-02	Industry 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:03:10.180798	2012-12-13 16:03:10.180798
15	15	it	Industry-03	Industry 03	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:03:51.951439	2012-12-13 16:03:51.951439
16	16	it	NFC-01	NFC 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:04:20.707184	2012-12-13 16:04:20.707184
17	17	it	NFC-02	NFC 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:04:42.090306	2012-12-13 16:04:42.090306
49	49	it	Biglietti-da-visita	Biglietti da visita	\N	\N	2013-03-26 10:23:08.40304	2013-03-26 10:23:08.40304
50	50	it	Anti-contraffazione	Anti-contraffazione	\N	\N	2013-03-26 10:23:18.793612	2013-03-26 10:23:18.793612
51	51	it	Anti-taccheggio	Anti-taccheggio	\N	\N	2013-03-26 10:23:26.393134	2013-03-26 10:23:26.393134
52	52	it	Auto-adesive-main-purpose	Auto-adesive main purpose	\N	\N	2013-03-26 10:23:36.733594	2013-03-26 10:23:36.733594
53	53	it	Pubblicit%C3%A1	Pubblicitá	\N	\N	2013-03-26 10:23:44.865553	2013-03-26 10:23:44.865553
20	20	it	RFID-01	RFID 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:06:08.476915	2012-12-13 16:06:08.476915
21	21	it	New-Tech-01	New Tech 01	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:06:45.233553	2012-12-13 16:06:45.233553
22	22	it	New-Tech-02	New Tech 02	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:07:10.605995	2012-12-13 16:07:10.605995
23	23	it	Farmaceutica	Farmaceutica	\N	\N	2013-03-11 14:08:49.669845	2013-03-11 14:08:49.669845
24	24	it	Chimica-e-prodotti-casa	Chimica e prodotti casa	\N	\N	2013-03-11 14:09:05.984938	2013-03-11 14:09:05.984938
25	25	it	Tecnics-&-Electronics	Tecnics & Electronics	\N	\N	2013-03-11 14:09:18.266966	2013-03-11 14:09:18.266966
26	26	it	Food-&-Beverage	Food & Beverage	\N	\N	2013-03-11 14:09:29.80222	2013-03-11 14:09:29.80222
27	27	it	Automotive	Automotive	\N	\N	2013-03-11 14:09:38.208682	2013-03-11 14:09:38.208682
28	28	it	Cosmetica-&-body-care	Cosmetica & body care	\N	\N	2013-03-11 14:09:51.927192	2013-03-11 14:09:51.927192
29	29	it	Logistica	Logistica	\N	\N	2013-03-11 14:10:03.385038	2013-03-11 14:10:03.385038
30	30	it	Sicurezza	Sicurezza	\N	\N	2013-03-11 14:10:28.526453	2013-03-11 14:10:28.526453
31	31	it	Anti-contraffazione	Anti contraffazione	\N	\N	2013-03-11 14:10:36.885981	2013-03-11 14:10:36.885981
32	32	it	Decorative	Decorative	\N	\N	2013-03-11 14:10:45.142182	2013-03-11 14:10:45.142182
33	33	it	Funzionali	Funzionali	\N	\N	2013-03-11 14:10:49.83411	2013-03-11 14:10:49.83411
34	34	it	Promozionali	Promozionali	\N	\N	2013-03-11 14:10:58.511456	2013-03-11 14:10:58.511456
35	35	it	Test	Test	\N	test	2013-03-14 10:47:32.600754	2013-03-14 10:47:32.600754
36	36	it	altro-elemento-blog	altro elemento blog	\N	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum aliquet justo quis massa commodo vel lacinia lectus malesuada. Mauris et nibh sed erat tempor aliquam. Suspendisse eleifend nunc sit amet risus ornare posuere. Donec sollicitudin dictum risus sit amet suscipit. Fusce vitae magna massa, sit amet semper lectus. Etiam sagittis nisl at dolor accumsan elementum. Sed a arcu sapien, ac adipiscing quam. Sed malesuada nibh vitae leo pellentesque facilisis. Donec metus turpis, rutrum sed blandit nec, rutrum id dolor. </p><p>Pellentesque mattis egestas lacinia. Sed in felis at nibh commodo adipiscing et eget ipsum. Curabitur porttitor mollis nulla, et gravida diam convallis sit amet. Nulla sed diam et lectus tempus hendrerit. Phasellus in lacus eu ante rutrum ultricies. Nunc nunc dui, placerat non imperdiet eget, luctus sed purus. Maecenas ante diam, rhoncus id accumsan vel, ultrices vel metus. Mauris placerat dui et lorem commodo porttitor. Aliquam aliquam metus in ipsum adipiscing ullamcorper. Cras fringilla eros eu purus condimentum feugiat faucibus purus mollis. Phasellus purus eros, condimentum scelerisque congue eget, tincidunt eget ante. Donec blandit velit facilisis odio tincidunt venenatis dictum leo vulputate. Cras vel orci lorem, ut volutpat mauris. </p>	2013-03-14 10:53:55.518091	2013-03-14 10:54:04.002704
38	38	it	Pubblica-amministrazione	Pubblica amministrazione	\N	\N	2013-03-26 10:20:42.474317	2013-03-26 10:20:42.474317
39	39	it	Gestione-aziendale	Gestione aziendale	\N	\N	2013-03-26 10:20:57.77247	2013-03-26 10:20:57.77247
40	40	it	Gestione-consumer/negozi	Gestione consumer/negozi	\N	\N	2013-03-26 10:21:08.971648	2013-03-26 10:21:08.971648
41	41	it	Sport	Sport	\N	\N	2013-03-26 10:21:18.3802	2013-03-26 10:21:18.3802
42	42	it	Sanit%C3%A1	Sanitá	\N	\N	2013-03-26 10:21:25.876064	2013-03-26 10:21:25.876064
43	43	it	Gestione-eventi	Gestione eventi	\N	\N	2013-03-26 10:21:34.487536	2013-03-26 10:21:34.487536
44	44	it	Gestione-accessi-veicoli/persone	Gestione accessi veicoli/persone	\N	\N	2013-03-26 10:22:10.44681	2013-03-26 10:22:10.44681
45	45	it	Ticketing	Ticketing	\N	\N	2013-03-26 10:22:21.283512	2013-03-26 10:22:21.283512
46	46	it	Gestione-rifiuti	Gestione rifiuti	\N	\N	2013-03-26 10:22:39.900137	2013-03-26 10:22:39.900137
47	47	it	Logistica	Logistica	\N	\N	2013-03-26 10:22:47.348318	2013-03-26 10:22:47.348318
48	48	it	Asset-Tracking	Asset Tracking	\N	\N	2013-03-26 10:22:58.028825	2013-03-26 10:22:58.028825
37	37	it	Tag-hf-adesivi	Tag hf adesivi	\N		2013-03-26 09:30:32.985503	2013-03-26 10:33:20.783407
54	54	it	Timing	Timing	\N	\N	2013-03-26 10:23:52.609394	2013-03-26 10:23:52.609394
55	55	it	Gestione-e-controllo-linee-di-produzione	Gestione e controllo linee di produzione	\N	\N	2013-03-26 10:24:04.881129	2013-03-26 10:24:04.881129
18	18	it	Tag-Adesivi	Tag Adesivi	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:05:08.1442	2013-03-26 10:32:23.849422
19	19	it	Tag-uhf-adesivi	Tag uhf adesivi	\N	&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque urna diam, consectetur nec aliquam id, rutrum in eros. Vestibulum et dolor lorem. Sed et nisl a dolor pellentesque elementum. Quisque tempus turpis et libero vehicula id pulvinar arcu mollis. Cras sed ante nec felis adipiscing condimentum. Aliquam erat volutpat. Aliquam eget diam lacus, sed convallis nibh. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;<br /><br />Integer at ante libero, vel varius felis. Cras purus sapien, ullamcorper suscipit commodo quis, pharetra tristique est. Phasellus placerat imperdiet sem, a dictum arcu dignissim ut. Aenean sed mollis nulla. Maecenas varius, nisl cursus dictum eleifend, neque ante tristique lectus, non fermentum dui tortor venenatis orci. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean arcu arcu, sagittis a tristique at, accumsan dignissim enim. <br />	2012-12-13 16:05:37.720639	2013-03-26 10:32:49.778227
56	56	it	Tag-ufh-bracciale	Tag ufh bracciale	\N		2013-03-26 10:35:38.56825	2013-03-26 10:35:38.56825
57	57	it	Keyfob	Keyfob	\N		2013-03-26 10:36:12.871913	2013-03-26 10:36:12.871913
58	58	it	Tag-on-metal	Tag on metal	\N		2013-03-26 10:36:38.935847	2013-03-26 10:36:38.935847
59	59	it	Biglietti-da-visita-NFC	Biglietti da visita NFC	\N		2013-03-26 10:37:11.573739	2013-03-26 10:37:11.573739
60	60	it	Adesivi-NFC	Adesivi NFC	\N		2013-03-26 10:37:36.221532	2013-03-26 10:37:36.221532
\.


--
-- Data for Name: mods; Type: TABLE DATA; Schema: public; Owner: -
--

COPY mods (id, idserv, fkcat, home, f_del, ordine, created_at, updated_at, published, data_evento, etc, settori, applicazioni) FROM stdin;
1	2	1	\N	0	\N	2012-11-08 10:12:43.957083	2012-11-08 10:12:43.957083	\N	\N	slogan_home	\N	\N
2	3	2	\N	1	\N	2012-11-08 10:26:01.764873	2012-11-08 10:40:30.898614	\N	\N	\N	\N	\N
11	4	10	\N	0	\N	2012-11-22 15:26:13.942511	2012-11-22 15:26:29.601736	1	\N	\N	\N	\N
12	4	10	\N	0	\N	2012-11-22 15:38:11.836707	2012-11-22 15:38:35.587019	1	\N	\N	\N	\N
3	3	2	\N	1	1	2012-11-08 10:49:54.68346	2012-12-13 15:51:37.097381	1	\N	\N	\N	\N
4	3	2	\N	1	2	2012-11-08 10:57:28.518635	2012-12-13 15:51:37.101385	1	\N	\N	\N	\N
6	3	2	\N	1	3	2012-11-08 11:00:40.765318	2012-12-13 15:51:37.104884	1	\N	\N	\N	\N
9	3	2	\N	1	4	2012-11-08 17:04:10.559746	2012-12-13 15:51:37.108358	\N	\N	\N	\N	\N
8	3	2	\N	1	5	2012-11-08 15:33:04.167152	2012-12-13 15:51:37.112293	\N	\N	\N	\N	\N
5	3	2	\N	1	6	2012-11-08 10:58:55.251288	2012-12-13 15:51:37.116077	\N	\N	\N	\N	\N
7	3	2	\N	1	7	2012-11-08 11:02:02.488112	2012-12-13 15:51:37.119724	\N	\N	\N	\N	\N
10	3	5	\N	1	\N	2012-11-12 16:54:42.72813	2012-12-13 15:51:49.326578	1	\N	\N	\N	\N
16	3	5	\N	0	\N	2012-12-13 16:04:20.703059	2012-12-13 16:04:30.716099	1	\N	\N	\N	\N
20	3	7	\N	0	\N	2012-12-13 16:06:08.472256	2012-12-13 16:06:29.257558	1	\N	\N	\N	\N
21	3	8	\N	0	\N	2012-12-13 16:06:45.228853	2012-12-13 16:07:00.695993	1	\N	\N	\N	\N
22	3	8	\N	0	\N	2012-12-13 16:07:10.601111	2012-12-13 16:07:23.339472	1	\N	\N	\N	\N
24	5	11	\N	0	2	2013-03-11 14:09:05.981496	2013-03-11 14:09:34.931715	1	\N	\N	\N	\N
25	5	11	\N	0	3	2013-03-11 14:09:18.263761	2013-03-11 14:09:34.93484	1	\N	\N	\N	\N
27	5	11	\N	0	\N	2013-03-11 14:09:38.205682	2013-03-11 14:09:44.414548	1	\N	\N	\N	\N
26	5	11	\N	0	4	2013-03-11 14:09:29.798968	2013-03-11 14:09:55.062861	1	\N	\N	\N	\N
28	5	11	\N	0	\N	2013-03-11 14:09:51.924073	2013-03-11 14:09:56.956659	1	\N	\N	\N	\N
29	5	11	\N	0	\N	2013-03-11 14:10:03.382042	2013-03-11 14:10:08.262073	1	\N	\N	\N	\N
30	6	12	\N	0	\N	2013-03-11 14:10:28.523377	2013-03-11 14:10:31.432277	1	\N	\N	\N	\N
31	6	12	\N	0	\N	2013-03-11 14:10:36.882883	2013-03-11 14:11:01.301307	1	\N	\N	\N	\N
32	6	12	\N	0	\N	2013-03-11 14:10:45.138343	2013-03-11 14:11:02.6298	1	\N	\N	\N	\N
33	6	12	\N	0	\N	2013-03-11 14:10:49.831121	2013-03-11 14:11:03.712105	1	\N	\N	\N	\N
34	6	12	\N	0	\N	2013-03-11 14:10:58.507916	2013-03-11 14:11:04.622113	1	\N	\N	\N	\N
13	3	2	\N	0	1	2012-12-13 16:02:29.970917	2013-03-11 17:16:23.516931	1	\N	\N	23#24#26	31#30
14	3	2	\N	0	2	2012-12-13 16:03:10.175649	2013-03-11 17:16:23.519611	1	\N	\N	23#24	31#30
15	3	2	\N	0	3	2012-12-13 16:03:51.947503	2013-03-11 17:16:23.522511	1	\N	\N	23#24	34
35	7	13	\N	0	\N	2013-03-14 10:47:32.594624	2013-03-14 10:53:11.926754	1	\N	\N	\N	\N
36	7	13	\N	0	\N	2013-03-14 10:53:55.514167	2013-03-14 10:54:07.020179	1	\N	\N	\N	\N
38	5	11	\N	0	\N	2013-03-26 10:20:42.107172	2013-03-26 10:20:48.07502	1	\N	\N	\N	\N
39	5	11	\N	0	\N	2013-03-26 10:20:56.83225	2013-03-26 10:21:48.650481	1	\N	\N	\N	\N
40	5	11	\N	0	\N	2013-03-26 10:21:08.436205	2013-03-26 10:21:50.631294	1	\N	\N	\N	\N
42	5	11	\N	0	\N	2013-03-26 10:21:25.76584	2013-03-26 10:21:52.553032	1	\N	\N	\N	\N
41	5	11	\N	0	\N	2013-03-26 10:21:18.01072	2013-03-26 10:21:53.7899	1	\N	\N	\N	\N
43	5	11	\N	0	\N	2013-03-26 10:21:34.38784	2013-03-26 10:21:54.948506	1	\N	\N	\N	\N
55	6	12	\N	0	\N	2013-03-26 10:24:04.864244	2013-03-26 10:24:09.801175	1	\N	\N	\N	\N
45	6	12	\N	0	\N	2013-03-26 10:22:18.044142	2013-03-26 10:24:12.765038	1	\N	\N	\N	\N
47	6	12	\N	0	\N	2013-03-26 10:22:47.274241	2013-03-26 10:24:14.484244	1	\N	\N	\N	\N
49	6	12	\N	0	\N	2013-03-26 10:23:08.376326	2013-03-26 10:24:16.06771	1	\N	\N	\N	\N
51	6	12	\N	0	\N	2013-03-26 10:23:26.361855	2013-03-26 10:24:17.293589	1	\N	\N	\N	\N
52	6	12	\N	0	\N	2013-03-26 10:23:36.701469	2013-03-26 10:24:19.446428	1	\N	\N	\N	\N
44	6	12	\N	0	\N	2013-03-26 10:22:09.797278	2013-03-26 10:24:22.562996	1	\N	\N	\N	\N
46	6	12	\N	0	\N	2013-03-26 10:22:38.889332	2013-03-26 10:24:24.696929	1	\N	\N	\N	\N
48	6	12	\N	0	\N	2013-03-26 10:22:58.001792	2013-03-26 10:24:26.176207	1	\N	\N	\N	\N
50	6	12	\N	0	\N	2013-03-26 10:23:18.773007	2013-03-26 10:24:28.04026	1	\N	\N	\N	\N
53	6	12	\N	0	\N	2013-03-26 10:23:44.115722	2013-03-26 10:24:30.560053	1	\N	\N	\N	\N
54	6	12	\N	0	\N	2013-03-26 10:23:51.193874	2013-03-26 10:24:33.360833	1	\N	\N	\N	\N
18	3	6	\N	0	\N	2012-12-13 16:05:08.139933	2013-03-26 10:32:23.816497	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
19	3	6	\N	0	\N	2012-12-13 16:05:37.716083	2013-03-26 10:32:49.753428	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
37	3	6	\N	0	\N	2013-03-26 09:30:32.682323	2013-03-26 10:33:26.909992	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
56	3	6	\N	0	\N	2013-03-26 10:35:38.539259	2013-03-26 10:35:54.295258	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
57	3	6	\N	0	\N	2013-03-26 10:36:12.845089	2013-03-26 10:36:18.874686	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
58	3	6	\N	0	\N	2013-03-26 10:36:38.913653	2013-03-26 10:36:43.983417	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
59	3	6	\N	0	\N	2013-03-26 10:37:11.545694	2013-03-26 10:37:15.851391	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
60	3	6	\N	0	\N	2013-03-26 10:37:36.193234	2013-03-26 10:37:41.125934	1	\N	\N	43#42#41#40#39#38	55#54#53#52#51#50#49#48#47#46#45#44
17	3	5	\N	0	\N	2012-12-13 16:04:42.085221	2013-07-08 10:06:48.487506	0	\N	\N	\N	\N
23	5	11	\N	0	1	2013-03-11 14:08:49.664351	2013-07-15 15:24:09.992271	1	\N	\N	\N	\N
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY schema_migrations (version) FROM stdin;
20120905160159
20120906075651
20120907092435
20120907095401
20120907095636
20120907095854
20120907100028
20120913125449
20120914155318
20120918075741
20120918080253
20120918080307
20120918080317
20120918080328
20120918080715
20120920074505
20120920101331
20120920135502
20120929141337
20121001153131
20121008161229
20121008161428
20121008162633
20121008163653
20121114094411
\.


--
-- Data for Name: servizis; Type: TABLE DATA; Schema: public; Owner: -
--

COPY servizis (id, fkparent, nome, label, enabled, ordine, path, skip_cat, created_at, updated_at) FROM stdin;
1	0	home	Home	1	1	/	\N	2012-11-08 10:06:46.584074	2012-11-08 10:06:46.584074
4	2	banner	Banner home	1	\N	/admin/mods?idserv=4	1	2012-11-22 15:25:49.875	2012-11-22 15:25:49.914521
5	2	settori	Gestione Settori	1	\N	/admin/mods?idserv=5	1	2013-03-11 14:06:26.792245	2013-03-11 14:06:26.800847
6	2	applicazioni	Gestione Applicazioni	1	\N	/admin/mods?idserv=6	1	2013-03-11 14:06:38.300794	2013-03-11 14:06:38.30317
7	2	blog	Gestione Blog	1	\N	/admin/mods?idserv=7	1	2013-03-14 10:46:25.388882	2013-03-14 10:47:05.068167
3	2	produzione	Produzione	1	\N	/admin/mods?idserv=3	1	2012-11-08 10:22:35.062329	2013-03-26 09:43:37.57577
\.


--
-- Data for Name: static_page_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY static_page_smls (id, title, subtitle, header, testo, footer, created_at, updated_at, static_page_id, fklang) FROM stdin;
4	Innovazione	\N	\N	In Rotas la capacità produttiva è un fatto strategico. I nostri macchinari sono “nostri” nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. A misura delle vostre esigenze. <br /><p style="color:#000;"><br />La capacità di disporre di un approccio globale, vi consente soluzioni semplici, o ricche di unicità e specificità. La produzione Rotas, lavora in stretto rapporto con l'ideazione. Per questo è in grado di creare soluzioni che affrontano tutti gli elementi della tecnologia: il <span style="background-color:;font-weight:bold;">materiale</span>, la <span style="background-color:;font-weight:bold;">superficie</span>, l'<span style="background-color:;font-weight:bold;">inchiostro</span>, l'<span style="background-color:;font-weight:bold;">adesivo</span>, il <span style="background-color:;font-weight:bold;">supporto</span>, <span style="background-color:;font-weight:bold;">rfid</span>. </p>	\N	2013-05-27 09:23:08.653094	2013-05-27 10:02:38.45361	4	it
3	Storia	\N	\N	<p style="color:#000;"><strong>40 anni di esperienza</strong> ci hanno portato a un modo originale di affrontare le opportunità di vestizione dei vostri prodotti. La nostra unicità, è di creare plus valore attraverso elementi di comunicazione, agendo sulle singole basi della tecnologia</p><p style="color:#000;"><span style="background-color:;font-weight:bold;">ROTAS</span>, crea un vestito ai vostri prodotti, dando un valore non solo estetico. Affianca i vostri creativi nell'individuare le soluzioni tecniche che creano plus valore per catturare l'attenzione, differenziare i prodotti e dare un'immagine di prima qualità e perciò di maggior valore. </p><p style="color:#000;">In <span style="background-color:;font-weight:bold;">Rotas</span> la capacità produttiva è un fatto strategico. I nostri macchinari sono "nostri" nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. </p><p style="color:#000;"><span style="background-color:;font-weight:bold;">A misura delle vostre esigenze.</span></p>	\N	2013-05-27 09:22:47.840327	2013-07-08 08:10:18.348109	3	it
6	Rotas Institute	\N	\N	testo rotas institute<br />	\N	2013-05-27 12:10:07.319283	2013-05-27 12:10:07.319283	6	it
1	-1- ... da 40 anni i primi	\N	\N	<p style="color:#000;"><span style="background-color:;font-weight:bold;">Abbiamo sempre contribuito</span> a creare le macchine nuove per la produzione e la stampa di etichette autoadesive perché abbiamo ideato prodotti che non esistevano, anticipando le richieste del mercato e gli sviluppi dei consumi. </p><h4>1966</h4><p class="ef-small-title" style="color:#000;">costruiamo la nostra <span style="background-color:;font-weight:bold;">prima macchina per stampare in bobina</span> </p><h4>1967</h4><p class="ef-small-title" style="color:#000;">Nasce Rotas <span style="background-color:;font-weight:bold;">prima azienda</span> in italia <span style="background-color:;font-weight:bold;">a produrre esclusivamente etichette adesive in bobina</span> </p><h4>1980</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">primo brevetto</span> (immagini e disegni 3D) </p><h4>1982</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rotas certificata da UL</span> in Italia e Spagna (sicurezza prodotti USA) </p><h4>1989</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta adesiva per enologia.</span> </p><h4>1992</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">primo "label-Book"</span> (etichetta multipagina) </p><h4>1993</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rototac, la prima macchina elettrica semiautomatica</span> per applicare etichette e marcare lotto </p><h4>1999</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta scratch-off</span> (tessere telefoniche) </p><h4>2001</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima etichetta RFID</span> (industria farmaceutica) </p><h4>2005</h4><p class="ef-small-title" style="color:#000;">nasce la nuova sede produttiva in Spagna, <span style="background-color:;font-weight:bold;">Rotas Ibérica</span> </p><h4>2006</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">prima antenna RFID totalmente disegnata e prodotta da Rotas</span> </p><h4>2006</h4><p class="ef-small-title" style="color:#000;">Creaiamo un'<span style="background-color:;font-weight:bold;">azienda esclusivamdnte dedicata a</span> sistemi e soluzioni <span style="background-color:;font-weight:bold;">RFID</span> </p><h4>2007</h4><p class="ef-small-title" style="color:#000;">prima etichetta RFID per <span style="background-color:;font-weight:bold;">applicazione sotterranea</span> (condotti gas) </p><h4>2009</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Biolabel (R)</span> l'etichetta piú ecologica (fatta di pietra) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Hercules (R)</span>, l'etichetta per bottiglie fredde e umide (ance per il cemento) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">Rotas Pipe</span>, (sistema RFID per composti medicinali) </p><h4>2010</h4><p class="ef-small-title" style="color:#000;">Membri dell'Istituto di Centromarca per la Difesa e l'Identificazione di Marchi Autentici e Lotta alla Contraffazione <span style="background-color:;font-weight:bold;">(INDICAM)</span> </p><h4>2011</h4><p class="ef-small-title" style="color:#000;">Patent applications: <span style="background-color:;font-weight:bold;">Rotas Building Tracking</span> (sistema per la tracciabilità nell'edilizia). Patent pending: <span style="background-color:;font-weight:bold;">Rotas Art</span> (innovativo tablet videoguida per musei). </p><h4>2012</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">NFC Tag</span> integrati in biglietti da visita ed etichette, da leggere con smartphone. </p><h4>2012</h4><p class="ef-small-title" style="color:#000;"><span style="background-color:;font-weight:bold;">1ª etichetta con Brillante.</span> </p>	\N	2013-05-27 08:59:38.396125	2013-07-08 08:04:53.603157	1	it
7	Bandi	\N	\N	<span style="background-color:#ffffff;">Testo rotas bandi</span><br />	\N	2013-05-27 12:10:20.553632	2013-07-08 08:10:50.459917	7	it
5	Company Profile	\N	\N	<p style="color:#000;"><span style="background-color:;font-weight:bold;">ROTAS</span>, crea un vestito ai vostri prodotti, dando un valore non solo estetico. Affianca i vostri creativi nell'individuare le soluzioni tecniche che creano plus valore per catturare l'attenzione, differenziare i prodotti e dare un'immagine di prima qualità e perciò di maggior valore. </p><p style="color:#000;">In <span style="background-color:;font-weight:bold;">Rotas</span> la capacità produttiva è un fatto strategico. I nostri macchinari sono "nostri" nel senso che sono unici. Studiati e sviluppati dai tecnici Rotas per dare soluzioni ad hoc. </p><p style="color:#000;"><span style="background-color:;font-weight:bold;">A misura delle vostre esigenze.</span></p>	\N	2013-05-27 10:08:39.7419	2013-07-12 12:07:23.663855	5	it
2	Come	\N	\N	<h4>Analisi dei problemi</h4><p style="color:#000;">Siamo educati all'ascolto.<br />In Rotas, siamo addestrati ad ascoltare in modo attivo, per “scavare” insieme, per capire fino in fondo le vostre esigenze, per individuare i vostri problemi, per mettere a fuoco il problema stesso. È parte integrante del nostro lavoro.<br />Spesso un differente angolo di visione, una nuova ottica amplia il campo visivo e fa emergere nuovi elementi.</p><h4>Problem solving</h4><p style="color:#000;">La soluzione creativa. Intuizione o professionalità?<br />Per dare soluzioni su misura e a richiesta, Rotas non si affida solo al “colpo di genio” che spesso ci vuole. Conoscenza delle tecniche e metodo di lavoro ci consentono di disporre di un patrimonio creativo che si basa sulle tecniche di IMAGINERING.<br />Immaginazione finalizzata alla soluzione.</p><h4>Soluzione lavori</h4><p style="color:#000;">Quasi 50 anni di attività: un'esperienza che ci consente di affrontare le opportunità in modo ampio. ”La” soluzione anticipa spesso “le” soluzioni.<br />Rotas affianca il cliente nel trovare insieme un equilibrio tra la qualità delle proposte, i costi, i tempi.<br />Il nostro patrimonio è anche la capacità di essere semplici, sintetici e pratici per individuare un percorso produttivo, capace di favorire i vostri prodotti con un approccio efficace e fattibile. Siamo un'azienda che produce direttamente. Il nostro punto di forza è la ricerca e l'innovazione, cioè lo sviluppo delle più moderne tecnologie e di nuovi prodotti. Siamo titolari di brevetti da molti anni.</p><h4>Pianificazione progetto</h4><p style="color:#000;">Fare le cose giuste, al momento giusto, nei tempi giusti. La qualità di coordinare rapidamente le esigenze del cliente con le possibilità produttive è un servizio fondamentale; i problemi si auto alimentano perché non vengono ben capiti. Così i rischi di interpretazione aumentano e le soluzioni si allontanano. Rotas ha individuato un sistema lavoro con un buon livello di pianificazione e con un sistema informatico totalmente integrato. Definizione delle caratteristiche del prodotto, definizione del disegno con l'approvazione del cliente, produzione e consegna.</p>	\N	2013-05-27 09:05:51.817487	2013-07-12 12:07:13.259919	2	it
8	Il Materiale	\N	\N	Le nuove tecnologie, e la capacità di studiare l'etichetta come un ... sistema integrato, ci consentono di utilizzare i più differenti materiali: dall'alluminio alla classica carta vergata, dal film alla carta riciclata, dai materiali resistenti alle alte temperature alla completa fragilità di quelli anti-manomissione. Abbiamo anche la possibilità di far coesistere differenti materiali nella stessa etichetta.	\N	2013-07-16 10:00:33.714329	2013-07-16 10:00:33.714329	8	it
9	La superficie	\N	\N	Trattamenti speciali di superficie, possono essere una forte ... caratterizzazione del packaging. Richiedono una impegnativa ricerca per la complessa relazione che intercorre fra il trattamento ed i materiali. L'aspetto nasconde molta sostanza. Plastificazione, laminazione, verniciatura lucida-opaca, goffratura, fustellatura superficiale sia interna che nel retro, Braillle, sono solo alcuni esempi.	\N	2013-07-16 10:01:20.416308	2013-07-16 10:01:20.416308	9	it
10	Il supporto	\N	\N	Spessore, grammatura, comportamento al calore, ... resistenza alla trazione, velocità di applicazione, leggibilità delle fotocellule. Queste tra le caratteristiche che devono essere considerate per garantire alte prestazioni nel tempo. Il liner può essere anche stampato nel retro sicurezza, a rilievo, ecologici, acidocromici, ecc.	\N	2013-07-16 10:01:57.695775	2013-07-16 10:01:57.695775	10	it
11	L'adesione	\N	\N	E' il principio fondamentale che, attraverso il contatto e la ... pressione, permette di creare un legame tra due superfici. Rotas produce etichette per le più differenti condizioni di applicazione e di utilizzo ed adatte alle diverse superfici di applicazione, lisce o ruvide, a temperature estreme, all'umidità, alla resistenza chimica, con adesioni permanenti, semipermanenti, riposizionabili, rimovibili, ultrarimovibili, antimanomissione, con interazione intramolecolare.	\N	2013-07-16 10:02:31.409095	2013-07-16 10:02:31.409095	11	it
12	L'inchiostro	\N	\N	Dalla capacità di "coprire" alla forza di trasmettere un messaggio ... attivo, vivo con l'ambiente. Uso di nanotecnologie, inchiostri lucidi e opachi, termocromici, fotocromatici, magnetici, igrostatici, di sicurezza, a rilievo, ecologici, acidocromici, ecc.	\N	2013-07-16 10:02:59.264336	2013-07-16 10:02:59.264336	12	it
13	L'RFID	\N	\N	La nostra divisione RFID é specializzata nella produzione a basso costo ed in grande volume di etichette con tag RFID.	\N	2013-07-16 10:03:29.427867	2013-07-16 10:03:29.427867	13	it
\.


--
-- Data for Name: static_pages; Type: TABLE DATA; Schema: public; Owner: -
--

COPY static_pages (id, f_del, published, created_at, updated_at, foto, foto_home, url_pagina, home_page, ordine, related_cat) FROM stdin;
2	0	\N	2013-05-27 09:05:51.812489	2013-07-12 13:15:19.221211	9afa2fd2-d548.jpg	\N	/about	0	1	
4	0	\N	2013-05-27 09:23:08.65012	2013-07-12 13:15:19.24501	6182964a-c0c4.jpg	\N	/innovazione	\N	2	
5	0	\N	2013-05-27 10:08:39.704536	2013-07-12 13:15:19.252578	ea381bb3-a580.jpg	\N	/company-profile	\N	3	
6	0	\N	2013-05-27 12:10:07.279859	2013-07-12 13:15:19.261006	028d3e1e-e0c3.jpg	\N	/rotas-institute	\N	4	
7	0	\N	2013-05-27 12:10:20.545711	2013-07-12 13:15:19.302806	5e6f4b0b-b2fa.jpg	\N	/bandi	\N	5	
1	0	\N	2013-05-27 08:59:38.378063	2013-07-12 13:15:19.310957	ea45afd8-23a1.jpg	\N	/rotas	0	6	
3	0	\N	2013-05-27 09:22:47.835653	2013-07-12 13:15:19.319317	4658c820-3f5b.jpg	\N	/storia	\N	7	
8	0	\N	2013-07-16 10:00:33.676931	2013-07-16 10:00:33.676931	\N	\N	/materiale	\N	\N	
9	0	\N	2013-07-16 10:01:20.40261	2013-07-16 10:01:20.40261	\N	\N	/superficie	\N	\N	
10	0	\N	2013-07-16 10:01:57.673065	2013-07-16 10:01:57.673065	\N	\N	/supporto	\N	\N	
11	0	\N	2013-07-16 10:02:31.394079	2013-07-16 10:02:31.394079	\N	\N	/adesione	\N	\N	
12	0	\N	2013-07-16 10:02:59.242585	2013-07-16 10:02:59.242585	\N	\N	/inchiostro	\N	\N	
13	0	\N	2013-07-16 10:03:29.411037	2013-07-16 10:03:29.411037	\N	\N	/rfid	\N	\N	
\.


--
-- Data for Name: translation_smls; Type: TABLE DATA; Schema: public; Owner: -
--

COPY translation_smls (id, translation_id, testo, created_at, updated_at, fklang) FROM stdin;
\.


--
-- Data for Name: translations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY translations (id, etc, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: upload_folders; Type: TABLE DATA; Schema: public; Owner: -
--

COPY upload_folders (id, fkparent, nome, created_at, updated_at, f_del) FROM stdin;
11	0	Desktop	2012-11-14 11:06:03.142096	2012-11-14 11:06:03.142096	0
12	11	immagini banner home	2012-11-22 15:25:24.42255	2012-11-22 15:25:24.42255	0
13	11	test	2013-07-16 06:55:37.236621	2013-07-16 06:55:41.32197	1
14	11	test1	2013-07-16 06:55:45.562271	2013-07-16 06:55:56.070733	1
\.


--
-- Data for Name: uploads; Type: TABLE DATA; Schema: public; Owner: -
--

COPY uploads (id, filename, ext, content_type, file_size, created_at, updated_at, fkfolder) FROM stdin;
31	5c2cde45-ccd8.jpg	image/jpeg	\N	178467	2012-12-13 15:47:49.141993	2012-12-13 15:47:49.141993	11
32	dae89035-6402.jpg	image/jpeg	\N	240510	2012-12-13 15:47:49.702667	2012-12-13 15:47:49.702667	11
33	879decf8-b95b.jpg	image/jpeg	\N	108248	2012-12-13 15:47:50.221277	2012-12-13 15:47:50.221277	11
34	60e7d48d-af59.jpg	image/jpeg	\N	106012	2012-12-13 15:47:50.759677	2012-12-13 15:47:50.759677	11
35	c46456f5-c231.jpg	image/jpeg	\N	264411	2012-12-13 15:50:43.873748	2012-12-13 15:50:43.873748	11
36	b6077cb4-b1b6.jpg	image/jpeg	\N	237897	2012-12-13 15:51:18.400599	2012-12-13 15:51:18.400599	11
37	3f11c2bc-cc20.jpg	image/jpeg	\N	220472	2012-12-13 15:51:19.112852	2012-12-13 15:51:19.112852	11
38	bc56c4fb-9bd4.jpg	image/jpeg	\N	187696	2012-12-13 15:51:19.667434	2012-12-13 15:51:19.667434	11
39	768f21f1-fc22.jpg	image/jpeg	\N	84002	2012-12-13 16:01:57.656988	2012-12-13 16:01:57.656988	11
28	02A13AWG.jpg	image/jpeg	\N	270718	2012-11-22 15:47:04.095673	2012-11-22 15:47:04.095673	12
29	team.jpg	image/jpeg	\N	363193	2012-11-22 15:47:04.865831	2012-11-22 15:47:04.865831	12
40	05dd65d9-41fb.jpg	image/jpeg	\N	590068	2013-07-15 15:14:43.066953	2013-07-15 15:14:43.066953	11
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, name, email, created_at, updated_at, password_digest, remember_token, level, active) FROM stdin;
1	Administrator	admin@rotas.it	2012-11-08 10:06:38.109858	2012-11-08 10:06:38.109858	$2a$10$lqlG.WLcUNNeqa1TOE7m5.h7duyotk7rjUCsuWjEs0croerYoEyvq	yKMhzHX8w-j0yNCi8QRXXA	0	1
\.


--
-- Data for Name: videos; Type: TABLE DATA; Schema: public; Owner: -
--

COPY videos (id, url, img, created_at, updated_at, title, url_name) FROM stdin;
16	https://www.youtube.com/watch?v=Ro0oQtJh1Ck&#38;feature=youtube_gdata	http://img.youtube.com/vi/Ro0oQtJh1Ck/2.jpg	2012-11-16 15:18:41.257315	2012-11-16 15:18:41.257315	Fresco e Vario - Spot TV 25" La squadra di rugby	Ro0oQtJh1Ck
17	https://www.youtube.com/watch?v=-Klyw1LgDv4&#38;feature=youtube_gdata	http://img.youtube.com/vi/-Klyw1LgDv4/2.jpg	2012-11-16 15:18:41.262447	2012-11-16 15:18:41.262447	Fresco e Vario - Spot TV 25" Frigo vuoto?	-Klyw1LgDv4
18	https://www.youtube.com/watch?v=M8zRrdgiaAI&#38;feature=youtube_gdata	http://img.youtube.com/vi/M8zRrdgiaAI/2.jpg	2012-11-16 15:18:41.265651	2012-11-16 15:18:41.265651	Fresco e Vario - Spot TV 25" La torta dei tuoi sogni	M8zRrdgiaAI
19	https://www.youtube.com/watch?v=b0cdoFhlRsQ&#38;feature=youtube_gdata	http://img.youtube.com/vi/b0cdoFhlRsQ/2.jpg	2012-11-16 15:18:41.268842	2012-11-16 15:18:41.268842	Fresco & Vario - Spot TV 25" - Al mercato	b0cdoFhlRsQ
20	https://www.youtube.com/watch?v=vQIFO4R4Fcg&#38;feature=youtube_gdata	http://img.youtube.com/vi/vQIFO4R4Fcg/2.jpg	2012-11-16 15:18:41.272956	2012-11-16 15:18:41.272956	Fresco e Vario - Spot TV 7"	vQIFO4R4Fcg
\.


--
-- Name: lingues_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lingues
    ADD CONSTRAINT lingues_pkey PRIMARY KEY (id);


--
-- Name: media_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media
    ADD CONSTRAINT media_pkey PRIMARY KEY (id);


--
-- Name: media_videos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY media_videos
    ADD CONSTRAINT media_videos_pkey PRIMARY KEY (id);


--
-- Name: mod_cat_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_cat_smls
    ADD CONSTRAINT mod_cat_smls_pkey PRIMARY KEY (id);


--
-- Name: mod_cats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_cats
    ADD CONSTRAINT mod_cats_pkey PRIMARY KEY (id);


--
-- Name: mod_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mod_smls
    ADD CONSTRAINT mod_smls_pkey PRIMARY KEY (id);


--
-- Name: mods_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mods
    ADD CONSTRAINT mods_pkey PRIMARY KEY (id);


--
-- Name: servizis_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY servizis
    ADD CONSTRAINT servizis_pkey PRIMARY KEY (id);


--
-- Name: static_page_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY static_page_smls
    ADD CONSTRAINT static_page_smls_pkey PRIMARY KEY (id);


--
-- Name: static_pages_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY static_pages
    ADD CONSTRAINT static_pages_pkey PRIMARY KEY (id);


--
-- Name: translation_smls_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY translation_smls
    ADD CONSTRAINT translation_smls_pkey PRIMARY KEY (id);


--
-- Name: translations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY translations
    ADD CONSTRAINT translations_pkey PRIMARY KEY (id);


--
-- Name: upload_folders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY upload_folders
    ADD CONSTRAINT upload_folders_pkey PRIMARY KEY (id);


--
-- Name: uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY uploads
    ADD CONSTRAINT uploads_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: videos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY videos
    ADD CONSTRAINT videos_pkey PRIMARY KEY (id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

