class Mod < ActiveRecord::Base
  default_scope order: 'ordine,published'

  has_many :sml, 		:class_name => "ModSml", :foreign_key => "fkparent"

  has_many :media,	:class_name => "Media", 	:foreign_key => "fkmod"
  has_many :upload, :through => :media

  has_many :media_video, :class_name => "MediaVideo", :foreign_key => "fkmod"
  has_many :video, :through => :media_video

  def self.findTotChild(parent)
     Mod.find_all_by_fkcat_and_f_del_and_published(parent, "0", "1")
  end

  def self.findTotChildBySettore(parent,settore)
    elementi = Mod.find_all_by_fkcat_and_f_del_and_published(parent, "0", "1")

    result = []
    elementi.each do |ele|
      if ele.settori && ele.settori.include?(settore)
        result.push(ele)
      end
    end

    result
  end

  def self.findTotChildByApp(parent,app)
    elementi = Mod.find_all_by_fkcat_and_f_del_and_published(parent, "0", "1")

    result = []
    elementi.each do |ele|
      if ele.applicazioni && ele.applicazioni.include?(app)
        result.push(ele)
      end
    end

    result
  end

  def self.findByCategorie(idcat)
    realizzazioni = Mod.where("fkcat=? AND f_del=?", idcat, "0")
    lingue = Lingue.where("active=?", "1").order('id')

    result = []
    realizzazioni.each do |rea|
      obj = {}
      obj['gen'] = rea
      lingue.each do |lang|
        obj['sml'] = { lang.cod => rea.sml.where("fklang=? AND fkparent=?", "it", rea.id).first }
      end

      result.push obj
    end

    result
  end

  def self.getLastElementi(servizio, tot)
    lingue = Lingue.where("active=?", "1").order("id")
    realizzazioni = Mod.where("idserv=? and f_del=?", servizio.id, "0").limit(tot)

    result = []
    realizzazioni.each do |rea|
      obj = {}
      obj['gen'] = rea
      obj['media'] = rea.media
      lingue.each do |lang|
        obj['sml'] = { lang.cod => rea.sml.where("fklang=? AND fkparent=?", "it", rea.id ).first }
      end

      result.push obj
    end

    result
  end

  def self.getElementByUrlTitle(url_title)
		lingue = Lingue.where("active=?", "1").order("id")
  	id_elemento = url_title.split("-").first
    elemento = Mod.find(id_elemento)

    if elemento.nil?
    	return
    else
    	result = {}
      result['gen'] = elemento
      sml = {}
      lingue.each do |lang|
        sml_ele = elemento.sml.where('fklang=? AND fkparent=?', lang.cod, elemento.id).first
        result['sml'] = {lang.cod => sml_ele}
      end
    end

    result
  end

  def self.findPrevNext(ordine,fkcat,idserv)
     Mod.where("ordine=? AND fkcat=? AND idserv=?", ordine,fkcat,idserv)
  end

end
