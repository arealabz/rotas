class UploadFolder < ActiveRecord::Base
  has_many :upload, :class_name => "Upload", :foreign_key => "fkfolder"
  default_scope :conditions => ["f_del = ?", "0"], :order => "nome"

  def self.deleteFolder(id)
  	folder = UploadFolder.find(id)
  	folder.update_attributes({"f_del" => 1})
  end

end
