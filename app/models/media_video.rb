class MediaVideo < ActiveRecord::Base
	belongs_to :mod, 		:class_name => "Mod", 		:foreign_key => "fkmod"
  belongs_to :video, 	:class_name => "Video",		:foreign_key => "fkvideo"
end
