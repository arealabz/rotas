class Media < ActiveRecord::Base

  belongs_to :mod, 		:class_name => "Mod", 		:foreign_key => "fkmod"
  belongs_to :upload, :class_name => "Upload",	:foreign_key => "fkfile"

  def self.find_associated_elements?(ele)
  	media = Media.find_all_by_fkfile(ele)
  	find = false
  	media.each do |md|
  		if Mod.find(md.fmod).f_del.eql?("0")
  			find = true
  			break
  		end
  	end

  	return find
  end

end
