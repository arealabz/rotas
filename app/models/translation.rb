class Translation < ActiveRecord::Base
  attr_accessible :etc
  has_many :sml, :class_name => "TranslationSml", :foreign_key => "translation_id"
end
