# encoding: utf-8

class PicUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  include CarrierWave::RMagick
  include CarrierWave::MimeTypes

  # Choose what kind of storage to use for this uploader:
  storage :file

  process :set_content_type

  def store_dir
    #"uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    'public/uploads/'
  end

  # Create different versions of your uploaded files:
  process :resize_to_fit => [1600, 1600], :if => :image?
  version :thumb, :if => :image? do
    process :resize_to_fill => [160, 107]
  end
  version :micro, :if => :image? do
    process :resize_to_fill => [80, 50]
  end
  version :normal, :if => :image? do
    process :resize_to_fit => [800, 800]
  end

  def extension_white_list
    %w(jpg jpeg gif png pdf)
  end

  def filename
     filename ||= "#{secure_token[0..12]}.#{file.extension}" if original_filename.present?
  end

  protected
    def image?(new_file)
      new_file.content_type.include? 'image'
    end

    def secure_token
      var = :"@#{mounted_as}_secure_token"
      model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
    end

end
