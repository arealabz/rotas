class Admin::VideoController < Admin::AdminController
	before_filter :signed_in_user

  def index
    @all_video = Video.all
  end

  def update
    # youtube video
    url = "https://gdata.youtube.com/feeds/api/users/rKOM84jfI4P0pZ8RUCfp5w/uploads"
    feed = Feedzirra::Feed.fetch_and_parse(url)
    feed.entries.each do |entry|
      finish = entry.url.index("&") - 1

      if !Video.find_by_url(entry.url) 
        Video.create!(:url => entry.url, 
                      :img => "http://img.youtube.com/vi/#{entry.url[32..finish]}/2.jpg", 
                      :title => entry.title, 
                      :url_name => entry.url[32..finish])
      end      
    end
    
    render :nothing => true
  end
  
end
