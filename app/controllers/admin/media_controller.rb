class Admin::MediaController < Admin::AdminController
	before_filter :signed_in_user

  def index
  	@upload = Upload.new

    if params[:idp] && params[:idp] != "0"
      @upload_folder = UploadFolder.where("fkparent=?", params[:idp])
      @main_cat = UploadFolder.find(params[:idp])
      @idp = params[:idp]
    else
      @main_cat = UploadFolder.find_by_nome("Desktop")
      @upload_folder = UploadFolder.where("fkparent=?", @main_cat.id)
      @idp = @main_cat.id
    end

  	@all_files = Upload.where("fkfolder=?", @main_cat.id)
    @all_folder = UploadFolder.all
  end

	def create
    obj = params[:media]
    obj['fkfolder'] = params[:idp]

	Upload.create!(obj)
#		upload = Upload.new(obj)
# 	  upload.save

		render :nothing => true
	end

  def destroyall
  	del_ele = params[:id].split(',')
    del_ele.each do |ele|

      unless Media.find_associated_elements?(ele)
      	dele = Upload.find(ele)
      	dele.remove_filename!
      	dele.destroy
      else
        flash[:error] = "Alcuni file non possono essere cancellati in quanto associati a degli elementi attivi"
      end

    end

    render :nothing => true
  end

  def create_categoria
    obj = params[:categoria]

    if obj[:id].eql?("")
      categoria = UploadFolder.new(params[:categoria])
      categoria.save
    else
      categoria = UploadFolder.find(obj[:id])
      categoria.update_attributes( obj )
    end

    redirect_to "/admin/media?idp=#{categoria.fkparent}"
  end

  def delcat
    UploadFolder.deleteFolder(params[:idc])

    render :nothing => true
  end

  def movecat
    move_ele = params[:ids].split(',')
    move_ele.each do |ele|
      upload = Upload.find(ele)
      upload.update_attributes({ :fkfolder => params[:idp] })
    end

    render :json => {"idp" => params[:idp]}
  end

  ##############################################################################
  #
  #                    SEZIONE IMMAGINI
  #
  ##############################################################################
  def associa
    @servizio = find_by_servizio params[:idserv]
    @ele = Mod.find(params[:idm])
    media = @ele.upload
    @m_array = []
    media.each do |m|
      @m_array.push(m.id)
    end
    @upload   = Upload.all
    @ufolder  = UploadFolder.all
  end

  def getfile
    uploads = Upload.where("fkfolder=?", params[:idf])
=begin
    <li class="span2">
      <div class="thumbnail">
        <% if up.ext =~ /image/ %>
          <%= image_tag up.filename_url(:micro), :class => "img-polaroid" %>
        <% else %>
          <%= image_tag "file_pdf.png", :class => "img-polaroid" %>
        <% end %>
        <br>
        <% check = "" %>
        <% if !@m_array.nil? && (@m_array.include? up.id) %>
          <% check = "checked='checked'" %>
        <% end %>
        <input type="checkbox" <%= check %> name="rimuovi" id="modal-<%= up.id %>" value="<%= up.id %>" />
        <%= truncate(print_file_name(up.filename_url), :length => 10) %>
      </div>
    </li>
=end
    out = ""
    uploads.each do |up|
      out << "<li class='span2'><div class='thumbnail'>"
      if up.ext =~ /image/
        out << "<img src='#{up.filename_url(:micro)}' class='img-polaroid' />"
      else
        out << "<img src='/assets/file_pdf.png' class='img-polaroid' />"
      end
      out << "<br/>"
      check = (false && (!@m_array.nil? && (@m_array.include? up.id))) || "checked='checked'"
      out << "<input type='checkbox' #{check} name='rimuovi' id='modal-#{up.id}' value='#{up.id}' />"
    end

    render :json => {"html" => out}
  end

  def associafile
    ele = Mod.find(params[:ide])
    ass_ele = params[:id].split(',')
    ass_ele.each do |media|
      md = Media.where("fkfile=? AND fkmod=?", media, ele.id)
      descrizione = md.first.descrizione unless md.first.nil?
      Media.delete(md)

      if descrizione.nil?
        Media.create!({:fkfile => media, :fkmod => ele.id})
      else
        Media.create!({:fkfile => media, :fkmod => ele.id, :descrizione => descrizione})
      end
    end
    str = ""

    ele.upload.each do |up|
      if up.ext =~ /image/
        str_file = "<img src='#{up.filename_url(:micro)}' />"
      else
        str_file = "<img src='/assets/file_pdf.png'  />"
      end
      nome_file = up.filename.to_s.split("/").last

      str += "<tr>
              <td><a href='#' style='cursor:hand'><span class='icon-move'></span></a></td>
              <td>
                #{str_file}
              </td>
              <td>
                <textarea id='text-#{up.id}' class='img-text' name='text-#{up.id}' style='width:100%;height:50px;'>#{Media.find_by_fkmod_and_fkfile(ele.id,up.id).descrizione}</textarea>
                <br/>
                #{nome_file[0..70]}...
              </td>
              <td><input type='checkbox' name='rimuovi' id='remove-#{up.id}' value='#{up.id}' /></td>
              </tr>
            "
    end
    render :json => {"txt" => str }
  end

  def destroyimages
    del_ele = params[:id].split(',')
    del_ele.each do |ele|
      #Media.where("fkfile=? AND fkmod=?", ele, params[:ide]).each { |media| media.destroy }
      Media.find(ele).destroy
    end

    render :nothing => true
  end

  def destroyallimages
    Media.where("fkmod=?", params[:id]).each { |ele| ele.destroy }

    redirect_to "/admin/media/associa?idserv=#{params[:idserv]}&idm=#{params[:id]}"
  end

  def updateimagetext
    media = Media.find(params[:id])
    #Media.delete( Media.where("fkfile=? AND fkmod=?", params[:id], params[:ide]) )
    #Media.create!({:fkfile => params[:id], :fkmod => params[:ide], :descrizione => params[:text]})
    media.descrizione = params[:text]
    media.save!

    render :nothing => true
  end

  def updateordermedia
    x = 1
    order_arr = params[:id].split(',')

    order_arr.each_with_index do |id, index|
      #logger.info "#{id} --> #{index}"
      order = {"ordine" => index + 1}

      mod = Media.find(id.to_i)
      mod.update_attributes(order)
    end

    render :nothing => true
  end


end
