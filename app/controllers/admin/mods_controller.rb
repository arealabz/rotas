class Admin::ModsController < Admin::AdminController
  before_filter :signed_in_user

  ##############################################################################
  #
  #                    SEZIONE CATEGORIE/SOTTOCATEGORIE
  #
  ##############################################################################

  def index
    @servizio = find_by_servizio params[:idserv]
    if params[:idp].nil? or params[:idp].eql?("0")
      @modules_cat = ModCat.joins(:sml).where("idserv=? AND fklang=? AND f_del='0' AND mod_cats.fkparent=?", params[:idserv], "it", "0").order("ordine")
      @id_parent = 0
    else
      @modules_cat = ModCat.joins(:sml).where("idserv=? AND mod_cats.fkparent=? AND fklang=? AND f_del='0'", params[:idserv], params[:idp], "it").order("ordine")

      @categoria = ModCat.find(params[:idp])
      @id_parent = @categoria.id

      # recupero anche la lista degli elementi di questa categoria per non lasciare una lista vuota
      # quando non ci sono sottocategorie ma solo elementi e si clicca sul nome della categoria nel breadcrumb
      @modules  = Mod.where("idserv=? AND fkcat=? AND f_del='0'", params[:idserv], params[:idp]).order("ordine")

    end
  end

  def addcat
    @servizio = find_by_servizio params[:idserv]
    @id_parent = params[:idp]

    @mod = ModCat.new
    mod_sml = @mod.sml.build
    @sml = {}
    @lingue.each do |lang|
      @sml[ lang.cod ] = nil
    end

    @settori  = ModCat.getDataByServizio( Servizi.find_by_nome('settori') )
    @applicazioni  = ModCat.getDataByServizio( Servizi.find_by_nome('applicazioni') )
  end

  def createcat
    gen = (true && params[:mods]) || {}
    gen['f_del'] = 0
    gen['published'] = 1
    gen['settori'] = params[:settori].keys.join("#")  unless params[:settori].nil?
    gen['applicazioni'] = params[:app].keys.join("#") unless params[:app].nil?

    @modcat = ModCat.new(gen)
    if @modcat.save
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['fkparent'] = @modcat.id


        mod_sml = ModCatSml.new(lingua)
        if mod_sml.save
          flash[:notice] = "Categoria salvata con successo"
        end
      end

      redirect_to "/admin/mods?idserv=#{@modcat.idserv}&idp=#{@modcat.fkparent}"
    end
  end

  def editcat
    @servizio = find_by_servizio params[:idserv]
    @mod = ModCat.find(params[:id])
    @id_parent = @mod.fkparent
    @sml = {}
    @lingue.each do |lang|
      sml_ele = @mod.sml.where('fklang=? AND fkparent=?', lang.cod, @mod.id).first
      @sml[ lang.cod ] = sml_ele
    end

    @settori  = ModCat.getDataByServizio( Servizi.find_by_nome('settori') )
    @applicazioni  = ModCat.getDataByServizio( Servizi.find_by_nome('applicazioni') )

    @sel_settori  = @mod.settori.split("#") unless @mod.settori.nil?
    @sel_app      = @mod.applicazioni.split("#") unless @mod.applicazioni.nil?
  end


  def updatecat
    @mod = ModCat.find(params[:id])

    gen = (true && params[:mods]) || {}
    gen['f_del'] = 0

    if params[:settori].nil?
      gen['settori'] = "#"
    else
      gen['settori'] = params[:settori].keys.join("#")
    end

    if params[:app].nil?
      gen['applicazioni'] = "#"
    else
      gen['applicazioni'] = params[:app].keys.join("#")
    end

    if @mod.update_attributes(gen)
      error = false
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['fkparent'] = @mod.id

        sml_mod = @mod.sml.where('fklang=? AND fkparent=?', lang.cod, @mod.id).first
        if !sml_mod.nil?
          if sml_mod.update_attributes(lingua)
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        else
          sml_mod = ModCatSml.new(lingua)
          if sml_mod.save
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        end

        render 'editcat' if error
      end

      redirect_to "/admin/mods?idserv=#{@mod.idserv}&idp=#{@mod.fkparent}" unless error
    else
      @servizio = find_by_servizio @mod.idserv
      @id_parent = @mod.fkparent
      @sml = {}
      @lingue.each do |lang|
        sml_ele = @mod.sml.where('fklang=? AND fkparent=?', lang.cod, @mod.id).first
        @sml[ lang.cod ] = sml_ele
      end

      @settori  = ModCat.getDataByServizio( Servizi.find_by_nome('settori') )
      @applicazioni  = ModCat.getDataByServizio( Servizi.find_by_nome('applicazioni') )

      @sel_settori  = @mod.settori.split("#") unless @mod.settori.nil?
      @sel_app      = @mod.applicazioni.split("#") unless @mod.applicazioni.nil?

      render 'editcat'
    end

  end


  def destroycat
    mod_cat = ModCat.find(params[:id])
    mod_cat.update_attributes({"f_del" => "1"})
    flash[:success] = "Categoria eliminata con successo."
    redirect_to "/admin/mods?idserv=#{params[:idserv]}&idp=#{mod_cat.fkparent}"
  end

  def deleteallcat
    del_ele = params[:id].split(',')
    del_ele.each { |ele| ModCat.find(ele).update_attributes({"f_del" => "1"}) }

    render :nothing => true
  end

  def publishedcat
    mod_cat = ModCat.find(params[:id])

    if mod_cat.published == 1
      mod_cat.update_attributes({"published" => 0})
    else
      mod_cat.update_attributes({"published" => 1})
    end

    redirect_to "/admin/mods?idserv=#{params[:idserv]}&idp=#{mod_cat.fkparent}"
  end

  def updateorder
    x = 1
    order_arr = params[:id].split(',')

    order_arr.each_with_index do |id, index|
      #logger.info "#{id} --> #{index}"
      order = {"ordine" => index + 1}

      mod = ModCat.find(id.to_i)
      mod.update_attributes(order)
    end

    render :nothing => true
  end


  ##############################################################################
  #
  #                    SEZIONE FOGLIE
  #
  ##############################################################################

  def list
    @servizio = find_by_servizio params[:idserv]
    @modules  = Mod.where("idserv=? AND fkcat=? AND f_del='0'", params[:idserv], params[:idc]).order("ordine")
    @cat      = ModCat.find(params[:idc])
  end

  def addele
    @servizio = find_by_servizio params[:idserv]
    @cat      = ModCat.find(params[:idp])
    @upload   = Upload.all
    @video    = Video.all

    if @servizio.id == 3
      @settori  = ModCat.getSettori( @cat.settori )
      @applicazioni  = ModCat.getSettori( @cat.applicazioni )
    end

    # read the configuration file for the fields
    @fields = YAML.load( File.open("#{Rails.root}/app/admin/configs/#{@servizio.nome}.yml") )

    @mod = Mod.new
    mod_sml = @mod.sml.build
    @sml = {}
    @lingue.each do |lang|
      @sml[ lang.cod ] = nil
    end
  end

  def createele
    gen = (true && params[:mods]) || {}
    gen['f_del'] = 0
    gen['settori'] = params[:settori].keys.join("#")  unless params[:settori].nil?
    gen['applicazioni'] = params[:app].keys.join("#") unless params[:app].nil?

    @mod = Mod.new(gen)
    if @mod.save
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['fkparent'] = @mod.id
        unless lingua['title'].nil?
          lingua['url_title'] = URI.escape(lingua['title'].gsub(" ", "-"))
        end

        mod_sml = ModSml.new(lingua)
        if mod_sml.save
          flash[:notice] = "Elemento salvato con successo"
        end
      end

      redirect_to "/admin/mods/editele?idserv=#{@mod.idserv}&idc=#{@mod.fkcat}&id=#{@mod.id}"
    else
      flash[:error] = "Errore salvataggio elemento"
      redirect_to "/admin/mods/list?idserv=#{@mod.idserv}&idc=#{@mod.fkcat}"
    end

  end

  def editele
    @servizio = find_by_servizio params[:idserv]
    @cat = ModCat.find(params[:idc])
    @upload   = Upload.all
    @video = Video.all

    if @servizio.id == 3
      @settori  = ModCat.getSettori( @cat.settori )
      @applicazioni  = ModCat.getSettori( @cat.applicazioni )
    end

    # read the configuration file for the fields
    @fields = YAML.load( File.open("#{Rails.root}/app/admin/configs/#{@servizio.nome}.yml") )

    @ele = Mod.find(params[:id])
    @sml = {}
    @lingue.each do |lang|
      sml_ele = @ele.sml.where('fklang=? AND fkparent=?', lang.cod, @ele.id).first
      @sml[lang.cod] = sml_ele
    end

    media = @ele.upload
    @m_array = []
    media.each do |m|
      @m_array.push(m.id)
    end

    video = @ele.video
    @v_array = []
    video.each do |v|
      @v_array.push(v.id)
    end

    @sel_settori  = @ele.settori.split("#") unless @ele.settori.nil?
    @sel_app      = @ele.applicazioni.split("#") unless @ele.applicazioni.nil?
  end


  def updateele
    @mod = Mod.find(params[:id])

    gen = (true && params[:mods]) || {}
    gen['f_del'] = 0
    gen['settori'] = params[:settori].keys.join("#")  unless params[:settori].nil?
    gen['applicazioni'] = params[:app].keys.join("#") unless params[:app].nil?

    if @mod.update_attributes(gen)
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['fkparent'] = @mod.id
        unless lingua['title'].nil?
          lingua['url_title'] = URI.escape(lingua['title'].gsub(" ", "-"))
        end

        sml_mod = @mod.sml.where('fklang=? AND fkparent=?', lang.cod, @mod.id).first
        if !sml_mod.nil?
          if sml_mod.update_attributes(lingua)
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        else
          sml_mod = ModSml.new(lingua)
          if sml_mod.save
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        end

        render 'edit' if error
      end
    end

    redirect_to "/admin/mods/list?idserv=#{@mod.idserv}&idc=#{@mod.fkcat}"
  end


  def destroyele
    mod = Mod.find(params[:id])
    mod.update_attributes({"f_del" => "1"})
    flash[:success] = "Elemento eliminata con successo."
    redirect_to "/admin/mods/list?idserv=#{params[:idserv]}&idc=#{mod.fkcat}"
  end

  def deleteallele
    del_ele = params[:id].split(',')
    del_ele.each { |ele| Mod.find(ele).update_attributes({"f_del" => "1"}) }

    render :nothing => true
  end

  ##############################################################################
  #
  #                    SEZIONE IMMAGINI
  #
  ##############################################################################
  def associafile
    ele = Mod.find(params[:ide])
    ass_ele = params[:id].split(',')
    ass_ele.each do |media|
      md = Media.where("fkfile=? AND fkmod=?", media, ele.id)
      descrizione = md.first.descrizione unless md.first.nil?
      Media.delete(md)

      if descrizione.nil?
        Media.create!({:fkfile => media, :fkmod => ele.id})
      else
        Media.create!({:fkfile => media, :fkmod => ele.id, :descrizione => descrizione})
      end
    end
    str = ""

    ele.upload.each do |up|
      if up.ext =~ /image/
        str_file = "<img src='#{up.filename_url(:micro)}' />"
      else
        str_file = "<img src='/assets/file_pdf.png'  />"
      end
      nome_file = up.filename.to_s.split("/").last

      str += "
              <div style='width:30%;float:left;margin-bottom:20px;'>
                #{str_file}
              </div>
              <div style='width:70%;float:right;margin-bottom:20px;'>
                <textarea id='text-#{up.id}' name='text-#{up.id}' style='width:100%;height:50px;'>#{Media.find_by_fkmod_and_fkfile(ele.id,up.id).descrizione}</textarea>
                <br/>
                <input type='checkbox' name='rimuovi' id='remove-#{up.id}' value='#{up.id}' />
                #{nome_file[0..70]}...
              </div>
              <div style='clear:both'></div>
            "
    end
    del_str = "
                <a href='#' id='elimina-immagini'>Elimina immagini selezionate</a>
                <a href='/admin/mods/destroyallimages?idserv=#{ele.idserv}&idc=#{ele.fkcat}&id=#{ele.id}' data-confirm='Sei sicuro di voler eliminare tutte le immagini?' data-method='delete' rel='nofollow'>Elimina tutte le immagini</a>
              "
    render :json => {"txt" => str, "del" => del_str}
  end

  def destroyimages
    del_ele = params[:id].split(',')
    del_ele.each do |ele|
      Media.where("fkfile=? AND fkmod=?", ele, params[:ide]).each { |media| media.destroy }
    end

    render :nothing => true
  end

  def destroyallimages
    Media.where("fkmod=?", params[:id]).each { |ele| ele.destroy }

    redirect_to "/admin/mods/editele?idserv=#{params[:idserv]}&idc=#{params[:idc]}&id=#{params[:id]}"
  end

  def updateimagetext
    Media.delete( Media.where("fkfile=? AND fkmod=?", params[:id], params[:ide]) )
    Media.create!({:fkfile => params[:id], :fkmod => params[:ide], :descrizione => params[:text]})

    render :nothing => true
  end


  ##############################################################################
  #
  #                    SEZIONE VIDEO
  #
  ##############################################################################
  def associavideo
    ele = Mod.find(params[:ide])
    ass_ele = params[:id].split(',')
    ass_ele.each do |media|
      md = MediaVideo.where("fkvideo=? AND fkmod=?", media, ele.id)
      descrizione = md.first.descrizione unless md.first.nil?
      MediaVideo.delete(md)

      if descrizione.nil?
        MediaVideo.create!({:fkvideo => media, :fkmod => ele.id})
      else
        MediaVideo.create!({:fkvideo => media, :fkmod => ele.id, :descrizione => descrizione})
      end
    end
    str = ""

    ele.video.each do |up|
      str_file = "<img src='#{up.img}' width='80' height='80' />"
      nome_file = up.title

      str += "
              <div style='width:30%;float:left;margin-bottom:20px;'>
                #{str_file}
              </div>
              <div style='width:70%;float:right;margin-bottom:20px;'>
                <textarea id='videotext-#{up.id}' name='videotext-#{up.id}' style='width:100%;height:50px;'>#{MediaVideo.find_by_fkmod_and_fkvideo(ele.id,up.id).descrizione}</textarea>
                <br/>
                <input type='checkbox' name='rimuovi' id='remove-#{up.id}' value='#{up.id}' />
                #{nome_file[0..70]}...
              </div>
              <div style='clear:both'></div>
            "
    end
    del_str = "
                <a href='#' id='elimina-immagini'>Elimina immagini selezionate</a>
                <a href='/admin/mods/destroyallvideos?idserv=#{ele.idserv}&idc=#{ele.fkcat}&id=#{ele.id}' data-confirm='Sei sicuro di voler eliminare tutti i video?' data-method='delete' rel='nofollow'>Elimina tutti i video associati</a>
              "
    render :json => {"txt" => str, "del" => del_str}
  end

  def destroyvideo
    del_ele = params[:id].split(',')
    del_ele.each do |ele|
      MediaVideo.where("fkvideo=? AND fkmod=?", ele, params[:ide]).each { |media| media.destroy }
    end

    render :nothing => true
  end

  def destroyallvideo
    MediaVideo.where("fkmod=?", params[:id]).each { |ele| ele.destroy }

    redirect_to "/admin/mods/editele?idserv=#{params[:idserv]}&idc=#{params[:idc]}&id=#{params[:id]}"
  end

  def updatevideotext
    MediaVideo.delete( MediaVideo.where("fkvideo=? AND fkmod=?", params[:id], params[:ide]) )
    MediaVideo.create!({:fkvideo => params[:id], :fkmod => params[:ide], :descrizione => params[:text]})

    render :nothing => true
  end


  def publishedele
    mod = Mod.find(params[:id])

    if mod.published == 1
      mod.update_attributes({"published" => 0})
    else
      mod.update_attributes({"published" => 1})
    end

    redirect_to "/admin/mods/list?idserv=#{mod.idserv}&idc=#{mod.fkcat}"
  end

  def updateorderele
    x = 1
    order_arr = params[:id].split(',')

    order_arr.each_with_index do |id, index|
      #logger.info "#{id} --> #{index}"
      order = {"ordine" => index + 1}

      mod = Mod.find(id.to_i)
      mod.update_attributes(order)
    end

    render :nothing => true
  end

  private
    def correct_user
      @user = User.find(params[:id])
      redirect_to(admin_root_path) unless current_user?(@user)
    end


end
