class Admin::TranslationsController < Admin::AdminController
  before_filter :signed_in_user

  def index
    @translations = Translation.all
  end

  def new
    @translation = Translation.new
    translation_sml = @translation.sml.build
    @lingue.each do |lang|
      @sml = {lang.cod => nil}
    end
  end

  def create
    gen = (true && params[:translation]) || {}

    @translation = Translation.new(gen)
    if @translation.save
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['translation_id'] = @translation.id

        #@static.static_page_sml.create(lingua)
        translation_sml = TranslationSml.new(lingua)
        if translation_sml.save
          flash[:notice] = "Traduzione salvata con successo"
          redirect_to admin_translations_path
        else
          render 'new'
        end
      end
    end
  end

 def edit
    @static = StaticPage.find(params[:id])

    @sml = {}
    @lingue.each do |lang|
      sml_ele = @static.sml.where('fklang=? AND static_page_id=?', lang.cod, @static.id).first
      @sml = {lang.cod => sml_ele}
    end

    cats = ModCat.fetchAllCatForServizi
    @sel_cat = generateSelectFromCats cats
  end

  def update
    @static = StaticPage.find(params[:id])

    gen = (true && params[:static_page]) || {}
    gen['f_del'] = 0

    unless params['moduli'].nil?
      str_cat = params['moduli'].keys.join(",")
    else 
      str_cat = ""
    end
    gen['related_cat'] = str_cat

    if @static.update_attributes(gen)
      @lingue.each do |lang|
        lingua = params[lang.cod]
        lingua['fklang'] = lang.cod
        lingua['static_page_id'] = @static.id

        sml_mod = @static.sml.where('fklang=? AND static_page_id=?', lang.cod, @static.id).first
        if !sml_mod.nil?
          if sml_mod.update_attributes(lingua)
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        else
          sml_mod = ModulesCatSml.new(lingua)
          if sml_mod.save
            flash[:notice] = 'Elemento aggiornato con successo.'
          else
            error = true
          end
        end

        render 'edit' if error
      end
    end

    redirect_to admin_static_pages_path     
  end

  def destroy
    StaticPage.find(params[:id]).update_attributes({"f_del" => "1"})
    flash[:success] = "Pagina statica eliminata con successo."
    redirect_to admin_static_pages_path
  end
end