class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter { @categorie_footer = ModCat.getCatForServizio( Servizi.find_by_nome("realizzazioni") ) }
end
