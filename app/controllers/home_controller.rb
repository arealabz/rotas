class HomeController < ApplicationController

	before_filter {
		@produzione_cat = ModCat.getCatForServizio( Servizi.find_by_nome('produzione') )
		@banner = ModCat.getDataByServizio( Servizi.find_by_nome('banner') )
		@blog = ModCat.getDataByServizio(Servizi.find_by_nome("blog"))
		@more_info = StaticPage.find_by_url_pagina("/moreinfo")
		@social = StaticPage.find_by_url_pagina("/social")
	}


	def index
		@err = params[:err]
	end

	def singlepage
		@pagina = StaticPage.find_by_url_pagina("/#{params[:url]}")

		logger.info @pagina.inspect
		render :layout => "single"
	end

=begin
  def rotas
    @pagina = StaticPage.find_by_url_pagina("/rotas")

    render :layout => "single"
  end

  def storia
    @pagina = StaticPage.find_by_url_pagina("/storia")

    render :layout => "single"
  end

  def innovazione
    @pagina = StaticPage.find_by_url_pagina("/innovazione")

    render :layout => "single"
  end

  def company
    @pagina = StaticPage.find_by_url_pagina("/company-profile")

    render :layout => "single"
  end

  def rotasinstitute
    @pagina = StaticPage.find_by_url_pagina("/rotas-institute")

    render :layout => "single"
  end

  def bandi
    @pagina = StaticPage.find_by_url_pagina("/bandi")

    render :layout => "single"
  end

  def about
    @pagina = StaticPage.find_by_url_pagina("/about")
    render :layout => "single"
  end
=end

  def job
  	render :layout => "single"
  end

  def clienti
  	render :layout => "single"
  end

  def contact
  	render :layout => "single"
  end

	def produzione
		servizio = Servizi.find_by_nome('produzione')

		idc = params[:type].split("-").last

		# recupero informazioni categoria
		@categoria = ModCat.find(idc)
		@sml = {}
		@sml['it'] = @categoria.sml.where('fklang=? AND fkparent=?', "it", @categoria.id).first

		if params[:sett].nil? && params[:app].nil?
			@child = Mod.findTotChild(@categoria.id)
		elsif params[:sett]
			@child = Mod.findTotChildBySettore(@categoria.id, params[:sett])
		elsif params[:app]
			@child = Mod.findTotChildByApp(@categoria.id, params[:app])
		end

		@settori = []
		if @categoria.settori
			@settori = ModCat.getSettori(@categoria.settori)
		end

		@applicazioni = []
		if @categoria.applicazioni
			@applicazioni = ModCat.getSettori(@categoria.applicazioni)
		end

		render :layout => "produzione"
	end

	def produzionefull
		servizio = Servizi.find_by_nome('produzione')

		idc = params[:type].split("-").last

		# recupero informazioni categoria
		@categoria = ModCat.find(idc)
		@sml = {}
		@sml['it'] = @categoria.sml.where('fklang=? AND fkparent=?', "it", @categoria.id).first

		if params[:sett].nil? && params[:app].nil?
			@child = Mod.findTotChild(@categoria.id)
		elsif params[:sett]
			@child = Mod.findTotChildBySettore(@categoria.id, params[:sett])
		elsif params[:app]
			@child = Mod.findTotChildByApp(@categoria.id, params[:app])
		end

		@settori = []
		if @categoria.settori
			@settori = ModCat.getSettori(@categoria.settori)
		end

		@applicazioni = []
		if @categoria.applicazioni
			@applicazioni = ModCat.getSettori(@categoria.applicazioni)
		end

		render :layout => "produzione"
	end

	def portfolio
		params_arr = params[:type].split("-")
		idp = params_arr.pop
		idc = params_arr.pop

		@elemento = Mod.find(idp)

		# recupero informazioni categoria
		@categoria = ModCat.find(idc)
		@sml = {}
		@sml['it'] = @categoria.sml.where('fklang=? AND fkparent=?', "it", @categoria.id).first

		if params[:sett].nil? && params[:app].nil?
			@child = Mod.findTotChild(@categoria.id)
		elsif params[:sett]
			@child = Mod.findTotChildBySettore(@categoria.id, params[:sett])
		elsif params[:app]
			@child = Mod.findTotChildByApp(@categoria.id, params[:app])
		end

		@settori = ModCat.getSettori(@categoria.settori)
		@applicazioni = ModCat.getSettori(@categoria.applicazioni)

		unless @elemento.ordine.nil?
			@succ = Mod.findPrevNext((@elemento.ordine - 1),@elemento.fkcat,@elemento.idserv)
			@prev = Mod.findPrevNext((@elemento.ordine + 1),@elemento.fkcat,@elemento.idserv)
		end

		@info = {}
		["materiale", "superficie", "supporto", "adesione", "inchiostro", "rfid"].each do |ele|
			@info["#{ele}"] = StaticPage.find_by_url_pagina("/#{ele}")
		end

		render :layout => "inner"
	end

	def blog
		@elementi = ModCat.getDataByServizio(Servizi.find_by_nome("blog"))

		if params[:id]
			@elemento = Mod.find(params[:id])
		else
			@elemento = Mod.find(@elementi.first['gen'].id)
		end


		render :layout => "single"
	end

	def sendcontatti

    @obj = {}
    params.each do |par|
      #logger.info par[0]
      if par[0] =~ /p_/
        @obj[ par[0].gsub("p_", "") ] = par[1]
      end
    end

    ContattiMailer.contact_email(@obj).deliver
	end

	def sendjob

    @obj = {}
    params.each do |par|
      #logger.info par[0]
      if par[0] =~ /p_/
        @obj[ par[0].gsub("p_", "") ] = par[1]
      end
    end

    ContattiMailer.contact_email(@obj).deliver
    render :nothing => true
	end

end


