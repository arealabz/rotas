class CreateTranslationSmls < ActiveRecord::Migration
  def change
    create_table :translation_smls do |t|
      t.integer :translation_id
      t.text :testo

      t.timestamps
    end
  end
end
