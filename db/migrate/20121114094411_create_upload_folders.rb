class CreateUploadFolders < ActiveRecord::Migration
  def change
    create_table :upload_folders do |t|
      t.integer :fkparent
      t.string :nome

      t.timestamps
    end
  end
end
