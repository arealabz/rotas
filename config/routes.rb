Rotas::Application.routes.draw do
  root :to => "home#index"

  match '/rotas/:url',             to: 'home#singlepage'
  match '/innovazione',             to: 'home#innovazione'
  match '/storia',                  to: 'home#storia'
  match '/company-profile',         to: 'home#company'
  match '/rotas-institute',         to: 'home#rotasinstitute'
  match '/bandi',                   to: 'home#bandi'
  match '/about',                   to: 'home#about'
  match '/produzione/:type',        to: 'home#produzione'
  match '/produzione/:sa/:type',    to: 'home#produzionefull'
  match '/portfolio/:type',         to: 'home#portfolio'
  match '/blog',                    to: 'home#blog'
  match '/contatti',                to: 'home#sendcontatti', via: :post
  match '/sendjob',                 to: 'home#sendjob',      via: :post
  match '/job',                     to: 'home#job'
  match '/clienti',                 to: 'home#clienti'
  match '/contact',                 to: 'home#contact'

  namespace :admin do
    root :to => "home#index"

    resources :sessions, only: [:create, :destroy]
    resources :static_pages, only: [:index, :new, :create, :edit, :update, :destroy]
    resources :servizis
    resources :media, only: [:index, :create, :destroy]
    resources :video
    resource  :home

    match '/main',      to: 'home#main'
    match '/signin',    to: "sessions#new"
    match '/signout',   to: "sessions#destroy", via: :delete

    match '/static_pages/addmoduli/:id',    to: "static_pages#addmoduli"
    match '/static_pages/savemoduli',   to: "static_pages#savemoduli", via: :post
    match '/static_pages/updateorder',      to: "static_pages#updateorder"


    match '/servizis/deleteall',  to: "servizis#deleteall", via: :post

    # custom routes for mods management
    # category and sub-category management
    match '/mods',                to: "mods#index"
    match '/mods/addcat',         to: "mods#addcat"
    match '/mods/createcat',      to: "mods#createcat",     via: :post
    match '/mods/editcat',        to: "mods#editcat"
    match '/mods/updatecat',      to: "mods#updatecat",     via: :post
    match '/mods/destroycat',     to: "mods#destroycat",    via: :delete
    match '/mods/deleteallcat',   to: "mods#deleteallcat",  via: :post
    match '/mods/publishedcat',   to: "mods#publishedcat"
    match '/mods/updateorder',    to: "mods#updateorder",   via: :post

    # leaf management
    match '/mods/list',             to: "mods#list"
    match '/mods/addele',           to: "mods#addele"
    match '/mods/createele',        to: "mods#createele",         via: :post
    match '/mods/editele',          to: "mods#editele"
    match '/mods/updateele',        to: "mods#updateele",         via: :post
    match '/mods/destroyele',       to: "mods#destroyele",        via: :delete
    match '/mods/deleteallele',     to: "mods#deleteallele",      via: :post
    match '/mods/associafile',      to: "mods#associafile",       via: :post
    match '/mods/destroyimages',    to: "mods#destroyimages",     via: :post
    match '/mods/destroyallimages', to: "mods#destroyallimages",  via: :delete
    match '/mods/updateimagetext',  to: "mods#updateimagetext",   via: :post
    match '/mods/publishedele',     to: "mods#publishedele"
    match '/mods/updateorderele',   to: "mods#updateorderele",    via: :post

    match '/mods/associavideo',     to: "mods#associavideo",      via: :post
    match '/mods/destroyvideo',     to: "mods#destroyvideo",      via: :post
    match '/mods/destroyallvideo',  to: "mods#destroyallvideo",   via: :delete
    match '/mods/updatevideotext',  to: "mods#updatevideotext",   via: :post


    # media management
    match '/media/destroyall',          to: "media#destroyall",         via: :post
    match '/media/create_categoria',    to: "media#create_categoria",   via: :post
    match '/media/delcat',              to: "media#delcat",             via: :post
    match '/media/movecat',             to: "media#movecat",            via: :post
    match '/media/associa',             to: "media#associa"
    match '/media/associafile',         to: "media#associafile",       via: :post
    match '/media/destroyimages',       to: "media#destroyimages",     via: :post
    match '/media/destroyallimages',    to: "media#destroyallimages",  via: :delete
    match '/media/updateimagetext',     to: "media#updateimagetext",   via: :post
    match '/media/updateordermedia',    to: "media#updateordermedia",  via: :post
    match '/media/getfile',             to: "media#getfile"

    # video management
    match '/video/update',            to: "video#update",           via: :post
  end
end
